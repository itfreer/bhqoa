// 外来组件加载控件变量
var arcGISObject = {hasInitFinsh:false, initFinshEvent:null};
var cesiumObject = {hasInitFinsh:false, initFinshEvent:null};
var echartsObject = {hasInitFinsh:false, initFinshEvent:[]};
var editObject = {hasInitFinsh:false, initFinshEvent:[]};

require.config({
    baseUrl: './',
    paths: {
        'jquery-easyui' : 'http://localhost/web-base/jquery-easyui/v1.5.2/jquery.easyui.min',
        'jquery-easyui-zh' : 'http://localhost/web-base/jquery-easyui/v1.5.2/locale/easyui-lang-zh_CN',

        'angular': 'http://localhost/web-base/angular/v1.3.0/angular.min',
        'angular-route': 'http://localhost/web-base/angular/v1.3.0/angular-route.min',
        'angular-ui-router': 'http://localhost/web-base/angular/v1.3.0/angular-ui-router.min',
        'angular-async-loader': 'http://localhost/web-base/angular-async-loader/angular-async-loader.min',
		'jquery-easyui' : 'http://localhost/web-base/jquery-easyui/v1.5.2/easyui.min',
        'jquery-easyui-zh' : 'http://localhost/web-base/jquery-easyui/v1.5.2/locale/easyui-lang-zh_CN',
        'easyui': 'http://localhost/angular-base/dest/ui/easyui',
        'form': 'http://localhost/angular-base/dest/form/form',
        'security': 'http://localhost/angular-base/dest/security/security',

        'app' : 'app',
        'app-routes': 'app-routes',
        
        'echarts':'http://localhost/web-base/echarts/echarts.min',
        'echarts.china':'http://localhost/web-base/echarts/china.map',
        'ui.echarts':'http://localhost/angular-base/angular-ui/extend/charts/ui-echarts',

        'plupload':'http://localhost/web-base/plupload/v2.1.4/plupload.full.min',
        'plupload.queue':'http://localhost/web-base/plupload/v2.1.4/jquery.plupload.queue/jquery.plupload.queue.min',
        'plupload.i18n':'http://localhost/web-base/plupload/v2.1.4/i18n/zh_CN',

        'as-files' : 'http://localhost/angular-base/angular-ui/extend/upload/as-files',
        'ui-upload' : 'http://localhost/angular-base/angular-ui/extend/upload/ui-upload',

        'kindeditor':'http://localhost/web-base/kindeditor/v4.1.11/kindeditor-all',
        'kindeditor.i18n':'http://localhost/web-base/kindeditor/v4.1.11/zh-CN',
        'ui-edit':'http://localhost/angular-base/angular-ui/extend/edit/ui-edit',
        
        'bpm-init':'http://localhost/angular-base/angular-activiti/base/bpm-init',
        'bpm-workflow':'http://localhost/angular-base/angular-activiti/base/bpm-workflow',
        'bpm-workflow-sign':'http://localhost/angular-base/angular-activiti/base/bpm-workflow-sign',
        'bpm-workflow-back':'http://localhost/angular-base/angular-activiti/base/bpm-workflow-back',
        'bpm-workflow-skip':'http://localhost/angular-base/angular-activiti/base/bpm-workflow-skip',
		'bpm-workflow-retract':'http://localhost/angular-base/angular-activiti/base/bpm-workflow-retract',
        'bpm-workflow-disposal':'http://localhost/angular-base/angular-activiti/base/bpm-workflow-disposal',
        'bpm-selected' :'http://localhost/angular-base/angular-activiti/base/bpm-selected',
        'bpm-process' :'http://localhost/angular-base/angular-activiti/base/bpm-process'
    },
    packages: [
        { name: 'bmap', location: "http://localhost/web-base/arcgis/extent/bmap" },
        { name: 'tdmap', location: "http://localhost/web-base/arcgis/extent/tdmap" },
        { name: 'dijit', location: "http://localhost/web-base/arcgis/v3.21/dijit" },
        { name: 'dojo', location: "http://localhost/web-base/arcgis/v3.21/dojo" },
        { name: 'dojox', location: "http://localhost/web-base/arcgis/v3.21/dojox" },
        { name: 'esri', location: "http://localhost/web-base/arcgis/v3.21/esri" },
        { name: 'moment', location: "http://localhost/web-base/arcgis/v3.21/moment" }
    ],
    shim: {
        'angular': {exports: 'angular'},
    	'angular-route': {deps: ['angular']},
        'angular-ui-router': {deps: ['angular']},
        'angular-async-loader': {deps: ['angular']},

        'easyui': {deps: ['angular']},
        'form': {deps: ['angular']},
        'security' : {deps: ['angular']},
        
        'app-routes' : {deps: ['angular']},

        'kindeditor.i18n':{deps: ['kindeditor']},
        'ui-edit':{deps: ['angular']},

        'plupload':{exports: 'plupload'},
        'plupload.queue':{deps: ['plupload']},
        'plupload.i18n':{deps: ['plupload']},
        'as-files' :{deps: ['angular']},
        'ui-upload' :{deps: ['angular']},
        
        'bpm-workflow' :{deps: ['angular']},
        'bpm-workflow-sign' :{deps: ['angular']},
        'bpm-workflow-back' :{deps: ['angular']},
        'bpm-workflow-skip' :{deps: ['angular']},
		'bpm-workflow-retract' :{deps: ['angular']},
        'bpm-workflow-disposal' :{deps: ['angular']},
        
        'bpm-init' :{deps: ['angular']},
        'bpm-selected' :{deps: ['angular']},
        'bpm-process' :{deps: ['angular']},
    }
});

// angular app 加载
require(['angular','app','app-routes'], function (angular) {
    angular.element(document).ready(function () {
        angular.bootstrap(document, ['app']);
        angular.element(document).find('html').addClass('ng-app');
    });
});