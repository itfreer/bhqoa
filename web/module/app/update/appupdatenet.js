define(function (require) {
	require(['plupload','plupload.queue','plupload.i18n'], function () { 
	    
	});
	
	require(['kindeditor', 'kindeditor.i18n'], function () {
	    editObject.hasInitFinsh = true;
	    editObject.ke = KindEditor;
	    if(editObject.initFinshEvent){
	        editObject.initFinshEvent.forEach(function(e){
	        	e(KindEditor);
	        });
	    }
	});
	
	require('ui-upload');
	require('as-files');
	require('ui-edit');
	
	var app = require('app');
	app.useModule('ui.upload');
	app.useModule('ui.as.files');
	app.useModule('ui.edit');
    app.controller('appupdatenetCtrl', ['$scope','$rootScope', function($scope,$rootScope){
    	

	}]);
});