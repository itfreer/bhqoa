define(function (require) {
	
	
    var app = require('app');

    app.controller('messageBoardCtrl', ['$scope','$rootScope', function($scope,$rootScope){

		//重写新增方法，出来留言人id和name
		$scope.addMessage = function(formName, editRow, windowName){
			//留言者name和id取当前登录人name和id
			if(editRow.messagePersonName==null || editRow.messagePersonName==''){
				/*try{
					editRow.messagePersonName = $rootScope.curUser.userName;
					editRow.messagePersonId = $rootScope.curUser.userid;
					if(editRow.messageTime==null || editRow.messageTime==""){
						editRow.messageTime = $rootScope.formatDate(new Date());	
					}

				}catch(e){
					
				}*/
				//留言者name和id取当前登录人name和id，没有继承单点登录，先写死
				editRow.messagePersonName = '测试-超级管理员';
				editRow.messagePersonId = 'test_admin';
			}
			//新增默认回复状态为未审核
			if(editRow.state=='' || editRow.state==null){
				editRow.state = '1';
			};
        	$scope.save(formName, editRow, function() {
				$scope.$apply(function() {
					$.messager.alert('系统提示', '保存成功！');
					$rootScope.loadData('cms_messageboard');
					$rootScope.loadData('cms_messageboard_wsh');
					$rootScope.loadData('cms_messageboard_shtg');
					$rootScope.loadData('cms_messageboard_shwtg');
					$rootScope.loadData('cms_messageboard_dhf');
					$rootScope.loadData('cms_messageboard_yhf');
				
					$rootScope.closeWindow(windowName);
				});
			}, function() {
				$.messager.alert('系统提示', '保存失败！');
			});
		};

		//重置
 		$scope.resetQuery = function(){
			//全部 
			$rootScope['cms_messageboard'].queryObject = {};
			$rootScope.loadData('cms_messageboard');
			//未审核
			$rootScope['cms_messageboard_wsh'].queryObject = {'state:=':'1'};
			$rootScope.loadData('cms_messageboard_wsh');
			//审核通过
			$rootScope['cms_messageboard_shtg'].queryObject = {'state:=':'2'};
			$rootScope.loadData('cms_messageboard_shtg');
			//审核未通过
			$rootScope['cms_messageboard_shwtg'].queryObject = {'state:=':'3'};
			$rootScope.loadData('cms_messageboard_shwtg');
			//待回复
			$rootScope['cms_messageboard_dhf'].queryObject = {'replycontent:isnull':''};
			$rootScope.loadData('cms_messageboard_dhf');
			//已回复
			$rootScope['cms_messageboard_yhf'].queryObject = {'replycontent:isnnull':''};
			$rootScope.loadData('cms_messageboard_yhf');
		};
		
		//查询
		$scope.loadDataMessageboard = function(){
			
			var siteId = $rootScope['cms_messageboard'].queryObject['siteId'];
			var type = $rootScope['cms_messageboard'].queryObject['type'];
			var messageTitle = $rootScope['cms_messageboard'].queryObject['messageTitle'];
			//全部 
			$rootScope['cms_messageboard'].queryObject = {'siteId:like':siteId,'type:like':type,'messageTitle:like':messageTitle};
			$rootScope.loadData('cms_messageboard');
			//未审核
			$rootScope['cms_messageboard_wsh'].queryObject = {'siteId:like':siteId,'type:like':type,'messageTitle:like':messageTitle,'state:=':'1'};
			$rootScope.loadData('cms_messageboard_wsh');
			//审核通过
			$rootScope['cms_messageboard_shtg'].queryObject = {'siteId:like':siteId,'type:like':type,'messageTitle:like':messageTitle,'state:=':'2'};
			$rootScope.loadData('cms_messageboard_shtg');
			//审核未通过
			$rootScope['cms_messageboard_shwtg'].queryObject = {'siteId:like':siteId,'type:like':type,'messageTitle:like':messageTitle,'state:=':'3'};
			$rootScope.loadData('cms_messageboard_shwtg');
			//待回复
			$rootScope['cms_messageboard_dhf'].queryObject = {'siteId:like':siteId,'type:like':type,'messageTitle:like':messageTitle,'replycontent:isnull':''};
			$rootScope.loadData('cms_messageboard_dhf');
			//已回复
			$rootScope['cms_messageboard_yhf'].queryObject = {'siteId:like':siteId,'type:like':type,'messageTitle:like':messageTitle,'replycontent:isnnull':''};
			$rootScope.loadData('cms_messageboard_yhf');
		};
		
		
		$scope.formInitMessageAll = function(){
			
			//全部
    		$rootScope.formInit('cms_messageboard', {
				keyField:'id',
				randomFields:['id'],
				pageSize:10,
				orderObject:{id:0} 
			});
			$rootScope.loadData('cms_messageboard');

			//未审核
			$rootScope.formInit('cms_messageboard_wsh', {
				ssn:'cms_messageboard',
				esn:'cms_messageboard',
				keyField:'id',
				randomFields:['id'],
				pageSize:10,
				queryObject:{state:'1'},
				orderObject:{id:0} 
			});
			$rootScope.loadData('cms_messageboard_wsh');
			
			//审核通过
			$rootScope.formInit('cms_messageboard_shtg', {
				ssn:'cms_messageboard',
				esn:'cms_messageboard',
				keyField:'id',
				randomFields:['id'],
				pageSize:10,
				queryObject:{state:'2'},
				orderObject:{id:0} 
			});
			$rootScope.loadData('cms_messageboard_shtg');

			//审核未通过
			$rootScope.formInit('cms_messageboard_shwtg', {
				ssn:'cms_messageboard',
				esn:'cms_messageboard',
				keyField:'id',
				randomFields:['id'],
				pageSize:10,
				queryObject:{state:'3'},
				orderObject:{id:0} 
			});
			$rootScope.loadData('cms_messageboard_shwtg');
			
			//待回复
			$rootScope.formInit('cms_messageboard_dhf', {
				ssn:'cms_messageboard',
				esn:'cms_messageboard',
				keyField:'id',
				randomFields:['id'],
				pageSize:10,
				queryObject:{'replycontent:isnull':''},    
				//queryObject:{'mid:=':'e_elzy2011','dictionaryName:isnnull':''},
				orderObject:{id:0} 
			});
			$rootScope.loadData('cms_messageboard_dhf');
			
			//已回复
			$rootScope.formInit('cms_messageboard_yhf', {
				ssn:'cms_messageboard',
				esn:'cms_messageboard',
				keyField:'id',
				randomFields:['id'],
				pageSize:10,
				queryObject:{'replycontent:isnnull':''},    
				//queryObject:{'mid:=':'e_elzy2011','dictionaryName:isnnull':''},
				orderObject:{id:0} 
			});
			$rootScope.loadData('cms_messageboard_yhf');
		
		};
		



	}]);
});