define(function (require) {
	require(['plupload','plupload.queue','plupload.i18n'], function () { 
	    
	});

	require('ui-upload');
	require('as-files');

    var app = require('app');
    app.useModule('ui.upload');
    app.useModule('ui.as.files');
    app.controller('reviewsCtrl', ['$scope','$rootScope', function($scope,$rootScope){
		$scope.saveGzjjr = function(fName,editRow){
			if(editRow.id){
				editRow.id=$rootScope.getGUID();
			}
			$rootScope.save(fName, editRow, $rootScope.saveCloseWindow());
		}
	}]);
});