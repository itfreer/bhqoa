define(function(require){
    require(['plupload','plupload.queue','plupload.i18n'], function () { 
        
    });

    require(['kindeditor', 'kindeditor.i18n'], function () {
	    editObject.hasInitFinsh = true;
        editObject.ke = KindEditor;
        if(editObject.initFinshEvent){
            editObject.initFinshEvent.forEach(function(e){
            	e(KindEditor);
            });
        }
	});

	require('ui-upload');
    require('as-files');
	require('ui-edit');
	
    var app = require('app');
    app.useModule('ui.upload');
    app.useModule('ui.as.files');
    app.useModule('ui.edit');
    
    app.controller('documentCtrl', ['$scope','$rootScope', function($scope,$rootScope){
    	
    	//初始化方法
    	$scope.formInitAll = function(){
    		//子表 评论签批
    		$rootScope.formInit('comment', {
				keyField:'commentid',
				orderObject : {'spzt':0},
				pageSize:10,
				randomFields:['commentid'],
			});
    		
    		//全部
    		$rootScope.formInit('cms_document', {
				keyField:'id',
				randomFields:['id'],
				queryFields:'id,title,column,subject,publishDate,release,state',
				pageSize:10,
				orderObject:{id:0} 
			});
			$rootScope.loadData('cms_document');

    		//草稿
    		$rootScope.formInit('cms_document_cg', {
				ssn:'cms_document',
				esn:'cms_document',
				keyField:'id',
				randomFields:['id'],
				queryFields:'id,title,column,subject,publishDate,release,state',
				pageSize:10,
				queryObject:{state:'1'},
				orderObject:{id:0} 
			});
			$rootScope.loadData('cms_document_cg');
			
			//投稿  tg
			$rootScope.formInit('cms_document_tg', {
				ssn:'cms_document',
				esn:'cms_document',
				keyField:'id',
				randomFields:['id'],
				queryFields:'id,title,column,subject,publishDate,release,state',
				pageSize:10,
				queryObject:{state:'2'},
				orderObject:{id:0} 
			});
			$rootScope.loadData('cms_document_tg');
			//待审ds
			$rootScope.formInit('cms_document_ds', {
				ssn:'cms_document',
				esn:'cms_document',
				keyField:'id',
				randomFields:['id'],
				queryFields:'id,title,column,subject,publishDate,release,state',
				pageSize:10,
				queryObject:{state:'3'},
				orderObject:{id:0} 
			});
			$rootScope.loadData('cms_document_ds');
			
			//待发布dfb
			$rootScope.formInit('cms_document_dfb', {
				ssn:'cms_document',
				esn:'cms_document',
				keyField:'id',
				randomFields:['id'],
				queryFields:'id,title,column,subject,publishDate,release,state',
				pageSize:10,
				queryObject:{state:'4'},
				orderObject:{id:0} 
			});
			$rootScope.loadData('cms_document_dfb');
			
			//已发布yfb
			$rootScope.formInit('cms_document_yfb', {
				ssn:'cms_document',
				esn:'cms_document',
				keyField:'id',
				randomFields:['id'],
				queryFields:'id,title,column,subject,publishDate,release,state',
				pageSize:10,
				queryObject:{state:'5'},
				orderObject:{id:0} 
			});
			$rootScope.loadData('cms_document_yfb');
			
			//过期gq
			$rootScope.formInit('cms_document_gq', {
				ssn:'cms_document',
				esn:'cms_document',
				keyField:'id',
				randomFields:['id'],
				queryFields:'id,title,column,subject,publishDate,release,state',
				pageSize:10,
				queryObject:{state:'6'},
				orderObject:{id:0} 
			});
			$rootScope.loadData('cms_document_gq');
			
			//退稿tuig
			$rootScope.formInit('cms_document_tuig', {
				ssn:'cms_document',
				esn:'cms_document',
				keyField:'id',
				randomFields:['id'],
				queryFields:'id,title,column,subject,publishDate,release,state',
				pageSize:10,
				queryObject:{state:'7'},
				orderObject:{id:0} 
			});
			$rootScope.loadData('cms_document_tuig');
			
			//评论待审plds
			$rootScope.formInit('cms_document_plds', {
				ssn:'cms_document',
				esn:'cms_document',
				keyField:'id',
				randomFields:['id'],
				pageSize:10,
				//queryObject:{'mid:=':'e_elzy2011','dictionaryName:isnnull':''},
				//queryObject:{'comment:=':''},
				orderObject:{id:0} 
			});
			$rootScope.loadData('cms_document_plds');
    	};

        // 当前选择的栏目类型
        $scope.columnMode = '';
        $scope.columnModeName='';
        // 栏目选择
        $rootScope.cmsColumnSearch = function(node){
        	$scope.columnMode = node.other.mode;
        	var columnModeVar = node.other.mode;
        	 $scope.columnModeName=node.display;
        	//过滤数据
        	//全部
        	$rootScope['cms_document'].queryObject['column:like%'] = columnModeVar;
			$rootScope.loadData('cms_document');
        	//草稿
        	$rootScope['cms_document_cg'].queryObject['column:like%'] = columnModeVar;
			$rootScope.loadData('cms_document_cg');
			//投稿  tg
			$rootScope['cms_document_tg'].queryObject['column:like%'] = columnModeVar;
			$rootScope.loadData('cms_document_tg');
			//待审ds
			$rootScope['cms_document_ds'].queryObject['column:like%'] = columnModeVar;
			$rootScope.loadData('cms_document_ds');
			//待发布dfb
			$rootScope['cms_document_dfb'].queryObject['column:like%'] = columnModeVar;
			$rootScope.loadData('cms_document_dfb');
			//已发布yfb
			$rootScope['cms_document_yfb'].queryObject['column:like%'] = columnModeVar;
			$rootScope.loadData('cms_document_yfb');
			//过期gq
			$rootScope['cms_document_gq'].queryObject['column:like%'] = columnModeVar;
			$rootScope.loadData('cms_document_gq');
			//退稿tuig
			$rootScope['cms_document_tuig'].queryObject['column:like%'] = columnModeVar;
			$rootScope.loadData('cms_document_tuig');
			//评论待审plds
			$rootScope['cms_document_plds'].queryObject['column:like%'] = columnModeVar;
			$rootScope.loadData('cms_document_plds');
        };
        // 根据栏目类型，新增内容
        $scope.addNewDoc = function(){
            var m = $scope.columnMode;
            var columnModeName = $scope.columnModeName;

            if(m==null || m=='' || m=='undefined'){
            	$.messager.alert('系统提示', '请选择“栏目分类”！！！');
            }
            
            $rootScope["cms_document_column"] = {};
			$rootScope["cms_document_column"].editRow = [];
			$rootScope["cms_document_column"].editRow.column='';
			$rootScope["cms_document_column"].editRow.columnName='';
	
            //所属栏目赋值
            $rootScope["cms_document_column"].editRow.column = m;
            $rootScope["cms_document_column"].editRow.columnName = columnModeName;
            
            if(m=='NEWS'){
                $rootScope.addRow('cms_document') && $rootScope.openWindow('edit_news');
            } else if(m=='NOTICE'){
                $rootScope.addRow('cms_document') && $rootScope.openWindow('edit_notice');
            } else if(m=='PICTURE'){
                $rootScope.addRow('cms_document') && $rootScope.openWindow('edit_images');
            } else if(m=='VIDEO'){
                $rootScope.addRow('cms_document') && $rootScope.openWindow('edit_videos');
            } else if(m=='DOWNLOAD'){
                $rootScope.addRow('cms_document') && $rootScope.openWindow('edit_file');
            } else if(m=='LIBRARY'){
                $rootScope.addRow('cms_document') && $rootScope.openWindow('edit_pdf');
            } else if(m=='RECRUIT'){
                $rootScope.addRow('cms_document') && $rootScope.openWindow('edit_recruit');
            } else if(m=='PRODUCT'){
                $rootScope.addRow('cms_document') && $rootScope.openWindow('edit_products');
            }
        };
        
        //根据栏目类型，打开编辑弹窗
        $scope.openWindowByColumnMode = function(row){
       
       		$rootScope["cms_document_column"] = {};
			$rootScope["cms_document_column"].editRow = [];
			$rootScope["cms_document_column"].editRow.column='';
			$rootScope["cms_document_column"].editRow.columnName='';
        	//所属栏目赋值
            $rootScope["cms_document_column"].editRow.column = row.column;
            $rootScope["cms_document_column"].editRow.columnName = row.columnName;

        	var fl = row.column;
        	if(fl=='NEWS'){
               $rootScope.editRow('cms_document',row) && $rootScope.openWindow('edit_news');
            } else if(fl=='NOTICE'){
               $rootScope.editRow('cms_document',row) && $rootScope.openWindow('edit_notice');
            } else if(fl=='PICTURE'){
               $rootScope.editRow('cms_document',row) && $rootScope.openWindow('edit_images');
            } else if(fl=='VIDEO'){
               $rootScope.editRow('cms_document',row) && $rootScope.openWindow('edit_videos');
            } else if(fl=='DOWNLOAD'){
               $rootScope.editRow('cms_document',row) && $rootScope.openWindow('edit_file');
            } else if(fl=='LIBRARY'){
               $rootScope.editRow('cms_document',row) && $rootScope.openWindow('edit_pdf');
            } else if(fl=='RECRUIT'){
               $rootScope.editRow('cms_document',row) && $rootScope.openWindow('edit_recruit');
            } else if(fl=='PRODUCT'){
               $rootScope.editRow('cms_document',row) && $rootScope.openWindow('edit_products');
            }
        };
        
        //重写保存方法
        $scope.saveAdd = function(formName, editRow, windowName){
        	
        	if(editRow.column==null || editRow.column==''){
        		editRow.column = $scope.columnMode;
        	};
        	
        	if(editRow.state==null || editRow.state ==''){
        		editRow.state='1';
        	}
        	var stateColumn = editRow.state;
        	$scope.save(formName, editRow, function() {
				$scope.$apply(function() {
					$.messager.alert('系统提示', '保存成功！');
					$rootScope.closeWindow(windowName);
					
					if(stateColumn=='1'){
						$rootScope.loadData('cms_document');
						$rootScope.loadData('cms_document_cg');
					}else if(stateColumn=='2'){
						$rootScope.loadData('cms_document');
						$rootScope.loadData('cms_document_tg');
					}else if(stateColumn=='3'){
						$rootScope.loadData('cms_document');
						$rootScope.loadData('cms_document_ds');
					}else if(stateColumn=='4'){
						$rootScope.loadData('cms_document');
						$rootScope.loadData('cms_document_dfb');
					}else if(stateColumn=='5'){
						$rootScope.loadData('cms_document');
						$rootScope.loadData('cms_document_yfb');
					}else if(stateColumn=='6'){
						$rootScope.loadData('cms_document');
						$rootScope.loadData('cms_document_gq');
					}else if(stateColumn=='7'){
						$rootScope.loadData('cms_document');
						$rootScope.loadData('cms_document_tuig');
					}
				});
			}, function() {
				$.messager.alert('系统提示', '保存失败！');
				$rootScope.closeWindow(windowName);
			});
        };
 		
 		//重置
 		$scope.resetQuery = function(){
			//全部 
			$rootScope['cms_document'].queryObject =  {};
			$rootScope.loadData('cms_document');
			//草稿
			$rootScope['cms_document_cg'].queryObject =  {'state:=':'1'};
			$rootScope.loadData('cms_document_cg');
			//投稿  tg
			$rootScope['cms_document_tg'].queryObject =  {'state:=':'2'};
			$rootScope.loadData('cms_document_tg');
			//待审ds
			$rootScope['cms_document_ds'].queryObject =  {'state:=':'3'};
			$rootScope.loadData('cms_document_ds');
			//待发布dfb
			$rootScope['cms_document_dfb'].queryObject = {'state:=':'4'};
			$rootScope.loadData('cms_document_dfb');
			//已发布yfb
			$rootScope['cms_document_yfb'].queryObject = {'state:=':'5'};
			$rootScope.loadData('cms_document_yfb');
			//过期gq
			$rootScope['cms_document_gq'].queryObject =  {'state:=':'6'};
			$rootScope.loadData('cms_document_gq');
			//退稿tuig
			$rootScope['cms_document_tuig'].queryObject ={'state:=':'7'};
			$rootScope.loadData('cms_document_tuig');
			//评论待审plds
			$rootScope['cms_document_plds'].queryObject =  {};
			$rootScope.loadData('cms_document_plds');	
		};
		
		//查询
		$scope.loadDataDocument = function(){
			var subject = $rootScope['cms_document'].queryObject['subject'];
			var title = $rootScope['cms_document'].queryObject['title'];
			var release = $rootScope['cms_document'].queryObject['release'];
			var author = $rootScope['cms_document'].queryObject['author'];
			//全部 
			$rootScope['cms_document'].queryObject = {'subject:like':subject,'title:like':title,'release:like':release,'author:like':author};
			$rootScope.loadData('cms_document');
			//草稿
			$rootScope['cms_document_cg'].queryObject = {'subject:like':subject,'title:like':title,'release:like':release,'author:like':author,'state:=':'1'};
			$rootScope.loadData('cms_document_cg');
			//投稿  tg
			$rootScope['cms_document_tg'].queryObject = {'subject:like':subject,'title:like':title,'release:like':release,'author:like':author,'state:=':'2'};
			$rootScope.loadData('cms_document_tg');
			//待审ds
			$rootScope['cms_document_ds'].queryObject = {'subject:like':subject,'title:like':title,'release:like':release,'author:like':author,'state:=':'3'};
			$rootScope.loadData('cms_document_ds');
			//待发布dfb
			$rootScope['cms_document_dfb'].queryObject = {'subject:like':subject,'title:like':title,'release:like':release,'author:like':author,'state:=':'4'};
			$rootScope.loadData('cms_document_dfb');
			//已发布yfb
			$rootScope['cms_document_yfb'].queryObject = {'subject:like':subject,'title:like':title,'release:like':release,'author:like':author,'state:=':'5'};
			$rootScope.loadData('cms_document_yfb');
			//过期gq
			$rootScope['cms_document_gq'].queryObject = {'subject:like':subject,'title:like':title,'release:like':release,'author:like':author,'state:=':'6'};
			$rootScope.loadData('cms_document_gq');
			//退稿tuig
			$rootScope['cms_document_tuig'].queryObject = {'subject:like':subject,'title:like':title,'release:like':release,'author:like':author,'state:=':'7'};
			$rootScope.loadData('cms_document_tuig');
			//评论待审plds
			$rootScope['cms_document_plds'].queryObject = {'subject:like':subject,'title:like':title,'release:like':release,'author:like':author};
			$rootScope.loadData('cms_document_plds');	
		};
		
		
		
		
		//评论审核通过
		$scope.plshtg = function(formName,editRow,row){
			//spzt 2 审核通过
			if(row.spzt!='2' && row.spzt!='' && row.spzt!=null){
				row.spzt='2';
				row.spDate = $rootScope.formatDate(new Date());
				
				$scope.save(formName, editRow, function() {
				$scope.$apply(function() {
					$.messager.alert('系统提示', '审核通过！');
					$rootScope.loadData('cms_document_plds');
					$rootScope.closeWindow('edit_plsp');
				});
				}, function() {
					$.messager.alert('系统提示', '通过失败！');
					//$rootScope.closeWindow(windowName);
				});
			}
			
			//审核不通过
			if(row.spzt=='2'){
				row.spzt='3';
				row.spDate = $rootScope.formatDate(new Date());
				
				$scope.save(formName, editRow, function() {
				$scope.$apply(function() {
					$.messager.alert('系统提示', '审核不通过成功！');
					$rootScope.loadData('cms_document_plds');
					$rootScope.closeWindow('edit_plsp');
				});
			}, function() {
				$.messager.alert('系统提示', '不通过失败！');
				//$rootScope.closeWindow(windowName);
			});
			
			}
		
		};
		
	
 
    }]);
});