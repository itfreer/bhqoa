define(function (require) {
	require(['plupload','plupload.queue','plupload.i18n'], function () { 
	    
	});

	require('ui-upload');
	require('as-files');

    var app = require('app');
    app.useModule('ui.upload');
    app.useModule('ui.as.files');

    app.controller('qjglCtrl', ['$scope','$rootScope','$state', function($scope,$rootScope,$state,){
			/**
    	 * 启动新的流程示例
    	 * @return {[type]} [description]
    	 */
    	$scope.startNewBpm=function(url,bpmkey,formname){
    		var openurl=url+"?bk="+encodeURIComponent(bpmkey)
    			+"&fn="+encodeURIComponent(formname);
    		window.open(openurl);
    	};

    	/**
    	 * 查看项目过程
    	 * @param  {[type]} url      项目地址
    	 * @param  {[type]} bpmkey   流程类型键
    	 * @param  {[type]} formname 表单id
    	 * @param  {[type]} viewid   uiId
    	 * @param  {[type]} sexeid   流程项目主键
    	 * @return {[type]}          描述
    	 */
    	$scope.toQueryWork=function(url,bpmkey,formname,viewid,guid){
    		var openurl=url+"?bk="+encodeURIComponent(bpmkey)
    			+"&fn="+encodeURIComponent(formname)
    			+"&vi="+encodeURIComponent(viewid)
    			+"&dk="+encodeURIComponent(guid);
    		window.open(openurl);
    	};

    	/**
		 * 获取系统查询参数
		 */
		$scope.getQueryString=function(key){
	        var reg = new RegExp("(^|&)"+key+"=([^&]*)(&|$)");
	        var result = window.location.search.substr(1).match(reg);
	        return result?decodeURIComponent(result[2]):null;
	   };
	   
	   /**
	    * 计算天数功能
	    */
	   $scope.setTs=function(){
		   	var start=$rootScope.getDateObject($rootScope.gzqjgl.editRow.kssj);
		   	var fmstart = $rootScope.parseData(start,'string');
		   	var end=$rootScope.getDateObject($rootScope.gzqjgl.editRow.jssj);
		   	var fmend = $rootScope.parseData(end,'string');
		   	var i=(fmend - fmstart) / 1000 / 60 / 60 /24+1;
		   	
		   	var time=i.toFixed(0);
		   	if($rootScope.gzqjgl.editRow.sjsjjd==14 || $rootScope.gzqjgl.editRow.sjsjjd=='14'){
		   		time=time-0.5;
		   	}
		   	
		   	if($rootScope.gzqjgl.editRow.jssjjd==12 || $rootScope.gzqjgl.editRow.jssjjd=='12'){
		   		time=time-0.5;
		   	}
		   	$rootScope.gzqjgl.editRow.qjts=time;
	   };
	   
	   
	   $scope.qdgl=function(method,success,error){
	   		var url=getServerBaseUrl()+"/gzqdgl/qdgl";
	   		jQuery.support.cors = true;
	   		$.ajax({
				url : url,
				type : 'POST',
				data : {'method':method},
				timeout : 35000,
				dataType : 'json',
				headers: { 'jwt': $.cookie('jwt') },
				contentType: "application/json;charset=utf-8",
				success : function(data){
					if(data){
						success(data);
					}else{
						error();
					}
				},
				error : error
			});
	   };
	   
	     $scope.gzqdgldata=function(method){
		     	$scope.qdgl(method,function(data){	
					if(data){
				        if(data["status"]){
					        if(data["type"]=="create"){
					            $.messager.alert("系统提示", data["message"]);
					        }else{
					            $.messager.confirm("系统提示", data["message"],function(r){
					            	if(r){
					            		$scope.syqdgldata("update");
					            	}
					            });
					        }
					        $rootScope.loadData("gzqdgl");
				        }else{
				          $.messager.alert("系统提示", data["message"]);
				        }
			        }else{
			        	$.messager.alert("系统提示",'打卡失败，请重试！');
			      	}
		     	},function(){
		     		$.messager.alert("系统提示","打卡失败，请重试！");
		     	});
		    };
	     
	     /**
	      * 变化出差申请人时变化
	      */
	     $scope.onchangeCcglSqr=function(){
    		setTimeout(function(){
    			$rootScope.gzccgl.editRow.ccrxm=$rootScope.schedulefzr.selectedRow.userName;
    		},50);
    	};
		
		var attendance={};
		
		$scope.getAttendance = function(attendance){
			if ($rootScope.isOptioning) {
				$.messager.alert("系统提示", "数据正在处理中，请稍后！");
				return;
			}
			if(attendance.kssj==null){
				$.messager.alert("系统提示", "请选择开始时间！");
				return;
			}
			if(attendance.jssj==null){
				$.messager.alert("系统提示", "请选择结束时间！");
				return;
			}
			if(attendance.kssj>attendance.jssj){
				$.messager.alert("系统提示", "结束时间不能选在开始时间之前！");
				return;
			}
			var args=attendance;
			args.userid = $rootScope.curUser.userid;
			var baseUrl=getServerBaseUrl()+"/attendance/result";
			$.ajax({
				url : baseUrl,
				type : 'POST',
				data : args,
				timeout : 35000,
				dataType : 'json',
				headers: { 'jwt': $.cookie('jwt') },
				contentType: "application/json;charset=utf-8",
				success : function(data){
					$rootScope.isOptioning = false;
					if (data) {
						attendance.datas=data;
					}else{
						attendance.datas=[];
						$.messager.alert("系统提示","未查询到数据！");
					}
				},
				error : function(e){
					$.messager.alert("系统提示","获取数据失败，请重试！");
				} 
			});
		}
		
		//导出excel
		$scope.printKQTJ = function(fn,printType,list){
			var	row = {datas:list};
			var data=$.extend(true, data, row);
			if($rootScope.isOptioning){
				$.messager.alert("系统提示","系统处理中，请稍等！");
				return false;
			}
			console.log(data);
			$scope.print(fn,'kqtj.html',printType,data,'考勤统计.xls',null,null);
		};
		
		//编辑签到记录
		$scope.editQd =function(fn,editRow,wn){
			if(editRow == null){
				$.messager.alert("系统提示","请选择一条要编辑的数据！");
				return false;
			}
			if(wn == 'edit'){
				var qdsj=new Date(editRow.qdsj);
				var qtsj=new Date(editRow.qtsj);
				var xwqdsj=new Date(editRow.xwqdsj);
				var xwqtsj=new Date(editRow.xwqtsj);
				
				editRow.qds=qdsj.getHours();
				editRow.qdf=qdsj.getMinutes();
				editRow.qdm=qdsj.getSeconds();
				
				editRow.qts=qtsj.getHours();
				editRow.qtf=qtsj.getMinutes();
				editRow.qtm=qtsj.getSeconds();
				
				editRow.xwqds=xwqdsj.getHours();
				editRow.xwqdf=xwqdsj.getMinutes();
				editRow.xwqdm=xwqdsj.getSeconds();
				
				editRow.xwqts=xwqtsj.getHours();
				editRow.xwqtf=xwqtsj.getMinutes();
				editRow.xwqtm=xwqtsj.getSeconds();
			}
			var dkbzs=editRow.dkbz.split('；');
			var dkbz='';
			for(var i=0;i<dkbzs.length;i++){
				if(i!=(dkbzs.length-1)){
					dkbz+=dkbzs[i]+'\n';
				}else{
					dkbz+=dkbzs[i];
				}
			}
			editRow.dkbz=dkbz;
			var bzs=editRow.bz.split('；');
			var bz='';
			for(var i=0;i<bzs.length;i++){
				if(i!=(dkbzs.length-1)){
					bz+=bzs[i]+'\n';
				}else{
					bz+=bzs[i];
				}
			}
			editRow.bz=bz;
			
			$rootScope.editRow(fn, editRow);
			$rootScope.openWindow(wn);
		};
		
		$scope.setShi = function(fn,value,zName){
			if(isNaN(value)){
				$rootScope[fn].editRow[zName]=null;
			}else{
				if(value>23){
					$rootScope[fn].editRow[zName]=23;
				}else if(value<0){
					$rootScope[fn].editRow[zName]=0;
				}
			}
		};
		
		$scope.setFen = function(value,zName){
			if(isNaN(value)){
				$rootScope[fn].editRow[zName]=null;
			}else{
				if(value>59){
					$rootScope[fn].editRow[zName]=59;
				}else if(value<0){
					$rootScope[fn].editRow[zName]=0;
				}
			}
		};
		
		$scope.saveQd = function(fName,row,callback,onerror){
			var qdsj=new Date(row.qdsj);
			var qtsj=new Date(row.qtsj);
			var xwqdsj=new Date(row.xwqdsj);
			var xwqtsj=new Date(row.xwqtsj);
			qdsj.setHours(row.qds);
			qdsj.setMinutes(row.qdf);
			qdsj.setSeconds(row.qdm);
			row.qdsj=qdsj.getTime();
			qtsj.setHours(row.qts);
			qtsj.setMinutes(row.qtf);
			qtsj.setSeconds(row.qtm);
			row.qtsj=qtsj.getTime();
			xwqdsj.setHours(row.xwqds);
			xwqdsj.setMinutes(row.xwqdf);
			xwqdsj.setSeconds(row.xwqdm);
			row.xwqdsj=xwqdsj.getTime();
			xwqtsj.setHours(row.xwqts);
			xwqtsj.setMinutes(row.xwqtf);
			xwqtsj.setSeconds(row.xwqtm);
			row.xwqtsj=xwqtsj.getTime();
			$rootScope.save(fName,row,callback,onerror);
		};
	   
	}]);
});