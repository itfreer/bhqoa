define(function (require) {

    var app = require('app');

    app.controller('qdszCtrl', ['$scope','$rootScope','$state', function($scope,$rootScope,$state,){
		$scope.setYF = function(value,zName){
			if(isNaN(value)){
				$rootScope['gzgzrqdsj'].editRow[zName]=null;
			}else{
				if(value>12){
					$rootScope['gzgzrqdsj'].editRow[zName]=12;
				}else if(value<1){
					$rootScope['gzgzrqdsj'].editRow[zName]=1;
				}
			}
		}
		
		$scope.setRQ = function(value,zName){
			if(isNaN(value)){
				$rootScope['gzgzrqdsj'].editRow[zName]=null;
			}else{
				if(value>31){
					$rootScope['gzgzrqdsj'].editRow[zName]=31;
				}else if(value<1){
					$rootScope['gzgzrqdsj'].editRow[zName]=1;
				}
			}
		}
		
		$scope.setShi = function(value,zName){
			if(isNaN(value)){
				$rootScope['gzgzrqdsj'].editRow[zName]=null;
			}else{
				if(value>23){
					$rootScope['gzgzrqdsj'].editRow[zName]=23;
				}else if(value<0){
					$rootScope['gzgzrqdsj'].editRow[zName]=0;
				}
			}
		}
		
		$scope.setFen = function(value,zName){
			if(isNaN(value)){
				$rootScope['gzgzrqdsj'].editRow[zName]=null;
			}else{
				if(value>59){
					$rootScope['gzgzrqdsj'].editRow[zName]=59;
				}else if(value<0){
					$rootScope['gzgzrqdsj'].editRow[zName]=0;
				}
			}
		}
	   
	}]);
});