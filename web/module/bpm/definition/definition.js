define(function (require) {
	//require('security');
	//app.useModule('security');

    var app = require('app')
    .controller('bpm_definitonCtrl', ['$scope','$rootScope', function($scope,$rootScope){

    	// 逻辑批量删除，不提交到数据库
		$scope.delSelectedTasks = function(rowList){
			if(rowList==null){
				return;
			}
			var dRows = [];
			rowList.forEach(function(e){  
				if(e['selected']!=null && e['selected']==true){
					dRows.push(e);
				}
			});
			dRows.forEach(function(e){  
				rowList.remove(e);
			});
		};

    	$scope.addTask=function(configrow){
    		if(configrow.taskconfigs==null){
    			configrow.taskconfigs=[];
    		}
    		var task={};
    		task.taskid=$rootScope.getGUID();
    		task.actid=configrow.actid;
    		task.actdefid=configrow.actdefid;
    		configrow.taskconfigs.push(task);
    	};

    	$scope.editBpmSus=function(){
    		$rootScope.closeWindow('edit');
    		$rootScope.loadData('bpmconfig');
    	};

    	$scope.registerBpmSus=function(){
    		$rootScope.closeWindow('register');
    		$rootScope.loadData('bpmconfig');
    	};

    	$scope.saveTaskEditRow=function(){
    		$rootScope.bpmconfig.editRow.taskconfigs[$scope.taskEditIndex]=$.extend({},$rootScope.bpmtaskconfig.editRow);
    		$rootScope.closeWindow('cEdit');
    		return true;
    	};

    	$scope.setTaskEditRow=function(index){
    		$scope.taskEditIndex=index;
    		var r=$rootScope.bpmconfig.editRow.taskconfigs[index];
    		$rootScope.editRow('bpmtaskconfig', r);
    		return true;
    	};
		/**
		 * 注册流程服务
		 * @param  {[type]} id      [description]
		 * @param  {[type]} success [description]
		 * @param  {[type]} error   [description]
		 * @return {[type]}         [description]
		 */
		$scope.registerBpm =function(id,success,error) {
			if(id==null || id==''){
				$.messager.alert("系统提示","流程id不能为空");
				return;
			}
			var url=getServerBaseUrl()+"/bpm/model/deployModel/"+id;
			jQuery.support.cors = true;
			$.ajax({
				url : url,
				type : 'POST',
				data : null,
				timeout : 35000,
				dataType : 'json',
				contentType: "application/json;charset=utf-8",
				success : function(data){
					if(data!=null){
						if(data.status){
							$.messager.alert("系统提示","服务注册成功");
							if(success){
								success();
							}
						}else{
							$.messager.alert("系统提示","服务注册失败");
							if(error){
								error();
							}
						}
					}else{
						$.messager.alert("系统提示","服务注册失败");
						if(error){
								error();
							}
					}
				},
				error : function(e){
					$.messager.alert("系统提示","服务注册失败");
					if(error){
						error();
					}
				}
			});
		};


	}]);
});