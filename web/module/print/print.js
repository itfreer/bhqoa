define(function (require) {
	var app = require('app')
    .controller('printCtrl', ['$scope','$rootScope', function($scope,$rootScope){

    	$scope.downTest = function() {
    		var template = "bhq_gj.html";
			var	filename = "国家级自然保护区统计.xls";
			var	data = {datas:[{s_name:'察隅慈巴沟国家自然保护区',zmj:980.11,hxqmj:518.22,syqmj:228.24,hcqmj:233.65},{s_name:'察隅慈巴沟国家自然保护区',zmj:980.11,hxqmj:518.22,syqmj:228.24,hcqmj:233.65}]};
			$scope.toExcel(template,data,filename);
		};
    	
		$scope.toExcel = function(template,data,filename) {
			data = $rootScope.toJSONStr(data);
			var url = getServerBaseUrl()+'/print/toExcel';
	        var form = $("<form></form>").attr("action", url).attr("method", "post");
	        form.append($("<input></input>").attr("type", "hidden").attr("name", "template").attr("value", template));
	        form.append($("<input></input>").attr("type", "hidden").attr("name", "filename").attr("value", filename));
	        form.append($("<input></input>").attr("type", "hidden").attr("name", "data").attr("value", data));
	        form.appendTo('body').submit().remove();
		};

	}]);
});