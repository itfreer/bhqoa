define(function (require) {
	require(['plupload','plupload.queue','plupload.i18n'], function () { 
	    
	});

	require('ui-upload');
	require('as-files');

    var app = require('app');
    app.useModule('ui.upload');
    app.useModule('ui.as.files');

    app.controller('clglCtrl', ['$scope','$rootScope', function($scope,$rootScope){
		/**
    	 * 启动新的流程示例
    	 * @return {[type]} [description]
    	 */
    	$scope.startNewBpm=function(url,bpmkey,formname){
    		var openurl=url+"?bk="+encodeURIComponent(bpmkey)
    			+"&fn="+encodeURIComponent(formname);
    		window.open(openurl);
    	};

    	/**
    	 * 查看项目过程
    	 * @param  {[type]} url      项目地址
    	 * @param  {[type]} bpmkey   流程类型键
    	 * @param  {[type]} formname 表单id
    	 * @param  {[type]} viewid   uiId
    	 * @param  {[type]} sexeid   流程项目主键
    	 * @return {[type]}          描述
    	 */
    	$scope.toQueryWork=function(url,bpmkey,formname,viewid,guid){
    		var openurl=url+"?bk="+encodeURIComponent(bpmkey)
    			+"&fn="+encodeURIComponent(formname)
    			+"&vi="+encodeURIComponent(viewid)
    			+"&dk="+encodeURIComponent(guid);
    		window.open(openurl);
    	};

    	/**
		 * 获取系统查询参数
		 */
		$scope.getQueryString=function(key){
	        var reg = new RegExp("(^|&)"+key+"=([^&]*)(&|$)");
	        var result = window.location.search.substr(1).match(reg);
	        return result?decodeURIComponent(result[2]):null;
	    };
	   
	   	$scope.chooseClda = function(editRow,rEditRow,closeId){
	   		if(rEditRow != null){
				editRow.clxh=rEditRow.sclxh;
				editRow.cph=rEditRow.scph;
				$scope.closeWindow(closeId);
			}else{
				$.messager.alert("系统提示","请选择一个车辆！");
				return false;
			}
			
		};
		
		$scope.ghwxcl=function(fn,editRow){
			if(editRow.wxzt=="wxzt_yjs"){
				$.messager.alert("系统提示","车辆维修已结束！");
				return;
			}
			if(editRow != null){
				editRow.wxzt="wxzt_yjs";
				$scope.save(fn, editRow, $scope.closeWindow('edit'));
				$.messager.alert("系统提示","车辆维修结束！");
			}else{
				$.messager.alert("系统提示","请选择要结束维修的车辆！");
				return false;
			}
		};
		
		$scope.yjghcl=function(fn,editRow){
			/*if(editRow.sbmldsp==null){
				$.messager.alert("系统提示","分管领导签批过后才能归还车辆！");
				return;
			}
			if(editRow.clzt=="clsyzt_ygh"){
				$.messager.alert("系统提示","车辆已归还！");
				return;
			}*/
			//TODO 
			if(editRow != null){
				editRow.clzt="clsyzt_ygh";
				$scope.save(fn, editRow, function(){
					$scope.closeWindow('edit')
					$.messager.alert("系统提示","车辆归还成功！");
					$rootScope.loadData("gzclsq");
				});
				
			}else{
				$.messager.alert("系统提示","请选择要归还的车辆！");
				return false;
			}
		};
		
		/**
    	 * 添加新增记录
    	 */
    	$scope.addCldaRow=function(){
    		$rootScope.addRow('gzclda');
    		$rootScope.gzclda.editRow.scgbm=$rootScope.curUser.departmentId;
    		$rootScope.gzclda.editRow.scgbmName= $rootScope.curUser.departmentName;
    		$rootScope.gzclda.editRow.scgr=$rootScope.curUser.userid;
    		$rootScope.gzclda.editRow.scgrmc= $rootScope.curUser.userName;
    		$rootScope.openWindow('edit');
    	};
    	
    	$scope.saveClwxSuccess=function(){
    		$rootScope.closeWindow('edit');
    		
    		$rootScope.loadData("gzclwx");
    	};
    	
    	$scope.onchangeClwxFzr=function(){
    		setTimeout(function(){
    			$rootScope.gzclwx.editRow.sfzrmc=$rootScope.gzclwxfzr.selectedRow.userName;
    		},50);
    	};
    	
	}]);
});