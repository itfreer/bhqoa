define(function (require) {
	require(['plupload','plupload.queue','plupload.i18n'], function () { 
	    
	});

	require('ui-upload');
	require('as-files');

    var app = require('app');
    app.useModule('ui.upload');
    app.useModule('ui.as.files');

    app.controller('deviceCtrl', ['$scope','$rootScope', function($scope,$rootScope){
		
		$scope.chooseDevice = function(editRow,rEditRow,closeId){
	   		if(rEditRow != null){
				editRow.ssqbh=rEditRow.ssbbh;
				editRow.ssbbh=rEditRow.ssbbh;
				editRow.ssbmc=rEditRow.ssbmc;
				$scope.closeWindow(closeId);
			}else{
				$.messager.alert("系统提示","请选择一个设备！");
				return false;
			}
			
		};
	}]);
});