define(function (require) {
	var app = require('app')
    .controller('depCtrl', ['$scope','$rootScope', function($scope,$rootScope){

    	// 政区树选择查询
    	$rootScope.depOrgSearch = function(node){
    		$rootScope['department'].queryObject.organizationId = node.id;
    		$rootScope.loadData('department');
    	};

	}]);
});