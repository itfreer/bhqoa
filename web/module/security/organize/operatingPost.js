define(function (require) {
	var app = require('app')
    .controller('operatingPostCtrl', ['$scope','$rootScope', function($scope,$rootScope){

    	// 政区树选择查询
    	$rootScope.operatingPostOrgSearch = function(node){
    		$rootScope['operatingPost'].queryObject.organizationId = node.id;
    		$rootScope.loadData('operatingPost');
    	};

	}]);
});