define(function (require) {
	var app = require('app')
    .controller('workingGroupCtrl', ['$scope','$rootScope', function($scope,$rootScope){

    	// 政区树选择查询
    	$rootScope.workingGroupOrgSearch = function(node){
    		$rootScope['workingGroup'].queryObject.organizationId = node.id;
    		$rootScope.loadData('workingGroup');
    	};

	}]);
});