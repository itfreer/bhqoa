define(function (require) {
	var app = require('app')
    .controller('positionCtrl', ['$scope','$rootScope', function($scope,$rootScope){

    	// 政区树选择查询
    	$rootScope.positionOrgSearch = function(node){
    		$rootScope['position'].queryObject.organizationId = node.id;
    		$rootScope.loadData('position');
    	};

	}]);
});