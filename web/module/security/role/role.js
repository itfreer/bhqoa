define(function (require) {
    var app = require('app')
    .controller('roleCtrl', ['$scope','$rootScope', function($scope,$rootScope,$role){

        $scope.searchCurRole = true;
        $scope.searchNoCurRole = false;
        $scope.searchCurRoleName = '';
        $scope.searchCurRoleId = '';

        // 初始化加载当前角色下的用户
        $scope.searchUser = function(r){
            $scope.searchCurRole = true;
            $scope.searchCurRoleName = r.display;
            $scope.searchCurRoleId = r.id;
            $rootScope['vuserrole'].queryObject['roles:like'] = $scope.searchCurRoleId;
            $scope.loadUserData();
            return true;
        };

        // 加载用户，处理用户选中
        $scope.loadUserData = function(){
            $rootScope.loadData('vuserrole',function(fn,data){
                data.forEach(function(e){
                    if(e.roles){
                        if(e.roles.indexOf($scope.searchCurRoleId)>=0){
                            e['selected']=true;
                        }else{
                            e['selected']=false;
                        }
                    }else{
                        e['selected']=false;
                    }
                });
            });
        };

        // 只查询当前角色下的用户
        $scope.setSearch = function(){
            $scope.searchNoCurRole = false;
            if($rootScope['vuserrole'].queryObject.hasOwnProperty('roles:notlike')){
                delete $rootScope['vuserrole'].queryObject['roles:notlike'];
            }

            var field = 'roles:like';
            if($scope.searchCurRole){
                $rootScope['vuserrole'].queryObject[field] = $scope.searchCurRoleId;
            }else{
                if($rootScope['vuserrole'].queryObject.hasOwnProperty(field)){
                    delete $rootScope['vuserrole'].queryObject[field];
                }
            }
        };

        // 只查询非当前角色下的用户
        $scope.setSearch2 = function(){
            $scope.searchCurRole = false;
            if($rootScope['vuserrole'].queryObject.hasOwnProperty('roles:like')){
                delete $rootScope['vuserrole'].queryObject['roles:like'];
            }

            var field = 'roles:notlike';
            if($scope.searchNoCurRole){
                $rootScope['vuserrole'].queryObject[field] = $scope.searchCurRoleId;
            }else{
                if($rootScope['vuserrole'].queryObject.hasOwnProperty(field)){
                    delete $rootScope['vuserrole'].queryObject[field];
                }
            }
        };

        // 添加用户和角色关联信息
        $scope.addUserRole = function(uid,rid,callback,onerror){
            var url = getServerBaseUrl()+'/role/addUserRole/'+uid+'/'+rid;
            $.ajax({
                url : url,
                type : 'POST',
                data : null,
                timeout : 35000,
                dataType : 'json',
                success : callback,
                error : onerror
            });
        };

        // 删除用户和角色关联信息
        $scope.delUserRole = function(uid,rid,callback,onerror){
            var url = getServerBaseUrl()+'/role/delUserRole/'+uid+'/'+rid;
            $.ajax({
                url : url,
                type : 'DELETE',
                data : null,
                timeout : 35000,
                dataType : 'json',
                success : callback,
                error : onerror
            });
        };

        // 设置用户和角色关联信息
        $scope.setUserRole = function(r){
            if($rootScope.isOptioning){
                r.selected = !r.selected;
                $.messager.alert("系统提示","系统处理中，请稍等！");
                return false;
            }
            var uid = r.userid;
            var rid = $scope.searchCurRoleId;
            $rootScope.isOptioning = true;

            if(r.selected){
                $scope.addUserRole(uid,rid,function(){
                    $rootScope.$apply(function() {
                        $rootScope.isOptioning = false;
                        if(r.roleNames==null || r.roleNames==''){
                            r.roleNames=$scope.searchCurRoleName;
                        }else{
                            r.roleNames+=','+$scope.searchCurRoleName;
                        }

                        if(r.roles==null || r.roles==''){
                            r.roles=$scope.searchCurRoleId;
                        }else{
                            r.roles+=','+$scope.searchCurRoleId;
                        }
                    });
                },function(){
                    $rootScope.$apply(function() {
                        r.selected = !r.selected;
                        $rootScope.isOptioning = false;
                    });
                });
            }else{
                $scope.delUserRole(uid,rid,function(){
                    var rs = r.roles.split(',');
                    var roleIndex = -1;
                    for(var i=0;i<rs.length;i++){
                        if(rs[i]==$scope.searchCurRoleId){
                            roleIndex = i;
                            break;
                        }
                    }

                    var rids = '';
                    var ridns = '';

                    if(roleIndex!=-1){
                        var rns = r.roleNames.split(',');
                        for(var i=0;i<rs.length;i++){
                            if(i==roleIndex) continue;

                            rids += rs[i]+',';
                            ridns += rns[i]+',';
                        }
                        rids = rids.substring(0,rids.length-1);
                        ridns = ridns.substring(0,ridns.length-1);
                    }

                    $rootScope.$apply(function() {
                        $rootScope.isOptioning = false;
                        r.roleNames = ridns;
                        r.roles = rids;
                    });
                },function(){
                    $rootScope.$apply(function() {
                        r.selected = !r.selected;
                        $rootScope.isOptioning = false;
                    });
                });
            }
        }

	}]);
});