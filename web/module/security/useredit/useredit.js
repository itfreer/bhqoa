define(function(require) {

	require(['plupload','plupload.queue','plupload.i18n'], function () { 
        
    });

	require('ui-upload');
    require('as-files');
	require('ui-edit');

	var app = require('app');
	app.useModule('ui.upload');
    app.useModule('ui.as.files');
		app.controller('usereditCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {

    	/**
    	 * 用户申请提交保存
    	 * @param {Object} fName
    	 * @param {Object} row
    	 * @param {Object} callback
    	 * @param {Object} onerror
    	 */
    	$scope.subSave = function(fName,row,callback,onerror){
 			if(row!=null){
 				row.state = "2";
 			};
 			$rootScope.save(fName,row,callback,onerror);
    	};
    	
    	/**
    	 * 签批通过
    	 */
    	$scope.passSave = function(fName,row,callback,onerror){
 			row.state = "3";
 			$rootScope.save(fName,row,callback,onerror);
    	};
    	
    	/**
    	 * 签批不通过
    	 */
    	$scope.noPassSave = function(fName,row,callback,onerror){
 			row.state = "4";
 			$rootScope.save(fName,row,callback,onerror);
    	};
    
	}]);
});