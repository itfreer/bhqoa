define(function (require) {
	//require('security');
	//app.useModule('security');

    var app = require('app')
    .controller('userCtrl', ['$scope','$rootScope', function($scope,$rootScope){

    	// 初始化用户权限表单
    	$rootScope.formInit('userauthentication', {keyField:'userid', orderObject:{userid:0}});

    	// 政区树选择查询
    	$rootScope.userOrgSearch = function(node){
    		$rootScope['userinfo'].queryObject.organizationId = node.id;
    		$rootScope.loadData('userinfo');
    	};

    	// 用户登录授权
		$scope.userAuthentication = function() {
			if($rootScope.isOptioning){
				$.messager.alert("系统提示","系统处理中，请稍等！");
				return false;
			}
			
			if($rootScope['userinfo'].sRows.length!=1){
				$.messager.alert("系统提示","请选择一条要授权的数据！");
				return false;
			}

			var row = $rootScope['userinfo'].sRows[0];
			row.password = '******';
			$rootScope.editRowByLocal('userauthentication',row);
			if($rootScope['userauthentication'].editRow.accounts==null
				|| $rootScope['userauthentication'].editRow.accounts==''){
				$rootScope['userauthentication'].editRow.password='123456';
				$rootScope['userauthentication'].editRow[$rootScope.isNew]=true;
			}
			return true;
	    };

	    // 登录授权
	    $scope.saveUserAuthentication = function(fName,row,callback,onerror){
	    	var uRow = $rootScope['userinfo'].sRows[0];
	    	$rootScope.save(fName,row,function(fn,data){
	    		$rootScope.$apply(function() {
    				uRow.accounts = data.accounts;
    				uRow.valid = data.valid;
    			});
	    		if(callback) callback(fn,data);
	    	},onerror);
	    };

	}]);
});