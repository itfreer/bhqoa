define(function (require) {
	require(['plupload','plupload.queue','plupload.i18n'], function () { 
        
    });
	require('ui-upload');
    require('as-files');
	require('ui-edit');
	
    var app = require('app');
    app.useModule('ui.upload');
    app.useModule('ui.as.files');
    
    app.controller('serviceCtrl', ['$scope','$rootScope','$app', function($scope,$rootScope,$app){

        $scope.sso={};

        $scope.regService = function(){
            var e = $rootScope.registerservice.editRow;

            // 先获取tgt
            $scope.getTgt($scope.sso.username,$scope.sso.password,function(tgt){
                if(e.ssoid && e.ssoid!=''){
                    $scope.exitService(e.ssoid,
                        function(){
                            $rootScope.closeWindow('edit2');
                        },
                        function(){
                            // 注册服务
                            $scope.addServices(tgt, e.url, function(id){
                                e.ssoid = id;
                                // 更新数据
                                $rootScope.save('registerservice', e, $rootScope.saveCloseWindow);
                            });
                        }
                    );
                }else{
                    // 注册服务
                    $scope.addServices(tgt, e.url, function(id){
                        e.ssoid = id;
                        // 更新数据
                        $rootScope.save('registerservice', e, $rootScope.saveCloseWindow);
                    });
                }
            });
        };

        // 获取tgt
        $scope.getTgt = function(username,password,onSuccess) {
            $.ajax({
                url : getServerBaseUrl()+'/v1/tickets?username='+username+'&password='+password,
                type : 'POST',
                data : null,
                timeout : 35000,
                dataType : 'json',
                success : function(data, textStatus, xOptions){
                    if(onSuccess) onSuccess(data.ok);
                },
                error : function(xhr, status, data){
                    $.messager.alert("系统提示","安全认证失败！");
                }
            });
        };

        // 服务是否存在
        $scope.exitService = function(sid,onSuccess,onError) {
            $.ajax({
                url : getServerBaseUrl()+'/v1/exitService/'+sid,
                type : 'POST',
                data : null,
                timeout : 35000,
                dataType : 'json',
                success : function(data, textStatus, xOptions){
                    if(onSuccess) onSuccess();
                },
                error : function(xhr, status, data){
                    if(onError) onError();
                }
            });
        };

        // 服务注册
        $scope.addServices = function(tgt,sname,onSuccess) {
            $.ajax({
                url : getServerBaseUrl()+'/v1/addServices/'+tgt+'?serviceId='+sname+'&name='+sname+'&description='+sname+'&enabled=true&ssoEnabled=true',
                type : 'POST',
                data : null,
                timeout : 35000,
                dataType : 'json',
                success : function(data, textStatus, xOptions){
                    if(onSuccess) onSuccess(data.ok);
                },
                error : function(xhr, status, data){
                    $.messager.alert("系统提示","服务注册失败！");
                }
            });
        };

        $scope.createifprovide = function(r){
            var guid = $rootScope.getGUID();
            r.ifprovide = guid;
        }

	}]);
});