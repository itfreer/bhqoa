define(function (require) {
    var app = require('app');
    app.controller('jurisdictionCtrl', ['$scope','$rootScope','$app', function($scope,$rootScope,$app){

    	$scope.showRegisterService = function(fn,r){
    		if(r==null){
    			return;
    		}
    		if($rootScope[fn].treeNavs.length>1){
				r.registerService = $rootScope[fn].treeNavs[$rootScope[fn].treeNavs.length-1].d.registerService;
    		}
    		return r[$rootScope.isNew]==true && $rootScope[fn].treeNavs.length==1;
    	};

	}]);
});