define(function(require) {

	var app = require('app')
		.controller('newsCtrl', ['$scope', '$rootScope', function($scope, $rootScope) {

		/**
		 * 加载新闻详情
		 */
		$scope.InitNwesData = function(){
			var guid = $scope.getQueryString("key");
			
			var url = getServerBaseUrl()+'/cms_document/getEntity'; 

			$rootScope.getEntity(url,guid,function(dataA){
					if(dataA!=null){
						
						$scope.$apply(function(){
							
							
							$("#news").html(dataA.text)
						});
					}else{
						console.log("元数据没找到！");
					}
				});
		};
		
		/**
		 * 加载新闻列表
		 */
		$scope.InitNewsLiist = function(){
			
			var type = $scope.getQueryString("type");
			
			if(type=="notice"){
				
				$rootScope.formInit('cms_document', {
            	ssn:'cms_document',
				esn:'cms_document',
		        keyField:'id',
		        randomFields:['id'],
		        queryObject:{column:'NOTICE'},
		        pageSize:5,
		        orderObject:{id:0}
		    }); 
		    $rootScope.loadData('cms_document');
				
			}else if(type=="news"){
				$rootScope.formInit('cms_document', {
            	ssn:'cms_document',
				esn:'cms_document',
		        keyField:'id',
		        randomFields:['id'],
		        queryObject:{column:'NEWS'},
		        pageSize:5,
		        orderObject:{id:0}
		    }); 
		    $rootScope.loadData('cms_document');
			}

		};
		
		
		/**
		 * 加载下载列表
		 */
		$scope.InitDowloadList = function(){
			$rootScope.formInit('cms_document', {
            	ssn:'cms_document',
				esn:'cms_document',
		        keyField:'id',
		        randomFields:['id'],
		        queryObject:{column:'DOWNLOAD'},
		        pageSize:5,
		        orderObject:{id:0}
		    }); 
		    $rootScope.loadData('cms_document');
		};
		
		
		/**
    	 * 打开新闻详情
    	 * @param {Object} r
    	 */
    	$scope.openNewsShow = function(r){
			var url = "news.html";
			url=url+"?key="+encodeURIComponent(r.id);
			var news = r.text;
			$("#news").html(news);
			
			window.open(url, "");
		};
		
		$scope.download = function(r){
			
			var downloadUrl = getServerBaseUrl()+"/file/plupload/download?bucket="+r.file.split(":")[0]
						+"&key="+r.file.split(":")[1]+"&name="+r.file.split(":")[2];
			
		 	window.location.assign(downloadUrl);	
		};
		

		}]);
});