(function(window, undefined){

	// 获取应用中的静态资源根路径,GIS中应用
	getBasePath = function(){
		return "http://localhost/web-base";
	};

	// 服务根路径
	getServerUrl = function(){
		// return "http://47.95.220.20:808/bhqoa";
        return "http://localhost:9090/bhqoa";
	};

	// 服务根路径
	getServerBaseUrl = function(){
		return getServerUrl()+"/rest";
	};
	
	//
	getRootPath = function(){
		return "http://localhost/bhqoa/bhqoa/web";
	};
	
	
	

	// 以下为单点登录配置
	getServiceName = function(){
		return "http://localhost:8017/";
	};
	getLoginUrl = function(){
		return "http://localhost:9090/bhqoa/sso.html";
	};
	getMainUrl = function(){
		return "sy.html";
	};
	// 结束

}(window));