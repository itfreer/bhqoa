define(function (require) {
	require(['echarts', 'echarts.china'], function (es) {
	    echartsObject.hasInitFinsh = true;
        echartsObject.es = es;
        if(echartsObject.initFinshEvent){
            echartsObject.initFinshEvent.forEach(function(e){
            	e(es);
            });
        }
	});

	require('ui.echarts');

    var app = require('app');
    app.useModule('ui.echarts');

    app.controller('reportCtrl', ['$scope','$rootScope', function($scope,$rootScope){

	}]);
});