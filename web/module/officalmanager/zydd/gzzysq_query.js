define(function (require) {
	//require('security');
	//app.useModule('security');

    var app = require('app')
    .filter("todoReadFilter", ["$sce", function ($sce) {
	    return function (isread) {
	    	if(isread == "0" || isread==null || isread==''){
	    		return "font-weight: bold";
	    	}else {
	    		return "";
	    	}
	    };
	}]).
	filter("toDoWorkJB", ["$sce", function ($sce) {
	    return function (input, waitMinutes) {
	    	if(waitMinutes==null || waitMinutes=='undefined'){
	    		return ;
	    	}
	    	if(waitMinutes>0){
				return ;
	    	}else{
	    		return "background: #FF1313;";
	    	}
	    };
	}])
    .controller('zysqCtrl', ['$scope','$rootScope', function($scope,$rootScope){
    	/**
    	 * 启动新的流程示例
    	 * @return {[type]} [description]
    	 */
    	$scope.startNewBpm=function(url,bpmkey,formname){
    		var openurl=url+"?bk="+encodeURIComponent(bpmkey)
    			+"&fn="+encodeURIComponent(formname);
    		window.open(openurl);
    	};

    	/**
    	 * 查看项目过程
    	 * @param  {[type]} url      项目地址
    	 * @param  {[type]} bpmkey   流程类型键
    	 * @param  {[type]} formname 表单id
    	 * @param  {[type]} viewid   uiId
    	 * @param  {[type]} sexeid   流程项目主键
    	 * @return {[type]}          描述
    	 */
    	$scope.toQueryWork=function(url,bpmkey,formname,viewid,guid){
    		var openurl=url+"?bk="+encodeURIComponent(bpmkey)
    			+"&fn="+encodeURIComponent(formname)
    			+"&vi="+encodeURIComponent(viewid)
    			+"&dk="+encodeURIComponent(guid);
    		window.open(openurl);
    	};

    	/**
		 * 获取系统查询参数
		 */
		$scope.getQueryString=function(key){
	        var reg = new RegExp("(^|&)"+key+"=([^&]*)(&|$)");
	        var result = window.location.search.substr(1).match(reg);
	        return result?decodeURIComponent(result[2]):null;
	    };
	}]);
});