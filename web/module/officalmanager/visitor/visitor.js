define(function (require) {
	require(['plupload','plupload.queue','plupload.i18n'], function () { 
	    
	});

	require('ui-upload');
	require('as-files');

    var app = require('app');
    app.useModule('ui.upload');
    app.useModule('ui.as.files');
    app.controller('visitorCtrl', ['$scope','$rootScope', function($scope,$rootScope){
		//挂接预约管理
		$scope.chooseYYGL = function(editRow,rEditRow,closeId){
			if(rEditRow==null){
				$.messager.alert("系统提示","请先选择记录！");
			}else{
				editRow.yyid=rEditRow.id;
				
				editRow.lfdw=rEditRow.lfdw;
				editRow.lfzt=rEditRow.lfzt;
				editRow.dfsj=rEditRow.dfsj;
				editRow.dfjssj=rEditRow.dfjssj;
				editRow.lfry=rEditRow.lfry;
				editRow.lfrs=rEditRow.lfrs;
				editRow.sfxyapzs=rEditRow.sfxyapzs;
				editRow.sfxyapzsName=rEditRow.sfxyapzsName;
				editRow.nxrs=rEditRow.nxrs;
				editRow.wxrs=rEditRow.wxrs;
				editRow.kcnr=rEditRow.kcnr;
				editRow.tel=rEditRow.tel;
				editRow.cjrmc=rEditRow.cjrmc;
				editRow.jdbmmc=rEditRow.jdbmmc;
				editRow.jdbmmcName=rEditRow.jdbmmcName;
				editRow.jdfzrmc=rEditRow.jdfzrmc;
				editRow.cyr=rEditRow.cyr;
				
				$scope.closeWindow(closeId);
			}

		};
		
		$rootScope.$watch('gzvisitorreception.datas', function() {
			if($rootScope['gzvisitorreception']){
				var datas=$rootScope['gzvisitorreception'].datas;
				var lfrsTotal=0;
				if(datas.length>0){
					for(var i=0;i<datas.length;i++){
						lfrsTotal+=Number(datas[i].lfrs);
					}
				}
				$rootScope['gzvisitorreception'].lfrsTotal=lfrsTotal;
			}
		});
		
	}]);
});