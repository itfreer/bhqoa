define(function (require) {
	require(['plupload','plupload.queue','plupload.i18n'], function () { 
	    
	});

	require('ui-upload');
	require('as-files');

    var app = require('app');
    app.useModule('ui.upload');
    app.useModule('ui.as.files');
	app.filter("asChBool", ['$rootScope', function ($rootScope) {
   		 return function (input) {
   		 	if(input==true || input=='true' || input=='TRUE' || input=='1'  || input==1){
   		 		return '是';
   		 	}else{
   		 		return '否';
   		 	}
   		 };
	}]);
	
    app.controller('fileCtrl', ['$scope','$rootScope', function($scope,$rootScope){
		$scope.confirmReceive = function(fn,editRow){
			if(editRow == null){
				$.messager.alert('系统提示', '请选择一条记录！');
				return;
			}
			
			if(editRow.sfqr!="1" && editRow.sfqr!=1){
				if($rootScope.isOptioning){
					$.messager.alert("系统提示","系统处理中，请稍等！");
					return false;
				}
				if(editRow != null){
					
					$rootScope.editRow(fn, editRow);
					$scope.openWindow('edit');
					
					editRow.sfqr="1";
					//测试代码
					//editRow.ydsj=new Date();
					editRow.ydsj=$rootScope.formatTime(new Date())
					$rootScope.save(fn, editRow,function(){
						$rootScope.loadData(fn);
					});
					
				}else{
					$.messager.alert('系统提示', '请选择一条记录！');
					return;
				}
			}else{
				$rootScope.editRow(fn, editRow);
				$scope.openWindow('edit');
			}
    	};
    	
    	/**
    	 * 查看收文项目
    	 */
//  	$scope.readReceive=function(fn,editRow){
//  		if(editRow == null){
//				$.messager.alert('系统提示', '请选择一条记录！');
//				return;
//			}
//			
//			if(editRow.sfqr!="1" && editRow.sfqr!=1){
//				if($rootScope.isOptioning){
//					$.messager.alert("系统提示","系统处理中，请稍等！");
//					return false;
//				}
//				if(editRow != null){
//					
//					$rootScope.editRow(fn, editRow);
//					
//					//$scope.openWindow('edit');
//					
//					editRow.sfqr="1";
//					//测试代码
//					//editRow.ydsj=new Date();
//					editRow.ydsj=$rootScope.formatTime(new Date())
//					$rootScope.save(fn, editRow,function(){
//						$rootScope.loadData(fn);
//						$scope.toQueryWork('gzpublishfile.html',editRow.bpmkey,
//						'gzpublishfile','gzpublishfile',editRow.fwid);
//					});
//					
//				}else{
//					$.messager.alert('系统提示', '请选择一条记录！');
//					return;
//				}
//			}else{
//				//$rootScope.editRow(fn, editRow);
//				$scope.toQueryWork('gzpublishfile.html',editRow.bpmkey,'gzpublishfile','gzpublishfile',editRow.fwid);
//				//$scope.openWindow('edit');
//			}
//  	};
    	
    	/**
    	 * 处理收文负责人问题
    	 */
    	$scope.onchangeSwFzr=function(){
    		setTimeout(function(){
    			$rootScope.gzpublishfile.editRow.zyfzrname=$rootScope.schedulefzr.selectedRow.userName;
    		},50);
    	};
    	
    	/**
    	 * 紧急等级发生变化时，处理逻辑
    	 */
    	$rootScope.onchangeJjdj=function(recod){
    		/*setTimeout(function(){*/
    		$rootScope.gzpublishfile.editRow.jjdjName=recod.display;
    		/*},50);*/
    	};
    	
    	$scope.loadReceiveData = function(fn,query,type){
    		var queryObject={'sfqr':query};
    		$rootScope[fn].queryObject=queryObject;
    		$rootScope.loadData(fn);
    		$rootScope.setTab('receivemenu', type);
    	};
    	
    	/**
    	 * 启动新的流程示例
    	 * @return {[type]} [description]
    	 */
    	$scope.startNewBpm=function(url,bpmkey,formname){
    		var openurl=url+"?bk="+encodeURIComponent(bpmkey)
    			+"&fn="+encodeURIComponent(formname);
    		window.open(openurl);
    	};

    	/**
    	 * 查看项目过程
    	 * @param  {[type]} url      项目地址
    	 * @param  {[type]} bpmkey   流程类型键
    	 * @param  {[type]} formname 表单id
    	 * @param  {[type]} viewid   uiId
    	 * @param  {[type]} sexeid   流程项目主键
    	 * @return {[type]}          描述
    	 */
    	$scope.toQueryWork=function(url,bpmkey,formname,viewid,guid){
    		var openurl=url+"?bk="+encodeURIComponent(bpmkey)
    			+"&fn="+encodeURIComponent(formname)
    			+"&vi="+encodeURIComponent(viewid)
    			+"&dk="+encodeURIComponent(guid);
    		window.open(openurl);
    	};

    	/**
		 * 获取系统查询参数
		 */
		$scope.getQueryString=function(key){
	        var reg = new RegExp("(^|&)"+key+"=([^&]*)(&|$)");
	        var result = window.location.search.substr(1).match(reg);
	        return result?decodeURIComponent(result[2]):null;
	    };
	    
	    /**
	     * 页面初始化时，加载数据
	     */
	    $scope.initReceiveLoadData=function(){
	    	
	    	var tempForm="gzreceiveview";
	    	var temp=$scope.getQueryString("key");
	    	if(temp!=null){
	    		var baseUrl = getServerBaseUrl()+'/'+$rootScope[tempForm].ssn+'/getEntity';
				//添加用户指定的path
				if($rootScope[tempForm].rootpath){
					baseUrl = $rootScope[tempForm].rootpath+'/'+$rootScope[tempForm].ssn+'/getEntity';
				}
				$rootScope.getEntity(baseUrl,temp,function(data){
					if(data!=null){
						$scope.confirmReceive(tempForm,data);
					}
					
					$rootScope.loadData('gzreceiveview');
				},function(error){
					$rootScope.loadData('gzreceiveview');
				});
	    	}else{
	    		$rootScope.loadData('gzreceiveview');
	    	}
	    };
	    
	    
	    //打印外籍人员入区申请表
		$scope.printFile = function(fn,printType,row){
			
			var editRow={};
			 editRow=$.extend(true, editRow, row);
			if($rootScope.isOptioning){
				$.messager.alert("系统提示","系统处理中，请稍等！");
				return false;
			}
			/*var lfsj=new Date(editRow.dlfsj);
			var bmspsj=new Date(editRow.dbmspsj);
			var dwspsj=new Date(editRow.ddwspsj);
			var cbrsj=new Date(editRow.cbrsj);
			var slsj=new Date(editRow.cbrsj);
			if(slsj)editRow.slsj=slsj.getFullYear()+"年"+(slsj.getMonth()+1)+"月"+slsj.getDate()+"日";
			if(lfsj)editRow.dlfsj=lfsj.getFullYear()+"年"+(lfsj.getMonth()+1)+"月"+lfsj.getDate()+"日";
			if(cbrsj)editRow.cbrsj=cbrsj.getFullYear()+"年"+(cbrsj.getMonth()+1)+"月"+cbrsj.getDate()+"日";
			if(bmspsj)editRow.dbmspsj=bmspsj.getFullYear()+"年"+(bmspsj.getMonth()+1)+"月"+bmspsj.getDate()+"日";
			if(dwspsj)editRow.ddwspsj=dwspsj.getFullYear()+"年"+(dwspsj.getMonth()+1)+"月"+dwspsj.getDate()+"日";*/
			if(editRow.swsj){
				var swsj=new Date(editRow.swsj);
				if(swsj){
					editRow.lwsjn=swsj.getFullYear();
					editRow.lwsjy=swsj.getMonth()+1;
					editRow.lwsjr=swsj.getDate();
				}
			}
			try{
				editRow.usertask2=$rootScope.gzpublishfile.process.usertask2.history;
				if(editRow.usertask2[0].opttime){
					var date=new Date(editRow.usertask2[0].opttime);
					editRow.usertask2[0].year=date.getFullYear()+"";
					var month=date.getMonth()+1;
					if(month<10){
						editRow.usertask2[0].month="0"+month;
					}else{
						editRow.usertask2[0].month=month+"";
					}
					var day=date.getDate();
					if(day<10){
						editRow.usertask2[0].day="0"+day;
					}else{
						editRow.usertask2[0].day=day+"";
					}
				}
			}catch(e){
				
			}
			
			try{
				editRow.usertask3=$rootScope.gzpublishfile.process.usertask3.history;
				if(editRow.usertask3[0].opttime){
					var date=new Date(editRow.usertask3[0].opttime);
					editRow.usertask3[0].year=date.getFullYear()+"";
					var month=date.getMonth()+1;
					if(month<10){
						editRow.usertask3[0].month="0"+month;
					}else{
						editRow.usertask3[0].month=month+"";
					}
					var day=date.getDate();
					if(day<10){
						editRow.usertask3[0].day="0"+day;
					}else{
						editRow.usertask3[0].day=day+"";
					}
				}
			}catch(e){
				
			}
			$scope.print(fn,'fwcf.html',printType,editRow,'贵州茂兰保护区管理局文书处理笺.pdf',null,null);
		};
		
		$scope.getYj = function(content){
			$scope.gzpublishfile.process.usertask3.editRow.optcontent=content;
		}

		$scope.deleteJzbg = function(data){
			//先判断是否已读
			var isRead = false;
			var reader = '';
			for(var i=0;i<data.gzreceivefile.length;i++){
				var row = data.gzreceivefile[i];
				if(row.sfqr=="1" || row.sfqr==1){//已读
					isRead=true;
					reader += row.jsrmc + ',';
				}
			}
			if(isRead){
				//已读，不可删除
				$.messager.alert("系统提示","该发文已被阅读,无法删除，阅读人："+reader.substring(0,reader.length-1));
			}else{
				$rootScope.delEntity('gzpublishfile',data);
			}
		}
	    
	    
	}]);
});