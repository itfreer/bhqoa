define(function (require) {
	require(['plupload','plupload.queue','plupload.i18n'], function () { 
	    
	});

	require('ui-upload');
	require('as-files');

    var app = require('app');
    app.useModule('ui.upload');
    app.useModule('ui.as.files');
	app.filter("asChBool", ['$rootScope', function ($rootScope) {
   		 return function (input) {
   		 	if(input==true || input=='true' || input=='TRUE' || input=='1'  || input==1){
   		 		return '是';
   		 	}else{
   		 		return '否';
   		 	}
   		 };
	}]);
    app.controller('yzglCtrl', ['$scope','$rootScope', function($scope,$rootScope){
	
    	
    	/**
    	 * 启动新的流程示例
    	 * @return {[type]} [description]
    	 */
    	$scope.startNewBpm=function(url,bpmkey,formname){
    		var openurl=url+"?bk="+encodeURIComponent(bpmkey)
    			+"&fn="+encodeURIComponent(formname);
    		window.open(openurl);
    	};

    	/**
    	 * 查看项目过程
    	 * @param  {[type]} url      项目地址
    	 * @param  {[type]} bpmkey   流程类型键
    	 * @param  {[type]} formname 表单id
    	 * @param  {[type]} viewid   uiId
    	 * @param  {[type]} sexeid   流程项目主键
    	 * @return {[type]}          描述
    	 */
    	$scope.toQueryWork=function(url,bpmkey,formname,viewid,guid){
    		var openurl=url+"?bk="+encodeURIComponent(bpmkey)
    			+"&fn="+encodeURIComponent(formname)
    			+"&vi="+encodeURIComponent(viewid)
    			+"&dk="+encodeURIComponent(guid);
    		window.open(openurl);
    	};

    	/**
		 * 获取系统查询参数
		 */
		$scope.getQueryString=function(key){
	        var reg = new RegExp("(^|&)"+key+"=([^&]*)(&|$)");
	        var result = window.location.search.substr(1).match(reg);
	        return result?decodeURIComponent(result[2]):null;
	    };
	}]);
});