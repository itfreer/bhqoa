define(function (require) {
	require(['plupload','plupload.queue','plupload.i18n'], function () { 
	    
	});

	require('ui-upload');
	require('as-files');

    var app = require('app');
    app.useModule('ui.upload');
    app.useModule('ui.as.files');
	app.filter("asImage", ["$sce", function ($sce) {
	    return function (fileInfo) {
            if(fileInfo==null || fileInfo=="") return null;
            fileInfoArr = fileInfo.split(":");
            if(fileInfoArr.length<3)return null;
            var bucket = fileInfoArr[0];
            var key = fileInfoArr[1];
            var name = fileInfoArr[2];
            var downloadUrl=getServerBaseUrl()+'/file/plupload/download';
            var _downloadUrl = downloadUrl+"?bucket="+bucket+"&key="+key+"&name="+encodeURIComponent(name);
            return _downloadUrl;
	    };
	}]);
    app.controller('xzxkblCtrl', ['$scope','$rootScope', function($scope,$rootScope){
			/**
    	 * 启动新的流程示例
    	 * @return {[type]} [description]
    	 */
    	$scope.startNewBpm=function(url,bpmkey,formname){
    		var openurl=url+"?bk="+encodeURIComponent(bpmkey)
    			+"&fn="+encodeURIComponent(formname);
    		window.open(openurl);
    	};

    	/**
    	 * 查看项目过程
    	 * @param  {[type]} url      项目地址
    	 * @param  {[type]} bpmkey   流程类型键
    	 * @param  {[type]} formname 表单id
    	 * @param  {[type]} viewid   uiId
    	 * @param  {[type]} sexeid   流程项目主键
    	 * @return {[type]}          描述
    	 */
    	$scope.toQueryWork=function(url,bpmkey,formname,viewid,guid){
    		var openurl=url+"?bk="+encodeURIComponent(bpmkey)
    			+"&fn="+encodeURIComponent(formname)
    			+"&vi="+encodeURIComponent(viewid)
    			+"&dk="+encodeURIComponent(guid);
    		window.open(openurl);
    	};

    	/**
		 * 获取系统查询参数
		 */
		$scope.getQueryString=function(key){
	        var reg = new RegExp("(^|&)"+key+"=([^&]*)(&|$)");
	        var result = window.location.search.substr(1).match(reg);
	        return result?decodeURIComponent(result[2]):null;
	    };

		$scope.saveRQXK=function(type,fn,editRow){
			editRow.wlxmlx=type;
			$scope.save(fn,editRow,$scope.closeWindow('edit'));
		};
		
		//打印外籍人员入区申请表
		$scope.printSQB = function(fn,printType,row){
			
			var editRow=$.extend(true, editRow, row);
			if($rootScope.isOptioning){
				$.messager.alert("系统提示","系统处理中，请稍等！");
				return false;
			}
			//时间修改为字符串
			var lfsj=new Date(editRow.dlfsj);
			var bmspsj=new Date(editRow.dbmspsj);
			var dwspsj=new Date(editRow.ddwspsj);
			var cbrsj=new Date(editRow.cbrsj);
			var slsj=new Date(editRow.cbrsj);
			if(slsj)editRow.slsj=slsj.getFullYear()+"年"+(slsj.getMonth()+1)+"月"+slsj.getDate()+"日";
			if(lfsj)editRow.dlfsj=lfsj.getFullYear()+"年"+(lfsj.getMonth()+1)+"月"+lfsj.getDate()+"日";
			if(cbrsj)editRow.cbrsj=cbrsj.getFullYear()+"年"+(cbrsj.getMonth()+1)+"月"+cbrsj.getDate()+"日";
			if(bmspsj)editRow.dbmspsj=bmspsj.getFullYear()+"年"+(bmspsj.getMonth()+1)+"月"+bmspsj.getDate()+"日";
			if(dwspsj)editRow.ddwspsj=dwspsj.getFullYear()+"年"+(dwspsj.getMonth()+1)+"月"+dwspsj.getDate()+"日";
			
			$scope.print(fn,'wjryrqsqb.html',printType,editRow,'外籍人员入区申请表.pdf',null,null);
		};
		
		$scope.printXKPZ = function(fName,template,printType,row,filename){
			//时间修改为字符串
			var data=$.extend(true, data, row);
			var sqsj=new Date(data.xzxksqsj);
			var slsj=new Date(data.cbrsj);
			if(sqsj)data.sqsj=sqsj.getFullYear()+"年"+(sqsj.getMonth()+1)+"月"+sqsj.getDate()+"日";
			if(slsj)data.slsj=slsj.getFullYear()+"年"+(slsj.getMonth()+1)+"月"+slsj.getDate()+"日";
			
			$scope.print(fName,template,printType,data,filename,null,null);
		};
		
		$scope.printXKJDS = function(fName,template,printType,row,filename){
			//时间修改为字符串
			var data=$.extend(true, data, row);
			var sqsj=new Date(data.xzxksqsj);
			var slsj=new Date(data.ddwspsj);
			if(sqsj)data.sqsj=sqsj.getFullYear()+"年"+(sqsj.getMonth()+1)+"月"+sqsj.getDate()+"日";
			if(slsj)data.slsj=slsj.getFullYear()+"年"+(slsj.getMonth()+1)+"月"+slsj.getDate()+"日";
			
			$scope.print(fName,template,printType,data,filename,null,null);
		};
		
		$scope.printXKBJBG = function(fName,template,printType,row,filename){
			//时间修改为字符串
			var data=$.extend(true, data, row);
			var fwrq=new Date(data.ddwspsj);
			if(fwrq)data.fwrq=fwrq.getFullYear()+"年"+(fwrq.getMonth()+1)+"月"+fwrq.getDate()+"日";
			
			$scope.print(fName,template,printType,data,filename,null,null);
		};
		
		$scope.printXKSLSP = function(fName,template,printType,row,filename){
			var data=$.extend(true, data, row);
			$scope.print(fName,template,printType,data,filename,null,null);
		};
		
		$scope.printXKJDSP = function(fName,template,printType,row,filename){
			var data=$.extend(true, data, row);
			$scope.print(fName,template,printType,data,filename,null,null);
		};
		
		$scope.printSQCLQD = function(fName,template,printType,list,filename){
			var	row = {datas:list};
			var data=$.extend(true, data, row);
			$scope.print(fName,template,printType,data,filename,null,null);
		};
		
		$scope.downloadSQB = function(){
			window.open(getServiceName()+"/download/" + encodeURI("申请书模板.doc"));
		};	
		
		$scope.convertFilePath=function(fileInfo){
            if(fileInfo==null || fileInfo=="") return null;
            fileInfoArr = fileInfo.split(":");
            if(fileInfoArr.length<3)return null;
            var bucket = fileInfoArr[0];
            var key = fileInfoArr[1];
            var name = fileInfoArr[2];
            var downloadUrl=getServerBaseUrl()+'/file/plupload/download';
            var _downloadUrl = downloadUrl+"?bucket="+bucket+"&key="+key+"&name="+encodeURIComponent(name);
            return _downloadUrl;
		};
		
		/*$rootScope.uploadCbrQz=function(fileInfo){
			$rootScope.gzwjryrqxk.editRow.chrspqz=$rootScope.uploadQz($rootScope.gzwjryrqxk.editRow.test01);
			$rootScope.gzwjryrqxk.editRow.test01=null;
		};*/
		
		$scope.ConvertImagePath = function(fName,template,printType,row,filename){
			var data=$.extend(true, data, row);
			if(data.chrspqz!=null && data.chrspqz!=""){
				data.chrspqzimage=$scope.convertFilePath(data.chrspqz);
			}
			if(data.kyksqqz!=null && data.kyksqqz!=""){
				data.kyksqqzimage=$scope.convertFilePath(data.kyksqqz);
			}
			if(data.bgssqqz!=null && data.bgssqqz!=""){
				data.bgssqqzimage=$scope.convertFilePath(data.bgssqqz);
			}
			$scope.print(fName,template,printType,data,filename,null,null);
		};
		
		$scope.printSlspb=function(fName,template,printType,row,filename){
			var data=$.extend(true, data, row);
			if(data.cbrsj!=null){
				var cbrsjgs=new Date(data.cbrsj);
				if(cbrsjgs)data.cbrsjgs=cbrsjgs.getFullYear()+"年"+(cbrsjgs.getMonth()+1)+"月"+cbrsjgs.getDate()+"日";
			}
			
			if(data.dbmspsj!=null){
				var dbmspsjgs=new Date(data.dbmspsj);
				if(dbmspsjgs)data.dbmspsjgs=dbmspsjgs.getFullYear()+"年"+(dbmspsjgs.getMonth()+1)+"月"+dbmspsjgs.getDate()+"日";
			}
			if(data.ddwspsj!=null){
				var ddwspsjgs=new Date(data.ddwspsj);
				if(ddwspsjgs)data.ddwspsjgs=ddwspsjgs.getFullYear()+"年"+(ddwspsjgs.getMonth()+1)+"月"+ddwspsjgs.getDate()+"日";
			}
			
			$scope.ConvertImagePath(fName,template,printType,data,filename);
		};
		
	}]);
});