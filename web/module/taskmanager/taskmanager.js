define(function (require) {

    var app = require('app')
    .controller('taskmanagerCtrl', ['$scope','$rootScope', function($scope,$rootScope){

    	$scope.loadTaskData = function(fn,query,type){
    		$rootScope[fn].queryObject=query;
    		$rootScope.loadData(fn);
    		$rootScope.setTab('taskmenu', type);
    	};
    	
    	$scope.confirmTask = function(fn,editRow){
    		if($rootScope.isOptioning){
				$.messager.alert("系统提示","系统处理中，请稍等！");
				return false;
			}
			if(editRow != null){
				editRow.sfqr="1";
				editRow.zt="rwzt_yyd";
				editRow.qrsj=$rootScope.formatTime(new Date());
				$scope.changeRwzt(fn,editRow);
			}else{
				$.messager.alert('系统提示', '请选择一条记录！');
				return;
			}
    	};
    	
    	//修改任务状态为已阅读并保存
    	$scope.changeRwzt = function(fn,editRow){
			if($rootScope.isOptioning){
				$.messager.alert("系统提示","系统处理中，请稍等！");
				return false;
			}
			if(editRow != null){
				editRow.zt="rwzt_yyd";
	    		$rootScope.save(fn, editRow, function(){
					$scope.closeWindow('edit')
					var queryObject={'zt:!=':'rwzt_ywc'};
					$rootScope[fn].queryObject=queryObject;
					$rootScope.loadData(fn);
				});
			}else{
				$.messager.alert('系统提示', '请选择一条记录！');
				return;
			}
    	};
    	
    	//修改任务状态为已完成并保存
    	$scope.completeTask = function(fn,editRow){
    		if($rootScope.isOptioning){
				$.messager.alert("系统提示","系统处理中，请稍等！");
				return false;
			}
			if(editRow != null){
				$.messager.confirm("操作确认", "是否确认已完成?", function(r){
		            if(r){
		                editRow.zt="rwzt_ywc";
			    		editRow.wcsj=$rootScope.formatTime(new Date());
			    		$rootScope.save(fn, editRow, $scope.closeWindow('edit'));
						var queryObject={'zt':'rwzt_ywc'};
			    		$rootScope[fn].queryObject=queryObject;
						$rootScope.loadData(fn);
		            }
		        });
			}else{
				$.messager.alert('系统提示', '请选择一条记录！');
				return;
			}
    	};
    	
    	/**
    	 * 添加新增记录
    	 */
    	$scope.addScheduleRow=function(){
    		$rootScope.addRow('schedule');
    		$rootScope.schedule.editRow.bmmc=$rootScope.curUser.departmentId;
    		$rootScope.schedule.editRow.bmmcName= $rootScope.curUser.departmentName;
    		$rootScope.schedule.editRow.zt='rwzt_wyd';
    		$rootScope.schedule.editRow.ztName= '未阅读';
    		$rootScope.openWindow('edit');
    	};
    	
    	$scope.onchangeScheduleFzr=function(){
    		setTimeout(function(){
    			$rootScope.schedule.editRow.fzrmc=$rootScope.schedulefzr.selectedRow.userName;
    		},50);
    	};
    	
    	 /**
	     * 页面初始化时，加载数据
	     */
	    $scope.initTaskLoadData=function(){
	    	var tempForm="gztaskview";
	    	var temp=$scope.getQueryString("key");
	    	if(temp!=null){
	    		var baseUrl = getServerBaseUrl()+'/'+$rootScope[tempForm].ssn+'/getEntity';
				//添加用户指定的path
				if($rootScope[tempForm].rootpath){
					baseUrl = $rootScope[tempForm].rootpath+'/'+$rootScope[tempForm].ssn+'/getEntity';
				}
				$rootScope.getEntity(baseUrl,temp,function(data){
					//$scope.confirmReceive(tempForm,data);
					//
					if(data!=null){
						$rootScope.editRow('gztaskview',data);
						$rootScope.openWindow('edit');
					}
					$rootScope.loadData('gztaskview');
				},function(error){
					$rootScope.loadData('gztaskview');
				});
	    	}else{
	    		$rootScope.loadData('gztaskview');
	    	}
	    };
    	

	}]);
});