define(function (require) {
	require(['plupload','plupload.queue','plupload.i18n'], function () { 
	    
	});

	require('ui-upload');
	require('as-files');

    var app = require('app');
    app.useModule('ui.upload');
    app.useModule('ui.as.files');
	app.filter("asImage", ["$sce", function ($sce) {
	    return function (fileInfo) {
	        if(fileInfo==null || fileInfo=="") return null;
	        fileInfoArr = fileInfo.split(":");
	        if(fileInfoArr.length<3)return null;
	        var bucket = fileInfoArr[0];
	        var key = fileInfoArr[1];
	        var name = fileInfoArr[2];
	        var downloadUrl=getServerBaseUrl()+'/file/plupload/download';
	        var _downloadUrl = downloadUrl+"?bucket="+bucket+"&key="+key+"&name="+encodeURIComponent(name);
	        return _downloadUrl;
	    };
	}]);
    app.controller('projectsCtrl', ['$scope','$rootScope', function($scope,$rootScope){
					/**
    	 * 启动新的流程示例
    	 * @return {[type]} [description]
    	 */
    	$scope.startNewBpm=function(url,bpmkey,formname){
    		var openurl=url+"?bk="+encodeURIComponent(bpmkey)
    			+"&fn="+encodeURIComponent(formname);
    		window.open(openurl);
    	};

    	/**
    	 * 查看项目过程
    	 * @param  {[type]} url      项目地址
    	 * @param  {[type]} bpmkey   流程类型键
    	 * @param  {[type]} formname 表单id
    	 * @param  {[type]} viewid   uiId
    	 * @param  {[type]} sexeid   流程项目主键
    	 * @return {[type]}          描述
    	 */
    	$scope.toQueryWork=function(url,bpmkey,formname,viewid,guid){
    		var openurl=url+"?bk="+encodeURIComponent(bpmkey)
    			+"&fn="+encodeURIComponent(formname)
    			+"&vi="+encodeURIComponent(viewid)
    			+"&dk="+encodeURIComponent(guid);
    		window.open(openurl);
    	};
		
		$scope.toRyQueryWork=function(url,bpmkey,formname,viewid,guid){
			if(guid){
				var openurl=url+"?bk="+encodeURIComponent(bpmkey)
					+"&fn="+encodeURIComponent(formname)
					+"&vi="+encodeURIComponent(viewid)
					+"&dk="+encodeURIComponent(guid);
				window.open(openurl);
			}else{
				$.messager.alert("系统提示","请选择项目！");
			}
		};

    	/**
		 * 获取系统查询参数
		 */
		$scope.getQueryString=function(key){
	        var reg = new RegExp("(^|&)"+key+"=([^&]*)(&|$)");
	        var result = window.location.search.substr(1).match(reg);
	        return result?decodeURIComponent(result[2]):null;
	    };
	    
	    /**
	     * 修改科研项目负责人时，保存负责人名称
	     */
	    $scope.onChangeProjectFzr=function(){
	    	setTimeout(function(){
	    		$rootScope.gzprojectinfo.editRow.sxmjlname=$rootScope.fzr.selectedRow.userName;
	    	},50);
	    };
	    
	    /**
	     * 添加项目成果
	     * @param {Object} editRow
	     * @param {Object} childValues
	     */
	    $scope.addXmcg=function(editRow,childValues){
    		if(editRow[childValues]==null){
    			editRow[childValues]=[];
    		}
    		var row={};
    		row.id=$rootScope.getGUID();
    		row.xmbh=editRow.id;
    		
    		row.scsj=$rootScope.formatDate(new Date())
    		row.scryid=$rootScope.curUser.userid;
    		row.scrymc=$rootScope.curUser.userName;
    		editRow[childValues].push(row);
    	};
		
		$scope.onchangekyxmFzr=function(type){
			if(type==1){
				setTimeout(function(){
					$rootScope.gzprojectinfo.editRow.sqr=$rootScope.schedulefzr.selectedRow.userName;
				},50);
			}else if(type==2){
				setTimeout(function(){
					$rootScope.gzwlkyxmsqb.editRow.fzr=$rootScope.schedulefzr.selectedRow.userName;
				},50);
			}
		};
		
		$rootScope.$watch('gzwlkyxmsqb.datas', function() {
			if($rootScope['gzwlkyxmsqb']){
				var datas=$rootScope['gzwlkyxmsqb'].datas;
				var rsTotal=0;
				if(datas.length>0){
					for(var i=0;i<datas.length;i++){
						rsTotal+=Number(datas[i].renshu);
					}
				}
				$rootScope['gzwlkyxmsqb'].rsTotal=rsTotal;
			}
		});
	}]);
});