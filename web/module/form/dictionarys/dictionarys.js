define(function (require) {
	var app = require('app')
    .controller('dictionarysCtrl', ['$scope','$rootScope', function($scope,$rootScope){

    	$scope.dicName = null;
        
        // 字典选中
        $rootScope.dicSearch = function(node){
            $scope.setDic("dictionary",node.id);
        };

    	// 设置当前字典项
    	$scope.setDic = function(fn,name){
    		$rootScope[fn].defaultQueryObject.dicName=name;
            $rootScope[fn].queryObject={pid:'0'};
            $rootScope[fn].treeNavs=[{pid:'0',n:'所有字典项'}];
            $rootScope[fn].navLastPid='0';
    		$rootScope.loadData(fn);
    		$scope.dicName = name;
    	};

    	// 设置当前字典项的字典名称
    	$scope.setDicName = function(fn){
    		if($scope.dicName==null){
    			$.messager.alert("系统提示","请选择一条字典数据！");
				return false;
    		}

    		$rootScope[fn].editRow.dicName = $scope.dicName;
    		return true;
    	};

        $rootScope.dicFilter = function(fn,data){
            var fo = $rootScope[fn];
            var toItem = function(e){
                if(fo.valueField!='id'){
                    e.id = e[fo.valueField];
                }

                if(fo.textField!='text'){
                    e.text = e[fo.textField];
                }

                if(fo.showCode && e.text!=e.id){
                    e.text += "("+e.id+")";
                }

                if(fo.leafField==null || fo.leafField==''){
                    // 没有配置子节点字段的，默认全部为子节点
                    e.state = 'open';
                }else{
                    if(e[fo.leafField]==true){
                        e.state = 'open';
                    }else{
                        e.state = 'closed';
                    }
                }
            }

            var toItems = function(es){
                var ri = [];
                es.forEach(function(e){
                    if(e.info && e.info=='3'){
                        ri.push(e);
                    }
                });
                ri.forEach(function(ei){
                    es.remove(ei);
                });

                es.forEach(function(e){
                    toItem(e);
                    var children = e.children;
                    if(children && children.length>0){
                        toItems(children);
                    }
                });
            }
            
            toItems(data);
        };

        // 批量删除实体
        $scope.delEntitysEx = function(fn){
            if($rootScope.isOptioning){
                $.messager.alert("系统提示","系统处理中，请稍等！");
                return false;
            }
            var rows = $rootScope[fn].sRows;
            if(rows==null || rows.length<=0){
                $.messager.alert('系统提示', '请选择要删除的记录！');
                return;
            }
            $scope.delsEx(fn,rows);
        };

        // 内部删除
        $scope.delsEx = function(fn,rows){
            $.messager.confirm("操作确认", "确定删除选择记录?", function(r){
                if(r){
                    var fo = $rootScope[fn];
                    var values = "";
                    var dn = '';
                    rows.forEach(function(e){
                        dn = e["dicName"];
                        values += e[fo.keyField] + ",";
                    });

                    if(values!=""){
                        values = values.substr(0, values.length-1);
                        var url = getServerBaseUrl()+'/'+fo.sn+'/deletesex/'+dn+'/'+values;
                        $rootScope.isOptioning = true;
                        $rootScope.deletes(url,
                            function(data){
                                $rootScope.$apply(function() {
                                    rows.forEach(function(e){  
                                        fo.datas.remove(e);
                                    });
                                    if(fo.datas.length<=0){
                                        $rootScope.loadData(fn);
                                    }else{
                                        fo.rowsCount -= rows.length;
                                    }
                                });
                                $rootScope.isOptioning = false;
                            }, 
                            function(xhr, status, data){
                                var msg = '';
                                if(xhr.status<10){
                                    $.messager.alert('系统提示', '删除失败，请确认网络与认证服务是否正常');
                                }else{
                                    $.messager.alert('系统提示', '删除失败。系统返回信息 ['+data+']');
                                }
                                $rootScope.isOptioning = false;
                            }
                        );
                    }
                    fo.sRows = [];
                }
            });
        };

	}]);
});