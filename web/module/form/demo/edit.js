define(function (require) {
	require(['kindeditor', 'kindeditor.i18n'], function () {
	    editObject.hasInitFinsh = true;
        editObject.ke = KindEditor;
        if(editObject.initFinshEvent){
            editObject.initFinshEvent.forEach(function(e){
            	e(KindEditor);
            });
        }
	});

	require('ui-edit');
	var app = require('app');
	app.useModule('ui.edit');
    app.controller('editCtrl', ['$scope','$rootScope', function($scope,$rootScope){

	}]);
});