define(function (require) {
    var app = require('app');
    //配置路由
    app.config(function ($stateProvider, $urlRouterProvider) {
		
		$stateProvider.state('appupdate_net', {
		    views: {
		        'model': {
		            template: ''
		        },
		        'panel': {
		            templateUrl: 'module/app/update/appupdatenet.html',
					controllerUrl: 'module/app/update/appupdatenet.js',
					controller: 'appupdatenetCtrl'
		        }
		    }
		});

        $stateProvider.state('security_user', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/user/user.html',
                    controllerUrl: 'module/security/user/user',
                    controller: 'userCtrl'
                }
            }
        });

        $stateProvider.state('security_role', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/role/role.html',
                    controllerUrl: 'module/security/role/role',
                    controller: 'roleCtrl'
                }
            }
        });

        $stateProvider.state('security_useredit', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/useredit/useredit.html',
                    controllerUrl: 'module/security/useredit/useredit',
                    controller: 'usereditCtrl'
                }
            }
        });

        $stateProvider.state('security_usereditapp', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/useredit/usereditapp.html',
                    controllerUrl: 'module/security/useredit/useredit',
                    controller: 'usereditCtrl'
                }
            }
        });

        $stateProvider.state('security_roleentrust', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/role/roleentrust.html'
                }
            }
        });

        $stateProvider.state('security_organization', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/organize/organization.html'
                }
            }
        });

        $stateProvider.state('security_administrative', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/organize/administrative.html'
                }
            }
        });

        $stateProvider.state('security_department', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/organize/department.html',
                    controllerUrl: 'module/security/organize/department',
                    controller: 'depCtrl'
                }
            }
        });

        $stateProvider.state('security_operatingPost', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/organize/operatingPost.html',
                    controllerUrl: 'module/security/organize/operatingPost',
                    controller: 'operatingPostCtrl'
                }
            }
        });

        $stateProvider.state('security_position', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/organize/position.html',
                    controllerUrl: 'module/security/organize/position',
                    controller: 'positionCtrl'
                }
            }
        });

        $stateProvider.state('security_workingGroup', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/organize/workingGroup.html',
                    controllerUrl: 'module/security/organize/workingGroup',
                    controller: 'workingGroupCtrl'
                }
            }
        });

        $stateProvider.state('security_menujurisdiction', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/jurisdiction/menu.html',
                    controllerUrl: 'module/security/jurisdiction/jurisdiction',
                    controller: 'jurisdictionCtrl'
                }
            }
        });
        $stateProvider.state('security_datajurisdiction', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/jurisdiction/data.html',
                    controllerUrl: 'module/security/jurisdiction/jurisdiction',
                    controller: 'jurisdictionCtrl'
                }
            }
        });
        $stateProvider.state('security_mapextentjurisdiction', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/jurisdiction/mapextent.html',
                    controllerUrl: 'module/security/jurisdiction/jurisdiction',
                    controller: 'jurisdictionCtrl'
                }
            }
        });
        $stateProvider.state('security_maplayerjurisdiction', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/jurisdiction/maplayer.html',
                    controllerUrl: 'module/security/jurisdiction/jurisdiction',
                    controller: 'jurisdictionCtrl'
                }
            }
        });

        $stateProvider.state('security_service', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/jurisdiction/service.html',
                    controllerUrl: 'module/security/jurisdiction/service',
                    controller: 'serviceCtrl'
                }
            }
        });

        $stateProvider.state('signin_logging', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/log/signin_logging.html'
                }
            }
        });

        $stateProvider.state('visit_logging', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/log/visit_logging.html'
                }
            }
        });

        $stateProvider.state('cms_document', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/cms/content/document.html',
                    controllerUrl: 'module/cms/content/document',
                    controller: 'documentCtrl'
                }
            }
        });

        $stateProvider.state('cms_column', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/cms/content/column.html'
                }
            }
        });
		
		$stateProvider.state('gz_gzjjr', {
		    views: {
		        'model': {
		            template: ''
		        },
		        'panel': {
		            templateUrl: 'module/cms/content/gzgzjjr.html',
					controllerUrl: 'module/cms/content/reviews',
					controller: 'reviewsCtrl'
		        }
		    }
		});

        $stateProvider.state('cms_subject', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/cms/content/subject.html',
                    controllerUrl: 'module/cms/content/subject',
                    controller: 'subjectCtrl'
                }
            }
        });

        $stateProvider.state('cms_messageBoard', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/cms/message/messageBoard.html'
                }
            }
        });

        $stateProvider.state('cms_messageClass', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/cms/message/messageClass.html'
                }
            }
        });

        $stateProvider.state('cms_sensitive', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/cms/module/sensitive.html'
                }
            }
        });

        $stateProvider.state('cms_link', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/cms/module/link.html',
                    controllerUrl: 'module/cms/module/link',
                    controller: 'linkCtrl'
                }
            }
        });

        $stateProvider.state('cms_site', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/cms/module/site.html'
                }
            }
        });

        $stateProvider.state('knowledge', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/knowledge/content/knowledge.html'
                }
            }
        });

        $stateProvider.state('knowledge_interlocution', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/knowledge/content/interlocution.html'
                }
            }
        });

        $stateProvider.state('knowledge_site', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/knowledge/config/site.html'
                }
            }
        });

        $stateProvider.state('knowledge_label', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/knowledge/config/label.html'
                }
            }
        });

        $stateProvider.state('knowledge_catalog', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/knowledge/config/catalog.html'
                }
            }
        });

        $stateProvider.state('form_dic', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/form/dictionarys/dictionarys.html',
                    controllerUrl: 'module/form/dictionarys/dictionarys',
                    controller: 'dictionarysCtrl'
                }
            }
        });

        $stateProvider.state('form_metadata', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/form/metadata/metadata.html',
                    controllerUrl: 'module/form/metadata/metadata',
                    controller: 'metadataCtrl'
                }
            }
        });

        $stateProvider.state('form_metadata_name', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/form/metadata/metadataname.html'
                }
            }
        });

        $stateProvider.state('gis_layerconfig', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/gis/layerconfig/layerconfig.html'
                }
            }
        });

        $stateProvider.state('gis_analysisconfig', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/gis/analysisconfig/analysisconfig.html',
                    controllerUrl: 'module/gis/analysisconfig/analysisconfig',
                    controller: 'analysisConfigCtrl'
                }
            }
        });

        $stateProvider.state('analysis_gps', {
            views: {
                'model': {
                    templateUrl: 'module/gis/analysis/gps.html',
                    controllerUrl: 'module/gis/analysis/analysis',
                    controller: 'analysis_Ctrl'
                },
                'panel': {
                    template: ''
                }
            }
        });

        $stateProvider.state('analysis_cad', {
            views: {
                'model': {
                    templateUrl: 'module/gis/analysis/cad.html',
                    controllerUrl: 'module/gis/analysis/analysis',
                    controller: 'analysis_Ctrl'
                },
                'panel': {
                    template: ''
                }
            }
        });

        $stateProvider.state('analysis_select', {
            views: {
                'model': {
                    templateUrl: 'module/gis/analysis/select.html',
                    controllerUrl: 'module/gis/analysis/analysis',
                    controller: 'analysis_Ctrl'

                },
                'panel': {
                    template: ''
                }
            }
        });

        $stateProvider.state('analysis_draw', {
            views: {
                'model': {
                    templateUrl: 'module/gis/analysis/draw.html',
                    controllerUrl: 'module/gis/analysis/analysis',
                    controller: 'analysis_Ctrl'
                },
                'panel': {
                    template: ''
                }
            }
        });

        $stateProvider.state('analysis_shape', {
            views: {
                'model': {
                    templateUrl: 'module/gis/analysis/shape.html',
                    controllerUrl: 'module/gis/analysis/analysis',
                    controller: 'analysis_Ctrl'
                },
                'panel': {
                    template: ''
                }
            }
        });

        $stateProvider.state('gis_mapconfig', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/gis/mapconfig/mapconfig.html'
                }
            }
        });

        $stateProvider.state('arcgis_attribute', {
            views: {
                'model': {
                    templateUrl: 'module/arcgis/search/attribute.html'
                },
                'panel': {
                    template: ''
                }
            }
        });

        $stateProvider.state('arcgis_space', {
            views: {
                'model': {
                    templateUrl: 'module/arcgis/search/space.html'
                },
                'panel': {
                    template: ''
                }
            }
        });

        $stateProvider.state('arcgis_find', {
            views: {
                'model': {
                    templateUrl: 'module/arcgis/search/find.html'
                },
                'panel': {
                    template: ''
                }
            }
        });

        $stateProvider.state('arcgis_identify', {
            views: {
                'model': {
                    templateUrl: 'module/arcgis/search/identify.html'
                },
                'panel': {
                    template: ''
                }
            }
        });

        $stateProvider.state('arcgis_book', {
            views: {
                'model': {
                    templateUrl: 'module/arcgis/locator/book.html',
                    controllerUrl: 'module/arcgis/locator/book',
                    controller: 'bookCtrl'
                },
                'panel': {
                    template: ''
                }
            }
        });

        $stateProvider.state('arcgis_coordinate', {
            views: {
                'model': {
                    templateUrl: 'module/arcgis/locator/coordinate.html'
                },
                'panel': {
                    template: ''
                }
            }
        });

        $stateProvider.state('report_ldt', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/report/ldt.html',
                    controllerUrl: 'module/report/report',
                    controller: 'reportCtrl'
                }
            }
        });

        $stateProvider.state('report_map', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/report/map.html',
                    controllerUrl: 'module/report/report',
                    controller: 'reportCtrl'
                }
            }
        });

        $stateProvider.state('report_pt', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/report/pt.html',
                    controllerUrl: 'module/report/report',
                    controller: 'reportCtrl'
                }
            }
        });

        $stateProvider.state('report_ybp', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/report/ybp.html',
                    controllerUrl: 'module/report/report',
                    controller: 'reportCtrl'
                }
            }
        });

        $stateProvider.state('report_zxt', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/report/zxt.html',
                    controllerUrl: 'module/report/report',
                    controller: 'reportCtrl'
                }
            }
        });

        $stateProvider.state('report_zzt', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/report/zzt.html',
                    controllerUrl: 'module/report/report',
                    controller: 'reportCtrl'
                }
            }
        });

        $stateProvider.state('print_excel', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/print/excel.html',
                    controllerUrl: 'module/print/print',
                    controller: 'printCtrl'
                }
            }
        });

        $stateProvider.state('print_word', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/print/word.html',
                    controllerUrl: 'module/print/print',
                    controller: 'printCtrl'
                }
            }
        });

        $stateProvider.state('print_model', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/print/model.html',
                    controllerUrl: 'module/print/print',
                    controller: 'printCtrl'
                }
            }
        });

        $stateProvider.state('file_upordown', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/file/upordown.html',
                    controllerUrl: 'module/file/file',
                    controller: 'fileCtrl'
                }
            }
        });

        $stateProvider.state('form_demo_edit', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/form/demo/edit.html',
                    controllerUrl: 'module/form/demo/edit',
                    controller: 'editCtrl'
                }
            }
        });

        $stateProvider.state('bpm_definition', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/bpm/definition/definition.html',
                    controllerUrl: 'module/bpm/definition/definition',
                    controller: 'bpm_definitonCtrl'
                }
            }
        });

        $stateProvider.state('bpm_rcbg', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/bpm/rcbg/rcbg.html',
                    controllerUrl: 'module/bpm/rcbg/rcbg',
                    controller: 'bpm_rcbgCtrl'
                }
            }
        });

        $stateProvider.state('bpm_simpleDemo', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/bpm/demo/simpleDemo.html',
                    controllerUrl: 'module/bpm/demo/simpleDemo',
                    controller: 'bpm_simpleDemoCtrl'
                }
            }
        });

        /**
         * 日程
         */
        $stateProvider.state('schedule', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/taskmanager/schedule.html',
                    controllerUrl: 'module/taskmanager/taskmanager',
                    controller: 'taskmanagerCtrl'
                }
            }
        });

        /**
        * 指派给我的
        */
        $stateProvider.state('gztask', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/taskmanager/gztask.html',
                    controllerUrl: 'module/taskmanager/taskmanager',
                    controller: 'taskmanagerCtrl'
                }
            }
        });

        /**
         * 任务监测
         */
        $stateProvider.state('gztaskprogresstate', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/taskmanager/gztaskprogresstate.html'
                    /*  controllerUrl:  'module/bpm/demo/simpleDemo',
                      controller:     'bpm_simpleDemoCtrl'*/
                }
            }
        });


        /**
         * 发文管理
         */
        $stateProvider.state('gzpublishfile', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/gzpublishfile.html',
                    controllerUrl: 'module/officalmanager/file',
                    controller: 'fileCtrl'
                }
            }
        });

        /**
         * 发文管理查询
         */
        $stateProvider.state('gzpublishfile_query', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/gzpublishfile_query.html',
                    controllerUrl: 'module/officalmanager/file',
                    controller: 'fileCtrl'
                }
            }
        });

        /**
         * 用章管理
         */
        $stateProvider.state('gzyzgl', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/yzgl/gzyzgl.html',
                    controllerUrl: 'module/officalmanager/yzgl/yzgl',
                    controller: 'yzglCtrl'
                }
            }
        });

        /**
         * 用章管理查询
         */
        $stateProvider.state('gzyzgl_query', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/yzgl/gzyzgl_query.html',
                    controllerUrl: 'module/officalmanager/yzgl/yzgl',
                    controller: 'yzglCtrl'
                }
            }
        });

        /**
         * 收文管理
         */
        $stateProvider.state('gzreceivefile', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/gzreceiveview.html',
                    controllerUrl: 'module/officalmanager/file',
                    controller: 'fileCtrl'
                }
            }
        });

        /**
       * 访客预约管理
       */
        $stateProvider.state('gzvisitorreserve', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/visitor/gzvisitorreserve.html'
                    // controllerUrl: 'module/officalmanager/visitor/visitor',
                    // controller: 'visitorCtrl'
                }
            }
        });

        /**
         *访客接待管理
         */
        $stateProvider.state('gzvisitorreception', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/visitor/gzvisitorreception.html',
                    controllerUrl: 'module/officalmanager/visitor/visitor',
                    controller: 'visitorCtrl'
                }
            }
        });

        /**
         *访客统计（视图）
         */
        $stateProvider.state('gzvisitorstatisticsview', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/visitor/gzvisitorstatisticsview.html'
                    /*  controllerUrl:  'module/bpm/demo/simpleDemo',
                      controller:     'bpm_simpleDemoCtrl'*/
                }
            }
        });

        /**
       *护林员月度考核
       */
        $stateProvider.state('gzhlyydkh', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/appraisal/gzhlyydkh.html'
                    /*  controllerUrl:  'module/bpm/demo/simpleDemo',
                      controller:     'bpm_simpleDemoCtrl'*/
                }
            }
        });

        /**
        *护林员季度考核
        */
        $stateProvider.state('gzhlyjdkh', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/appraisal/gzhlyjdkh.html'
                    /*  controllerUrl:  'module/bpm/demo/simpleDemo',
                      controller:     'bpm_simpleDemoCtrl'*/
                }
            }
        });

        /**
        *护林员季度考核
        */
        $stateProvider.state('gzhlyndkh', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/appraisal/gzhlyndkh.html'
                    /*  controllerUrl:  'module/bpm/demo/simpleDemo',
                      controller:     'bpm_simpleDemoCtrl'*/
                }
            }
        });

        /**
         *项目立项管理（查询）
         */
        $stateProvider.state('gzprojectinfo_query', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/projects/research/gzprojectinfo_query.html',
                    controllerUrl: 'module/projects/projects',
                    controller: 'projectsCtrl'
                }
            }
        });

        /**
         *项目立项管理
         */
        $stateProvider.state('gzprojectinfo', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/projects/research/gzprojectinfo.html',
                    controllerUrl: 'module/projects/projects',
                    controller: 'projectsCtrl'
                }
            }
        });

        /**
        *项目档案
        */
        $stateProvider.state('gzprojectxmda', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/projects/research/gzprojectxmda.html',
                    controllerUrl: 'module/projects/projects',
                    controller: 'projectsCtrl'
                }
            }
        });

        /**
        *项目人员
        */
        $stateProvider.state('gzplanpersonnel', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/projects/research/gzplanpersonnel.html',
                    controllerUrl: 'module/projects/projects',
                    controller: 'projectsCtrl'
                }
            }
        });

        /**
        *项目成果
        */
        $stateProvider.state('gzprojectxmcg', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/projects/research/gzprojectxmcg.html',
                    controllerUrl: 'module/projects/projects',
                    controller: 'projectsCtrl'
                }
            }
        });

        /**
        *计划与进度
        */
        $stateProvider.state('gzprojectmonthly', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/projects/research/gzprojectmonthly.html'
                    /*  controllerUrl:  'module/bpm/demo/simpleDemo',
                      controller:     'bpm_simpleDemoCtrl'*/
                }
            }
        });

        /**
        *外来科研项目申请表(查询)
        */
        $stateProvider.state('gzwlkyxmsqb_query', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/projects/foreignresearch/gzwlkyxmsqb_query.html',
                    controllerUrl: 'module/projects/projects',
                    controller: 'projectsCtrl'
                }
            }
        });

        /**
         *外来科研项目申请表(查询)
         */
        $stateProvider.state('gzwlkyxmsqb', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/projects/foreignresearch/gzwlkyxmsqb.html',
                    controllerUrl: 'module/projects/projects',
                    controller: 'projectsCtrl'
                }
            }
        });

        /**
         *外来科研项目成果
         */
        $stateProvider.state('gzwlkyxmcg', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/projects/foreignresearch/gzwlkyxmcg.html',
                    controllerUrl: 'module/projects/projects',
                    controller: 'projectsCtrl'
                }
            }
        });

        /**
         *外来科研项目人随访人员
         */
        $stateProvider.state('gzwlkyxmlfry', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/projects/foreignresearch/gzwlkyxmlfry.html'
                    /*  controllerUrl:  'module/bpm/demo/simpleDemo',
                      controller:     'bpm_simpleDemoCtrl'*/
                }
            }
        });

        /**
         *林业统计案件
         */
        $stateProvider.state('gzlytjaj01', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/zygl/forestrylaw/gzlytjaj01.html'
                    /*  controllerUrl:  'module/bpm/demo/simpleDemo',
                      controller:     'bpm_simpleDemoCtrl'*/
                }
            }
        });

        /**
         *林业统计案件续
         */
        $stateProvider.state('gzlytjajx', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/zygl/forestrylaw/gzlytjajx.html'
                    /*  controllerUrl:  'module/bpm/demo/simpleDemo',
                      controller:     'bpm_simpleDemoCtrl'*/
                }
            }
        });

        /**
         *护林员工资管理
         */
        $stateProvider.state('gzhlygzgl', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/zygl/gyl/gzhlygzgl.html'
                    /*  controllerUrl:  'module/bpm/demo/simpleDemo',
                      controller:     'bpm_simpleDemoCtrl'*/
                }
            }
        });

        /**
        *管护费发放
        */
        $stateProvider.state('gzghfff', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/zygl/gyl/gzghfff.html'
                    /*  controllerUrl:  'module/bpm/demo/simpleDemo',
                      controller:     'bpm_simpleDemoCtrl'*/
                }
            }
        });

        /**
      *设备
      */
        $stateProvider.state('gzdevice', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/gdzc/sbgl/gzdevice.html',
                  	controllerUrl:  'module/gdzc/sbgl/device',
                  	controller:     'deviceCtrl'
                }
            }
        });

        /**
        *设备借还
        */
        $stateProvider.state('gzdeviceborrow', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/gdzc/sbgl/gzdeviceborrow.html',
                    controllerUrl:  'module/gdzc/sbgl/device',
                  	controller:     'deviceCtrl'
                }
            }
        });


        /**
        *设备采购管理
        */
        $stateProvider.state('gzdevicepurchase', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/gdzc/sbgl/gzdevicepurchase.html',
                    controllerUrl:  'module/gdzc/sbgl/device',
                  	controller:     'deviceCtrl'
                }
            }
        });

        /**
        *设备维修记录
        */
        $stateProvider.state('gzdevicemaintain', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/gdzc/sbgl/gzdevicemaintain.html',
                    controllerUrl: 'module/gdzc/sbgl/device',
                    controller: 'deviceCtrl'
                }
            }
        });

        /**
        *签到管理
        */
        $stateProvider.state('gzqdgl', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/rsgl/kqgl/gzqdgl.html',
                    controllerUrl: 'module/rsgl/kqgl/gzqjgl',
                    controller: 'qjglCtrl'
                }
            }
        });

        /**
        *请假管理(查询)
        */
        $stateProvider.state('gzqjgl_query', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/rsgl/kqgl/gzqjgl_query.html',
                    controllerUrl: 'module/rsgl/kqgl/gzqjgl',
                    controller: 'qjglCtrl'
                }
            }
        });

        /**
        *请假管理(流程)
        */
        $stateProvider.state('gzqjgl', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/rsgl/kqgl/gzqjgl.html',
                    controllerUrl: 'module/rsgl/kqgl/gzqjgl',
                    controller: 'qjglCtrl'
                }
            }
        });



        /**
        *出差管理(查询)
        */
        $stateProvider.state('gzccgl_query', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/rsgl/kqgl/gzccgl_query.html',
                    controllerUrl: 'module/rsgl/kqgl/gzqjgl',
                    controller: 'qjglCtrl'
                }
            }
        });

        /**
       *出差管理(流程)
       */
        $stateProvider.state('gzccgl', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/rsgl/kqgl/gzccgl.html',
                    controllerUrl: 'module/rsgl/kqgl/gzqjgl',
                    controller: 'qjglCtrl'
                }
            }
        });

        /**
         *考勤统计
         */
        $stateProvider.state('gzrskqtj', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/rsgl/kqgl/gzrskqtj.html',
                    controllerUrl: 'module/rsgl/kqgl/gzqjgl',
                    controller: 'qjglCtrl'
                }
            }
        });

        /**
         * 人事档案
         */
        $stateProvider.state('gzryda', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/security/organize/gzryda.html'/*,
                    controllerUrl:  'module/security/organize/gzryda',
                    controller:     'operatingPostCtrl'*/
                }
            }
        });

        /**
         *项目建设许可
         */
        $stateProvider.state('gzxmjsxk', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/xzxkbl/gzxmjsxk.html',
                    controllerUrl: 'module/officalmanager/xzxkbl/xzxkbl',
                    controller: 'xzxkblCtrl'
                }
            }
        });

        /**
         *车辆档案
         */
        $stateProvider.state('gzclda', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/gdzc/clmanager/gzclda.html',
                    controllerUrl: 'module/gdzc/clmanager/clgl',
                    controller: 'clglCtrl'
                }
            }
        });

        /**
        *车辆维修
        */
        $stateProvider.state('gzclwx', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/gdzc/clmanager/gzclwx.html',
                    controllerUrl: 'module/gdzc/clmanager/clgl',
                    controller: 'clglCtrl'
                }
            }
        });

        /**
        *油卡充值
        */
        $stateProvider.state('gzykcz', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/gdzc/clmanager/gzykcz.html',
                    controllerUrl: 'module/gdzc/clmanager/clgl',
                    controller: 'clglCtrl'
                }
            }
        });

        /**
        *车辆使用(查询)
        */
        $stateProvider.state('gzclsq_query', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/gdzc/clmanager/gzclsq_query.html',
                    controllerUrl: 'module/gdzc/clmanager/clgl',
                    controller: 'clglCtrl'
                }
            }
        });

        /**
       *车辆使用（流程）
       */
        $stateProvider.state('gzclsq', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/gdzc/clmanager/gzclsq.html',
                    controllerUrl: 'module/gdzc/clmanager/clgl',
                    controller: 'clglCtrl'
                }
            }
        });

        /**
        *采购统计
        */
        $stateProvider.state('gzcgtj', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/gdzc/sjtj/gzcgtj.html',
                    controllerUrl: 'module/gdzc/clmanager/clgl',
                    controller: 'clglCtrl'
                }
            }
        });

        /**
        *维修统计
        */
        $stateProvider.state('gzwxtj', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/gdzc/sjtj/gzwxtj.html',
                    controllerUrl: 'module/gdzc/clmanager/clgl',
                    controller: 'clglCtrl'
                }
            }
        });

        /**
        *资源申请表单
        */
        $stateProvider.state('gzzysq_query', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/zydd/gzzysq_query.html',
                    controllerUrl: 'module/officalmanager/zydd/gzzysq_query',
                    controller: 'zysqCtrl'
                }
            }
        });

        /**
        *资源申请工作流
        */
        $stateProvider.state('gzzysq', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/zydd/gzzysq.html',
                    /*controllerUrl:  'module/gdzc/clmanager/clgl',
                    controller:     'clglCtrl'*/
                }
            }
        });

        /**
        *调度台账
        */
        $stateProvider.state('gzddtz', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/zydd/gzddtz.html',
                    /*controllerUrl:  'module/gdzc/clmanager/clgl',
                    controller:     'clglCtrl'*/
                }
            }
        });

        /**
        *调度统计
        */
        $stateProvider.state('gzddtj', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/zydd/gzddtj.html',
                    /*controllerUrl:  'module/gdzc/clmanager/clgl',
                    controller:     'clglCtrl'*/
                }
            }
        });

        /**
        *社区项目申请/台账
        */
        $stateProvider.state('gzsqxm', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/projects/sqresearch/gzsqxm.html',
                    /*controllerUrl:  'module/gdzc/clmanager/clgl',
                    controller:     'clglCtrl'*/
                }
            }
        });

        /**
        *社区项目统计
        */
        $stateProvider.state('gzsqxmtjview', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/projects/sqresearch/gzsqxmtjview.html',
                    /*controllerUrl:  'module/gdzc/clmanager/clgl',
                    controller:     'clglCtrl'*/
                }
            }
        });

        /**
        *管理站档案
        */
        $stateProvider.state('gzglzda', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/zygl/gyl/gzglzda.html',
                    /*controllerUrl:  'module/gdzc/clmanager/clgl',
                    controller:     'clglCtrl'*/
                }
            }
        });

        /**
        *护林员档案
        */
        $stateProvider.state('gzhlyda', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/zygl/gyl/gzhlyda.html',
                    /*controllerUrl:  'module/gdzc/clmanager/clgl',
                    controller:     'clglCtrl'*/
                }
            }
        });

        /**
        *外籍人员入区许可(查询)
        */
        $stateProvider.state('gzwjryrqxk_query', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/xzxkbl/gzwjryrqxk_query.html',
                    controllerUrl: 'module/officalmanager/xzxkbl/xzxkbl',
                    controller: 'xzxkblCtrl'
                }
            }
        });

        /**
       *外籍人员入区许可（流程）
       */
        $stateProvider.state('gzwjryrqxk', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/xzxkbl/gzwjryrqxk.html',
                    controllerUrl: 'module/officalmanager/xzxkbl/xzxkbl',
                    controller: 'xzxkblCtrl'
                }
            }
        });
        /**
        *外来科研人员入区许可（查询表单）
        */
        $stateProvider.state('gzryrqxk_query', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/xzxkbl/gzryrqxk_query.html',
                    controllerUrl: 'module/officalmanager/xzxkbl/xzxkbl',
                    controller: 'xzxkblCtrl'
                }
            }
        });
        /**
        *外来科研人员入区许可流程表单
        */
        $stateProvider.state('gzryrqxk', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/xzxkbl/gzryrqxk.html',
                    controllerUrl: 'module/officalmanager/xzxkbl/xzxkbl',
                    controller: 'xzxkblCtrl'
                }
            }
        });
        /**
        *安全保卫
        */
        $stateProvider.state('gzaqbw', {
            views: {
                'model': {
                    template: ''
                },
                'panel': {
                    templateUrl: 'module/officalmanager/hqab/gzaqbw.html',
                    /*controllerUrl:  'module/gdzc/clmanager/clgl',
                    controller:     'clglCtrl'*/
                }
            }
        });
		
		/**
		*签到时间
		*/
		$stateProvider.state('gzgzrqdsj', {
		    views: {
		        'model': {
		            template: ''
		        },
		        'panel': {
		            templateUrl: 'module/rsgl/kqsz/gzgzrqdsj.html',
		            controllerUrl:  'module/rsgl/kqsz/gzqdsz',
		            controller:     'qdszCtrl'
		        }
		    }
		});
		
		/**
		*签到地点
		*/
		$stateProvider.state('gzqdglqddd', {
		    views: {
		        'model': {
		            template: ''
		        },
		        'panel': {
		            templateUrl: 'module/rsgl/kqsz/gzqdglqddd.html',
		            controllerUrl:  'module/rsgl/kqsz/gzqdsz',
		            controller:     'qdszCtrl'
		        }
		    }
		});
    })
});