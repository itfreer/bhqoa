// JavaScript Document
(function($) {
	$.fn.itabExtend = function(options) {
		var defaults = {};
		options = $.extend(defaults, options);
		return this.each(function() {
			var $ul = $(this).find("ul");
			var $lis = $ul.find("li");
			var boxW = $ul.width();
			var bBtn = false;
			var $control = $("<li class='itab-control'><i class='prev'></i><i class='next'></i></li>").appendTo($ul).hide();
			var extendW = $control.outerWidth() || 32;
			controlTab(initTab());

			function initTab() {
				var k = 0;
				var liVisW = 0;
				var len = $lis.size();
				var arr = [];
				for (var i = 0; i < len; i++) {
					liVisW += $lis.eq(i).outerWidth(true);
				}
				if (liVisW + 5 > boxW) {
					liVisW = 0;
					for (var i = 0; i < len; i++) {
						liVisW += $lis.eq(i).outerWidth(true);
						if (liVisW + extendW > boxW) {
							bBtn = true;
							liVisW = $lis.eq(i).outerWidth(true);
							arr[k++] = i;
						}
					}
					arr[k] = len;
				}
				if (bBtn) {
					$lis.hide();
					for (var j = 0; j < arr[0]; j++) {
						$lis.eq(j).show();
					}
					$control.show();
				} else {
					$control.remove();
				}
				return arr;
			};

			function controlTab(arr) {
				if (bBtn) {
					var $prev = $control.find(".prev");
					var $next = $control.find(".next");
					var iNow = 0;
					var iAll = arr.length - 1;
					$prev.click(function() {
						if (iNow <= 0) {
							return;
						}
						if (iNow <= iAll) {
							iNow--;
							toggle();
						}
					});
					$next.click(function() {
						if (iNow >= iAll) {
							return;
						}
						if (iNow >= 0) {
							iNow++;
							toggle();
						}
					});
				}

				function toggle() {
					$lis.hide();
					var j = iNow == 0 ? 0 : arr[iNow - 1];
					for (; j < arr[iNow]; j++) {
						$lis.eq(j).show();
					}
				}
			}
		});
	}
	$(function() {
		$(".zs-itab-title").itabExtend();
	});
})(jQuery);