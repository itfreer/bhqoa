$(function() {
	initMenu();
	initCKXLMenu();
	pageResize();
	$(window).resize(function() {
		setTimeout(function() {
			pageResize();
		}, 100);
	});
	$(".zs-switch").click(function() {
		miniMenu();
	});
	$(".tips").tipTip({
		edgeOffset: 5
	});
});

function initMenu() {
	var menuItem = $(".zs-menu-item");
	var menuList = $(".zs-menu-list");
	var menuSwitch = $(".zs-menu-item").children("a");

	menuItem.each(function() {
		var item = $(this);
		item.find(menuList).hide();
		item.children("a").click(function() {
			menuItem.removeClass("open");
			if ($(this).hasClass("selected")) {
				$(this).removeClass("selected");
				$(this).next().hide();
				item.removeClass("open");
			} else {
				menuSwitch.removeClass("selected").next().hide();
				$(this).addClass("selected");
				$(this).next().show();
				item.addClass("open");
			}
		});
	});

	menuList.each(function() {
		$(this).find("ul").each(function() {
			$(this).children("li").each(function() {
				var liObj = $(this);
				if (liObj.children("ul").length > 0) {
					liObj.children("a").append('<b class="expandable close"></b>');
					liObj.children("ul.seclist").prepend("<b class='seclist-title'>" + liObj.children("a").text() + "</b>");
					liObj.children("ul").hide();
				}
				liObj.children("a").click(function() {
					if ($(this).next("ul").length) {
						if ($(this).children(".expandable").hasClass("close")) {
							$(this).parent().siblings("li").find("ul").hide().prev().children(".expandable").removeClass("open").addClass("close");
							$(this).children(".expandable").removeClass("close").addClass("open");
							$(this).next().show();
						} else {
							$(this).children(".expandable").removeClass("open").addClass("close");
							$(this).next().hide();
						}
					} else {
						$(this).parent().siblings("li").find("ul").hide().prev().children(".expandable").removeClass("open").addClass("close");
						if ($(".zs-wrap.sideClose").length) {
							menuList.find("a.selected").removeClass("selected");
							$(this).addClass("selected");
							menuList.children("ul").children("li").each(function() {
								if ($(this).find("a.selected").length) {
									$(this).children("a").addClass("selected");
								}
							})
						} else {
							menuList.find("a").removeClass("selected");
							$(this).addClass("selected");
						}
					}
				});
			});
		});
	});

	//初始化
	if (menuItem.find('a.selected').length){
		menuItem.find('a.selected').parents(menuList).show().siblings("a").click().parents(menuItem).addClass("open");
		menuItem.find('a.selected').parents("ul").show().siblings("a").children(".expandable").removeClass("close").addClass("open");
	} else {
		menuItem.eq(0).addClass("open");
		menuList.eq(0).show();
	}

	menuSwitch.each(function() {
		if ($(this).next().is(":visible")) {
			$(this).addClass("selected");
		}
	});
};

function miniMenu() {
	if ($(".zs-wrap.sideClose").length) {
		$(".zs-wrap").removeClass("sideClose");
		$(".zs-side").addClass("scroll");
		$(".zs-menu a.selected").parents("ul").siblings("a").removeClass("selected");
		$(".zs-menu-list").find(".expandable").show();
		$(".zs-menu-list").find('a.selected').parents("ul").show().siblings("a").children(".expandable").removeClass("close").addClass("open");
	} else {
		$(".zs-wrap").addClass("sideClose");
		$(".zs-side").removeClass("scroll");
		$(".zs-menu-list .firlist").children("li").each(function() {
			if ($(this).find("a.selected").length) {
				$(this).children("a").addClass("selected");
			}
		})
		$(".zs-menu-list").find(".expandable").removeClass("open").addClass("close");
		$(".zs-menu-list").find(".firlist").children("li").children("a").children(".expandable").hide();
		$(".zs-menu-list").find(".seclist").hide();
	}
};

function pageResize() {
	var winWidth = $(window).width();
	var winheight = $(window).height();
	if (winWidth > 1200) {
		$(".zs-wrap").removeClass("sideClose");
		$(".zs-menu a.selected").parents("ul").siblings("a").removeClass("selected");
	} else {
		$(".zs-wrap").addClass("sideClose");
		$(".zs-menu a.selected").parents("ul").siblings("a").addClass("selected");
		$(".zs-menu-list").find(".firlist").children("li").children("a").children(".expandable").hide();
	}
}

function initCKXLMenu() {
	//	zs-menu
	var items = $(".zs-menu.ckxl .item");
	var btns = $(".zs-menu.ckxl .list a");
	btns.click(function(e){
		e.stopPropagation();
		if($(this).hasClass("selected")){
			return;
		}
		btns.removeClass("selected");
		items.removeClass("selected");
		$(this).addClass("selected").parents(".item").addClass("selected");
	});
}
