(function($) {
	$.fn.iBox = function(options) {
		var defaults = {
			opacity: 0.5,
			callBack: null,
			target: null,
			requestType: null,
			title: "Message",
			drag: true,
			clone: false,
			overlay: true,
			iframelay: false,
			showMin: false,
			iframeWH: {
				width: 600,
				height: 300
			},
			html: ''
		},
			_this = this;

		this.TCDOS = $.extend(defaults, options);
		var iBoxHtml = '',
			iBoxOverlay = '',
			B = null,
			C = null,
			$win = $(window),
			$t = $(this),
			CWidth = 0,
			CHeight = 0,
			iBoxIframelay = '',
			iBoxControlMin = '';
		if (this.TCDOS.showMin) {
			iBoxControlMin = '<i class="ibox-restore">+</i><i class="ibox-min">-</i>';
		}
		if (this.TCDOS.iframelay) {
			iBoxIframelay += '<iframe id="ibox-iframe" style="position:absolute;z-index:-1; top:0; left:0;" scrolling="no" frameborder="0"></iframe>';
		}
        
		//弹窗容器
		iBoxHtml = '<div id="zs-ibox">';
		iBoxHtml += '<div class="ibox-popup">';
		iBoxHtml += '<table><tr><td>';
		iBoxHtml += '<div class="ibox-body">';
		iBoxHtml += '<div class="ibox-title"><span>' + _this.TCDOS.title + '</span><div class="control">' + iBoxControlMin + '<i class="ibox-close">×</i></div></div>';
		iBoxHtml += '<div class="ibox-content" id="ibox-content"></div>';
		iBoxHtml += '</div>';
		iBoxHtml += '<td></tr></table>';
		iBoxHtml += '</div>';
		iBoxHtml += iBoxIframelay;
		iBoxHtml += '</div>';
		iBoxOverlay = '<div id="zs-ibox-overlay" class="zs-ibox-hide"></div>';
		this.showBox = function() {
			$("#iBox_overlay").remove();
			$("#iBox").remove();
			if (_this.TCDOS.overlay) {
				B = $(iBoxOverlay).hide().addClass('zs-ibox-overlay-bg').css('opacity', _this.TCDOS.opacity).appendTo('body').fadeIn(300);
			}
			C = $(iBoxHtml).appendTo('body');
			handleClick();
		}

		//点击
		function handleClick() {
			var con = C.find("#ibox-content");
			if (_this.TCDOS.requestType && $.inArray(_this.TCDOS.requestType, ['iframe', 'ajax', 'img']) != -1) {
				con.html("<div class='ibox-load'><div id='ibox-loading'></div></div>");
				if (_this.TCDOS.requestType === "img") {
					var img = $("<img />");
					img.attr("src", _this.TCDOS.target);
					img.load(function() {
						img.appendTo(con.empty());
						setPosition();
						afterHandleClick();
					});
				} else {
					if (_this.TCDOS.requestType === "ajax") {
						$.get(_this.TCDOS.target, function(data) {
							con.html(data);
							C.find('.ibox-close').click(_this.close);
							setPosition();
							afterHandleClick();
						})
					} else {
						ifr = $("<iframe name='iBoxIframe' style='width:" + _this.TCDOS.iframeWH.width + "px;height:" + _this.TCDOS.iframeWH.height + "px;' scrolling='auto' frameborder='0' src='" + _this.TCDOS.target + "'></iframe>");
						ifr.appendTo(con.empty());
						ifr.load(function() {
							try {
								$it = $(this).contents();
								$it.find('.ibox-close').click(_this.close);
								fH = $it.height() + 20;
								fW = $it.width() + 20;
								w = $win;
								newW = Math.min(w.width() - 180, fW);
								newH = Math.min(w.height() - 200, fH);
								if (!newH) return;
								var lt = sizePosition(newW);
								C.css({
									left: lt[0],
									top: lt[1]
								});
								$(this).css({
									height: newH,
									width: newW
								});
								afterHandleClick();
							} catch (e) {}
						});
					}
				}
			} else {
				if (_this.TCDOS.target) {
					if (_this.TCDOS.clone) {
						$(_this.TCDOS.target).clone(true).show().appendTo(con.empty());
					} else {
						$(_this.TCDOS.target).show().appendTo(con.empty());
					}

				} else {
					if (_this.TCDOS.html) {
						con.html(_this.TCDOS.html);
					} else {
						$t.clone(this.TCDOS.clone).show().appendTo(con.empty());
					}
				}
				afterHandleClick();
			}
		}

		//点击后

		function afterHandleClick() {
			setPosition();
			C.show().find('i').mousedown(function() {
				C.css('opacity', 0.8);
				return false;
			}).mouseup(function() {
				C.css('opacity', 1);
			}).mouseout(function() {
				C.css('opacity', 1);
			});
			C.find('.ibox-close').click(_this.close);
			C.find('.ibox-restore').click(_this.restore);
			C.find('.ibox-min').click(_this.minimize);
			typeof _this.TCDOS.callBack === 'function' ? _this.TCDOS.callBack() : null;
			_this.TCDOS.drag ? drag() : null;
		}

		//位置属性设置

		function setPosition() {
			if (!C) return false;
			CWidth = C.width(), CHeight = C.height(), lt = sizePosition(CWidth, CHeight);
			C.css({
				left: lt[0],
				top: lt[1]
			});
			if (_this.TCDOS.iframelay) {
				C.find("#ibox-iframe").css({
					'width': CWidth,
					'height': CHeight
				});
			}
		}

		//计算位置

		function sizePosition(w, h) {
			var l = ($win.width() - w) / 2;
			var t = $win.scrollTop() + ($win.height() - h) / 2;
			if (l < 0) l = 0;
			if (t < 0) t = 0;
			return [l, t];
		}

		//拖拽

		function drag() {
			var dx, dy, l, t;
			var T = C.find('.ibox-title').css('cursor', 'move');
			T.bind("selectstart", function() {
				return false;
			});
			T.mousedown(function(e) {
				dx = e.clientX - parseInt(C.css("left"));
				dy = e.clientY - parseInt(C.css("top"));
				$(document).mousemove(move);
				C.css('opacity', 0.8);
				$(document).mouseup(up);
				return false;
			});
			//移动

			function move(e) {
				l = e.clientX - dx;
				t = e.clientY - dy;
				if (l > $win.width() - C.width()) {
					l = $win.width() - C.width();
				}
				if (l < 0) {
					l = 0;
				}
				if (t > $win.height() - C.height()) {
					t = $win.height() - C.height();
				}
				if (t < 0) {
					t = 0;
				}
				C.css({
					left: l,
					top: t
				});

			}

			function up(e) {
				$(document).unbind("mousemove", move);
				C.css('opacity', 1);
				T.unbind("mouseup", up);
			}
		}

		//关闭
		this.close = function() {
			if (C) {
				if (_this.TCDOS.overlay) {
					B.remove();
				}
				C.stop().fadeOut(0, function() {
					C.remove();
				})
			}
		}
		//还原
		this.restore = function() {
			C.find('.ibox-restore').hide();
			if (_this.TCDOS.overlay) {
				B.fadeIn(300);
			}
			$("#ibox-content").stop().slideDown(function() {
				C.find('.ibox-min').show();
				if (_this.TCDOS.iframelay) {
					C.find("#ibox-iframe").css({
						'width': CWidth,
						'height': CHeight
					});
				}
			});
		}
		//最小化
		this.minimize = function() {
			C.find('.ibox-min').hide();
			$("#ibox-content").stop().slideUp(function() {
				C.find('.ibox-restore').show();
				var w = C.width(),
					h = C.height();
				if (_this.TCDOS.overlay) {
					B.fadeOut(300);
				}
				if (_this.TCDOS.iframelay) {
					C.find("#ibox-iframe").css({
						'width': w,
						'height': h
					});
				}
			});
		}
		//resize    
		$win.resize(function() {
			setPosition();
		});

		//触发方式
		$t.click(function() {
			_this.showBox();
			return false;
		});
		return this;
	};
})(jQuery);