$(document).ready(function() {
	// gis|mis切换
	// $(".zs-menu a").click(function() {
	// 	var _url = $(this).attr("href");
	// 	var _dataType = $(this).attr("data-type");
	// 	if (typeof(_url) !== "undefined") {
	// 		if (_dataType.toLowerCase() == "gis") {
	// 			$("#zs-gis").show();
	// 			$("#zs-mis").hide();
	// 			if (_url.indexOf(".") >= 0) {
	// 				$("#zs-gis").load(_url.replace('#', ''));
	// 			}
	// 			$(".zs-area .switch").removeClass("open").siblings(".area").hide();
	// 			$(".zs-attribute .switch").addClass("open").siblings(".search").slideDown("fast");
	// 		} else {
	// 			$("#zs-mis").show();
	// 			$("#zs-gis").hide();
	// 			if (_url.indexOf(".") >= 0) {
	// 				$("#zs-mis").load(_url.replace('#', ''),function(){
	// 					setTableCheck();
	// 				});
	// 			}
	// 		}
	// 	}
	// });
	// 导航气泡快捷隐藏
	$(".zs-main").click(function() {
		$(".zs-wrap.sideClose .zs-menu .seclist:visible").siblings("a").children(".expandable").removeClass("open").addClass("close");
		$(".zs-wrap.sideClose .zs-menu .seclist:visible").hide();
	});
	$(document).on("click", ".zs-area .switch", function() {
		// 政区选择
		$(".zs-attribute .switch").removeClass("open").siblings(".search, .result").hide();
		$(this).toggleClass("open");
		$(this).next().slideToggle(300);
	}).on("click", ".zs-area .area .title .close", function() {
		$(".zs-area .switch").removeClass("open");
		$(".zs-area .switch").next().slideUp(300);
	}).on("click", ".zs-area .area .list a", function() {
		var present = $(this).text();
		$(".zs-area .area .title").children(".present").text(present);
		$(".zs-area .area .list a").removeClass("selected");
		$(this).addClass("selected");
	}).on("click", ".zs-subside .item .title .switch", function() {
		// 侧栏开关
		if($(this).hasClass("close")){
			$(this).parents(".zs-subside").removeClass("on-min").height("400px");
		}else{
			$(this).parents(".zs-subside").addClass("on-min").css({
				"height":"40px"
			})
		}
		$(this).toggleClass("close");
//		$(this).parents(".zs-subside").toggleClass("on-min");
	}).on("click", ".zs-maptools-type-list ul li a", function() {
		//地图交互选择方式
		$(this).addClass("selected").parent().siblings("li").children("a").removeClass("selected");
	}).on("click", "input[name=so-so]", function() {
		//搜索交互规则：搜索表单与搜索结果分开展示[name=so-so]
		$(".zs-so-form").hide();
		$(".zs-so-result").show();
	}).on("click", "a.zs-so-back", function() {
		$(".zs-so-form").show();
		$(".zs-so-result").hide();
	});
//	2017/10/10 subside拖拽
	(function(){
		var con;
		var line = $(".zs-subside .drag-line");
		var dx;
		line.bind("selectstart", function() {
			return false;
		});
		line.mousedown(function(e) {
			con = $(this).parents(".zs-subside");
			winH = $(window).height();
			$(document).mousemove(move);
			$(document).mouseup(up);
			con.css("transition", "none");
			return false;
		});

		function move(e) {
			var ms = e.clientY;
			if (ms < 100) {
				ms = 100;
			}
			console.log(ms);
			dx = winH - ms;
			if(dx<40){
				dx=40;
			}
			con.height(dx);
		}

		function up(e) {
			$(document).unbind("mousemove");
			line.unbind("mouseup", up);
		}
	})();
//	2017/10/16 user点击
	$(".zs-head .user .img").click(function(event){
		event.stopPropagation();
		$(this).siblings(".board").stop().slideToggle("fast");
	});
	//		地图层级筛选器
	$(".zs-map-tool .tool-tcglq").click(function() {
		$(".zs-map-ex").toggleClass("show");
	});
	$(".zs-map-ex .close").click(function() {
		$(".zs-map-ex").removeClass("show");
	});
	// $(".zs-map-ex .layer-items a:not(.sw-btn)").click(function() {
	// 	$(this).siblings("a:not(.sw-btn)").removeClass("selected");
	// 	$(this).addClass("selected");
	// });
	// $(".zs-map-ex .layer-items a.sw-btn").click(function() {
	// 	$(this).toggleClass("selected");
	// });
	// $(".zs-attribute .switch").click(function(event){
	// 	event.stopPropagation();
	// 	$(".zs-area .switch").removeClass("open").siblings(".area").hide();
	// 	if($(this).hasClass("open")){
	// 		$(this).removeClass("open").siblings(".search, .result").slideUp("fast");
	// 	}else{
	// 		$(this).addClass("open").siblings(".search").slideDown("fast");
	// 	}
	// });
	// $(".zs-attribute .open-result").click(function(event){
	// 	event.stopPropagation();
	// 	$(this).parents(".search").hide().siblings(".result").slideDown("fast");
	// });
	// $(".zs-attribute .result .back").click(function(event){
	// 	event.stopPropagation();
	// 	$(this).parents(".result").hide().siblings(".search").slideDown("fast");
	// });
//	表格选中
	// function setTableCheck() {
	// 	var sigCk = $(".zs-data-table tbody .ck-box");
	// 	var allCk = $(".zs-data-table .ck-all");
	// 	var table = null;
	// 	sigCk.unbind("click").click(function(event){
	// 		event.stopPropagation();
	// 		table = $(this).parents(".zs-data-table");
	// 		var iSigCk = table.find("tbody .ck-box");
	// 		var iAllCk = table.find(".ck-all");
	// 		if($(this).hasClass("selected")){
	// 			var ckdLen = table.find("tbody .ck-box.selected").length;
	// 			if(ckdLen == iSigCk.length){
	// 				iAllCk.removeClass("selected").siblings("input[type='checkbox']").prop("checked", false);
	// 			}
	// 			$(this).removeClass("selected").siblings("input[type='checkbox']").prop("checked", false);
	// 		}else{
	// 			$(this).addClass("selected").siblings("input[type='checkbox']").prop("checked", true);
	// 			var ckdLen = table.find("tbody .ck-box.selected").length;
	// 			if(iAllCk.length){
	// 				if(ckdLen == iSigCk.length){
	// 					iAllCk.addClass("selected").siblings("input[type='checkbox']").prop("checked", true);
	// 				}
	// 			}
	// 		}
	// 	});
	// 	allCk.unbind("click").click(function(event){
	// 		event.stopPropagation();
	// 		table = $(this).parents(".zs-data-table");
	// 		var iSigCk = table.find("tbody .ck-box");
	// 		var iSigInput = table.find("tbody input[type='checkbox']");
	// 		if($(this).hasClass("selected")){
	// 			$(this).removeClass("selected").siblings("input[type='checkbox']").prop("checked", false);
	// 			iSigCk.removeClass("selected");
	// 			iSigInput.prop("checked", false);
	// 		} else {
	// 			$(this).addClass("selected").siblings("input[type='checkbox']").prop("checked", true);
	// 			iSigCk.addClass("selected");
	// 			iSigInput.prop("checked", true);
	// 		}
	// 	});
	// }
	// setTableCheck();
})