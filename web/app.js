define(function (require, exports, module) {
	// require('jquery-easyui-zh');
	// require('jquery-easyui');

	var angular = require('angular');
	require('angular-route');
    require('angular-ui-router');
    require('ui-upload');
    require('as-files');
    var asyncLoader = require('angular-async-loader');

	require('easyui');
	require('form');
	require('security');

    require('bpm-workflow');
    require('bpm-workflow-sign');
    require('bpm-workflow-back');
    require('bpm-workflow-skip');
	require('bpm-workflow-retract');
    require('bpm-workflow-disposal');
    require('bpm-init');
    require('bpm-selected');
    require('bpm-process');
    
    var app = angular.module('app', ['ui.router','ui.easyui','form','security',
    'bpm.init','bpm.workflow','bpm.selected','bpm.process',
    'bpm.workflow.sign','bpm.workflow.back',
    'bpm.workflow.skip','bpm.workflow.retract','bpm.workflow.disposal'])
    .filter("asFormateDateTime", ['$rootScope', function ($rootScope) {
	    return function (input) {
	    	if (input == null) {
				return '';
			}
			var dType = typeof(input);
			var date = $rootScope.parseData(input,dType);
			if(date){
				var m = date.getMonth() + 1;
				var result = date.getFullYear()+'-';
				if(m<10){
					result=result+"0"+m;
				}else{
					result=result+m;
				}
				
				if(date.getDate()<10){
					result=result+'-'+"0"+date.getDate();
				}else{
					result=result+'-'+date.getDate();
				}
				
				if(date.getHours()<10){
					result=result+' 0'+date.getHours()+':';
				}else{
					result=result+' '+date.getHours()+':';
				}
				
				if(date.getMinutes()<10){
					result=result+'0'+date.getMinutes()+':';
				}else{
					result=result+''+date.getMinutes()+':';
				}
				
				if(date.getSeconds()<10){
					result=result+'  0'+date.getSeconds();
				}else{
					result=result+'  '+date.getSeconds();
				}
				return result;
			}else{
				return '';
			}
	    };
	}])
    .filter("asULength", ['$rootScope', function ($rootScope) {
   		  return function (input) {
   		  	var maxLength=20;
	    	if(input==null || input==""){
	    		return "";
	    	}
	    	if(input.length<=maxLength){
	    		return input;
	    	}else{
	    		input = input.substring(0, maxLength-3) + "...";
	    		return input;
	    	}
	    };
	}])
    .factory('$app', function($rootScope, $http) {
		$app = {
			// 打开地图查询面板
			attrOpen : false,
			setAttrOpen : function(isOpen){
				if(isOpen==null){
					this.attrOpen = !this.attrOpen;
				}else{
					this.attrOpen = isOpen;
				}
			},
			// 打开查询结果面板
			searchOpen:true,
			setSearchOpen : function(isOpen){
				if(isOpen==null){
					this.searchOpen = !this.searchOpen;
				}else{
					this.searchOpen = isOpen;
				}
			},

			// mis 下zs-content设置高度
			setConHei:function(){
				var hei = $("#zs-main").height() - $("#zs-mis .zs-map-tool").outerHeight();
				$("#zs-mis .zs-content").height(hei);
			}
		};
		for(f in $app){
			$rootScope[f] = $app[f];
		}
		return $app;
	})
    .filter("todoReadFilter", ["$sce", function ($sce) {
	    return function (isread) {
	    	if(isread == "0" || isread==null || isread==''){
	    		return "font-weight: bold;color: red;";
	    	}else {
	    		return "";
	    	}
	    };
	}]).
	filter("toDoWorkJB", ["$sce", function ($sce) {
	    return function (input, waitMinutes) {
	    	if(waitMinutes==null || waitMinutes=='undefined'){
	    		return ;
	    	}
	    	if(waitMinutes>0){
				return ;
	    	}else{
	    		return "background: #FF1313;";
	    	}
	    };
	}])
	.controller('appCtrl', ['$rootScope', '$scope', '$app', '$location', '$anchorScroll',
		function($rootScope, $scope, $app, $location, $anchorScroll){
			// 挂接项目
			$scope.chooseProject = function(editRow,rEditRow,closeId,proId){
				if(proId == 'gzprojectinfo'){
					editRow.sxmmc=rEditRow.sxmmc;
					editRow.sxmbh=rEditRow.sxmbh;
					editRow.sqr=rEditRow.sqr;
					editRow.sqrid=rEditRow.sqrid;
					editRow.sqbm=rEditRow.sqbm;
					editRow.sqbmName=rEditRow.sqbmName;
					editRow.sqsj=rEditRow.sqsj;
					editRow.jxsj=rEditRow.jxsj;
				}else if(proId == 'gzwlkyxmsqb'){
					editRow.sxmmc=rEditRow.xmmc;
					editRow.sxmbh=rEditRow.bh;
					editRow.fzr=rEditRow.fzr;
					editRow.fzrid=rEditRow.fzrid;
					editRow.fzbm=rEditRow.fzbm;
					editRow.fzbmName=rEditRow.fzbmName;
					editRow.lfsj=rEditRow.lfsj;
					editRow.lfsjjssj=rEditRow.lfsjjssj;
				}else{
					editRow.xmmc=rEditRow.sxmmc;
				}
				editRow.xmid=rEditRow.id;
				$scope.closeWindow(closeId);
			};
			
			//挂接预约管理
			$scope.chooseYYGL = function(editRow,rEditRow,closeId){
					/*editRow.yyid=rEditRow.id;
					$scope.closeWindow(closeId);*/
				if(rEditRow==null){
					$.messager.alert("系统提示","请先选择记录！");
				}else{
					editRow.yyid=rEditRow.id;
					editRow.lfdw=rEditRow.lfdw;
					editRow.lfzt=rEditRow.lfzt;
					editRow.dfsj=rEditRow.dfsj;
					editRow.dfjssj=rEditRow.dfjssj;
					editRow.lfry=rEditRow.lfry;
					editRow.lfrs=rEditRow.lfrs;
					editRow.sfxyapzs=rEditRow.sfxyapzs;
					editRow.sfxyapzsName=rEditRow.sfxyapzsName;
					editRow.nxrs=rEditRow.nxrs;
					editRow.wxrs=rEditRow.wxrs;
					editRow.kcnr=rEditRow.kcnr;
					editRow.tel=rEditRow.tel;
					editRow.cjrmc=rEditRow.cjrmc;
					editRow.jdbmmc=rEditRow.jdbmmc;
					editRow.jdbmmcName=rEditRow.jdbmmcName;
					editRow.jdfzrmc=rEditRow.jdfzrmc;
					editRow.cyr=rEditRow.cyr;
					$scope.closeWindow(closeId);
				}
			};
			
			// 挂接用户
			$scope.chooseUser = function(editRow,rEditRow,closeId){
				editRow.cyr=rEditRow.userName;
				editRow.cyrid=rEditRow.id;
				$scope.closeWindow(closeId);
			};
			
			$scope.chooseWlProject = function(editRow,rEditRow,closeId){
				editRow.sxmmc=rEditRow.xmmc;
				editRow.xmid=rEditRow.id;
				$scope.closeWindow(closeId);
			};
			
			$scope.addChildRow=function(editRow,childValues){
	    		if(editRow[childValues]==null){
	    			editRow[childValues]=[];
	    		}
	    		var row={};
	    		row.id=$rootScope.getGUID();
	    		row.xmid=editRow.id;
	    		editRow[childValues].push(row);
	    	};
	    	
	    	$scope.newpageproject={};
	    	
	    	$scope.cheekProject=function(xmid){
	    		if(xmid){
	    			var url=getServerBaseUrl()+"/gzprojectinfo/getEntity";
			    	$rootScope.getEntity(url,xmid,function(data){
			    		$scope.$apply(function(){
				    			console.log(data);
				    			var row=JSON.parse(data.param);
				    			newpageproject=row;
			    		});
			    	},function(e){
			    		console.log("请求临时数据失败");
			    		alert("加载数据失败，请重试！");
			    	});
	    			window.open("/module/newpage/projectinfo.html");
	    		}else{
	    			$.messager.alert("系统提示","请选择项目！");
	    		}
	    	};
	    	
	    	/**
	    	 * 设置默认用户
	    	 * @param {Object} pfn
	    	 * @param {Object} field
	    	 * @param {Object} fieldName
	    	 */
	    	$scope.setUser=function(pfn,field,fieldName){
	    		if($rootScope[pfn].editRow["FIELD_ISNEW"]){
	    			if(field!=null){
		    			$rootScope[pfn].editRow[field]=$rootScope.curUser.userid;
		    		}
		    		
		    		if(fieldName!=null){
		    			$rootScope[pfn].editRow[fieldName]=$rootScope.curUser.userName;
		    		}
	    		}
	    		return true;
	    	};
	    	
	    	
	    	// 批量选择收件人
			$scope.chooseSJR = function(pfn,childField,fn,closeId){
				if($rootScope.isOptioning){
					$.messager.alert("系统提示","系统处理中，请稍等！");
					return false;
				}
				var rows = $rootScope[fn].sRows;
				if(rows==null || rows.length<=0){
					$.messager.alert('系统提示', '请选择收件人！');
					return;
				}
				
				
				if(!$rootScope[pfn].editRow[childField]){
					$rootScope[pfn].editRow[childField]=[];
				}
				
				
				for(var i=0;i<rows.length;i++){
					var cRow={};
					cRow.id=$rootScope.getGUID();
					cRow.jsrid=rows[i].userid;
					cRow.jsrmc=rows[i].userName;
					if(rows[i].departmentName=="局领导"){
						cRow.jsrzw=rows[i].departmentName;
					}else{
						cRow.jsrzw=rows[i].roleNames;
					}
					cRow.sfqr='0';
					var organizationName=rows[i].organizationName?rows[i].organizationName:"";
					var departmentName=rows[i].departmentName?rows[i].departmentName:"";
					var operatingPostName=rows[i].operatingPostName?rows[i].operatingPostName:"";
					//cRow[i].jsrjg=organizationName+departmentName+operatingPostName;
					cRow.jsrjg=rows[i].departmentId;
					cRow.jsrjgmc=rows[i].departmentName;
					
					if($rootScope[pfn].editRow[childField]!=null && $rootScope[pfn].editRow[childField].length>0){
						var ufind=true;
						for(var k=0;k<$rootScope[pfn].editRow[childField].length;k++){
							if($rootScope[pfn].editRow[childField][k].jsrid==cRow.jsrid) {
								ufind=false;
								break;
							}
						}
						if(ufind){
							$rootScope[pfn].editRow[childField].push($.extend(true, {}, cRow));
						}
					}else{
						$rootScope[pfn].editRow[childField].push($.extend(true, {}, cRow));
					}
					
                }
				
				
				/*if(!childRow){
					childRow=[];
				}*/
				
				
				//$rootScope.$apply(function(){
					//childRow.push.apply(childRow,cRow);
				//});
			
				$scope.closeWindow(closeId);
			};
			
			//获取进度条
			$scope.getPrijectJd =function(datas){
				for(i=0;i<datas.length;i++){
					var xmjd=datas[i].xmjd;
					datas[i].xmjdt="<div class=\"progress-bar\" aria-valuenow=\""+xmjd+"\""
									"aria-valuemax=\"100\" aria-valuemin=\"0\" style=\"width:"+xmjd+"%\">"+xmjd%+"</div>";
				}
			};
			
			//打印
	    	$scope.print = function(fName,template,printType,data,filename,callback,onerror){
				var fo=$rootScope[fName];
				var baseUrl = getServerBaseUrl()+'/print/'+printType;
				//添加用户指定的path
				if(fo.rootpath){
					baseUrl = fo.rootpath+'/print/'+printType;
				}
				
				data = $rootScope.toJSONStr(data);
		        var form = $("<form></form>").attr("action", baseUrl).attr("method", "post").attr("target", "_blank");
		        form.append($("<input></input>").attr("type", "hidden").attr("name", "template").attr("value", template));
		        if(printType=="HtmltoWord"){
		        	form.append($("<input></input>").attr("type", "hidden").attr("name", "pageLocation").attr("value", pageLocation));
		        }
		        form.append($("<input></input>").attr("type", "hidden").attr("name", "filename").attr("value", filename));
		        form.append($("<input></input>").attr("type", "hidden").attr("name", "data").attr("value", data));
		        form.appendTo('body').submit().remove();
	    	};
	    	
	    	
	    	 $scope.initTestUser=function(){
		    	//$rootScope.curUser={};
				//$rootScope.curUser["userid"]="e11a92409b4d45639e4b837df0264f4d";
				/*$rootScope.curUser["userid"]="0cb13e9616ac4d769235a20f3121e49c";
				$rootScope.curUser["userid"]="08538e05b5e146b283d5760d29410981";
				$rootScope.curUser["userid"]="08538e05b5e146b283d5760d29410981";*/
				//$rootScope.curUser["userid"]="d42ccdcb3a9a481e856908d1437a0642";
				//$rootScope.curUser["userName"]="省级受理";
				//$rootScope.curUser["userName"]="县用户02";
				//$rootScope.curUser["xian"]="520000";
				//$rootScope.curUser["xianName"]="贵州";
		    };
			/**
			 * 资源调度
			 * @param {Object} formName
			 */
		    $scope.initGzzysq=function(formName){
				if($rootScope[formName].editRow==null){
					$rootScope[formName].editRow={};
					$rootScope[formName].editRow.id=$rootScope.getGUID();
					$rootScope[formName].editRow[$rootScope.isNew]=true;
				}
				
				$scope.setbpmInitTab(formName);
			};
			/**
			 * 外来科员人员入区许可
			 * @param {Object} formName
			 */
			 $scope.initGzwlkyryrqxk=function(formName){
				if($rootScope[formName].editRow==null){
					$rootScope[formName].editRow={};
					$rootScope[formName].editRow.id=$rootScope.getGUID();
					$rootScope[formName].editRow.wlxmlx='1';
					$rootScope[formName].editRow[$rootScope.isNew]=true;
				}
				$scope.setbpmInitTab(formName);
			};
			
			/**
			 * 外籍人员入区许可
			 * @param {Object} formName
			 */
			$scope.initGzwjryrqxk=function(formName){
				if($rootScope[formName].editRow==null){
					$rootScope[formName].editRow={};
					$rootScope[formName].editRow.id=$rootScope.getGUID();
					$rootScope[formName].editRow.wlxmlx='2';
					$rootScope[formName].editRow[$rootScope.isNew]=true;
				}
				$scope.setbpmInitTab(formName);
			};
			
			/**
			 * 外来科研项目入区许可
			 * @param {Object} formName
			 */
			$scope.initGzwlkyxmrqxk=function(formName){
				if($rootScope[formName].editRow==null){
					$rootScope[formName].editRow={};
					$rootScope[formName].editRow.id=$rootScope.getGUID();
					$rootScope[formName].editRow[$rootScope.isNew]=true;
					$rootScope[formName].editRow.bpmkey=$scope.getQueryString("bk");
				}
				$scope.setbpmInitTab(formName);
			};
			
			/**
			 * 请假管理
			 * @param {Object} formName
			 */
			$scope.initGzqjgl=function(formName){
				if($rootScope[formName].editRow==null){
					$rootScope[formName].editRow={};
					$rootScope[formName].editRow.id=$rootScope.getGUID();
					$rootScope[formName].editRow[$rootScope.isNew]=true;
					$rootScope[formName].editRow.qjts=1;
					$rootScope[formName].editRow.bpmkey=$scope.getQueryString("bk");
				}
				$scope.setbpmInitTab(formName);
			};
			
			/**
			 * 初始化发文管理功能
			 * @param {Object} formName
			 */
			$scope.initGzPublishFile=function(formName){
				if($rootScope[formName].editRow==null){
					$rootScope[formName].editRow={};
					$rootScope[formName].editRow.id=$rootScope.getGUID();
					$rootScope[formName].editRow[$rootScope.isNew]=true;
					$rootScope[formName].editRow.qjts=1;
					$rootScope[formName].editRow.bpmkey=$scope.getQueryString("bk");
				}
				$scope.setbpmInitTab(formName);
			};
			
			/**
			 * 初始化用章管理
			 * @param {Object} formName
			 */
			$scope.initYzgl=function(formName){
				if($rootScope[formName].editRow==null){
					$rootScope[formName].editRow={};
					$rootScope[formName].editRow.id=$rootScope.getGUID();
					$rootScope[formName].editRow[$rootScope.isNew]=true;
					$rootScope[formName].editRow.qjts=1;
					$rootScope[formName].editRow.bpmkey=$scope.getQueryString("bk");
				}
				$scope.setbpmInitTab(formName);
			};
			
			/**
	    	 * 查看项目过程
	    	 * @param  {[type]} url      项目地址
	    	 * @param  {[type]} bpmkey   流程类型键
	    	 * @param  {[type]} formname 表单id
	    	 * @param  {[type]} viewid   uiId
	    	 * @param  {[type]} sexeid   流程项目主键
	    	 * @return {[type]}          描述
	    	 */
	    	$scope.toQueryWork=function(url,bpmkey,formname,viewid,guid){
	    		var openurl=url+"?bk="+encodeURIComponent(bpmkey)
	    			+"&fn="+encodeURIComponent(formname)
	    			+"&vi="+encodeURIComponent(viewid)
	    			+"&dk="+encodeURIComponent(guid);
	    		window.open(openurl);
	    	};
	    	
	    	$scope.confirmReceive = function(fn,editRow){
	    		if($rootScope.isOptioning){
					$.messager.alert("系统提示","系统处理中，请稍等！");
					return false;
				}
				if(editRow != null){
					editRow.sfqr="1";
					$rootScope.editRow(fn, editRow);
					$rootScope.save(fn, editRow, $scope.openWindow('edit'));
					var queryObject={'sfqr':'1'};
	    			$rootScope[fn].queryObject=queryObject;
					$rootScope.loadData(fn);
				}else{
					$.messager.alert('系统提示', '请选择一条记录！');
					return;
				}
				
	    	};
	    	
		    /**
	    	 * 查看收文项目
	    	 */
	    	$scope.readReceive=function(fn,editRow){
	    		if(editRow == null){
					$.messager.alert('系统提示', '请选择一条记录！');
					return;
				}
				
				if(editRow.sfqr!="1" && editRow.sfqr!=1){
					if($rootScope.isOptioning){
						$.messager.alert("系统提示","系统处理中，请稍等！");
						return false;
					}
					if(editRow != null){
						
						$rootScope.editRow(fn, editRow);
						
						//$scope.openWindow('edit');
						
						editRow.sfqr="1";
						//测试代码
						//editRow.ydsj=new Date();
						editRow.ydsj=$rootScope.formatTime(new Date())
						$rootScope.save(fn, editRow,function(){
							$rootScope.loadData(fn);
							$scope.toQueryWork('gzpublishfile.html',editRow.bpmkey,
							'gzpublishfile','gzpublishfile',editRow.fwid);
						});
						
					}else{
						$.messager.alert('系统提示', '请选择一条记录！');
						return;
					}
				}else{
					//$rootScope.editRow(fn, editRow);
					$scope.toQueryWork('gzpublishfile.html',editRow.bpmkey,'gzpublishfile','gzpublishfile',editRow.fwid);
					//$scope.openWindow('edit');
				}
	    	};
	    	
	    	$scope.loadReceiveData = function(fn,query,type){
	    		var queryObject={'sfqr':query};
	    		$rootScope[fn].queryObject=queryObject;
	    		$rootScope.loadData(fn);
	    		$rootScope.setTab('receivemenu', type);
	    	};
	    	
	    	/**
			 * 获取系统查询参数
			 */
			$scope.getQueryString=function(key){
		        var reg = new RegExp("(^|&)"+key+"=([^&]*)(&|$)");
		        var result = window.location.search.substr(1).match(reg);
		        return result?decodeURIComponent(result[2]):null;
		    };
	    	
	    	
	    	$scope.initMainPage=function(){
	    		var page=$scope.getQueryString('page');
	    		if(page==null || page=="") page="gztask";
	    		
	    		$rootScope.iniTab('cmenu', page, 'selected');
	    		$rootScope.setUiView(page);
	    		
	    		var menu=$scope.getQueryString('menu');
	    		if(menu==null || menu=="") menu="rwgl";
	    		$rootScope.iniTab('menu', menu, 'selected');
	    	};
	    	
	    	$scope.getNowFormatDate=function() {
		        var mydate = new Date();
		        var year = mydate.getFullYear();
		        var month = mydate.getMonth() + 1;
		        var strDate = mydate.getDate();
		        if (month >= 1 && month <= 9) {
		            month = "0" + month;
		        }
		        if (strDate >= 0 && strDate <= 9) {
		            strDate = "0" + strDate;
		        }
		        $rootScope.currentdate = year + "年" + month + "月" + strDate+"日";
		        
		        var myddy=mydate.getDay();//获取存储当前日期
  				var weekday=["星期日","星期一","星期二","星期三","星期四","星期五","星期六"];
  				 $rootScope.myddy=weekday[myddy];
  				 
  				var hours = mydate.getHours();
				var minutes = mydate.getMinutes();
				var seconds = mydate.getSeconds();
				
				if(hours<10){
					hours="0"+hours;
				}
				
				if(minutes<10){
					minutes="0"+minutes;
				}
				
				if(seconds<10){
					seconds="0"+seconds;
				}
				
				$rootScope.currentTime=hours+":"+minutes+":"+seconds;
  				
		        //return currentdate;
		    };
	    	
	    	setInterval(function(){
	    		$rootScope.$apply(function(){
	    			$scope.getNowFormatDate();
	    		});
	    		
	    	},1000);
	    	
	    	/***
	    	 * 签到管理
	    	 * @param {Object} success
	    	 * @param {Object} error
	    	 */
	    	$scope.qdgl=function(method,type,success,error){
		   		var url=getServerBaseUrl()+"/gzqdgl/qdgl";
		   		jQuery.support.cors = true;
		   		$.ajax({
					url : url,
					type : 'POST',
					data : {'method':method,'type':type},
					timeout : 35000,
					dataType : 'json',
					headers: { 'jwt': $.cookie('jwt') },
					contentType: "application/json;charset=utf-8",
					success : function(data){
						if(data!=null){
							success(data);
						}else{
							error();
						}
					},
					error : error
				});
		    };
		    
		    $scope.syqdgldata=function(method,type){
		     	$scope.qdgl(method,type,function(data){	
					if(data){
				        if(data["status"]){
					        if(data["type"]=="create"){
					            $.messager.alert("系统提示", data["message"]);
					        }else{
					            $.messager.confirm("系统提示", data["message"],function(r){
					            	if(r){
					            		$scope.syqdgldata("update",type);
					            	}
					            });
					        }
				        }else{
				          $.messager.alert("系统提示", data["message"]);
				        }
			        }else{
			        	$.messager.alert("系统提示",'打卡失败，请重试！');
			      	}
		     	},function(){
		     		$.messager.alert("系统提示","打卡失败，请重试！");
		     	});
		    };
		    
		    $scope.bpmSuccess=function(){
		    	window.close();
		    };
		    
		    
		    /**
	    	 * 打开新闻详情
	    	 * @param {Object} r
	    	 */
	    	$scope.openNewsShow = function(r){
				var url = "module/newsAndDowload/news.html";
				url=url+"?key="+encodeURIComponent(r.id);
				var news = r.text;
				$("#news").html(news);
				
				window.open(url, "");
			};
	    	
	    	/**
	    	 * 打开新闻列表
	    	 */
	    	$scope.openNewsList = function(type){
	    		
	    		var url = "module/newsAndDowload/newsList.html";
	    		url=url+"?type="+encodeURIComponent(type);
	    		window.open(url, "");
	    	};
	    	
	    	/**
	    	 * 打开下载列表
	    	 */
	    	$scope.openDowloadList = function(){
	    		
	    		var url = "module/newsAndDowload/dowloadList.html";
	    		window.open(url, "");
	    	};
	    	
	    	/**
	    	 * 正反选
	    	 */
	    	$scope.selectOrCancelAll = function(fn,cn,field,trueValue,falseValue){
				field = field ? field : $rootScope.selectedField;
				var checked = $("#"+cn).attr("checked") == "checked";
				var checkedValue = null;
				if(checked){
					checkedValue = trueValue ? trueValue : checked;
				}else{
					checkedValue = falseValue ? falseValue : checked;
				}
				$rootScope[fn].sRows = [];
				$rootScope[fn].datas.forEach(function(e){
					/*if(e[field]==undefined){
						e[field]=false;
					}
				    e[field] = !e[field];
				    if(checked){
				    	$rootScope[fn].sRows.push(e);
				    }*/
				   e[field] =checkedValue;
				   if(checked){
				    	$rootScope[fn].sRows.push(e);
				    }
				});
			};
			
			/**
			 * 选中或者撤销选中子表记录
			 */
			$scope.selectRows=function(list,cn,field,trueValue,falseValue){
				field = field ? field : $rootScope.selectedField;
				var checked = $("#"+cn).attr("checked") == "checked";
				var checkedValue = null;
				if(checked){
					checkedValue = trueValue ? trueValue : checked;
				}else{
					checkedValue = falseValue ? falseValue : checked;
				}
				if(list
					&& list.length>0){
					for(var k=0;k< list.length;k++){
						list[k][field]=checkedValue;
					}
				}
			};
			
			$scope.openDataWindow=function(url,id){
				
				var temp=url+"&key="+encodeURIComponent(id);
				window.open(temp);
			};
			
			/**
			 * window窗体关闭功能
			 */
			$scope.windowClose=function(){
				window.close();
			};
			
			//移动到锚点
			$scope.gotoById = function (id) {
	            $location.hash(id);
	            $anchorScroll();
	            return true;
			};
			
			/**
			 * 工作流定点到猫
			 * @param {Object} fName
			 */
			$scope.setbpmInitTab=function(fName){
				setTimeout(function(){
					if( $rootScope[fName].ctab!=null){
		            	var ctabArr =  $rootScope[fName].ctab.split(",");
		            	for(var k= 0; k<ctabArr.length;k++){
		            		$scope.gotoById(ctabArr[k]);
		            	}
	            	}
				},200);
				
			};
			
			
			$scope.showSwglSave=function(cRows){
				var flag=false;
				for(var cRow in cRows){
				  if(cRows[cRow]["jsrid"] == $scope.curUser.userid || $scope.curUser.userid=="admin"){
					flag=true;
					break;
				  }
				}
				return flag;
			}
			
			
		}
	]);

    asyncLoader.configure(app);
    module.exports = app;
});