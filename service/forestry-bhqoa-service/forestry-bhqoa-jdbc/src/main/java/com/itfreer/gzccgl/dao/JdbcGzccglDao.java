package com.itfreer.gzccgl.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzccgl.dao.GzccglDao;
import com.itfreer.gzccgl.entity.GzccglEntity;
import com.itfreer.gzccgl.entity.JdbcGzccglEntity;

/**
 * 定义出差管理 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzccglDao")
public class JdbcGzccglDao extends JdbcBaseDaoImp<GzccglEntity, JdbcGzccglEntity> implements GzccglDao {

}
