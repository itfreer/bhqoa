package com.itfreer.gzhlyda.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzhlyda.dao.GzhlydaDao;
import com.itfreer.gzhlyda.entity.GzhlydaEntity;
import com.itfreer.gzhlyda.entity.JdbcGzhlydaEntity;

/**
 * 定义护林员档案 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzhlydaDao")
public class JdbcGzhlydaDao extends JdbcBaseDaoImp<GzhlydaEntity, JdbcGzhlydaEntity> implements GzhlydaDao {

}
