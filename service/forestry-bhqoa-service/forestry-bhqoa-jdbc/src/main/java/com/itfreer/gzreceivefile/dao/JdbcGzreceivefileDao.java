package com.itfreer.gzreceivefile.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzreceivefile.dao.GzreceivefileDao;
import com.itfreer.gzreceivefile.entity.GzreceivefileEntity;
import com.itfreer.gzreceivefile.entity.JdbcGzreceivefileEntity;

/**
 * 定义收文管理 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzreceivefileDao")
public class JdbcGzreceivefileDao extends JdbcBaseDaoImp<GzreceivefileEntity, JdbcGzreceivefileEntity> implements GzreceivefileDao {

}
