package com.itfreer.gzqjgl.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzqjgl.dao.GzqjglDao;
import com.itfreer.gzqjgl.entity.GzqjglEntity;
import com.itfreer.gzqjgl.entity.JdbcGzqjglEntity;

/**
 * 定义请假管理 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzqjglDao")
public class JdbcGzqjglDao extends JdbcBaseDaoImp<GzqjglEntity, JdbcGzqjglEntity> implements GzqjglDao {

}
