package com.itfreer.gzvisitorreception.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzvisitorreception.entity.GzvisitorreceptionEntity;

/**
 * 定义访问接待管理实体
 */
 @Entity(name = "GZ_VISITOR_RECEPTION")
public class JdbcGzvisitorreceptionEntity implements JdbcBaseEntity<GzvisitorreceptionEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 来访单位
	 */
	@Column(name = "LFDW", length = 500, nullable = true)
	private String lfdw;
	
	
	/**
	 * 来访主题
	 */
	@Column(name = "LFZT", length = 500, nullable = true)
	private String lfzt;
	
	
	/**
	 * 来访人员
	 */
	@Column(name = "LFRY", length = 500, nullable = true)
	private String lfry;
	
	
	/**
	 * 来访人数
	 */
	@Column(name = "LFRS", length = 20, nullable = true)
	private Integer lfrs;
	
	/**
	 * 到访时间
	 */
	@Column(name = "DFSJ", nullable = true)
	private java.util.Date dfsj;
	
	
	/**
	 * 是否需要安排住宿
	 */
	@Column(name = "SFXYAPZS", length = 500, nullable = true)
	private String sfxyapzs;
	
	
	/**
	 * 男性人数
	 */
	@Column(name = "NXRS", length = 500, nullable = true)
	private String nxrs;
	
	
	/**
	 * 女性人数
	 */
	@Column(name = "WXRS", length = 500, nullable = true)
	private String wxrs;
	
	/**
	 * 到访结束时间
	 */
	@Column(name = "DFJSSJ", nullable = true)
	private java.util.Date dfjssj;
	
	
	/**
	 * 创建人ID
	 */
	@Column(name = "CRJID", length = 500, nullable = true)
	private String crjid;
	
	
	/**
	 * 创建人名称
	 */
	@Column(name = "CJRMC", length = 500, nullable = true)
	private String cjrmc;
	
	
	/**
	 * 领队联系电话
	 */
	@Column(name = "TEL", length = 500, nullable = true)
	private String tel;
	
	
	/**
	 * 考察内容
	 */
	@Column(name = "KCNR", length = 500, nullable = true)
	private String kcnr;
	
	
	/**
	 * 接待负责人ID
	 */
	@Column(name = "JDFZRID", length = 500, nullable = true)
	private String jdfzrid;
	
	
	/**
	 * 接待负责人名称
	 */
	@Column(name = "JDFZRMC", length = 500, nullable = true)
	private String jdfzrmc;
	
	
	/**
	 * 参与人
	 */
	@Column(name = "CYR", length = 500, nullable = true)
	private String cyr;
	
	
	/**
	 * 接待部门ID
	 */
	@Column(name = "JDBMID", length = 500, nullable = true)
	private String jdbmid;
	
	
	/**
	 * 接待部门名称
	 */
	@Column(name = "JDBMMC", length = 500, nullable = true)
	private String jdbmmc;
	
	
	/**
	 * 预约ID号
	 */
	@Column(name = "YYID", length = 500, nullable = true)
	private String yyid;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 来访单位
	 */
	public String getLfdw() {
		return lfdw;
	}

	/**
	 * 来访单位
	 */
	public void setLfdw(String value) {
		this.lfdw = value;
	}
	/**
	 * 来访主题
	 */
	public String getLfzt() {
		return lfzt;
	}

	/**
	 * 来访主题
	 */
	public void setLfzt(String value) {
		this.lfzt = value;
	}
	/**
	 * 来访人员
	 */
	public String getLfry() {
		return lfry;
	}

	/**
	 * 来访人员
	 */
	public void setLfry(String value) {
		this.lfry = value;
	}
	/**
	 * 来访人数
	 */
	public Integer getLfrs() {
		return lfrs;
	}

	/**
	 * 来访人数
	 */
	public void setLfrs(Integer value) {
		this.lfrs = value;
	}
	/**
	 * 到访时间
	 */
	public java.util.Date getDfsj() {
		return dfsj;
	}

	/**
	 * 到访时间
	 */
	public void setDfsj(java.util.Date value) {
		this.dfsj = value;
	}
	/**
	 * 是否需要安排住宿
	 */
	public String getSfxyapzs() {
		return sfxyapzs;
	}

	/**
	 * 是否需要安排住宿
	 */
	public void setSfxyapzs(String value) {
		this.sfxyapzs = value;
	}
	/**
	 * 男性人数
	 */
	public String getNxrs() {
		return nxrs;
	}

	/**
	 * 男性人数
	 */
	public void setNxrs(String value) {
		this.nxrs = value;
	}
	/**
	 * 女性人数
	 */
	public String getWxrs() {
		return wxrs;
	}

	/**
	 * 女性人数
	 */
	public void setWxrs(String value) {
		this.wxrs = value;
	}
	/**
	 * 到访结束时间
	 */
	public java.util.Date getDfjssj() {
		return dfjssj;
	}

	/**
	 * 到访结束时间
	 */
	public void setDfjssj(java.util.Date value) {
		this.dfjssj = value;
	}
	/**
	 * 创建人ID
	 */
	public String getCrjid() {
		return crjid;
	}

	/**
	 * 创建人ID
	 */
	public void setCrjid(String value) {
		this.crjid = value;
	}
	/**
	 * 创建人名称
	 */
	public String getCjrmc() {
		return cjrmc;
	}

	/**
	 * 创建人名称
	 */
	public void setCjrmc(String value) {
		this.cjrmc = value;
	}
	/**
	 * 领队联系电话
	 */
	public String getTel() {
		return tel;
	}

	/**
	 * 领队联系电话
	 */
	public void setTel(String value) {
		this.tel = value;
	}
	/**
	 * 考察内容
	 */
	public String getKcnr() {
		return kcnr;
	}

	/**
	 * 考察内容
	 */
	public void setKcnr(String value) {
		this.kcnr = value;
	}
	/**
	 * 接待负责人ID
	 */
	public String getJdfzrid() {
		return jdfzrid;
	}

	/**
	 * 接待负责人ID
	 */
	public void setJdfzrid(String value) {
		this.jdfzrid = value;
	}
	/**
	 * 接待负责人名称
	 */
	public String getJdfzrmc() {
		return jdfzrmc;
	}

	/**
	 * 接待负责人名称
	 */
	public void setJdfzrmc(String value) {
		this.jdfzrmc = value;
	}
	/**
	 * 参与人
	 */
	public String getCyr() {
		return cyr;
	}

	/**
	 * 参与人
	 */
	public void setCyr(String value) {
		this.cyr = value;
	}
	/**
	 * 接待部门ID
	 */
	public String getJdbmid() {
		return jdbmid;
	}

	/**
	 * 接待部门ID
	 */
	public void setJdbmid(String value) {
		this.jdbmid = value;
	}
	/**
	 * 接待部门名称
	 */
	public String getJdbmmc() {
		return jdbmmc;
	}

	/**
	 * 接待部门名称
	 */
	public void setJdbmmc(String value) {
		this.jdbmmc = value;
	}
	/**
	 * 预约ID号
	 */
	public String getYyid() {
		return yyid;
	}

	/**
	 * 预约ID号
	 */
	public void setYyid(String value) {
		this.yyid = value;
	}

	@Override
	public void from(GzvisitorreceptionEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzvisitorreceptionEntity toEntity() {
		GzvisitorreceptionEntity toEntity = new GzvisitorreceptionEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}