package com.itfreer.gzccsxry.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzccsxry.entity.GzccsxryEntity;

/**
 * 定义出差随行人员实体
 */
 @Entity(name = "GZ_CCSXRY")
public class JdbcGzccsxryEntity implements JdbcBaseEntity<GzccsxryEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	/**
	 * 出差管理记录id
	 */
	@Column(name = "CCGLID", length = 500, nullable = true)
	private String ccglid;
	
	
	/**
	 * 随行人员id
	 */
	@Column(name = "JSRID", length = 500, nullable = true)
	private String jsrid;
	
	
	/**
	 * 随行人员名称
	 */
	@Column(name = "JSRMC", length = 500, nullable = true)
	private String jsrmc;
	
	
	/**
	 * 机构名称
	 */
	@Column(name = "JSRJG", length = 500, nullable = true)
	private String jsrjg;
	
	
	/**
	 * 机构名称
	 */
	@Column(name = "JSRJGMC", length = 500, nullable = true)
	private String jsrjgmc;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 随行人员id
	 */
	public String getJsrid() {
		return jsrid;
	}

	/**
	 * 随行人员id
	 */
	public void setJsrid(String value) {
		this.jsrid = value;
	}
	/**
	 * 随行人员名称
	 */
	public String getJsrmc() {
		return jsrmc;
	}

	/**
	 * 随行人员名称
	 */
	public void setJsrmc(String value) {
		this.jsrmc = value;
	}
	/**
	 * 机构名称
	 */
	public String getJsrjg() {
		return jsrjg;
	}

	/**
	 * 机构名称
	 */
	public void setJsrjg(String value) {
		this.jsrjg = value;
	}
	/**
	 * 机构名称
	 */
	public String getJsrjgmc() {
		return jsrjgmc;
	}

	/**
	 * 机构名称
	 */
	public void setJsrjgmc(String value) {
		this.jsrjgmc = value;
	}
	
	public String getCcglid() {
		return ccglid;
	}

	public void setCcglid(String ccglid) {
		this.ccglid = ccglid;
	}

	@Override
	public void from(GzccsxryEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzccsxryEntity toEntity() {
		GzccsxryEntity toEntity = new GzccsxryEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}