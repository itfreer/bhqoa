package com.itfreer.gzpublishfile.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.springframework.beans.BeanUtils;

import com.itfreer.bpm.api.IJdbcBpmBaseEntity;
import com.itfreer.gzreceivefile.entity.GzreceivefileEntity;
import com.itfreer.gzreceivefile.entity.JdbcGzreceivefileEntity;

/**
 * 定义发文管理实体
 */
 @Entity(name = "GZ_PUBLISH_FILE")
public class JdbcGzpublishfileEntity implements 
IJdbcBpmBaseEntity<GzpublishfileEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 发文主题
	 */
	@Column(name = "FWZT", length = 500, nullable = true)
	private String fwzt;
	
	
	/**
	 * 发文说明
	 */
	@Column(name = "FWSM", length = 500, nullable = true)
	private String fwsm;
	
	/**
	 * 拟办意见
	 */
	@Column(name = "NBYJ", length = 500, nullable = true)
	private String nbyj;
	
	/**
	 * 负责人部门名称
	 */
	@Column(name = "FZRBMMC", length = 255, nullable = true)
	private String fzrbmmc;
	
	public String getFzrbmmc() {
		return fzrbmmc;
	}

	public void setFzrbmmc(String fzrbmmc) {
		this.fzrbmmc = fzrbmmc;
	}
	/**
	 * 创建人ID
	 */
	@Column(name = "CJRID", length = 500, nullable = true)
	private String cjrid;
	
	
	/**
	 * 创建人名称
	 */
	@Column(name = "CJRMC", length = 500, nullable = true)
	private String cjrmc;
	
	/**
	 * 创建时间
	 */
	@Column(name = "CJSJ", nullable = true)
	private java.util.Date cjsj;
	
	
	/**
	 * 附件
	 */
	@Column(name = "FJ", length = 500, nullable = true)
	private String fj;
	
	
	/**
	 * 部门ID
	 */
	@Column(name = "BMID", length = 500, nullable = true)
	private String bmid;
	
	
	/**
	 * 部门名称
	 */
	@Column(name = "BMMC", length = 500, nullable = true)
	private String bmmc;
	
	/**
	 * 分管局领导（用来区分姚局分管项目，还是王局分管项目）
	 */
	@Column(name = "FGJLD", length = 500, nullable = true)
	private String fgjld;
	
	
	/**
	 * 收件人ID
	 */
	@Column(name = "SJRID", length = 500, nullable = true)
	private String sjrid;
	
	
	/**
	 * 收件人名称
	 */
	@Column(name = "SJRMC", length = 500, nullable = true)
	private String sjrmc;
	
	/**
	 * 办理情况
	 */
	@Column(name = "BLQK", length = 500, nullable = true)
	private String blqk;
	
	public String getBlqk() {
		return blqk;
	}

	public void setBlqk(String blqk) {
		this.blqk = blqk;
	}
	/**
	 * 紧急等级
	 */
	@Column(name = "JJDJ", length = 500, nullable = true)
	private String jjdj;
	

	public String getJjdj() {
		return jjdj;
	}

	public void setJjdj(String jjdj) {
		this.jjdj = jjdj;
	}

	public String getFgjld() {
		return fgjld;
	}

	public void setFgjld(String fgjld) {
		this.fgjld = fgjld;
	}

	public String getSjrid() {
		return sjrid;
	}

	public void setSjrid(String sjrid) {
		this.sjrid = sjrid;
	}

	public String getSjrmc() {
		return sjrmc;
	}

	public void setSjrmc(String sjrmc) {
		this.sjrmc = sjrmc;
	}
	
	/**
	 * 收文主题
	 */
	@SuppressWarnings("deprecation")
	@org.hibernate.annotations.ForeignKey(name = "none")
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "FWID")
	@OrderBy("YDSJ ASC")
	private Set<JdbcGzreceivefileEntity> gzreceivefile = new LinkedHashSet<JdbcGzreceivefileEntity>();

	
	/**
	 * 收文主题
	 * @return
	 */
	public Set<JdbcGzreceivefileEntity> getGzreceivefile() {
		return gzreceivefile;
	}
	/**
	 * 收文主题
	 * @return
	 */
	public void setGzreceivefile(Set<JdbcGzreceivefileEntity> gzreceivefile) {
		this.gzreceivefile = gzreceivefile;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 发文主题
	 */
	public String getFwzt() {
		return fwzt;
	}

	/**
	 * 发文主题
	 */
	public void setFwzt(String value) {
		this.fwzt = value;
	}
	/**
	 * 发文说明
	 */
	public String getFwsm() {
		return fwsm;
	}

	/**
	 * 发文说明
	 */
	public void setFwsm(String value) {
		this.fwsm = value;
	}

	public String getNbyj() {
		return nbyj;
	}

	public void setNbyj(String nbyj) {
		this.nbyj = nbyj;
	}
	/**
	 * 创建人ID
	 */
	public String getCjrid() {
		return cjrid;
	}

	/**
	 * 创建人ID
	 */
	public void setCjrid(String value) {
		this.cjrid = value;
	}
	/**
	 * 创建人名称
	 */
	public String getCjrmc() {
		return cjrmc;
	}

	/**
	 * 创建人名称
	 */
	public void setCjrmc(String value) {
		this.cjrmc = value;
	}
	/**
	 * 创建时间
	 */
	public java.util.Date getCjsj() {
		return cjsj;
	}

	/**
	 * 创建时间
	 */
	public void setCjsj(java.util.Date value) {
		this.cjsj = value;
	}
	/**
	 * 附件
	 */
	public String getFj() {
		return fj;
	}

	/**
	 * 附件
	 */
	public void setFj(String value) {
		this.fj = value;
	}
	/**
	 * 部门ID
	 */
	public String getBmid() {
		return bmid;
	}

	/**
	 * 部门ID
	 */
	public void setBmid(String value) {
		this.bmid = value;
	}
	/**
	 * 部门名称
	 */
	public String getBmmc() {
		return bmmc;
	}

	/**
	 * 部门名称
	 */
	public void setBmmc(String value) {
		this.bmmc = value;
	}

	/*@Override
	public void from(GzpublishfileEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzpublishfileEntity toEntity() {
		GzpublishfileEntity toEntity = new GzpublishfileEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}*/
	
	@Override
	public void from(GzpublishfileEntity t) {
		BeanUtils.copyProperties(t, this);
		
		//项目人员表-start-
		Set<JdbcGzreceivefileEntity> jdbcgzreceivefilel = new LinkedHashSet<JdbcGzreceivefileEntity>();
		Set<GzreceivefileEntity> gzreceivefilel = t.getGzreceivefile();
		if (gzreceivefilel != null) {
			for (GzreceivefileEntity item : gzreceivefilel) {
				JdbcGzreceivefileEntity jItem = new JdbcGzreceivefileEntity();
				jItem.from(item);
				jdbcgzreceivefilel.add(jItem);
			}
		}
		this.setGzreceivefile(jdbcgzreceivefilel);
	}
	@Override
	public GzpublishfileEntity toEntity() {
		GzpublishfileEntity toEntity = new GzpublishfileEntity();
		BeanUtils.copyProperties(this, toEntity);
		
		//查询匹配条件-start-
		Set<GzreceivefileEntity> gzreceivefile = new LinkedHashSet<GzreceivefileEntity>();
		Set<JdbcGzreceivefileEntity> jdbcgzreceivefile = this.getGzreceivefile();
		
		if (jdbcgzreceivefile != null) {
			for (JdbcGzreceivefileEntity item : jdbcgzreceivefile) {
				gzreceivefile.add(item.toEntity());
			}
		}
		toEntity.setGzreceivefile(gzreceivefile);
		return toEntity;
	}
	
	@Column(name = "s_exeid")
	private String sexeid;

	@Override
	public String getProjectname() {
		return this.fwzt;
	}

	@Override
	public String getSexeid() {
		return this.sexeid;
	}
	
	/**
	 * 流程实例主键
	 */

	public void setSexeid(String sexeid) {
		this.sexeid=sexeid;
	}
	
	/**
	 * 主要负责人
	 */
	@Column(name = "zyfzrid",length=50,nullable=true)
	private String zyfzrid;
	
	/**
	 * 主要负责人
	 */
	@Column(name = "zyfzrname",length=50,nullable=true)
	private String zyfzrname;

	
	public String getZyfzrid() {
		return zyfzrid;
	}

	public void setZyfzrid(String zyfzrid) {
		this.zyfzrid = zyfzrid;
	}

	public String getZyfzrname() {
		return zyfzrname;
	}

	public void setZyfzrname(String zyfzrname) {
		this.zyfzrname = zyfzrname;
	}
	
	/**
	 * 来文单位
	 */
	@Column(name = "lwdw",length=500,nullable=true)
	public String lwdw;
	
	/**
	 * 来文编号
	 */
	@Column(name = "lwbh",length=500,nullable=true)
	public String lwbh;
	
	/**
	 * 收文时间
	 */
	@Column(name = "swsj",nullable=true)
	public Date swsj;
	
	/**
	 * 办理结果
	 */
	@Column(name = "bljg",nullable=true)
	public String bljg;

	public String getLwdw() {
		return lwdw;
	}

	public void setLwdw(String lwdw) {
		this.lwdw = lwdw;
	}

	public String getLwbh() {
		return lwbh;
	}

	public void setLwbh(String lwbh) {
		this.lwbh = lwbh;
	}

	public Date getSwsj() {
		return swsj;
	}

	public void setSwsj(Date swsj) {
		this.swsj = swsj;
	}

	public String getBljg() {
		return bljg;
	}

	public void setBljg(String bljg) {
		this.bljg = bljg;
	}
}