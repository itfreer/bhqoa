package com.itfreer.appupdatenet.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.appupdatenet.dao.AppupdatenetDao;
import com.itfreer.appupdatenet.entity.AppupdatenetEntity;
import com.itfreer.appupdatenet.entity.JdbcAppupdatenetEntity;

/**
 * 定义APP更新 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcAppupdatenetDao")
public class JdbcAppupdatenetDao extends JdbcBaseDaoImp<AppupdatenetEntity, JdbcAppupdatenetEntity> implements AppupdatenetDao {

}
