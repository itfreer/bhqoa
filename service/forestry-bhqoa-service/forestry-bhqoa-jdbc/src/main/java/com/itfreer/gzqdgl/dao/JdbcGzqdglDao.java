package com.itfreer.gzqdgl.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzqdgl.dao.GzqdglDao;
import com.itfreer.gzqdgl.entity.GzqdglEntity;
import com.itfreer.gzqdgl.entity.JdbcGzqdglEntity;

/**
 * 定义签到管理 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzqdglDao")
public class JdbcGzqdglDao extends JdbcBaseDaoImp<GzqdglEntity, JdbcGzqdglEntity> implements GzqdglDao {

}
