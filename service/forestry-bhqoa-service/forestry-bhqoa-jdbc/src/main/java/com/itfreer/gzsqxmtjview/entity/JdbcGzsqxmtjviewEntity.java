package com.itfreer.gzsqxmtjview.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzsqxmtjview.entity.GzsqxmtjviewEntity;

/**
 * 定义社区项目统计(视图)实体
 */
 @Entity(name = "GZ_SQXMTJ_VIEW")
public class JdbcGzsqxmtjviewEntity implements JdbcBaseEntity<GzsqxmtjviewEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	
	/**
	 * 项目状态
	 */
	@Column(name = "S_XMZT", length = 50, nullable = true)
	private String sxmzt;
	
	
	/**
	 * 项目年度
	 */
	@Id
	@Column(name = "NF", length = 500, nullable = true)
	private String nf;
	
	
	/**
	 * 项目月份
	 */
	@Column(name = "YF", length = 500, nullable = true)
	private String yf;
	
	/**
	 * 项目个数
	 */
	@Column(name = "XMGS", nullable = true)
	private Integer xmgs;
	
	/**
	 * 申请资金
	 */
	@Column(name = "SQZZJ", nullable = true)
	private Double sqzzj;
	
	/**
	 * 产生效益
	 */
	@Column(name = "CSZXY", nullable = true)
	private Double cszxy;
	

	/**
	 * 项目状态
	 */
	public String getSxmzt() {
		return sxmzt;
	}

	/**
	 * 项目状态
	 */
	public void setSxmzt(String value) {
		this.sxmzt = value;
	}
	/**
	 * 项目年度
	 */
	public String getNf() {
		return nf;
	}

	/**
	 * 项目年度
	 */
	public void setNf(String value) {
		this.nf = value;
	}
	/**
	 * 项目月份
	 */
	public String getYf() {
		return yf;
	}

	/**
	 * 项目月份
	 */
	public void setYf(String value) {
		this.yf = value;
	}
	/**
	 * 项目个数
	 */
	public Integer getXmgs() {
		return xmgs;
	}

	/**
	 * 项目个数
	 */
	public void setXmgs(Integer value) {
		this.xmgs = value;
	}
	/**
	 * 申请资金
	 */
	public Double getSqzzj() {
		return sqzzj;
	}

	/**
	 * 申请资金
	 */
	public void setSqzzj(Double value) {
		this.sqzzj = value;
	}
	/**
	 * 产生效益
	 */
	public Double getCszxy() {
		return cszxy;
	}

	/**
	 * 产生效益
	 */
	public void setCszxy(Double value) {
		this.cszxy = value;
	}

	@Override
	public void from(GzsqxmtjviewEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzsqxmtjviewEntity toEntity() {
		GzsqxmtjviewEntity toEntity = new GzsqxmtjviewEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}

	@Override
	public void setId(String arg0) {
		// TODO Auto-generated method stub
		
	}
}