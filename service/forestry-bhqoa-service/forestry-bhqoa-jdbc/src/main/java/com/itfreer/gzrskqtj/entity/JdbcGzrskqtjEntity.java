package com.itfreer.gzrskqtj.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzrskqtj.entity.GzrskqtjEntity;

/**
 * 定义考勤统计实体
 */
 @Entity(name = "GZ_RS_KQTJ")
public class JdbcGzrskqtjEntity implements JdbcBaseEntity<GzrskqtjEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 保护区名称
	 */
	@Column(name = "BHQMC", length = 500, nullable = true)
	private String bhqmc;
	
	
	/**
	 * 保护区ID
	 */
	@Column(name = "BHQID", length = 500, nullable = true)
	private String bhqid;
	
	
	/**
	 * 管理站名称
	 */
	@Column(name = "GLZMC", length = 500, nullable = true)
	private String glzmc;
	
	
	/**
	 * 管理站ID
	 */
	@Column(name = "GLZID", length = 500, nullable = true)
	private String glzid;
	
	
	/**
	 * 考勤年度
	 */
	@Column(name = "KQND", length = 500, nullable = true)
	private String kqnd;
	
	
	/**
	 * 考勤月份
	 */
	@Column(name = "KQYF", length = 500, nullable = true)
	private String kqyf;
	
	/**
	 * 制表时间
	 */
	@Column(name = "ZBSJ", nullable = true)
	private java.util.Date zbsj;
	
	
	/**
	 * 考勤人ID
	 */
	@Column(name = "KQRID", length = 500, nullable = true)
	private String kqrid;
	
	
	/**
	 * 考勤人名称
	 */
	@Column(name = "KQRMC", length = 500, nullable = true)
	private String kqrmc;
	
	/**
	 * 出勤总天数
	 */
	@Column(name = "CQZTS", nullable = true)
	private Double cqzts;
	
	/**
	 * 社区工作
	 */
	@Column(name = "SQGZ", nullable = true)
	private Double sqgz;
	
	/**
	 * 出差
	 */
	@Column(name = "CHUCHAI", nullable = true)
	private Double chuchai;
	
	/**
	 * 学习
	 */
	@Column(name = "XUEXI", nullable = true)
	private Double xuexi;
	
	/**
	 * 会议
	 */
	@Column(name = "HUIYI", nullable = true)
	private Double huiyi;
	
	/**
	 * 驻村
	 */
	@Column(name = "ZHUCUN", nullable = true)
	private Double zhucun;
	
	/**
	 * 科研
	 */
	@Column(name = "KEYAN", nullable = true)
	private Double keyan;
	
	/**
	 * 其他
	 */
	@Column(name = "QITA", nullable = true)
	private Double qita;
	
	/**
	 * 在站值班
	 */
	@Column(name = "ZAIZZB", nullable = true)
	private Double zaizzb;
	
	
	/**
	 * 备注
	 */
	@Column(name = "BEIZHU", length = 500, nullable = true)
	private String beizhu;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 保护区名称
	 */
	public String getBhqmc() {
		return bhqmc;
	}

	/**
	 * 保护区名称
	 */
	public void setBhqmc(String value) {
		this.bhqmc = value;
	}
	/**
	 * 保护区ID
	 */
	public String getBhqid() {
		return bhqid;
	}

	/**
	 * 保护区ID
	 */
	public void setBhqid(String value) {
		this.bhqid = value;
	}
	/**
	 * 管理站名称
	 */
	public String getGlzmc() {
		return glzmc;
	}

	/**
	 * 管理站名称
	 */
	public void setGlzmc(String value) {
		this.glzmc = value;
	}
	/**
	 * 管理站ID
	 */
	public String getGlzid() {
		return glzid;
	}

	/**
	 * 管理站ID
	 */
	public void setGlzid(String value) {
		this.glzid = value;
	}
	/**
	 * 考勤年度
	 */
	public String getKqnd() {
		return kqnd;
	}

	/**
	 * 考勤年度
	 */
	public void setKqnd(String value) {
		this.kqnd = value;
	}
	/**
	 * 考勤月份
	 */
	public String getKqyf() {
		return kqyf;
	}

	/**
	 * 考勤月份
	 */
	public void setKqyf(String value) {
		this.kqyf = value;
	}
	/**
	 * 制表时间
	 */
	public java.util.Date getZbsj() {
		return zbsj;
	}

	/**
	 * 制表时间
	 */
	public void setZbsj(java.util.Date value) {
		this.zbsj = value;
	}
	/**
	 * 考勤人ID
	 */
	public String getKqrid() {
		return kqrid;
	}

	/**
	 * 考勤人ID
	 */
	public void setKqrid(String value) {
		this.kqrid = value;
	}
	/**
	 * 考勤人名称
	 */
	public String getKqrmc() {
		return kqrmc;
	}

	/**
	 * 考勤人名称
	 */
	public void setKqrmc(String value) {
		this.kqrmc = value;
	}
	/**
	 * 出勤总天数
	 */
	public Double getCqzts() {
		return cqzts;
	}

	/**
	 * 出勤总天数
	 */
	public void setCqzts(Double value) {
		this.cqzts = value;
	}
	/**
	 * 社区工作
	 */
	public Double getSqgz() {
		return sqgz;
	}

	/**
	 * 社区工作
	 */
	public void setSqgz(Double value) {
		this.sqgz = value;
	}
	/**
	 * 出差
	 */
	public Double getChuchai() {
		return chuchai;
	}

	/**
	 * 出差
	 */
	public void setChuchai(Double value) {
		this.chuchai = value;
	}
	/**
	 * 学习
	 */
	public Double getXuexi() {
		return xuexi;
	}

	/**
	 * 学习
	 */
	public void setXuexi(Double value) {
		this.xuexi = value;
	}
	/**
	 * 会议
	 */
	public Double getHuiyi() {
		return huiyi;
	}

	/**
	 * 会议
	 */
	public void setHuiyi(Double value) {
		this.huiyi = value;
	}
	/**
	 * 驻村
	 */
	public Double getZhucun() {
		return zhucun;
	}

	/**
	 * 驻村
	 */
	public void setZhucun(Double value) {
		this.zhucun = value;
	}
	/**
	 * 科研
	 */
	public Double getKeyan() {
		return keyan;
	}

	/**
	 * 科研
	 */
	public void setKeyan(Double value) {
		this.keyan = value;
	}
	/**
	 * 其他
	 */
	public Double getQita() {
		return qita;
	}

	/**
	 * 其他
	 */
	public void setQita(Double value) {
		this.qita = value;
	}
	/**
	 * 在站值班
	 */
	public Double getZaizzb() {
		return zaizzb;
	}

	/**
	 * 在站值班
	 */
	public void setZaizzb(Double value) {
		this.zaizzb = value;
	}
	/**
	 * 备注
	 */
	public String getBeizhu() {
		return beizhu;
	}

	/**
	 * 备注
	 */
	public void setBeizhu(String value) {
		this.beizhu = value;
	}

	@Override
	public void from(GzrskqtjEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzrskqtjEntity toEntity() {
		GzrskqtjEntity toEntity = new GzrskqtjEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}