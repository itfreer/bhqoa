package com.itfreer.gzgzjjr.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzgzjjr.entity.GzgzjjrEntity;

/**
 * 定义工作节假日实体
 */
 @Entity(name = "GZ_GZJJR")
public class JdbcGzgzjjrEntity implements JdbcBaseEntity<GzgzjjrEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	
	/**
	 * id
	 */
	@Id
	@Column(name = "ID", length = 100, nullable = true)
	private String id;
	
	
	/**
	 * 类型
	 */
	@Column(name = "TYPE", length = 100, nullable = true)
	private String type;
	
	/**
	 * 日期
	 */
	@Column(name = "SPEDATE", nullable = true)
	private java.util.Date spedate;
	
	
	/**
	 * 备注
	 */
	@Column(name = "BZ", length = 100, nullable = true)
	private String bz;
	

	/**
	 * id
	 */
	public String getId() {
		return id;
	}

	/**
	 * id
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 类型
	 */
	public String getType() {
		return type;
	}

	/**
	 * 类型
	 */
	public void setType(String value) {
		this.type = value;
	}
	/**
	 * 日期
	 */
	public java.util.Date getSpedate() {
		return spedate;
	}

	/**
	 * 日期
	 */
	public void setSpedate(java.util.Date value) {
		this.spedate = value;
	}
	/**
	 * 备注
	 */
	public String getBz() {
		return bz;
	}

	/**
	 * 备注
	 */
	public void setBz(String value) {
		this.bz = value;
	}

	@Override
	public void from(GzgzjjrEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzgzjjrEntity toEntity() {
		GzgzjjrEntity toEntity = new GzgzjjrEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}