package com.itfreer.gzdeviceborrow.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzdeviceborrow.dao.GzdeviceborrowDao;
import com.itfreer.gzdeviceborrow.entity.GzdeviceborrowEntity;
import com.itfreer.gzdeviceborrow.entity.JdbcGzdeviceborrowEntity;

/**
 * 定义设备借还 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzdeviceborrowDao")
public class JdbcGzdeviceborrowDao extends JdbcBaseDaoImp<GzdeviceborrowEntity, JdbcGzdeviceborrowEntity> implements GzdeviceborrowDao {

}
