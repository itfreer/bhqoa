package com.itfreer.gzddtz.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzddtz.dao.GzddtzDao;
import com.itfreer.gzddtz.entity.GzddtzEntity;
import com.itfreer.gzddtz.entity.JdbcGzddtzEntity;

/**
 * 定义调度台账 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzddtzDao")
public class JdbcGzddtzDao extends JdbcBaseDaoImp<GzddtzEntity, JdbcGzddtzEntity> implements GzddtzDao {

}
