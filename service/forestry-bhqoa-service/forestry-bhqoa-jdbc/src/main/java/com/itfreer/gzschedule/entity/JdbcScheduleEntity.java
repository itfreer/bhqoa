package com.itfreer.gzschedule.entity;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzschedule.entity.ScheduleEntity;
import com.itfreer.gztask.entity.JdbcgztaskEntity;
import com.itfreer.gztask.entity.gztaskEntity;

/**
 * 定义日常安排实体
 */
@Entity(name = "GZ_SCHEDULE")
public class JdbcScheduleEntity implements JdbcBaseEntity<ScheduleEntity>, Serializable {
	
	private static final long serialVersionUID = 4673423560339517162L;
	
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;

	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}

	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;

	/**
	 * 参与人
	 */
	@Column(name = "CYRID", length = 500, nullable = true)
	private String cyrid;

	public String getCyrid() {
		return cyrid;
	}

	public void setCyrid(String cyrid) {
		this.cyrid = cyrid;
	}

	/**
	 * 参与人
	 */
	@Column(name = "CYR", length = 500, nullable = true)
	private String cyr;

	/**
	 * 开始时间
	 */
	@Column(name = "KSSJ", nullable = true)
	private java.util.Date kssj;

	/**
	 * 结束时间
	 */
	@Column(name = "JSSJ", nullable = true)
	private java.util.Date jssj;

	/**
	 * 创建人ID
	 */
	@Column(name = "CJRID", length = 500, nullable = true)
	private String cjrid;

	/**
	 * 创建人名称
	 */
	@Column(name = "CJRMC", length = 500, nullable = true)
	private String cjrmc;

	/**
	 * 创建时间
	 */
	@Column(name = "CJSJ", nullable = true)
	private java.util.Date cjsj;

	/**
	 * 组织机构ID
	 */
	@Column(name = "ZZJGID", length = 500, nullable = true)
	private String zzjgid;

	/**
	 * 组织机构名称
	 */
	@Column(name = "ZZJGMC", length = 500, nullable = true)
	private String zzjgmc;

	/**
	 * 部门ID
	 */
	@Column(name = "BMID", length = 500, nullable = true)
	private String bmid;

	/**
	 * 部门名称
	 */
	@Column(name = "BMMC", length = 500, nullable = true)
	private String bmmc;

	/**
	 * 项目ID
	 */
	@Column(name = "XMID", length = 500, nullable = true)
	private String xmid;

	/**
	 * 项目名称
	 */
	@Column(name = "XMMC", length = 500, nullable = true)
	private String xmmc;

	/**
	 * 事项说明
	 */
	@Column(name = "SXSM", length = 500, nullable = true)
	private String sxsm;

	/**
	 * 状态
	 */
	@Column(name = "ZT", length = 100, nullable = true)
	private String zt;

	/**
	 * 单位名称
	 */
	@Column(name = "DWMC", length = 500, nullable = true)
	private String dwmc;

	/**
	 * 负责人
	 */
	@Column(name = "FZR", length = 500, nullable = true)
	private String fzr;

	/**
	 * 负责人名称
	 */
	@Column(name = "FZRMC", length = 500, nullable = true)
	private String fzrmc;

	/**
	 * 完成时间
	 */
	@Column(name = "WCSJ", nullable = true)
	private java.util.Date wcsj;

	public java.util.Date getWcsj() {
		return wcsj;
	}

	public void setWcsj(java.util.Date wcsj) {
		this.wcsj = wcsj;
	}

	public String getFzrmc() {
		return fzrmc;
	}

	public void setFzrmc(String fzrmc) {
		this.fzrmc = fzrmc;
	}

	/**
	 * 主题
	 */
	@Column(name = "ZHUTI", length = 500, nullable = true)
	private String zhuti;

	public String getZhuti() {
		return zhuti;
	}

	public void setZhuti(String zhuti) {
		this.zhuti = zhuti;
	}

	/**
	 * 任务指派
	 */
	@SuppressWarnings("deprecation")
	@org.hibernate.annotations.ForeignKey(name = "none")
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "FWID")
	@OrderBy("QRSJ ASC")
	private Set<JdbcgztaskEntity> gztask = new LinkedHashSet<JdbcgztaskEntity>();

	public Set<JdbcgztaskEntity> getGztask() {
		return gztask;
	}

	public void setGztask(Set<JdbcgztaskEntity> gztask) {
		this.gztask = gztask;
	}

	public String getDwmc() {
		return dwmc;
	}

	public void setDwmc(String dwmc) {
		this.dwmc = dwmc;
	}

	public String getFzr() {
		return fzr;
	}

	public void setFzr(String fzr) {
		this.fzr = fzr;
	}

	public String getZt() {
		return zt;
	}

	public void setZt(String zt) {
		this.zt = zt;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}

	/**
	 * 参与人
	 */
	public String getCyr() {
		return cyr;
	}

	/**
	 * 参与人
	 */
	public void setCyr(String value) {
		this.cyr = value;
	}

	/**
	 * 开始时间
	 */
	public java.util.Date getKssj() {
		return kssj;
	}

	/**
	 * 开始时间
	 */
	public void setKssj(java.util.Date value) {
		this.kssj = value;
	}

	/**
	 * 结束时间
	 */
	public java.util.Date getJssj() {
		return jssj;
	}

	/**
	 * 结束时间
	 */
	public void setJssj(java.util.Date value) {
		this.jssj = value;
	}

	/**
	 * 创建人ID
	 */
	public String getCjrid() {
		return cjrid;
	}

	/**
	 * 创建人ID
	 */
	public void setCjrid(String value) {
		this.cjrid = value;
	}

	/**
	 * 创建人名称
	 */
	public String getCjrmc() {
		return cjrmc;
	}

	/**
	 * 创建人名称
	 */
	public void setCjrmc(String value) {
		this.cjrmc = value;
	}

	/**
	 * 创建时间
	 */
	public java.util.Date getCjsj() {
		return cjsj;
	}

	/**
	 * 创建时间
	 */
	public void setCjsj(java.util.Date value) {
		this.cjsj = value;
	}

	/**
	 * 组织机构ID
	 */
	public String getZzjgid() {
		return zzjgid;
	}

	/**
	 * 组织机构ID
	 */
	public void setZzjgid(String value) {
		this.zzjgid = value;
	}

	/**
	 * 组织机构名称
	 */
	public String getZzjgmc() {
		return zzjgmc;
	}

	/**
	 * 组织机构名称
	 */
	public void setZzjgmc(String value) {
		this.zzjgmc = value;
	}

	/**
	 * 部门ID
	 */
	public String getBmid() {
		return bmid;
	}

	/**
	 * 部门ID
	 */
	public void setBmid(String value) {
		this.bmid = value;
	}

	/**
	 * 部门名称
	 */
	public String getBmmc() {
		return bmmc;
	}

	/**
	 * 部门名称
	 */
	public void setBmmc(String value) {
		this.bmmc = value;
	}

	/**
	 * 项目ID
	 */
	public String getXmid() {
		return xmid;
	}

	/**
	 * 项目ID
	 */
	public void setXmid(String value) {
		this.xmid = value;
	}

	/**
	 * 项目名称
	 */
	public String getXmmc() {
		return xmmc;
	}

	/**
	 * 项目名称
	 */
	public void setXmmc(String value) {
		this.xmmc = value;
	}

	/**
	 * 事项说明
	 */
	public String getSxsm() {
		return sxsm;
	}

	/**
	 * 事项说明
	 */
	public void setSxsm(String value) {
		this.sxsm = value;
	}

	/*
	 * @Override public void from(ScheduleEntity t) { BeanUtils.copyProperties(t,
	 * this); }
	 * 
	 * @Override public ScheduleEntity toEntity() { ScheduleEntity toEntity = new
	 * ScheduleEntity(); BeanUtils.copyProperties(this, toEntity); return toEntity;
	 * }
	 */

	@Override
	public void from(ScheduleEntity t) {
		BeanUtils.copyProperties(t, this);

		// 项目人员表-start-
		Set<JdbcgztaskEntity> jdbcgztask = new LinkedHashSet<JdbcgztaskEntity>();
		Set<gztaskEntity> gztask = t.getGztask();
		if (gztask != null) {
			for (gztaskEntity item : gztask) {
				JdbcgztaskEntity jItem = new JdbcgztaskEntity();
				jItem.from(item);
				jdbcgztask.add(jItem);
			}
		}
		this.setGztask(jdbcgztask);
	}

	@Override
	public ScheduleEntity toEntity() {
		ScheduleEntity toEntity = new ScheduleEntity();
		BeanUtils.copyProperties(this, toEntity);

		// 查询匹配条件-start-
		Set<gztaskEntity> gztask = new LinkedHashSet<gztaskEntity>();
		Set<JdbcgztaskEntity> jdbcgztask = this.getGztask();

		if (jdbcgztask != null) {
			for (JdbcgztaskEntity item : jdbcgztask) {
				gztask.add(item.toEntity());
			}
		}
		toEntity.setGztask(gztask);
		return toEntity;
	}
}