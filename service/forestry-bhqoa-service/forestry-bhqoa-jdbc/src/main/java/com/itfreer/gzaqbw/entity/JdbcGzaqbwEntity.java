package com.itfreer.gzaqbw.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzaqbw.entity.GzaqbwEntity;

/**
 * 定义安全保卫实体
 */
 @Entity(name = "GZ_AQBW")
public class JdbcGzaqbwEntity implements JdbcBaseEntity<GzaqbwEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}

	/**
	 * 唯一编号
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	/**
	 * 记录时间
	 */
	@Column(name = "D_JLSJ", nullable = true)
	private java.util.Date djlsj;
	
	
	/**
	 * 值班人
	 */
	@Column(name = "S_ZBR", nullable = true)
	private String szbr;
	
	
	/**
	 * 巡查范围
	 */
	@Column(name = "S_XCFW", length = 500, nullable = true)
	private String sxcfw;
	
	
	/**
	 * 巡查时间段
	 */
	@Column(name = "S_XCSJD", length = 500, nullable = true)
	private String sxcsjd;
	
	
	/**
	 * 异常情况
	 */
	@Column(name = "S_YCQK", length = 500, nullable = true)
	private String sycqk;
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 记录时间
	 */
	public java.util.Date getDjlsj() {
		return djlsj;
	}

	/**
	 * 记录时间
	 */
	public void setDjlsj(java.util.Date value) {
		this.djlsj = value;
	}
	/**
	 * 值班人
	 */
	public String getSzbr() {
		return szbr;
	}

	/**
	 * 值班人
	 */
	public void setSzbr(String value) {
		this.szbr = value;
	}
	/**
	 * 巡查范围
	 */
	public String getSxcfw() {
		return sxcfw;
	}

	/**
	 * 巡查范围
	 */
	public void setSxcfw(String value) {
		this.sxcfw = value;
	}
	/**
	 * 巡查时间段
	 */
	public String getSxcsjd() {
		return sxcsjd;
	}

	/**
	 * 巡查时间段
	 */
	public void setSxcsjd(String value) {
		this.sxcsjd = value;
	}
	/**
	 * 异常情况
	 */
	public String getSycqk() {
		return sycqk;
	}

	/**
	 * 异常情况
	 */
	public void setSycqk(String value) {
		this.sycqk = value;
	}

	@Override
	public void from(GzaqbwEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzaqbwEntity toEntity() {
		GzaqbwEntity toEntity = new GzaqbwEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}