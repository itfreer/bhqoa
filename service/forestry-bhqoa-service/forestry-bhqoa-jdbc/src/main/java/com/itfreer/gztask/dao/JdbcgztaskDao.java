package com.itfreer.gztask.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gztask.dao.gztaskDao;
import com.itfreer.gztask.entity.gztaskEntity;
import com.itfreer.gztask.entity.JdbcgztaskEntity;

/**
 * 定义任务安排 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcgztaskDao")
public class JdbcgztaskDao extends JdbcBaseDaoImp<gztaskEntity, JdbcgztaskEntity> implements gztaskDao {

}
