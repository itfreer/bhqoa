package com.itfreer.gzwjryrqxk.entity;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzplanpersonnel.entity.GzplanpersonnelEntity;
import com.itfreer.gzplanpersonnel.entity.JdbcGzplanpersonnelEntity;
import com.itfreer.gzsqclqd.entity.GzsqclqdEntity;
import com.itfreer.gzsqclqd.entity.JdbcGzsqclqdEntity;
import com.itfreer.gzwjryrqxk.entity.GzwjryrqxkEntity;

/**
 * 定义外籍人员入区许可实体
 */
 @Entity(name = "GZ_WJRYRQXK")
public class JdbcGzwjryrqxkEntity implements JdbcBaseEntity<GzwjryrqxkEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	@Column(name = "s_exeid")
	private String sexeid;
	
	
	/**
	 * 申请编号
	 */
	@Column(name = "S_SQBH", length = 50, nullable = true)
	private String ssqbh;
	
	
	/**
	 * 单位名称
	 */
	@Column(name = "S_DWMC", length = 100, nullable = true)
	private String sdwmc;
	
	
	/**
	 * 项目名称
	 */
	@Column(name = "S_XMMC", length = 100, nullable = true)
	private String sxmmc;
	
	
	/**
	 * 是否有介绍信(函)
	 */
	@Column(name = "I_JSX", length = 20, nullable = true)
	private String ijsx;
	
	
	/**
	 * 提供活动方案情况
	 */
	@Column(name = "S_HDFAQK", length = 100, nullable = true)
	private String shdfaqk;
	
	
	/**
	 * 是否鉴定合作协议
	 */
	@Column(name = "I_JDHZXY", length = 20, nullable = true)
	private String ijdhzxy;
	
	
	/**
	 * 协议编号
	 */
	@Column(name = "S_XYBH", length = 100, nullable = true)
	private String sxybh;
	
	
	/**
	 * 带队负责人
	 */
	@Column(name = "S_DDFZR", length = 20, nullable = true)
	private String sddfzr;
	
	
	/**
	 * 职务
	 */
	@Column(name = "S_ZW", length = 20, nullable = true)
	private String szw;
	
	
	/**
	 * 联系电话
	 */
	@Column(name = "S_LXDH", length = 50, nullable = true)
	private String slxdh;
	
	
	/**
	 * 人数
	 */
	@Column(name = "S_RS", length = 20, nullable = true)
	private String srs;
	
	/**
	 * 来访时间
	 */
	@Column(name = "D_LFSJ", nullable = true)
	private java.util.Date dlfsj;
	
	
	/**
	 * 活动计划时限
	 */
	@Column(name = "S_HDJHSX", length = 100, nullable = true)
	private String shdjhsx;
	
	
	/**
	 * 项目简介
	 */
	@Column(name = "S_XMJJ", length = 500, nullable = true)
	private String sxmjj;
	
	
	/**
	 * 部门审批意见
	 */
	@Column(name = "S_BMSPYJ", length = 500, nullable = true)
	private String sbmspyj;
	
	/**
	 * 部门审批时间
	 */
	@Column(name = "D_BMSPSJ", nullable = true)
	private java.util.Date dbmspsj;
	
	
	/**
	 * 部门审批人
	 */
	@Column(name = "S_BMSPR", length = 50, nullable = true)
	private String sbmspr;
	
	
	/**
	 * 单位审批意见
	 */
	@Column(name = "S_DWSPYJ", length = 500, nullable = true)
	private String sdwspyj;
	
	/**
	 * 单位审批时间
	 */
	@Column(name = "D_DWSPSJ", nullable = true)
	private java.util.Date ddwspsj;
	
	
	/**
	 * 单位审批人
	 */
	@Column(name = "S_DWSPR", length = 50, nullable = true)
	private String sdwspr;
	
	
	/**
	 * 项目编号
	 */
	@Column(name = "S_XMBH", length = 100, nullable = true)
	private String sxmbh;
	
	/**
	 * 外来项目类型
	 */
	@Column(name = "WLXMLX", length = 100, nullable = true)
	private String wlxmlx;
	
	/**
	 * 法定代表人名称
	 */
	@Column(name = "FDDBRMC", length = 100, nullable = true)
	private String fddbrmc;
	
	/**
	 * 地址
	 */
	@Column(name = "SQDWDZ", length = 100, nullable = true)
	private String sqdwdz;
	
	/**
	 * 联系电话
	 */
	@Column(name = "SQDWLXDH", length = 100, nullable = true)
	private String sqdwlxdh;
	
	/**
	 * 邮编
	 */
	@Column(name = "SQDWLXYB", length = 100, nullable = true)
	private String sqdwlxyb;
	
	/**
	 * 申请时间
	 */
	@Column(name = "XZXKSQSJ", nullable = true)
	private java.util.Date xzxksqsj;
	
	/**
	 * 行政许可申请事项
	 */
	@Column(name = "XZXKSQSX", length = 500, nullable = true)
	private String xzxksqsx;
	
	/**
	 * 受理声明
	 */
	@Column(name = "SLSM", length = 100, nullable = true)
	private String slsm;
	
	/**
	 * 受理序号
	 */
	@Column(name = "SLXH", length = 100, nullable = true)
	private String slxh;
	
	/**
	 * 受理编号
	 */
	@Column(name = "SLBH", length = 100, nullable = true)
	private String slbh;
	
	/**
	 * 行政许可审查情况
	 */
	@Column(name = "XZXKSCQK", length = 500, nullable = true)
	private String xzxkscqk;
	
	/**
	 * 承办人意见
	 */
	@Column(name = "CBRYJ", length = 500, nullable = true)
	private String cbryj;
	
	/**
	 * 审批时间
	 */
	@Column(name = "CBRSJ", nullable = true)
	private java.util.Date cbrsj;
	
	/**
	 * 审批人
	 */
	@Column(name = "CBRMC", length = 500, nullable = true)
	private String cbrmc;
	
	/**
	 * 委托代理人姓名
	 */
	@Column(name = "WTDLRXM", length = 500, nullable = true)
	private String wtdlrxm;
	
	/**
	 * 委托代理人身份证号
	 */
	@Column(name = "WTDLRSFZH", length = 500, nullable = true)
	private String wtdlrsfzh;
	
	/**
	 * 委托代理人住址
	 */
	@Column(name = "WTDLRZZ", length = 500, nullable = true)
	private String wtdlrzz;
	
	/**
	 * 委托代理人电话
	 */
	@Column(name = "WTDLRDH", length = 500, nullable = true)
	private String wtdlrdh;
	
	/**
	 * 承办人审批签字
	 */
	@Column(name = "chrspqz", length = 500, nullable = true)
	private String chrspqz;
	
	/**
	 * 科研科审批签字
	 */
	@Column(name = "kyksqqz", length = 500, nullable = true)
	private String kyksqqz;
	
	/**
	 * 办公室审批签字
	 */
	@Column(name = "bgssqqz", length = 500, nullable = true)
	private String bgssqqz;
	
	/**
	 * 申请书附件
	 */
	@Column(name = "FJ_SQS", length = 255, nullable = true)
	private String fjsqs;
	
	/**
	 * 申请材料清单附件
	 */
	@Column(name = "FJ_SQCLQD", length = 255, nullable = true)
	private String fjsqclqd;
	
	/**
	 * 受理凭证附件
	 */
	@Column(name = "FJ_SLPZ", length = 255, nullable = true)
	private String fjslpz;
	
	/**
	 * 受理审批表附件
	 */
	@Column(name = "FJ_SLSPB", length = 255, nullable = true)
	private String fjslspb;
	
	/**
	 * 决定审批表附件
	 */
	@Column(name = "FJ_JDSPB", length = 255, nullable = true)
	private String fjjdspb;
	
	/**
	 * 决定书附件
	 */
	@Column(name = "FJ_JDS", length = 255, nullable = true)
	private String fjjds;
	
	/**
	 * 办结报告附件
	 */
	@Column(name = "FJ_BJBG", length = 255, nullable = true)
	private String fjbjbg;
	
	public String getFjsqs() {
		return fjsqs;
	}

	public void setFjsqs(String fjsqs) {
		this.fjsqs = fjsqs;
	}
	
	public String getFjsqclqd() {
		return fjsqclqd;
	}

	public void setFjsqclqd(String fjsqclqd) {
		this.fjsqclqd = fjsqclqd;
	}
	
	public String getFjslpz() {
		return fjslpz;
	}

	public void setFjslpz(String fjslpz) {
		this.fjslpz = fjslpz;
	}
	
	public String getFjslspb() {
		return fjslspb;
	}

	public void setFjslspb(String fjslspb) {
		this.fjslspb = fjslspb;
	}
	
	public String getFjjdspb() {
		return fjjdspb;
	}

	public void setFjjdspb(String fjjdspb) {
		this.fjjdspb = fjjdspb;
	}
	
	public String getFjjds() {
		return fjjds;
	}

	public void setFjjds(String fjjds) {
		this.fjjds = fjjds;
	}
	
	public String getFjbjbg() {
		return fjbjbg;
	}

	public void setFjbjbg(String fjbjbg) {
		this.fjbjbg = fjbjbg;
	}
	
	public String getChrspqz() {
		return chrspqz;
	}

	public void setChrspqz(String chrspqz) {
		this.chrspqz = chrspqz;
	}

	public String getKyksqqz() {
		return kyksqqz;
	}

	public void setKyksqqz(String kyksqqz) {
		this.kyksqqz = kyksqqz;
	}

	public String getBgssqqz() {
		return bgssqqz;
	}

	public void setBgssqqz(String bgssqqz) {
		this.bgssqqz = bgssqqz;
	}

	public String getFddbrmc() {
		return fddbrmc;
	}

	public void setFddbrmc(String fddbrmc) {
		this.fddbrmc = fddbrmc;
	}

	public String getSqdwdz() {
		return sqdwdz;
	}

	public void setSqdwdz(String sqdwdz) {
		this.sqdwdz = sqdwdz;
	}

	public String getSqdwlxdh() {
		return sqdwlxdh;
	}

	public void setSqdwlxdh(String sqdwlxdh) {
		this.sqdwlxdh = sqdwlxdh;
	}

	public String getSqdwlxyb() {
		return sqdwlxyb;
	}

	public void setSqdwlxyb(String sqdwlxyb) {
		this.sqdwlxyb = sqdwlxyb;
	}

	public java.util.Date getXzxksqsj() {
		return xzxksqsj;
	}

	public void setXzxksqsj(java.util.Date xzxksqsj) {
		this.xzxksqsj = xzxksqsj;
	}

	public String getXzxksqsx() {
		return xzxksqsx;
	}

	public void setXzxksqsx(String xzxksqsx) {
		this.xzxksqsx = xzxksqsx;
	}

	public String getSlsm() {
		return slsm;
	}

	public void setSlsm(String slsm) {
		this.slsm = slsm;
	}

	public String getSlxh() {
		return slxh;
	}

	public void setSlxh(String slxh) {
		this.slxh = slxh;
	}

	public String getSlbh() {
		return slbh;
	}

	public void setSlbh(String slbh) {
		this.slbh = slbh;
	}

	public String getXzxkscqk() {
		return xzxkscqk;
	}

	public void setXzxkscqk(String xzxkscqk) {
		this.xzxkscqk = xzxkscqk;
	}

	public String getCbryj() {
		return cbryj;
	}

	public void setCbryj(String cbryj) {
		this.cbryj = cbryj;
	}

	public java.util.Date getCbrsj() {
		return cbrsj;
	}

	public void setCbrsj(java.util.Date cbrsj) {
		this.cbrsj = cbrsj;
	}

	public String getCbrmc() {
		return cbrmc;
	}

	public void setCbrmc(String cbrmc) {
		this.cbrmc = cbrmc;
	}

	public String getWtdlrxm() {
		return wtdlrxm;
	}

	public void setWtdlrxm(String wtdlrxm) {
		this.wtdlrxm = wtdlrxm;
	}

	public String getWtdlrsfzh() {
		return wtdlrsfzh;
	}

	public void setWtdlrsfzh(String wtdlrsfzh) {
		this.wtdlrsfzh = wtdlrsfzh;
	}

	public String getWtdlrzz() {
		return wtdlrzz;
	}

	public void setWtdlrzz(String wtdlrzz) {
		this.wtdlrzz = wtdlrzz;
	}

	public String getWtdlrdh() {
		return wtdlrdh;
	}

	public void setWtdlrdh(String wtdlrdh) {
		this.wtdlrdh = wtdlrdh;
	}

	public String getWlxmlx() {
		return wlxmlx;
	}

	public void setWlxmlx(String wlxmlx) {
		this.wlxmlx = wlxmlx;
	}

	public String getSxmbh() {
		return sxmbh;
	}

	public void setSxmbh(String sxmbh) {
		this.sxmbh = sxmbh;
	}

	public String getXmid() {
		return xmid;
	}

	public void setXmid(String xmid) {
		this.xmid = xmid;
	}

	/**
	 * 项目id
	 */
	@Column(name = "XMID", length = 100, nullable = true)
	private String xmid;
	
	
	/**
	 * 附件
	 */
	@Column(name = "FJ", length = 200, nullable = true)
	private String fj;
	
	public String getFj() {
		return fj;
	}

	public void setFj(String fj) {
		this.fj = fj;
	}

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 申请编号
	 */
	public String getSsqbh() {
		return ssqbh;
	}

	/**
	 * 申请编号
	 */
	public void setSsqbh(String value) {
		this.ssqbh = value;
	}
	/**
	 * 单位名称
	 */
	public String getSdwmc() {
		return sdwmc;
	}

	/**
	 * 单位名称
	 */
	public void setSdwmc(String value) {
		this.sdwmc = value;
	}
	/**
	 * 项目名称
	 */
	public String getSxmmc() {
		return sxmmc;
	}

	/**
	 * 项目名称
	 */
	public void setSxmmc(String value) {
		this.sxmmc = value;
	}
	/**
	 * 是否有介绍信(函)
	 */
	public String getIjsx() {
		return ijsx;
	}

	/**
	 * 是否有介绍信(函)
	 */
	public void setIjsx(String value) {
		this.ijsx = value;
	}
	/**
	 * 提供活动方案情况
	 */
	public String getShdfaqk() {
		return shdfaqk;
	}

	/**
	 * 提供活动方案情况
	 */
	public void setShdfaqk(String value) {
		this.shdfaqk = value;
	}
	/**
	 * 是否鉴定合作协议
	 */
	public String getIjdhzxy() {
		return ijdhzxy;
	}

	/**
	 * 是否鉴定合作协议
	 */
	public void setIjdhzxy(String value) {
		this.ijdhzxy = value;
	}
	/**
	 * 协议编号
	 */
	public String getSxybh() {
		return sxybh;
	}

	/**
	 * 协议编号
	 */
	public void setSxybh(String value) {
		this.sxybh = value;
	}
	/**
	 * 带队负责人
	 */
	public String getSddfzr() {
		return sddfzr;
	}

	/**
	 * 带队负责人
	 */
	public void setSddfzr(String value) {
		this.sddfzr = value;
	}
	/**
	 * 职务
	 */
	public String getSzw() {
		return szw;
	}

	/**
	 * 职务
	 */
	public void setSzw(String value) {
		this.szw = value;
	}
	/**
	 * 联系电话
	 */
	public String getSlxdh() {
		return slxdh;
	}

	/**
	 * 联系电话
	 */
	public void setSlxdh(String value) {
		this.slxdh = value;
	}
	/**
	 * 人数
	 */
	public String getSrs() {
		return srs;
	}

	/**
	 * 人数
	 */
	public void setSrs(String value) {
		this.srs = value;
	}
	/**
	 * 来访时间
	 */
	public java.util.Date getDlfsj() {
		return dlfsj;
	}

	/**
	 * 来访时间
	 */
	public void setDlfsj(java.util.Date value) {
		this.dlfsj = value;
	}
	/**
	 * 活动计划时限
	 */
	public String getShdjhsx() {
		return shdjhsx;
	}

	/**
	 * 活动计划时限
	 */
	public void setShdjhsx(String value) {
		this.shdjhsx = value;
	}
	/**
	 * 项目简介
	 */
	public String getSxmjj() {
		return sxmjj;
	}

	/**
	 * 项目简介
	 */
	public void setSxmjj(String value) {
		this.sxmjj = value;
	}
	/**
	 * 部门审批意见
	 */
	public String getSbmspyj() {
		return sbmspyj;
	}

	/**
	 * 部门审批意见
	 */
	public void setSbmspyj(String value) {
		this.sbmspyj = value;
	}
	/**
	 * 部门审批时间
	 */
	public java.util.Date getDbmspsj() {
		return dbmspsj;
	}

	/**
	 * 部门审批时间
	 */
	public void setDbmspsj(java.util.Date value) {
		this.dbmspsj = value;
	}
	/**
	 * 部门审批人
	 */
	public String getSbmspr() {
		return sbmspr;
	}

	/**
	 * 部门审批人
	 */
	public void setSbmspr(String value) {
		this.sbmspr = value;
	}
	/**
	 * 单位审批意见
	 */
	public String getSdwspyj() {
		return sdwspyj;
	}

	/**
	 * 单位审批意见
	 */
	public void setSdwspyj(String value) {
		this.sdwspyj = value;
	}
	/**
	 * 单位审批时间
	 */
	public java.util.Date getDdwspsj() {
		return ddwspsj;
	}

	/**
	 * 单位审批时间
	 */
	public void setDdwspsj(java.util.Date value) {
		this.ddwspsj = value;
	}
	/**
	 * 单位审批人
	 */
	public String getSdwspr() {
		return sdwspr;
	}

	/**
	 * 单位审批人
	 */
	public void setSdwspr(String value) {
		this.sdwspr = value;
	}
	
	

	/*@Override
	public void from(GzwjryrqxkEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzwjryrqxkEntity toEntity() {
		GzwjryrqxkEntity toEntity = new GzwjryrqxkEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}*/
	
	public String getSexeid() {
		return sexeid;
	}

	public void setSexeid(String sexeid) {
		this.sexeid = sexeid;
	}

	/**
	 * 项目人员
	 */
	@SuppressWarnings("deprecation")
	@org.hibernate.annotations.ForeignKey(name = "none")
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "XMID")
	@OrderBy("S_JHRWBH ASC")
	private Set<JdbcGzplanpersonnelEntity> gzplanpersonnel = new LinkedHashSet<JdbcGzplanpersonnelEntity>();
	
	/**
	 * 项目人员
	 * @return
	 */
	public Set<JdbcGzplanpersonnelEntity> getGzplanpersonnel() {
		return gzplanpersonnel;
	}
	/**
	 * 项目人员
	 * @return
	 */
	public void setGzplanpersonnel(Set<JdbcGzplanpersonnelEntity> gzplanpersonnel) {
		this.gzplanpersonnel = gzplanpersonnel;
	}
	
	
	/**
	 * 申请材料清单
	 */
	@SuppressWarnings("deprecation")
	@org.hibernate.annotations.ForeignKey(name = "none")
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "XMID")
	@OrderBy("ID")
	private Set<JdbcGzsqclqdEntity> gzsqclqd = new LinkedHashSet<JdbcGzsqclqdEntity>();
	
	public Set<JdbcGzsqclqdEntity> getGzsqclqd() {
		return gzsqclqd;
	}

	public void setGzsqclqd(Set<JdbcGzsqclqdEntity> gzsqclqd) {
		this.gzsqclqd = gzsqclqd;
	}

	@Override
	public void from(GzwjryrqxkEntity t) {
		BeanUtils.copyProperties(t, this);
		
		//项目人员表-start-
		Set<JdbcGzplanpersonnelEntity> jdbcgzplanpersonnel = new LinkedHashSet<JdbcGzplanpersonnelEntity>();
		Set<GzplanpersonnelEntity> gzplanpersonnel = t.getGzplanpersonnel();
		if (gzplanpersonnel != null) {
			for (GzplanpersonnelEntity item : gzplanpersonnel) {
				JdbcGzplanpersonnelEntity jItem = new JdbcGzplanpersonnelEntity();
				jItem.from(item);
				jdbcgzplanpersonnel.add(jItem);
			}
		}
		this.setGzplanpersonnel(jdbcgzplanpersonnel);
		//项目人员表-end-
		
		//申请材料清单表-start-
		Set<JdbcGzsqclqdEntity> jdbcGzsqclqd = new LinkedHashSet<JdbcGzsqclqdEntity>();
		Set<GzsqclqdEntity> gzsqclqd = t.getGzsqclqd();
		if (gzsqclqd != null) {
			for (GzsqclqdEntity item : gzsqclqd) {
				JdbcGzsqclqdEntity jItem = new JdbcGzsqclqdEntity();
				jItem.from(item);
				jdbcGzsqclqd.add(jItem);
			}
		}
		this.setGzsqclqd(jdbcGzsqclqd);
		//项目人员表-end-
	}
	@Override
	public GzwjryrqxkEntity toEntity() {
		GzwjryrqxkEntity toEntity = new GzwjryrqxkEntity();
		BeanUtils.copyProperties(this, toEntity);
		
		//查询匹配条件-start-
		Set<GzplanpersonnelEntity> gzplanpersonnel = new LinkedHashSet<GzplanpersonnelEntity>();
		Set<JdbcGzplanpersonnelEntity> jdbcgzplanpersonnel = this.getGzplanpersonnel();
		
		if (jdbcgzplanpersonnel != null) {
			for (JdbcGzplanpersonnelEntity item : jdbcgzplanpersonnel) {
				gzplanpersonnel.add(item.toEntity());
			}
		}
		toEntity.setGzplanpersonnel(gzplanpersonnel);
		
		//申请材料清单-start-
		Set<GzsqclqdEntity> gzsqclqd = new LinkedHashSet<GzsqclqdEntity>();
		Set<JdbcGzsqclqdEntity> jdbcGzsqclqd = this.getGzsqclqd();
		
		if (jdbcGzsqclqd != null) {
			for (JdbcGzsqclqdEntity item : jdbcGzsqclqd) {
				gzsqclqd.add(item.toEntity());
			}
		}
		toEntity.setGzsqclqd(gzsqclqd);
		
		return toEntity;
	}
}