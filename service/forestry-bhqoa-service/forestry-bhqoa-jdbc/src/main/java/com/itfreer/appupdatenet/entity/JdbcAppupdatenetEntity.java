package com.itfreer.appupdatenet.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.appupdatenet.entity.AppupdatenetEntity;

/**
 * 定义APP更新实体
 */
 @Entity(name = "APPUPDATE_NET")
public class JdbcAppupdatenetEntity implements JdbcBaseEntity<AppupdatenetEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 255, nullable = true)
	private String id;
	
	
	/**
	 * 版本
	 */
	@Column(name = "VERSION", length = 255, nullable = true)
	private String version;
	
	/**
	 * 版本编号
	 */
	@Column(name = "NUM_VERSION", nullable = true)
	private Integer numversion;
	
	
	/**
	 * 更新内容
	 */
	@Column(name = "CONTENT", length = 255, nullable = true)
	private String content;
	
	
	/**
	 * 下载地址
	 */
	@Column(name = "URL", length = 255, nullable = true)
	private String url;
	
	/**
	 * 创建时间
	 */
	@Column(name = "CREATE_TIME", nullable = true)
	private java.util.Date createtime;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 版本
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * 版本
	 */
	public void setVersion(String value) {
		this.version = value;
	}
	/**
	 * 版本编号
	 */
	public Integer getNumversion() {
		return numversion;
	}

	/**
	 * 版本编号
	 */
	public void setNumversion(Integer value) {
		this.numversion = value;
	}
	/**
	 * 更新内容
	 */
	public String getContent() {
		return content;
	}

	/**
	 * 更新内容
	 */
	public void setContent(String value) {
		this.content = value;
	}
	/**
	 * 下载地址
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * 下载地址
	 */
	public void setUrl(String value) {
		this.url = value;
	}
	/**
	 * 创建时间
	 */
	public java.util.Date getCreatetime() {
		return createtime;
	}

	/**
	 * 创建时间
	 */
	public void setCreatetime(java.util.Date value) {
		this.createtime = value;
	}

	@Override
	public void from(AppupdatenetEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public AppupdatenetEntity toEntity() {
		AppupdatenetEntity toEntity = new AppupdatenetEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}