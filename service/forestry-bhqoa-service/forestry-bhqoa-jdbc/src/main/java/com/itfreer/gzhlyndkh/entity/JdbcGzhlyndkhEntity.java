package com.itfreer.gzhlyndkh.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzhlyndkh.entity.GzhlyndkhEntity;

/**
 * 定义护林员年度考核实体
 */
 @Entity(name = "GZ_HLY_ND_KH")
public class JdbcGzhlyndkhEntity implements JdbcBaseEntity<GzhlyndkhEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 保护区名称
	 */
	@Column(name = "BHQMC", length = 500, nullable = true)
	private String bhqmc;
	
	
	/**
	 * 保护区ID
	 */
	@Column(name = "BHQID", length = 500, nullable = true)
	private String bhqid;
	
	
	/**
	 * 管理站名称
	 */
	@Column(name = "GLZMC", length = 500, nullable = true)
	private String glzmc;
	
	
	/**
	 * 管理站ID
	 */
	@Column(name = "GLZID", length = 500, nullable = true)
	private String glzid;
	
	
	/**
	 * 负责人名称
	 */
	@Column(name = "FZRMC", length = 500, nullable = true)
	private String fzrmc;
	
	
	/**
	 * 负责人ID
	 */
	@Column(name = "FZRID", length = 500, nullable = true)
	private String fzrid;
	
	
	/**
	 * 考核年度
	 */
	@Column(name = "KHND", length = 500, nullable = true)
	private String khnd;
	
	/**
	 * 制表日期
	 */
	@Column(name = "ZBRQ", nullable = true)
	private java.util.Date zbrq;
	
	/**
	 * 出勤天数_巡山
	 */
	@Column(name = "CQTSXS", nullable = true)
	private Double cqtsxs;
	
	/**
	 * 出勤天数_在岗
	 */
	@Column(name = "CQTSZG", nullable = true)
	private Double cqtszg;
	
	/**
	 * 出勤天数_事假
	 */
	@Column(name = "CQTSSJ", nullable = true)
	private Double cqtssj;
	
	/**
	 * 出勤天数_病假
	 */
	@Column(name = "CQTSBJ", nullable = true)
	private Double cqtsbj;
	
	/**
	 * 出勤天数_旷工
	 */
	@Column(name = "CQTSKG", nullable = true)
	private Double cqtskg;
	
	/**
	 * 目标量化考核评分结果
	 */
	@Column(name = "MBLHKHPFJG", nullable = true)
	private Double mblhkhpfjg;
	
	/**
	 * 目标考核得分90%
	 */
	@Column(name = "MBLHKHPF90", nullable = true)
	private Double mblhkhpf90;
	
	/**
	 * 民主测评分
	 */
	@Column(name = "MZCPF", nullable = true)
	private Double mzcpf;
	
	/**
	 * 明主测评分10%
	 */
	@Column(name = "MZCPF10", nullable = true)
	private Double mzcpf10;
	
	/**
	 * 优秀护林员加分
	 */
	@Column(name = "YXHLYJF", nullable = true)
	private Double yxhlyjf;
	
	/**
	 * 年度综合考核得分
	 */
	@Column(name = "NDZHKHDF", nullable = true)
	private Double ndzhkhdf;
	
	
	/**
	 * 管理站工资兑现意见
	 */
	@Column(name = "GLZGZDXYJ", length = 500, nullable = true)
	private String glzgzdxyj;
	
	
	/**
	 * 管理局工资兑现意见
	 */
	@Column(name = "GLJGZDXYJ", length = 500, nullable = true)
	private String gljgzdxyj;
	
	
	/**
	 * 被考核人
	 */
	@Column(name = "BKHRMC", length = 500, nullable = true)
	private String bkhrmc;
	
	
	/**
	 * 被考核人ID
	 */
	@Column(name = "BKHRID", length = 500, nullable = true)
	private String bkhrid;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 保护区名称
	 */
	public String getBhqmc() {
		return bhqmc;
	}

	/**
	 * 保护区名称
	 */
	public void setBhqmc(String value) {
		this.bhqmc = value;
	}
	/**
	 * 保护区ID
	 */
	public String getBhqid() {
		return bhqid;
	}

	/**
	 * 保护区ID
	 */
	public void setBhqid(String value) {
		this.bhqid = value;
	}
	/**
	 * 管理站名称
	 */
	public String getGlzmc() {
		return glzmc;
	}

	/**
	 * 管理站名称
	 */
	public void setGlzmc(String value) {
		this.glzmc = value;
	}
	/**
	 * 管理站ID
	 */
	public String getGlzid() {
		return glzid;
	}

	/**
	 * 管理站ID
	 */
	public void setGlzid(String value) {
		this.glzid = value;
	}
	/**
	 * 负责人名称
	 */
	public String getFzrmc() {
		return fzrmc;
	}

	/**
	 * 负责人名称
	 */
	public void setFzrmc(String value) {
		this.fzrmc = value;
	}
	/**
	 * 负责人ID
	 */
	public String getFzrid() {
		return fzrid;
	}

	/**
	 * 负责人ID
	 */
	public void setFzrid(String value) {
		this.fzrid = value;
	}
	/**
	 * 考核年度
	 */
	public String getKhnd() {
		return khnd;
	}

	/**
	 * 考核年度
	 */
	public void setKhnd(String value) {
		this.khnd = value;
	}
	/**
	 * 制表日期
	 */
	public java.util.Date getZbrq() {
		return zbrq;
	}

	/**
	 * 制表日期
	 */
	public void setZbrq(java.util.Date value) {
		this.zbrq = value;
	}
	/**
	 * 出勤天数_巡山
	 */
	public Double getCqtsxs() {
		return cqtsxs;
	}

	/**
	 * 出勤天数_巡山
	 */
	public void setCqtsxs(Double value) {
		this.cqtsxs = value;
	}
	/**
	 * 出勤天数_在岗
	 */
	public Double getCqtszg() {
		return cqtszg;
	}

	/**
	 * 出勤天数_在岗
	 */
	public void setCqtszg(Double value) {
		this.cqtszg = value;
	}
	/**
	 * 出勤天数_事假
	 */
	public Double getCqtssj() {
		return cqtssj;
	}

	/**
	 * 出勤天数_事假
	 */
	public void setCqtssj(Double value) {
		this.cqtssj = value;
	}
	/**
	 * 出勤天数_病假
	 */
	public Double getCqtsbj() {
		return cqtsbj;
	}

	/**
	 * 出勤天数_病假
	 */
	public void setCqtsbj(Double value) {
		this.cqtsbj = value;
	}
	/**
	 * 出勤天数_旷工
	 */
	public Double getCqtskg() {
		return cqtskg;
	}

	/**
	 * 出勤天数_旷工
	 */
	public void setCqtskg(Double value) {
		this.cqtskg = value;
	}
	/**
	 * 目标量化考核评分结果
	 */
	public Double getMblhkhpfjg() {
		return mblhkhpfjg;
	}

	/**
	 * 目标量化考核评分结果
	 */
	public void setMblhkhpfjg(Double value) {
		this.mblhkhpfjg = value;
	}
	/**
	 * 目标考核得分90%
	 */
	public Double getMblhkhpf90() {
		return mblhkhpf90;
	}

	/**
	 * 目标考核得分90%
	 */
	public void setMblhkhpf90(Double value) {
		this.mblhkhpf90 = value;
	}
	/**
	 * 民主测评分
	 */
	public Double getMzcpf() {
		return mzcpf;
	}

	/**
	 * 民主测评分
	 */
	public void setMzcpf(Double value) {
		this.mzcpf = value;
	}
	/**
	 * 明主测评分10%
	 */
	public Double getMzcpf10() {
		return mzcpf10;
	}

	/**
	 * 明主测评分10%
	 */
	public void setMzcpf10(Double value) {
		this.mzcpf10 = value;
	}
	/**
	 * 优秀护林员加分
	 */
	public Double getYxhlyjf() {
		return yxhlyjf;
	}

	/**
	 * 优秀护林员加分
	 */
	public void setYxhlyjf(Double value) {
		this.yxhlyjf = value;
	}
	/**
	 * 年度综合考核得分
	 */
	public Double getNdzhkhdf() {
		return ndzhkhdf;
	}

	/**
	 * 年度综合考核得分
	 */
	public void setNdzhkhdf(Double value) {
		this.ndzhkhdf = value;
	}
	/**
	 * 管理站工资兑现意见
	 */
	public String getGlzgzdxyj() {
		return glzgzdxyj;
	}

	/**
	 * 管理站工资兑现意见
	 */
	public void setGlzgzdxyj(String value) {
		this.glzgzdxyj = value;
	}
	/**
	 * 管理局工资兑现意见
	 */
	public String getGljgzdxyj() {
		return gljgzdxyj;
	}

	/**
	 * 管理局工资兑现意见
	 */
	public void setGljgzdxyj(String value) {
		this.gljgzdxyj = value;
	}
	/**
	 * 被考核人
	 */
	public String getBkhrmc() {
		return bkhrmc;
	}

	/**
	 * 被考核人
	 */
	public void setBkhrmc(String value) {
		this.bkhrmc = value;
	}
	/**
	 * 被考核人ID
	 */
	public String getBkhrid() {
		return bkhrid;
	}

	/**
	 * 被考核人ID
	 */
	public void setBkhrid(String value) {
		this.bkhrid = value;
	}

	@Override
	public void from(GzhlyndkhEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzhlyndkhEntity toEntity() {
		GzhlyndkhEntity toEntity = new GzhlyndkhEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}