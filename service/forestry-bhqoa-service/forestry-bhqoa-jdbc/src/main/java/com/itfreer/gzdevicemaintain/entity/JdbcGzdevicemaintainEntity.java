package com.itfreer.gzdevicemaintain.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzdevicemaintain.entity.GzdevicemaintainEntity;

/**
 * 定义设备维修记录实体
 */
 @Entity(name = "GZ_DEVICE_MAINTAIN")
public class JdbcGzdevicemaintainEntity implements JdbcBaseEntity<GzdevicemaintainEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 100, nullable = true)
	private String id;
	
	/**
	 * I_VERSION
	 */
	@Column(name = "I_VERSION", nullable = true)
	private Integer iversion;
	
	
	/**
	 * 设备编号
	 */
	@Column(name = "S_SBBH", length = 50, nullable = true)
	private String ssbbh;
	
	/**
	 * 设备名称
	 */
	@Column(name = "S_SBMC", length = 50, nullable = true)
	private String ssbmc;
	
	
	/**
	 * 维修单位
	 */
	@Column(name = "S_WXDW", length = 50, nullable = true)
	private String swxdw;
	
	
	/**
	 * 维修单位联系方式
	 */
	@Column(name = "S_WXDWLXFS", length = 50, nullable = true)
	private String swxdwlxfs;
	
	
	/**
	 * 故障描述
	 */
	@Column(name = "S_GZMS", length = 500, nullable = true)
	private String sgzms;
	
	
	/**
	 * 申请人
	 */
	@Column(name = "S_SQR", length = 50, nullable = true)
	private String ssqr;
	
	/**
	 * 申请时间
	 */
	@Column(name = "D_SQSJ", nullable = true)
	private java.util.Date dsqsj;
	
	
	/**
	 * 故障报告接收人
	 */
	@Column(name = "S_GZBGJSR", length = 50, nullable = true)
	private String sgzbgjsr;
	
	/**
	 * 报告接收日期
	 */
	@Column(name = "D_BGJSRQ", nullable = true)
	private java.util.Date dbgjsrq;
	
	
	/**
	 * 故障维修工程师
	 */
	@Column(name = "S_GZWXGCS", length = 50, nullable = true)
	private String sgzwxgcs;
	
	/**
	 * 故障维修日期
	 */
	@Column(name = "D_GZWXRQ", nullable = true)
	private java.util.Date dgzwxrq;
	
	
	/**
	 * 故障诊断报告
	 */
	@Column(name = "S_GZZDBG", length = 500, nullable = true)
	private String sgzzdbg;
	
	
	/**
	 * 故障维修报告
	 */
	@Column(name = "S_GZWXBG", length = 500, nullable = true)
	private String sgzwxbg;
	
	
	/**
	 * 维修工程师
	 */
	@Column(name = "S_WXGCS", length = 50, nullable = true)
	private String swxgcs;
	
	/**
	 * 维修日期
	 */
	@Column(name = "D_WXRQ", nullable = true)
	private java.util.Date dwxrq;
	
	
	/**
	 * 维修完成情况
	 */
	@Column(name = "S_WXWCQK", length = 500, nullable = true)
	private String swxwcqk;
	
	
	/**
	 * 服务态度
	 */
	@Column(name = "S_FWTD", length = 50, nullable = true)
	private String sfwtd;
	
	
	/**
	 * 维修效率
	 */
	@Column(name = "S_WXXL", length = 50, nullable = true)
	private String swxxl;
	
	
	/**
	 * 维修单位负责人
	 */
	@Column(name = "S_WXDWFZR", length = 50, nullable = true)
	private String swxdwfzr;
	
	
	/**
	 * 维修费用
	 */
	@Column(name = "wxfy", length = 50, nullable = true)
	private Double wxfy;
	
	/**
	 * 维修费用附件
	 */
	@Column(name = "wxfyfj", length = 50, nullable = true)
	private String wxfyfj;
	
	/**
	 * 维修审批意见
	 */
	@Column(name = "spyj", length = 50, nullable = true)
	private String spyj;
	
	/**
	 * 审批人
	 */
	@Column(name = "spr_id", length = 50, nullable = true)
	private String sprid;
	
	/**
	 * 审批人名称
	 */
	@Column(name = "spr_name", length = 50, nullable = true)
	private String sprname;
	/**
	 * 审批时间
	 */
	@Column(name = "sprq", length = 50, nullable = true)
	private Date sprq;
	
	/**
	 * 维修验收人
	 */
	@Column(name = "wxysr_id", length = 50, nullable = true)
	private String wxysrid;
	
	/**
	 * 维修验收人名称
	 */
	@Column(name = "wxysr_name", length = 50, nullable = true)
	private String wxysrname;
	/**
	 * 验收时间
	 */
	@Column(name = "wxys_sj", length = 50, nullable = true)
	private Date wxyssj;
	
	/**
	 * 维修验收意见
	 */
	@Column(name = "wxys_yj", length = 50, nullable = true)
	private  String wxysyj;
	
	
	public String getSwxdwlxfs() {
		return swxdwlxfs;
	}

	public void setSwxdwlxfs(String swxdwlxfs) {
		this.swxdwlxfs = swxdwlxfs;
	}

	public Double getWxfy() {
		return wxfy;
	}

	public void setWxfy(Double wxfy) {
		this.wxfy = wxfy;
	}

	public String getSpyj() {
		return spyj;
	}

	public void setSpyj(String spyj) {
		this.spyj = spyj;
	}

	public String getSprid() {
		return sprid;
	}

	public void setSprid(String sprid) {
		this.sprid = sprid;
	}

	public String getSprname() {
		return sprname;
	}

	public void setSprname(String sprname) {
		this.sprname = sprname;
	}

	public Date getSprq() {
		return sprq;
	}

	public void setSprq(Date sprq) {
		this.sprq = sprq;
	}

	public String getWxysrid() {
		return wxysrid;
	}

	public void setWxysrid(String wxysrid) {
		this.wxysrid = wxysrid;
	}

	public String getWxysrname() {
		return wxysrname;
	}

	public void setWxysrname(String wxysrname) {
		this.wxysrname = wxysrname;
	}

	public Date getWxyssj() {
		return wxyssj;
	}

	public void setWxyssj(Date wxyssj) {
		this.wxyssj = wxyssj;
	}

	public String getWxysyj() {
		return wxysyj;
	}

	public void setWxysyj(String wxysyj) {
		this.wxysyj = wxysyj;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * I_VERSION
	 */
	public Integer getIversion() {
		return iversion;
	}

	/**
	 * I_VERSION
	 */
	public void setIversion(Integer value) {
		this.iversion = value;
	}
	/**
	 * 设备编号
	 */
	public String getSsbbh() {
		return ssbbh;
	}

	/**
	 * 设备编号
	 */
	public void setSsbbh(String value) {
		this.ssbbh = value;
	}
	/**
	 * 维修单位
	 */
	public String getSwxdw() {
		return swxdw;
	}

	/**
	 * 维修单位
	 */
	public void setSwxdw(String value) {
		this.swxdw = value;
	}
	/**
	 * 故障描述
	 */
	public String getSgzms() {
		return sgzms;
	}

	/**
	 * 故障描述
	 */
	public void setSgzms(String value) {
		this.sgzms = value;
	}
	/**
	 * 申请人
	 */
	public String getSsqr() {
		return ssqr;
	}

	/**
	 * 申请人
	 */
	public void setSsqr(String value) {
		this.ssqr = value;
	}
	/**
	 * 申请时间
	 */
	public java.util.Date getDsqsj() {
		return dsqsj;
	}

	/**
	 * 申请时间
	 */
	public void setDsqsj(java.util.Date value) {
		this.dsqsj = value;
	}
	/**
	 * 故障报告接收人
	 */
	public String getSgzbgjsr() {
		return sgzbgjsr;
	}

	/**
	 * 故障报告接收人
	 */
	public void setSgzbgjsr(String value) {
		this.sgzbgjsr = value;
	}
	/**
	 * 报告接收日期
	 */
	public java.util.Date getDbgjsrq() {
		return dbgjsrq;
	}

	/**
	 * 报告接收日期
	 */
	public void setDbgjsrq(java.util.Date value) {
		this.dbgjsrq = value;
	}
	/**
	 * 故障维修工程师
	 */
	public String getSgzwxgcs() {
		return sgzwxgcs;
	}

	/**
	 * 故障维修工程师
	 */
	public void setSgzwxgcs(String value) {
		this.sgzwxgcs = value;
	}
	/**
	 * 故障维修日期
	 */
	public java.util.Date getDgzwxrq() {
		return dgzwxrq;
	}

	/**
	 * 故障维修日期
	 */
	public void setDgzwxrq(java.util.Date value) {
		this.dgzwxrq = value;
	}
	/**
	 * 故障诊断报告
	 */
	public String getSgzzdbg() {
		return sgzzdbg;
	}

	/**
	 * 故障诊断报告
	 */
	public void setSgzzdbg(String value) {
		this.sgzzdbg = value;
	}
	/**
	 * 故障维修报告
	 */
	public String getSgzwxbg() {
		return sgzwxbg;
	}

	/**
	 * 故障维修报告
	 */
	public void setSgzwxbg(String value) {
		this.sgzwxbg = value;
	}
	/**
	 * 维修工程师
	 */
	public String getSwxgcs() {
		return swxgcs;
	}

	/**
	 * 维修工程师
	 */
	public void setSwxgcs(String value) {
		this.swxgcs = value;
	}
	/**
	 * 维修日期
	 */
	public java.util.Date getDwxrq() {
		return dwxrq;
	}

	/**
	 * 维修日期
	 */
	public void setDwxrq(java.util.Date value) {
		this.dwxrq = value;
	}
	/**
	 * 维修完成情况
	 */
	public String getSwxwcqk() {
		return swxwcqk;
	}

	/**
	 * 维修完成情况
	 */
	public void setSwxwcqk(String value) {
		this.swxwcqk = value;
	}
	/**
	 * 服务态度
	 */
	public String getSfwtd() {
		return sfwtd;
	}

	/**
	 * 服务态度
	 */
	public void setSfwtd(String value) {
		this.sfwtd = value;
	}
	/**
	 * 维修效率
	 */
	public String getSwxxl() {
		return swxxl;
	}

	/**
	 * 维修效率
	 */
	public void setSwxxl(String value) {
		this.swxxl = value;
	}
	/**
	 * 维修单位负责人
	 */
	public String getSwxdwfzr() {
		return swxdwfzr;
	}

	/**
	 * 维修单位负责人
	 */
	public void setSwxdwfzr(String value) {
		this.swxdwfzr = value;
	}
	
	

	public String getSsbmc() {
		return ssbmc;
	}

	public void setSsbmc(String ssbmc) {
		this.ssbmc = ssbmc;
	}
	
	

	public String getWxfyfj() {
		return wxfyfj;
	}

	public void setWxfyfj(String wxfyfj) {
		this.wxfyfj = wxfyfj;
	}

	@Override
	public void from(GzdevicemaintainEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzdevicemaintainEntity toEntity() {
		GzdevicemaintainEntity toEntity = new GzdevicemaintainEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}