package com.itfreer.gzprojectinfo.entity;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.springframework.beans.BeanUtils;

import com.itfreer.bpm.api.IJdbcBpmBaseEntity;
import com.itfreer.gzkyxmcg.entity.GzkyxmcgEntity;
import com.itfreer.gzkyxmcg.entity.JdbcGzkyxmcgEntity;
import com.itfreer.gzplanpersonnel.entity.GzplanpersonnelEntity;
import com.itfreer.gzplanpersonnel.entity.JdbcGzplanpersonnelEntity;

/**
 * 定义项目立项管理实体
 */
 @Entity(name = "GZ_PROJECT_INFO")
public class JdbcGzprojectinfoEntity implements IJdbcBpmBaseEntity<GzprojectinfoEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 项目编号
	 */
	@Column(name = "S_XMBH", length = 100, nullable = true)
	private String sxmbh;
	
	
	/**
	 * 项目名称
	 */
	@Column(name = "S_XMMC", length = 100, nullable = true)
	private String sxmmc;
	
	/**
	 * 项目内容
	 */
	@Column(name = "S_XMNR", length = 500, nullable = true)
	private String sxmnr;
	
	
	/**
	 * 项目规模
	 */
	@Column(name = "S_XMGM", length = 100, nullable = true)
	private String sxmgm;
	
	
	/**
	 * 项目状态
	 */
	@Column(name = "S_XMZT", length = 100, nullable = true)
	private String sxmzt;
	
	/**
	 * 项目经理（项目负责人）
	 */
	@Column(name = "S_XMJL", length = 100, nullable = true)
	private String sxmjl;
	
	/**
	 * 项目经理（项目负责人）名称
	 */
	@Column(name = "S_XMJLNAME", length = 100, nullable = true)
	private String sxmjlname;
	
	/**
	 * 联系电话
	 */
	@Column(name = "S_LXDH", length = 500, nullable = true)
	private String slxdh;
	
	/**
	 * 办公电话
	 */
	@Column(name = "S_BGDH", length = 500, nullable = true)
	private String sbgdh;
	
	/**
	 * 电子邮箱
	 */
	@Column(name = "S_DZYX", length = 500, nullable = true)
	private String sdzyx;
	
	/**
	 * 备注
	 */
	@Column(name = "S_BZ", length = 500, nullable = true)
	private String sbz;
	
	/**
	 *项目成效
	 */
	@Column(name = "S_XMCX", length = 500, nullable = true)
	private String Sxmcx;
	
	/**
	 *申请部门
	 */
	@Column(name = "SQBM", length = 255, nullable = true)
	private String sqbm;
	/**
	 *申请人
	 */
	@Column(name = "SQR", length = 255, nullable = true)
	private String sqr;
	/**
	 *申请人id
	 */
	@Column(name = "SQRID", length = 255, nullable = true)
	private String sqrid;
	/**
	 *项目来源
	 */
	@Column(name = "XMLY", length = 255, nullable = true)
	private String xmly;
	/**
	 *立项情况
	 */
	@Column(name = "LXQK", length = 255, nullable = true)
	private String lxqk;
	/**
	 * 申请时间
	 */
	@Column(name = "SQSJ", nullable = true)
	private java.util.Date sqsj;
	/**
	 * 结项时间
	 */
	@Column(name = "JXSJ", nullable = true)
	private java.util.Date jxsj;
	
	
	public String getSqbm() {
		return sqbm;
	}

	public void setSqbm(String sqbm) {
		this.sqbm = sqbm;
	}

	public String getSqr() {
		return sqr;
	}

	public void setSqr(String sqr) {
		this.sqr = sqr;
	}
	
	public String getSqrid() {
		return sqrid;
	}

	public void setSqrid(String sqrid) {
		this.sqrid = sqrid;
	}

	public String getXmly() {
		return xmly;
	}

	public void setXmly(String xmly) {
		this.xmly = xmly;
	}

	public String getLxqk() {
		return lxqk;
	}

	public void setLxqk(String lxqk) {
		this.lxqk = lxqk;
	}

	public java.util.Date getSqsj() {
		return sqsj;
	}

	public void setSqsj(java.util.Date sqsj) {
		this.sqsj = sqsj;
	}

	public java.util.Date getJxsj() {
		return jxsj;
	}

	public void setJxsj(java.util.Date jxsj) {
		this.jxsj = jxsj;
	}
	

	public String getSxmjlname() {
		return sxmjlname;
	}

	public void setSxmjlname(String sxmjlname) {
		this.sxmjlname = sxmjlname;
	}

	public String getSxmnr() {
		return sxmnr;
	}

	public void setSxmnr(String sxmnr) {
		this.sxmnr = sxmnr;
	}

	public String getSlxdh() {
		return slxdh;
	}

	public void setSlxdh(String slxdh) {
		this.slxdh = slxdh;
	}

	public String getSbgdh() {
		return sbgdh;
	}

	public void setSbgdh(String sbgdh) {
		this.sbgdh = sbgdh;
	}

	public String getSdzyx() {
		return sdzyx;
	}

	public void setSdzyx(String sdzyx) {
		this.sdzyx = sdzyx;
	}

	public String getSbz() {
		return sbz;
	}

	public void setSbz(String sbz) {
		this.sbz = sbz;
	}

	public String getSxmcx() {
		return Sxmcx;
	}

	public void setSxmcx(String sxmcx) {
		Sxmcx = sxmcx;
	}

	/**
	 * 项目类型
	 */
	@Column(name = "S_XMLX", length = 100, nullable = true)
	private String sxmlx;
	
	
	/**
	 * 项目性质
	 */
	@Column(name = "S_XMXZ", length = 100, nullable = true)
	private String sxmxz;
	

	
	
	/**
	 * 工作区域省
	 */
	@Column(name = "S_SHENG", length = 100, nullable = true)
	private String ssheng;
	
	
	/**
	 * 工作区域市
	 */
	@Column(name = "S_SHI", length = 100, nullable = true)
	private String sshi;
	
	
	/**
	 * 工作区域县
	 */
	@Column(name = "S_XIAN", length = 100, nullable = true)
	private String sxian;
	
	
	/**
	 * 工作地点
	 */
	@Column(name = "S_GZDD", length = 100, nullable = true)
	private String sgzdd;
	
	
	/**
	 * 是否野外项目
	 */
	@Column(name = "S_SFYWXM", length = 100, nullable = true)
	private String ssfywxm;
	
	
	/**
	 * 作业部门
	 */
	@Column(name = "S_ZYBM", length = 100, nullable = true)
	private String szybm;
	
	

	
	
	/**
	 * 标书
	 */
	@Column(name = "S_BS", length = 100, nullable = true)
	private String sbs;
	
	
	/**
	 * 合同
	 */
	@Column(name = "S_HT", length = 100, nullable = true)
	private String sht;
	
	
	/**
	 * 合同期限
	 */
	@Column(name = "S_HTQX", length = 100, nullable = true)
	private String shtqx;
	
	/**
	 * 合同签订日期
	 */
	@Column(name = "D_HTQDRQ", nullable = true)
	private java.util.Date dhtqdrq;
	
	
	/**
	 * 委托单位
	 */
	@Column(name = "S_WTDW", length = 100, nullable = true)
	private String swtdw;
	
	/**
	 * 登记日期
	 */
	@Column(name = "S_DJRQ", nullable = true)
	private java.util.Date sdjrq;
	
	
	/**
	 * 实施规划书
	 */
	@Column(name = "S_SSGHS", length = 100, nullable = true)
	private String sssghs;
	
	
	/**
	 * 技术设计书
	 */
	@Column(name = "S_JSSJS", length = 100, nullable = true)
	private String sjssjs;
	
	
	/**
	 * 过程资料
	 */
	@Column(name = "S_GCZL", length = 100, nullable = true)
	private String sgczl;
	
	/**
	 * 开工日期
	 */
	@Column(name = "D_KGRQ", nullable = true)
	private java.util.Date dkgrq;
	
	/**
	 * 竣工日期
	 */
	@Column(name = "D_JGRQ", nullable = true)
	private java.util.Date djgrq;
	
	
	/**
	 * 产品质量
	 */
	@Column(name = "S_CPZL", length = 100, nullable = true)
	private String scpzl;
	
	
	/**
	 * 服务质量
	 */
	@Column(name = "S_FWZL", length = 100, nullable = true)
	private String sfwzl;
	
	
	/**
	 * 合同履约
	 */
	@Column(name = "S_HTLY", length = 100, nullable = true)
	private String shtly;
	
	
	/**
	 * 收费合理性
	 */
	@Column(name = "S_SFHLX", length = 100, nullable = true)
	private String ssfhlx;
	
	
	/**
	 * 调查方式
	 */
	@Column(name = "S_DCFS", length = 100, nullable = true)
	private String sdcfs;
	

	
	
	/**
	 * 流程编号
	 */
	@Column(name = "S_LCBH", length = 100, nullable = true)
	private String slcbh;
	
	
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 100, nullable = true)
	private String id;
	

	/**
	 * 附件
	 */
	@Column(name = "FJ", length = 500, nullable = true)
	private String fj;

	public String getFj() {
		return fj;
	}

	public void setFj(String fj) {
		this.fj = fj;
	}
	
	/**
	 * 成果材料
	 */
	@Column(name = "CGCL", length = 100, nullable = true)
	private String cgcl;
	
	public String getCgcl() {
		return cgcl;
	}

	public void setCgcl(String cgcl) {
		this.cgcl = cgcl;
	}

	/**
	 * 项目人员
	 */
	@SuppressWarnings("deprecation")
	@org.hibernate.annotations.ForeignKey(name = "none")
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "XMID")
	@OrderBy("S_JHRWBH ASC")
	private Set<JdbcGzplanpersonnelEntity> gzplanpersonnel = new LinkedHashSet<JdbcGzplanpersonnelEntity>();

	/**
	 * 项目人员
	 * @return
	 */
	public Set<JdbcGzplanpersonnelEntity> getGzplanpersonnel() {
		return gzplanpersonnel;
	}
	/**
	 * 项目人员
	 * @return
	 */
	public void setGzplanpersonnel(Set<JdbcGzplanpersonnelEntity> gzplanpersonnel) {
		this.gzplanpersonnel = gzplanpersonnel;
	}
	
	
	/**
	 * 项目编号
	 */
	public String getSxmbh() {
		return sxmbh;
	}

	/**
	 * 项目编号
	 */
	public void setSxmbh(String value) {
		this.sxmbh = value;
	}
	/**
	 * 项目名称
	 */
	public String getSxmmc() {
		return sxmmc;
	}

	/**
	 * 项目名称
	 */
	public void setSxmmc(String value) {
		this.sxmmc = value;
	}
	
	/**
	 * 项目类型
	 */
	public String getSxmlx() {
		return sxmlx;
	}

	/**
	 * 项目类型
	 */
	public void setSxmlx(String value) {
		this.sxmlx = value;
	}
	/**
	 * 项目性质
	 */
	public String getSxmxz() {
		return sxmxz;
	}

	/**
	 * 项目性质
	 */
	public void setSxmxz(String value) {
		this.sxmxz = value;
	}
	/**
	 * 项目规模
	 */
	public String getSxmgm() {
		return sxmgm;
	}

	/**
	 * 项目规模
	 */
	public void setSxmgm(String value) {
		this.sxmgm = value;
	}
	/**
	 * 工作区域省
	 */
	public String getSsheng() {
		return ssheng;
	}

	/**
	 * 工作区域省
	 */
	public void setSsheng(String value) {
		this.ssheng = value;
	}
	/**
	 * 工作区域市
	 */
	public String getSshi() {
		return sshi;
	}

	/**
	 * 工作区域市
	 */
	public void setSshi(String value) {
		this.sshi = value;
	}
	/**
	 * 工作区域县
	 */
	public String getSxian() {
		return sxian;
	}

	/**
	 * 工作区域县
	 */
	public void setSxian(String value) {
		this.sxian = value;
	}
	/**
	 * 工作地点
	 */
	public String getSgzdd() {
		return sgzdd;
	}

	/**
	 * 工作地点
	 */
	public void setSgzdd(String value) {
		this.sgzdd = value;
	}
	/**
	 * 是否野外项目
	 */
	public String getSsfywxm() {
		return ssfywxm;
	}

	/**
	 * 是否野外项目
	 */
	public void setSsfywxm(String value) {
		this.ssfywxm = value;
	}
	/**
	 * 作业部门
	 */
	public String getSzybm() {
		return szybm;
	}

	/**
	 * 作业部门
	 */
	public void setSzybm(String value) {
		this.szybm = value;
	}
	/**
	 * 项目经理
	 */
	public String getSxmjl() {
		return sxmjl;
	}

	/**
	 * 项目经理
	 */
	public void setSxmjl(String value) {
		this.sxmjl = value;
	}
	/**
	 * 标书
	 */
	public String getSbs() {
		return sbs;
	}

	/**
	 * 标书
	 */
	public void setSbs(String value) {
		this.sbs = value;
	}
	/**
	 * 合同
	 */
	public String getSht() {
		return sht;
	}

	/**
	 * 合同
	 */
	public void setSht(String value) {
		this.sht = value;
	}
	/**
	 * 合同期限
	 */
	public String getShtqx() {
		return shtqx;
	}

	/**
	 * 合同期限
	 */
	public void setShtqx(String value) {
		this.shtqx = value;
	}
	/**
	 * 合同签订日期
	 */
	public java.util.Date getDhtqdrq() {
		return dhtqdrq;
	}

	/**
	 * 合同签订日期
	 */
	public void setDhtqdrq(java.util.Date value) {
		this.dhtqdrq = value;
	}
	/**
	 * 委托单位
	 */
	public String getSwtdw() {
		return swtdw;
	}

	/**
	 * 委托单位
	 */
	public void setSwtdw(String value) {
		this.swtdw = value;
	}
	/**
	 * 登记日期
	 */
	public java.util.Date getSdjrq() {
		return sdjrq;
	}

	/**
	 * 登记日期
	 */
	public void setSdjrq(java.util.Date value) {
		this.sdjrq = value;
	}
	/**
	 * 实施规划书
	 */
	public String getSssghs() {
		return sssghs;
	}

	/**
	 * 实施规划书
	 */
	public void setSssghs(String value) {
		this.sssghs = value;
	}
	/**
	 * 技术设计书
	 */
	public String getSjssjs() {
		return sjssjs;
	}

	/**
	 * 技术设计书
	 */
	public void setSjssjs(String value) {
		this.sjssjs = value;
	}
	/**
	 * 过程资料
	 */
	public String getSgczl() {
		return sgczl;
	}

	/**
	 * 过程资料
	 */
	public void setSgczl(String value) {
		this.sgczl = value;
	}
	/**
	 * 开工日期
	 */
	public java.util.Date getDkgrq() {
		return dkgrq;
	}

	/**
	 * 开工日期
	 */
	public void setDkgrq(java.util.Date value) {
		this.dkgrq = value;
	}
	/**
	 * 竣工日期
	 */
	public java.util.Date getDjgrq() {
		return djgrq;
	}

	/**
	 * 竣工日期
	 */
	public void setDjgrq(java.util.Date value) {
		this.djgrq = value;
	}
	/**
	 * 产品质量
	 */
	public String getScpzl() {
		return scpzl;
	}

	/**
	 * 产品质量
	 */
	public void setScpzl(String value) {
		this.scpzl = value;
	}
	/**
	 * 服务质量
	 */
	public String getSfwzl() {
		return sfwzl;
	}

	/**
	 * 服务质量
	 */
	public void setSfwzl(String value) {
		this.sfwzl = value;
	}
	/**
	 * 合同履约
	 */
	public String getShtly() {
		return shtly;
	}

	/**
	 * 合同履约
	 */
	public void setShtly(String value) {
		this.shtly = value;
	}
	/**
	 * 收费合理性
	 */
	public String getSsfhlx() {
		return ssfhlx;
	}

	/**
	 * 收费合理性
	 */
	public void setSsfhlx(String value) {
		this.ssfhlx = value;
	}
	/**
	 * 调查方式
	 */
	public String getSdcfs() {
		return sdcfs;
	}

	/**
	 * 调查方式
	 */
	public void setSdcfs(String value) {
		this.sdcfs = value;
	}
	/**
	 * 项目状态
	 */
	public String getSxmzt() {
		return sxmzt;
	}

	/**
	 * 项目状态
	 */
	public void setSxmzt(String value) {
		this.sxmzt = value;
	}
	/**
	 * 流程编号
	 */
	public String getSlcbh() {
		return slcbh;
	}

	/**
	 * 流程编号
	 */
	public void setSlcbh(String value) {
		this.slcbh = value;
	}
	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}

	/*@Override
	public void from(GzprojectinfoEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzprojectinfoEntity toEntity() {
		GzprojectinfoEntity toEntity = new GzprojectinfoEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}*/
	@Override
	public void from(GzprojectinfoEntity t) {
		BeanUtils.copyProperties(t, this);
		
		//项目人员表-start-
		Set<JdbcGzplanpersonnelEntity> jdbcgzplanpersonnel = new LinkedHashSet<JdbcGzplanpersonnelEntity>();
		Set<GzplanpersonnelEntity> gzplanpersonnel = t.getGzplanpersonnel();
		if (gzplanpersonnel != null) {
			for (GzplanpersonnelEntity item : gzplanpersonnel) {
				JdbcGzplanpersonnelEntity jItem = new JdbcGzplanpersonnelEntity();
				jItem.from(item);
				jdbcgzplanpersonnel.add(jItem);
			}
		}
		this.setGzplanpersonnel(jdbcgzplanpersonnel);
		
		//项目人员表-start-
		Set<JdbcGzkyxmcgEntity> jdbcxmcg = new LinkedHashSet<JdbcGzkyxmcgEntity>();
		Set<GzkyxmcgEntity> xmcg = t.getXmcg();
		if (xmcg != null) {
			for (GzkyxmcgEntity item : xmcg) {
				JdbcGzkyxmcgEntity jItem = new JdbcGzkyxmcgEntity();
				jItem.from(item);
				jdbcxmcg.add(jItem);
			}
		}
		this.setXmcg(jdbcxmcg);
	}
	@Override
	public GzprojectinfoEntity toEntity() {
		GzprojectinfoEntity toEntity = new GzprojectinfoEntity();
		BeanUtils.copyProperties(this, toEntity);
		
		//查询匹配条件-start-
		Set<GzplanpersonnelEntity> gzplanpersonnel = new LinkedHashSet<GzplanpersonnelEntity>();
		Set<JdbcGzplanpersonnelEntity> jdbcgzplanpersonnel = this.getGzplanpersonnel();
		
		if (jdbcgzplanpersonnel != null) {
			for (JdbcGzplanpersonnelEntity item : jdbcgzplanpersonnel) {
				gzplanpersonnel.add(item.toEntity());
			}
		}
		toEntity.setGzplanpersonnel(gzplanpersonnel);
		
		//查询匹配条件-start-
		Set<GzkyxmcgEntity> xmcg = new LinkedHashSet<GzkyxmcgEntity>();
		Set<JdbcGzkyxmcgEntity> jdbcxmcg = this.getXmcg();

		if (jdbcxmcg != null) {
			for (JdbcGzkyxmcgEntity item : jdbcxmcg) {
				xmcg.add(item.toEntity());
			}
		}
		toEntity.setXmcg(xmcg);
		
		return toEntity;
	}
	
	@Column(name = "s_exeid")
	private String sexeid;

	/**
	 * 获取项目名称
	 */
	@Override
	public String getProjectname() {
		return this.sxmmc;
	}

	/**
	 * 流程实例主键
	 */

	public String getSexeid() {
		return this.sexeid;
	}
	/**
	 * 流程实例主键
	 */

	public void setSexeid(String sexeid) {
		this.sexeid=sexeid;
	}
	
	/**
	 * 项目成果
	 */
	@SuppressWarnings("deprecation")
	@org.hibernate.annotations.ForeignKey(name = "none")
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "XMBH")
	@OrderBy("SCSJ ASC")
	private Set<JdbcGzkyxmcgEntity> xmcg=new LinkedHashSet<JdbcGzkyxmcgEntity>();
	
	public Set<JdbcGzkyxmcgEntity> getXmcg() {
		return xmcg;
	}

	public void setXmcg(Set<JdbcGzkyxmcgEntity> xmcg) {
		this.xmcg = xmcg;
	}
}