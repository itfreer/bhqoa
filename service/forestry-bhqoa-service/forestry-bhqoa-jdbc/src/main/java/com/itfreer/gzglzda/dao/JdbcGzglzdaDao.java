package com.itfreer.gzglzda.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzglzda.dao.GzglzdaDao;
import com.itfreer.gzglzda.entity.GzglzdaEntity;
import com.itfreer.gzglzda.entity.JdbcGzglzdaEntity;

/**
 * 定义管理站档案 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzglzdaDao")
public class JdbcGzglzdaDao extends JdbcBaseDaoImp<GzglzdaEntity, JdbcGzglzdaEntity> implements GzglzdaDao {

}
