package com.itfreer.gzclda.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzclda.entity.GzcldaEntity;

/**
 * 定义车辆档案实体
 */
 @Entity(name = "GZ_CLDA")
public class JdbcGzcldaEntity implements JdbcBaseEntity<GzcldaEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 车辆型号
	 */
	@Column(name = "S_CLXH", length = 500, nullable = true)
	private String sclxh;
	
	
	/**
	 * 车辆信息
	 */
	@Column(name = "S_CLXX", length = 500, nullable = true)
	private String sclxx;
	
	
	/**
	 * 车牌号
	 */
	@Column(name = "S_CPH", length = 500, nullable = true)
	private String scph;
	
	
	/**
	 * 保修期限
	 */
	@Column(name = "S_BXQX", length = 500, nullable = true)
	private String sbxqx;
	
	
	/**
	 * 采购日期
	 */
	@Column(name = "D_CGRQ", length = 500, nullable = true)
	private String dcgrq;
	
	
	/**
	 * 采购价格
	 */
	@Column(name = "S_CGJG", length = 500, nullable = true)
	private String scgjg;
	
	
	/**
	 * 采购部门
	 */
	@Column(name = "S_CGBM", length = 500, nullable = true)
	private String scgbm;
	
	
	/**
	 * 采购人
	 */
	@Column(name = "S_CGR", length = 500, nullable = true)
	private String scgr;
	
	
	/**
	 * 采购人名称
	 */
	@Column(name = "S_CGRMC", length = 500, nullable = true)
	private String scgrmc;
	
	
	public String getScgrmc() {
		return scgrmc;
	}

	public void setScgrmc(String scgrmc) {
		this.scgrmc = scgrmc;
	}
	
	
	/**
	 * 采购原因
	 */
	@Column(name = "S_CGYY", length = 500, nullable = true)
	private String scgyy;
	
	
	/**
	 * 联系方式
	 */
	@Column(name = "S_LXFF", length = 500, nullable = true)
	private String slxff;
	
	
	/**
	 * 使用情况
	 */
	@Column(name = "S_SYQK", length = 500, nullable = true)
	private String ssyqk;
	
	
	/**
	 * 维修情况
	 */
	@Column(name = "S_WXQK", length = 500, nullable = true)
	private String swxqk;
	
	
	/**
	 * 油卡充值情况
	 */
	@Column(name = "S_YKCZQK", length = 500, nullable = true)
	private String sykczqk;
	
	
	/**
	 * 车辆信息附件
	 */
	@Column(name = "CLXXFJ", length = 500, nullable = true)
	private String clxxfj;
	
	
	/**
	 * 车辆状态
	 */
	@Column(name = "CLZT", length = 500, nullable = true)
	private String clzt;
	

	public String getClxxfj() {
		return clxxfj;
	}

	public void setClxxfj(String clxxfj) {
		this.clxxfj = clxxfj;
	}

	public String getClzt() {
		return clzt;
	}

	public void setClzt(String clzt) {
		this.clzt = clzt;
	}

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 车辆型号
	 */
	public String getSclxh() {
		return sclxh;
	}

	/**
	 * 车辆型号
	 */
	public void setSclxh(String value) {
		this.sclxh = value;
	}
	/**
	 * 车辆信息
	 */
	public String getSclxx() {
		return sclxx;
	}

	/**
	 * 车辆信息
	 */
	public void setSclxx(String value) {
		this.sclxx = value;
	}
	/**
	 * 车牌号
	 */
	public String getScph() {
		return scph;
	}

	/**
	 * 车牌号
	 */
	public void setScph(String value) {
		this.scph = value;
	}
	/**
	 * 保修期限
	 */
	public String getSbxqx() {
		return sbxqx;
	}

	/**
	 * 保修期限
	 */
	public void setSbxqx(String value) {
		this.sbxqx = value;
	}
	/**
	 * 采购日期
	 */
	public String getDcgrq() {
		return dcgrq;
	}

	/**
	 * 采购日期
	 */
	public void setDcgrq(String value) {
		this.dcgrq = value;
	}
	/**
	 * 采购价格
	 */
	public String getScgjg() {
		return scgjg;
	}

	/**
	 * 采购价格
	 */
	public void setScgjg(String value) {
		this.scgjg = value;
	}
	/**
	 * 采购部门
	 */
	public String getScgbm() {
		return scgbm;
	}

	/**
	 * 采购部门
	 */
	public void setScgbm(String value) {
		this.scgbm = value;
	}
	/**
	 * 采购人
	 */
	public String getScgr() {
		return scgr;
	}

	/**
	 * 采购人
	 */
	public void setScgr(String value) {
		this.scgr = value;
	}
	/**
	 * 采购原因
	 */
	public String getScgyy() {
		return scgyy;
	}

	/**
	 * 采购原因
	 */
	public void setScgyy(String value) {
		this.scgyy = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxff() {
		return slxff;
	}

	/**
	 * 联系方式
	 */
	public void setSlxff(String value) {
		this.slxff = value;
	}
	/**
	 * 使用情况
	 */
	public String getSsyqk() {
		return ssyqk;
	}

	/**
	 * 使用情况
	 */
	public void setSsyqk(String value) {
		this.ssyqk = value;
	}
	/**
	 * 维修情况
	 */
	public String getSwxqk() {
		return swxqk;
	}

	/**
	 * 维修情况
	 */
	public void setSwxqk(String value) {
		this.swxqk = value;
	}
	/**
	 * 油卡充值情况
	 */
	public String getSykczqk() {
		return sykczqk;
	}

	/**
	 * 油卡充值情况
	 */
	public void setSykczqk(String value) {
		this.sykczqk = value;
	}

	@Override
	public void from(GzcldaEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzcldaEntity toEntity() {
		GzcldaEntity toEntity = new GzcldaEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}