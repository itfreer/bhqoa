package com.itfreer.gzhlyda.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzhlyda.entity.GzhlydaEntity;

/**
 * 定义护林员档案实体
 */
 @Entity(name = "GZ_HLYDA")
public class JdbcGzhlydaEntity implements JdbcBaseEntity<GzhlydaEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 护林员名称
	 */
	@Column(name = "S_HLYMC", length = 500, nullable = true)
	private String shlymc;
	
	/**
	 * 护林员出生日期
	 */
	@Column(name = "D_HLYSR", nullable = true)
	private java.util.Date dhlysr;
	
	
	/**
	 * 联系方式
	 */
	@Column(name = "S_LXFS", length = 500, nullable = true)
	private String slxfs;
	
	
	/**
	 * 护林员类型
	 */
	@Column(name = "S_HLYLX", length = 500, nullable = true)
	private String shlylx;
	
	
	/**
	 * 所在管理站
	 */
	@Column(name = "S_SZGLZ", length = 500, nullable = true)
	private String sszglz;
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 护林员名称
	 */
	public String getShlymc() {
		return shlymc;
	}

	/**
	 * 护林员名称
	 */
	public void setShlymc(String value) {
		this.shlymc = value;
	}
	/**
	 * 护林员出生日期
	 */
	public java.util.Date getDhlysr() {
		return dhlysr;
	}

	/**
	 * 护林员出生日期
	 */
	public void setDhlysr(java.util.Date value) {
		this.dhlysr = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxfs() {
		return slxfs;
	}

	/**
	 * 联系方式
	 */
	public void setSlxfs(String value) {
		this.slxfs = value;
	}
	/**
	 * 护林员类型
	 */
	public String getShlylx() {
		return shlylx;
	}

	/**
	 * 护林员类型
	 */
	public void setShlylx(String value) {
		this.shlylx = value;
	}
	/**
	 * 所在管理站
	 */
	public String getSszglz() {
		return sszglz;
	}

	/**
	 * 所在管理站
	 */
	public void setSszglz(String value) {
		this.sszglz = value;
	}

	@Override
	public void from(GzhlydaEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzhlydaEntity toEntity() {
		GzhlydaEntity toEntity = new GzhlydaEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}