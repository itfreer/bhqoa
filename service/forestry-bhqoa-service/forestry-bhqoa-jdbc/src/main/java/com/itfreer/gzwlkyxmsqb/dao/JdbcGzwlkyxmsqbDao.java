package com.itfreer.gzwlkyxmsqb.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzwlkyxmsqb.dao.GzwlkyxmsqbDao;
import com.itfreer.gzwlkyxmsqb.entity.GzwlkyxmsqbEntity;
import com.itfreer.gzwlkyxmsqb.entity.JdbcGzwlkyxmsqbEntity;

/**
 * 定义外来科研项目申请表 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzwlkyxmsqbDao")
public class JdbcGzwlkyxmsqbDao extends JdbcBaseDaoImp<GzwlkyxmsqbEntity, JdbcGzwlkyxmsqbEntity> implements GzwlkyxmsqbDao {

}
