package com.itfreer.gzhlyydkh.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzhlyydkh.dao.GzhlyydkhDao;
import com.itfreer.gzhlyydkh.entity.GzhlyydkhEntity;
import com.itfreer.gzhlyydkh.entity.JdbcGzhlyydkhEntity;

/**
 * 定义护林员月度考核 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzhlyydkhDao")
public class JdbcGzhlyydkhDao extends JdbcBaseDaoImp<GzhlyydkhEntity, JdbcGzhlyydkhEntity> implements GzhlyydkhDao {

}
