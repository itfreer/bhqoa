package com.itfreer.gzgzrqdsj.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;

/**
 * 定义签到地点实体
 */
 @Entity(name = "GZ_GZR_QDSJ")
public class JdbcGzgzrqdsjEntity implements JdbcBaseEntity<GzgzrqdsjEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 开始月份
	 */
	@Column(name = "KSYF", length = 2, nullable = true)
	private Integer ksyf;
	
	
	/**
	 * 开始日期
	 */
	@Column(name = "KSRQ", length = 2, nullable = true)
	private Integer ksrq;
	
	/**
	 * 结束月份
	 */
	@Column(name = "JSYF", length = 2, nullable = true)
	private Integer jsyf;
	
	
	/**
	 * 结束日期
	 */
	@Column(name = "JSRQ", length = 2, nullable = true)
	private Integer jsrq;
	
	/**
	 * 上午上班时
	 */
	@Column(name = "SWSBS", length = 2, nullable = true)
	private Integer swsbs;
	
	
	/**
	 * 上午上班分
	 */
	@Column(name = "SWSBF", length = 2, nullable = true)
	private Integer swsbf;
	
	/**
	 * 上午下班时
	 */
	@Column(name = "SWXBS", length = 2, nullable = true)
	private Integer swxbs;
	
	
	/**
	 * 上午下班分
	 */
	@Column(name = "SWXBF", length = 2, nullable = true)
	private Integer swxbf;
	
	/**
	 * 下午上班时
	 */
	@Column(name = "XWSBS", length = 2, nullable = true)
	private Integer xwsbs;
	
	
	/**
	 * 下午上班分
	 */
	@Column(name = "XWSBF", length = 2, nullable = true)
	private Integer xwsbf;
	
	/**
	 * 下午下班时
	 */
	@Column(name = "XWXBS", length = 2, nullable = true)
	private Integer xwxbs;
	
	
	/**
	 * 下午下班分
	 */
	@Column(name = "XWXBF", length = 2, nullable = true)
	private Integer xwxbf;
	
	/**
	 * 创建时间
	 */
	@Column(name = "CJSJ", nullable = true)
	private java.util.Date cjsj;
	
	/**
	 * 备注
	 */
	@Column(name = "BZ", nullable = true)
	private String bz;
	
	
	/**
	 * 上午上班结束时
	 */
	@Column(name = "SWSBJSS", length = 2, nullable = true)
	private Integer swsbjss;
	
	
	/**
	 * 上午上班结束分
	 */
	@Column(name = "SWSBJSF", length = 2, nullable = true)
	private Integer swsbjsf;
	
	/**
	 * 上午下班结束时
	 */
	@Column(name = "SWXBJSS", length = 2, nullable = true)
	private Integer swxbjss;
	
	
	/**
	 * 上午下班结束分
	 */
	@Column(name = "SWXBJSF", length = 2, nullable = true)
	private Integer swxbjsf;
	
	/**
	 * 下午上班结束时
	 */
	@Column(name = "XWSBJSS", length = 2, nullable = true)
	private Integer xwsbjss;
	
	
	/**
	 * 下午上班结束分
	 */
	@Column(name = "XWSBJSF", length = 2, nullable = true)
	private Integer xwsbjsf;
	
	/**
	 * 下午下班结束时
	 */
	@Column(name = "XWXBJSS", length = 2, nullable = true)
	private Integer xwxbjss;
	
	
	/**
	 * 下午下班结束分
	 */
	@Column(name = "XWXBJSF", length = 2, nullable = true)
	private Integer xwxbjsf;
	
	
	
	public Integer getSwsbjss() {
		return swsbjss;
	}

	public void setSwsbjss(Integer swsbjss) {
		this.swsbjss = swsbjss;
	}

	public Integer getSwsbjsf() {
		return swsbjsf;
	}

	public void setSwsbjsf(Integer swsbjsf) {
		this.swsbjsf = swsbjsf;
	}

	public Integer getSwxbjss() {
		return swxbjss;
	}

	public void setSwxbjss(Integer swxbjss) {
		this.swxbjss = swxbjss;
	}

	public Integer getSwxbjsf() {
		return swxbjsf;
	}

	public void setSwxbjsf(Integer swxbjsf) {
		this.swxbjsf = swxbjsf;
	}

	public Integer getXwsbjss() {
		return xwsbjss;
	}

	public void setXwsbjss(Integer xwsbjss) {
		this.xwsbjss = xwsbjss;
	}

	public Integer getXwsbjsf() {
		return xwsbjsf;
	}

	public void setXwsbjsf(Integer xwsbjsf) {
		this.xwsbjsf = xwsbjsf;
	}

	public Integer getXwxbjss() {
		return xwxbjss;
	}

	public void setXwxbjss(Integer xwxbjss) {
		this.xwxbjss = xwxbjss;
	}

	public Integer getXwxbjsf() {
		return xwxbjsf;
	}

	public void setXwxbjsf(Integer xwxbjsf) {
		this.xwxbjsf = xwxbjsf;
	}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getKsyf() {
		return ksyf;
	}

	public void setKsyf(Integer ksyf) {
		this.ksyf = ksyf;
	}

	public Integer getKsrq() {
		return ksrq;
	}

	public void setKsrq(Integer ksrq) {
		this.ksrq = ksrq;
	}

	public Integer getJsyf() {
		return jsyf;
	}

	public void setJsyf(Integer jsyf) {
		this.jsyf = jsyf;
	}

	public Integer getJsrq() {
		return jsrq;
	}

	public void setJsrq(Integer jsrq) {
		this.jsrq = jsrq;
	}

	public Integer getSwsbs() {
		return swsbs;
	}

	public void setSwsbs(Integer swsbs) {
		this.swsbs = swsbs;
	}

	public Integer getXwsbf() {
		return xwsbf;
	}

	public void setXwsbf(Integer xwsbf) {
		this.xwsbf = xwsbf;
	}

	public Integer getSwxbs() {
		return swxbs;
	}

	public void setSwxbs(Integer swxbs) {
		this.swxbs = swxbs;
	}

	public Integer getSwxbf() {
		return swxbf;
	}

	public void setSwxbf(Integer swxbf) {
		this.swxbf = swxbf;
	}

	public Integer getXwsbs() {
		return xwsbs;
	}

	public void setXwsbs(Integer xwsbs) {
		this.xwsbs = xwsbs;
	}

	public Integer getSwsbf() {
		return swsbf;
	}

	public void setSwsbf(Integer swsbf) {
		this.swsbf = swsbf;
	}

	public Integer getXwxbs() {
		return xwxbs;
	}

	public void setXwxbs(Integer xwxbs) {
		this.xwxbs = xwxbs;
	}

	public Integer getXwxbf() {
		return xwxbf;
	}

	public void setXwxbf(Integer xwxbf) {
		this.xwxbf = xwxbf;
	}

	public java.util.Date getCjsj() {
		return cjsj;
	}

	public void setCjsj(java.util.Date cjsj) {
		this.cjsj = cjsj;
	}

	public String getBz() {
		return bz;
	}

	public void setBz(String bz) {
		this.bz = bz;
	}

	@Override
	public void from(GzgzrqdsjEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzgzrqdsjEntity toEntity() {
		GzgzrqdsjEntity toEntity = new GzgzrqdsjEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}

}