package com.itfreer.gzrskqtj.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzrskqtj.dao.GzrskqtjDao;
import com.itfreer.gzrskqtj.entity.GzrskqtjEntity;
import com.itfreer.gzrskqtj.entity.JdbcGzrskqtjEntity;

/**
 * 定义考勤统计 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzrskqtjDao")
public class JdbcGzrskqtjDao extends JdbcBaseDaoImp<GzrskqtjEntity, JdbcGzrskqtjEntity> implements GzrskqtjDao {

}
