package com.itfreer.gzryrqxk.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzryrqxk.entity.GzryrqxkEntity;

/**
 * 定义外籍人员入区许可、外来科研人员入区许可实体
 */
 @Entity(name = "GZ_RYRQXK")
public class JdbcGzryrqxkEntity implements JdbcBaseEntity<GzryrqxkEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	@Column(name = "s_exeid")
	private String sexeid;
	
	
	/**
	 * 项目名称
	 */
	@Column(name = "S_XMMC", length = 500, nullable = true)
	private String sxmmc;
	
	
	public String getSxmmc() {
		return sxmmc;
	}

	public void setSxmmc(String sxmmc) {
		this.sxmmc = sxmmc;
	}


	/**
	 * 项目ID
	 */
	@Column(name = "XMID", length = 500, nullable = true)
	private String xmid;
	
	public String getXmid() {
		return xmid;
	}

	public void setXmid(String xmid) {
		this.xmid = xmid;
	}
	
	
	/**
	 * 许可类型
	 */
	@Column(name = "S_XKLX", length = 500, nullable = true)
	private String sxklx;
	
	
	/**
	 * 适用范围
	 */
	@Column(name = "S_SYFW", length = 500, nullable = true)
	private String ssyfw;
	
	
	/**
	 * 办理机构
	 */
	@Column(name = "S_BLJG", length = 500, nullable = true)
	private String sbljg;
	
	
	/**
	 * 责任处(科)室
	 */
	@Column(name = "S_ZRCS", length = 500, nullable = true)
	private String szrcs;
	
	
	/**
	 * 申请人
	 */
	@Column(name = "S_SQR", length = 500, nullable = true)
	private String ssqr;
	
	/**
	 * 申请时间
	 */
	@Column(name = "D_SQSJ", nullable = true)
	private java.util.Date dsqsj;
	
	
	/**
	 * 申请方式
	 */
	@Column(name = "S_SQFS", length = 500, nullable = true)
	private String ssqfs;
	
	
	/**
	 * 联系方式
	 */
	@Column(name = "S_LXFS", length = 500, nullable = true)
	private String slxfs;
	
	
	/**
	 * 申请材料
	 */
	@Column(name = "S_SQCL", length = 500, nullable = true)
	private String ssqcl;
	
	
	/**
	 * 入区许可状态
	 */
	@Column(name = "S_RQXKZT", length = 500, nullable = true)
	private String srqxkzt;
	
	
	/**
	 * 经办人
	 */
	@Column(name = "S_JBR", length = 500, nullable = true)
	private String sjbr;
	
	/**
	 * 许可开始时间
	 */
	@Column(name = "D_XKKSSJ", nullable = true)
	private java.util.Date dxkkssj;
	
	/**
	 * 许可结束时间
	 */
	@Column(name = "D_XKJSSJ", nullable = true)
	private java.util.Date dxkjssj;
	
	
	/**
	 * 备注
	 */
	@Column(name = "S_BZ", length = 500, nullable = true)
	private String sbz;
	
	/**
	 * 附件
	 */
	@Column(name = "FJ", length = 500, nullable = true)
	private String fj;

	public String getFj() {
		return fj;
	}

	public void setFj(String fj) {
		this.fj = fj;
	}

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 许可类型
	 */
	public String getSxklx() {
		return sxklx;
	}

	/**
	 * 许可类型
	 */
	public void setSxklx(String value) {
		this.sxklx = value;
	}
	/**
	 * 适用范围
	 */
	public String getSsyfw() {
		return ssyfw;
	}

	/**
	 * 适用范围
	 */
	public void setSsyfw(String value) {
		this.ssyfw = value;
	}
	/**
	 * 办理机构
	 */
	public String getSbljg() {
		return sbljg;
	}

	/**
	 * 办理机构
	 */
	public void setSbljg(String value) {
		this.sbljg = value;
	}
	/**
	 * 责任处(科)室
	 */
	public String getSzrcs() {
		return szrcs;
	}

	/**
	 * 责任处(科)室
	 */
	public void setSzrcs(String value) {
		this.szrcs = value;
	}
	/**
	 * 申请人
	 */
	public String getSsqr() {
		return ssqr;
	}

	/**
	 * 申请人
	 */
	public void setSsqr(String value) {
		this.ssqr = value;
	}
	/**
	 * 申请时间
	 */
	public java.util.Date getDsqsj() {
		return dsqsj;
	}

	/**
	 * 申请时间
	 */
	public void setDsqsj(java.util.Date value) {
		this.dsqsj = value;
	}
	/**
	 * 申请方式
	 */
	public String getSsqfs() {
		return ssqfs;
	}

	/**
	 * 申请方式
	 */
	public void setSsqfs(String value) {
		this.ssqfs = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxfs() {
		return slxfs;
	}

	/**
	 * 联系方式
	 */
	public void setSlxfs(String value) {
		this.slxfs = value;
	}
	/**
	 * 申请材料
	 */
	public String getSsqcl() {
		return ssqcl;
	}

	/**
	 * 申请材料
	 */
	public void setSsqcl(String value) {
		this.ssqcl = value;
	}
	/**
	 * 入区许可状态
	 */
	public String getSrqxkzt() {
		return srqxkzt;
	}

	/**
	 * 入区许可状态
	 */
	public void setSrqxkzt(String value) {
		this.srqxkzt = value;
	}
	/**
	 * 经办人
	 */
	public String getSjbr() {
		return sjbr;
	}

	/**
	 * 经办人
	 */
	public void setSjbr(String value) {
		this.sjbr = value;
	}
	/**
	 * 许可开始时间
	 */
	public java.util.Date getDxkkssj() {
		return dxkkssj;
	}

	/**
	 * 许可开始时间
	 */
	public void setDxkkssj(java.util.Date value) {
		this.dxkkssj = value;
	}
	/**
	 * 许可结束时间
	 */
	public java.util.Date getDxkjssj() {
		return dxkjssj;
	}

	/**
	 * 许可结束时间
	 */
	public void setDxkjssj(java.util.Date value) {
		this.dxkjssj = value;
	}
	/**
	 * 备注
	 */
	public String getSbz() {
		return sbz;
	}

	/**
	 * 备注
	 */
	public void setSbz(String value) {
		this.sbz = value;
	}
	
	

	public String getSexeid() {
		return sexeid;
	}

	public void setSexeid(String sexeid) {
		this.sexeid = sexeid;
	}

	@Override
	public void from(GzryrqxkEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzryrqxkEntity toEntity() {
		GzryrqxkEntity toEntity = new GzryrqxkEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}