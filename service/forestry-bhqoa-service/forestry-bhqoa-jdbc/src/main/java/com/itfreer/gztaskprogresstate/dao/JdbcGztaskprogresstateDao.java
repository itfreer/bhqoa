package com.itfreer.gztaskprogresstate.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gztaskprogresstate.dao.GztaskprogresstateDao;
import com.itfreer.gztaskprogresstate.entity.GztaskprogresstateEntity;
import com.itfreer.gztaskprogresstate.entity.JdbcGztaskprogresstateEntity;

/**
 * 定义任务监测 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGztaskprogresstateDao")
public class JdbcGztaskprogresstateDao extends JdbcBaseDaoImp<GztaskprogresstateEntity, JdbcGztaskprogresstateEntity> implements GztaskprogresstateDao {

}
