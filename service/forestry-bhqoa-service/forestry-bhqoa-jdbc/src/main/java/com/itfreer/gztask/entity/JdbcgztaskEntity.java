package com.itfreer.gztask.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gztask.entity.gztaskEntity;

/**
 * 定义任务安排实体
 */
 @Entity(name = "GZ_TASK")
public class JdbcgztaskEntity implements JdbcBaseEntity<gztaskEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 任务ID
	 */
	@Column(name = "FWID", length = 500, nullable = true)
	private String fwid;
	
	
	/**
	 * 接收人ID
	 */
	@Column(name = "JSRID", length = 500, nullable = true)
	private String jsrid;
	
	/**
	 * 接收人名称
	 */
	@Column(name = "JSRMC", length = 500, nullable = true)
	private String jsrmc;
	
	
	/**
	 * 接收人机构
	 */
	@Column(name = "JSRJG", length = 500, nullable = true)
	private String jsrjg;
	
	
	/**
	 * 接收人部门
	 */
	@Column(name = "JSRJGMC", length = 500, nullable = true)
	private String jsrjgmc;
	

	public String getJsrjgmc() {
		return jsrjgmc;
	}

	public void setJsrjgmc(String jsrjgmc) {
		this.jsrjgmc = jsrjgmc;
	}
	
	
	/**
	 * 确认时间
	 */
	@Column(name = "QRSJ", nullable = true)
	private java.util.Date qrsj;
	
	
	/**
	 * 是否确认
	 */
	@Column(name = "SFQR", length = 500, nullable = true)
	private String sfqr;
	
	
	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	
	public String getFwid() {
		return fwid;
	}

	public void setFwid(String fwid) {
		this.fwid = fwid;
	}

	public String getJsrid() {
		return jsrid;
	}

	public void setJsrid(String jsrid) {
		this.jsrid = jsrid;
	}

	public String getJsrmc() {
		return jsrmc;
	}

	public void setJsrmc(String jsrmc) {
		this.jsrmc = jsrmc;
	}

	public String getJsrjg() {
		return jsrjg;
	}

	public void setJsrjg(String jsrjg) {
		this.jsrjg = jsrjg;
	}

	public java.util.Date getQrsj() {
		return qrsj;
	}

	public void setQrsj(java.util.Date qrsj) {
		this.qrsj = qrsj;
	}

	public String getSfqr() {
		return sfqr;
	}

	public void setSfqr(String sfqr) {
		this.sfqr = sfqr;
	}


	@Override
	public void from(gztaskEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public gztaskEntity toEntity() {
		gztaskEntity toEntity = new gztaskEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}