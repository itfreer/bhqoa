package com.itfreer.gzxmjsxk.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzxmjsxk.dao.GzxmjsxkDao;
import com.itfreer.gzxmjsxk.entity.GzxmjsxkEntity;
import com.itfreer.gzxmjsxk.entity.JdbcGzxmjsxkEntity;

/**
 * 定义项目建设许可 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzxmjsxkDao")
public class JdbcGzxmjsxkDao extends JdbcBaseDaoImp<GzxmjsxkEntity, JdbcGzxmjsxkEntity> implements GzxmjsxkDao {

}
