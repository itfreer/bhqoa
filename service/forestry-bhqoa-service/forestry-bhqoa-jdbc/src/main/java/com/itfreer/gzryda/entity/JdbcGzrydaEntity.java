package com.itfreer.gzryda.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzryda.entity.GzrydaEntity;

/**
 * 定义人事档案实体
 */
 @Entity(name = "GZ_RYDA")
public class JdbcGzrydaEntity implements JdbcBaseEntity<GzrydaEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 姓名
	 */
	@Column(name = "XING_MING", length = 500, nullable = true)
	private String xingming;
	
	
	/**
	 * 性别
	 */
	@Column(name = "XING_BIE", length = 500, nullable = true)
	private String xingbie;
	
	/**
	 * 出生日期
	 */
	@Column(name = "CHU_SHENG_RQ", nullable = true)
	private java.util.Date chushengrq;
	
	
	/**
	 * 身份证证号
	 */
	@Column(name = "SHENG_FEN_Z", length = 500, nullable = true)
	private String shengfenz;
	
	
	/**
	 * 民族
	 */
	@Column(name = "MING_ZU", length = 500, nullable = true)
	private String mingzu;
	
	
	/**
	 * 籍贯
	 */
	@Column(name = "JI_GUANG", length = 500, nullable = true)
	private String jiguang;
	
	
	/**
	 * 政治面貌
	 */
	@Column(name = "ZHENG_ZHI_MM", length = 500, nullable = true)
	private String zhengzhimm;
	
	/**
	 * 工作时间
	 */
	@Column(name = "GONG_ZUO_SJ", nullable = true)
	private java.util.Date gongzuosj;
	
	
	/**
	 * 学历
	 */
	@Column(name = "XUE_LI", length = 500, nullable = true)
	private String xueli;
	
	
	/**
	 * 毕业学校
	 */
	@Column(name = "BI_YE_XX", length = 500, nullable = true)
	private String biyexx;
	
	/**
	 * 毕业时间
	 */
	@Column(name = "BI_YE_SJ", nullable = true)
	private java.util.Date biyesj;
	
	
	/**
	 * 专业
	 */
	@Column(name = "ZHUANG_YE", length = 500, nullable = true)
	private String zhuangye;
	
	
	/**
	 * 行政职务
	 */
	@Column(name = "XING_ZHENG_ZW", length = 500, nullable = true)
	private String xingzhengzw;
	
	
	/**
	 * 技术职务
	 */
	@Column(name = "JI_SHU_ZW", length = 500, nullable = true)
	private String jishuzw;
	
	
	/**
	 * 部门
	 */
	@Column(name = "BU_MEN", length = 500, nullable = true)
	private String bumen;
	
	
	/**
	 * 职务
	 */
	@Column(name = "ZHI_WU", length = 500, nullable = true)
	private String zhiwu;
	
	
	/**
	 * 岗位类型
	 */
	@Column(name = "GAGN_WEI_LX", length = 500, nullable = true)
	private String gagnweilx;
	
	
	/**
	 * 备注
	 */
	@Column(name = "BEI_ZHU", length = 500, nullable = true)
	private String beizhu;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 姓名
	 */
	public String getXingming() {
		return xingming;
	}

	/**
	 * 姓名
	 */
	public void setXingming(String value) {
		this.xingming = value;
	}
	/**
	 * 性别
	 */
	public String getXingbie() {
		return xingbie;
	}

	/**
	 * 性别
	 */
	public void setXingbie(String value) {
		this.xingbie = value;
	}
	/**
	 * 出生日期
	 */
	public java.util.Date getChushengrq() {
		return chushengrq;
	}

	/**
	 * 出生日期
	 */
	public void setChushengrq(java.util.Date value) {
		this.chushengrq = value;
	}
	/**
	 * 身份证证号
	 */
	public String getShengfenz() {
		return shengfenz;
	}

	/**
	 * 身份证证号
	 */
	public void setShengfenz(String value) {
		this.shengfenz = value;
	}
	/**
	 * 民族
	 */
	public String getMingzu() {
		return mingzu;
	}

	/**
	 * 民族
	 */
	public void setMingzu(String value) {
		this.mingzu = value;
	}
	/**
	 * 籍贯
	 */
	public String getJiguang() {
		return jiguang;
	}

	/**
	 * 籍贯
	 */
	public void setJiguang(String value) {
		this.jiguang = value;
	}
	/**
	 * 政治面貌
	 */
	public String getZhengzhimm() {
		return zhengzhimm;
	}

	/**
	 * 政治面貌
	 */
	public void setZhengzhimm(String value) {
		this.zhengzhimm = value;
	}
	/**
	 * 工作时间
	 */
	public java.util.Date getGongzuosj() {
		return gongzuosj;
	}

	/**
	 * 工作时间
	 */
	public void setGongzuosj(java.util.Date value) {
		this.gongzuosj = value;
	}
	/**
	 * 学历
	 */
	public String getXueli() {
		return xueli;
	}

	/**
	 * 学历
	 */
	public void setXueli(String value) {
		this.xueli = value;
	}
	/**
	 * 毕业学校
	 */
	public String getBiyexx() {
		return biyexx;
	}

	/**
	 * 毕业学校
	 */
	public void setBiyexx(String value) {
		this.biyexx = value;
	}
	/**
	 * 毕业时间
	 */
	public java.util.Date getBiyesj() {
		return biyesj;
	}

	/**
	 * 毕业时间
	 */
	public void setBiyesj(java.util.Date value) {
		this.biyesj = value;
	}
	/**
	 * 专业
	 */
	public String getZhuangye() {
		return zhuangye;
	}

	/**
	 * 专业
	 */
	public void setZhuangye(String value) {
		this.zhuangye = value;
	}
	/**
	 * 行政职务
	 */
	public String getXingzhengzw() {
		return xingzhengzw;
	}

	/**
	 * 行政职务
	 */
	public void setXingzhengzw(String value) {
		this.xingzhengzw = value;
	}
	/**
	 * 技术职务
	 */
	public String getJishuzw() {
		return jishuzw;
	}

	/**
	 * 技术职务
	 */
	public void setJishuzw(String value) {
		this.jishuzw = value;
	}
	/**
	 * 部门
	 */
	public String getBumen() {
		return bumen;
	}

	/**
	 * 部门
	 */
	public void setBumen(String value) {
		this.bumen = value;
	}
	/**
	 * 职务
	 */
	public String getZhiwu() {
		return zhiwu;
	}

	/**
	 * 职务
	 */
	public void setZhiwu(String value) {
		this.zhiwu = value;
	}
	/**
	 * 岗位类型
	 */
	public String getGagnweilx() {
		return gagnweilx;
	}

	/**
	 * 岗位类型
	 */
	public void setGagnweilx(String value) {
		this.gagnweilx = value;
	}
	/**
	 * 备注
	 */
	public String getBeizhu() {
		return beizhu;
	}

	/**
	 * 备注
	 */
	public void setBeizhu(String value) {
		this.beizhu = value;
	}

	@Override
	public void from(GzrydaEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzrydaEntity toEntity() {
		GzrydaEntity toEntity = new GzrydaEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}