package com.itfreer.gzdevice.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzdevice.dao.GzdeviceDao;
import com.itfreer.gzdevice.entity.GzdeviceEntity;
import com.itfreer.gzdevice.entity.JdbcGzdeviceEntity;

/**
 * 定义设备 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzdeviceDao")
public class JdbcGzdeviceDao extends JdbcBaseDaoImp<GzdeviceEntity, JdbcGzdeviceEntity> implements GzdeviceDao {

}
