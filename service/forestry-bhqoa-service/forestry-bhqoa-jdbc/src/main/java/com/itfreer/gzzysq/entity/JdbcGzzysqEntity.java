package com.itfreer.gzzysq.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzzysq.entity.GzzysqEntity;

/**
 * 定义资源申请实体
 */
 @Entity(name = "GZ_ZYSQ")
public class JdbcGzzysqEntity implements JdbcBaseEntity<GzzysqEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 资源类型
	 */
	@Column(name = "S_ZYLX", length = 500, nullable = true)
	private String szylx;
	
	
	/**
	 * 使用人
	 */
	@Column(name = "S_SYR", length = 500, nullable = true)
	private String ssyr;
	
	
	/**
	 * 申请原因
	 */
	@Column(name = "S_SQYY", length = 500, nullable = true)
	private String ssqyy;
	
	
	/**
	 * 联系方式
	 */
	@Column(name = "S_LXFS", length = 500, nullable = true)
	private String slxfs;
	
	/**
	 * 使用时间
	 */
	@Column(name = "D_SYSJ", nullable = true)
	private java.util.Date dsysj;
	
	
	/**
	 * 所属项目
	 */
	@Column(name = "S_SSXM", length = 500, nullable = true)
	private String sssxm;
	
	
	/**
	 * 申请时间
	 */
	@Column(name = "D_SQSJ", nullable = true)
	private java.util.Date dsqsj;
	
	/**
	 * 使用开始时间
	 */
	@Column(name = "D_SYKSSJ", nullable = true)
	private java.util.Date dsykssj;
	
	/**
	 * 使用结束时间
	 */
	@Column(name = "D_SYJSSJ", nullable = true)
	private java.util.Date dsyjssj;

	public java.util.Date getDsqsj() {
		return dsqsj;
	}

	public void setDsqsj(java.util.Date dsqsj) {
		this.dsqsj = dsqsj;
	}

	public java.util.Date getDsykssj() {
		return dsykssj;
	}

	public void setDsykssj(java.util.Date dsykssj) {
		this.dsykssj = dsykssj;
	}

	public java.util.Date getDsyjssj() {
		return dsyjssj;
	}

	public void setDsyjssj(java.util.Date dsyjssj) {
		this.dsyjssj = dsyjssj;
	}

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 资源类型
	 */
	public String getSzylx() {
		return szylx;
	}

	/**
	 * 资源类型
	 */
	public void setSzylx(String value) {
		this.szylx = value;
	}
	/**
	 * 使用人
	 */
	public String getSsyr() {
		return ssyr;
	}

	/**
	 * 使用人
	 */
	public void setSsyr(String value) {
		this.ssyr = value;
	}
	/**
	 * 申请原因
	 */
	public String getSsqyy() {
		return ssqyy;
	}

	/**
	 * 申请原因
	 */
	public void setSsqyy(String value) {
		this.ssqyy = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxfs() {
		return slxfs;
	}

	/**
	 * 联系方式
	 */
	public void setSlxfs(String value) {
		this.slxfs = value;
	}
	/**
	 * 使用时间
	 */
	public java.util.Date getDsysj() {
		return dsysj;
	}

	/**
	 * 使用时间
	 */
	public void setDsysj(java.util.Date value) {
		this.dsysj = value;
	}
	/**
	 * 所属项目
	 */
	public String getSssxm() {
		return sssxm;
	}
	
	@Column(name = "s_exeid")
	private String sexeid;
	
	

	public String getSexeid() {
		return sexeid;
	}

	public void setSexeid(String sexeid) {
		this.sexeid = sexeid;
	}

	/**
	 * 所属项目
	 */
	public void setSssxm(String value) {
		this.sssxm = value;
	}

	@Override
	public void from(GzzysqEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzzysqEntity toEntity() {
		GzzysqEntity toEntity = new GzzysqEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}