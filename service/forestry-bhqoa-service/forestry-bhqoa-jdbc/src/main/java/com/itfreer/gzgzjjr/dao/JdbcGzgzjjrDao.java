package com.itfreer.gzgzjjr.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzgzjjr.dao.GzgzjjrDao;
import com.itfreer.gzgzjjr.entity.GzgzjjrEntity;
import com.itfreer.gzgzjjr.entity.JdbcGzgzjjrEntity;

/**
 * 定义工作节假日 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzgzjjrDao")
public class JdbcGzgzjjrDao extends JdbcBaseDaoImp<GzgzjjrEntity, JdbcGzgzjjrEntity> implements GzgzjjrDao {

}
