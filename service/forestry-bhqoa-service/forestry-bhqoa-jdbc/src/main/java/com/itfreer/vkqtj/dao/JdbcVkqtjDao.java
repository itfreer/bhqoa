package com.itfreer.vkqtj.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.vkqtj.dao.VkqtjDao;
import com.itfreer.vkqtj.entity.VkqtjEntity;
import com.itfreer.vkqtj.entity.JdbcVkqtjEntity;

/**
 * 定义考勤统计 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcVkqtjDao")
public class JdbcVkqtjDao extends JdbcBaseDaoImp<VkqtjEntity, JdbcVkqtjEntity> implements VkqtjDao {

}
