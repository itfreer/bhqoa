package com.itfreer.gzsqxmtjview.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzsqxmtjview.dao.GzsqxmtjviewDao;
import com.itfreer.gzsqxmtjview.entity.GzsqxmtjviewEntity;
import com.itfreer.gzsqxmtjview.entity.JdbcGzsqxmtjviewEntity;

/**
 * 定义社区项目统计(视图) jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzsqxmtjviewDao")
public class JdbcGzsqxmtjviewDao extends JdbcBaseDaoImp<GzsqxmtjviewEntity, JdbcGzsqxmtjviewEntity> implements GzsqxmtjviewDao {

}
