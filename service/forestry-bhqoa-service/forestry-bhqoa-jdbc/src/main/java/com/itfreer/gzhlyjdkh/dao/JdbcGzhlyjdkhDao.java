package com.itfreer.gzhlyjdkh.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzhlyjdkh.dao.GzhlyjdkhDao;
import com.itfreer.gzhlyjdkh.entity.GzhlyjdkhEntity;
import com.itfreer.gzhlyjdkh.entity.JdbcGzhlyjdkhEntity;

/**
 * 定义护林员季度考核 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzhlyjdkhDao")
public class JdbcGzhlyjdkhDao extends JdbcBaseDaoImp<GzhlyjdkhEntity, JdbcGzhlyjdkhEntity> implements GzhlyjdkhDao {

}
