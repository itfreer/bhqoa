package com.itfreer.gzdevicepurchase.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzdevicepurchase.dao.GzdevicepurchaseDao;
import com.itfreer.gzdevicepurchase.entity.GzdevicepurchaseEntity;
import com.itfreer.gzdevicepurchase.entity.JdbcGzdevicepurchaseEntity;

/**
 * 定义设备采购管理 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzdevicepurchaseDao")
public class JdbcGzdevicepurchaseDao extends JdbcBaseDaoImp<GzdevicepurchaseEntity, JdbcGzdevicepurchaseEntity> implements GzdevicepurchaseDao {

}
