package com.itfreer.gzschedule.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzschedule.dao.ScheduleDao;
import com.itfreer.gzschedule.entity.ScheduleEntity;
import com.itfreer.gzschedule.entity.JdbcScheduleEntity;

/**
 * 定义日常安排 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcScheduleDao")
public class JdbcScheduleDao extends JdbcBaseDaoImp<ScheduleEntity, JdbcScheduleEntity> implements ScheduleDao {

}
