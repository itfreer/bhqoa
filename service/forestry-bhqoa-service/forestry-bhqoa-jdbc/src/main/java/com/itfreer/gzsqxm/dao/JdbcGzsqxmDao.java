package com.itfreer.gzsqxm.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzsqxm.dao.GzsqxmDao;
import com.itfreer.gzsqxm.entity.GzsqxmEntity;
import com.itfreer.gzsqxm.entity.JdbcGzsqxmEntity;

/**
 * 定义社区项目申请/台账 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzsqxmDao")
public class JdbcGzsqxmDao extends JdbcBaseDaoImp<GzsqxmEntity, JdbcGzsqxmEntity> implements GzsqxmDao {

}
