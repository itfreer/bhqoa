package com.itfreer.gzvisitorreserve.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzvisitorreserve.dao.GzvisitorreserveDao;
import com.itfreer.gzvisitorreserve.entity.GzvisitorreserveEntity;
import com.itfreer.gzvisitorreserve.entity.JdbcGzvisitorreserveEntity;

/**
 * 定义访客预约管理 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzvisitorreserveDao")
public class JdbcGzvisitorreserveDao extends JdbcBaseDaoImp<GzvisitorreserveEntity, JdbcGzvisitorreserveEntity> implements GzvisitorreserveDao {

}
