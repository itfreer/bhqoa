package com.itfreer.gzhlyndkh.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzhlyndkh.dao.GzhlyndkhDao;
import com.itfreer.gzhlyndkh.entity.GzhlyndkhEntity;
import com.itfreer.gzhlyndkh.entity.JdbcGzhlyndkhEntity;

/**
 * 定义护林员年度考核 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzhlyndkhDao")
public class JdbcGzhlyndkhDao extends JdbcBaseDaoImp<GzhlyndkhEntity, JdbcGzhlyndkhEntity> implements GzhlyndkhDao {

}
