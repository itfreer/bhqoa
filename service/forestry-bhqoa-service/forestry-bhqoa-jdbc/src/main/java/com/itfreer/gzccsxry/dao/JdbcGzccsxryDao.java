package com.itfreer.gzccsxry.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzccsxry.dao.GzccsxryDao;
import com.itfreer.gzccsxry.entity.GzccsxryEntity;
import com.itfreer.gzccsxry.entity.JdbcGzccsxryEntity;

/**
 * 定义出差随行人员 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzccsxryDao")
public class JdbcGzccsxryDao extends JdbcBaseDaoImp<GzccsxryEntity, JdbcGzccsxryEntity> implements GzccsxryDao {

}
