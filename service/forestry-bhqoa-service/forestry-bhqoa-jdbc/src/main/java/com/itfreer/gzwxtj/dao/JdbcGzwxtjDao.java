package com.itfreer.gzwxtj.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzwxtj.dao.GzwxtjDao;
import com.itfreer.gzwxtj.entity.GzwxtjEntity;
import com.itfreer.gzwxtj.entity.JdbcGzwxtjEntity;

/**
 * 定义维修统计 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzwxtjDao")
public class JdbcGzwxtjDao extends JdbcBaseDaoImp<GzwxtjEntity, JdbcGzwxtjEntity> implements GzwxtjDao {

}
