package com.itfreer.gzclwx.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzclwx.dao.GzclwxDao;
import com.itfreer.gzclwx.entity.GzclwxEntity;
import com.itfreer.gzclwx.entity.JdbcGzclwxEntity;

/**
 * 定义车辆维修 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzclwxDao")
public class JdbcGzclwxDao extends JdbcBaseDaoImp<GzclwxEntity, JdbcGzclwxEntity> implements GzclwxDao {

}
