package com.itfreer.gzykcz.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzykcz.dao.GzykczDao;
import com.itfreer.gzykcz.entity.GzykczEntity;
import com.itfreer.gzykcz.entity.JdbcGzykczEntity;

/**
 * 定义油卡充值 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzykczDao")
public class JdbcGzykczDao extends JdbcBaseDaoImp<GzykczEntity, JdbcGzykczEntity> implements GzykczDao {

}
