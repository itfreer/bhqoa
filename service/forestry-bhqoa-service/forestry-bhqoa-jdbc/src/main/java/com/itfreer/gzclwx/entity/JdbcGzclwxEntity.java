package com.itfreer.gzclwx.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzclwx.entity.GzclwxEntity;

/**
 * 定义车辆维修实体
 */
 @Entity(name = "GZ_CLWX")
public class JdbcGzclwxEntity implements JdbcBaseEntity<GzclwxEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	@Column(name = "S_CLXX", length = 500, nullable = true)
	private String sclxx;
	
	/**
	 * 维修时间
	 */
	@Column(name = "D_WXSJ", nullable = true)
	private java.util.Date dwxsj;
	
	
	/**
	 * 维修原因
	 */
	@Column(name = "S_WXYY", length = 500, nullable = true)
	private String swxyy;
	
	
	/**
	 * 维修价格
	 */
	@Column(name = "S_WXJG", length = 500, nullable = true)
	private String swxjg;
	
	
	/**
	 * 维修地点
	 */
	@Column(name = "S_WXDD", length = 500, nullable = true)
	private String swxdd;
	
	
	/**
	 * 负责人
	 */
	@Column(name = "S_FZR", length = 500, nullable = true)
	private String sfzr;
	
	
	/**
	 * 负责人名称
	 */
	@Column(name = "S_FZRMC", length = 500, nullable = true)
	private String sfzrmc;
	
	
	public String getSfzrmc() {
		return sfzrmc;
	}

	public void setSfzrmc(String sfzrmc) {
		this.sfzrmc = sfzrmc;
	}

	/**
	 * 联系方式
	 */
	@Column(name = "S_LXFF", length = 500, nullable = true)
	private String slxff;
	
	
	/**
	 * 维修费用
	 */
	@Column(name = "S_WXFY", length = 500, nullable = true)
	private String swxfy;
	
	
	/**
	 * 电子发票
	 */
	@Column(name = "S_DZFP", length = 500, nullable = true)
	private String sdzfp;
	
	/**
	 * 车辆型号
	 */
	@Column(name = "CLXH", length = 500, nullable = true)
	private String clxh;
	
	/**
	 * 车牌号
	 */
	@Column(name = "CPH", length = 500, nullable = true)
	private String cph;
	
	
	/**
	 * 维修状态
	 */
	@Column(name = "WXZT", length = 500, nullable = true)
	private String wxzt;
	

	public String getWxzt() {
		return wxzt;
	}

	public void setWxzt(String wxzt) {
		this.wxzt = wxzt;
	}

	public String getClxh() {
		return clxh;
	}

	public void setClxh(String clxh) {
		this.clxh = clxh;
	}

	public String getCph() {
		return cph;
	}

	public void setCph(String cph) {
		this.cph = cph;
	}

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	public String getSclxx() {
		return sclxx;
	}

	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	public void setSclxx(String value) {
		this.sclxx = value;
	}
	/**
	 * 维修时间
	 */
	public java.util.Date getDwxsj() {
		return dwxsj;
	}

	/**
	 * 维修时间
	 */
	public void setDwxsj(java.util.Date value) {
		this.dwxsj = value;
	}
	/**
	 * 维修原因
	 */
	public String getSwxyy() {
		return swxyy;
	}

	/**
	 * 维修原因
	 */
	public void setSwxyy(String value) {
		this.swxyy = value;
	}
	/**
	 * 维修价格
	 */
	public String getSwxjg() {
		return swxjg;
	}

	/**
	 * 维修价格
	 */
	public void setSwxjg(String value) {
		this.swxjg = value;
	}
	/**
	 * 维修地点
	 */
	public String getSwxdd() {
		return swxdd;
	}

	/**
	 * 维修地点
	 */
	public void setSwxdd(String value) {
		this.swxdd = value;
	}
	/**
	 * 负责人
	 */
	public String getSfzr() {
		return sfzr;
	}

	/**
	 * 负责人
	 */
	public void setSfzr(String value) {
		this.sfzr = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxff() {
		return slxff;
	}

	/**
	 * 联系方式
	 */
	public void setSlxff(String value) {
		this.slxff = value;
	}
	/**
	 * 维修费用
	 */
	public String getSwxfy() {
		return swxfy;
	}

	/**
	 * 维修费用
	 */
	public void setSwxfy(String value) {
		this.swxfy = value;
	}
	/**
	 * 电子发票
	 */
	public String getSdzfp() {
		return sdzfp;
	}

	/**
	 * 电子发票
	 */
	public void setSdzfp(String value) {
		this.sdzfp = value;
	}

	@Override
	public void from(GzclwxEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzclwxEntity toEntity() {
		GzclwxEntity toEntity = new GzclwxEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}