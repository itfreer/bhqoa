package com.itfreer.gzclda.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzclda.dao.GzcldaDao;
import com.itfreer.gzclda.entity.GzcldaEntity;
import com.itfreer.gzclda.entity.JdbcGzcldaEntity;

/**
 * 定义车辆档案 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzcldaDao")
public class JdbcGzcldaDao extends JdbcBaseDaoImp<GzcldaEntity, JdbcGzcldaEntity> implements GzcldaDao {

}
