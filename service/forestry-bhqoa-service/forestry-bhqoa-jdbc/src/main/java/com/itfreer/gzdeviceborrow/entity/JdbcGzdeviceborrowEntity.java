package com.itfreer.gzdeviceborrow.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzdeviceborrow.entity.GzdeviceborrowEntity;

/**
 * 定义设备借还实体
 */
 @Entity(name = "GZ_DEVICE_BORROW")
public class JdbcGzdeviceborrowEntity implements JdbcBaseEntity<GzdeviceborrowEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 100, nullable = true)
	private String id;
	
	/**
	 * I_VERSION
	 */
	@Column(name = "I_VERSION", nullable = true)
	private Integer iversion;
	
	
	/**
	 * 申请编号
	 */
	@Column(name = "S_SQBH", length = 50, nullable = true)
	private String ssqbh;
	
	/**
	 * 申请日期
	 */
	@Column(name = "D_SQRQ", nullable = true)
	private java.util.Date dsqrq;
	
	
	/**
	 * 使用单位
	 */
	@Column(name = "S_SYDW", length = 50, nullable = true)
	private String ssydw;
	
	
	/**
	 * 使用项目
	 */
	@Column(name = "S_SYXM", length = 50, nullable = true)
	private String ssyxm;
	
	
	/**
	 * 申请人
	 */
	@Column(name = "S_SQR", length = 50, nullable = true)
	private String ssqr;
	
	/**
	 * 预还日期
	 */
	@Column(name = "D_YHRQ", nullable = true)
	private java.util.Date dyhrq;
	
	/**
	 * 归还日期
	 */
	@Column(name = "D_GHRQ", nullable = true)
	private java.util.Date dghrq;
	
	
	/**
	 * 归还人
	 */
	@Column(name = "S_GHR", length = 50, nullable = true)
	private String sghr;
	
	
	/**
	 * 备注
	 */
	@Column(name = "S_BZ", length = 400, nullable = true)
	private String sbz;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * I_VERSION
	 */
	public Integer getIversion() {
		return iversion;
	}

	/**
	 * I_VERSION
	 */
	public void setIversion(Integer value) {
		this.iversion = value;
	}
	/**
	 * 申请编号
	 */
	public String getSsqbh() {
		return ssqbh;
	}

	/**
	 * 申请编号
	 */
	public void setSsqbh(String value) {
		this.ssqbh = value;
	}
	/**
	 * 申请日期
	 */
	public java.util.Date getDsqrq() {
		return dsqrq;
	}

	/**
	 * 申请日期
	 */
	public void setDsqrq(java.util.Date value) {
		this.dsqrq = value;
	}
	/**
	 * 使用单位
	 */
	public String getSsydw() {
		return ssydw;
	}

	/**
	 * 使用单位
	 */
	public void setSsydw(String value) {
		this.ssydw = value;
	}
	/**
	 * 使用项目
	 */
	public String getSsyxm() {
		return ssyxm;
	}

	/**
	 * 使用项目
	 */
	public void setSsyxm(String value) {
		this.ssyxm = value;
	}
	/**
	 * 申请人
	 */
	public String getSsqr() {
		return ssqr;
	}

	/**
	 * 申请人
	 */
	public void setSsqr(String value) {
		this.ssqr = value;
	}
	/**
	 * 预还日期
	 */
	public java.util.Date getDyhrq() {
		return dyhrq;
	}

	/**
	 * 预还日期
	 */
	public void setDyhrq(java.util.Date value) {
		this.dyhrq = value;
	}
	/**
	 * 归还日期
	 */
	public java.util.Date getDghrq() {
		return dghrq;
	}

	/**
	 * 归还日期
	 */
	public void setDghrq(java.util.Date value) {
		this.dghrq = value;
	}
	/**
	 * 归还人
	 */
	public String getSghr() {
		return sghr;
	}

	/**
	 * 归还人
	 */
	public void setSghr(String value) {
		this.sghr = value;
	}
	/**
	 * 备注
	 */
	public String getSbz() {
		return sbz;
	}

	/**
	 * 备注
	 */
	public void setSbz(String value) {
		this.sbz = value;
	}

	@Override
	public void from(GzdeviceborrowEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzdeviceborrowEntity toEntity() {
		GzdeviceborrowEntity toEntity = new GzdeviceborrowEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}