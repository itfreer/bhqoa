package com.itfreer.gzwlkyxmsqb.entity;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.springframework.beans.BeanUtils;

import com.itfreer.bpm.api.IJdbcBpmBaseEntity;
import com.itfreer.gzkyxmcg.entity.GzkyxmcgEntity;
import com.itfreer.gzkyxmcg.entity.JdbcGzkyxmcgEntity;
import com.itfreer.gzwlkyxmcg.entity.GzwlkyxmcgEntity;
import com.itfreer.gzwlkyxmcg.entity.JdbcGzwlkyxmcgEntity;
import com.itfreer.gzwlkyxmlfry.entity.GzwlkyxmlfryEntity;
import com.itfreer.gzwlkyxmlfry.entity.JdbcGzwlkyxmlfryEntity;

/**
 * 定义外来科研项目申请表实体
 */
 @Entity(name = "GZ_WLKYXM_SQB")
public class JdbcGzwlkyxmsqbEntity implements IJdbcBpmBaseEntity<GzwlkyxmsqbEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 申请编号
	 */
	@Column(name = "SQBH", length = 500, nullable = true)
	private String sqbh;
	
	
	/**
	 * 单位ID
	 */
	@Column(name = "DWID", length = 500, nullable = true)
	private String dwid;
	
	
	/**
	 * 单位名称
	 */
	@Column(name = "DWMC", length = 500, nullable = true)
	private String dwmc;
	
	
	/**
	 * 项目名称
	 */
	@Column(name = "XMMC", length = 500, nullable = true)
	private String xmmc;
	
	
	/**
	 * 项目ID
	 */
	@Column(name = "XMID", length = 500, nullable = true)
	private String xmid;
	
	
	/**
	 * 是否有介绍信（函）
	 */
	@Column(name = "SFYJSX", length = 500, nullable = true)
	private String sfyjsx;
	
	
	/**
	 * 提供活动方案情况
	 */
	@Column(name = "TJHDFAQK", length = 500, nullable = true)
	private String tjhdfaqk;
	
	
	/**
	 * 是否鉴定合作协议
	 */
	@Column(name = "SJQDHZXY", length = 500, nullable = true)
	private String sjqdhzxy;
	
	
	/**
	 * 协议编号
	 */
	@Column(name = "XYBH", length = 500, nullable = true)
	private String xybh;
	
	
	/**
	 * 带队负责人ID
	 */
	@Column(name = "DDFZRID", length = 500, nullable = true)
	private String ddfzrid;
	
	
	/**
	 * 带队负责人名称
	 */
	@Column(name = "DDFZRMC", length = 500, nullable = true)
	private String ddfzrmc;
	
	
	/**
	 * 带队负责人职务ID
	 */
	@Column(name = "DDFZRZWID", length = 500, nullable = true)
	private String ddfzrzwid;
	
	
	/**
	 * 带队负责人职务名称
	 */
	@Column(name = "DDFZRZWMC", length = 500, nullable = true)
	private String ddfzrzwmc;
	
	
	/**
	 * 带队负责人联系电话
	 */
	@Column(name = "DDFZRLXDH", length = 500, nullable = true)
	private String ddfzrlxdh;
	
	/**
	 * 人数
	 */
	@Column(name = "RENSHU", nullable = true)
	private Double renshu;
	
	/**
	 * 来访时间开始时间
	 */
	@Column(name = "LFSJ", nullable = true)
	private java.util.Date lfsj;
	
	/**
	 * 来访时间结束时间
	 */
	@Column(name = "LFSJ_JSSJ", nullable = true)
	private java.util.Date lfsjjssj;
	
	
	/**
	 * 活动计划时限
	 */
	@Column(name = "HDJHSX", length = 500, nullable = true)
	private String hdjhsx;
	
	
	/**
	 * 项目简介
	 */
	@Column(name = "XMJJ", length = 500, nullable = true)
	private String xmjj;
	
	
	/**
	 * 管理站审批意见
	 */
	@Column(name = "GLZSPYJ", length = 500, nullable = true)
	private String glzspyj;
	
	
	/**
	 * 管理站审批人
	 */
	@Column(name = "GLZSPR", length = 500, nullable = true)
	private String glzspr;
	
	/**
	 * 管理站审批时间
	 */
	@Column(name = "GLZSPSJ", nullable = true)
	private java.util.Date glzspsj;
	
	
	/**
	 * 管理局审批意见
	 */
	@Column(name = "GLJSPYJ", length = 500, nullable = true)
	private String gljspyj;
	
	
	/**
	 * 管理局审批时间
	 */
	@Column(name = "GLJSPSJ", length = 500, nullable = true)
	private String gljspsj;
	
	/**
	 * 管理局审批人
	 */
	@Column(name = "GLJSPR", nullable = true)
	private java.util.Date gljspr;
	
	
	/**
	 * 成果材料
	 */
	@Column(name = "CGCL", length = 500, nullable = true)
	private String cgcl;
	
	
	/**
	 * 活动范围
	 */
	@Column(name = "HDFW", length = 500, nullable = true)
	private String hdfw;
	
	
	/**
	 * 注意事项
	 */
	@Column(name = "ZYSX", length = 500, nullable = true)
	private String zysx;
	
	
	/**
	 * 编号
	 */
	@Column(name = "BH", length = 500, nullable = true)
	private String bh;
	
	/**
	 * 负责部门
	 */
	@Column(name = "FZBM", length = 255, nullable = true)
	private String fzbm;
	
	/**
	 * 负责人
	 */
	@Column(name = "FZR", length = 255, nullable = true)
	private String fzr;
	
	/**
	 * 负责人ID
	 */
	@Column(name = "FZRID", length = 255, nullable = true)
	private String fzrid;
	
	public String getFzbm() {
		return fzbm;
	}

	public void setFzbm(String fzbm) {
		this.fzbm = fzbm;
	}

	public String getFzr() {
		return fzr;
	}

	public void setFzr(String fzr) {
		this.fzr = fzr;
	}

	public String getFzrid() {
		return fzrid;
	}

	public void setFzrid(String fzrid) {
		this.fzrid = fzrid;
	}
	

	public String getHdfw() {
		return hdfw;
	}

	public void setHdfw(String hdfw) {
		this.hdfw = hdfw;
	}

	public String getZysx() {
		return zysx;
	}

	public void setZysx(String zysx) {
		this.zysx = zysx;
	}

	public String getBh() {
		return bh;
	}

	public void setBh(String bh) {
		this.bh = bh;
	}

	public String getCgcl() {
		return cgcl;
	}

	public void setCgcl(String cgcl) {
		this.cgcl = cgcl;
	}
	
	/**
	 * 项目人员
	 */
	@SuppressWarnings("deprecation")
	@org.hibernate.annotations.ForeignKey(name = "none")
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "XMID")
	@OrderBy("ID ASC")
	private Set<JdbcGzwlkyxmlfryEntity> gzwlkyxmlfry = new LinkedHashSet<JdbcGzwlkyxmlfryEntity>();

	/**
	 * 项目人员
	 * @return
	 */
	public Set<JdbcGzwlkyxmlfryEntity> getGzwlkyxmlfry() {
		return gzwlkyxmlfry;
	}
	/**
	 * 项目人员
	 * @return
	 */
	public void setGzwlkyxmlfry(Set<JdbcGzwlkyxmlfryEntity> gzwlkyxmlfry) {
		this.gzwlkyxmlfry = gzwlkyxmlfry;
	}
	
	
	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 申请编号
	 */
	public String getSqbh() {
		return sqbh;
	}

	/**
	 * 申请编号
	 */
	public void setSqbh(String value) {
		this.sqbh = value;
	}
	/**
	 * 单位ID
	 */
	public String getDwid() {
		return dwid;
	}

	/**
	 * 单位ID
	 */
	public void setDwid(String value) {
		this.dwid = value;
	}
	/**
	 * 单位名称
	 */
	public String getDwmc() {
		return dwmc;
	}

	/**
	 * 单位名称
	 */
	public void setDwmc(String value) {
		this.dwmc = value;
	}
	/**
	 * 项目名称
	 */
	public String getXmmc() {
		return xmmc;
	}

	/**
	 * 项目名称
	 */
	public void setXmmc(String value) {
		this.xmmc = value;
	}
	/**
	 * 项目ID
	 */
	public String getXmid() {
		return xmid;
	}

	/**
	 * 项目ID
	 */
	public void setXmid(String value) {
		this.xmid = value;
	}
	/**
	 * 是否有介绍信（函）
	 */
	public String getSfyjsx() {
		return sfyjsx;
	}

	/**
	 * 是否有介绍信（函）
	 */
	public void setSfyjsx(String value) {
		this.sfyjsx = value;
	}
	/**
	 * 提供活动方案情况
	 */
	public String getTjhdfaqk() {
		return tjhdfaqk;
	}

	/**
	 * 提供活动方案情况
	 */
	public void setTjhdfaqk(String value) {
		this.tjhdfaqk = value;
	}
	/**
	 * 是否鉴定合作协议
	 */
	public String getSjqdhzxy() {
		return sjqdhzxy;
	}

	/**
	 * 是否鉴定合作协议
	 */
	public void setSjqdhzxy(String value) {
		this.sjqdhzxy = value;
	}
	/**
	 * 协议编号
	 */
	public String getXybh() {
		return xybh;
	}

	/**
	 * 协议编号
	 */
	public void setXybh(String value) {
		this.xybh = value;
	}
	/**
	 * 带队负责人ID
	 */
	public String getDdfzrid() {
		return ddfzrid;
	}

	/**
	 * 带队负责人ID
	 */
	public void setDdfzrid(String value) {
		this.ddfzrid = value;
	}
	/**
	 * 带队负责人名称
	 */
	public String getDdfzrmc() {
		return ddfzrmc;
	}

	/**
	 * 带队负责人名称
	 */
	public void setDdfzrmc(String value) {
		this.ddfzrmc = value;
	}
	/**
	 * 带队负责人职务ID
	 */
	public String getDdfzrzwid() {
		return ddfzrzwid;
	}

	/**
	 * 带队负责人职务ID
	 */
	public void setDdfzrzwid(String value) {
		this.ddfzrzwid = value;
	}
	/**
	 * 带队负责人职务名称
	 */
	public String getDdfzrzwmc() {
		return ddfzrzwmc;
	}

	/**
	 * 带队负责人职务名称
	 */
	public void setDdfzrzwmc(String value) {
		this.ddfzrzwmc = value;
	}
	/**
	 * 带队负责人联系电话
	 */
	public String getDdfzrlxdh() {
		return ddfzrlxdh;
	}

	/**
	 * 带队负责人联系电话
	 */
	public void setDdfzrlxdh(String value) {
		this.ddfzrlxdh = value;
	}
	/**
	 * 人数
	 */
	public Double getRenshu() {
		return renshu;
	}

	/**
	 * 人数
	 */
	public void setRenshu(Double value) {
		this.renshu = value;
	}
	/**
	 * 来访时间
	 */
	public java.util.Date getLfsj() {
		return lfsj;
	}

	/**
	 * 来访时间
	 */
	public void setLfsj(java.util.Date value) {
		this.lfsj = value;
	}
	/**
	 * 活动计划时限
	 */
	public String getHdjhsx() {
		return hdjhsx;
	}

	/**
	 * 活动计划时限
	 */
	public void setHdjhsx(String value) {
		this.hdjhsx = value;
	}
	/**
	 * 项目简介
	 */
	public String getXmjj() {
		return xmjj;
	}

	/**
	 * 项目简介
	 */
	public void setXmjj(String value) {
		this.xmjj = value;
	}
	/**
	 * 管理站审批意见
	 */
	public String getGlzspyj() {
		return glzspyj;
	}

	/**
	 * 管理站审批意见
	 */
	public void setGlzspyj(String value) {
		this.glzspyj = value;
	}
	/**
	 * 管理站审批人
	 */
	public String getGlzspr() {
		return glzspr;
	}

	/**
	 * 管理站审批人
	 */
	public void setGlzspr(String value) {
		this.glzspr = value;
	}
	/**
	 * 管理站审批时间
	 */
	public java.util.Date getGlzspsj() {
		return glzspsj;
	}

	/**
	 * 管理站审批时间
	 */
	public void setGlzspsj(java.util.Date value) {
		this.glzspsj = value;
	}
	/**
	 * 管理局审批意见
	 */
	public String getGljspyj() {
		return gljspyj;
	}

	/**
	 * 管理局审批意见
	 */
	public void setGljspyj(String value) {
		this.gljspyj = value;
	}
	/**
	 * 管理局审批时间
	 */
	public String getGljspsj() {
		return gljspsj;
	}

	/**
	 * 管理局审批时间
	 */
	public void setGljspsj(String value) {
		this.gljspsj = value;
	}
	/**
	 * 管理局审批人
	 */
	public java.util.Date getGljspr() {
		return gljspr;
	}

	/**
	 * 管理局审批人
	 */
	public void setGljspr(java.util.Date value) {
		this.gljspr = value;
	}

	
	/*@Override
	public void from(GzwlkyxmsqbEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzwlkyxmsqbEntity toEntity() {
		GzwlkyxmsqbEntity toEntity = new GzwlkyxmsqbEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}*/
	
	public java.util.Date getLfsjjssj() {
		return lfsjjssj;
	}

	public void setLfsjjssj(java.util.Date lfsjjssj) {
		this.lfsjjssj = lfsjjssj;
	}

	@Override
	public void from(GzwlkyxmsqbEntity t) {
		BeanUtils.copyProperties(t, this);
		
		//项目人员表-start-
		Set<JdbcGzwlkyxmlfryEntity> jdbcgzwlkyxmlfry = new LinkedHashSet<JdbcGzwlkyxmlfryEntity>();
		Set<GzwlkyxmlfryEntity> gzwlkyxmlfry = t.getGzwlkyxmlfry();
		if (gzwlkyxmlfry != null) {
			for (GzwlkyxmlfryEntity item : gzwlkyxmlfry) {
				JdbcGzwlkyxmlfryEntity jItem = new JdbcGzwlkyxmlfryEntity();
				jItem.from(item);
				jdbcgzwlkyxmlfry.add(jItem);
			}
		}
		this.setGzwlkyxmlfry(jdbcgzwlkyxmlfry);
		
		//项目成果表-start-
		Set<JdbcGzwlkyxmcgEntity> jdbcwlxmcg = new LinkedHashSet<JdbcGzwlkyxmcgEntity>();
		Set<GzwlkyxmcgEntity> wlxmcg = t.getWlxmcg();
		if (wlxmcg != null) {
			for (GzwlkyxmcgEntity item : wlxmcg) {
				JdbcGzwlkyxmcgEntity jItem = new JdbcGzwlkyxmcgEntity();
				jItem.from(item);
				jdbcwlxmcg.add(jItem);
			}
		}
		this.setWlxmcg(jdbcwlxmcg);
	}
	@Override
	public GzwlkyxmsqbEntity toEntity() {
		GzwlkyxmsqbEntity toEntity = new GzwlkyxmsqbEntity();
		BeanUtils.copyProperties(this, toEntity);
		
		//查询匹配条件-start-
		Set<GzwlkyxmlfryEntity> gzwlkyxmlfry = new LinkedHashSet<GzwlkyxmlfryEntity>();
		Set<JdbcGzwlkyxmlfryEntity> jdbcgzwlkyxmlfry = this.getGzwlkyxmlfry();
		
		if (jdbcgzwlkyxmlfry != null) {
			for (JdbcGzwlkyxmlfryEntity item : jdbcgzwlkyxmlfry) {
				gzwlkyxmlfry.add(item.toEntity());
			}
		}
		toEntity.setGzwlkyxmlfry(gzwlkyxmlfry);
		
		//查询匹配条件-start-
		Set<GzwlkyxmcgEntity> wlxmcg = new LinkedHashSet<GzwlkyxmcgEntity>();
		Set<JdbcGzwlkyxmcgEntity> jdbcwlxmcg = this.getWlxmcg();

		if (jdbcwlxmcg != null) {
			for (JdbcGzwlkyxmcgEntity item : jdbcwlxmcg) {
				wlxmcg.add(item.toEntity());
			}
		}
		toEntity.setWlxmcg(wlxmcg);
				
		return toEntity;
	}
	
	/**
	 * 项目成果
	 */
	@SuppressWarnings("deprecation")
	@org.hibernate.annotations.ForeignKey(name = "none")
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "XMBH")
	@OrderBy("SCSJ ASC")
	private Set<JdbcGzwlkyxmcgEntity> wlxmcg=new LinkedHashSet<JdbcGzwlkyxmcgEntity>();
	
	public Set<JdbcGzwlkyxmcgEntity> getWlxmcg() {
		return wlxmcg;
	}

	public void setWlxmcg(Set<JdbcGzwlkyxmcgEntity> wlxmcg) {
		this.wlxmcg = wlxmcg;
	}

	@Override
	public String getProjectname() {
	
		return this.xmmc;
	}
	
	@Column(name = "s_exeid")
	private String sexeid;


	@Override
	public String getSexeid() {
		return this.sexeid;
	}

	public void setSexeid(String sexeid) {
		this.sexeid = sexeid;
	}
}