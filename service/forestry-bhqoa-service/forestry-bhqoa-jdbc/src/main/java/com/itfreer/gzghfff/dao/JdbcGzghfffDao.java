package com.itfreer.gzghfff.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzghfff.dao.GzghfffDao;
import com.itfreer.gzghfff.entity.GzghfffEntity;
import com.itfreer.gzghfff.entity.JdbcGzghfffEntity;

/**
 * 定义管护费发放 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzghfffDao")
public class JdbcGzghfffDao extends JdbcBaseDaoImp<GzghfffEntity, JdbcGzghfffEntity> implements GzghfffDao {

}
