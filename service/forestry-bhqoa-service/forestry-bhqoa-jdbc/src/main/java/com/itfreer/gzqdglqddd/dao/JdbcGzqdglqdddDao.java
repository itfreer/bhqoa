package com.itfreer.gzqdglqddd.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzqdglqddd.dao.GzqdglqdddDao;
import com.itfreer.gzqdglqddd.entity.GzqdglqdddEntity;
import com.itfreer.gzqdglqddd.entity.JdbcGzqdglqdddEntity;

/**
 * 定义签到地点 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzqdglqdddDao")
public class JdbcGzqdglqdddDao extends JdbcBaseDaoImp<GzqdglqdddEntity, JdbcGzqdglqdddEntity> implements GzqdglqdddDao {

}
