package com.itfreer.gzsqclqd.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzsqclqd.entity.GzsqclqdEntity;

/**
 * 定义申请材料清单实体
 */
 @Entity(name = "GZ_SQCLQD")
public class JdbcGzsqclqdEntity implements JdbcBaseEntity<GzsqclqdEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 项目ID
	 */
	@Column(name = "XMID", length = 50, nullable = true)
	private String xmid;
	
	
	/**
	 * 材料名称
	 */
	@Column(name = "CLMC", length = 200, nullable = true)
	private String clmc;
	
	
	/**
	 * 材料数量
	 */
	@Column(name = "CLSL", length = 200, nullable = true)
	private String clsl;
	
	
	/**
	 * 材料备注
	 */
	@Column(name = "CLBZ", length = 200, nullable = true)
	private String clbz;
	
	
	/**
	 * 材料附件
	 */
	@Column(name = "CLFJ", length = 200, nullable = true)
	private String clfj;
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 项目ID
	 */
	public String getXmid() {
		return xmid;
	}

	/**
	 * 项目ID
	 */
	public void setXmid(String value) {
		this.xmid = value;
	}
	/**
	 * 材料名称
	 */
	public String getClmc() {
		return clmc;
	}

	/**
	 * 材料名称
	 */
	public void setClmc(String value) {
		this.clmc = value;
	}
	/**
	 * 材料数量
	 */
	public String getClsl() {
		return clsl;
	}

	/**
	 * 材料数量
	 */
	public void setClsl(String value) {
		this.clsl = value;
	}
	/**
	 * 材料备注
	 */
	public String getClbz() {
		return clbz;
	}

	/**
	 * 材料备注
	 */
	public void setClbz(String value) {
		this.clbz = value;
	}
	/**
	 * 材料附件
	 */
	public String getClfj() {
		return clfj;
	}

	/**
	 * 材料附件
	 */
	public void setClfj(String value) {
		this.clfj = value;
	}

	@Override
	public void from(GzsqclqdEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzsqclqdEntity toEntity() {
		GzsqclqdEntity toEntity = new GzsqclqdEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}