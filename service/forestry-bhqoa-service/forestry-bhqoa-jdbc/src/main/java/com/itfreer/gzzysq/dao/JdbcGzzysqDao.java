package com.itfreer.gzzysq.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzzysq.dao.GzzysqDao;
import com.itfreer.gzzysq.entity.GzzysqEntity;
import com.itfreer.gzzysq.entity.JdbcGzzysqEntity;

/**
 * 定义资源申请 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzzysqDao")
public class JdbcGzzysqDao extends JdbcBaseDaoImp<GzzysqEntity, JdbcGzzysqEntity> implements GzzysqDao {

}
