package com.itfreer.gzreceivefile.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzreceivefile.entity.GzreceivefileEntity;

/**
 * 定义收文管理实体
 */
 @Entity(name = "GZ_RECEIVE_FILE")
public class JdbcGzreceivefileEntity implements JdbcBaseEntity<GzreceivefileEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 发文ID
	 */
	@Column(name = "FWID", length = 500, nullable = true)
	private String fwid;
	
	
	/**
	 * 接收人ID
	 */
	@Column(name = "JSRID", length = 500, nullable = true)
	private String jsrid;
	
	
	/**
	 * 接收人名称
	 */
	@Column(name = "JSRMC", length = 500, nullable = true)
	private String jsrmc;
	
	
	/**
	 * 是否已读
	 */
	@Column(name = "SFYD", length = 500, nullable = true)
	private String sfyd;
	
	/**
	 * 阅读时间
	 */
	@Column(name = "YDSJ", nullable = true)
	private java.util.Date ydsj;
	
	
	/**
	 * 是否已确认
	 */
	@Column(name = "SFQR", length = 500, nullable = true)
	private String sfqr;
	
	
	/**
	 * 接收人机构
	 */
	@Column(name = "JSRJG", length = 500, nullable = true)
	private String jsrjg;
	
	
	/**
	 * 接收人意见
	 */
	@Column(name = "JSRYJ", length = 500, nullable = true)
	private String jsryj;
	
	
	public String getJsryj() {
		return jsryj;
	}

	public void setJsryj(String jsryj) {
		this.jsryj = jsryj;
	}
	
	/**
	 * 接收人职位
	 */
	@Column(name = "JSRZW", length = 500, nullable = true)
	private String jsrzw;
	
	public String getJsrzw() {
		return jsrzw;
	}

	public void setJsrzw(String jsrzw) {
		this.jsrzw = jsrzw;
	}
	/**
	 * 接收人机构名称
	 */
	@Column(name = "JSRJGMC", length = 500, nullable = true)
	private String jsrjgmc;
	
	

	public String getJsrjgmc() {
		return jsrjgmc;
	}

	public void setJsrjgmc(String jsrjgmc) {
		this.jsrjgmc = jsrjgmc;
	}

	
	public String getJsrjg() {
		return jsrjg;
	}

	public void setJsrjg(String jsrjg) {
		this.jsrjg = jsrjg;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 发文ID
	 */
	public String getFwid() {
		return fwid;
	}

	/**
	 * 发文ID
	 */
	public void setFwid(String value) {
		this.fwid = value;
	}
	/**
	 * 接收人ID
	 */
	public String getJsrid() {
		return jsrid;
	}

	/**
	 * 接收人ID
	 */
	public void setJsrid(String value) {
		this.jsrid = value;
	}
	/**
	 * 接收人名称
	 */
	public String getJsrmc() {
		return jsrmc;
	}

	/**
	 * 接收人名称
	 */
	public void setJsrmc(String value) {
		this.jsrmc = value;
	}
	/**
	 * 是否已读
	 */
	public String getSfyd() {
		return sfyd;
	}

	/**
	 * 是否已读
	 */
	public void setSfyd(String value) {
		this.sfyd = value;
	}
	/**
	 * 阅读时间
	 */
	public java.util.Date getYdsj() {
		return ydsj;
	}

	/**
	 * 阅读时间
	 */
	public void setYdsj(java.util.Date value) {
		this.ydsj = value;
	}
	/**
	 * 是否已确认
	 */
	public String getSfqr() {
		return sfqr;
	}

	/**
	 * 是否已确认
	 */
	public void setSfqr(String value) {
		this.sfqr = value;
	}

	@Override
	public void from(GzreceivefileEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzreceivefileEntity toEntity() {
		GzreceivefileEntity toEntity = new GzreceivefileEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}