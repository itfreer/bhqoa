package com.itfreer.gzvisitorreception.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzvisitorreception.dao.GzvisitorreceptionDao;
import com.itfreer.gzvisitorreception.entity.GzvisitorreceptionEntity;
import com.itfreer.gzvisitorreception.entity.JdbcGzvisitorreceptionEntity;

/**
 * 定义访问接待管理 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzvisitorreceptionDao")
public class JdbcGzvisitorreceptionDao extends JdbcBaseDaoImp<GzvisitorreceptionEntity, JdbcGzvisitorreceptionEntity> implements GzvisitorreceptionDao {

}
