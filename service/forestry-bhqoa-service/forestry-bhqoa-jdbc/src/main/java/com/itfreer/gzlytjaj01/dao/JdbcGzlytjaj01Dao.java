package com.itfreer.gzlytjaj01.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzlytjaj01.dao.Gzlytjaj01Dao;
import com.itfreer.gzlytjaj01.entity.Gzlytjaj01Entity;
import com.itfreer.gzlytjaj01.entity.JdbcGzlytjaj01Entity;

/**
 * 定义林业统计案件 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzlytjaj01Dao")
public class JdbcGzlytjaj01Dao extends JdbcBaseDaoImp<Gzlytjaj01Entity, JdbcGzlytjaj01Entity> implements Gzlytjaj01Dao {

}
