package com.itfreer.gzwjryrqxk.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzwjryrqxk.dao.GzwjryrqxkDao;
import com.itfreer.gzwjryrqxk.entity.GzwjryrqxkEntity;
import com.itfreer.gzwjryrqxk.entity.JdbcGzwjryrqxkEntity;

/**
 * 定义外籍人员入区许可 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzwjryrqxkDao")
public class JdbcGzwjryrqxkDao extends JdbcBaseDaoImp<GzwjryrqxkEntity, JdbcGzwjryrqxkEntity> implements GzwjryrqxkDao {

}
