package com.itfreer.gzqdglqddd.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;

/**
 * 定义签到地点实体
 */
 @Entity(name = "GZ_QDGL_QDDD")
public class JdbcGzqdglqdddEntity implements JdbcBaseEntity<GzqdglqdddEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 地点坐标x
	 */
	@Column(name = "KSZBX", length = 500, nullable = true)
	private String kqzbx;
	
	
	/**
	 * 地点坐标y
	 */
	@Column(name = "KSZBY", length = 500, nullable = true)
	private String kqzby;
	
	/**
	 * 考勤地点
	 */
	@Column(name = "KQDD", length = 500, nullable = true)
	private String kqdd;
	
	/**
	 * 创建时间
	 */
	@Column(name = "CJSJ", nullable = true)
	private java.util.Date cjsj;
	
	/**
	 * 备注
	 */
	@Column(name = "BZ", nullable = true)
	private String bz;
	
	
	/**
	 * 考勤范围
	 */
	@Column(name = "KQFW", length = 10, nullable = true)
	private Integer kqfw;
	
	
	
	
	public Integer getKqfw() {
		return kqfw;
	}

	public void setKqfw(Integer kqfw) {
		this.kqfw = kqfw;
	}

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKqzbx() {
		return kqzbx;
	}

	public void setKqzbx(String kqzbx) {
		this.kqzbx = kqzbx;
	}

	public String getKqzby() {
		return kqzby;
	}

	public void setKqzby(String kqzby) {
		this.kqzby = kqzby;
	}

	public String getKqdd() {
		return kqdd;
	}

	public void setKqdd(String kqdd) {
		this.kqdd = kqdd;
	}

	public java.util.Date getCjsj() {
		return cjsj;
	}

	public void setCjsj(java.util.Date cjsj) {
		this.cjsj = cjsj;
	}

	public String getBz() {
		return bz;
	}

	public void setBz(String bz) {
		this.bz = bz;
	}

	@Override
	public void from(GzqdglqdddEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzqdglqdddEntity toEntity() {
		GzqdglqdddEntity toEntity = new GzqdglqdddEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}