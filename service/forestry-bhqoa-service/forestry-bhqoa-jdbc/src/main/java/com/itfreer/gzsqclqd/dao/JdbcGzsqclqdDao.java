package com.itfreer.gzsqclqd.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzsqclqd.dao.GzsqclqdDao;
import com.itfreer.gzsqclqd.entity.GzsqclqdEntity;
import com.itfreer.gzsqclqd.entity.JdbcGzsqclqdEntity;

/**
 * 定义申请材料清单 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzsqclqdDao")
public class JdbcGzsqclqdDao extends JdbcBaseDaoImp<GzsqclqdEntity, JdbcGzsqclqdEntity> implements GzsqclqdDao {

}
