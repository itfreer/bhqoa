package com.itfreer.gzddtj.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzddtj.entity.GzddtjEntity;

/**
 * 定义调度统计实体
 */
 @Entity(name = "GZ_DDTJ")
public class JdbcGzddtjEntity implements JdbcBaseEntity<GzddtjEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 调度年度
	 */
	@Column(name = "S_DDND", length = 500, nullable = true)
	private String sddnd;
	
	
	/**
	 * 调度月份
	 */
	@Column(name = "S_DDYF", length = 500, nullable = true)
	private String sddyf;
	
	
	/**
	 * 资源类型
	 */
	@Column(name = "S_ZYLX", length = 500, nullable = true)
	private String szylx;
	
	
	/**
	 * 使用次数
	 */
	@Column(name = "S_SYCS", length = 500, nullable = true)
	private String ssycs;
	
	
	/**
	 * 成本消耗
	 */
	@Column(name = "S_CBXH", length = 500, nullable = true)
	private String scbxh;
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 调度年度
	 */
	public String getSddnd() {
		return sddnd;
	}

	/**
	 * 调度年度
	 */
	public void setSddnd(String value) {
		this.sddnd = value;
	}
	/**
	 * 调度月份
	 */
	public String getSddyf() {
		return sddyf;
	}

	/**
	 * 调度月份
	 */
	public void setSddyf(String value) {
		this.sddyf = value;
	}
	/**
	 * 资源类型
	 */
	public String getSzylx() {
		return szylx;
	}

	/**
	 * 资源类型
	 */
	public void setSzylx(String value) {
		this.szylx = value;
	}
	/**
	 * 使用次数
	 */
	public String getSsycs() {
		return ssycs;
	}

	/**
	 * 使用次数
	 */
	public void setSsycs(String value) {
		this.ssycs = value;
	}
	/**
	 * 成本消耗
	 */
	public String getScbxh() {
		return scbxh;
	}

	/**
	 * 成本消耗
	 */
	public void setScbxh(String value) {
		this.scbxh = value;
	}

	@Override
	public void from(GzddtjEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzddtjEntity toEntity() {
		GzddtjEntity toEntity = new GzddtjEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}