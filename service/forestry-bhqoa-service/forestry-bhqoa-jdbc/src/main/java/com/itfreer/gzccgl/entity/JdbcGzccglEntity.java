package com.itfreer.gzccgl.entity;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.springframework.beans.BeanUtils;

import com.itfreer.bpm.api.IJdbcBpmBaseEntity;
import com.itfreer.gzccsxry.entity.GzccsxryEntity;
import com.itfreer.gzccsxry.entity.JdbcGzccsxryEntity;
import com.itfreer.gztask.entity.JdbcgztaskEntity;
import com.itfreer.gztask.entity.gztaskEntity;

/**
 * 定义出差管理实体
 */
 @Entity(name = "GZ_CCGL")
public class JdbcGzccglEntity implements IJdbcBpmBaseEntity<GzccglEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 所在单位
	 */
	@Column(name = "SZDW", length = 500, nullable = true)
	private String szdw;
	
	/**
	 * 出差出发地
	 */
	@Column(name = "CFD", length = 500, nullable = true)
	private String cfd;
	
	/**
	 * 出差目的地
	 */
	@Column(name = "MDD", length = 500, nullable = true)
	private String mdd;
	
	
	/**
	 * 单位ID
	 */
	@Column(name = "DWID", length = 500, nullable = true)
	private String dwid;
	
	
	/**
	 * 出差人姓名
	 */
	@Column(name = "CCRXM", length = 500, nullable = true)
	private String ccrxm;
	
	
	/**
	 * 出差人ID
	 */
	@Column(name = "CCRID", length = 500, nullable = true)
	private String ccrid;
	
	/**
	 * 开始日期
	 */
	@Column(name = "KSRQ", nullable = true)
	private java.util.Date ksrq;
	
	/**
	 * 开始时间
	 */
	@Column(name = "KSSJ", nullable = true)
	private java.util.Date kssj;
	
	/**
	 * 结束时间
	 */
	@Column(name = "JSSJ", nullable = true)
	private java.util.Date jssj;
	
	/**
	 * 结束日期
	 */
	@Column(name = "JSRQ", nullable = true)
	private java.util.Date jsrq;
	
	
	/**
	 * 出差类型
	 */
	@Column(name = "CCLX", length = 500, nullable = true)
	private String cclx;
	
	/**
	 * 出差天数
	 */
	@Column(name = "CCTS", nullable = true)
	private Double ccts;
	
	
	/**
	 * 外出事由
	 */
	@Column(name = "WCSY", length = 500, nullable = true)
	private String wcsy;
	
	
	/**
	 * 关联项目ID
	 */
	@Column(name = "GLXMID", length = 500, nullable = true)
	private String glxmid;
	
	
	/**
	 * 关联项目名称
	 */
	@Column(name = "GLXMMC", length = 500, nullable = true)
	private String glxmmc;
	
	
	/**
	 * 出差报告
	 */
	@Column(name = "CCBG", length = 500, nullable = true)
	private String ccbg;
	
	/**
	 * 申请日期
	 */
	@Column(name = "SQRQ", nullable = true)
	private java.util.Date sqrq;
	
	
	/**
	 * 申请人ID
	 */
	@Column(name = "SQRID", length = 500, nullable = true)
	private String sqrid;
	
	
	/**
	 * 申请人名称
	 */
	@Column(name = "SQRMC", length = 500, nullable = true)
	private String sqrmc;
	
	
	/**
	 * 核对人ID
	 */
	@Column(name = "HDRID", length = 500, nullable = true)
	private String hdrid;
	
	
	/**
	 * 核对人名称
	 */
	@Column(name = "HDRMC", length = 500, nullable = true)
	private String hdrmc;
	
	
	/**
	 * 核对结果
	 */
	@Column(name = "HDJG", length = 500, nullable = true)
	private String hdjg;
	
	/**
	 * 核对时间
	 */
	@Column(name = "HDSJ", nullable = true)
	private java.util.Date hdsj;
	
	
	/**
	 * 审核人
	 */
	@Column(name = "SHR", length = 500, nullable = true)
	private String shr;
	
	
	/**
	 * 审核意见
	 */
	@Column(name = "SHYJ", length = 500, nullable = true)
	private String shyj;
	
	
	/**
	 * 审核结果
	 */
	@Column(name = "SHJG", length = 500, nullable = true)
	private String shjg;
	
	
	/**
	 * 审核人ID
	 */
	@Column(name = "SHRID", length = 500, nullable = true)
	private String shrid;
	
	/**
	 * 审核时间
	 */
	@Column(name = "SHSJ", nullable = true)
	private java.util.Date shsj;
	
	/**
	 * 撤销时间
	 */
	@Column(name = "CXSJ", nullable = true)
	private java.util.Date cxsj;
	
	/**
	 * 出行方式
	 */
	@Column(name = "CXFS", nullable = true)
	public String cxfs;
	
	
	/**
	 * 撤销人
	 */
	@Column(name = "CXRMC", length = 500, nullable = true)
	private String cxrmc;
	
	
	/**
	 * 撤销原因
	 */
	@Column(name = "CXYY", length = 500, nullable = true)
	private String cxyy;
	
	
	/**
	 * 撤销人ID
	 */
	@Column(name = "CXRID", length = 500, nullable = true)
	private String cxrid;
	
	
	/**
	 * 出差人职务
	 */
	@Column(name = "CCRZW", length = 500, nullable = true)
	private String ccrzw;
	
	
	@Column(name = "cxfsfj")
	public Boolean cxfsfj;
	
	@Column(name = "cxfshc")
	public Boolean cxfshc;
	
	@Column(name = "cxfsdwpc")
	public Boolean cxfsdwpc;
	
	@Column(name = "cxfsqc")
	public Boolean cxfsqc;
	
	@Column(name = "cxfszdc")
	public Boolean cxfszdc;
	
	@Column(name = "cxfsqt")
	public Boolean cxfsqt;
	
	@Column(name = "spzt")
	public String spzt;
	
	
	
	public String getSpzt() {
		return spzt;
	}

	public void setSpzt(String spzt) {
		this.spzt = spzt;
	}
	/**
	 * 出差人员
	 */
	@SuppressWarnings("deprecation")
	@org.hibernate.annotations.ForeignKey(name = "none")
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "CCGLID")
	@OrderBy("ID ASC")
	private Set<JdbcGzccsxryEntity> ccsxry = new LinkedHashSet<JdbcGzccsxryEntity>();
	
	public Set<JdbcGzccsxryEntity> getCcsxry() {
		return ccsxry;
	}

	public void setCcsxry(Set<JdbcGzccsxryEntity> ccsxry) {
		this.ccsxry = ccsxry;
	}

	public Boolean getCxfsfj() {
		return cxfsfj;
	}

	public void setCxfsfj(Boolean cxfsfj) {
		this.cxfsfj = cxfsfj;
	}

	public Boolean getCxfshc() {
		return cxfshc;
	}

	public void setCxfshc(Boolean cxfshc) {
		this.cxfshc = cxfshc;
	}

	public Boolean getCxfsdwpc() {
		return cxfsdwpc;
	}

	public void setCxfsdwpc(Boolean cxfsdwpc) {
		this.cxfsdwpc = cxfsdwpc;
	}

	public Boolean getCxfsqc() {
		return cxfsqc;
	}

	public void setCxfsqc(Boolean cxfsqc) {
		this.cxfsqc = cxfsqc;
	}

	public Boolean getCxfszdc() {
		return cxfszdc;
	}

	public void setCxfszdc(Boolean cxfszdc) {
		this.cxfszdc = cxfszdc;
	}

	public Boolean getCxfsqt() {
		return cxfsqt;
	}

	public void setCxfsqt(Boolean cxfsqt) {
		this.cxfsqt = cxfsqt;
	}
	
	public String getCcrzw() {
		return ccrzw;
	}

	public void setCcrzw(String ccrzw) {
		this.ccrzw = ccrzw;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 所在单位
	 */
	public String getSzdw() {
		return szdw;
	}

	/**
	 * 所在单位
	 */
	public void setSzdw(String value) {
		this.szdw = value;
	}
	/**
	 * 单位ID
	 */
	public String getDwid() {
		return dwid;
	}

	/**
	 * 单位ID
	 */
	public void setDwid(String value) {
		this.dwid = value;
	}
	/**
	 * 出差人姓名
	 */
	public String getCcrxm() {
		return ccrxm;
	}

	/**
	 * 出差人姓名
	 */
	public void setCcrxm(String value) {
		this.ccrxm = value;
	}
	/**
	 * 出差人ID
	 */
	public String getCcrid() {
		return ccrid;
	}

	/**
	 * 出差人ID
	 */
	public void setCcrid(String value) {
		this.ccrid = value;
	}
	/**
	 * 开始日期
	 */
	public java.util.Date getKsrq() {
		return ksrq;
	}

	/**
	 * 开始日期
	 */
	public void setKsrq(java.util.Date value) {
		this.ksrq = value;
	}
	/**
	 * 开始时间
	 */
	public java.util.Date getKssj() {
		return kssj;
	}

	/**
	 * 开始时间
	 */
	public void setKssj(java.util.Date value) {
		this.kssj = value;
	}
	/**
	 * 结束时间
	 */
	public java.util.Date getJssj() {
		return jssj;
	}

	/**
	 * 结束时间
	 */
	public void setJssj(java.util.Date value) {
		this.jssj = value;
	}
	/**
	 * 结束日期
	 */
	public java.util.Date getJsrq() {
		return jsrq;
	}

	/**
	 * 结束日期
	 */
	public void setJsrq(java.util.Date value) {
		this.jsrq = value;
	}
	/**
	 * 出差类型
	 */
	public String getCclx() {
		return cclx;
	}

	/**
	 * 出差类型
	 */
	public void setCclx(String value) {
		this.cclx = value;
	}
	/**
	 * 出差天数
	 */
	public Double getCcts() {
		return ccts;
	}

	/**
	 * 出差天数
	 */
	public void setCcts(Double value) {
		this.ccts = value;
	}
	/**
	 * 外出事由
	 */
	public String getWcsy() {
		return wcsy;
	}

	/**
	 * 外出事由
	 */
	public void setWcsy(String value) {
		this.wcsy = value;
	}
	/**
	 * 关联项目ID
	 */
	public String getGlxmid() {
		return glxmid;
	}

	/**
	 * 关联项目ID
	 */
	public void setGlxmid(String value) {
		this.glxmid = value;
	}
	/**
	 * 关联项目名称
	 */
	public String getGlxmmc() {
		return glxmmc;
	}

	/**
	 * 关联项目名称
	 */
	public void setGlxmmc(String value) {
		this.glxmmc = value;
	}
	/**
	 * 出差报告
	 */
	public String getCcbg() {
		return ccbg;
	}

	/**
	 * 出差报告
	 */
	public void setCcbg(String value) {
		this.ccbg = value;
	}
	/**
	 * 申请日期
	 */
	public java.util.Date getSqrq() {
		return sqrq;
	}

	/**
	 * 申请日期
	 */
	public void setSqrq(java.util.Date value) {
		this.sqrq = value;
	}
	/**
	 * 申请人ID
	 */
	public String getSqrid() {
		return sqrid;
	}

	/**
	 * 申请人ID
	 */
	public void setSqrid(String value) {
		this.sqrid = value;
	}
	/**
	 * 申请人名称
	 */
	public String getSqrmc() {
		return sqrmc;
	}

	/**
	 * 申请人名称
	 */
	public void setSqrmc(String value) {
		this.sqrmc = value;
	}
	/**
	 * 核对人ID
	 */
	public String getHdrid() {
		return hdrid;
	}

	/**
	 * 核对人ID
	 */
	public void setHdrid(String value) {
		this.hdrid = value;
	}
	/**
	 * 核对人名称
	 */
	public String getHdrmc() {
		return hdrmc;
	}

	/**
	 * 核对人名称
	 */
	public void setHdrmc(String value) {
		this.hdrmc = value;
	}
	/**
	 * 核对结果
	 */
	public String getHdjg() {
		return hdjg;
	}

	/**
	 * 核对结果
	 */
	public void setHdjg(String value) {
		this.hdjg = value;
	}
	/**
	 * 核对时间
	 */
	public java.util.Date getHdsj() {
		return hdsj;
	}

	/**
	 * 核对时间
	 */
	public void setHdsj(java.util.Date value) {
		this.hdsj = value;
	}
	/**
	 * 审核人
	 */
	public String getShr() {
		return shr;
	}

	/**
	 * 审核人
	 */
	public void setShr(String value) {
		this.shr = value;
	}
	/**
	 * 审核意见
	 */
	public String getShyj() {
		return shyj;
	}

	/**
	 * 审核意见
	 */
	public void setShyj(String value) {
		this.shyj = value;
	}
	/**
	 * 审核结果
	 */
	public String getShjg() {
		return shjg;
	}

	/**
	 * 审核结果
	 */
	public void setShjg(String value) {
		this.shjg = value;
	}
	/**
	 * 审核人ID
	 */
	public String getShrid() {
		return shrid;
	}

	/**
	 * 审核人ID
	 */
	public void setShrid(String value) {
		this.shrid = value;
	}
	/**
	 * 审核时间
	 */
	public java.util.Date getShsj() {
		return shsj;
	}

	/**
	 * 审核时间
	 */
	public void setShsj(java.util.Date value) {
		this.shsj = value;
	}
	/**
	 * 撤销时间
	 */
	public java.util.Date getCxsj() {
		return cxsj;
	}

	/**
	 * 撤销时间
	 */
	public void setCxsj(java.util.Date value) {
		this.cxsj = value;
	}
	/**
	 * 撤销人
	 */
	public String getCxrmc() {
		return cxrmc;
	}

	/**
	 * 撤销人
	 */
	public void setCxrmc(String value) {
		this.cxrmc = value;
	}
	/**
	 * 撤销原因
	 */
	public String getCxyy() {
		return cxyy;
	}

	/**
	 * 撤销原因
	 */
	public void setCxyy(String value) {
		this.cxyy = value;
	}
	
	
	public String getCxfs() {
		return cxfs;
	}

	public void setCxfs(String cxfs) {
		this.cxfs = cxfs;
	}

	/**
	 * 撤销人ID
	 */
	public String getCxrid() {
		return cxrid;
	}

	/**
	 * 撤销人ID
	 */
	public void setCxrid(String value) {
		this.cxrid = value;
	}

	@Override
	public void from(GzccglEntity t) {
		BeanUtils.copyProperties(t, this);
		
		//项目人员表-start-
		Set<JdbcGzccsxryEntity> jdbcgztask = new LinkedHashSet<JdbcGzccsxryEntity>();
		Set<GzccsxryEntity> gztask = t.getCcsxry();
		if (gztask != null) {
			for (GzccsxryEntity item : gztask) {
				JdbcGzccsxryEntity jItem = new JdbcGzccsxryEntity();
				jItem.from(item);
				jdbcgztask.add(jItem);
			}
		}
		this.setCcsxry(jdbcgztask);
	}

	@Override
	public GzccglEntity toEntity() {
		GzccglEntity toEntity = new GzccglEntity();
		BeanUtils.copyProperties(this, toEntity);
		
		//查询匹配条件-start-
		Set<GzccsxryEntity> gztask = new LinkedHashSet<GzccsxryEntity>();
		Set<JdbcGzccsxryEntity> jdbcgztask = this.getCcsxry();
		
		if (jdbcgztask != null) {
			for (JdbcGzccsxryEntity item : jdbcgztask) {
				gztask.add(item.toEntity());
			}
		}
		toEntity.setCcsxry(gztask);
		
		return toEntity;
	}
	@Column(name = "s_exeid")
	private String sexeid;
	
	@Override
	public String getProjectname() {
		
		return this.ccrxm;
	}

	@Override
	public String getSexeid() {
		return this.sexeid;
	}

	public void setSexeid(String sexeid) {
		this.sexeid = sexeid;
	}

	public String getMdd() {
		return mdd;
	}

	public void setMdd(String mdd) {
		this.mdd = mdd;
	}

	public String getCfd() {
		return cfd;
	}

	public void setCfd(String cfd) {
		this.cfd = cfd;
	}
}