package com.itfreer.gzclsq.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;

import com.itfreer.bpm.api.IJdbcBpmBaseEntity;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzclsq.entity.GzclsqEntity;

/**
 * 定义车辆使用实体
 */
 @Entity(name = "GZ_CLSQ")
public class JdbcGzclsqEntity implements IJdbcBpmBaseEntity<GzclsqEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	
	@Column(name = "spzt")
	public String spzt;

	public String getSpzt() {
		return spzt;
	}

	public void setSpzt(String spzt) {
		this.spzt = spzt;
	}
	/**
	 * 唯一编号
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 申请人id
	 */
	@Column(name = "S_SQRID", length = 500, nullable = true)
	private String ssqrid;
	
	
	public String getSsqrid() {
		return ssqrid;
	}

	public void setSsqrid(String ssqrid) {
		this.ssqrid = ssqrid;
	}
	/**
	 * 申请人
	 */
	@Column(name = "S_SQR", length = 500, nullable = true)
	private String ssqr;
	
	/**
	 * 申请时间
	 */
	@Column(name = "D_SQSJ", nullable = true)
	private java.util.Date dsqsj;
	
	
	/**
	 * 申请原因
	 */
	@Column(name = "S_SQYY", length = 500, nullable = true)
	private String ssqyy;
	
	
	/**
	 * 联系方式
	 */
	@Column(name = "S_LXFS", length = 500, nullable = true)
	private String slxfs;
	
	/**
	 * 借用时间
	 */
	@Column(name = "D_JYSJ", nullable = true)
	private java.util.Date djysj;
	
	/**
	 * 归还时间
	 */
	@Column(name = "D_GHSJ", nullable = true)
	private java.util.Date dghsj;
	
	
	/**
	 * 是否同意申请
	 */
	@Column(name = "I_TYSQ", length = 500, nullable = true)
	private String itysq;
	
	
	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	@Column(name = "S_CLXX", length = 500, nullable = true)
	private String sclxx;
	
	
	/**
	 * 备注
	 */
	@Column(name = "S_BZ", length = 500, nullable = true)
	private String sbz;
	
	/**
	 * 用车部门
	 */
	@Column(name = "YCBM", length = 500, nullable = true)
	private String ycbm;
	
	/**
	 * 用车地点
	 */
	@Column(name = "YCDD", length = 500, nullable = true)
	private String ycdd;
	
	/**
	 * 用车人数
	 */
	@Column(name = "YCRS", length = 500, nullable = true)
	private String ycrs;
	
	/**
	 * 用车人员
	 */
	@Column(name = "YCRY", length = 500, nullable = true)
	private String ycry;
	
	/**
	 * 车牌号
	 */
	@Column(name = "CPH", length = 500, nullable = true)
	private String cph;
	
	/**
	 * 驾驶员
	 */
	@Column(name = "JSY", length = 500, nullable = true)
	private String jsy;
	
	/**
	 * 部门审批意见
	 */
	@Column(name = "S_BMSPYJ", length = 500, nullable = true)
	private String sbmspyj;
	
	/**
	 * 现金加油
	 */
	@Column(name = "S_XJJY", length = 500, nullable = true)
	private String sxjjy;
	
	/**
	 * 卡加油
	 */
	@Column(name = "S_KJY", length = 500, nullable = true)
	private String skjy;
	
	/**
	 * 加油经办人
	 */
	@Column(name = "S_JYJBR", length = 500, nullable = true)
	private String sjyjbr;
	
	/**
	 * 办公室调度意见
	 */
	@Column(name = "S_BGSDDYJ", length = 500, nullable = true)
	private String sbgsddyj;
	
	/**
	 * 部门领导审批
	 */
	@Column(name = "S_BMLDSP", length = 500, nullable = true)
	private String sbmldsp;
	
	
	/**
	 * 车辆状态
	 */
	@Column(name = "CLZT", length = 500, nullable = true)
	private String clzt;
	

	public String getClzt() {
		return clzt;
	}

	public void setClzt(String clzt) {
		this.clzt = clzt;
	}

	public String getSbmspyj() {
		return sbmspyj;
	}

	public void setSbmspyj(String sbmspyj) {
		this.sbmspyj = sbmspyj;
	}

	public String getSxjjy() {
		return sxjjy;
	}

	public void setSxjjy(String sxjjy) {
		this.sxjjy = sxjjy;
	}

	public String getSkjy() {
		return skjy;
	}

	public void setSkjy(String skjy) {
		this.skjy = skjy;
	}

	public String getSjyjbr() {
		return sjyjbr;
	}

	public void setSjyjbr(String sjyjbr) {
		this.sjyjbr = sjyjbr;
	}

	public String getSbgsddyj() {
		return sbgsddyj;
	}

	public void setSbgsddyj(String sbgsddyj) {
		this.sbgsddyj = sbgsddyj;
	}

	public String getSbmldsp() {
		return sbmldsp;
	}

	public void setSbmldsp(String sbmldsp) {
		this.sbmldsp = sbmldsp;
	}

	public String getYcbm() {
		return ycbm;
	}

	public void setYcbm(String ycbm) {
		this.ycbm = ycbm;
	}

	public String getYcdd() {
		return ycdd;
	}

	public void setYcdd(String ycdd) {
		this.ycdd = ycdd;
	}

	public String getYcrs() {
		return ycrs;
	}

	public void setYcrs(String ycrs) {
		this.ycrs = ycrs;
	}

	public String getYcry() {
		return ycry;
	}

	public void setYcry(String ycry) {
		this.ycry = ycry;
	}

	public String getCph() {
		return cph;
	}

	public void setCph(String cph) {
		this.cph = cph;
	}

	public String getJsy() {
		return jsy;
	}

	public void setJsy(String jsy) {
		this.jsy = jsy;
	}

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 申请人
	 */
	public String getSsqr() {
		return ssqr;
	}

	/**
	 * 申请人
	 */
	public void setSsqr(String value) {
		this.ssqr = value;
	}
	/**
	 * 申请时间
	 */
	public java.util.Date getDsqsj() {
		return dsqsj;
	}

	/**
	 * 申请时间
	 */
	public void setDsqsj(java.util.Date value) {
		this.dsqsj = value;
	}
	/**
	 * 申请原因
	 */
	public String getSsqyy() {
		return ssqyy;
	}

	/**
	 * 申请原因
	 */
	public void setSsqyy(String value) {
		this.ssqyy = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxfs() {
		return slxfs;
	}

	/**
	 * 联系方式
	 */
	public void setSlxfs(String value) {
		this.slxfs = value;
	}
	/**
	 * 借用时间
	 */
	public java.util.Date getDjysj() {
		return djysj;
	}

	/**
	 * 借用时间
	 */
	public void setDjysj(java.util.Date value) {
		this.djysj = value;
	}
	/**
	 * 归还时间
	 */
	public java.util.Date getDghsj() {
		return dghsj;
	}

	/**
	 * 归还时间
	 */
	public void setDghsj(java.util.Date value) {
		this.dghsj = value;
	}
	/**
	 * 是否同意申请
	 */
	public String getItysq() {
		return itysq;
	}

	/**
	 * 是否同意申请
	 */
	public void setItysq(String value) {
		this.itysq = value;
	}
	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	public String getSclxx() {
		return sclxx;
	}

	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	public void setSclxx(String value) {
		this.sclxx = value;
	}
	/**
	 * 备注
	 */
	public String getSbz() {
		return sbz;
	}

	/**
	 * 备注
	 */
	public void setSbz(String value) {
		this.sbz = value;
	}

	@Override
	public void from(GzclsqEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzclsqEntity toEntity() {
		GzclsqEntity toEntity = new GzclsqEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}

	@Override
	public String getProjectname() {
		
		return this.ssqyy;
	}
	@Column(name = "s_exeid")
	private String sexeid;

	@Override
	public String getSexeid() {
	
		return sexeid;
	}

	public void setSexeid(String sexeid) {
		this.sexeid = sexeid;
	}
}