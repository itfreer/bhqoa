package com.itfreer.gzplanpersonnel.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzplanpersonnel.entity.GzplanpersonnelEntity;

/**
 * 定义项目规划人员实体
 */
 @Entity(name = "GZ_PLAN_PERSONNEL")
public class JdbcGzplanpersonnelEntity implements JdbcBaseEntity<GzplanpersonnelEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 计划任务编号
	 */
	@Column(name = "S_JHRWBH", length = 50, nullable = true)
	private String sjhrwbh;
	
	
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 人员编号
	 */
	@Column(name = "S_RYBH", length = 50, nullable = true)
	private String srybh;
	
	
	/**
	 * 人员名称
	 */
	@Column(name = "YRMC", length = 500, nullable = true)
	private String yrmc;
	

	/**
	 * 人员性别
	 */
	@Column(name = "S_SEX", length = 50, nullable = true)
	private String ssex;
	

	/**
	 * 人员身份证
	 */
	@Column(name = "S_SFZ", length = 50, nullable = true)
	private String ssfz;
	
	
	/**
	 * 人员联系方式
	 */
	@Column(name = "S_LXFS", length = 50, nullable = true)
	private String slxfs;
	
	
	/**
	 * 项目编号
	 */
	@Column(name = "S_XMBH", length = 100, nullable = true)
	private String sxmbh;
	
	
	/**
	 * 项目名称
	 */
	@Column(name = "S_XMMC", length = 100, nullable = true)
	private String sxmmc;
	
	
	/**
	 * 项目id
	 */
	@Column(name = "XMID", length = 100, nullable = true)
	private String xmid;
	
	/**
	 * 人员职称
	 */
	@Column(name = "S_RYZC", length = 100, nullable = true)
	private String sryzc;
	
	/**
	 * 项目申请人
	 */
	@Column(name = "SQR", length = 255, nullable = true)
	private String sqr;
	/**
	 * 项目申请人ID
	 */
	@Column(name = "SQRID", length = 255, nullable = true)
	private String sqrid;
	/**
	 * 项目申请人部门
	 */
	@Column(name = "SQBM", length = 255, nullable = true)
	private String sqbm;
	/**
	 * 项目申请时间
	 */
	@Column(name = "SQSJ", nullable = true)
	private java.util.Date sqsj;
	/**
	 * 项目结项时间
	 */
	@Column(name = "JXSJ", nullable = true)
	private java.util.Date jxsj;
	
	public String getSqr() {
		return sqr;
	}

	public void setSqr(String sqr) {
		this.sqr = sqr;
	}

	public String getSqrid() {
		return sqrid;
	}

	public void setSqrid(String sqrid) {
		this.sqrid = sqrid;
	}

	public String getSqbm() {
		return sqbm;
	}

	public void setSqbm(String sqbm) {
		this.sqbm = sqbm;
	}

	public java.util.Date getSqsj() {
		return sqsj;
	}

	public void setSqsj(java.util.Date sqsj) {
		this.sqsj = sqsj;
	}

	public java.util.Date getJxsj() {
		return jxsj;
	}

	public void setJxsj(java.util.Date jxsj) {
		this.jxsj = jxsj;
	}
	

	public String getSryzc() {
		return sryzc;
	}

	public void setSryzc(String sryzc) {
		this.sryzc = sryzc;
	}

	public String getXmid() {
		return xmid;
	}

	public void setXmid(String xmid) {
		this.xmid = xmid;
	}

	public String getSsex() {
		return ssex;
	}

	public void setSsex(String ssex) {
		this.ssex = ssex;
	}

	public String getSsfz() {
		return ssfz;
	}

	public void setSsfz(String ssfz) {
		this.ssfz = ssfz;
	}

	public String getSlxfs() {
		return slxfs;
	}

	public void setSlxfs(String slxfs) {
		this.slxfs = slxfs;
	}
	
	/**
	 * 项目编号
	 */
	public String getSxmbh() {
		return sxmbh;
	}

	/**
	 * 项目编号
	 */
	public void setSxmbh(String value) {
		this.sxmbh = value;
	}
	/**
	 * 项目名称
	 */
	public String getSxmmc() {
		return sxmmc;
	}

	/**
	 * 项目名称
	 */
	public void setSxmmc(String value) {
		this.sxmmc = value;
	}

	/**
	 * 计划任务编号
	 */
	public String getSjhrwbh() {
		return sjhrwbh;
	}

	/**
	 * 计划任务编号
	 */
	public void setSjhrwbh(String value) {
		this.sjhrwbh = value;
	}
	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 人员编号
	 */
	public String getSrybh() {
		return srybh;
	}

	/**
	 * 人员编号
	 */
	public void setSrybh(String value) {
		this.srybh = value;
	}
	/**
	 * 人员名称
	 */
	public String getYrmc() {
		return yrmc;
	}

	/**
	 * 人员名称
	 */
	public void setYrmc(String value) {
		this.yrmc = value;
	}

	@Override
	public void from(GzplanpersonnelEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzplanpersonnelEntity toEntity() {
		GzplanpersonnelEntity toEntity = new GzplanpersonnelEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}