package com.itfreer.gzkyxmcg.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzkyxmcg.dao.GzkyxmcgDao;
import com.itfreer.gzkyxmcg.entity.GzkyxmcgEntity;
import com.itfreer.gzkyxmcg.entity.JdbcGzkyxmcgEntity;

/**
 * 定义科研项目成果 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzkyxmcgDao")
public class JdbcGzkyxmcgDao extends JdbcBaseDaoImp<GzkyxmcgEntity, JdbcGzkyxmcgEntity> implements GzkyxmcgDao {

}
