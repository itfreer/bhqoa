package com.itfreer.gzpublishfile.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzpublishfile.dao.GzpublishfileDao;
import com.itfreer.gzpublishfile.entity.GzpublishfileEntity;
import com.itfreer.gzpublishfile.entity.JdbcGzpublishfileEntity;

/**
 * 定义发文管理 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzpublishfileDao")
public class JdbcGzpublishfileDao extends JdbcBaseDaoImp<GzpublishfileEntity, JdbcGzpublishfileEntity> implements GzpublishfileDao {

}
