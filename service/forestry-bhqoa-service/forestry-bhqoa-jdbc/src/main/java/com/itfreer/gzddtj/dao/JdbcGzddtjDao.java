package com.itfreer.gzddtj.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzddtj.dao.GzddtjDao;
import com.itfreer.gzddtj.entity.GzddtjEntity;
import com.itfreer.gzddtj.entity.JdbcGzddtjEntity;

/**
 * 定义调度统计 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzddtjDao")
public class JdbcGzddtjDao extends JdbcBaseDaoImp<GzddtjEntity, JdbcGzddtjEntity> implements GzddtjDao {

}
