package com.itfreer.gzddtz.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzddtz.entity.GzddtzEntity;

/**
 * 定义调度台账实体
 */
 @Entity(name = "GZ_DDTZ")
public class JdbcGzddtzEntity implements JdbcBaseEntity<GzddtzEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 资源类型
	 */
	@Column(name = "S_ZYLX", length = 500, nullable = true)
	private String szylx;
	
	/**
	 * 购置时间
	 */
	@Column(name = "D_GZSJ", nullable = true)
	private java.util.Date dgzsj;
	
	
	/**
	 * 分配部门
	 */
	@Column(name = "S_FPBM", length = 500, nullable = true)
	private String sfpbm;
	
	
	/**
	 * 使用人
	 */
	@Column(name = "S_SYR", length = 500, nullable = true)
	private String ssyr;
	
	/**
	 * 使用时间
	 */
	@Column(name = "D_SYSJ", nullable = true)
	private java.util.Date dsysj;
	
	
	/**
	 * 使用情况
	 */
	@Column(name = "S_SYQK", length = 500, nullable = true)
	private String ssyqk;
	
	
	/**
	 * 成本消耗
	 */
	@Column(name = "S_CBXH", length = 500, nullable = true)
	private String scbxh;
	
	
	/**
	 * 所属项目
	 */
	@Column(name = "S_SSXM", length = 500, nullable = true)
	private String sssxm;
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 资源类型
	 */
	public String getSzylx() {
		return szylx;
	}

	/**
	 * 资源类型
	 */
	public void setSzylx(String value) {
		this.szylx = value;
	}
	/**
	 * 购置时间
	 */
	public java.util.Date getDgzsj() {
		return dgzsj;
	}

	/**
	 * 购置时间
	 */
	public void setDgzsj(java.util.Date value) {
		this.dgzsj = value;
	}
	/**
	 * 分配部门
	 */
	public String getSfpbm() {
		return sfpbm;
	}

	/**
	 * 分配部门
	 */
	public void setSfpbm(String value) {
		this.sfpbm = value;
	}
	/**
	 * 使用人
	 */
	public String getSsyr() {
		return ssyr;
	}

	/**
	 * 使用人
	 */
	public void setSsyr(String value) {
		this.ssyr = value;
	}
	/**
	 * 使用时间
	 */
	public java.util.Date getDsysj() {
		return dsysj;
	}

	/**
	 * 使用时间
	 */
	public void setDsysj(java.util.Date value) {
		this.dsysj = value;
	}
	/**
	 * 使用情况
	 */
	public String getSsyqk() {
		return ssyqk;
	}

	/**
	 * 使用情况
	 */
	public void setSsyqk(String value) {
		this.ssyqk = value;
	}
	/**
	 * 成本消耗
	 */
	public String getScbxh() {
		return scbxh;
	}

	/**
	 * 成本消耗
	 */
	public void setScbxh(String value) {
		this.scbxh = value;
	}
	/**
	 * 所属项目
	 */
	public String getSssxm() {
		return sssxm;
	}

	/**
	 * 所属项目
	 */
	public void setSssxm(String value) {
		this.sssxm = value;
	}

	@Override
	public void from(GzddtzEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzddtzEntity toEntity() {
		GzddtzEntity toEntity = new GzddtzEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}