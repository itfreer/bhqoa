package com.itfreer.gzwlkyxmlfry.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzwlkyxmlfry.entity.GzwlkyxmlfryEntity;

/**
 * 定义外来科研项目人随访人员实体
 */
 @Entity(name = "GZ_WLKYXM_LFRY")
public class JdbcGzwlkyxmlfryEntity implements JdbcBaseEntity<GzwlkyxmlfryEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 姓名
	 */
	@Column(name = "XINGMING", length = 500, nullable = true)
	private String xingming;
	
	
	/**
	 * 性别
	 */
	@Column(name = "XINGBIE", length = 500, nullable = true)
	private String xingbie;
	
	
	/**
	 * 身份证号
	 */
	@Column(name = "SHENFZH", length = 500, nullable = true)
	private String shenfzh;
	
	
	/**
	 * 联系方式
	 */
	@Column(name = "LXFS", length = 500, nullable = true)
	private String lxfs;
	
	
	/**
	 * 申请项目ID
	 */
	@Column(name = "XMID", length = 500, nullable = true)
	private String xmid;
	
	
	/**
	 * 项目名称
	 */
	@Column(name = "S_XMMC", length = 100, nullable = true)
	private String sxmmc;
	
	/**
	 * 项目编号
	 */
	@Column(name = "S_XMBH", length = 100, nullable = true)
	private String sxmbh;
	
	/**
	 * 负责部门
	 */
	@Column(name = "FZBM", length = 255, nullable = true)
	private String fzbm;
	
	/**
	 * 负责人
	 */
	@Column(name = "FZR", length = 255, nullable = true)
	private String fzr;
	
	/**
	 * 负责人ID
	 */
	@Column(name = "FZRID", length = 255, nullable = true)
	private String fzrid;
	
	/**
	 * 来访时间开始时间
	 */
	@Column(name = "LFSJ", nullable = true)
	private java.util.Date lfsj;
	
	/**
	 * 来访时间结束时间
	 */
	@Column(name = "LFSJ_JSSJ", nullable = true)
	private java.util.Date lfsjjssj;
	
	public String getFzbm() {
		return fzbm;
	}

	public void setFzbm(String fzbm) {
		this.fzbm = fzbm;
	}

	public String getFzr() {
		return fzr;
	}

	public void setFzr(String fzr) {
		this.fzr = fzr;
	}

	public String getFzrid() {
		return fzrid;
	}

	public void setFzrid(String fzrid) {
		this.fzrid = fzrid;
	}

	public java.util.Date getLfsj() {
		return lfsj;
	}

	public void setLfsj(java.util.Date lfsj) {
		this.lfsj = lfsj;
	}

	public java.util.Date getLfsjjssj() {
		return lfsjjssj;
	}

	public void setLfsjjssj(java.util.Date lfsjjssj) {
		this.lfsjjssj = lfsjjssj;
	}

	public String getSxmbh() {
		return sxmbh;
	}

	public void setSxmbh(String sxmbh) {
		this.sxmbh = sxmbh;
	}

	/**
	 * 项目名称
	 */
	public String getSxmmc() {
		return sxmmc;
	}

	/**
	 * 项目名称
	 */
	public void setSxmmc(String value) {
		this.sxmmc = value;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 姓名
	 */
	public String getXingming() {
		return xingming;
	}

	/**
	 * 姓名
	 */
	public void setXingming(String value) {
		this.xingming = value;
	}
	/**
	 * 性别
	 */
	public String getXingbie() {
		return xingbie;
	}

	/**
	 * 性别
	 */
	public void setXingbie(String value) {
		this.xingbie = value;
	}
	/**
	 * 身份证号
	 */
	public String getShenfzh() {
		return shenfzh;
	}

	/**
	 * 身份证号
	 */
	public void setShenfzh(String value) {
		this.shenfzh = value;
	}
	/**
	 * 联系方式
	 */
	public String getLxfs() {
		return lxfs;
	}

	/**
	 * 联系方式
	 */
	public void setLxfs(String value) {
		this.lxfs = value;
	}
	/**
	 * 申请项目ID
	 */
	public String getXmid() {
		return xmid;
	}

	/**
	 * 申请项目ID
	 */
	public void setXmid(String value) {
		this.xmid = value;
	}

	@Override
	public void from(GzwlkyxmlfryEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzwlkyxmlfryEntity toEntity() {
		GzwlkyxmlfryEntity toEntity = new GzwlkyxmlfryEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}