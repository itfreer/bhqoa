package com.itfreer.gzplanpersonnel.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzplanpersonnel.dao.GzplanpersonnelDao;
import com.itfreer.gzplanpersonnel.entity.GzplanpersonnelEntity;
import com.itfreer.gzplanpersonnel.entity.JdbcGzplanpersonnelEntity;

/**
 * 定义项目规划人员 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzplanpersonnelDao")
public class JdbcGzplanpersonnelDao extends JdbcBaseDaoImp<GzplanpersonnelEntity, JdbcGzplanpersonnelEntity> implements GzplanpersonnelDao {

}
