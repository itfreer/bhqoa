package com.itfreer.gzaqbw.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzaqbw.dao.GzaqbwDao;
import com.itfreer.gzaqbw.entity.GzaqbwEntity;
import com.itfreer.gzaqbw.entity.JdbcGzaqbwEntity;

/**
 * 定义安全保卫 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzaqbwDao")
public class JdbcGzaqbwDao extends JdbcBaseDaoImp<GzaqbwEntity, JdbcGzaqbwEntity> implements GzaqbwDao {

}
