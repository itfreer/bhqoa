package com.itfreer.gzwlkyxmcg.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzwlkyxmcg.dao.GzwlkyxmcgDao;
import com.itfreer.gzwlkyxmcg.entity.GzwlkyxmcgEntity;
import com.itfreer.gzwlkyxmcg.entity.JdbcGzwlkyxmcgEntity;

/**
 * 定义外来科研项目成果 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzwlkyxmcgDao")
public class JdbcGzwlkyxmcgDao extends JdbcBaseDaoImp<GzwlkyxmcgEntity, JdbcGzwlkyxmcgEntity> implements GzwlkyxmcgDao {

}
