package com.itfreer.gzglzda.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzglzda.entity.GzglzdaEntity;

/**
 * 定义管理站档案实体
 */
 @Entity(name = "GZ_GLZDA")
public class JdbcGzglzdaEntity implements JdbcBaseEntity<GzglzdaEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 管理站名称
	 */
	@Column(name = "S_GLZMC", length = 500, nullable = true)
	private String sglzmc;
	
	
	/**
	 * 管理站地址
	 */
	@Column(name = "S_GLZDZ", length = 500, nullable = true)
	private String sglzdz;
	
	
	/**
	 * 负责人
	 */
	@Column(name = "S_FZR", length = 500, nullable = true)
	private String sfzr;
	
	
	/**
	 * 负责人联系方式
	 */
	@Column(name = "S_LXFF", length = 500, nullable = true)
	private String slxff;
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 管理站名称
	 */
	public String getSglzmc() {
		return sglzmc;
	}

	/**
	 * 管理站名称
	 */
	public void setSglzmc(String value) {
		this.sglzmc = value;
	}
	/**
	 * 管理站地址
	 */
	public String getSglzdz() {
		return sglzdz;
	}

	/**
	 * 管理站地址
	 */
	public void setSglzdz(String value) {
		this.sglzdz = value;
	}
	/**
	 * 负责人
	 */
	public String getSfzr() {
		return sfzr;
	}

	/**
	 * 负责人
	 */
	public void setSfzr(String value) {
		this.sfzr = value;
	}
	/**
	 * 负责人联系方式
	 */
	public String getSlxff() {
		return slxff;
	}

	/**
	 * 负责人联系方式
	 */
	public void setSlxff(String value) {
		this.slxff = value;
	}

	@Override
	public void from(GzglzdaEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzglzdaEntity toEntity() {
		GzglzdaEntity toEntity = new GzglzdaEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}