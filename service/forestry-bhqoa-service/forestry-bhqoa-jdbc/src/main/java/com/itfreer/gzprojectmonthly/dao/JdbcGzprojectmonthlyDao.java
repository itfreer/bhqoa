package com.itfreer.gzprojectmonthly.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzprojectmonthly.dao.GzprojectmonthlyDao;
import com.itfreer.gzprojectmonthly.entity.GzprojectmonthlyEntity;
import com.itfreer.gzprojectmonthly.entity.JdbcGzprojectmonthlyEntity;

/**
 * 定义项目月报 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzprojectmonthlyDao")
public class JdbcGzprojectmonthlyDao extends JdbcBaseDaoImp<GzprojectmonthlyEntity, JdbcGzprojectmonthlyEntity> implements GzprojectmonthlyDao {

}
