package com.itfreer.gzdevicepurchase.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzdevicepurchase.entity.GzdevicepurchaseEntity;

/**
 * 定义设备采购管理实体
 */
 @Entity(name = "GZ_DEVICE_PURCHASE")
public class JdbcGzdevicepurchaseEntity implements JdbcBaseEntity<GzdevicepurchaseEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 100, nullable = true)
	private String id;
	
	/**
	 * I_VERSION
	 */
	@Column(name = "I_VERSION", nullable = true)
	private Integer iversion;
	
	
	/**
	 * 采购编号
	 */
	@Column(name = "I_CGBH", length = 60, nullable = true)
	private String icgbh;
	
	
	/**
	 * 采购说明
	 */
	@Column(name = "S_CGSM", length = 200, nullable = true)
	private String scgsm;
	
	
	/**
	 * 申请单位
	 */
	@Column(name = "S_SQBM", length = 60, nullable = true)
	private String ssqbm;
	
	
	/**
	 * 申请时间
	 */
	@Column(name = "D_SQSJ", nullable = true)
	private java.util.Date dsqsj;
	
	
	/**
	 * 经办人
	 */
	@Column(name = "S_JBR", length = 60, nullable = true)
	private String sjbr;
	
	
	/**
	 * 联系电话
	 */
	@Column(name = "S_LXDH", length = 60, nullable = true)
	private String slxdh;
	
	
	/**
	 * 购买理由
	 */
	@Column(name = "S_GMLY", length = 200, nullable = true)
	private String sgmly;
	
	/**
	 * 总价
	 */
	@Column(name = "S_ZJ", nullable = true)
	private Double szj;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * I_VERSION
	 */
	public Integer getIversion() {
		return iversion;
	}

	/**
	 * I_VERSION
	 */
	public void setIversion(Integer value) {
		this.iversion = value;
	}
	/**
	 * 采购编号
	 */
	public String getIcgbh() {
		return icgbh;
	}

	/**
	 * 采购编号
	 */
	public void setIcgbh(String value) {
		this.icgbh = value;
	}
	/**
	 * 采购说明
	 */
	public String getScgsm() {
		return scgsm;
	}

	/**
	 * 采购说明
	 */
	public void setScgsm(String value) {
		this.scgsm = value;
	}
	/**
	 * 申请单位
	 */
	public String getSsqbm() {
		return ssqbm;
	}

	/**
	 * 申请单位
	 */
	public void setSsqbm(String value) {
		this.ssqbm = value;
	}
	/**
	 * 申请时间
	 */
	public java.util.Date getDsqsj() {
		return dsqsj;
	}

	/**
	 * 申请时间
	 */
	public void setDsqsj(java.util.Date value) {
		this.dsqsj = value;
	}
	/**
	 * 经办人
	 */
	public String getSjbr() {
		return sjbr;
	}

	/**
	 * 经办人
	 */
	public void setSjbr(String value) {
		this.sjbr = value;
	}
	/**
	 * 联系电话
	 */
	public String getSlxdh() {
		return slxdh;
	}

	/**
	 * 联系电话
	 */
	public void setSlxdh(String value) {
		this.slxdh = value;
	}
	/**
	 * 购买理由
	 */
	public String getSgmly() {
		return sgmly;
	}

	/**
	 * 购买理由
	 */
	public void setSgmly(String value) {
		this.sgmly = value;
	}
	/**
	 * 总价
	 */
	public Double getSzj() {
		return szj;
	}

	/**
	 * 总价
	 */
	public void setSzj(Double value) {
		this.szj = value;
	}

	@Override
	public void from(GzdevicepurchaseEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzdevicepurchaseEntity toEntity() {
		GzdevicepurchaseEntity toEntity = new GzdevicepurchaseEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}