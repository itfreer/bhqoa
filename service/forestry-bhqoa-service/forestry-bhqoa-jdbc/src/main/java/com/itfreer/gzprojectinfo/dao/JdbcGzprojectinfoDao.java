package com.itfreer.gzprojectinfo.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzprojectinfo.dao.GzprojectinfoDao;
import com.itfreer.gzprojectinfo.entity.GzprojectinfoEntity;
import com.itfreer.gzprojectinfo.entity.JdbcGzprojectinfoEntity;

/**
 * 定义项目立项管理 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzprojectinfoDao")
public class JdbcGzprojectinfoDao extends JdbcBaseDaoImp<GzprojectinfoEntity, JdbcGzprojectinfoEntity> implements GzprojectinfoDao {

}
