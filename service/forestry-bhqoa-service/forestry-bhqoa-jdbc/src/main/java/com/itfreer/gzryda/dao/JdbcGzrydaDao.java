package com.itfreer.gzryda.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzryda.dao.GzrydaDao;
import com.itfreer.gzryda.entity.GzrydaEntity;
import com.itfreer.gzryda.entity.JdbcGzrydaEntity;

/**
 * 定义人事档案 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzrydaDao")
public class JdbcGzrydaDao extends JdbcBaseDaoImp<GzrydaEntity, JdbcGzrydaEntity> implements GzrydaDao {

}
