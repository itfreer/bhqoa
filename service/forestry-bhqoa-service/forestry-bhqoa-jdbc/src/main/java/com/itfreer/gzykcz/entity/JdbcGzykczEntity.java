package com.itfreer.gzykcz.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzykcz.entity.GzykczEntity;

/**
 * 定义油卡充值实体
 */
 @Entity(name = "GZ_YKCZ")
public class JdbcGzykczEntity implements JdbcBaseEntity<GzykczEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	@Column(name = "S_CLXX", length = 500, nullable = true)
	private String sclxx;
	
	/**
	 * 充值时间
	 */
	@Column(name = "D_CZSJ", nullable = true)
	private java.util.Date dczsj;
	
	
	/**
	 * 充值地点
	 */
	@Column(name = "S_CZDD", length = 500, nullable = true)
	private String sczdd;
	
	
	/**
	 * 充值人
	 */
	@Column(name = "S_CZR", length = 500, nullable = true)
	private String sczr;
	
	
	/**
	 * 联系方式
	 */
	@Column(name = "S_LXFS", length = 500, nullable = true)
	private String slxfs;
	
	
	/**
	 * 充值金额
	 */
	@Column(name = "S_CZJE", length = 500, nullable = true)
	private String sczje;
	
	
	/**
	 * 电子发票
	 */
	@Column(name = "S_DZFP", length = 500, nullable = true)
	private String sdzfp;
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	public String getSclxx() {
		return sclxx;
	}

	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	public void setSclxx(String value) {
		this.sclxx = value;
	}
	/**
	 * 充值时间
	 */
	public java.util.Date getDczsj() {
		return dczsj;
	}

	/**
	 * 充值时间
	 */
	public void setDczsj(java.util.Date value) {
		this.dczsj = value;
	}
	/**
	 * 充值地点
	 */
	public String getSczdd() {
		return sczdd;
	}

	/**
	 * 充值地点
	 */
	public void setSczdd(String value) {
		this.sczdd = value;
	}
	/**
	 * 充值人
	 */
	public String getSczr() {
		return sczr;
	}

	/**
	 * 充值人
	 */
	public void setSczr(String value) {
		this.sczr = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxfs() {
		return slxfs;
	}

	/**
	 * 联系方式
	 */
	public void setSlxfs(String value) {
		this.slxfs = value;
	}
	/**
	 * 充值金额
	 */
	public String getSczje() {
		return sczje;
	}

	/**
	 * 充值金额
	 */
	public void setSczje(String value) {
		this.sczje = value;
	}
	/**
	 * 电子发票
	 */
	public String getSdzfp() {
		return sdzfp;
	}

	/**
	 * 电子发票
	 */
	public void setSdzfp(String value) {
		this.sdzfp = value;
	}

	@Override
	public void from(GzykczEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzykczEntity toEntity() {
		GzykczEntity toEntity = new GzykczEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}