package com.itfreer.gztaskprogresstate.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gztaskprogresstate.entity.GztaskprogresstateEntity;

/**
 * 定义任务监测实体
 */
 @Entity(name = "GZ_TASK_PROGRESS_RATE")
public class JdbcGztaskprogresstateEntity implements JdbcBaseEntity<GztaskprogresstateEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 任务ID
	 */
	@Column(name = "RWID", length = 500, nullable = true)
	private String rwid;
	
	
	/**
	 * 任务主题
	 */
	@Column(name = "RWZT", length = 500, nullable = true)
	private String rwzt;
	
	
	/**
	 * 项目ID
	 */
	@Column(name = "XMID", length = 500, nullable = true)
	private String xmid;
	
	
	/**
	 * 项目名称
	 */
	@Column(name = "XMMC", length = 500, nullable = true)
	private String xmmc;
	
	/**
	 * 完成进度
	 */
	@Column(name = "WCJD", nullable = true)
	private Double wcjd;
	
	
	/**
	 * 完成情况说明
	 */
	@Column(name = "WCQKSM", length = 500, nullable = true)
	private String wcqksm;
	
	
	/**
	 * 下一步工作安排
	 */
	@Column(name = "XYBJS", length = 500, nullable = true)
	private String xybjs;
	
	/**
	 * 监理时间
	 */
	@Column(name = "JLSJ", nullable = true)
	private java.util.Date jlsj;
	
	
	/**
	 * 监理人ID
	 */
	@Column(name = "JLRID", length = 500, nullable = true)
	private String jlrid;
	
	
	/**
	 * 监理人名称
	 */
	@Column(name = "JLRMC", length = 500, nullable = true)
	private String jlrmc;
	
	
	/**
	 * 完成进度状态（提前，滞后，正常完成，正常进行）
	 */
	@Column(name = "WCJDZT", length = 500, nullable = true)
	private String wcjdzt;
	
	/**
	 * 时间
	 */
	@Column(name = "TBSJ", nullable = true)
	private Double tbsj;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 任务ID
	 */
	public String getRwid() {
		return rwid;
	}

	/**
	 * 任务ID
	 */
	public void setRwid(String value) {
		this.rwid = value;
	}
	/**
	 * 任务主题
	 */
	public String getRwzt() {
		return rwzt;
	}

	/**
	 * 任务主题
	 */
	public void setRwzt(String value) {
		this.rwzt = value;
	}
	/**
	 * 项目ID
	 */
	public String getXmid() {
		return xmid;
	}

	/**
	 * 项目ID
	 */
	public void setXmid(String value) {
		this.xmid = value;
	}
	/**
	 * 项目名称
	 */
	public String getXmmc() {
		return xmmc;
	}

	/**
	 * 项目名称
	 */
	public void setXmmc(String value) {
		this.xmmc = value;
	}
	/**
	 * 完成进度
	 */
	public Double getWcjd() {
		return wcjd;
	}

	/**
	 * 完成进度
	 */
	public void setWcjd(Double value) {
		this.wcjd = value;
	}
	/**
	 * 完成情况说明
	 */
	public String getWcqksm() {
		return wcqksm;
	}

	/**
	 * 完成情况说明
	 */
	public void setWcqksm(String value) {
		this.wcqksm = value;
	}
	/**
	 * 下一步工作安排
	 */
	public String getXybjs() {
		return xybjs;
	}

	/**
	 * 下一步工作安排
	 */
	public void setXybjs(String value) {
		this.xybjs = value;
	}
	/**
	 * 监理时间
	 */
	public java.util.Date getJlsj() {
		return jlsj;
	}

	/**
	 * 监理时间
	 */
	public void setJlsj(java.util.Date value) {
		this.jlsj = value;
	}
	/**
	 * 监理人ID
	 */
	public String getJlrid() {
		return jlrid;
	}

	/**
	 * 监理人ID
	 */
	public void setJlrid(String value) {
		this.jlrid = value;
	}
	/**
	 * 监理人名称
	 */
	public String getJlrmc() {
		return jlrmc;
	}

	/**
	 * 监理人名称
	 */
	public void setJlrmc(String value) {
		this.jlrmc = value;
	}
	/**
	 * 完成进度状态（提前，滞后，正常完成，正常进行）
	 */
	public String getWcjdzt() {
		return wcjdzt;
	}

	/**
	 * 完成进度状态（提前，滞后，正常完成，正常进行）
	 */
	public void setWcjdzt(String value) {
		this.wcjdzt = value;
	}
	/**
	 * 时间
	 */
	public Double getTbsj() {
		return tbsj;
	}

	/**
	 * 时间
	 */
	public void setTbsj(Double value) {
		this.tbsj = value;
	}

	@Override
	public void from(GztaskprogresstateEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GztaskprogresstateEntity toEntity() {
		GztaskprogresstateEntity toEntity = new GztaskprogresstateEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}