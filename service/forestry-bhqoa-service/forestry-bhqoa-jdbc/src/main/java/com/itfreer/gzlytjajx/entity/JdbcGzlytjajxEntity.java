package com.itfreer.gzlytjajx.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzlytjajx.entity.GzlytjajxEntity;

/**
 * 定义林业统计案件续实体
 */
 @Entity(name = "GZ_LYTJAJ_X")
public class JdbcGzlytjajxEntity implements JdbcBaseEntity<GzlytjajxEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 填报单位名称
	 */
	@Column(name = "TBDWMC", length = 500, nullable = true)
	private String tbdwmc;
	
	
	/**
	 * 填报单位ID
	 */
	@Column(name = "TBDWID", length = 500, nullable = true)
	private String tbdwid;
	
	
	/**
	 * 统计期限
	 */
	@Column(name = "TJQX", length = 500, nullable = true)
	private String tjqx;
	
	/**
	 * 统计日期
	 */
	@Column(name = "TJRQ", nullable = true)
	private java.util.Date tjrq;
	
	
	/**
	 * 案件类型
	 */
	@Column(name = "AJLX", length = 500, nullable = true)
	private String ajlx;
	
	/**
	 * 被处罚人数（人）
	 */
	@Column(name = "BCFRS", nullable = true)
	private Double bcfrs;
	
	/**
	 * 没收非法收入所得（万元）
	 */
	@Column(name = "MSFFSR", nullable = true)
	private Double msffsr;
	
	/**
	 * 罚款（万元）
	 */
	@Column(name = "FAKUANG", nullable = true)
	private Double fakuang;
	
	/**
	 * 吊销许可证（件）
	 */
	@Column(name = "DXXKZ", nullable = true)
	private Double dxxkz;
	
	/**
	 * 恢复林地（公顷）
	 */
	@Column(name = "HFLD", nullable = true)
	private Double hfld;
	
	/**
	 * 没收木材（立方米）
	 */
	@Column(name = "MSMC", nullable = true)
	private Double msmc;
	
	/**
	 * 没收种子（公顷）
	 */
	@Column(name = "MSZZ", nullable = true)
	private Double mszz;
	
	/**
	 * 没收幼树或苗木（万株）
	 */
	@Column(name = "MSYMHMM", nullable = true)
	private Double msymhmm;
	
	/**
	 * 补种树木（株）
	 */
	@Column(name = "BZSM", nullable = true)
	private Double bzsm;
	
	/**
	 * 治理或收回国有沙地（公顷）
	 */
	@Column(name = "ZLHSHGYSD", nullable = true)
	private Double zlhshgysd;
	
	/**
	 * 恢复保护区或栖息地（公顷）
	 */
	@Column(name = "HFBHQQXD", nullable = true)
	private Double hfbhqqxd;
	
	/**
	 * 没收野生动物总数（只）
	 */
	@Column(name = "MSYSDWZS", nullable = true)
	private Double msysdwzs;
	
	/**
	 * 没收野生动物国家重点保护对象（只）
	 */
	@Column(name = "MSYSDWGJZDBH", nullable = true)
	private Double msysdwgjzdbh;
	
	/**
	 * 没收野生动物制品总数（件）
	 */
	@Column(name = "MSYSDWZPZS", nullable = true)
	private Double msysdwzpzs;
	
	/**
	 * 没收野生动物制品国家重点保护对象（件）
	 */
	@Column(name = "MSYSDWZPGJZDBH", nullable = true)
	private Double msysdwzpgjzdbh;
	
	/**
	 * 没收野生植物总数（株）
	 */
	@Column(name = "MSYSZWZS", nullable = true)
	private Double msyszwzs;
	
	/**
	 * 没收野生植物国家重点对象（株）
	 */
	@Column(name = "MSYSZWGJZDBH", nullable = true)
	private Double msyszwgjzdbh;
	
	/**
	 * 没收野生植物制品总数（件）
	 */
	@Column(name = "MSYSZWZPZS", nullable = true)
	private Double msyszwzpzs;
	
	/**
	 * 没收野生植物制品国家重点保护对象（件）
	 */
	@Column(name = "MSYSZWZPGJZDBH", nullable = true)
	private Double msyszwzpgjzdbh;
	
	/**
	 * 统计有效期结束时间
	 */
	@Column(name = "TJYXQJSSJ", nullable = true)
	private java.util.Date tjyxqjssj;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 填报单位名称
	 */
	public String getTbdwmc() {
		return tbdwmc;
	}

	/**
	 * 填报单位名称
	 */
	public void setTbdwmc(String value) {
		this.tbdwmc = value;
	}
	/**
	 * 填报单位ID
	 */
	public String getTbdwid() {
		return tbdwid;
	}

	/**
	 * 填报单位ID
	 */
	public void setTbdwid(String value) {
		this.tbdwid = value;
	}
	/**
	 * 统计期限
	 */
	public String getTjqx() {
		return tjqx;
	}

	/**
	 * 统计期限
	 */
	public void setTjqx(String value) {
		this.tjqx = value;
	}
	/**
	 * 统计日期
	 */
	public java.util.Date getTjrq() {
		return tjrq;
	}

	/**
	 * 统计日期
	 */
	public void setTjrq(java.util.Date value) {
		this.tjrq = value;
	}
	/**
	 * 案件类型
	 */
	public String getAjlx() {
		return ajlx;
	}

	/**
	 * 案件类型
	 */
	public void setAjlx(String value) {
		this.ajlx = value;
	}
	/**
	 * 被处罚人数（人）
	 */
	public Double getBcfrs() {
		return bcfrs;
	}

	/**
	 * 被处罚人数（人）
	 */
	public void setBcfrs(Double value) {
		this.bcfrs = value;
	}
	/**
	 * 没收非法收入所得（万元）
	 */
	public Double getMsffsr() {
		return msffsr;
	}

	/**
	 * 没收非法收入所得（万元）
	 */
	public void setMsffsr(Double value) {
		this.msffsr = value;
	}
	/**
	 * 罚款（万元）
	 */
	public Double getFakuang() {
		return fakuang;
	}

	/**
	 * 罚款（万元）
	 */
	public void setFakuang(Double value) {
		this.fakuang = value;
	}
	/**
	 * 吊销许可证（件）
	 */
	public Double getDxxkz() {
		return dxxkz;
	}

	/**
	 * 吊销许可证（件）
	 */
	public void setDxxkz(Double value) {
		this.dxxkz = value;
	}
	/**
	 * 恢复林地（公顷）
	 */
	public Double getHfld() {
		return hfld;
	}

	/**
	 * 恢复林地（公顷）
	 */
	public void setHfld(Double value) {
		this.hfld = value;
	}
	/**
	 * 没收木材（立方米）
	 */
	public Double getMsmc() {
		return msmc;
	}

	/**
	 * 没收木材（立方米）
	 */
	public void setMsmc(Double value) {
		this.msmc = value;
	}
	/**
	 * 没收种子（公顷）
	 */
	public Double getMszz() {
		return mszz;
	}

	/**
	 * 没收种子（公顷）
	 */
	public void setMszz(Double value) {
		this.mszz = value;
	}
	/**
	 * 没收幼树或苗木（万株）
	 */
	public Double getMsymhmm() {
		return msymhmm;
	}

	/**
	 * 没收幼树或苗木（万株）
	 */
	public void setMsymhmm(Double value) {
		this.msymhmm = value;
	}
	/**
	 * 补种树木（株）
	 */
	public Double getBzsm() {
		return bzsm;
	}

	/**
	 * 补种树木（株）
	 */
	public void setBzsm(Double value) {
		this.bzsm = value;
	}
	/**
	 * 治理或收回国有沙地（公顷）
	 */
	public Double getZlhshgysd() {
		return zlhshgysd;
	}

	/**
	 * 治理或收回国有沙地（公顷）
	 */
	public void setZlhshgysd(Double value) {
		this.zlhshgysd = value;
	}
	/**
	 * 恢复保护区或栖息地（公顷）
	 */
	public Double getHfbhqqxd() {
		return hfbhqqxd;
	}

	/**
	 * 恢复保护区或栖息地（公顷）
	 */
	public void setHfbhqqxd(Double value) {
		this.hfbhqqxd = value;
	}
	/**
	 * 没收野生动物总数（只）
	 */
	public Double getMsysdwzs() {
		return msysdwzs;
	}

	/**
	 * 没收野生动物总数（只）
	 */
	public void setMsysdwzs(Double value) {
		this.msysdwzs = value;
	}
	/**
	 * 没收野生动物国家重点保护对象（只）
	 */
	public Double getMsysdwgjzdbh() {
		return msysdwgjzdbh;
	}

	/**
	 * 没收野生动物国家重点保护对象（只）
	 */
	public void setMsysdwgjzdbh(Double value) {
		this.msysdwgjzdbh = value;
	}
	/**
	 * 没收野生动物制品总数（件）
	 */
	public Double getMsysdwzpzs() {
		return msysdwzpzs;
	}

	/**
	 * 没收野生动物制品总数（件）
	 */
	public void setMsysdwzpzs(Double value) {
		this.msysdwzpzs = value;
	}
	/**
	 * 没收野生动物制品国家重点保护对象（件）
	 */
	public Double getMsysdwzpgjzdbh() {
		return msysdwzpgjzdbh;
	}

	/**
	 * 没收野生动物制品国家重点保护对象（件）
	 */
	public void setMsysdwzpgjzdbh(Double value) {
		this.msysdwzpgjzdbh = value;
	}
	/**
	 * 没收野生植物总数（株）
	 */
	public Double getMsyszwzs() {
		return msyszwzs;
	}

	/**
	 * 没收野生植物总数（株）
	 */
	public void setMsyszwzs(Double value) {
		this.msyszwzs = value;
	}
	/**
	 * 没收野生植物国家重点对象（株）
	 */
	public Double getMsyszwgjzdbh() {
		return msyszwgjzdbh;
	}

	/**
	 * 没收野生植物国家重点对象（株）
	 */
	public void setMsyszwgjzdbh(Double value) {
		this.msyszwgjzdbh = value;
	}
	/**
	 * 没收野生植物制品总数（件）
	 */
	public Double getMsyszwzpzs() {
		return msyszwzpzs;
	}

	/**
	 * 没收野生植物制品总数（件）
	 */
	public void setMsyszwzpzs(Double value) {
		this.msyszwzpzs = value;
	}
	/**
	 * 没收野生植物制品国家重点保护对象（件）
	 */
	public Double getMsyszwzpgjzdbh() {
		return msyszwzpgjzdbh;
	}

	/**
	 * 没收野生植物制品国家重点保护对象（件）
	 */
	public void setMsyszwzpgjzdbh(Double value) {
		this.msyszwzpgjzdbh = value;
	}
	/**
	 * 统计有效期结束时间
	 */
	public java.util.Date getTjyxqjssj() {
		return tjyxqjssj;
	}

	/**
	 * 统计有效期结束时间
	 */
	public void setTjyxqjssj(java.util.Date value) {
		this.tjyxqjssj = value;
	}

	@Override
	public void from(GzlytjajxEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzlytjajxEntity toEntity() {
		GzlytjajxEntity toEntity = new GzlytjajxEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}