package com.itfreer.gzreceiveview.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzreceiveview.entity.GzreceiveviewEntity;

/**
 * 定义收文管理实体
 */
 @Entity(name = "GZ_RECEIVE_VIEW")
public class JdbcGzreceiveviewEntity implements JdbcBaseEntity<GzreceiveviewEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	
	/**
	 * 紧急等级
	 */
	@Column(name = "JJDJ", length = 500, nullable = true)
	private String jjdj;
	

	public String getJjdj() {
		return jjdj;
	}

	public void setJjdj(String jjdj) {
		this.jjdj = jjdj;
	}
	
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 发文主题
	 */
	@Column(name = "FWZT", length = 500, nullable = true)
	private String fwzt;
	
	
	/**
	 * 发文说明
	 */
	@Column(name = "FWSM", length = 500, nullable = true)
	private String fwsm;
	
	
	/**
	 * 创建人ID
	 */
	@Column(name = "CJRID", length = 500, nullable = true)
	private String cjrid;
	
	
	/**
	 * 创建人名称
	 */
	@Column(name = "CJRMC", length = 500, nullable = true)
	private String cjrmc;
	
	/**
	 * 创建时间
	 */
	@Column(name = "CJSJ", nullable = true)
	private java.util.Date cjsj;
	

	
	
	/**
	 * 附件
	 */
	@Column(name = "FJ", length = 500, nullable = true)
	private String fj;
	
	
	/**
	 * 发文ID
	 */
	@Column(name = "FWID", length = 500, nullable = true)
	private String fwid;
	
	
	/**
	 * 接收人ID
	 */
	@Column(name = "JSRID", length = 500, nullable = true)
	private String jsrid;
	
	
	/**
	 * 接收人名称
	 */
	@Column(name = "JSRMC", length = 500, nullable = true)
	private String jsrmc;
	
	
	/**
	 * 接收人意见
	 */
	@Column(name = "JSRYJ", length = 500, nullable = true)
	private String jsryj;
	
	
	public String getJsryj() {
		return jsryj;
	}

	public void setJsryj(String jsryj) {
		this.jsryj = jsryj;
	}
	
	/**
	 * 接收人职位
	 */
	@Column(name = "JSRZW", length = 500, nullable = true)
	private String jsrzw;
	
	public String getJsrzw() {
		return jsrzw;
	}

	public void setJsrzw(String jsrzw) {
		this.jsrzw = jsrzw;
	}
	
	
	/**
	 * 是否已读
	 */
	@Column(name = "SFYD", length = 500, nullable = true)
	private String sfyd;
	
	/**
	 * 阅读时间
	 */
	@Column(name = "YDSJ", nullable = true)
	private java.util.Date ydsj;
	
	
	/**
	 * 是否已确认
	 */
	@Column(name = "SFQR", length = 500, nullable = true)
	private String sfqr;
	
	
	/**
	 * 接收人机构
	 */
	@Column(name = "JSRJG", length = 100, nullable = true)
	private String jsrjg;
	
	/**
	 * 接收人机构
	 */
	@Column(name = "JSRJGMC", length = 100, nullable = true)
	private String jsrjgmc;
	
	public String getJsrjgmc() {
		return jsrjgmc;
	}

	public void setJsrjgmc(String jsrjgmc) {
		this.jsrjgmc = jsrjgmc;
	}

	/**
	 * 部门名称
	 */
	@Column(name = "BMMC", length = 100, nullable = true)
	private String bmmc;
	
	public String getBmmc() {
		return bmmc;
	}

	public void setBmmc(String bmmc) {
		this.bmmc = bmmc;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 发文主题
	 */
	public String getFwzt() {
		return fwzt;
	}

	/**
	 * 发文主题
	 */
	public void setFwzt(String value) {
		this.fwzt = value;
	}
	/**
	 * 发文说明
	 */
	public String getFwsm() {
		return fwsm;
	}

	/**
	 * 发文说明
	 */
	public void setFwsm(String value) {
		this.fwsm = value;
	}
	/**
	 * 创建人ID
	 */
	public String getCjrid() {
		return cjrid;
	}

	/**
	 * 创建人ID
	 */
	public void setCjrid(String value) {
		this.cjrid = value;
	}
	/**
	 * 创建人名称
	 */
	public String getCjrmc() {
		return cjrmc;
	}

	/**
	 * 创建人名称
	 */
	public void setCjrmc(String value) {
		this.cjrmc = value;
	}
	/**
	 * 创建时间
	 */
	public java.util.Date getCjsj() {
		return cjsj;
	}

	/**
	 * 创建时间
	 */
	public void setCjsj(java.util.Date value) {
		this.cjsj = value;
	}
	/**
	 * 附件
	 */
	public String getFj() {
		return fj;
	}

	/**
	 * 附件
	 */
	public void setFj(String value) {
		this.fj = value;
	}
	/**
	 * 发文ID
	 */
	public String getFwid() {
		return fwid;
	}

	/**
	 * 发文ID
	 */
	public void setFwid(String value) {
		this.fwid = value;
	}
	/**
	 * 接收人ID
	 */
	public String getJsrid() {
		return jsrid;
	}

	/**
	 * 接收人ID
	 */
	public void setJsrid(String value) {
		this.jsrid = value;
	}
	/**
	 * 接收人名称
	 */
	public String getJsrmc() {
		return jsrmc;
	}

	/**
	 * 接收人名称
	 */
	public void setJsrmc(String value) {
		this.jsrmc = value;
	}
	/**
	 * 是否已读
	 */
	public String getSfyd() {
		return sfyd;
	}

	/**
	 * 是否已读
	 */
	public void setSfyd(String value) {
		this.sfyd = value;
	}
	/**
	 * 阅读时间
	 */
	public java.util.Date getYdsj() {
		return ydsj;
	}

	/**
	 * 阅读时间
	 */
	public void setYdsj(java.util.Date value) {
		this.ydsj = value;
	}
	/**
	 * 是否已确认
	 */
	public String getSfqr() {
		return sfqr;
	}

	/**
	 * 是否已确认
	 */
	public void setSfqr(String value) {
		this.sfqr = value;
	}
	/**
	 * 接收人机构
	 */
	public String getJsrjg() {
		return jsrjg;
	}

	/**
	 * 接收人机构
	 */
	public void setJsrjg(String value) {
		this.jsrjg = value;
	}

	@Override
	public void from(GzreceiveviewEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzreceiveviewEntity toEntity() {
		GzreceiveviewEntity toEntity = new GzreceiveviewEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
	

	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	
	/**
	 * 办理情况
	 */
	@Column(name = "BLQK", length = 500, nullable = true)
	private String blqk;
	
	public String getBlqk() {
		return blqk;
	}

	public void setBlqk(String blqk) {
		this.blqk = blqk;
	}
	
	/**
	 * 主要负责人
	 */
	@Column(name = "zyfzrid",length=50,nullable=true)
	private String zyfzrid;
	
	/**
	 * 主要负责人
	 */
	@Column(name = "zyfzrname",length=50,nullable=true)
	private String zyfzrname;

	public String getZyfzrid() {
		return zyfzrid;
	}

	public void setZyfzrid(String zyfzrid) {
		this.zyfzrid = zyfzrid;
	}

	public String getZyfzrname() {
		return zyfzrname;
	}

	public void setZyfzrname(String zyfzrname) {
		this.zyfzrname = zyfzrname;
	}
}