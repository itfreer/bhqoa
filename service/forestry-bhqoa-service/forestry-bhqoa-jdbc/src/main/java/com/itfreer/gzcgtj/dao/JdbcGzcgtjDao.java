package com.itfreer.gzcgtj.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzcgtj.dao.GzcgtjDao;
import com.itfreer.gzcgtj.entity.GzcgtjEntity;
import com.itfreer.gzcgtj.entity.JdbcGzcgtjEntity;

/**
 * 定义采购统计 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzcgtjDao")
public class JdbcGzcgtjDao extends JdbcBaseDaoImp<GzcgtjEntity, JdbcGzcgtjEntity> implements GzcgtjDao {

}
