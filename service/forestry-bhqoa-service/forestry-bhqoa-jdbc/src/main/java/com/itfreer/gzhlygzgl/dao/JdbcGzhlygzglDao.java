package com.itfreer.gzhlygzgl.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzhlygzgl.dao.GzhlygzglDao;
import com.itfreer.gzhlygzgl.entity.GzhlygzglEntity;
import com.itfreer.gzhlygzgl.entity.JdbcGzhlygzglEntity;

/**
 * 定义护林员工资管理 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzhlygzglDao")
public class JdbcGzhlygzglDao extends JdbcBaseDaoImp<GzhlygzglEntity, JdbcGzhlygzglEntity> implements GzhlygzglDao {

}
