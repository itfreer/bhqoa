package com.itfreer.gzvisitorstatisticsview.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzvisitorstatisticsview.dao.GzvisitorstatisticsviewDao;
import com.itfreer.gzvisitorstatisticsview.entity.GzvisitorstatisticsviewEntity;
import com.itfreer.gzvisitorstatisticsview.entity.JdbcGzvisitorstatisticsviewEntity;

/**
 * 定义访客统计（视图） jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzvisitorstatisticsviewDao")
public class JdbcGzvisitorstatisticsviewDao extends JdbcBaseDaoImp<GzvisitorstatisticsviewEntity, JdbcGzvisitorstatisticsviewEntity> implements GzvisitorstatisticsviewDao {

}
