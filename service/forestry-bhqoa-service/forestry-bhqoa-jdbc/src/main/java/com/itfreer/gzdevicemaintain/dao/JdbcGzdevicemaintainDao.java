package com.itfreer.gzdevicemaintain.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzdevicemaintain.dao.GzdevicemaintainDao;
import com.itfreer.gzdevicemaintain.entity.GzdevicemaintainEntity;
import com.itfreer.gzdevicemaintain.entity.JdbcGzdevicemaintainEntity;

/**
 * 定义设备维修记录 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzdevicemaintainDao")
public class JdbcGzdevicemaintainDao extends JdbcBaseDaoImp<GzdevicemaintainEntity, JdbcGzdevicemaintainEntity> implements GzdevicemaintainDao {

}
