package com.itfreer.gzlytjajx.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzlytjajx.dao.GzlytjajxDao;
import com.itfreer.gzlytjajx.entity.GzlytjajxEntity;
import com.itfreer.gzlytjajx.entity.JdbcGzlytjajxEntity;

/**
 * 定义林业统计案件续 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzlytjajxDao")
public class JdbcGzlytjajxDao extends JdbcBaseDaoImp<GzlytjajxEntity, JdbcGzlytjajxEntity> implements GzlytjajxDao {

}
