package com.itfreer.gzgzrqdsj.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzgzrqdsj.dao.GzgzrqdsjDao;
import com.itfreer.gzgzrqdsj.entity.GzgzrqdsjEntity;
import com.itfreer.gzgzrqdsj.entity.JdbcGzgzrqdsjEntity;

/**
 * 定义签到时间 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzgzrqdsjDao")
public class JdbcGzgzrqdsjDao extends JdbcBaseDaoImp<GzgzrqdsjEntity, JdbcGzgzrqdsjEntity> implements GzgzrqdsjDao {

}
