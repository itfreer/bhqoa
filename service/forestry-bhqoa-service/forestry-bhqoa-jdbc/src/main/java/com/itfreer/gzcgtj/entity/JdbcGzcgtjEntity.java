package com.itfreer.gzcgtj.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzcgtj.entity.GzcgtjEntity;

/**
 * 定义采购统计实体
 */
 @Entity(name = "GZ_CGTJ")
public class JdbcGzcgtjEntity implements JdbcBaseEntity<GzcgtjEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 采购年度
	 */
	@Column(name = "S_CGND", length = 500, nullable = true)
	private String scgnd;
	
	
	/**
	 * 采购月份
	 */
	@Column(name = "S_CGYF", length = 500, nullable = true)
	private String scgyf;
	
	
	/**
	 * 采购设备名称
	 */
	@Column(name = "S_CGSBMC", length = 500, nullable = true)
	private String scgsbmc;
	
	
	/**
	 * 采购设备数量
	 */
	@Column(name = "D_CGSBSL", length = 500, nullable = true)
	private String dcgsbsl;
	
	
	/**
	 * 采购费用
	 */
	@Column(name = "S_CGFY", length = 500, nullable = true)
	private String scgfy;
	
	
	/**
	 * 采购用途
	 */
	@Column(name = "S_CGYT", length = 500, nullable = true)
	private String scgyt;
	
	
	/**
	 * 采购部门
	 */
	@Column(name = "S_CGBM", length = 500, nullable = true)
	private String scgbm;
	
	
	/**
	 * 采购人
	 */
	@Column(name = "S_CGR", length = 500, nullable = true)
	private String scgr;
	
	
	/**
	 * 联系方式
	 */
	@Column(name = "S_LXFS", length = 500, nullable = true)
	private String slxfs;
	
	
	/**
	 * 是否同意采购
	 */
	@Column(name = "I_TYCG", length = 500, nullable = true)
	private String itycg;
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 采购年度
	 */
	public String getScgnd() {
		return scgnd;
	}

	/**
	 * 采购年度
	 */
	public void setScgnd(String value) {
		this.scgnd = value;
	}
	/**
	 * 采购月份
	 */
	public String getScgyf() {
		return scgyf;
	}

	/**
	 * 采购月份
	 */
	public void setScgyf(String value) {
		this.scgyf = value;
	}
	/**
	 * 采购设备名称
	 */
	public String getScgsbmc() {
		return scgsbmc;
	}

	/**
	 * 采购设备名称
	 */
	public void setScgsbmc(String value) {
		this.scgsbmc = value;
	}
	/**
	 * 采购设备数量
	 */
	public String getDcgsbsl() {
		return dcgsbsl;
	}

	/**
	 * 采购设备数量
	 */
	public void setDcgsbsl(String value) {
		this.dcgsbsl = value;
	}
	/**
	 * 采购费用
	 */
	public String getScgfy() {
		return scgfy;
	}

	/**
	 * 采购费用
	 */
	public void setScgfy(String value) {
		this.scgfy = value;
	}
	/**
	 * 采购用途
	 */
	public String getScgyt() {
		return scgyt;
	}

	/**
	 * 采购用途
	 */
	public void setScgyt(String value) {
		this.scgyt = value;
	}
	/**
	 * 采购部门
	 */
	public String getScgbm() {
		return scgbm;
	}

	/**
	 * 采购部门
	 */
	public void setScgbm(String value) {
		this.scgbm = value;
	}
	/**
	 * 采购人
	 */
	public String getScgr() {
		return scgr;
	}

	/**
	 * 采购人
	 */
	public void setScgr(String value) {
		this.scgr = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxfs() {
		return slxfs;
	}

	/**
	 * 联系方式
	 */
	public void setSlxfs(String value) {
		this.slxfs = value;
	}
	/**
	 * 是否同意采购
	 */
	public String getItycg() {
		return itycg;
	}

	/**
	 * 是否同意采购
	 */
	public void setItycg(String value) {
		this.itycg = value;
	}

	@Override
	public void from(GzcgtjEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzcgtjEntity toEntity() {
		GzcgtjEntity toEntity = new GzcgtjEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}