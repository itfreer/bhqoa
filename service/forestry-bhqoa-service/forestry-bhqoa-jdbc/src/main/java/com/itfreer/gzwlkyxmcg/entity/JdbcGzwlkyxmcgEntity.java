package com.itfreer.gzwlkyxmcg.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzwlkyxmcg.entity.GzwlkyxmcgEntity;

/**
 * 定义外来科研项目成果实体
 */
 @Entity(name = "GZ_WLKY_XMCG")
public class JdbcGzwlkyxmcgEntity implements JdbcBaseEntity<GzwlkyxmcgEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 附件编号
	 */
	@Column(name = "FJBH", length = 500, nullable = true)
	private String fjbh;
	
	
	/**
	 * 附件名称
	 */
	@Column(name = "FJMC", length = 500, nullable = true)
	private String fjmc;
	
	/**
	 * 上传时间
	 */
	@Column(name = "SCSJ", nullable = true)
	private java.util.Date scsj;
	
	
	/**
	 * 上传人员
	 */
	@Column(name = "SCRYID", length = 500, nullable = true)
	private String scryid;
	
	
	/**
	 * 上传人员名称
	 */
	@Column(name = "SCRYMC", length = 500, nullable = true)
	private String scrymc;
	
	
	/**
	 * 项目编号
	 */
	@Column(name = "XMBH", length = 500, nullable = true)
	private String xmbh;
	
	
	/**
	 * 存档位置
	 */
	@Column(name = "CDWZ", length = 500, nullable = true)
	private String cdwz;
	
	
	/**
	 * 备注
	 */
	@Column(name = "BZ", length = 500, nullable = true)
	private String bz;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 附件编号
	 */
	public String getFjbh() {
		return fjbh;
	}

	/**
	 * 附件编号
	 */
	public void setFjbh(String value) {
		this.fjbh = value;
	}
	/**
	 * 附件名称
	 */
	public String getFjmc() {
		return fjmc;
	}

	/**
	 * 附件名称
	 */
	public void setFjmc(String value) {
		this.fjmc = value;
	}
	/**
	 * 上传时间
	 */
	public java.util.Date getScsj() {
		return scsj;
	}

	/**
	 * 上传时间
	 */
	public void setScsj(java.util.Date value) {
		this.scsj = value;
	}
	/**
	 * 上传人员
	 */
	public String getScryid() {
		return scryid;
	}

	/**
	 * 上传人员
	 */
	public void setScryid(String value) {
		this.scryid = value;
	}
	/**
	 * 上传人员名称
	 */
	public String getScrymc() {
		return scrymc;
	}

	/**
	 * 上传人员名称
	 */
	public void setScrymc(String value) {
		this.scrymc = value;
	}
	/**
	 * 项目编号
	 */
	public String getXmbh() {
		return xmbh;
	}

	/**
	 * 项目编号
	 */
	public void setXmbh(String value) {
		this.xmbh = value;
	}
	/**
	 * 存档位置
	 */
	public String getCdwz() {
		return cdwz;
	}

	/**
	 * 存档位置
	 */
	public void setCdwz(String value) {
		this.cdwz = value;
	}
	/**
	 * 备注
	 */
	public String getBz() {
		return bz;
	}

	/**
	 * 备注
	 */
	public void setBz(String value) {
		this.bz = value;
	}

	@Override
	public void from(GzwlkyxmcgEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzwlkyxmcgEntity toEntity() {
		GzwlkyxmcgEntity toEntity = new GzwlkyxmcgEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}