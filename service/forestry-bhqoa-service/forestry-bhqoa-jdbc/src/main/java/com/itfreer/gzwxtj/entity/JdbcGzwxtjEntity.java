package com.itfreer.gzwxtj.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzwxtj.entity.GzwxtjEntity;

/**
 * 定义维修统计实体
 */
 @Entity(name = "GZ_WXTJ")
public class JdbcGzwxtjEntity implements JdbcBaseEntity<GzwxtjEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 维修年度
	 */
	@Column(name = "S_WXND", length = 500, nullable = true)
	private String swxnd;
	
	
	/**
	 * 维修月份
	 */
	@Column(name = "S_WXYF", length = 500, nullable = true)
	private String swxyf;
	
	
	/**
	 * 维修设备名称
	 */
	@Column(name = "S_WXSBMC", length = 500, nullable = true)
	private String swxsbmc;
	
	
	/**
	 * 维修设备数量
	 */
	@Column(name = "S_WXSBSL", length = 500, nullable = true)
	private String swxsbsl;
	
	
	/**
	 * 维修次数
	 */
	@Column(name = "S_WXCS", length = 500, nullable = true)
	private String swxcs;
	
	
	/**
	 * 维修费用
	 */
	@Column(name = "S_WXFY", length = 500, nullable = true)
	private String swxfy;
	
	
	/**
	 * 负责人
	 */
	@Column(name = "S_FZR", length = 500, nullable = true)
	private String sfzr;
	
	
	/**
	 * 联系方式
	 */
	@Column(name = "S_LXFS", length = 500, nullable = true)
	private String slxfs;
	
	
	/**
	 * 电子发票
	 */
	@Column(name = "S_DZFP", length = 500, nullable = true)
	private String sdzfp;
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 维修年度
	 */
	public String getSwxnd() {
		return swxnd;
	}

	/**
	 * 维修年度
	 */
	public void setSwxnd(String value) {
		this.swxnd = value;
	}
	/**
	 * 维修月份
	 */
	public String getSwxyf() {
		return swxyf;
	}

	/**
	 * 维修月份
	 */
	public void setSwxyf(String value) {
		this.swxyf = value;
	}
	/**
	 * 维修设备名称
	 */
	public String getSwxsbmc() {
		return swxsbmc;
	}

	/**
	 * 维修设备名称
	 */
	public void setSwxsbmc(String value) {
		this.swxsbmc = value;
	}
	/**
	 * 维修设备数量
	 */
	public String getSwxsbsl() {
		return swxsbsl;
	}

	/**
	 * 维修设备数量
	 */
	public void setSwxsbsl(String value) {
		this.swxsbsl = value;
	}
	/**
	 * 维修次数
	 */
	public String getSwxcs() {
		return swxcs;
	}

	/**
	 * 维修次数
	 */
	public void setSwxcs(String value) {
		this.swxcs = value;
	}
	/**
	 * 维修费用
	 */
	public String getSwxfy() {
		return swxfy;
	}

	/**
	 * 维修费用
	 */
	public void setSwxfy(String value) {
		this.swxfy = value;
	}
	/**
	 * 负责人
	 */
	public String getSfzr() {
		return sfzr;
	}

	/**
	 * 负责人
	 */
	public void setSfzr(String value) {
		this.sfzr = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxfs() {
		return slxfs;
	}

	/**
	 * 联系方式
	 */
	public void setSlxfs(String value) {
		this.slxfs = value;
	}
	/**
	 * 电子发票
	 */
	public String getSdzfp() {
		return sdzfp;
	}

	/**
	 * 电子发票
	 */
	public void setSdzfp(String value) {
		this.sdzfp = value;
	}

	@Override
	public void from(GzwxtjEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzwxtjEntity toEntity() {
		GzwxtjEntity toEntity = new GzwxtjEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}