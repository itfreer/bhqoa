package com.itfreer.gzyzgl.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzyzgl.dao.GzyzglDao;
import com.itfreer.gzyzgl.entity.GzyzglEntity;
import com.itfreer.gzyzgl.entity.JdbcGzyzglEntity;

/**
 * 定义用章管理 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzyzglDao")
public class JdbcGzyzglDao extends JdbcBaseDaoImp<GzyzglEntity, JdbcGzyzglEntity> implements GzyzglDao {

}
