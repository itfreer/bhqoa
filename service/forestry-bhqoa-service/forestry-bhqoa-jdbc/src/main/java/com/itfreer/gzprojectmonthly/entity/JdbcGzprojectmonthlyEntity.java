package com.itfreer.gzprojectmonthly.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzprojectmonthly.entity.GzprojectmonthlyEntity;

/**
 * 定义项目月报实体
 */
 @Entity(name = "GZ_PROJECT_MONTHLY")
public class JdbcGzprojectmonthlyEntity implements JdbcBaseEntity<GzprojectmonthlyEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 100, nullable = true)
	private String id;
	
	/**
	 * I_VERSION
	 */
	@Column(name = "I_VERSION", nullable = true)
	private Integer iversion;
	
	
	/**
	 * 项目编号
	 */
	@Column(name = "S_XMBH", length = 50, nullable = true)
	private String sxmbh;
	
	/**
	 * 项目名称
	 */
	@Column(name = "S_XMMC", length = 50, nullable = true)
	private String sxmmc;
	
	/**
	 * 进度月报编号
	 */
	@Column(name = "I_JDYBBH", nullable = true)
	private Integer ijdybbh;
	
	
	/**
	 * 月度（年度）
	 */
	@Column(name = "S_YD", length = 50, nullable = true)
	private String syd;
	
	
	/**
	 * 项目经理
	 */
	@Column(name = "S_XMJL", length = 50, nullable = true)
	private String sxmjl;
	
	
	/**
	 * 项目副经理
	 */
	@Column(name = "S_XMFJL", length = 50, nullable = true)
	private String sxmfjl;
	
	
	/**
	 * 技术负责人
	 */
	@Column(name = "S_JSFZR", length = 50, nullable = true)
	private String sjsfzr;
	
	/**
	 * 项目人员数
	 */
	@Column(name = "I_XMRYS", nullable = true)
	private Integer ixmrys;
	
	
	/**
	 * 作业方法
	 */
	@Column(name = "S_ZYFF", length = 50, nullable = true)
	private String szyff;
	
	/**
	 * 进行测绘登记时间
	 */
	@Column(name = "D_DJSJ", nullable = true)
	private java.util.Date ddjsj;
	
	
	/**
	 * 项目id
	 */
	@Column(name = "XMID", length = 100, nullable = true)
	private String xmid;
	
	public String getXmid() {
		return xmid;
	}

	public void setXmid(String xmid) {
		this.xmid = xmid;
	}
	
	/**
	 * 进行测绘登记地点
	 */
	@Column(name = "S_DJDD", length = 50, nullable = true)
	private String sdjdd;
	
	/**
	 * 开始时间
	 */
	@Column(name = "D_KSSJ", nullable = true)
	private java.util.Date dkssj;
	
	/**
	 * 结束时间
	 */
	@Column(name = "D_JSSJ", nullable = true)
	private java.util.Date djssj;
	
	
	/**
	 * 问题与措施
	 */
	@Column(name = "S_WD", length = 500, nullable = true)
	private String swd;
	
	
	/**
	 * 其他
	 */
	@Column(name = "S_QT", length = 500, nullable = true)
	private String sqt;
	
	
	/**
	 * 提交人
	 */
	@Column(name = "S_TJR", length = 50, nullable = true)
	private String stjr;
	
	/**
	 * 提交时间
	 */
	@Column(name = "D_TJSJ", nullable = true)
	private java.util.Date dtjsj;
	
	/**
	 * 项目进度
	 */
	@Column(name = "XMJD", nullable = true)
	private Double xmjd;
	
	
	
	public Double getXmjd() {
		return xmjd;
	}

	public void setXmjd(Double xmjd) {
		this.xmjd = xmjd;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * I_VERSION
	 */
	public Integer getIversion() {
		return iversion;
	}

	/**
	 * I_VERSION
	 */
	public void setIversion(Integer value) {
		this.iversion = value;
	}
	/**
	 * 项目编号
	 */
	public String getSxmbh() {
		return sxmbh;
	}

	/**
	 * 项目编号
	 */
	public void setSxmbh(String value) {
		this.sxmbh = value;
	}
	/**
	 * 项目名称
	 */
	public String getSxmmc() {
		return sxmmc;
	}

	/**
	 * 项目名称
	 */
	public void setSxmmc(String value) {
		this.sxmmc = value;
	}
	/**
	 * 进度月报编号
	 */
	public Integer getIjdybbh() {
		return ijdybbh;
	}

	/**
	 * 进度月报编号
	 */
	public void setIjdybbh(Integer value) {
		this.ijdybbh = value;
	}
	/**
	 * 月度（年度）
	 */
	public String getSyd() {
		return syd;
	}

	/**
	 * 月度（年度）
	 */
	public void setSyd(String value) {
		this.syd = value;
	}
	/**
	 * 项目经理
	 */
	public String getSxmjl() {
		return sxmjl;
	}

	/**
	 * 项目经理
	 */
	public void setSxmjl(String value) {
		this.sxmjl = value;
	}
	/**
	 * 项目副经理
	 */
	public String getSxmfjl() {
		return sxmfjl;
	}

	/**
	 * 项目副经理
	 */
	public void setSxmfjl(String value) {
		this.sxmfjl = value;
	}
	/**
	 * 技术负责人
	 */
	public String getSjsfzr() {
		return sjsfzr;
	}

	/**
	 * 技术负责人
	 */
	public void setSjsfzr(String value) {
		this.sjsfzr = value;
	}
	/**
	 * 项目人员数
	 */
	public Integer getIxmrys() {
		return ixmrys;
	}

	/**
	 * 项目人员数
	 */
	public void setIxmrys(Integer value) {
		this.ixmrys = value;
	}
	/**
	 * 作业方法
	 */
	public String getSzyff() {
		return szyff;
	}

	/**
	 * 作业方法
	 */
	public void setSzyff(String value) {
		this.szyff = value;
	}
	/**
	 * 进行测绘登记时间
	 */
	public java.util.Date getDdjsj() {
		return ddjsj;
	}

	/**
	 * 进行测绘登记时间
	 */
	public void setDdjsj(java.util.Date value) {
		this.ddjsj = value;
	}
	/**
	 * 进行测绘登记地点
	 */
	public String getSdjdd() {
		return sdjdd;
	}

	/**
	 * 进行测绘登记地点
	 */
	public void setSdjdd(String value) {
		this.sdjdd = value;
	}
	/**
	 * 开始时间
	 */
	public java.util.Date getDkssj() {
		return dkssj;
	}

	/**
	 * 开始时间
	 */
	public void setDkssj(java.util.Date value) {
		this.dkssj = value;
	}
	/**
	 * 结束时间
	 */
	public java.util.Date getDjssj() {
		return djssj;
	}

	/**
	 * 结束时间
	 */
	public void setDjssj(java.util.Date value) {
		this.djssj = value;
	}
	/**
	 * 问题与措施
	 */
	public String getSwd() {
		return swd;
	}

	/**
	 * 问题与措施
	 */
	public void setSwd(String value) {
		this.swd = value;
	}
	/**
	 * 其他
	 */
	public String getSqt() {
		return sqt;
	}

	/**
	 * 其他
	 */
	public void setSqt(String value) {
		this.sqt = value;
	}
	/**
	 * 提交人
	 */
	public String getStjr() {
		return stjr;
	}

	/**
	 * 提交人
	 */
	public void setStjr(String value) {
		this.stjr = value;
	}
	/**
	 * 提交时间
	 */
	public java.util.Date getDtjsj() {
		return dtjsj;
	}

	/**
	 * 提交时间
	 */
	public void setDtjsj(java.util.Date value) {
		this.dtjsj = value;
	}

	@Override
	public void from(GzprojectmonthlyEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzprojectmonthlyEntity toEntity() {
		GzprojectmonthlyEntity toEntity = new GzprojectmonthlyEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}