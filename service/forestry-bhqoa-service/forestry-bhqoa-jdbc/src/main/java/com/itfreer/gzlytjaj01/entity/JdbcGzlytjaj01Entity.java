package com.itfreer.gzlytjaj01.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzlytjaj01.entity.Gzlytjaj01Entity;

/**
 * 定义林业统计案件实体
 */
 @Entity(name = "GZ_LYTJAJ_01")
public class JdbcGzlytjaj01Entity implements JdbcBaseEntity<Gzlytjaj01Entity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	
	/**
	 * 填报单位名称
	 */
	@Column(name = "TBDWMC", length = 500, nullable = true)
	private String tbdwmc;
	
	
	/**
	 * 填报单位ID
	 */
	@Column(name = "TBDWID", length = 500, nullable = true)
	private String tbdwid;
	
	
	/**
	 * 统计期限
	 */
	@Column(name = "TJQX", length = 500, nullable = true)
	private String tjqx;
	
	/**
	 * 统计日期
	 */
	@Column(name = "TJRQ", nullable = true)
	private java.util.Date tjrq;
	
	
	/**
	 * 案件类型
	 */
	@Column(name = "AJLX", length = 500, nullable = true)
	private String ajlx;
	
	/**
	 * 统计有效期结束时间
	 */
	@Column(name = "TJYXQJSSJ", nullable = true)
	private java.util.Date tjyxqjssj;
	
	/**
	 * 案件数量总数
	 */
	@Column(name = "AJSLZS", nullable = true)
	private Double ajslzs;
	
	/**
	 * 案件数量_公民
	 */
	@Column(name = "AJSLGM", nullable = true)
	private Double ajslgm;
	
	/**
	 * 案件数量_法人
	 */
	@Column(name = "AJSLFR", nullable = true)
	private Double ajslfr;
	
	/**
	 * 案件数量其他组织
	 */
	@Column(name = "AJSLQT", nullable = true)
	private Double ajslqt;
	
	/**
	 * 查结案件总数
	 */
	@Column(name = "CJAJZS", nullable = true)
	private Double cjajzs;
	
	/**
	 * 查结案件结案率
	 */
	@Column(name = "CJAJJAL", nullable = true)
	private Double cjajjal;
	
	/**
	 * 林地面积（公顷）
	 */
	@Column(name = "LMMJ", nullable = true)
	private Double lmmj;
	
	/**
	 * 林木蓄积（立方米）
	 */
	@Column(name = "LMXJ", nullable = true)
	private Double lmxj;
	
	/**
	 * 竹子（万株）
	 */
	@Column(name = "ZJZS", nullable = true)
	private Double zjzs;
	
	/**
	 * 种子（公顷）
	 */
	@Column(name = "ZHOGNZI", nullable = true)
	private Double zhognzi;
	
	/**
	 * 幼树或苗木（万株）
	 */
	@Column(name = "YMHMM", nullable = true)
	private Double ymhmm;
	
	/**
	 * 沙地（公顷）
	 */
	@Column(name = "SHADI", nullable = true)
	private Double shadi;
	
	/**
	 * 保护区或栖息地（公顷）
	 */
	@Column(name = "BHQHQXD", nullable = true)
	private Double bhqhqxd;
	
	/**
	 * 野生动物总数（只）
	 */
	@Column(name = "YSDWZS", nullable = true)
	private Double ysdwzs;
	
	/**
	 * 野生动物国家重点保护对象（只）
	 */
	@Column(name = "YSDWGJZDBH", nullable = true)
	private Double ysdwgjzdbh;
	
	/**
	 * 野生植物总数（株）
	 */
	@Column(name = "YSZWZS", nullable = true)
	private Double yszwzs;
	
	/**
	 * 野生植物国家重点保护对象（株）
	 */
	@Column(name = "YSZWGJZDBH", nullable = true)
	private Double yszwgjzdbh;
	
	
	/**
	 * 单位负责人
	 */
	@Column(name = "DEFZRMC", length = 500, nullable = true)
	private String defzrmc;
	
	
	/**
	 * 填表人
	 */
	@Column(name = "TBR", length = 500, nullable = true)
	private String tbr;
	
	/**
	 * 填表时间
	 */
	@Column(name = "TBSJ", nullable = true)
	private java.util.Date tbsj;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 填报单位名称
	 */
	public String getTbdwmc() {
		return tbdwmc;
	}

	/**
	 * 填报单位名称
	 */
	public void setTbdwmc(String value) {
		this.tbdwmc = value;
	}
	/**
	 * 填报单位ID
	 */
	public String getTbdwid() {
		return tbdwid;
	}

	/**
	 * 填报单位ID
	 */
	public void setTbdwid(String value) {
		this.tbdwid = value;
	}
	/**
	 * 统计期限
	 */
	public String getTjqx() {
		return tjqx;
	}

	/**
	 * 统计期限
	 */
	public void setTjqx(String value) {
		this.tjqx = value;
	}
	/**
	 * 统计日期
	 */
	public java.util.Date getTjrq() {
		return tjrq;
	}

	/**
	 * 统计日期
	 */
	public void setTjrq(java.util.Date value) {
		this.tjrq = value;
	}
	/**
	 * 案件类型
	 */
	public String getAjlx() {
		return ajlx;
	}

	/**
	 * 案件类型
	 */
	public void setAjlx(String value) {
		this.ajlx = value;
	}
	/**
	 * 统计有效期结束时间
	 */
	public java.util.Date getTjyxqjssj() {
		return tjyxqjssj;
	}

	/**
	 * 统计有效期结束时间
	 */
	public void setTjyxqjssj(java.util.Date value) {
		this.tjyxqjssj = value;
	}
	/**
	 * 案件数量总数
	 */
	public Double getAjslzs() {
		return ajslzs;
	}

	/**
	 * 案件数量总数
	 */
	public void setAjslzs(Double value) {
		this.ajslzs = value;
	}
	/**
	 * 案件数量_公民
	 */
	public Double getAjslgm() {
		return ajslgm;
	}

	/**
	 * 案件数量_公民
	 */
	public void setAjslgm(Double value) {
		this.ajslgm = value;
	}
	/**
	 * 案件数量_法人
	 */
	public Double getAjslfr() {
		return ajslfr;
	}

	/**
	 * 案件数量_法人
	 */
	public void setAjslfr(Double value) {
		this.ajslfr = value;
	}
	/**
	 * 案件数量其他组织
	 */
	public Double getAjslqt() {
		return ajslqt;
	}

	/**
	 * 案件数量其他组织
	 */
	public void setAjslqt(Double value) {
		this.ajslqt = value;
	}
	/**
	 * 查结案件总数
	 */
	public Double getCjajzs() {
		return cjajzs;
	}

	/**
	 * 查结案件总数
	 */
	public void setCjajzs(Double value) {
		this.cjajzs = value;
	}
	/**
	 * 查结案件结案率
	 */
	public Double getCjajjal() {
		return cjajjal;
	}

	/**
	 * 查结案件结案率
	 */
	public void setCjajjal(Double value) {
		this.cjajjal = value;
	}
	/**
	 * 林地面积（公顷）
	 */
	public Double getLmmj() {
		return lmmj;
	}

	/**
	 * 林地面积（公顷）
	 */
	public void setLmmj(Double value) {
		this.lmmj = value;
	}
	/**
	 * 林木蓄积（立方米）
	 */
	public Double getLmxj() {
		return lmxj;
	}

	/**
	 * 林木蓄积（立方米）
	 */
	public void setLmxj(Double value) {
		this.lmxj = value;
	}
	/**
	 * 竹子（万株）
	 */
	public Double getZjzs() {
		return zjzs;
	}

	/**
	 * 竹子（万株）
	 */
	public void setZjzs(Double value) {
		this.zjzs = value;
	}
	/**
	 * 种子（公顷）
	 */
	public Double getZhognzi() {
		return zhognzi;
	}

	/**
	 * 种子（公顷）
	 */
	public void setZhognzi(Double value) {
		this.zhognzi = value;
	}
	/**
	 * 幼树或苗木（万株）
	 */
	public Double getYmhmm() {
		return ymhmm;
	}

	/**
	 * 幼树或苗木（万株）
	 */
	public void setYmhmm(Double value) {
		this.ymhmm = value;
	}
	/**
	 * 沙地（公顷）
	 */
	public Double getShadi() {
		return shadi;
	}

	/**
	 * 沙地（公顷）
	 */
	public void setShadi(Double value) {
		this.shadi = value;
	}
	/**
	 * 保护区或栖息地（公顷）
	 */
	public Double getBhqhqxd() {
		return bhqhqxd;
	}

	/**
	 * 保护区或栖息地（公顷）
	 */
	public void setBhqhqxd(Double value) {
		this.bhqhqxd = value;
	}
	/**
	 * 野生动物总数（只）
	 */
	public Double getYsdwzs() {
		return ysdwzs;
	}

	/**
	 * 野生动物总数（只）
	 */
	public void setYsdwzs(Double value) {
		this.ysdwzs = value;
	}
	/**
	 * 野生动物国家重点保护对象（只）
	 */
	public Double getYsdwgjzdbh() {
		return ysdwgjzdbh;
	}

	/**
	 * 野生动物国家重点保护对象（只）
	 */
	public void setYsdwgjzdbh(Double value) {
		this.ysdwgjzdbh = value;
	}
	/**
	 * 野生植物总数（株）
	 */
	public Double getYszwzs() {
		return yszwzs;
	}

	/**
	 * 野生植物总数（株）
	 */
	public void setYszwzs(Double value) {
		this.yszwzs = value;
	}
	/**
	 * 野生植物国家重点保护对象（株）
	 */
	public Double getYszwgjzdbh() {
		return yszwgjzdbh;
	}

	/**
	 * 野生植物国家重点保护对象（株）
	 */
	public void setYszwgjzdbh(Double value) {
		this.yszwgjzdbh = value;
	}
	/**
	 * 单位负责人
	 */
	public String getDefzrmc() {
		return defzrmc;
	}

	/**
	 * 单位负责人
	 */
	public void setDefzrmc(String value) {
		this.defzrmc = value;
	}
	/**
	 * 填表人
	 */
	public String getTbr() {
		return tbr;
	}

	/**
	 * 填表人
	 */
	public void setTbr(String value) {
		this.tbr = value;
	}
	/**
	 * 填表时间
	 */
	public java.util.Date getTbsj() {
		return tbsj;
	}

	/**
	 * 填表时间
	 */
	public void setTbsj(java.util.Date value) {
		this.tbsj = value;
	}

	@Override
	public void from(Gzlytjaj01Entity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public Gzlytjaj01Entity toEntity() {
		Gzlytjaj01Entity toEntity = new Gzlytjaj01Entity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}