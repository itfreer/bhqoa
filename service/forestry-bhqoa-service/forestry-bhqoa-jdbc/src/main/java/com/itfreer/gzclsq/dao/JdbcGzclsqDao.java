package com.itfreer.gzclsq.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzclsq.dao.GzclsqDao;
import com.itfreer.gzclsq.entity.GzclsqEntity;
import com.itfreer.gzclsq.entity.JdbcGzclsqEntity;

/**
 * 定义车辆使用 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzclsqDao")
public class JdbcGzclsqDao extends JdbcBaseDaoImp<GzclsqEntity, JdbcGzclsqEntity> implements GzclsqDao {

}
