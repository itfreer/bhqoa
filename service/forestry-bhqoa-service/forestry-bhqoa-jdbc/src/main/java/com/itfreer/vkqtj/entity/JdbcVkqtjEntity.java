package com.itfreer.vkqtj.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.vkqtj.entity.VkqtjEntity;

/**
 * 定义考勤统计实体
 */
 @Entity(name = "V_KQTJ")
public class JdbcVkqtjEntity implements JdbcBaseEntity<VkqtjEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	
	/**
	 * id
	 */
	@Id
	@Column(name = "id", length = 100, nullable = true)
	private String id;
	
	/**
	 * 部门id
	 */
	@Column(name = "dptid", length = 100, nullable = true)
	private String dptid;
	
	public String getDptid() {
		return dptid;
	}

	public void setDptid(String dptid) {
		this.dptid = dptid;
	}

	public String getId() {
		return id;
	}

	/**
	 * 类型
	 */
	@Column(name = "TYPE", length = 100, nullable = true)
	private String type;
	
	
	/**
	 * 用户id
	 */
	@Column(name = "USERID", length = 100, nullable = true)
	private String userid;
	
	
	/**
	 * 用户姓名
	 */
	@Column(name = "USERNAME", length = 100, nullable = true)
	private String username;
	
	/**
	 * 时间
	 */
	@Column(name = "SJ", nullable = true)
	private java.util.Date sj;
	
	/**
	 * 开始时间
	 */
	@Column(name = "KSSJ", nullable = true)
	private java.util.Date kssj;
	
	/**
	 * 结束时间
	 */
	@Column(name = "JSSJ", nullable = true)
	private java.util.Date jssj;
	

	/**
	 * 类型
	 */
	public String getType() {
		return type;
	}

	/**
	 * 类型
	 */
	public void setType(String value) {
		this.type = value;
	}
	/**
	 * 用户id
	 */
	public String getUserid() {
		return userid;
	}

	/**
	 * 用户id
	 */
	public void setUserid(String value) {
		this.userid = value;
	}
	/**
	 * 用户姓名
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * 用户姓名
	 */
	public void setUsername(String value) {
		this.username = value;
	}
	/**
	 * 时间
	 */
	public java.util.Date getSj() {
		return sj;
	}

	/**
	 * 时间
	 */
	public void setSj(java.util.Date value) {
		this.sj = value;
	}
	/**
	 * 开始时间
	 */
	public java.util.Date getKssj() {
		return kssj;
	}

	/**
	 * 开始时间
	 */
	public void setKssj(java.util.Date value) {
		this.kssj = value;
	}
	/**
	 * 结束时间
	 */
	public java.util.Date getJssj() {
		return jssj;
	}

	/**
	 * 结束时间
	 */
	public void setJssj(java.util.Date value) {
		this.jssj = value;
	}

	@Override
	public void from(VkqtjEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public VkqtjEntity toEntity() {
		VkqtjEntity toEntity = new VkqtjEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}

	@Override
	public void setId(String arg0) {
		// TODO Auto-generated method stub
		
	}
}