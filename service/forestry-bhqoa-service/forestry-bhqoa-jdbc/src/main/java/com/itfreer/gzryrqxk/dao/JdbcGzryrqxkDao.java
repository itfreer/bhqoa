package com.itfreer.gzryrqxk.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzryrqxk.dao.GzryrqxkDao;
import com.itfreer.gzryrqxk.entity.GzryrqxkEntity;
import com.itfreer.gzryrqxk.entity.JdbcGzryrqxkEntity;

/**
 * 定义外籍人员入区许可、外来科研人员入区许可 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzryrqxkDao")
public class JdbcGzryrqxkDao extends JdbcBaseDaoImp<GzryrqxkEntity, JdbcGzryrqxkEntity> implements GzryrqxkDao {

}
