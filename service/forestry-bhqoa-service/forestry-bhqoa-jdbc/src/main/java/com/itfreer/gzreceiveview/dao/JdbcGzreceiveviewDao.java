package com.itfreer.gzreceiveview.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzreceiveview.dao.GzreceiveviewDao;
import com.itfreer.gzreceiveview.entity.GzreceiveviewEntity;
import com.itfreer.gzreceiveview.entity.JdbcGzreceiveviewEntity;

/**
 * 定义收文管理 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzreceiveviewDao")
public class JdbcGzreceiveviewDao extends JdbcBaseDaoImp<GzreceiveviewEntity, JdbcGzreceiveviewEntity> implements GzreceiveviewDao {

}
