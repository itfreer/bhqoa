package com.itfreer.gztaskview.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gztaskview.entity.GztaskviewEntity;

/**
 * 定义收文视图实体
 */
@Entity(name = "GZ_TASK_VIEW")
public class JdbcGztaskviewEntity implements JdbcBaseEntity<GztaskviewEntity>, Serializable {
	private static final long serialVersionUID = 4673423560339517162L;
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;

	/**
	 * 创建人部门名称
	 */
	@Column(name = "BMMC", length = 500, nullable = true)
	private String bmmc;

	/**
	 * 事项说明
	 */
	@Column(name = "SXSM", length = 500, nullable = true)
	private String sxsm;

	/**
	 * 创建人ID
	 */
	@Column(name = "CJRID", length = 500, nullable = true)
	private String cjrid;

	/**
	 * 创建人名称
	 */
	@Column(name = "CJRMC", length = 500, nullable = true)
	private String cjrmc;

	/**
	 * 创建时间
	 */
	@Column(name = "CJSJ", nullable = true)
	private java.util.Date cjsj;

	/**
	 * 项目名称
	 */
	@Column(name = "XMMC", length = 500, nullable = true)
	private String xmmc;

	/**
	 * 发步ID
	 */
	@Column(name = "FWID", length = 500, nullable = true)
	private String fwid;

	/**
	 * 接收人ID
	 */
	@Column(name = "JSRID", length = 500, nullable = true)
	private String jsrid;

	/**
	 * 接收人名称
	 */
	@Column(name = "JSRMC", length = 500, nullable = true)
	private String jsrmc;

	/**
	 * 接收人机构
	 */
	@Column(name = "JSRJG", length = 100, nullable = true)
	private String jsrjg;

	/**
	 * 主题
	 */
	@Column(name = "ZHUTI", length = 100, nullable = true)
	private String zhuti;

	public String getZhuti() {
		return zhuti;
	}

	public void setZhuti(String zhuti) {
		this.zhuti = zhuti;
	}

	/**
	 * 确认时间
	 */
	@Column(name = "QRSJ", nullable = true)
	private java.util.Date qrsj;

	/**
	 * 是否已确认
	 */
	@Column(name = "SFQR", length = 500, nullable = true)
	private String sfqr;

	/**
	 * 结束时间
	 */
	@Column(name = "JSSJ", nullable = true)
	private java.util.Date jssj;

	/**
	 * 开始时间
	 */
	@Column(name = "KSSJ", nullable = true)
	private java.util.Date kssj;

	/**
	 * 组织机构名称
	 */
	@Column(name = "ZZJGMC", length = 500, nullable = true)
	private String zzjgmc;

	/**
	 * 状态
	 */
	@Column(name = "ZT", length = 500, nullable = true)
	private String zt;

	/**
	 * 单位名称
	 */
	@Column(name = "DWMC", length = 500, nullable = true)
	private String dwmc;

	/**
	 * 负责人
	 */
	@Column(name = "FZR", length = 500, nullable = true)
	private String fzr;

	/**
	 * 负责人名称
	 */
	@Column(name = "FZRMC", length = 500, nullable = true)
	private String fzrmc;

	public String getFzrmc() {
		return fzrmc;
	}

	public void setFzrmc(String fzrmc) {
		this.fzrmc = fzrmc;
	}

	/**
	 * 完成时间
	 */
	@Column(name = "WCSJ", nullable = true)
	private java.util.Date wcsj;

	public java.util.Date getWcsj() {
		return wcsj;
	}

	public void setWcsj(java.util.Date wcsj) {
		this.wcsj = wcsj;
	}

	public String getDwmc() {
		return dwmc;
	}

	public void setDwmc(String dwmc) {
		this.dwmc = dwmc;
	}

	public String getFzr() {
		return fzr;
	}

	public void setFzr(String fzr) {
		this.fzr = fzr;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}

	/**
	 * 创建人部门名称
	 */
	public String getBmmc() {
		return bmmc;
	}

	/**
	 * 创建人部门名称
	 */
	public void setBmmc(String value) {
		this.bmmc = value;
	}

	/**
	 * 事项说明
	 */
	public String getSxsm() {
		return sxsm;
	}

	/**
	 * 事项说明
	 */
	public void setSxsm(String value) {
		this.sxsm = value;
	}

	/**
	 * 创建人ID
	 */
	public String getCjrid() {
		return cjrid;
	}

	/**
	 * 创建人ID
	 */
	public void setCjrid(String value) {
		this.cjrid = value;
	}

	/**
	 * 创建人名称
	 */
	public String getCjrmc() {
		return cjrmc;
	}

	/**
	 * 创建人名称
	 */
	public void setCjrmc(String value) {
		this.cjrmc = value;
	}

	/**
	 * 创建时间
	 */
	public java.util.Date getCjsj() {
		return cjsj;
	}

	/**
	 * 创建时间
	 */
	public void setCjsj(java.util.Date value) {
		this.cjsj = value;
	}

	/**
	 * 项目名称
	 */
	public String getXmmc() {
		return xmmc;
	}

	/**
	 * 项目名称
	 */
	public void setXmmc(String value) {
		this.xmmc = value;
	}

	/**
	 * 发步ID
	 */
	public String getFwid() {
		return fwid;
	}

	/**
	 * 发步ID
	 */
	public void setFwid(String value) {
		this.fwid = value;
	}

	/**
	 * 接收人ID
	 */
	public String getJsrid() {
		return jsrid;
	}

	/**
	 * 接收人ID
	 */
	public void setJsrid(String value) {
		this.jsrid = value;
	}

	/**
	 * 接收人名称
	 */
	public String getJsrmc() {
		return jsrmc;
	}

	/**
	 * 接收人名称
	 */
	public void setJsrmc(String value) {
		this.jsrmc = value;
	}

	/**
	 * 接收人机构
	 */
	public String getJsrjg() {
		return jsrjg;
	}

	/**
	 * 接收人机构
	 */
	public void setJsrjg(String value) {
		this.jsrjg = value;
	}

	/**
	 * 确认时间
	 */
	public java.util.Date getQrsj() {
		return qrsj;
	}

	/**
	 * 确认时间
	 */
	public void setQrsj(java.util.Date value) {
		this.qrsj = value;
	}

	/**
	 * 是否已确认
	 */
	public String getSfqr() {
		return sfqr;
	}

	/**
	 * 是否已确认
	 */
	public void setSfqr(String value) {
		this.sfqr = value;
	}

	/**
	 * 结束时间
	 */
	public java.util.Date getJssj() {
		return jssj;
	}

	/**
	 * 结束时间
	 */
	public void setJssj(java.util.Date value) {
		this.jssj = value;
	}

	/**
	 * 开始时间
	 */
	public java.util.Date getKssj() {
		return kssj;
	}

	/**
	 * 开始时间
	 */
	public void setKssj(java.util.Date value) {
		this.kssj = value;
	}

	/**
	 * 组织机构名称
	 */
	public String getZzjgmc() {
		return zzjgmc;
	}

	/**
	 * 组织机构名称
	 */
	public void setZzjgmc(String value) {
		this.zzjgmc = value;
	}

	/**
	 * 状态
	 */
	public String getZt() {
		return zt;
	}

	/**
	 * 状态
	 */
	public void setZt(String value) {
		this.zt = value;
	}

	@Override
	public void from(GztaskviewEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GztaskviewEntity toEntity() {
		GztaskviewEntity toEntity = new GztaskviewEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}