package com.itfreer.gztaskview.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gztaskview.dao.GztaskviewDao;
import com.itfreer.gztaskview.entity.GztaskviewEntity;
import com.itfreer.gztaskview.entity.JdbcGztaskviewEntity;

/**
 * 定义收文视图 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGztaskviewDao")
public class JdbcGztaskviewDao extends JdbcBaseDaoImp<GztaskviewEntity, JdbcGztaskviewEntity> implements GztaskviewDao {

}
