package com.itfreer.gzwlkyxmlfry.dao;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.JdbcBaseDaoImp;

import com.itfreer.gzwlkyxmlfry.dao.GzwlkyxmlfryDao;
import com.itfreer.gzwlkyxmlfry.entity.GzwlkyxmlfryEntity;
import com.itfreer.gzwlkyxmlfry.entity.JdbcGzwlkyxmlfryEntity;

/**
 * 定义外来科研项目人随访人员 jdbc实现类
 */
@Transactional(readOnly = false)
@Component("jdbcGzwlkyxmlfryDao")
public class JdbcGzwlkyxmlfryDao extends JdbcBaseDaoImp<GzwlkyxmlfryEntity, JdbcGzwlkyxmlfryEntity> implements GzwlkyxmlfryDao {

}
