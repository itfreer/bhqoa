package com.itfreer.gzqdgl.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzqdgl.entity.GzqdglEntity;

/**
 * 定义签到管理实体
 */
 @Entity(name = "GZ_QDGL")
public class JdbcGzqdglEntity implements JdbcBaseEntity<GzqdglEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	@Column(name = "latitude", length = 50, nullable = true)
	String latitude;
	@Column(name = "longitude", length = 50, nullable = true)
	String longitude;
	@Column(name = "address", length = 200, nullable = true)
	String address;
	@Column(name = "accuracy", length = 50, nullable = true)
	String accuracy;
	@Column(name = "dirtype", length = 50, nullable = true)
	String dirtype;
	
	@Column(name = "outlatitude", length = 50, nullable = true)
	String outlatitude;
	@Column(name = "outlongitude", length = 50, nullable = true)
	String outlongitude;
	@Column(name = "outaddress", length = 200, nullable = true)
	String outaddress;
	@Column(name = "outaccuracy", length = 50, nullable = true)
	String outaccuracy;
	@Column(name = "outdirtype", length = 50, nullable = true)
	String outdirtype;
	
	@Column(name = "xwlatitude", length = 50, nullable = true)
	String xwlatitude;
	@Column(name = "xwlongitude", length = 50, nullable = true)
	String xwlongitude;
	@Column(name = "xwaddress", length = 200, nullable = true)
	String xwaddress;
	@Column(name = "xwaccuracy", length = 50, nullable = true)
	String xwaccuracy;
	@Column(name = "xwdirtype", length = 50, nullable = true)
	String xwdirtype;
	
	@Column(name = "xwoutlatitude", length = 50, nullable = true)
	String xwoutlatitude;
	@Column(name = "xwoutlongitude", length = 50, nullable = true)
	String xwoutlongitude;
	@Column(name = "xwoutaddress", length = 200, nullable = true)
	String xwoutaddress;
	@Column(name = "xwoutaccuracy", length = 50, nullable = true)
	String xwoutaccuracy;
	@Column(name = "xwoutdirtype", length = 50, nullable = true)
	String xwoutdirtype;
	
	
	public String getXwlatitude() {
		return xwlatitude;
	}

	public void setXwlatitude(String xwlatitude) {
		this.xwlatitude = xwlatitude;
	}

	public String getXwlongitude() {
		return xwlongitude;
	}

	public void setXwlongitude(String xwlongitude) {
		this.xwlongitude = xwlongitude;
	}

	public String getXwaddress() {
		return xwaddress;
	}

	public void setXwaddress(String xwaddress) {
		this.xwaddress = xwaddress;
	}

	public String getXwaccuracy() {
		return xwaccuracy;
	}

	public void setXwaccuracy(String xwaccuracy) {
		this.xwaccuracy = xwaccuracy;
	}

	public String getXwdirtype() {
		return xwdirtype;
	}

	public void setXwdirtype(String xwdirtype) {
		this.xwdirtype = xwdirtype;
	}

	public String getXwoutlatitude() {
		return xwoutlatitude;
	}

	public void setXwoutlatitude(String xwoutlatitude) {
		this.xwoutlatitude = xwoutlatitude;
	}

	public String getXwoutlongitude() {
		return xwoutlongitude;
	}

	public void setXwoutlongitude(String xwoutlongitude) {
		this.xwoutlongitude = xwoutlongitude;
	}

	public String getXwoutaddress() {
		return xwoutaddress;
	}

	public void setXwoutaddress(String xwoutaddress) {
		this.xwoutaddress = xwoutaddress;
	}

	public String getXwoutaccuracy() {
		return xwoutaccuracy;
	}

	public void setXwoutaccuracy(String xwoutaccuracy) {
		this.xwoutaccuracy = xwoutaccuracy;
	}

	public String getXwoutdirtype() {
		return xwoutdirtype;
	}

	public void setXwoutdirtype(String xwoutdirtype) {
		this.xwoutdirtype = xwoutdirtype;
	}

	public String getOutlatitude() {
		return outlatitude;
	}

	public void setOutlatitude(String outlatitude) {
		this.outlatitude = outlatitude;
	}

	public String getOutlongitude() {
		return outlongitude;
	}

	public void setOutlongitude(String outlongitude) {
		this.outlongitude = outlongitude;
	}

	public String getOutaddress() {
		return outaddress;
	}

	public void setOutaddress(String outaddress) {
		this.outaddress = outaddress;
	}

	public String getOutaccuracy() {
		return outaccuracy;
	}

	public void setOutaccuracy(String outaccuracy) {
		this.outaccuracy = outaccuracy;
	}

	public String getOutdirtype() {
		return outdirtype;
	}

	public void setOutdirtype(String outdirtype) {
		this.outdirtype = outdirtype;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(String accuracy) {
		this.accuracy = accuracy;
	}

	public String getDirtype() {
		return dirtype;
	}

	public void setDirtype(String dirtype) {
		this.dirtype = dirtype;
	}
	
	/**
	 * 流程ID
	 */
	@Column(name = "BPMKEY", length = 50, nullable = true)
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	@Column(name = "ID", length = 50, nullable = true)
	private String id;
	
	/**
	 * 构建时间
	 */
	@Column(name = "createtime", nullable = true)
	private java.util.Date createtime;
	
	public java.util.Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(java.util.Date createtime) {
		this.createtime = createtime;
	}
	
	
	/**
	 * 所属单位
	 */
	@Column(name = "DANWEIMC", length = 500, nullable = true)
	private String danweimc;
	
	
	/**
	 * 单位ID
	 */
	@Column(name = "DANWEIID", length = 500, nullable = true)
	private String danweiid;
	
	@Column(name = "BZ", length = 500, nullable = true)
	private String bz;
	
	@Column(name = "DKBZ", length = 500, nullable = true)
	private String dkbz;
	
	public String getDkbz() {
		return dkbz;
	}

	public void setDkbz(String dkbz) {
		this.dkbz = dkbz;
	}
	
	@Column(name = "TXBZ", length = 500, nullable = true)
	private String txbz;
	
	public String getTxbz() {
		return txbz;
	}

	public void setTxbz(String txbz) {
		this.txbz = txbz;
	}
	
	@Column(name = "SFKQFW", length = 500, nullable = true)
	private String sfkqfw;
	
	@Column(name = "SFSJYC", length = 500, nullable = true)
	private String sfsjyc;
	
	public String getSfsjyc() {
		return sfsjyc;
	}

	public void setSfsjyc(String sfsjyc) {
		this.sfsjyc = sfsjyc;
	}
	
	public String getSfkqfw() {
		return sfkqfw;
	}

	public void setSfkqfw(String sfkqfw) {
		this.sfkqfw = sfkqfw;
	}

	public String getBz() {
		return bz;
	}

	public void setBz(String bz) {
		this.bz = bz;
	}

	/**
	 * 考勤人ID
	 */
	@Column(name = "KAOQRID", length = 500, nullable = true)
	private String kaoqrid;
	
	
	/**
	 * 考勤人名称
	 */
	@Column(name = "KAOQRMC", length = 500, nullable = true)
	private String kaoqrmc;
	
	/**
	 * 年度
	 */
	@Column(name = "NANDU", nullable = true)
	private Integer nandu;
	
	/**
	 * 月份
	 */
	@Column(name = "YUEFEN", nullable = true)
	private Integer yuefen;
	
	/**
	 * 日期
	 */
	@Column(name = "RIQI", nullable = true)
	private java.util.Date riqi;
	
	
	/**
	 * 星期
	 */
	@Column(name = "XINGQI", length = 500, nullable = true)
	private String xingqi;
	
	
	/**
	 * 工作日状态（工作日，节假日）
	 */
	@Column(name = "GZRZT", length = 500, nullable = true)
	private String gzrzt;
	
	/**
	 * 有效签入时间
	 */
	@Column(name = "YXQRSJ", nullable = true)
	private java.util.Date yxqrsj;
	
	/**
	 * 有效迁出时间
	 */
	@Column(name = "YXQCSJ", nullable = true)
	private java.util.Date yxqcsj;
	
	/**
	 * 签到时间
	 */
	@Column(name = "QDSJ", nullable = true)
	private java.util.Date qdsj;
	
	/**
	 * 签退时间
	 */
	@Column(name = "QTSJ", nullable = true)
	private java.util.Date qtsj;
	
	/**
	 * 工作时间
	 */
	@Column(name = "GZSJ", nullable = true)
	private Double gzsj;
	
	/**
	 * 下午签到时间
	 */
	@Column(name = "XWQDSJ", nullable = true)
	private java.util.Date xwqdsj;
	
	/**
	 * 下午签退时间
	 */
	@Column(name = "XWQTSJ", nullable = true)
	private java.util.Date xwqtsj;
	
	public java.util.Date getXwqdsj() {
		return xwqdsj;
	}

	public void setXwqdsj(java.util.Date xwqdsj) {
		this.xwqdsj = xwqdsj;
	}

	public java.util.Date getXwqtsj() {
		return xwqtsj;
	}

	public void setXwqtsj(java.util.Date xwqtsj) {
		this.xwqtsj = xwqtsj;
	}
	
	/**
	 * 迟到
	 */
	@Column(name = "CHIDAO", nullable = true)
	private Double chidao;
	
	/**
	 * 早退
	 */
	@Column(name = "ZAOTUI", nullable = true)
	private Double zaotui;
	
	/**
	 * 早来
	 */
	@Column(name = "ZAOLAI", nullable = true)
	private Double zaolai;
	
	/**
	 * 晚走
	 */
	@Column(name = "WANZOU", nullable = true)
	private Double wanzou;
	
	/**
	 * 未打卡次数
	 */
	@Column(name = "WDKCS", nullable = true)
	private Double wdkcs;
	
	/**
	 * 请假
	 */
	@Column(name = "QINGJIA", nullable = true)
	private Double qingjia;
	
	/**
	 * 外出
	 */
	@Column(name = "WAICHU", nullable = true)
	private Double waichu;
	
	/**
	 * 加班
	 */
	@Column(name = "JIABAN", nullable = true)
	private Double jiaban;
	
	/**
	 * 旷工
	 */
	@Column(name = "KUANGGONG", nullable = true)
	private Double kuanggong;
	
	/**
	 * 天
	 */
	@Column(name = "I_DAY", nullable = true)
	private Integer day;
	
	
	
	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 所属单位
	 */
	public String getDanweimc() {
		return danweimc;
	}

	/**
	 * 所属单位
	 */
	public void setDanweimc(String value) {
		this.danweimc = value;
	}
	/**
	 * 单位ID
	 */
	public String getDanweiid() {
		return danweiid;
	}

	/**
	 * 单位ID
	 */
	public void setDanweiid(String value) {
		this.danweiid = value;
	}
	/**
	 * 考勤人ID
	 */
	public String getKaoqrid() {
		return kaoqrid;
	}

	/**
	 * 考勤人ID
	 */
	public void setKaoqrid(String value) {
		this.kaoqrid = value;
	}
	/**
	 * 考勤人名称
	 */
	public String getKaoqrmc() {
		return kaoqrmc;
	}

	/**
	 * 考勤人名称
	 */
	public void setKaoqrmc(String value) {
		this.kaoqrmc = value;
	}
	/**
	 * 年度
	 */
	public Integer getNandu() {
		return nandu;
	}

	/**
	 * 年度
	 */
	public void setNandu(Integer value) {
		this.nandu = value;
	}
	/**
	 * 月份
	 */
	public Integer getYuefen() {
		return yuefen;
	}

	/**
	 * 月份
	 */
	public void setYuefen(Integer value) {
		this.yuefen = value;
	}
	/**
	 * 日期
	 */
	public java.util.Date getRiqi() {
		return riqi;
	}

	/**
	 * 日期
	 */
	public void setRiqi(java.util.Date value) {
		this.riqi = value;
	}
	/**
	 * 星期
	 */
	public String getXingqi() {
		return xingqi;
	}

	/**
	 * 星期
	 */
	public void setXingqi(String value) {
		this.xingqi = value;
	}
	/**
	 * 工作日状态（工作日，节假日）
	 */
	public String getGzrzt() {
		return gzrzt;
	}

	/**
	 * 工作日状态（工作日，节假日）
	 */
	public void setGzrzt(String value) {
		this.gzrzt = value;
	}
	/**
	 * 有效签入时间
	 */
	public java.util.Date getYxqrsj() {
		return yxqrsj;
	}

	/**
	 * 有效签入时间
	 */
	public void setYxqrsj(java.util.Date value) {
		this.yxqrsj = value;
	}
	/**
	 * 有效迁出时间
	 */
	public java.util.Date getYxqcsj() {
		return yxqcsj;
	}

	/**
	 * 有效迁出时间
	 */
	public void setYxqcsj(java.util.Date value) {
		this.yxqcsj = value;
	}
	/**
	 * 签到时间
	 */
	public java.util.Date getQdsj() {
		return qdsj;
	}

	/**
	 * 签到时间
	 */
	public void setQdsj(java.util.Date value) {
		this.qdsj = value;
	}
	/**
	 * 签退时间
	 */
	public java.util.Date getQtsj() {
		return qtsj;
	}

	/**
	 * 签退时间
	 */
	public void setQtsj(java.util.Date value) {
		this.qtsj = value;
	}
	/**
	 * 工作时间
	 */
	public Double getGzsj() {
		return gzsj;
	}

	/**
	 * 工作时间
	 */
	public void setGzsj(Double value) {
		this.gzsj = value;
	}
	/**
	 * 迟到
	 */
	public Double getChidao() {
		return chidao;
	}

	/**
	 * 迟到
	 */
	public void setChidao(Double value) {
		this.chidao = value;
	}
	/**
	 * 早退
	 */
	public Double getZaotui() {
		return zaotui;
	}

	/**
	 * 早退
	 */
	public void setZaotui(Double value) {
		this.zaotui = value;
	}
	/**
	 * 早来
	 */
	public Double getZaolai() {
		return zaolai;
	}

	/**
	 * 早来
	 */
	public void setZaolai(Double value) {
		this.zaolai = value;
	}
	/**
	 * 晚走
	 */
	public Double getWanzou() {
		return wanzou;
	}

	/**
	 * 晚走
	 */
	public void setWanzou(Double value) {
		this.wanzou = value;
	}
	/**
	 * 未打卡次数
	 */
	public Double getWdkcs() {
		return wdkcs;
	}

	/**
	 * 未打卡次数
	 */
	public void setWdkcs(Double value) {
		this.wdkcs = value;
	}
	/**
	 * 请假
	 */
	public Double getQingjia() {
		return qingjia;
	}

	/**
	 * 请假
	 */
	public void setQingjia(Double value) {
		this.qingjia = value;
	}
	/**
	 * 外出
	 */
	public Double getWaichu() {
		return waichu;
	}

	/**
	 * 外出
	 */
	public void setWaichu(Double value) {
		this.waichu = value;
	}
	/**
	 * 加班
	 */
	public Double getJiaban() {
		return jiaban;
	}

	/**
	 * 加班
	 */
	public void setJiaban(Double value) {
		this.jiaban = value;
	}
	/**
	 * 旷工
	 */
	public Double getKuanggong() {
		return kuanggong;
	}

	/**
	 * 旷工
	 */
	public void setKuanggong(Double value) {
		this.kuanggong = value;
	}

	@Override
	public void from(GzqdglEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzqdglEntity toEntity() {
		GzqdglEntity toEntity = new GzqdglEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}
}