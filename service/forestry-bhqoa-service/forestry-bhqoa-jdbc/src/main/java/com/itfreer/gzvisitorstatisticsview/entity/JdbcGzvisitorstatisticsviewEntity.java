package com.itfreer.gzvisitorstatisticsview.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.beans.BeanUtils;
import com.itfreer.form.api.JdbcBaseEntity;
import com.itfreer.gzvisitorstatisticsview.entity.GzvisitorstatisticsviewEntity;

/**
 * 定义访客统计（视图）实体
 */
 @Entity(name = "GZ_VISITOR_STATISTICS_VIEW")
public class JdbcGzvisitorstatisticsviewEntity implements JdbcBaseEntity<GzvisitorstatisticsviewEntity>, Serializable  {
	private static final long serialVersionUID = 4673423560339517162L;
	
	/**
	 * 接待部门
	 */
	@Column(name = "JDBMMC", length = 500, nullable = true)
	private String jdbmmc;
	
	
	/**
	 * 来访总人数
	 */
	@Column(name = "LFZRS", length = 20, nullable = true)
	private String lfzrs;
	
	/**
	 * 年份
	 */
	@Id
	@Column(name = "NF", nullable = true)
	private String nf;
	

	/**
	 * 接待部门
	 */
	public String getJdbmmc() {
		return jdbmmc;
	}

	/**
	 * 接待部门
	 */
	public void setJdbmmc(String value) {
		this.jdbmmc = value;
	}
	/**
	 * 来访总人数
	 */
	public String getLfzrs() {
		return lfzrs;
	}

	/**
	 * 来访总人数
	 */
	public void setLfzrs(String value) {
		this.lfzrs = value;
	}
	/**
	 * 年份
	 */
	public String getNf() {
		return nf;
	}

	/**
	 * 年份
	 */
	public void setNf(String value) {
		this.nf = value;
	}

	@Override
	public void from(GzvisitorstatisticsviewEntity t) {
		BeanUtils.copyProperties(t, this);
	}

	@Override
	public GzvisitorstatisticsviewEntity toEntity() {
		GzvisitorstatisticsviewEntity toEntity = new GzvisitorstatisticsviewEntity();
		BeanUtils.copyProperties(this, toEntity);
		return toEntity;
	}

	@Override
	public void setId(String arg0) {
		// TODO Auto-generated method stub
		
	}
}