
create or replace view gz_receive_view AS select r.*,p.FWZT,p.FWSM,p.CJRID,p.CJRMC,p.CJSJ,p.FJ,p.BMID,p.BMMC
 from GZ_RECEIVE_FILE r LEFT JOIN GZ_PUBLISH_FILE p on r.FWID=p.ID ;


create or replace view  gz_sqxmtj_view AS select year(D_SQSJ) as nf,month(D_SQSJ) as yf,S_XMZT,count(id) xmgs,sum(S_SQZJ) sqzzj,(S_CSXY) cszxy from gz_sqxm 
GROUP BY year(D_SQSJ),month(D_SQSJ),S_XMZT ;


create or replace view  gz_task_view AS select r.*,p.BMMC,p.JSSJ,p.CJRID,p.CJRMC,p.CJSJ,p.KSSJ,p.SXSM,p.XMMC,p.ZZJGMC,p.ZT,p.DWMC,p.FZR,p.FZRMC 
 from gz_task r LEFT JOIN gz_schedule p on r.FWID=p.ID ;

create or replace view gz_visitor_statistics_view AS select year(dfsj) as nf,jdbmmc,sum(lfrs) lfzrs from gz_visitor_reception GROUP BY year(dfsj),jdbmmc ;


create or replace  VIEW v_sso_user_info AS select u.ID AS ID,u.BIRTH AS BIRTH,u.DEGREE AS DEGREE,u.DEPARTMENT_ID AS DEPARTMENT_ID,u.EMAIL AS EMAIL,u.ENTER_TIME AS ENTER_TIME,u.GRADUATE_SCHOOL AS GRADUATE_SCHOOL,u.LOCAL AS LOCAL,u.NATION AS NATION,u.OPERATING_POST_ID AS OPERATING_POST_ID,u.ORGANIZATION_ID AS ORGANIZATION_ID,u.OTHER_ATTRIBUTES_JSON AS OTHER_ATTRIBUTES_JSON,u.PHOTO AS PHOTO,u.POSITION_ID AS POSITION_ID,u.SEX AS SEX,u.SHENG AS SHENG,u.SHI AS SHI,u.TEL AS TEL,u.USER_NAME AS USER_NAME,u.WORKING_GROUP_ID AS WORKING_GROUP_ID,u.XIAN AS XIAN,u.job_statu AS job_statu,u.social_credit_code AS social_credit_code,u.isIdCheck AS isIdCheck,a.s_accounts AS s_accounts,a.is_valid AS is_valid,(select group_concat(r.role_id separator ',') from sso_user_role r where (r.user_id = u.ID)) AS roles from (sso_user_info u left join sso_user_uthentication a on((u.ID = a.id))) ;


create or replace  VIEW v_sso_user_role AS select u.ID AS id,u.USER_NAME AS user_name,u.ORGANIZATION_ID AS organization_id,u.DEPARTMENT_ID AS department_id,u.OPERATING_POST_ID AS operating_post_id,a.s_accounts AS s_accounts,a.is_valid AS is_valid,(select group_concat(r.role_id separator ',') from sso_user_role r where (r.user_id = u.ID)) AS roles from (sso_user_info u left join sso_user_uthentication a on((u.ID = a.id))) ;

