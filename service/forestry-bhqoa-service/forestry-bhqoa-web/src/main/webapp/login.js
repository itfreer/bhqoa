(function(window, angular, undefined){
	/**
	 * 定义核心模块
	 */
	angular.module('loginApp', [])
	.factory('$login', function($rootScope) {
		$login = {

			/**
			* 登录对象
			*/
			loginObject : { isShowLogin:true, Msg:'' },

			/**
			* 初始化
			*/
			initC : function(){
				// 是否有TGT
				var tgt = $.cookie('SSO_TGT');
				var service = $login.getUrlParam('service');
				var username = $login.getUrlParam('username');
				var password = $login.getUrlParam('password');

				// 是否有应用服务
				if(service && service!=""){
					//账号解密处理
					$.ajax({
						url : './rest/userPasswordEncoder/decode?username='+encodeURIComponent(username)+'&password='+encodeURIComponent(password),
						type : 'POST',
						data : null,
						timeout : 35000,
						dataType : 'json',
						success : function(data){
							if(data.username){
								username = data.username;
							}
							if(data.password){
								password = data.password;
							}
							// 已登录
							if(tgt && tgt!=""){
								// 验证登录与服务票据
								$login.serviceTicketsAjax(tgt, service,
									function(data, textStatus, xOptions){
										// 成功，转跳到应用服务
										$(location).prop('href', service+'?ticket='+data.ok);
									}, 
									function(xhr, status, data){
										// 失败，模似登录
										$login.mklogin(username,password,service);
									}
								);
							}else{
								// 没登录，模似登录
								$login.mklogin(username,password,service);
							}
						},
						error : function(data){
							console.log("123");
						}
					});
					// 已登录
//					if(tgt && tgt!=""){
//						// 验证登录与服务票据
//						$login.serviceTicketsAjax(tgt, service,
//							function(data, textStatus, xOptions){
//								// 成功，转跳到应用服务
//								$(location).prop('href', service+'?ticket='+data.ok);
//							}, 
//							function(xhr, status, data){
//								// 失败，模似登录
//								$login.mklogin(username,password,service);
//							}
//						);
//					}else{
//						// 没登录，模似登录
//						$login.mklogin(username,password,service);
//					}
				}else{
					// 没有应用服务，直接转到登录界面
					$(location).prop('href', 'login.html');
				}
			},

			// 模似登录
			mklogin : function(username,password,service){
				// 如果验证失败，并且传了用户名和密码，则模拟登录
				if(username && password){
					$login.loginAjax(username, password, 
					function(data, textStatus, xOptions){
						// 设置cookie
						$.cookie('SSO_TGT', data.ok, { expires: 30 });
						// 创建服务票据
						$login.serviceTicketsAjax(data.ok, service,
							function(data, textStatus, xOptions){
								// 转跳到应用服务
								$(location).prop('href', service+'?ticket='+data.ok);
							}, 
							function(xhr, status, data){
								// 转到登录界面
								$(location).prop('href', 'login.html?service='+service);
							}
						);
					}, 
					function(xhr, status, data){
						// 转到登录界面
						$(location).prop('href', 'login.html?service='+service);
					});
				}else{
					// 转到登录界面
					$(location).prop('href', 'login.html?service='+service);
				}
			},

			/**
			* 初始化
			*/
			init : function(){
				$login.loginObject.isShowLogin = true;

				// 是否有TGT
				var tgt = $.cookie('SSO_TGT');
				if(tgt && tgt!=""){
					// 是否其它应用来验证
					$login.loginObject.isShowLogin = false;
					$login.loginObject.Msg = '验证中...';

					var service = $login.getUrlParam('service');
					if(service && service!=""){
						// 创建服务票据
						$login.serviceTicketsAjax(tgt, service,
							function(data, textStatus, xOptions){
								$rootScope.$apply(function() {
									$login.loginObject.Msg = '登录成功';
								});
								// 转跳到应用服务
								$(location).prop('href', service+'?ticket='+data.ok);
							}, 
							function(xhr, status, data){
								var msg = '';
								if(xhr.status<10){
									msg = '登录失败，请确认网络与认证服务是否正常';
								}else{
									msg = '登录已过期，请重新登录！';
								}
								$rootScope.$apply(function() {
									$login.loginObject.Msg = msg;
									$login.loginObject.isShowLogin = true;
								});
							}
						);
					}else{
						// 验证TGT
						$login.ticketsValidateAjax(tgt,
							function(data, textStatus, xOptions){
								$rootScope.$apply(function() {
									$login.loginObject.Msg = '登录成功';
								});
							}, 
							function(xhr, status, data){
								var msg = '';
								if(xhr.status<10){
									msg = '登录失败，请确认网络与认证服务是否正常';
								}else{
									msg = '登录已过期，请重新登录！';
								}
								$rootScope.$apply(function() {
									$login.loginObject.isShowLogin = true;
									$login.loginObject.Msg = msg;
								});
							}
						);
					}
				}
			},

			/**
			* 系统登录
			*/
			login : function(){
				if(!$login.loginObject.username || $login.loginObject.username==null || $login.loginObject.username==""){
					$login.loginObject.Msg = '用户名不能为空';
					return;
				}

				if(!$login.loginObject.password || $login.loginObject.password==null || $login.loginObject.password==""){
					$login.loginObject.Msg = '密码不能为空！';
					return;
				}

				$login.loginObject.isShowLogin = false;
				$login.loginObject.Msg = '登录中...';

				$login.loginAjax(
					$login.loginObject.username, 
					$login.loginObject.password, 
					function(data, textStatus, xOptions){
						// 设置cookie
						$.cookie('SSO_TGT', data.ok, { expires: 30 });
						
						var service = $login.getUrlParam('service');
						if(service && service!=""){
							$rootScope.$apply(function() {
								$login.loginObject.Msg = '登录成功，请稍后...';
							});
							
							// 创建服务票据
							$login.serviceTicketsAjax(data.ok, service,
								function(data, textStatus, xOptions){
									// 转跳到应用服务
									$(location).prop('href', service+'?ticket='+data.ok);
								}, 
								function(xhr, status, data){
									var msg = '';
									if(xhr.status<10){
										msg = '登录失败，请确认网络与认证服务是否正常';
									}else{
										msg = '登录已过期，请重新登录！';
									}
									$rootScope.$apply(function() {
										$login.loginObject.Msg = msg;
									});
								}
							);
						}else{
							$rootScope.$apply(function() {
								$login.loginObject.Msg = '登录成功';
							});
						}
					}, 
					function(xhr, status, data){
						var msg = '';
						if(xhr.status<10){
							msg = '登录失败，请确认网络与认证服务是否正常';
						}else{
							msg = '登录失败，请确认用户名和密码输入无误。';
						}
						$rootScope.$apply(function() {
							$login.loginObject.isShowLogin = true;
							$login.loginObject.Msg = msg;
						});
					}
				);
			},

			/**
			* 系统退出
			*/
			logout : function(){
				var tgt = $.cookie('SSO_TGT');
				if(tgt && tgt!=""){
					// 删除TGT
					$login.deleteTicketsAjax(tgt,
						function(data, textStatus, xOptions){
							$.cookie('SSO_TGT', '', { expires: -1 });
							$rootScope.$apply(function() {
								$login.loginObject.isShowLogin = true;
								$login.loginObject.Msg = '';
							});
						}, 
						function(xhr, status, data){
							var msg = '';
							if(xhr.status<10){
								msg = '系统退出失败，请确认网络与认证服务是否正常';
							}else{
								msg = '系统退出失败。';
							}
							$rootScope.$apply(function() {
								$login.loginObject.Msg = msg;
							});
						}
					);
				}else{
					$.cookie('SSO_TGT', '', { expires: -1 });
					$login.loginObject.isShowLogin = true;
					$login.loginObject.Msg = '';
				}
			},

			/**
			 * 系统登录
			 */
			loginAjax : function(username, password, onsuccess, onerror) {
				$.ajax({
					url : './rest/v1/tickets?username='+username+'&password='+password,
					type : 'POST',
					data : null,
					timeout : 35000,
					dataType : 'json',
					success : onsuccess,
					error : onerror
				});
			},

			/**
			 * TGT验证
			 */
			ticketsValidateAjax : function(tgt, onsuccess, onerror) {
				$.ajax({
					url : './rest/ticketsValidate/'+tgt,
					type : 'POST',
					data : null,
					timeout : 35000,
					dataType : 'json',
					success : onsuccess,
					error : onerror
				});
			},

			/**
			 * 创建ST
			 */
			serviceTicketsAjax : function(tgt, service, onsuccess, onerror) {
				$.ajax({
					url : './rest/v1/serviceTickets/'+tgt+'?service='+service,
					type : 'POST',
					data : null,
					timeout : 35000,
					dataType : 'json',
					success : onsuccess,
					error : onerror
				});
			},

			/**
			 * 删除TGT
			 */
			deleteTicketsAjax : function(tgt, onsuccess, onerror){
				$.ajax({
					url : './rest/v1/tickets/'+tgt,
					type : 'DELETE',
					data : null,
					timeout : 35000,
					dataType : 'json',
					success : onsuccess,
					error : onerror
				});
			},

			getUrlParam : function (name) {
                var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
                var r = window.location.search.substr(1).match(reg);
                if (r != null) return unescape(r[2]); return null;
            }
		};
		for(f in $login){
			$rootScope[f] = $login[f];
		}
		return $login;
	})
	.controller('loginCtrl', ['$rootScope', '$scope', '$login',
		function($rootScope, $scope, $login){

		}
	])
	.run(function($rootScope, $login) {
	});
}(window, angular));