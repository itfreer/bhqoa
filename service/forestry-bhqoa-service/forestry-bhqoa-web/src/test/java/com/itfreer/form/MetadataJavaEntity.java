package com.itfreer.form;

import com.itfreer.form.entity.metadata.MetadataEntity;

/**
 * 数据导出构建多余的字段问题
 * @author Administrator
 *
 */
public class MetadataJavaEntity extends MetadataEntity {
	
	private static final long serialVersionUID = -705886199074018420L;
	
	private String fieldName=null;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
}
