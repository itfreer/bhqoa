package com.itfreer.form;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.itfreer.form.entity.metadata.MetadataEntity;
import com.itfreer.form.service.metadata.MetadataService;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/applicationContext.xml" })
public class JavaWebGenerateTest {

	@Autowired
	private MetadataService metadataService;

	@SuppressWarnings("unchecked")
	@Test
	public void test() {
		List<Map<String, Object>> dataMap = new ArrayList<Map<String, Object>>();
		String outPath = "D://make-projects//javaEntity/";
		// String htmlOutPath = "D://make-projects//html/";
		// String outPath =
		// "F://workProjects//project-git//bhqoa//service//forestry-bhqoa-service/";
		String htmlOutPath = "F://workProjects//project-git//bhqoa//web/";
		Map<String, Object> item = new HashMap<String, Object>();
		item.put("projectName", "forestry-bhqoa");

		String rootTableName = "GZ_GZR_QDSJ";
		String aliaName = "签到时间";
		String rootFormName = rootTableName.toLowerCase().replace("_", "");
		String rootClassName = "G" + rootFormName.substring(1, rootFormName.length());
		item.put("model", "gzr.qdsj");

		item.put("jarName", "com.itfreer." + rootFormName);
		item.put("className", rootClassName);

		item.put("tableName", rootTableName);
		item.put("matename", rootTableName);

		item.put("formName", rootFormName);
		item.put("formAliseName", aliaName);
		item.put("classAliseName", aliaName);

		item.put("keyFiled", "id");
		item.put("order", "{id:0}");
		item.put("searchTabField", null);
		item.put("searchTabs", null);

		List<Map<String, String>> searchTabss = new ArrayList<Map<String, String>>();
		Map<String, String> a = new HashMap<String, String>();
		a.put("value", "01");
		a.put("text", "未开始");
		searchTabss.add(a);
		a = new HashMap<String, String>();
		a.put("value", "02");
		a.put("text", "进行中");
		searchTabss.add(a);
		a = new HashMap<String, String>();
		a.put("value", "03");
		a.put("text", "已完成");
		searchTabss.add(a);
		item.put("searchTabs", searchTabss);

		dataMap.add(item);

		/*
		 * item = new HashMap<String, String>(); item.put("matename", "002");
		 * item.put("jarName", "com.itfreer.gis.entity"); item.put("className",
		 * "GisTask"); item.put("classAliseName", "任务"); item.put("tableName",
		 * "gis_task"); dataMap.add(item);
		 * 
		 * item = new HashMap<String, String>(); item.put("matename", "003");
		 * item.put("jarName", "com.itfreer.gis.entity"); item.put("className",
		 * "GisNotice"); item.put("classAliseName", "通知"); item.put("tableName",
		 * "gis_notice"); dataMap.add(item);
		 * 
		 * item = new HashMap<String, String>(); item.put("matename", "004");
		 * item.put("jarName", "com.itfreer.gis.entity"); item.put("className",
		 * "GisDataImport"); item.put("classAliseName", "数据导入记录"); item.put("tableName",
		 * "gis_data_import"); dataMap.add(item);
		 * 
		 * item = new HashMap<String, String>(); item.put("matename", "005");
		 * item.put("jarName", "com.itfreer.gis.entity"); item.put("className",
		 * "GisBaseMap"); item.put("classAliseName", "离线底图"); item.put("tableName",
		 * "gis_base_map"); dataMap.add(item);
		 * 
		 * item = new HashMap<String, String>(); item.put("matename", "006");
		 * item.put("jarName", "com.itfreer.gis.entity"); item.put("className",
		 * "GisDataSyn"); item.put("classAliseName", "工程同步记录"); item.put("tableName",
		 * "gis_data_syn"); dataMap.add(item);
		 */

		for (Map<String, Object> map : dataMap) {
			String matename = map.get("matename").toString();
			String jarName = map.get("jarName").toString();
			String className = map.get("className").toString();
			String classAliseName = map.get("classAliseName").toString();
			String tableName = map.get("tableName").toString();

			Map<String, Object> data = new HashMap<String, Object>();
			// 包名称
			data.put("JarName", jarName);
			// 类名称
			data.put("ClassName", className);
			// 类别名称
			data.put("ClassAliseName", classAliseName);
			// 表名称
			data.put("TableName", tableName);

			String formName = (String) map.get("formName");
			String formAliseName = (String) map.get("formAliseName");
			String keyFiled = (String) map.get("keyFiled");
			String order = (String) map.get("order");
			String searchTabField = (String) map.get("searchTabField");
			List<Map<String, String>> searchTabs = (List<Map<String, String>>) map.get("searchTabs");

			// 表单名称
			data.put("FormName", formName);
			// 表单别名
			data.put("FormAliseName", formAliseName);
			// 主健
			data.put("KeyFiled", keyFiled);
			// 排序
			data.put("order", order);
			// 查询选项卡
			data.put("searchTabs", searchTabs);
			data.put("searchTabField", searchTabField);

			// 字段信息
			List<MetadataEntity> list = metadataService.getEntitys(matename);
			List<MetadataJavaEntity> nlist = new ArrayList<>();
			if (list != null && list.size() > 0) {
				for (MetadataEntity imet : list) {
					MetadataJavaEntity nMet = new MetadataJavaEntity();
					BeanUtils.copyProperties(imet, nMet);
					nMet.setFieldName(imet.getName());
					nMet.setName(imet.getName().toLowerCase().replace("_", ""));

					if (nMet.getDictionaryName() != null && "".equals(nMet.getDictionaryName().trim())) {

						nMet.setDictionaryName(null);

					}

					nlist.add(nMet);
					// imet.setName(imet.getName().toLowerCase().replace("_", ""));
				}
			}
			data.put("tableFields", nlist);
			String projectPath = outPath + map.get("projectName");
			File projectDir = new File(projectPath);
			if (!projectDir.exists()) {
				projectDir.mkdirs();
			}
			String projectName = map.get("projectName") + "";

			// share项目包处理
			createShare(jarName, className, outPath, data, projectPath, projectName);

			// core项目处理
			createCore(jarName, className, outPath, data, projectPath, projectName);
			// 构建jdbc
			createJdbc(jarName, className, data, projectPath, projectName);

			// 构建rest服务
			createRest(jarName, className, data, projectPath, projectName);

			// 构建html表单页面
			String model = map.get("model").toString();
			// g("web/simpleform.html", htmlOutPath, formName + ".html", data);
			createHtml("web/simpleform.html", htmlOutPath, model, formName + ".html", data);
		}
	}

	/**
	 * 构建html页面代码
	 * 
	 * @param templetePath
	 * @param htmlOutPath
	 * @param model
	 * @param fileName
	 * @param data
	 */
	private void createHtml(String templetePath, String htmlOutPath, String model, String fileName,
			Map<String, Object> data) {
		String path = htmlOutPath + "module";
		File rootPath = new File(path);
		if (!rootPath.exists())
			rootPath.mkdirs();

		if (model != null && !"".equals(model)) {
			String[] models = model.split("\\.");
			for (String imodel : models) {
				if (imodel != null && !"".equals(imodel)) {
					path = path + "/" + imodel;

					File ifile = new File(path);
					if (!ifile.exists())
						ifile.mkdirs();
				}
			}
		}
		g(templetePath, path + "/", fileName, data);
	}

	/**
	 * 构建rest项目包
	 * 
	 * @param jarName
	 * @param className
	 * @param data
	 * @param projectPath
	 * @param projectName
	 */
	private void createRest(String jarName, String className, Map<String, Object> data, String projectPath,
			String projectName) {
		String restdir = createSourceDir(projectPath, projectName + "-rest", jarName);
		String serviceDirStr = restdir + "service";
		File serviceDir = new File(serviceDirStr);
		if (!serviceDir.exists()) {
			serviceDir.mkdirs();
		}
		serviceDirStr = serviceDirStr + "/";
		g("java/restservice.html", serviceDirStr, "Rest" + className + "Service.java", data);
	}

	/**
	 * 构建jdbc项目
	 * 
	 * @param jarName
	 * @param className
	 * @param data
	 * @param projectPath
	 * @param projectName
	 */
	private void createJdbc(String jarName, String className, Map<String, Object> data, String projectPath,
			String projectName) {
		String jdbcdir = createSourceDir(projectPath, projectName + "-jdbc", jarName);
		String entityDirStr = jdbcdir + "entity";
		File entityDir = new File(entityDirStr);
		if (!entityDir.exists()) {
			entityDir.mkdirs();
		}
		entityDirStr = entityDirStr + "/";
		g("java/jdbcentity.html", entityDirStr, "Jdbc" + className + "Entity.java", data);

		String daoDirStr = jdbcdir + "dao";
		File daoDir = new File(daoDirStr);
		if (!daoDir.exists()) {
			daoDir.mkdirs();
		}
		daoDirStr = daoDirStr + "/";
		g("java/jdbcdao.html", daoDirStr, "Jdbc" + className + "Dao.java", data);
	}

	/**
	 * 构建core包
	 * 
	 * @param jarName
	 * @param className
	 * @param outPath
	 * @param data
	 * @param projectPath
	 * @param projectName
	 */
	private void createCore(String jarName, String className, String outPath, Map<String, Object> data,
			String projectPath, String projectName) {
		String coredir = createSourceDir(projectPath, projectName + "-core", jarName);
		String daoDirStr = coredir + "dao";
		File daoDir = new File(daoDirStr);
		if (!daoDir.exists()) {
			daoDir.mkdirs();
		}
		daoDirStr = daoDirStr + "/";
		g("java/dao.html", daoDirStr, className + "Dao.java", data);

		String serviceDirStr = coredir + "service";
		File serviceDir = new File(serviceDirStr);
		if (!serviceDir.exists()) {
			serviceDir.mkdirs();
		}
		serviceDirStr = serviceDirStr + "/";
		g("java/serviceimp.html", serviceDirStr, className + "ServiceImp.java", data);
	}

	/**
	 * 构建share目录
	 * 
	 * @param jarName
	 * @param className
	 * @param outPath
	 * @param data
	 * @param projectPath
	 * @param projectName
	 */
	private void createShare(String jarName, String className, String outPath, Map<String, Object> data,
			String projectPath, String projectName) {
		String sharedir = createSourceDir(projectPath, projectName + "-share", jarName);
		String entityDirStr = sharedir + "entity";
		File entityDir = new File(entityDirStr);
		if (!entityDir.exists()) {
			entityDir.mkdirs();
		}
		entityDirStr = entityDirStr + "/";
		g("java/entity.html", entityDirStr, className + "Entity.java", data);

		String serviceDirStr = sharedir + "service";
		File serviceDir = new File(serviceDirStr);
		if (!serviceDir.exists()) {
			serviceDir.mkdirs();
		}
		serviceDirStr = serviceDirStr + "/";
		g("java/service.html", serviceDirStr, className + "Service.java", data);
	}

	/**
	 * 构建项目资源路径
	 * 
	 * @param root
	 */
	private static String createSourceDir(String root, String sourceName, String packageName) {

		String source = root + "/" + sourceName;
		File sourceFile = new File(source);
		if (!sourceFile.exists()) {
			sourceFile.mkdirs();
		}

		String src = source + "/src";
		File srcFile = new File(src);
		if (!srcFile.exists()) {
			srcFile.mkdirs();
		}

		String main = src + "/main";
		File mainFile = new File(main);
		if (!mainFile.exists()) {
			mainFile.mkdirs();
		}

		String java = main + "/java";
		File javaFile = new File(java);
		if (!javaFile.exists()) {
			javaFile.mkdirs();
		}

		String cdir = java;
		String[] packageDir = packageName.split("\\.");
		for (String idir : packageDir) {
			cdir = cdir + "/" + idir;
			File cdirFile = new File(cdir);
			if (!cdirFile.exists()) {
				cdirFile.mkdirs();
			}
		}
		return cdir + "/";
	}

	@SuppressWarnings("deprecation")
	private void g(String templateFile, String outPath, String outFileName, Map<String, Object> data) {
		String tempPath = this.getClass().getResource("/").getPath() + "flt";

		ByteArrayOutputStream htmlOut = new ByteArrayOutputStream();
		OutputStreamWriter htmlWriter = new OutputStreamWriter(htmlOut, Charset.forName("UTF-8"));

		try {
			Configuration freeMarkerconfig = new Configuration();
			freeMarkerconfig.setDirectoryForTemplateLoading(new File(tempPath));
			freeMarkerconfig.setDefaultEncoding("UTF-8");
			Template freeMarkerTemplateerconfig;
			freeMarkerTemplateerconfig = freeMarkerconfig.getTemplate(templateFile, "UTF-8");
			// 往模板中写入数据
			freeMarkerTemplateerconfig.process(data, htmlWriter);
			htmlWriter.flush();
			htmlOut.flush();

			String outFile = outPath + outFileName;
			FileOutputStream wordFile = new FileOutputStream(outFile);
			wordFile.write(htmlOut.toByteArray());
			wordFile.close();

			htmlWriter.close();
			htmlOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	}

}
