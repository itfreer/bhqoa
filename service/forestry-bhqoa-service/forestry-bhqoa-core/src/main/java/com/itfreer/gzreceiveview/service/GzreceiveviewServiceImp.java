package com.itfreer.gzreceiveview.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzreceivefile.dao.GzreceivefileDao;
import com.itfreer.gzreceivefile.entity.GzreceivefileEntity;
import com.itfreer.gzreceiveview.dao.GzreceiveviewDao;
import com.itfreer.gzreceiveview.entity.GzreceiveviewEntity;
import com.itfreer.gzreceiveview.service.GzreceiveviewService;

/**
 * 定义收文管理实现类
 */
@Component("GzreceiveviewServiceImp")
public class GzreceiveviewServiceImp extends DictionaryConvertServiceImp<GzreceiveviewEntity> implements GzreceiveviewService {

	@Autowired
	private GzreceiveviewDao dao;
	
	@Autowired
	private GzreceivefileDao filedao;

	@Override
	protected BaseDao<GzreceiveviewEntity> getDao() {
		return dao;
	}
	
	@Override
	public GzreceiveviewEntity update(GzreceiveviewEntity entity) {
		GzreceivefileEntity gzreceiveEntity=new GzreceivefileEntity();
		gzreceiveEntity.setId(entity.getId());
		gzreceiveEntity.setFwid(entity.getFwid());
		gzreceiveEntity.setJsrid(entity.getJsrid());
		gzreceiveEntity.setJsrjg(entity.getJsrjg());
		gzreceiveEntity.setJsrmc(entity.getJsrmc());
		gzreceiveEntity.setJsrjgmc(entity.getJsrjgmc());
		gzreceiveEntity.setJsryj(entity.getJsryj());
		gzreceiveEntity.setJsrzw(entity.getJsrzw());
		gzreceiveEntity.setSfqr(entity.getSfqr());
		gzreceiveEntity.setSfyd(entity.getSfyd());
		gzreceiveEntity.setYdsj(entity.getYdsj());
		filedao.update(gzreceiveEntity);
		return entity;
	}
}