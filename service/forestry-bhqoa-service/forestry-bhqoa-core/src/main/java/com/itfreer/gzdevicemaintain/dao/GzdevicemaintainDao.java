package com.itfreer.gzdevicemaintain.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzdevicemaintain.entity.GzdevicemaintainEntity;

/**
 * 定义设备维修记录接口
 */
public interface GzdevicemaintainDao extends BaseDao<GzdevicemaintainEntity> {
	
}