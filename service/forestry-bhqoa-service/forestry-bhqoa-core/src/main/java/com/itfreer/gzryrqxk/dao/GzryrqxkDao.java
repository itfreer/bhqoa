package com.itfreer.gzryrqxk.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzryrqxk.entity.GzryrqxkEntity;

/**
 * 定义外籍人员入区许可、外来科研人员入区许可接口
 */
public interface GzryrqxkDao extends BaseDao<GzryrqxkEntity> {
	
}