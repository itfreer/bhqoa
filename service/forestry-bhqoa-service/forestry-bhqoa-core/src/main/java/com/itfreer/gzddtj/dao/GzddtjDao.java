package com.itfreer.gzddtj.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzddtj.entity.GzddtjEntity;

/**
 * 定义调度统计接口
 */
public interface GzddtjDao extends BaseDao<GzddtjEntity> {
	
}