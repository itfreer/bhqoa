package com.itfreer.gzglzda.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzglzda.entity.GzglzdaEntity;

/**
 * 定义管理站档案接口
 */
public interface GzglzdaDao extends BaseDao<GzglzdaEntity> {
	
}