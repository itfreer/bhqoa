package com.itfreer.gzwlkyxmcg.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzwlkyxmcg.dao.GzwlkyxmcgDao;
import com.itfreer.gzwlkyxmcg.entity.GzwlkyxmcgEntity;
import com.itfreer.gzwlkyxmcg.service.GzwlkyxmcgService;

/**
 * 定义外来科研项目成果实现类
 */
@Component("GzwlkyxmcgServiceImp")
public class GzwlkyxmcgServiceImp extends DictionaryConvertServiceImp<GzwlkyxmcgEntity> implements GzwlkyxmcgService {

	@Autowired
	private GzwlkyxmcgDao dao;

	@Override
	protected BaseDao<GzwlkyxmcgEntity> getDao() {
		return dao;
	}
}