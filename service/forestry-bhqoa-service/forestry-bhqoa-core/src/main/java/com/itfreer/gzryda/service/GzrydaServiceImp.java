package com.itfreer.gzryda.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzryda.dao.GzrydaDao;
import com.itfreer.gzryda.entity.GzrydaEntity;
import com.itfreer.gzryda.service.GzrydaService;

/**
 * 定义人事档案实现类
 */
@Component("GzrydaServiceImp")
public class GzrydaServiceImp extends DictionaryConvertServiceImp<GzrydaEntity> implements GzrydaService {

	@Autowired
	private GzrydaDao dao;

	@Override
	protected BaseDao<GzrydaEntity> getDao() {
		return dao;
	}
}