package com.itfreer.gzzysq.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.flow.imp.BaseWorkFlowServiceImp;
import com.itfreer.form.api.BaseDao;
import com.itfreer.gzzysq.dao.GzzysqDao;
import com.itfreer.gzzysq.entity.GzzysqEntity;

/**
 * 定义资源申请实现类
 */
@Component("GzzysqServiceImp")
public class GzzysqServiceImp extends BaseWorkFlowServiceImp<GzzysqEntity> implements GzzysqService {

	@Autowired
	private GzzysqDao dao;

	@Override
	protected BaseDao<GzzysqEntity> getDao() {
		return dao;
	}
}