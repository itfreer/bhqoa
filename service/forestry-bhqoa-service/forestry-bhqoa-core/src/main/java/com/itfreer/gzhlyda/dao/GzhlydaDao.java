package com.itfreer.gzhlyda.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzhlyda.entity.GzhlydaEntity;

/**
 * 定义护林员档案接口
 */
public interface GzhlydaDao extends BaseDao<GzhlydaEntity> {
	
}