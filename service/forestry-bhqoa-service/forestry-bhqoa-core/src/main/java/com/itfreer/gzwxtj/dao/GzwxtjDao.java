package com.itfreer.gzwxtj.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzwxtj.entity.GzwxtjEntity;

/**
 * 定义维修统计接口
 */
public interface GzwxtjDao extends BaseDao<GzwxtjEntity> {
	
}