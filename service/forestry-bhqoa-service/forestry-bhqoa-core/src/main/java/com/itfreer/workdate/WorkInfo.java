package com.itfreer.workdate;

/**
 * 工作信息
 */
public class WorkInfo {

	/**
	 * 是否工作(包含请假，出差，外出等)
	 */
	private boolean isWork = false;
	/**
	 * 工作内容
	 */
	private String work;
	/**
	 * 是否有上班卡
	 */
	private boolean startWork = false;
	/**
	 * 迟到时间
	 */
	private int lateTime = 0;
	/**
	 * 是否有下班卡
	 */
	private boolean endWork = false;
	/**
	 * 早退时间
	 */
	private int leaveEarlyTime = 0;

	public boolean getIsWork() {
		return isWork;
	}

	public void setIsWork(boolean isWork) {
		this.isWork = isWork;
	}

	public String getWork() {
		return work;
	}

	public void setWork(String work) {
		this.work = work;
	}

	public boolean isStartWork() {
		return startWork;
	}

	public void setStartWork(boolean startWork) {
		this.startWork = startWork;
	}

	public int getLateTime() {
		return lateTime;
	}

	public void setLateTime(int lateTime) {
		this.lateTime = lateTime;
	}

	public boolean isEndWork() {
		return endWork;
	}

	public void setEndWork(boolean endWork) {
		this.endWork = endWork;
	}

	public int getLeaveEarlyTime() {
		return leaveEarlyTime;
	}

	public void setLeaveEarlyTime(int leaveEarlyTime) {
		this.leaveEarlyTime = leaveEarlyTime;
	}
}
