package com.itfreer.gzlytjajx.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzlytjajx.entity.GzlytjajxEntity;

/**
 * 定义林业统计案件续接口
 */
public interface GzlytjajxDao extends BaseDao<GzlytjajxEntity> {
	
}