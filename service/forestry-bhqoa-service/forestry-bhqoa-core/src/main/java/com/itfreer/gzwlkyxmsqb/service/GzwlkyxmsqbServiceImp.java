package com.itfreer.gzwlkyxmsqb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.flow.imp.BaseWorkFlowServiceImp;
import com.itfreer.form.api.BaseDao;
import com.itfreer.gzwlkyxmsqb.dao.GzwlkyxmsqbDao;
import com.itfreer.gzwlkyxmsqb.entity.GzwlkyxmsqbEntity;
import com.itfreer.gzwlkyxmsqb.service.GzwlkyxmsqbService;

/**
 * 定义外来科研项目申请表实现类
 */
@Component("GzwlkyxmsqbServiceImp")
public class GzwlkyxmsqbServiceImp extends BaseWorkFlowServiceImp<GzwlkyxmsqbEntity> implements GzwlkyxmsqbService {

	@Autowired
	private GzwlkyxmsqbDao dao;

	@Override
	protected BaseDao<GzwlkyxmsqbEntity> getDao() {
		return dao;
	}
}