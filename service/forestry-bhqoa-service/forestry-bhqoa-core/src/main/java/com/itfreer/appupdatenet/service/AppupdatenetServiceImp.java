package com.itfreer.appupdatenet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.appupdatenet.dao.AppupdatenetDao;
import com.itfreer.appupdatenet.entity.AppupdatenetEntity;
import com.itfreer.appupdatenet.service.AppupdatenetService;

/**
 * 定义APP更新实现类
 */
@Component("AppupdatenetServiceImp")
public class AppupdatenetServiceImp extends DictionaryConvertServiceImp<AppupdatenetEntity> implements AppupdatenetService {

	@Autowired
	private AppupdatenetDao dao;

	@Override
	protected BaseDao<AppupdatenetEntity> getDao() {
		return dao;
	}
}