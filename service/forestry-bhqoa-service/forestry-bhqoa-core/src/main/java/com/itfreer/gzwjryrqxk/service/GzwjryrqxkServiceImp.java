package com.itfreer.gzwjryrqxk.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.flow.imp.BaseWorkFlowServiceImp;
import com.itfreer.form.api.BaseDao;
import com.itfreer.gzwjryrqxk.dao.GzwjryrqxkDao;
import com.itfreer.gzwjryrqxk.entity.GzwjryrqxkEntity;

/**
 * 定义外籍人员入区许可实现类
 */
@Component("GzwjryrqxkServiceImp")
public class GzwjryrqxkServiceImp extends BaseWorkFlowServiceImp<GzwjryrqxkEntity> implements GzwjryrqxkService {

	@Autowired
	private GzwjryrqxkDao dao;

	@Override
	protected BaseDao<GzwjryrqxkEntity> getDao() {
		return dao;
	}
}