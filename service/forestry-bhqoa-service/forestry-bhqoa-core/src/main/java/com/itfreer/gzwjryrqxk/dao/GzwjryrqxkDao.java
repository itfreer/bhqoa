package com.itfreer.gzwjryrqxk.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzwjryrqxk.entity.GzwjryrqxkEntity;

/**
 * 定义外籍人员入区许可接口
 */
public interface GzwjryrqxkDao extends BaseDao<GzwjryrqxkEntity> {
	
}