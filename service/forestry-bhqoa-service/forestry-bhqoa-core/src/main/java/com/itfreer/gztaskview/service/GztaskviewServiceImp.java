package com.itfreer.gztaskview.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzschedule.dao.ScheduleDao;
import com.itfreer.gzschedule.entity.ScheduleEntity;
import com.itfreer.gzschedule.service.ScheduleServiceImp;
import com.itfreer.gztask.dao.gztaskDao;
import com.itfreer.gztask.entity.gztaskEntity;
import com.itfreer.gztaskview.dao.GztaskviewDao;
import com.itfreer.gztaskview.entity.GztaskviewEntity;
import com.itfreer.gztaskview.service.GztaskviewService;

/**
 * 定义收文视图实现类
 */
@Component("GztaskviewServiceImp")
public class GztaskviewServiceImp extends DictionaryConvertServiceImp<GztaskviewEntity> implements GztaskviewService {

	@Autowired
	private GztaskviewDao dao;
	
	@Autowired
	private gztaskDao filedao;
	
	@Autowired
	private ScheduleDao scheduledao;
	
	@Autowired
	private ScheduleServiceImp scheduleservice;

	@Override
	protected BaseDao<GztaskviewEntity> getDao() {
		return dao;
	}
	
	@Override
	public GztaskviewEntity update(GztaskviewEntity entity) {
		
		@SuppressWarnings("unchecked")
		Map<String,Object> where =new HashedMap();
		where.put("id:=", entity.getFwid());
		List<ScheduleEntity> scheduleEntitys=scheduleservice.getEntitys(null, where, null, 1, 1);
		ScheduleEntity scheduleEntity=scheduleEntitys.get(0);
		scheduleEntity.setId(entity.getFwid());
		scheduleEntity.setZt(entity.getZt());
		if(entity.getWcsj()!=null) {
			scheduleEntity.setWcsj(entity.getWcsj());
		}
		scheduledao.update(scheduleEntity);
		
		gztaskEntity gztaskEntity=new gztaskEntity();
		gztaskEntity.setId(entity.getId());
		gztaskEntity.setFwid(entity.getFwid());
		gztaskEntity.setJsrid(entity.getJsrid());
		gztaskEntity.setJsrjg(entity.getJsrjg());
		gztaskEntity.setJsrmc(entity.getJsrmc());
		gztaskEntity.setSfqr(entity.getSfqr());
		gztaskEntity.setQrsj(entity.getQrsj());
		filedao.update(gztaskEntity);
		return entity;
	}
}