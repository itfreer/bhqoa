package com.itfreer.gzvisitorstatisticsview.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzvisitorstatisticsview.entity.GzvisitorstatisticsviewEntity;

/**
 * 定义访客统计（视图）接口
 */
public interface GzvisitorstatisticsviewDao extends BaseDao<GzvisitorstatisticsviewEntity> {
	
}