package com.itfreer.gzccgl.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzccgl.entity.GzccglEntity;

/**
 * 定义出差管理接口
 */
public interface GzccglDao extends BaseDao<GzccglEntity> {
	
}