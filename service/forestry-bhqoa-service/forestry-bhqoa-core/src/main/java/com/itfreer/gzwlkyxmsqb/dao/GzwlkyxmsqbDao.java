package com.itfreer.gzwlkyxmsqb.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzwlkyxmsqb.entity.GzwlkyxmsqbEntity;

/**
 * 定义外来科研项目申请表接口
 */
public interface GzwlkyxmsqbDao extends BaseDao<GzwlkyxmsqbEntity> {
	
}