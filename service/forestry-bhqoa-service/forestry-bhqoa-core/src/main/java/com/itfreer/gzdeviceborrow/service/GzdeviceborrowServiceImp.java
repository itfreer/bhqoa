package com.itfreer.gzdeviceborrow.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzdeviceborrow.dao.GzdeviceborrowDao;
import com.itfreer.gzdeviceborrow.entity.GzdeviceborrowEntity;
import com.itfreer.gzdeviceborrow.service.GzdeviceborrowService;

/**
 * 定义设备借还实现类
 */
@Component("GzdeviceborrowServiceImp")
public class GzdeviceborrowServiceImp extends DictionaryConvertServiceImp<GzdeviceborrowEntity> implements GzdeviceborrowService {

	@Autowired
	private GzdeviceborrowDao dao;

	@Override
	protected BaseDao<GzdeviceborrowEntity> getDao() {
		return dao;
	}
}