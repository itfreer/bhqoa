package com.itfreer.gzryda.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzryda.entity.GzrydaEntity;

/**
 * 定义人事档案接口
 */
public interface GzrydaDao extends BaseDao<GzrydaEntity> {
	
}