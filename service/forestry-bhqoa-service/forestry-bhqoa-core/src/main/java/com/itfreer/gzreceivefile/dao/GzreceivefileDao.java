package com.itfreer.gzreceivefile.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzreceivefile.entity.GzreceivefileEntity;

/**
 * 定义收文管理接口
 */
public interface GzreceivefileDao extends BaseDao<GzreceivefileEntity> {
	
}