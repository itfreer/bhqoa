package com.itfreer.gzschedule.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzschedule.entity.ScheduleEntity;

/**
 * 定义日常安排接口
 */
public interface ScheduleDao extends BaseDao<ScheduleEntity> {
	
}