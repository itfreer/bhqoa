package com.itfreer.gzvisitorreserve.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzvisitorreserve.entity.GzvisitorreserveEntity;

/**
 * 定义访客预约管理接口
 */
public interface GzvisitorreserveDao extends BaseDao<GzvisitorreserveEntity> {
	
}