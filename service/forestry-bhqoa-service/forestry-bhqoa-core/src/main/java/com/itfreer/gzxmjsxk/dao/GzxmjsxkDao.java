package com.itfreer.gzxmjsxk.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzxmjsxk.entity.GzxmjsxkEntity;

/**
 * 定义项目建设许可接口
 */
public interface GzxmjsxkDao extends BaseDao<GzxmjsxkEntity> {
	
}