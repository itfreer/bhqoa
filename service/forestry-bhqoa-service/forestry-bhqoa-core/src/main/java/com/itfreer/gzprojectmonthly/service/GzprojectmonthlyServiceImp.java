package com.itfreer.gzprojectmonthly.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzprojectmonthly.dao.GzprojectmonthlyDao;
import com.itfreer.gzprojectmonthly.entity.GzprojectmonthlyEntity;
import com.itfreer.gzprojectmonthly.service.GzprojectmonthlyService;

/**
 * 定义项目月报实现类
 */
@Component("GzprojectmonthlyServiceImp")
public class GzprojectmonthlyServiceImp extends DictionaryConvertServiceImp<GzprojectmonthlyEntity> implements GzprojectmonthlyService {

	@Autowired
	private GzprojectmonthlyDao dao;

	@Override
	protected BaseDao<GzprojectmonthlyEntity> getDao() {
		return dao;
	}
}