package com.itfreer.gzwlkyxmlfry.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzwlkyxmlfry.dao.GzwlkyxmlfryDao;
import com.itfreer.gzwlkyxmlfry.entity.GzwlkyxmlfryEntity;
import com.itfreer.gzwlkyxmlfry.service.GzwlkyxmlfryService;

/**
 * 定义外来科研项目人随访人员实现类
 */
@Component("GzwlkyxmlfryServiceImp")
public class GzwlkyxmlfryServiceImp extends DictionaryConvertServiceImp<GzwlkyxmlfryEntity> implements GzwlkyxmlfryService {

	@Autowired
	private GzwlkyxmlfryDao dao;

	@Override
	protected BaseDao<GzwlkyxmlfryEntity> getDao() {
		return dao;
	}
}