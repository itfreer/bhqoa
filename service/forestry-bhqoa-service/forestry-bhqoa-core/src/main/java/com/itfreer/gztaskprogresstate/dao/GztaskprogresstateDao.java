package com.itfreer.gztaskprogresstate.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gztaskprogresstate.entity.GztaskprogresstateEntity;

/**
 * 定义任务监测接口
 */
public interface GztaskprogresstateDao extends BaseDao<GztaskprogresstateEntity> {
	
}