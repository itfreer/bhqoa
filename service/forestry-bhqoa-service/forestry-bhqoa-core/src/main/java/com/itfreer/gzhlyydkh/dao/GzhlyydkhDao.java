package com.itfreer.gzhlyydkh.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzhlyydkh.entity.GzhlyydkhEntity;

/**
 * 定义护林员月度考核接口
 */
public interface GzhlyydkhDao extends BaseDao<GzhlyydkhEntity> {
	
}