package com.itfreer.gzprojectinfo.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzprojectinfo.entity.GzprojectinfoEntity;

/**
 * 定义项目立项管理接口
 */
public interface GzprojectinfoDao extends BaseDao<GzprojectinfoEntity> {
	
}