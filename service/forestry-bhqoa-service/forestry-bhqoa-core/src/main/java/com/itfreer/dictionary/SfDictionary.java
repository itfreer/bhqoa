package com.itfreer.dictionary;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.itfreer.form.dictionary.LocalDictionaryBase;
import com.itfreer.form.dictionary.base.DictionaryItem;

@Component
public class SfDictionary  extends LocalDictionaryBase  {

	@Override
	public String getDictionaryName() {
		return "sf_zd";
	}

	@Override
	protected List<DictionaryItem> getDictionarys() {
		List<DictionaryItem> list = new ArrayList<DictionaryItem>();
		DictionaryItem item = new DictionaryItem();
		item.setId("1");
		item.setCode("1");
		item.setDisplay("是");
		item.setOrder(1);
		item.setInfo("是");
		list.add(item);
		
		item = new DictionaryItem();
		item.setId("0");
		item.setCode("0");
		item.setDisplay("否");
		item.setOrder(2);
		item.setInfo("否");
		list.add(item);
		return list;
	}
}
