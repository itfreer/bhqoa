package com.itfreer.gzrskqtj.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzrskqtj.entity.GzrskqtjEntity;

/**
 * 定义考勤统计接口
 */
public interface GzrskqtjDao extends BaseDao<GzrskqtjEntity> {
	
}