package com.itfreer.gzdeviceborrow.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzdeviceborrow.entity.GzdeviceborrowEntity;

/**
 * 定义设备借还接口
 */
public interface GzdeviceborrowDao extends BaseDao<GzdeviceborrowEntity> {
	
}