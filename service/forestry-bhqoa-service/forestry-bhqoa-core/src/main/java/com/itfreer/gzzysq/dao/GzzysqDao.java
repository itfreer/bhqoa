package com.itfreer.gzzysq.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzzysq.entity.GzzysqEntity;

/**
 * 定义资源申请接口
 */
public interface GzzysqDao extends BaseDao<GzzysqEntity> {
	
}