package com.itfreer.gzclwx.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzclda.dao.GzcldaDao;
import com.itfreer.gzclda.entity.GzcldaEntity;
import com.itfreer.gzclwx.dao.GzclwxDao;
import com.itfreer.gzclwx.entity.GzclwxEntity;
import com.itfreer.gzclwx.service.GzclwxService;

/**
 * 定义车辆维修实现类
 */
@Component("GzclwxServiceImp")
public class GzclwxServiceImp extends DictionaryConvertServiceImp<GzclwxEntity> implements GzclwxService {

	@Autowired
	private GzclwxDao dao;
	
	@Autowired
	private GzcldaDao cldadao;

	@Override
	protected BaseDao<GzclwxEntity> getDao() {
		return dao;
	}
	
	@Override
	public GzclwxEntity add(GzclwxEntity entity) {
		//设置车辆状态为维修中
		@SuppressWarnings("unchecked")
		Map<String,Object> where =new HashedMap();
		where.put("scph:=", entity.getCph());
		List<GzcldaEntity> gzcldaEntitys=cldadao.getEntitys(null, where, null, 1, 1);
		GzcldaEntity gzcldaEntity=gzcldaEntitys.get(0);
		gzcldaEntity.setClzt("clzt_wxz");
		cldadao.update(gzcldaEntity);
		
		entity.setWxzt("wxzt_wxz");
		return super.add(entity);
	}
	
	@Override
	public GzclwxEntity update(GzclwxEntity entity) {
		if(("wxzt_yjs").equals(entity.getWxzt())){
			//设置车辆状态为未使用
			@SuppressWarnings("unchecked")
			Map<String,Object> where =new HashedMap();
			where.put("scph:=", entity.getCph());
			List<GzcldaEntity> gzcldaEntitys=cldadao.getEntitys(null, where, null, 1, 1);
			GzcldaEntity gzcldaEntity=gzcldaEntitys.get(0);
			gzcldaEntity.setClzt("clzt_wsy");
			cldadao.update(gzcldaEntity);
		}
		return super.update(entity);
	}
	
}