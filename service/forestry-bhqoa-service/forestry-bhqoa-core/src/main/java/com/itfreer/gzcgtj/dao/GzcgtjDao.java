package com.itfreer.gzcgtj.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzcgtj.entity.GzcgtjEntity;

/**
 * 定义采购统计接口
 */
public interface GzcgtjDao extends BaseDao<GzcgtjEntity> {
	
}