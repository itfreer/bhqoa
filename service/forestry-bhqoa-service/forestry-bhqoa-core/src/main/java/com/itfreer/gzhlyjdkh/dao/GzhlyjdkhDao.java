package com.itfreer.gzhlyjdkh.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzhlyjdkh.entity.GzhlyjdkhEntity;

/**
 * 定义护林员季度考核接口
 */
public interface GzhlyjdkhDao extends BaseDao<GzhlyjdkhEntity> {
	
}