package com.itfreer.gztaskview.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gztaskview.entity.GztaskviewEntity;

/**
 * 定义收文视图接口
 */
public interface GztaskviewDao extends BaseDao<GztaskviewEntity> {
	
}