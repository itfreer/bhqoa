package com.itfreer.gzgzjjr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzgzjjr.dao.GzgzjjrDao;
import com.itfreer.gzgzjjr.entity.GzgzjjrEntity;
import com.itfreer.gzgzjjr.service.GzgzjjrService;

/**
 * 定义工作节假日实现类
 */
@Component("GzgzjjrServiceImp")
public class GzgzjjrServiceImp extends DictionaryConvertServiceImp<GzgzjjrEntity> implements GzgzjjrService {

	@Autowired
	private GzgzjjrDao dao;

	@Override
	protected BaseDao<GzgzjjrEntity> getDao() {
		return dao;
	}
}