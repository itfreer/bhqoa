package com.itfreer.gzqdgl.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzqdgl.dao.GzqdglDao;
import com.itfreer.gzqdgl.entity.GzqdglEntity;
import com.itfreer.gzqdgl.service.GzqdglService;

/**
 * 定义签到管理实现类
 */
@Component("GzqdglServiceImp")
public class GzqdglServiceImp extends DictionaryConvertServiceImp<GzqdglEntity> implements GzqdglService {

	@Autowired
	private GzqdglDao dao;

	@Override
	protected BaseDao<GzqdglEntity> getDao() {
		return dao;
	}
}