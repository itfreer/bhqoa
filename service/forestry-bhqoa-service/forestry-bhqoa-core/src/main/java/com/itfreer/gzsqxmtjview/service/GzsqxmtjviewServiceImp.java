package com.itfreer.gzsqxmtjview.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzsqxmtjview.dao.GzsqxmtjviewDao;
import com.itfreer.gzsqxmtjview.entity.GzsqxmtjviewEntity;
import com.itfreer.gzsqxmtjview.service.GzsqxmtjviewService;

/**
 * 定义社区项目统计(视图)实现类
 */
@Component("GzsqxmtjviewServiceImp")
public class GzsqxmtjviewServiceImp extends DictionaryConvertServiceImp<GzsqxmtjviewEntity> implements GzsqxmtjviewService {

	@Autowired
	private GzsqxmtjviewDao dao;

	@Override
	protected BaseDao<GzsqxmtjviewEntity> getDao() {
		return dao;
	}
}