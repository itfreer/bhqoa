package com.itfreer.vkqtj.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.vkqtj.entity.VkqtjEntity;

/**
 * 定义考勤统计接口
 */
public interface VkqtjDao extends BaseDao<VkqtjEntity> {
	
}