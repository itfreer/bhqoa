package com.itfreer.gzhlygzgl.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzhlygzgl.dao.GzhlygzglDao;
import com.itfreer.gzhlygzgl.entity.GzhlygzglEntity;
import com.itfreer.gzhlygzgl.service.GzhlygzglService;

/**
 * 定义护林员工资管理实现类
 */
@Component("GzhlygzglServiceImp")
public class GzhlygzglServiceImp extends DictionaryConvertServiceImp<GzhlygzglEntity> implements GzhlygzglService {

	@Autowired
	private GzhlygzglDao dao;

	@Override
	protected BaseDao<GzhlygzglEntity> getDao() {
		return dao;
	}
}