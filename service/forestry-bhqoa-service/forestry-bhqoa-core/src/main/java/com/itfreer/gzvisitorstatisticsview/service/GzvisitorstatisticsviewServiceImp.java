package com.itfreer.gzvisitorstatisticsview.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzvisitorstatisticsview.dao.GzvisitorstatisticsviewDao;
import com.itfreer.gzvisitorstatisticsview.entity.GzvisitorstatisticsviewEntity;
import com.itfreer.gzvisitorstatisticsview.service.GzvisitorstatisticsviewService;

/**
 * 定义访客统计（视图）实现类
 */
@Component("GzvisitorstatisticsviewServiceImp")
public class GzvisitorstatisticsviewServiceImp extends DictionaryConvertServiceImp<GzvisitorstatisticsviewEntity> implements GzvisitorstatisticsviewService {

	@Autowired
	private GzvisitorstatisticsviewDao dao;

	@Override
	protected BaseDao<GzvisitorstatisticsviewEntity> getDao() {
		return dao;
	}
}