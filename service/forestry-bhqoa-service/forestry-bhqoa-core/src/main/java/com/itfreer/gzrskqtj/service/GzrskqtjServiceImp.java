package com.itfreer.gzrskqtj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzrskqtj.dao.GzrskqtjDao;
import com.itfreer.gzrskqtj.entity.GzrskqtjEntity;
import com.itfreer.gzrskqtj.service.GzrskqtjService;

/**
 * 定义考勤统计实现类
 */
@Component("GzrskqtjServiceImp")
public class GzrskqtjServiceImp extends DictionaryConvertServiceImp<GzrskqtjEntity> implements GzrskqtjService {

	@Autowired
	private GzrskqtjDao dao;

	@Override
	protected BaseDao<GzrskqtjEntity> getDao() {
		return dao;
	}
}