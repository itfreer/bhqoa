package com.itfreer.dictionary;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.itfreer.form.dictionary.LocalDictionaryBase;
import com.itfreer.form.dictionary.base.DictionaryItem;

/**
 * 审批状态字典
 * @author gj
 *
 */
@Component
public class Spztzd  extends LocalDictionaryBase  {

	@Override
	public String getDictionaryName() {
		return "spzt_zd";
	}

	@Override
	protected List<DictionaryItem> getDictionarys() {
		List<DictionaryItem> list = new ArrayList<DictionaryItem>();
		DictionaryItem item = new DictionaryItem();
		item.setId("2");
		item.setCode("2");
		item.setDisplay("审批中");
		item.setOrder(3);
		item.setInfo("审批中");
		list.add(item);
		
		item = new DictionaryItem();
		item.setId("1");
		item.setCode("1");
		item.setDisplay("草稿");
		item.setOrder(2);
		item.setInfo("草稿");
		list.add(item);
		
		item = new DictionaryItem();
		item.setId("0");
		item.setCode("0");
		item.setDisplay("已审批");
		item.setOrder(1);
		item.setInfo("已审批");
		list.add(item);
		return list;
	}
}

