package com.itfreer.gzschedule.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzschedule.dao.ScheduleDao;
import com.itfreer.gzschedule.entity.ScheduleEntity;
import com.itfreer.gzschedule.service.ScheduleService;

/**
 * 定义日常安排实现类
 */
@Component("ScheduleServiceImp")
public class ScheduleServiceImp extends DictionaryConvertServiceImp<ScheduleEntity> implements ScheduleService {

	@Autowired
	private ScheduleDao dao;

	@Override
	protected BaseDao<ScheduleEntity> getDao() {
		return dao;
	}
}