package com.itfreer.gzdevicepurchase.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzdevicepurchase.entity.GzdevicepurchaseEntity;

/**
 * 定义设备采购管理接口
 */
public interface GzdevicepurchaseDao extends BaseDao<GzdevicepurchaseEntity> {
	
}