package com.itfreer.gzsqclqd.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzsqclqd.entity.GzsqclqdEntity;

/**
 * 定义申请材料清单接口
 */
public interface GzsqclqdDao extends BaseDao<GzsqclqdEntity> {
	
}