package com.itfreer.gzhlyydkh.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzhlyydkh.dao.GzhlyydkhDao;
import com.itfreer.gzhlyydkh.entity.GzhlyydkhEntity;
import com.itfreer.gzhlyydkh.service.GzhlyydkhService;

/**
 * 定义护林员月度考核实现类
 */
@Component("GzhlyydkhServiceImp")
public class GzhlyydkhServiceImp extends DictionaryConvertServiceImp<GzhlyydkhEntity> implements GzhlyydkhService {

	@Autowired
	private GzhlyydkhDao dao;

	@Override
	protected BaseDao<GzhlyydkhEntity> getDao() {
		return dao;
	}
}