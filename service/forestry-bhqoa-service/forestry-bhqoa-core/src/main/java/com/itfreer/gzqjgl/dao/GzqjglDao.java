package com.itfreer.gzqjgl.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzqjgl.entity.GzqjglEntity;

/**
 * 定义请假管理接口
 */
public interface GzqjglDao extends BaseDao<GzqjglEntity> {
	
}