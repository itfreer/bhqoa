package com.itfreer.gzvisitorreception.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzvisitorreception.entity.GzvisitorreceptionEntity;

/**
 * 定义访问接待管理接口
 */
public interface GzvisitorreceptionDao extends BaseDao<GzvisitorreceptionEntity> {
	
}