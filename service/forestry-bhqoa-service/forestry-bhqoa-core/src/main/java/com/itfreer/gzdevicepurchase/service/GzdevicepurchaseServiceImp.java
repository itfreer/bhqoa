package com.itfreer.gzdevicepurchase.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzdevicepurchase.dao.GzdevicepurchaseDao;
import com.itfreer.gzdevicepurchase.entity.GzdevicepurchaseEntity;
import com.itfreer.gzdevicepurchase.service.GzdevicepurchaseService;

/**
 * 定义设备采购管理实现类
 */
@Component("GzdevicepurchaseServiceImp")
public class GzdevicepurchaseServiceImp extends DictionaryConvertServiceImp<GzdevicepurchaseEntity> implements GzdevicepurchaseService {

	@Autowired
	private GzdevicepurchaseDao dao;

	@Override
	protected BaseDao<GzdevicepurchaseEntity> getDao() {
		return dao;
	}
}