package com.itfreer.gzddtz.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzddtz.dao.GzddtzDao;
import com.itfreer.gzddtz.entity.GzddtzEntity;
import com.itfreer.gzddtz.service.GzddtzService;

/**
 * 定义调度台账实现类
 */
@Component("GzddtzServiceImp")
public class GzddtzServiceImp extends DictionaryConvertServiceImp<GzddtzEntity> implements GzddtzService {

	@Autowired
	private GzddtzDao dao;

	@Override
	protected BaseDao<GzddtzEntity> getDao() {
		return dao;
	}
}