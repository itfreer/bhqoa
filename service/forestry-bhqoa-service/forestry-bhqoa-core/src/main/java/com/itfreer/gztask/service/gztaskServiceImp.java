package com.itfreer.gztask.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gztask.dao.gztaskDao;
import com.itfreer.gztask.entity.gztaskEntity;
import com.itfreer.gztask.service.gztaskService;

/**
 * 定义任务安排实现类
 */
@Component("gztaskServiceImp")
public class gztaskServiceImp extends DictionaryConvertServiceImp<gztaskEntity> implements gztaskService {

	@Autowired
	private gztaskDao dao;

	@Override
	protected BaseDao<gztaskEntity> getDao() {
		return dao;
	}
}