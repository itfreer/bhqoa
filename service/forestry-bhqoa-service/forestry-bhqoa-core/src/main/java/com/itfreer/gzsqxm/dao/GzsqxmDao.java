package com.itfreer.gzsqxm.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzsqxm.entity.GzsqxmEntity;

/**
 * 定义社区项目申请/台账接口
 */
public interface GzsqxmDao extends BaseDao<GzsqxmEntity> {
	
}