package com.itfreer.gzpublishfile.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.flow.imp.BaseWorkFlowServiceImp;
import com.itfreer.form.api.BaseDao;
import com.itfreer.gzpublishfile.dao.GzpublishfileDao;
import com.itfreer.gzpublishfile.entity.GzpublishfileEntity;

/**
 * 定义发文管理实现类
 */
@Component("GzpublishfileServiceImp")
public class GzpublishfileServiceImp extends BaseWorkFlowServiceImp<GzpublishfileEntity> implements GzpublishfileService {

	@Autowired
	private GzpublishfileDao dao;

	@Override
	protected BaseDao<GzpublishfileEntity> getDao() {
		return dao;
	}
}