package com.itfreer.gzplanpersonnel.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzplanpersonnel.dao.GzplanpersonnelDao;
import com.itfreer.gzplanpersonnel.entity.GzplanpersonnelEntity;
import com.itfreer.gzplanpersonnel.service.GzplanpersonnelService;

/**
 * 定义项目规划人员实现类
 */
@Component("GzplanpersonnelServiceImp")
public class GzplanpersonnelServiceImp extends DictionaryConvertServiceImp<GzplanpersonnelEntity> implements GzplanpersonnelService {

	@Autowired
	private GzplanpersonnelDao dao;

	@Override
	protected BaseDao<GzplanpersonnelEntity> getDao() {
		return dao;
	}
}