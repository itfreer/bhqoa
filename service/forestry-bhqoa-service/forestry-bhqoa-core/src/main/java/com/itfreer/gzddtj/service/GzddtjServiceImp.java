package com.itfreer.gzddtj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzddtj.dao.GzddtjDao;
import com.itfreer.gzddtj.entity.GzddtjEntity;
import com.itfreer.gzddtj.service.GzddtjService;

/**
 * 定义调度统计实现类
 */
@Component("GzddtjServiceImp")
public class GzddtjServiceImp extends DictionaryConvertServiceImp<GzddtjEntity> implements GzddtjService {

	@Autowired
	private GzddtjDao dao;

	@Override
	protected BaseDao<GzddtjEntity> getDao() {
		return dao;
	}
}