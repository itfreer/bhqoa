package com.itfreer.gzhlygzgl.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzhlygzgl.entity.GzhlygzglEntity;

/**
 * 定义护林员工资管理接口
 */
public interface GzhlygzglDao extends BaseDao<GzhlygzglEntity> {
	
}