package com.itfreer.gzykcz.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzykcz.entity.GzykczEntity;

/**
 * 定义油卡充值接口
 */
public interface GzykczDao extends BaseDao<GzykczEntity> {
	
}