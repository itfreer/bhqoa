package com.itfreer.gzqdgl.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzqdgl.entity.GzqdglEntity;

/**
 * 定义签到管理接口
 */
public interface GzqdglDao extends BaseDao<GzqdglEntity> {
	
}