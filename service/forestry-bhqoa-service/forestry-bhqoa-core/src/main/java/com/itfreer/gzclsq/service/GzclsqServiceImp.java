package com.itfreer.gzclsq.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.flow.imp.BaseWorkFlowServiceImp;
import com.itfreer.bpm.messager.BpmMessage;
import com.itfreer.bpm.option.para.OptionInfo;
import com.itfreer.bpm.power.para.UserInfo;
import com.itfreer.form.api.BaseDao;
import com.itfreer.gzclda.dao.GzcldaDao;
import com.itfreer.gzclda.entity.GzcldaEntity;
import com.itfreer.gzclsq.dao.GzclsqDao;
import com.itfreer.gzclsq.entity.GzclsqEntity;

/**
 * 定义车辆使用实现类
 */
@Component("GzclsqServiceImp")
public class GzclsqServiceImp extends BaseWorkFlowServiceImp<GzclsqEntity> implements GzclsqService {

	@Autowired
	private GzclsqDao dao;
	
	@Autowired
	private GzcldaDao cldadao;

	@Override
	protected BaseDao<GzclsqEntity> getDao() {
		return dao;
	}
	
	/*@Override
	public GzclsqEntity add(GzclsqEntity entity) {
		//设置车辆状态为使用中
		Map<String,Object> where =new HashedMap();
		where.put("scph:=", entity.getCph());
		List<GzcldaEntity> gzcldaEntitys=cldadao.getEntitys(null, where, null, 1, 1);
		GzcldaEntity gzcldaEntity=gzcldaEntitys.get(0);
		gzcldaEntity.setClzt("clzt_syz");
		cldadao.update(gzcldaEntity);
		
		entity.setClzt("clsyzt_syz");
		return super.add(entity);
	}*/
	
	@Override
	public GzclsqEntity update(GzclsqEntity entity) {
		if(("clsyzt_ygh").equals(entity.getClzt())){
			//设置车辆状态为未使用
			@SuppressWarnings("unchecked")
			Map<String,Object> where =new HashedMap();
			where.put("scph:=", entity.getCph());
			List<GzcldaEntity> gzcldaEntitys=cldadao.getEntitys(null, where, null, 1, 1);
			GzcldaEntity gzcldaEntity=gzcldaEntitys.get(0);
			gzcldaEntity.setClzt("clzt_wsy");
			cldadao.update(gzcldaEntity);
		}
		return super.update(entity);
	}
	
	@Override
	public BpmMessage signTask(OptionInfo oInfo,GzclsqEntity entity,UserInfo user) {
		if(oInfo.getNextTask()==null  || oInfo.getNextTask().size()<1) {
			entity.setSpzt("0");
			
			//设置车辆状态为未使用
			/*Map<String,Object> where =new HashedMap();
			where.put("scph:=", entity.getCph());
			List<GzcldaEntity> gzcldaEntitys=cldadao.getEntitys(null, where, null, 1, 1);
			GzcldaEntity gzcldaEntity=gzcldaEntitys.get(0);
			gzcldaEntity.setClzt("clzt_syz");
			cldadao.update(gzcldaEntity);*/
		}else {
			entity.setSpzt("2");
		}
		return super.signTask(oInfo, entity, user);
	}
	
	@Override
	public BpmMessage disposal(OptionInfo optInfo,GzclsqEntity entity,UserInfo user) {
		entity.setSpzt("0");
		return super.disposal(optInfo, entity, user);
	}
	
	@Override
	public BpmMessage userDispose(OptionInfo optInfo,GzclsqEntity entity,UserInfo user) {
		entity.setSpzt("0");
		return super.userDispose(optInfo, entity, user);
	}
	
	@Override
	public BpmMessage processSave(OptionInfo oInfo,GzclsqEntity entity,UserInfo user) {
		if(entity!=null && entity.getSexeid()!=null && "".equals(entity.getSexeid())) {
			
		}else if(entity!=null) {
			entity.setSpzt("1");
		}
		return super.processSave(oInfo, entity, user);
	}
	
	/**
	 * 项目激活
	 */
	@Override
	public BpmMessage easter(OptionInfo optInfo,GzclsqEntity entity,UserInfo user) {
		entity.setSpzt("2");
		return super.easter(optInfo, entity, user);
	}
}