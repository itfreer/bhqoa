package com.itfreer.gzqdglqddd.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzqdglqddd.entity.GzqdglqdddEntity;

/**
 * 定义签到地点接口
 */
public interface GzqdglqdddDao extends BaseDao<GzqdglqdddEntity> {
	
}