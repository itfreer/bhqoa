package com.itfreer.gzvisitorreception.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzvisitorreception.dao.GzvisitorreceptionDao;
import com.itfreer.gzvisitorreception.entity.GzvisitorreceptionEntity;
import com.itfreer.gzvisitorreception.service.GzvisitorreceptionService;

/**
 * 定义访问接待管理实现类
 */
@Component("GzvisitorreceptionServiceImp")
public class GzvisitorreceptionServiceImp extends DictionaryConvertServiceImp<GzvisitorreceptionEntity> implements GzvisitorreceptionService {

	@Autowired
	private GzvisitorreceptionDao dao;

	@Override
	protected BaseDao<GzvisitorreceptionEntity> getDao() {
		return dao;
	}
}