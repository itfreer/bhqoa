package com.itfreer.gzyzgl.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.flow.imp.BaseWorkFlowServiceImp;
import com.itfreer.form.api.BaseDao;
import com.itfreer.gzyzgl.dao.GzyzglDao;
import com.itfreer.gzyzgl.entity.GzyzglEntity;

/**
 * 定义用章管理实现类
 */
@Component("GzyzglServiceImp")
public class GzyzglServiceImp extends BaseWorkFlowServiceImp<GzyzglEntity> implements GzyzglService {

	@Autowired
	private GzyzglDao dao;

	@Override
	protected BaseDao<GzyzglEntity> getDao() {
		return dao;
	}
}