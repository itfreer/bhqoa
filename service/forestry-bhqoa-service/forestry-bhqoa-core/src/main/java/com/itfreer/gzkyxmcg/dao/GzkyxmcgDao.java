package com.itfreer.gzkyxmcg.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzkyxmcg.entity.GzkyxmcgEntity;

/**
 * 定义科研项目成果接口
 */
public interface GzkyxmcgDao extends BaseDao<GzkyxmcgEntity> {
	
}