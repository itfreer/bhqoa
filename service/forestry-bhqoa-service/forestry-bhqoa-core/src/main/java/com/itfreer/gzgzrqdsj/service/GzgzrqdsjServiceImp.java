package com.itfreer.gzgzrqdsj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzgzrqdsj.dao.GzgzrqdsjDao;
import com.itfreer.gzgzrqdsj.entity.GzgzrqdsjEntity;
import com.itfreer.gzgzrqdsj.service.GzgzrqdsjService;

/**
 * 定义签到时间实现类
 */
@Component("GzgzrqdsjServiceImp")
public class GzgzrqdsjServiceImp extends DictionaryConvertServiceImp<GzgzrqdsjEntity> implements GzgzrqdsjService {

	@Autowired
	private GzgzrqdsjDao dao;

	@Override
	protected BaseDao<GzgzrqdsjEntity> getDao() {
		return dao;
	}
}