package com.itfreer.gzryrqxk.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.flow.imp.BaseWorkFlowServiceImp;
import com.itfreer.form.api.BaseDao;
import com.itfreer.gzryrqxk.dao.GzryrqxkDao;
import com.itfreer.gzryrqxk.entity.GzryrqxkEntity;

/**
 * 定义外籍人员入区许可、外来科研人员入区许可实现类
 */
@Component("GzryrqxkServiceImp")
public class GzryrqxkServiceImp extends BaseWorkFlowServiceImp<GzryrqxkEntity> implements GzryrqxkService {

	@Autowired
	private GzryrqxkDao dao;

	@Override
	protected BaseDao<GzryrqxkEntity> getDao() {
		return dao;
	}
}