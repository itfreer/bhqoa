package com.itfreer.gzghfff.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzghfff.dao.GzghfffDao;
import com.itfreer.gzghfff.entity.GzghfffEntity;
import com.itfreer.gzghfff.service.GzghfffService;

/**
 * 定义管护费发放实现类
 */
@Component("GzghfffServiceImp")
public class GzghfffServiceImp extends DictionaryConvertServiceImp<GzghfffEntity> implements GzghfffService {

	@Autowired
	private GzghfffDao dao;

	@Override
	protected BaseDao<GzghfffEntity> getDao() {
		return dao;
	}
}