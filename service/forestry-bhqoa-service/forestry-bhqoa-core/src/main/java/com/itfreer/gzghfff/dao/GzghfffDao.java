package com.itfreer.gzghfff.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzghfff.entity.GzghfffEntity;

/**
 * 定义管护费发放接口
 */
public interface GzghfffDao extends BaseDao<GzghfffEntity> {
	
}