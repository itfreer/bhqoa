package com.itfreer.gzclwx.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzclwx.entity.GzclwxEntity;

/**
 * 定义车辆维修接口
 */
public interface GzclwxDao extends BaseDao<GzclwxEntity> {
	
}