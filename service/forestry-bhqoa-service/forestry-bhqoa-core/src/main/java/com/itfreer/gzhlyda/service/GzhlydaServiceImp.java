package com.itfreer.gzhlyda.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzhlyda.dao.GzhlydaDao;
import com.itfreer.gzhlyda.entity.GzhlydaEntity;
import com.itfreer.gzhlyda.service.GzhlydaService;

/**
 * 定义护林员档案实现类
 */
@Component("GzhlydaServiceImp")
public class GzhlydaServiceImp extends DictionaryConvertServiceImp<GzhlydaEntity> implements GzhlydaService {

	@Autowired
	private GzhlydaDao dao;

	@Override
	protected BaseDao<GzhlydaEntity> getDao() {
		return dao;
	}
}