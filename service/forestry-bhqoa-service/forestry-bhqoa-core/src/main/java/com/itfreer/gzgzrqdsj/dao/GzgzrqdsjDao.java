package com.itfreer.gzgzrqdsj.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzgzrqdsj.entity.GzgzrqdsjEntity;

/**
 * 定义签到时间接口
 */
public interface GzgzrqdsjDao extends BaseDao<GzgzrqdsjEntity> {
	
}