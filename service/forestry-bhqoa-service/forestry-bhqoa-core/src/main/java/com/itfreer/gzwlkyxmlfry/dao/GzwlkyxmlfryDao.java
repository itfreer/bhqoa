package com.itfreer.gzwlkyxmlfry.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzwlkyxmlfry.entity.GzwlkyxmlfryEntity;

/**
 * 定义外来科研项目人随访人员接口
 */
public interface GzwlkyxmlfryDao extends BaseDao<GzwlkyxmlfryEntity> {
	
}