package com.itfreer.gzcgtj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzcgtj.dao.GzcgtjDao;
import com.itfreer.gzcgtj.entity.GzcgtjEntity;
import com.itfreer.gzcgtj.service.GzcgtjService;

/**
 * 定义采购统计实现类
 */
@Component("GzcgtjServiceImp")
public class GzcgtjServiceImp extends DictionaryConvertServiceImp<GzcgtjEntity> implements GzcgtjService {

	@Autowired
	private GzcgtjDao dao;

	@Override
	protected BaseDao<GzcgtjEntity> getDao() {
		return dao;
	}
}