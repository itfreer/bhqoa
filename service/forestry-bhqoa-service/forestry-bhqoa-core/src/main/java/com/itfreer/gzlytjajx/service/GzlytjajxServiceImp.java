package com.itfreer.gzlytjajx.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzlytjajx.dao.GzlytjajxDao;
import com.itfreer.gzlytjajx.entity.GzlytjajxEntity;
import com.itfreer.gzlytjajx.service.GzlytjajxService;

/**
 * 定义林业统计案件续实现类
 */
@Component("GzlytjajxServiceImp")
public class GzlytjajxServiceImp extends DictionaryConvertServiceImp<GzlytjajxEntity> implements GzlytjajxService {

	@Autowired
	private GzlytjajxDao dao;

	@Override
	protected BaseDao<GzlytjajxEntity> getDao() {
		return dao;
	}
}