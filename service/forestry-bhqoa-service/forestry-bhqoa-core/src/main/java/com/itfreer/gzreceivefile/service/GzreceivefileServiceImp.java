package com.itfreer.gzreceivefile.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzreceivefile.dao.GzreceivefileDao;
import com.itfreer.gzreceivefile.entity.GzreceivefileEntity;
import com.itfreer.gzreceivefile.service.GzreceivefileService;

/**
 * 定义收文管理实现类
 */
@Component("GzreceivefileServiceImp")
public class GzreceivefileServiceImp extends DictionaryConvertServiceImp<GzreceivefileEntity> implements GzreceivefileService {

	@Autowired
	private GzreceivefileDao dao;

	@Override
	protected BaseDao<GzreceivefileEntity> getDao() {
		return dao;
	}
}