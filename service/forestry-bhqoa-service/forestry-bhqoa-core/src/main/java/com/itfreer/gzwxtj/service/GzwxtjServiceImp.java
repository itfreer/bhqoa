package com.itfreer.gzwxtj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzwxtj.dao.GzwxtjDao;
import com.itfreer.gzwxtj.entity.GzwxtjEntity;
import com.itfreer.gzwxtj.service.GzwxtjService;

/**
 * 定义维修统计实现类
 */
@Component("GzwxtjServiceImp")
public class GzwxtjServiceImp extends DictionaryConvertServiceImp<GzwxtjEntity> implements GzwxtjService {

	@Autowired
	private GzwxtjDao dao;

	@Override
	protected BaseDao<GzwxtjEntity> getDao() {
		return dao;
	}
}