package com.itfreer.gzaqbw.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzaqbw.dao.GzaqbwDao;
import com.itfreer.gzaqbw.entity.GzaqbwEntity;
import com.itfreer.gzaqbw.service.GzaqbwService;

/**
 * 定义安全保卫实现类
 */
@Transactional(readOnly = false)
@Component("GzaqbwServiceImp")
public class GzaqbwServiceImp extends DictionaryConvertServiceImp<GzaqbwEntity> implements GzaqbwService {

	@Autowired
	private GzaqbwDao dao;

	@Override
	protected BaseDao<GzaqbwEntity> getDao() {
		return dao;
	}
}