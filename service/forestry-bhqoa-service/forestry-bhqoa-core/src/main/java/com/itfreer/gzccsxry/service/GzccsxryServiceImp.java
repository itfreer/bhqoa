package com.itfreer.gzccsxry.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzccsxry.dao.GzccsxryDao;
import com.itfreer.gzccsxry.entity.GzccsxryEntity;
import com.itfreer.gzccsxry.service.GzccsxryService;

/**
 * 定义出差随行人员实现类
 */
@Component("GzccsxryServiceImp")
public class GzccsxryServiceImp extends DictionaryConvertServiceImp<GzccsxryEntity> implements GzccsxryService {

	@Autowired
	private GzccsxryDao dao;

	@Override
	protected BaseDao<GzccsxryEntity> getDao() {
		return dao;
	}
}