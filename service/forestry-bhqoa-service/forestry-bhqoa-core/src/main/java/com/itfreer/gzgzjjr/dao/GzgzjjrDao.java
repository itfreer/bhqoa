package com.itfreer.gzgzjjr.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzgzjjr.entity.GzgzjjrEntity;

/**
 * 定义工作节假日接口
 */
public interface GzgzjjrDao extends BaseDao<GzgzjjrEntity> {
	
}