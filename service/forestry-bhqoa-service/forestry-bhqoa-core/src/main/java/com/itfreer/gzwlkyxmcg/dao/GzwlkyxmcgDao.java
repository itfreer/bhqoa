package com.itfreer.gzwlkyxmcg.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzwlkyxmcg.entity.GzwlkyxmcgEntity;

/**
 * 定义外来科研项目成果接口
 */
public interface GzwlkyxmcgDao extends BaseDao<GzwlkyxmcgEntity> {
	
}