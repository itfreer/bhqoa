package com.itfreer.gzclda.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzclda.entity.GzcldaEntity;

/**
 * 定义车辆档案接口
 */
public interface GzcldaDao extends BaseDao<GzcldaEntity> {
	
}