package com.itfreer.gzccsxry.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzccsxry.entity.GzccsxryEntity;

/**
 * 定义出差随行人员接口
 */
public interface GzccsxryDao extends BaseDao<GzccsxryEntity> {
	
}