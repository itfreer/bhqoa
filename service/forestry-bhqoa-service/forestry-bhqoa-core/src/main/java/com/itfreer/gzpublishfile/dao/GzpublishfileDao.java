package com.itfreer.gzpublishfile.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzpublishfile.entity.GzpublishfileEntity;

/**
 * 定义发文管理接口
 */
public interface GzpublishfileDao extends BaseDao<GzpublishfileEntity> {
	
}