package com.itfreer.gzyzgl.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzyzgl.entity.GzyzglEntity;

/**
 * 定义用章管理接口
 */
public interface GzyzglDao extends BaseDao<GzyzglEntity> {
	
}