package com.itfreer.gzsqxmtjview.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzsqxmtjview.entity.GzsqxmtjviewEntity;

/**
 * 定义社区项目统计(视图)接口
 */
public interface GzsqxmtjviewDao extends BaseDao<GzsqxmtjviewEntity> {
	
}