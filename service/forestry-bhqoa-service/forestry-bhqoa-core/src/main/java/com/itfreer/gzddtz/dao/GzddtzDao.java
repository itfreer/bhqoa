package com.itfreer.gzddtz.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzddtz.entity.GzddtzEntity;

/**
 * 定义调度台账接口
 */
public interface GzddtzDao extends BaseDao<GzddtzEntity> {
	
}