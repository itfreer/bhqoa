package com.itfreer.gzccgl.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.flow.imp.BaseWorkFlowServiceImp;
import com.itfreer.bpm.messager.BpmMessage;
import com.itfreer.bpm.option.para.OptionInfo;
import com.itfreer.bpm.power.para.UserInfo;
import com.itfreer.form.api.BaseDao;
import com.itfreer.gzccgl.dao.GzccglDao;
import com.itfreer.gzccgl.entity.GzccglEntity;

/**
 * 定义出差管理实现类
 */
@Component("GzccglServiceImp")
public class GzccglServiceImp extends BaseWorkFlowServiceImp<GzccglEntity> implements GzccglService {

	@Autowired
	private GzccglDao dao;

	@Override
	protected BaseDao<GzccglEntity> getDao() {
		return dao;
	}
	@Override
	public BpmMessage signTask(OptionInfo oInfo,GzccglEntity entity,UserInfo user) {
		if(oInfo.getNextTask()==null  || oInfo.getNextTask().size()<1) {
			entity.setSpzt("0");
		}else {
			entity.setSpzt("2");
		}
		return super.signTask(oInfo, entity, user);
	}
	
	@Override
	public BpmMessage disposal(OptionInfo optInfo,GzccglEntity entity,UserInfo user) {
		entity.setSpzt("0");
		return super.disposal(optInfo, entity, user);
	}
	
	@Override
	public BpmMessage userDispose(OptionInfo optInfo,GzccglEntity entity,UserInfo user) {
		entity.setSpzt("0");
		return super.userDispose(optInfo, entity, user);
	}
	
	@Override
	public BpmMessage processSave(OptionInfo oInfo,GzccglEntity entity,UserInfo user) {
		if(entity!=null && entity.getSexeid()!=null && "".equals(entity.getSexeid())) {
			
		}else if(entity!=null) {
			entity.setSpzt("1");
		}
		return super.processSave(oInfo, entity, user);
	}
	
	/**
	 * 项目激活
	 */
	@Override
	public BpmMessage easter(OptionInfo optInfo,GzccglEntity entity,UserInfo user) {
		entity.setSpzt("2");
		return super.easter(optInfo, entity, user);
	}
}