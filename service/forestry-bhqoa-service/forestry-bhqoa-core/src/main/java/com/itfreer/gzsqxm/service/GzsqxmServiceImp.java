package com.itfreer.gzsqxm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzsqxm.dao.GzsqxmDao;
import com.itfreer.gzsqxm.entity.GzsqxmEntity;
import com.itfreer.gzsqxm.service.GzsqxmService;

/**
 * 定义社区项目申请/台账实现类
 */
@Component("GzsqxmServiceImp")
public class GzsqxmServiceImp extends DictionaryConvertServiceImp<GzsqxmEntity> implements GzsqxmService {

	@Autowired
	private GzsqxmDao dao;

	@Override
	protected BaseDao<GzsqxmEntity> getDao() {
		return dao;
	}
}