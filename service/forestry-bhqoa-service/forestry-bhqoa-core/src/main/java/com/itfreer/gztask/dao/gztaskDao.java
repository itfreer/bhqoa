package com.itfreer.gztask.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gztask.entity.gztaskEntity;

/**
 * 定义任务安排接口
 */
public interface gztaskDao extends BaseDao<gztaskEntity> {
	
}