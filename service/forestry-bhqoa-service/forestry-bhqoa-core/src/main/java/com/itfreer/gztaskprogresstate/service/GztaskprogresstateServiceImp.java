package com.itfreer.gztaskprogresstate.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gztaskprogresstate.dao.GztaskprogresstateDao;
import com.itfreer.gztaskprogresstate.entity.GztaskprogresstateEntity;
import com.itfreer.gztaskprogresstate.service.GztaskprogresstateService;

/**
 * 定义任务监测实现类
 */
@Component("GztaskprogresstateServiceImp")
public class GztaskprogresstateServiceImp extends DictionaryConvertServiceImp<GztaskprogresstateEntity> implements GztaskprogresstateService {

	@Autowired
	private GztaskprogresstateDao dao;

	@Override
	protected BaseDao<GztaskprogresstateEntity> getDao() {
		return dao;
	}
}