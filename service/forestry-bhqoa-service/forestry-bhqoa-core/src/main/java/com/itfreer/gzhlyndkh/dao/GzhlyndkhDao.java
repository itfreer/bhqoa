package com.itfreer.gzhlyndkh.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzhlyndkh.entity.GzhlyndkhEntity;

/**
 * 定义护林员年度考核接口
 */
public interface GzhlyndkhDao extends BaseDao<GzhlyndkhEntity> {
	
}