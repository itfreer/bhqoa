package com.itfreer.gzykcz.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzykcz.dao.GzykczDao;
import com.itfreer.gzykcz.entity.GzykczEntity;
import com.itfreer.gzykcz.service.GzykczService;

/**
 * 定义油卡充值实现类
 */
@Component("GzykczServiceImp")
public class GzykczServiceImp extends DictionaryConvertServiceImp<GzykczEntity> implements GzykczService {

	@Autowired
	private GzykczDao dao;

	@Override
	protected BaseDao<GzykczEntity> getDao() {
		return dao;
	}
}