package com.itfreer.gzclsq.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzclsq.entity.GzclsqEntity;

/**
 * 定义车辆使用接口
 */
public interface GzclsqDao extends BaseDao<GzclsqEntity> {
	
}