package com.itfreer.vkqtj.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.vkqtj.dao.VkqtjDao;
import com.itfreer.vkqtj.entity.VkqtjEntity;
import com.itfreer.vkqtj.service.VkqtjService;

/**
 * 定义考勤统计实现类
 */
@Component("VkqtjServiceImp")
public class VkqtjServiceImp extends DictionaryConvertServiceImp<VkqtjEntity> implements VkqtjService {

	@Autowired
	private VkqtjDao dao;

	@Override
	protected BaseDao<VkqtjEntity> getDao() {
		return dao;
	}
}