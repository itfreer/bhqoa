package com.itfreer.appupdatenet.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.appupdatenet.entity.AppupdatenetEntity;

/**
 * 定义APP更新接口
 */
public interface AppupdatenetDao extends BaseDao<AppupdatenetEntity> {
	
}