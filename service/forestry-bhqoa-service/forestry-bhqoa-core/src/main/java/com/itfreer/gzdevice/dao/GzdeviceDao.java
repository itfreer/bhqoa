package com.itfreer.gzdevice.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzdevice.entity.GzdeviceEntity;

/**
 * 定义设备接口
 */
public interface GzdeviceDao extends BaseDao<GzdeviceEntity> {
	
}