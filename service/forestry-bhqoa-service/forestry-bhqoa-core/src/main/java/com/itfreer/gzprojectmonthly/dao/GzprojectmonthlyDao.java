package com.itfreer.gzprojectmonthly.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzprojectmonthly.entity.GzprojectmonthlyEntity;

/**
 * 定义项目月报接口
 */
public interface GzprojectmonthlyDao extends BaseDao<GzprojectmonthlyEntity> {
	
}