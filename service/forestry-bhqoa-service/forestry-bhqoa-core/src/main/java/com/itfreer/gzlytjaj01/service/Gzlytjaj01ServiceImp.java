package com.itfreer.gzlytjaj01.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzlytjaj01.dao.Gzlytjaj01Dao;
import com.itfreer.gzlytjaj01.entity.Gzlytjaj01Entity;
import com.itfreer.gzlytjaj01.service.Gzlytjaj01Service;

/**
 * 定义林业统计案件实现类
 */
@Component("Gzlytjaj01ServiceImp")
public class Gzlytjaj01ServiceImp extends DictionaryConvertServiceImp<Gzlytjaj01Entity> implements Gzlytjaj01Service {

	@Autowired
	private Gzlytjaj01Dao dao;

	@Override
	protected BaseDao<Gzlytjaj01Entity> getDao() {
		return dao;
	}
}