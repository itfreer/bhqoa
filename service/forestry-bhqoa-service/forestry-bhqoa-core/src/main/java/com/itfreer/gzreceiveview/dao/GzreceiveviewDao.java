package com.itfreer.gzreceiveview.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzreceiveview.entity.GzreceiveviewEntity;

/**
 * 定义收文管理接口
 */
public interface GzreceiveviewDao extends BaseDao<GzreceiveviewEntity> {
	GzreceiveviewEntity update(GzreceiveviewEntity entity);
}