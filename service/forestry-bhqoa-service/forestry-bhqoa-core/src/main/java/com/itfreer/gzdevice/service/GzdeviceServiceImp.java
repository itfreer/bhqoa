package com.itfreer.gzdevice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzdevice.dao.GzdeviceDao;
import com.itfreer.gzdevice.entity.GzdeviceEntity;
import com.itfreer.gzdevice.service.GzdeviceService;

/**
 * 定义设备实现类
 */
@Component("GzdeviceServiceImp")
public class GzdeviceServiceImp extends DictionaryConvertServiceImp<GzdeviceEntity> implements GzdeviceService {

	@Autowired
	private GzdeviceDao dao;

	@Override
	protected BaseDao<GzdeviceEntity> getDao() {
		return dao;
	}
}