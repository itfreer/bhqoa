package com.itfreer.gzlytjaj01.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzlytjaj01.entity.Gzlytjaj01Entity;

/**
 * 定义林业统计案件接口
 */
public interface Gzlytjaj01Dao extends BaseDao<Gzlytjaj01Entity> {
	
}