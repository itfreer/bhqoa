package com.itfreer.gzhlyjdkh.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzhlyjdkh.dao.GzhlyjdkhDao;
import com.itfreer.gzhlyjdkh.entity.GzhlyjdkhEntity;
import com.itfreer.gzhlyjdkh.service.GzhlyjdkhService;

/**
 * 定义护林员季度考核实现类
 */
@Component("GzhlyjdkhServiceImp")
public class GzhlyjdkhServiceImp extends DictionaryConvertServiceImp<GzhlyjdkhEntity> implements GzhlyjdkhService {

	@Autowired
	private GzhlyjdkhDao dao;

	@Override
	protected BaseDao<GzhlyjdkhEntity> getDao() {
		return dao;
	}
}