package com.itfreer.gzqdglqddd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzqdglqddd.dao.GzqdglqdddDao;
import com.itfreer.gzqdglqddd.entity.GzqdglqdddEntity;
import com.itfreer.gzqdglqddd.service.GzqdglqdddService;

/**
 * 定义签到地点实现类
 */
@Component("GzqdglqdddServiceImp")
public class GzqdglqdddServiceImp extends DictionaryConvertServiceImp<GzqdglqdddEntity> implements GzqdglqdddService {

	@Autowired
	private GzqdglqdddDao dao;

	@Override
	protected BaseDao<GzqdglqdddEntity> getDao() {
		return dao;
	}
}