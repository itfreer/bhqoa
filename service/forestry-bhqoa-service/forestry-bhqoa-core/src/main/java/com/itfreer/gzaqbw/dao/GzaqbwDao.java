package com.itfreer.gzaqbw.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzaqbw.entity.GzaqbwEntity;

/**
 * 定义安全保卫接口
 */
public interface GzaqbwDao extends BaseDao<GzaqbwEntity> {
	
}