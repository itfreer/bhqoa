package com.itfreer.workflow;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.itfreer.bpm.flow.api.IUserNextTask;
import com.itfreer.bpm.option.para.NextTaskInfo;
import com.itfreer.bpm.option.para.NextTaskType;
import com.itfreer.bpm.option.para.TaskInfo;
import com.itfreer.gzqjgl.entity.GzqjglEntity;
import com.itfreer.gzyzgl.entity.GzyzglEntity;
/**
 * 用户请假后台逻辑过滤器
 * @author gj
 *
 */
@Component
public class GzUserTask implements IUserNextTask{

	@Override
	public NextTaskInfo getNextTask(String sexeid,String bpmkey,
			String taskid,String condition,
			NextTaskInfo nextTaskInfo,Object entity) {
		//科员请假流程
		if("qjgl".equals(bpmkey) && "提交".equals(condition)) {
			GzqjglEntity gjglqEntity=(GzqjglEntity)entity;
			Double qjts=gjglqEntity.getQjts();
			String userTask=null;
			if("usertask1".equals(taskid)) {
				List<TaskInfo> lst=nextTaskInfo.getNextTask();
				if(qjts<3) {
					userTask="usertask2";
				}else {
					userTask="usertask3";
				}
				
				for(TaskInfo itask:lst) {
					if(userTask.equals(itask.getTaskdefinedid())) {
						List<TaskInfo> nlst=new ArrayList<TaskInfo>();
						nlst.add(itask);
						itask.setAllUserSelect();
						nextTaskInfo.setNextTask(nlst);
						nextTaskInfo.setNextTaskType(NextTaskType.CommonTask);
						break;
					}
				}
				
				
			}
			
			if("usertask4".equals(taskid)) {
				List<TaskInfo> lst=nextTaskInfo.getNextTask();
				if(qjts<=7) {
					userTask="U9";
					//结束时，不需要节点了
					nextTaskInfo.setNextTask(new ArrayList<TaskInfo>());
				}else {
					userTask="usertask5";
					
					for(TaskInfo itask:lst) {
						if(userTask.equals(itask.getTaskdefinedid())) {
							List<TaskInfo> nlst=new ArrayList<TaskInfo>();
							nlst.add(itask);
							itask.setAllUserSelect();
							nextTaskInfo.setNextTask(nlst);
							nextTaskInfo.setNextTaskType(NextTaskType.CommonTask);
							break;
						}
					}
				}
			}
			/*if("usertask4".equals(taskid)){
				List<TaskInfo> lst=nextTaskInfo.getNextTask();
				if(qjts<=7) {
					userTask="usertask4";
				}else {
					userTask="usertask5";
				}
				
				for(TaskInfo itask:lst) {
					if(userTask.equals(itask.getTaskdefinedid())) {
						List<TaskInfo> nlst=new ArrayList<TaskInfo>();
						nlst.add(itask);
						itask.setAllUserSelect();
						nextTaskInfo.setNextTask(nlst);
						nextTaskInfo.setNextTaskType(NextTaskType.CommonTask);
						break;
					}
				}
			}*/
		}
		
		//科技干部请假流程管理
		if("qjgl_kjgb".equals(bpmkey) && "提交".equals(condition)) {
			GzqjglEntity gjglqEntity=(GzqjglEntity)entity;
			Double qjts=gjglqEntity.getQjts();
			String userTask=null;
			if("usertask4".equals(taskid)) {
				List<TaskInfo> lst=nextTaskInfo.getNextTask();
				if(qjts<=7) {
					userTask="U9";
					//结束时，不需要节点了
					nextTaskInfo.setNextTask(new ArrayList<TaskInfo>());
				}else {
					userTask="usertask5";
					
					for(TaskInfo itask:lst) {
						if(userTask.equals(itask.getTaskdefinedid())) {
							List<TaskInfo> nlst=new ArrayList<TaskInfo>();
							nlst.add(itask);
							itask.setAllUserSelect();
							nextTaskInfo.setNextTask(nlst);
							nextTaskInfo.setNextTaskType(NextTaskType.CommonTask);
							break;
						}
					}
				}
			}
		}
		
		//科员、科长用章流程
		if("yzgl".equals(bpmkey) && "提交".equals(condition)) {
			GzyzglEntity gjglqEntity=(GzyzglEntity)entity;
			Boolean qjts=gjglqEntity.getSfbhfwdbz();
			String userTask=null;
			if("usertask2".equals(taskid)) {
				List<TaskInfo> lst=nextTaskInfo.getNextTask();
				if(qjts!=null && qjts) {
					userTask="usertask4";
				}else {
					userTask="usertask3";
				}
				
				for(TaskInfo itask:lst) {
					if(userTask.equals(itask.getTaskdefinedid())) {
						List<TaskInfo> nlst=new ArrayList<TaskInfo>();
						nlst.add(itask);
						itask.setAllUserSelect();
						nextTaskInfo.setNextTask(nlst);
						nextTaskInfo.setNextTaskType(NextTaskType.CommonTask);
						break;
					}
				}
			}
		}
		
		//主任用章流程
		if("yzgl_zrjbgb".equals(bpmkey) && "提交".equals(condition)) {
			GzyzglEntity gjglqEntity=(GzyzglEntity)entity;
			Boolean qjts=gjglqEntity.getSfbhfwdbz();
			String userTask=null;
			if("usertask1".equals(taskid)) {
				List<TaskInfo> lst=nextTaskInfo.getNextTask();
				if(qjts!=null && qjts) {
					userTask="usertask4";
				}else {
					userTask="usertask3";
				}
				
				for(TaskInfo itask:lst) {
					if(userTask.equals(itask.getTaskdefinedid())) {
						List<TaskInfo> nlst=new ArrayList<TaskInfo>();
						nlst.add(itask);
						itask.setAllUserSelect();
						nextTaskInfo.setNextTask(nlst);
						nextTaskInfo.setNextTaskType(NextTaskType.CommonTask);
						break;
					}
				}
			}
		}
		return nextTaskInfo;
	}

}
