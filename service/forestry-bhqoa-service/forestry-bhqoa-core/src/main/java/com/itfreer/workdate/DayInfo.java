package com.itfreer.workdate;

public class DayInfo {

	/**
	 * 上午
	 */
	private WorkInfo morning = new WorkInfo();
	/**
	 * 下午
	 */
	private WorkInfo afternoon = new WorkInfo();

	/**
	 * 上午
	 */
	public WorkInfo getMorning() {
		return morning;
	}

	/**
	 * 下午
	 */
	public WorkInfo getAfternoon() {
		return afternoon;
	}
}
