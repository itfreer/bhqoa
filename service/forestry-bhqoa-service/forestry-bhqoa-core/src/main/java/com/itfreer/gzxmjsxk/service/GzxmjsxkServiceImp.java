package com.itfreer.gzxmjsxk.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzxmjsxk.dao.GzxmjsxkDao;
import com.itfreer.gzxmjsxk.entity.GzxmjsxkEntity;
import com.itfreer.gzxmjsxk.service.GzxmjsxkService;

/**
 * 定义项目建设许可实现类
 */
@Component("GzxmjsxkServiceImp")
public class GzxmjsxkServiceImp extends DictionaryConvertServiceImp<GzxmjsxkEntity> implements GzxmjsxkService {

	@Autowired
	private GzxmjsxkDao dao;

	@Override
	protected BaseDao<GzxmjsxkEntity> getDao() {
		return dao;
	}
}