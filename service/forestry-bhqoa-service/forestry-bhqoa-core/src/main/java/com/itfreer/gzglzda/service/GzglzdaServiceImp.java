package com.itfreer.gzglzda.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzglzda.dao.GzglzdaDao;
import com.itfreer.gzglzda.entity.GzglzdaEntity;
import com.itfreer.gzglzda.service.GzglzdaService;

/**
 * 定义管理站档案实现类
 */
@Component("GzglzdaServiceImp")
public class GzglzdaServiceImp extends DictionaryConvertServiceImp<GzglzdaEntity> implements GzglzdaService {

	@Autowired
	private GzglzdaDao dao;

	@Override
	protected BaseDao<GzglzdaEntity> getDao() {
		return dao;
	}
}