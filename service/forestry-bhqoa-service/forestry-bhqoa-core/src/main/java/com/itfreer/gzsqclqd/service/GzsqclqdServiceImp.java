package com.itfreer.gzsqclqd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzsqclqd.dao.GzsqclqdDao;
import com.itfreer.gzsqclqd.entity.GzsqclqdEntity;
import com.itfreer.gzsqclqd.service.GzsqclqdService;

/**
 * 定义申请材料清单实现类
 */
@Component("GzsqclqdServiceImp")
public class GzsqclqdServiceImp extends DictionaryConvertServiceImp<GzsqclqdEntity> implements GzsqclqdService {

	@Autowired
	private GzsqclqdDao dao;

	@Override
	protected BaseDao<GzsqclqdEntity> getDao() {
		return dao;
	}
}