package com.itfreer.gzkyxmcg.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzkyxmcg.dao.GzkyxmcgDao;
import com.itfreer.gzkyxmcg.entity.GzkyxmcgEntity;
import com.itfreer.gzkyxmcg.service.GzkyxmcgService;

/**
 * 定义科研项目成果实现类
 */
@Component("GzkyxmcgServiceImp")
public class GzkyxmcgServiceImp extends DictionaryConvertServiceImp<GzkyxmcgEntity> implements GzkyxmcgService {

	@Autowired
	private GzkyxmcgDao dao;

	@Override
	protected BaseDao<GzkyxmcgEntity> getDao() {
		return dao;
	}
}