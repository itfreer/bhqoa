package com.itfreer.gzdevicemaintain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzdevicemaintain.dao.GzdevicemaintainDao;
import com.itfreer.gzdevicemaintain.entity.GzdevicemaintainEntity;
import com.itfreer.gzdevicemaintain.service.GzdevicemaintainService;

/**
 * 定义设备维修记录实现类
 */
@Component("GzdevicemaintainServiceImp")
public class GzdevicemaintainServiceImp extends DictionaryConvertServiceImp<GzdevicemaintainEntity> implements GzdevicemaintainService {

	@Autowired
	private GzdevicemaintainDao dao;

	@Override
	protected BaseDao<GzdevicemaintainEntity> getDao() {
		return dao;
	}
}