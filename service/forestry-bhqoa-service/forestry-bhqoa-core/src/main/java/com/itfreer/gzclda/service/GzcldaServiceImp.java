package com.itfreer.gzclda.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzclda.dao.GzcldaDao;
import com.itfreer.gzclda.entity.GzcldaEntity;
import com.itfreer.gzclda.service.GzcldaService;

/**
 * 定义车辆档案实现类
 */
@Component("GzcldaServiceImp")
public class GzcldaServiceImp extends DictionaryConvertServiceImp<GzcldaEntity> implements GzcldaService {

	@Autowired
	private GzcldaDao dao;

	@Override
	protected BaseDao<GzcldaEntity> getDao() {
		return dao;
	}
}