package com.itfreer.gzplanpersonnel.dao;

import com.itfreer.form.api.BaseDao;
import com.itfreer.gzplanpersonnel.entity.GzplanpersonnelEntity;

/**
 * 定义项目规划人员接口
 */
public interface GzplanpersonnelDao extends BaseDao<GzplanpersonnelEntity> {
	
}