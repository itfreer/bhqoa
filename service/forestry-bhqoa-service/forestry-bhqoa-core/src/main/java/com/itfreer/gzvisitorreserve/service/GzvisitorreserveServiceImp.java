package com.itfreer.gzvisitorreserve.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzvisitorreserve.dao.GzvisitorreserveDao;
import com.itfreer.gzvisitorreserve.entity.GzvisitorreserveEntity;
import com.itfreer.gzvisitorreserve.service.GzvisitorreserveService;

/**
 * 定义访客预约管理实现类
 */
@Component("GzvisitorreserveServiceImp")
public class GzvisitorreserveServiceImp extends DictionaryConvertServiceImp<GzvisitorreserveEntity> implements GzvisitorreserveService {

	@Autowired
	private GzvisitorreserveDao dao;

	@Override
	protected BaseDao<GzvisitorreserveEntity> getDao() {
		return dao;
	}
}