package com.itfreer.gzprojectinfo.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.flow.api.IBpmWorkFlowService;
import com.itfreer.bpm.flow.api.IProofReadUser;
import com.itfreer.bpm.flow.imp.BaseWorkFlowServiceImp;
import com.itfreer.bpm.history.entity.BpmHistoryEntity;
import com.itfreer.bpm.messager.BpmMessage;
import com.itfreer.bpm.option.para.OptionInfo;
import com.itfreer.bpm.power.para.UserInfo;
import com.itfreer.form.api.BaseDao;
import com.itfreer.gzplanpersonnel.entity.GzplanpersonnelEntity;
import com.itfreer.gzprojectinfo.dao.GzprojectinfoDao;
import com.itfreer.gzprojectinfo.entity.GzprojectinfoEntity;

/**
 * 定义项目立项管理实现类
 */
@Component("GzprojectinfoServiceImp")
public class GzprojectinfoServiceImp extends BaseWorkFlowServiceImp<GzprojectinfoEntity> implements GzprojectinfoService {
	
	/**
	 * 流程驱动服务类
	 */
	@Autowired
	private IBpmWorkFlowService<GzprojectinfoEntity> bpmFlowService;
	
	@Autowired(required=false)
	private IProofReadUser proofRead;

	@Autowired
	private GzprojectinfoDao dao;

	@Override
	protected BaseDao<GzprojectinfoEntity> getDao() {
		return dao;
	}
	
	/**
	 * 流程保存操作
	 * @param optionInfo 流程操作信息（包括用户、节点操作）
	 * @param entity
	 */
	public BpmMessage processSave(OptionInfo oInfo,GzprojectinfoEntity entity,UserInfo user) {
		//用户校验
		if(proofRead!=null) {
			proofRead.ProofReadUser(user);
		}
		BpmMessage message= bpmFlowService.processSave(oInfo, entity, user);
		String id=entity.getId();
		if(id!=null && !"".equals(id)) {
			GzprojectinfoEntity entity2=super.getEntity(id);
			if(entity2!=null) {
				super.update(entity);
			}else {
				entity.setSxmzt("01");//单纯保存数据时，项目状态为草稿
				super.add(entity);
			}
		}
		message.setEntity(entity);
		return message;
	}
	
	/**
	 * 项目提交操作
	 * @param oInfo 操作信息
	 * @param entity 项目实体
	 * @param user 用户
	 * @return
	 */
	@Override
	public BpmMessage signTask(OptionInfo oInfo,GzprojectinfoEntity entity,UserInfo user) {
		//用户校验
		if(proofRead!=null) {
			proofRead.ProofReadUser(user);
		}
		BpmMessage message= bpmFlowService.signTask(oInfo, entity, user);
		if(oInfo.getNextTask()!=null && oInfo.getNextTask().size()>0) {
			entity.setSxmzt("03");//流程执行的过程中为审核中
		}else {
			entity.setSxmzt("04");//流程执行结束时，为进行中
		}
		
		Set<GzplanpersonnelEntity> lst= entity.getGzplanpersonnel();
		if(lst!=null && lst.size()>0) {
			for(GzplanpersonnelEntity ientity:lst) {
				ientity.setSxmmc(entity.getSxmmc());
				//ientity.setSxmbh(entity);
			}
		}
		super.update(entity);
		message.setEntity(entity);
		return message;
		
	}
	
	/**
	 * 项目退件
	 */
	@Override
	public BpmMessage disposal(OptionInfo optInfo,GzprojectinfoEntity entity,UserInfo user) {
		//用户校验
		if(proofRead!=null) {
			proofRead.ProofReadUser(user);
		}
		BpmMessage message= bpmFlowService.disposal(optInfo, entity, user);
		entity.setSxmzt("06");//项目退件时，项目状态为结束
		super.update(entity);
		message.setEntity(entity);
		return message;
	}
	
	/**
	 * 项目退件
	 */
	@Override
	public BpmMessage userDispose(OptionInfo optInfo,GzprojectinfoEntity entity,UserInfo user) {
		//用户校验
		if(proofRead!=null) {
			proofRead.ProofReadUser(user);
		}
		BpmMessage message= bpmFlowService.userDispose(optInfo, entity, user);
		entity.setSxmzt("06");//项目退件时，项目状态为结束
		super.update(entity);
		message.setEntity(entity);
		return message;
	}
	
	/**
	 * 项目退件
	 */
	@Override
	public BpmMessage easter(OptionInfo optInfo,GzprojectinfoEntity entity,UserInfo user) {
		//用户校验
		if(proofRead!=null) {
			proofRead.ProofReadUser(user);
		}
		BpmMessage message= bpmFlowService.easter(optInfo, entity, user);
		entity.setSxmzt("03");//项目复原时，状态修改为待审核
		super.update(entity);
		message.setEntity(entity);
		return message;
	}
	
	@Override
	public OptionInfo getNextTask(String definedid, GzprojectinfoEntity entity,List<BpmHistoryEntity> editHistory,UserInfo user) {
		//用户校验
		if(proofRead!=null) {
			proofRead.ProofReadUser(user);
		}
		
		OptionInfo info= bpmFlowService.getNextTask(definedid, entity,editHistory, user);
		//提交到下一个节点时，当前节点数据
		if(entity!=null) {
			String id=entity.getId();
			//T sjkEntity=this.getDao().getEntity(id);
			GzprojectinfoEntity sjkEntity=super.getEntity(id);
			if(sjkEntity==null) {
				//this.getDao().add(entity);
				entity.setSxmzt("03");//设置项目状态为审核中
				super.add(entity);
			}else {
				//this.getDao().update(entity);
				super.update(entity);
			}
			info.setEntity(entity);
		}
		return info;
	}
}