package com.itfreer.gzhlyndkh.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseDao;
import com.itfreer.form.api.imp.DictionaryConvertServiceImp;
import com.itfreer.gzhlyndkh.dao.GzhlyndkhDao;
import com.itfreer.gzhlyndkh.entity.GzhlyndkhEntity;
import com.itfreer.gzhlyndkh.service.GzhlyndkhService;

/**
 * 定义护林员年度考核实现类
 */
@Component("GzhlyndkhServiceImp")
public class GzhlyndkhServiceImp extends DictionaryConvertServiceImp<GzhlyndkhEntity> implements GzhlyndkhService {

	@Autowired
	private GzhlyndkhDao dao;

	@Override
	protected BaseDao<GzhlyndkhEntity> getDao() {
		return dao;
	}
}