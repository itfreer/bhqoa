package com.itfreer.demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.itfreer.workdate.CalculateServerImp;
import com.itfreer.workdate.DayInfo;

public class CalculateHoursTest {

	@Test
	public void test1() throws ParseException {
		String beginTime = "2019-5-1";
		String endTime = "2019-6-4";

		CalculateServerImp ch = new CalculateServerImp();
		List<String> result = ch.workDays(beginTime, endTime);
		System.out.println(result.size());

		result = ch.restDays(beginTime, endTime);
		System.out.println(result.size());

		beginTime = "2019-5";
		result = ch.workDays(beginTime);
		System.out.println(result.size());

		result = ch.restDays(beginTime);
		System.out.println(result.size());
	}

	@Test
	public void test2() throws ParseException {
		CalculateServerImp ch = new CalculateServerImp();
		// 一天
		String beginTime = "2019-6-1 9:00:00";
		String endTime = "2019-6-1 18:00:00";
		Map<String, DayInfo> result = new HashMap<>();
		float count = ch.workDays(beginTime, endTime, result, "请假");
		System.out.println(count);
		// 上午
		beginTime = "2019-6-1 9:30:00";
		endTime = "2019-6-1 12:30:00";
		result = new HashMap<>();
		count = ch.workDays(beginTime, endTime, result, "请假");
		System.out.println(count);
		
		beginTime = "2019-6-1 9:30:00";
		endTime = "2019-6-2 12:30:00";
		result = new HashMap<>();
		count = ch.workDays(beginTime, endTime, result, "请假");
		System.out.println(count);
		
		// 下午
		beginTime = "2019-5-1 12:30:00";
		endTime = "2019-5-1 18:30:00";
		result = new HashMap<>();
		count = ch.workDays(beginTime, endTime, result, "请假");
		System.out.println(count);
		
		beginTime = "2019-5-1 12:30:00";
		endTime = "2019-5-2 18:30:00";
		result = new HashMap<>();
		count = ch.workDays(beginTime, endTime, result, "请假");
		System.out.println(count);
		
		beginTime = "2019-5-1 9:00:00";
		endTime = "2019-5-2 18:00:00";
		result = new HashMap<>();
		count = ch.workDays(beginTime, endTime, result, "请假");
		System.out.println(count);
		
		beginTime = "2019-5-1 9:00:00";
		endTime = "2019-5-4 18:00:00";
		result = new HashMap<>();
		count = ch.workDays(beginTime, endTime, result, "请假");
		System.out.println(count);
	}
	
	public static String dateToStr(Date date,int type) {
		String str = "";
		if(type==1) {
			str=new SimpleDateFormat("yyyy-MM-dd").format(date);
		}else if(type==2) {
			str=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
		}
		return str;
	}
	public static Date strToDate(String str,int type) {
		Date date= null;
		try {
			if(type==1) {
				date=new SimpleDateFormat("yyyy-MM-dd").parse(str);
			}else if(type==2) {
				date=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str);
			}
			
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	//获取正常上下班时间//时间固定
	public static Date getNormalTime(Date today,int type) {
		Date date = null;
		if(type==1) {//上班
			String day=dateToStr(today,1);
			day=day+" 9:00:00";
			date=strToDate(day,2);
		}else if(type==2) {//下班
			String day=dateToStr(today,1);
			day=day+" 18:00:00";
			date=strToDate(day,2);
		}
		return date;
	}
	
	@Test
	public void test3() throws ParseException {
		String endTime = "2019-5-4 8:50:00";
		String beginTime = dateToStr(getNormalTime(strToDate(endTime,2),2),2);
		Map<String, DayInfo> result = new HashMap<>();

		CalculateServerImp ch = new CalculateServerImp();
		ch.workTimes(endTime, beginTime, result);
		System.out.println(result);
		System.out.println(result.size());

		beginTime = "2019-5-4 9:50:00";
		endTime = "2019-5-4 18:50:00";
		result = new HashMap<>();
		ch.workTimes(beginTime, endTime, result);
		System.out.println(result);
		System.out.println(result.size());
		
		beginTime = "2019-5-4 9:50:00";
		endTime = "2019-5-4 17:50:00";
		result = new HashMap<>();
		ch.workTimes(beginTime, endTime, result);
		System.out.println(result.size());
		
		beginTime = "2019-5-4 8:50:00";
		endTime = "2019-5-4 12:30:00";
		result = new HashMap<>();
		ch.workTimes(beginTime, endTime, result);
		System.out.println(result.size());
		
		beginTime = "2019-5-4 12:50:00";
		endTime = "2019-5-4 18:30:00";
		result = new HashMap<>();
		ch.workTimes(beginTime, endTime, result);
		System.out.println(result.size());
		
		beginTime = "2019-5-4 12:50:00";
		endTime = "2019-5-4 17:30:00";
		result = new HashMap<>();
		ch.workTimes(beginTime, endTime, result);
		System.out.println(result.size());
	}
}
