package com.itfreer.gzcgtj.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzcgtj.entity.GzcgtjEntity;
import com.itfreer.gzcgtj.service.GzcgtjService;

/**
 * 定义采购统计Rest服务接口
 */
@Component
@Path("/gzcgtj/")
public class RestGzcgtjService extends BaseRestService<GzcgtjEntity> {
	
	@Autowired
	private GzcgtjService service;

	@Override
	protected BaseService<GzcgtjEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzcgtj";
	}
}
