package com.itfreer.gzdevicemaintain.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzdevicemaintain.entity.GzdevicemaintainEntity;
import com.itfreer.gzdevicemaintain.service.GzdevicemaintainService;

/**
 * 定义设备维修记录Rest服务接口
 */
@Component
@Path("/gzdevicemaintain/")
public class RestGzdevicemaintainService extends BaseRestService<GzdevicemaintainEntity> {
	
	@Autowired
	private GzdevicemaintainService service;

	@Override
	protected BaseService<GzdevicemaintainEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzdevicemaintain";
	}
}
