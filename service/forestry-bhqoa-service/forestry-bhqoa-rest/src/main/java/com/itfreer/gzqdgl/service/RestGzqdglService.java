package com.itfreer.gzqdgl.service;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;
import com.itfreer.gzgzrqdsj.entity.GzgzrqdsjEntity;
import com.itfreer.gzgzrqdsj.service.GzgzrqdsjService;
import com.itfreer.gzqdgl.entity.GzqdglEntity;
import com.itfreer.gzqdglqddd.entity.GzqdglqdddEntity;
import com.itfreer.gzqdglqddd.service.GzqdglqdddService;
import com.itfreer.jwt.JwtTicket;
import com.itfreer.jwt.JwtTicketFactory;
import com.itfreer.power.entity.user.VUserInfoEntity;
import com.itfreer.utils.json.JsonUtils;

/**
 * 定义签到管理Rest服务接口
 */
@Component
@Path("/gzqdgl/")
public class RestGzqdglService extends BaseRestService<GzqdglEntity> {
	private final static String s_user = "user";
	@Autowired
	private JwtTicketFactory jwtTicketFactory;
	@Autowired
	private GzqdglService service;
	@Autowired
	private GzgzrqdsjService gzgzrqdsj;
	@Autowired
	private GzqdglqdddService gzqdglqddd;

	@Override
	protected BaseService<GzqdglEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzqdgl";
	}
	
	@SuppressWarnings({ "unchecked" })
	@POST
	@Path("/qdgl")
	@Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public QdResultParam qdgl(@Context HttpServletRequest request,String data) {
		JwtTicket ticket = jwtTicketFactory.getTicket(request);
		VUserInfoEntity userEntity = ticket != null ? (VUserInfoEntity) ticket.getAttribute().get(s_user) : null;
		
		QdResultParam result=new QdResultParam();
		if(userEntity==null) {
			result.status=false;
			result.message="尚未登录，请先登录系统！";
			return result;
		}
		
		HashMap<String, Object> args = getRestParam(data);
		String method="";//创建或更新
		String type="";//签到或签退
		String latitude="";
		String longitude="";
		String address="";
		String accuracy="";
		String dirtype="";
		if(args!=null) {
			if (args.containsKey("type") && args.get("type") != null)
				type = args.get("type").toString();
			if (args.containsKey("method") && args.get("method") != null)
				method = args.get("method").toString();
			if (args.containsKey("latitude") && args.get("latitude") != null)
				latitude = args.get("latitude").toString();
			if (args.containsKey("longitude") && args.get("longitude") != null)
				longitude = args.get("longitude").toString();
			if (args.containsKey("address") && args.get("address") != null)
				address = args.get("address").toString();
			if (args.containsKey("accuracy") && args.get("accuracy") != null)
				accuracy = args.get("accuracy").toString();
			if (args.containsKey("dirtype") && args.get("dirtype") != null)
				dirtype = args.get("dirtype").toString();

		}
		
		
		String userid=userEntity.getUserid();
		
		Map<String,Object> where =new HashedMap();
		Date cTime=new Date();
		Calendar now = Calendar.getInstance();
		where.put("nandu:=", now.get(Calendar.YEAR));
		where.put("yuefen:=", (now.get(Calendar.MONTH) + 1));
		where.put("day:=", now.get(Calendar.DAY_OF_MONTH));
		
		where.put("kaoqrid:=", userid);
		
		Map<String,Object> signInfo = new HashMap<String,Object>();
		signInfo.put("cTime", cTime);//打卡的时间
		signInfo.put("latitude", latitude);//打卡的纬度
		signInfo.put("longitude", longitude);//打卡的经度
		signInfo.put("type", type);//签到还是签退
		
		//查询是否已有打卡记录
		List<GzqdglEntity> lst=this.service.getEntitys(null, where, null, 1, 1);
		try {
			signInfo=checkSign(signInfo,lst);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		Date swsbks=(Date) signInfo.get("swsbks");//本日上午上班开始时间7
		Date swsbjs=(Date) signInfo.get("swsbjs");//本日上午上班结束时间9
		Date swxbks=(Date) signInfo.get("swxbks");//本日上午下班开始时间12
		Date swxbjs=(Date) signInfo.get("swxbjs");//本日上午下班结束时间14
		Date xwsbks=(Date) signInfo.get("xwsbks");//本日下午上班开始时间13
		Date xwsbjs=(Date) signInfo.get("xwsbjs");//本日下午上班结束时间14
		Date xwxbks=(Date) signInfo.get("xwxbks");//本日下午下班开始时间18
//		Date xwxbjs=(Date) signInfo.get("xwxbjs");//本日下午下班结束时间23
		
		//测量距离key=1d39e98c02ac6bab17d2545f82d20d85，高德地图
		Map<String,Object> where1 =new HashedMap();
		if(userEntity.getDepartmentName()!=null && userEntity.getDepartmentName().indexOf("管理站")!=-1) {
			where1.put("kqdd:like", userEntity.getDepartmentName());
		}else {
			where1.put("kqdd:like", "管理局");
		}
		List<GzqdglqdddEntity> qlist=this.gzqdglqddd.getEntitys(null, where1, null, 1000, 1);
		
		long distance=-1l;
		String key="1d39e98c02ac6bab17d2545f82d20d85";
		String startLonLat=longitude+","+latitude;
		
		String sfkqfw="是";//考勤范围是否有异常
		String sfsjyc="是";//考勤时间是否有异常
		long qdistance=0l;
		String bz="无考勤地点数据！；";
		String dkbz="";
		if(qlist!=null && qlist.size()>0) {//获取到考勤地点数据
			for(GzqdglqdddEntity qddd: qlist) {
				String endLonLat=qddd.getKqzbx();
				if(qddd.getKqfw()==null || qddd.getKqfw()<0) {//如果无考勤范围数据，则将考勤范围设置为500米
					qddd.setKqfw(500);
				}
				if(longitude == null || "".equals(longitude)) {//未获取到定位坐标
					sfkqfw="是";
					bz="未获取到定位坐标！；";
				}else if(endLonLat == null || "".equals(endLonLat)) {//考勤地点中无坐标数据
					sfkqfw="是";
					bz="考勤地点设置中未录入考勤坐标，请按照文档《考勤地点设置时获取坐标方法》获取考勤坐标并录入！；";
				}else {//坐标数据正常
					try {
						distance=getDistance(startLonLat,endLonLat,key);
					} catch (IOException e) {
						e.printStackTrace();
						System.out.println("距离查询出错！");
					}
					if(distance>=0l) {//获取到了误差数据
						long kqfw=qddd.getKqfw();
						if(kqfw>=distance) {//误差在考勤范围内
							sfkqfw="否";
							bz="";
							break;
						}else {//误差超出考勤范围
							if(qdistance>distance) {
								qdistance=distance;
								sfkqfw="是";
								bz="考勤范围超出，考勤地点为"+qddd.getKqfw()+",误差为"+qdistance+"米。；";
							}
						}
					}
				}
			}
		}
//		String startLonLat="112.988151,28.111832";
//		String endLonLat="112.989187,28.110545";
		String sTime=new SimpleDateFormat("yyyy-MM-dd").format(cTime);
		if(lst!=null && lst.size()>0) {//若有打卡记录
			GzqdglEntity iEntity=lst.get(0);
			if(!"是".equals(iEntity.getSfkqfw())) {
				iEntity.setSfkqfw(sfkqfw);
			}
			result.type="create";
			if("update".equals(method)) {//更新打卡时间
				if("signin".equals(type)) {//签到
					//签到时间不更新
				}else {//签退
					if(cTime.before(xwsbjs)) {//上午下班打卡
						iEntity.setQtsj(cTime);
						//处理签退位置，说明问题
						iEntity.setOutlatitude(latitude);
						iEntity.setOutlongitude(longitude);
						iEntity.setOutaccuracy(accuracy);
						iEntity.setOutaddress(address);
						iEntity.setOutdirtype(dirtype);
						
						//考勤地点处理
						if(!"".equals(bz)) {//如果备注不为空，则添加备注到记录
							bz="上午签退（"+sTime+"）："+bz;
						}
						if(cTime.before(swxbjs) && cTime.after(xwsbks)) {//正常
							sfsjyc="否";
							result.status=true;
							result.message="已更新打卡，上午签退时间："+this.getDateFormate(cTime)+"！";
						}else if(cTime.before(xwsbks)){//早退
							dkbz="上午签退（"+sTime+"）：早退；";
							sfsjyc="是";
							result.status=true;
							result.type="opentext";
							result.message="已更新打卡，上午签退时间："+this.getDateFormate(cTime)+"！早退，是否填写备注？";
						}else if(cTime.after(swxbjs)) {//已过上午下班打卡时间
							result.status=false;
							result.message="打卡失败，上午签退时间已过！";
							return result;
						}
					}else {//下午下班打卡
						iEntity.setXwqtsj(cTime);
						//处理签退位置，说明问题
						iEntity.setXwoutlatitude(latitude);
						iEntity.setXwoutlongitude(longitude);
						iEntity.setXwoutaccuracy(accuracy);
						iEntity.setXwoutaddress(address);
						iEntity.setXwoutdirtype(dirtype);
						
						//考勤地点处理
						if(!"".equals(bz)) {//如果备注不为空，则添加备注到记录
							bz="下午签退："+bz;
						}
						if(cTime.after(xwxbks)) {//正常
							sfsjyc="否";
							result.status=true;
							result.message="已更新打卡，下午签退时间："+this.getDateFormate(cTime)+"！";
						}else if(cTime.before(xwxbks)){//早退
							dkbz="下午签退（"+sTime+"）：早退；";
							sfsjyc="是";
							result.status=true;
							result.type="opentext";
							result.message="已更新打卡，下午签退时间："+this.getDateFormate(cTime)+"！早退，是否填写备注？";
						}
					}
					if(!"是".equals(iEntity.getSfsjyc())) {
						iEntity.setSfsjyc(sfsjyc);
					}
					if(iEntity.getDkbz()!=null) {
						dkbz=iEntity.getDkbz()+dkbz;
					}
					if(iEntity.getBz()!=null) {
						bz=iEntity.getBz()+bz;
					}
					iEntity.setBz(bz);
					iEntity.setDkbz(dkbz);
				}
				
				iEntity.setCreatetime(new Date());
				this.service.update(iEntity);
				if("opentext".equals(result.type)) {
					result.data=iEntity;
				}
				return result;
			}
			//已有打卡记录处理TODO
			if("signin".equals(type)) {//签到
				if(cTime.before(swxbks)) {//上午上班打卡
					if(iEntity.getQdsj()!=null && !this.timeEquals(iEntity.getQdsj(), cTime)) {//已打卡不做处理
						result.status=true;
						result.type="create";
						result.message="已打卡，上午签到时间："+this.getDateFormate(iEntity.getQdsj())+"！";
						return result;
					}
					iEntity.setQdsj(cTime);//上午签到时间
					//处理上午上班签到位置，说明问题
					iEntity.setLatitude(latitude);
					iEntity.setLongitude(longitude);
					iEntity.setAccuracy(accuracy);
					iEntity.setAddress(address);
					iEntity.setDirtype(dirtype);
					//考勤地点处理
					if(!"".equals(bz)) {//如果备注不为空，则添加备注到记录
						bz="上午签到（"+sTime+"）："+bz;
					}
					if(cTime.before(swsbjs) && cTime.after(swsbks)) {//正常
						sfsjyc="否";
						result.status=true;
						result.message="打卡成功，上午签到时间："+this.getDateFormate(cTime)+"！";
					}else if(cTime.after(swsbjs)) {//迟到
						dkbz="上午签到（"+sTime+"）：迟到；";
						sfsjyc="是";
						result.status=true;
						result.type="opentext";
						result.message="打卡成功，上午签到时间："+this.getDateFormate(cTime)+"！迟到，是否填写备注？";
					}else if(cTime.before(swsbks)) {//未到上午上班打卡时间
						result.status=false;
						result.message="打卡失败，未到上午签到时间！";
						return result;
					}
					
				}else {//下午上班打卡
					if(iEntity.getXwqdsj()!=null && !this.timeEquals(iEntity.getXwqdsj(), cTime)) {//已打卡不做处理
						result.status=true;
						result.type="create";
						result.message="已打卡，下午签到时间："+this.getDateFormate(iEntity.getXwqdsj())+"！";
						return result;
					}
					iEntity.setXwqdsj(cTime);//下午签到时间
					//处理下午上班签到位置，说明问题
					iEntity.setXwlatitude(latitude);
					iEntity.setXwlongitude(longitude);
					iEntity.setXwaccuracy(accuracy);
					iEntity.setXwaddress(address);
					iEntity.setXwdirtype(dirtype);
					//考勤地点处理
					if(!"".equals(bz)) {//如果备注不为空，则添加备注到记录
						bz="下午签到（"+sTime+"）："+bz;
					}
					if(cTime.before(xwsbjs) && cTime.after(xwsbks)) {//正常
						sfsjyc="否";
						result.message="打卡成功，下午签到时间："+this.getDateFormate(cTime)+"！";
					}else if(cTime.after(xwsbjs)){//迟到
						dkbz="下午签到（"+sTime+"）：迟到；";
						sfsjyc="是";
						result.type="opentext";
						result.message="打卡成功，下午签到时间："+this.getDateFormate(cTime)+"！迟到，是否填写备注？";
					}else if(cTime.before(xwsbks)) {//未到下午上班打卡时间
						result.status=false;
						result.message="打卡失败，未到下午签到时间！";
						return result;
					}
				}
				if(!"是".equals(iEntity.getSfsjyc())) {
					iEntity.setSfsjyc(sfsjyc);
				}
				if(iEntity.getDkbz()!=null) {
					dkbz=iEntity.getDkbz()+dkbz;
				}
				if(iEntity.getBz()!=null) {
					bz=iEntity.getBz()+bz;
				}
				iEntity.setBz(bz);
				iEntity.setDkbz(dkbz);
			}else {//签退
				if(cTime.before(xwsbjs)) {//上午下班打卡
					if(iEntity.getQtsj()!=null  && !this.timeEquals(iEntity.getQtsj(), cTime)) {
						result.status=true;
						result.type="update";
						result.message="已打卡，上午签退时间："+this.getDateFormate(iEntity.getQtsj())+",是否更新下班时间?";
						return result;
					}
					iEntity.setQtsj(cTime);
					//处理签退位置，说明问题
					iEntity.setOutlatitude(latitude);
					iEntity.setOutlongitude(longitude);
					iEntity.setOutaccuracy(accuracy);
					iEntity.setOutaddress(address);
					iEntity.setOutdirtype(dirtype);
					
					//考勤地点处理
					if(!"".equals(bz)) {//如果备注不为空，则添加备注到记录
						bz="上午签退（"+sTime+"）："+bz;
					}
					if(cTime.before(swxbjs) && cTime.after(xwsbks)) {//正常
						sfsjyc="否";
						result.status=true;
						result.message="打卡成功，上午签退时间："+this.getDateFormate(cTime)+"！";
					}else if(cTime.before(xwsbks)){//早退
						dkbz="上午签退（"+sTime+"）：早退；";
						sfsjyc="是";
						result.status=true;
						result.type="opentext";
						result.message="打卡成功，上午签退时间："+this.getDateFormate(cTime)+"！早退，是否填写备注？";
					}else if(cTime.after(swxbjs)) {//已过上午下班打卡时间
						result.status=false;
						result.message="打卡失败，上午签退时间已过！";
						return result;
					}
				}else {//下午下班打卡
					if(iEntity.getXwqtsj()!=null  && !this.timeEquals(iEntity.getXwqtsj(), cTime)) {
						result.status=true;
						result.type="update";
						result.message="已打卡，下午签退时间："+this.getDateFormate(iEntity.getXwqtsj())+",是否更新下班时间?";
						return result;
					}
					iEntity.setXwqtsj(cTime);
					//处理签退位置，说明问题
					iEntity.setXwoutlatitude(latitude);
					iEntity.setXwoutlongitude(longitude);
					iEntity.setXwoutaccuracy(accuracy);
					iEntity.setXwoutaddress(address);
					iEntity.setXwoutdirtype(dirtype);
					
					//考勤地点处理
					if(!"".equals(bz)) {//如果备注不为空，则添加备注到记录
						bz="下午签退（"+sTime+"）："+bz;
					}
					if(cTime.after(xwxbks)) {//正常
						sfsjyc="否";
						result.status=true;
						result.message="打卡成功，下午签退时间："+this.getDateFormate(cTime)+"！";
					}else if(cTime.before(xwxbks)){//早退
						dkbz="下午签退（"+sTime+"）：早退；";
						sfsjyc="是";
						result.status=true;
						result.type="opentext";
						result.message="打卡成功，下午签退时间："+this.getDateFormate(cTime)+"！早退，是否填写备注？";
					}
				}
				if(!"是".equals(iEntity.getSfsjyc())) {
					iEntity.setSfsjyc(sfsjyc);
				}
				if(iEntity.getDkbz()!=null) {
					dkbz=iEntity.getDkbz()+dkbz;
				}
				if(iEntity.getBz()!=null) {
					bz=iEntity.getBz()+bz;
				}
				iEntity.setBz(bz);
				iEntity.setDkbz(dkbz);
			}
			iEntity.setCreatetime(new Date());
			this.service.update(iEntity);
			if("opentext".equals(result.type)) {
				result.data=iEntity;
			}
		}else {//无打卡记录
			GzqdglEntity iEntity=new GzqdglEntity();//创建本日记录
			iEntity.setId(this.createUUid());
			iEntity.setDanweiid(userEntity.getDepartmentId());
			iEntity.setDanweimc(userEntity.getDepartmentId());
			iEntity.setNandu(now.get(Calendar.YEAR));
			iEntity.setYuefen((now.get(Calendar.MONTH) + 1));
			iEntity.setDay(now.get(Calendar.DAY_OF_MONTH));
			iEntity.setRiqi(cTime);
			
			List<String> xq=new ArrayList<String>();
			xq.add("星期日");
			xq.add("星期一");
			xq.add("星期二");
			xq.add("星期三");
			xq.add("星期四");
			xq.add("星期五");
			xq.add("星期六");
			System.out.println("当前时间星期时间数："+now.get(Calendar.DAY_OF_WEEK));
			iEntity.setXingqi(xq.get(now.get(Calendar.DAY_OF_WEEK)-1));
			
			iEntity.setKaoqrid(userEntity.getUserid());
			iEntity.setKaoqrmc(userEntity.getUserName());
			
			result.type="create";
			iEntity.setSfkqfw(sfkqfw);
			if("signin".equals(type)) {//签到
				if(cTime.before(swxbks)) {//上午上班打卡
					iEntity.setQdsj(cTime);//上午签到时间
					//处理上午上班签到位置，说明问题
					iEntity.setLatitude(latitude);
					iEntity.setLongitude(longitude);
					iEntity.setAccuracy(accuracy);
					iEntity.setAddress(address);
					iEntity.setDirtype(dirtype);
					//考勤地点处理
					if(!"".equals(bz)) {//如果备注不为空，则添加备注到记录
						bz="上午签到（"+sTime+"）："+bz;
						iEntity.setBz(bz);
					}
					if(cTime.before(swsbjs) && cTime.after(swsbks)) {//正常
						sfsjyc="否";
						result.status=true;
						result.message="打卡成功，上午签到时间："+this.getDateFormate(cTime)+"！";
					}else if(cTime.after(swsbjs)) {//迟到
						dkbz="上午签到（"+sTime+"）：迟到；";
						sfsjyc="是";
						result.status=true;
						result.type="opentext";
						result.message="打卡成功，上午签到时间："+this.getDateFormate(cTime)+"！迟到，是否填写备注？";
					}else if(cTime.before(swsbks)) {//未到上午上班打卡时间
						result.status=false;
						result.message="打卡失败，未到上午签到时间！";
						return result;
					}
					
				}else {//下午上班打卡
					iEntity.setXwqdsj(cTime);//下午签到时间
					//处理下午上班签到位置，说明问题
					iEntity.setXwlatitude(latitude);
					iEntity.setXwlongitude(longitude);
					iEntity.setXwaccuracy(accuracy);
					iEntity.setXwaddress(address);
					iEntity.setXwdirtype(dirtype);
					//考勤地点处理
					if(!"".equals(bz)) {//如果备注不为空，则添加备注到记录
						bz="下午签到（"+sTime+"）："+bz;
						iEntity.setBz(bz);
					}
					if(cTime.before(xwsbjs) && cTime.after(xwsbks)) {//正常
						sfsjyc="否";
						result.message="打卡成功，下午签到时间："+this.getDateFormate(cTime)+"！";
					}else if(cTime.after(xwsbjs)){//迟到
						dkbz="下午签到（"+sTime+"）：迟到；";
						sfsjyc="是";
						result.type="opentext";
						result.message="打卡成功，下午签到时间："+this.getDateFormate(cTime)+"！迟到，是否填写备注？";
					}else if(cTime.before(xwsbks)) {//未到下午上班打卡时间
						result.status=false;
						result.message="打卡失败，未到下午签到时间！";
						return result;
					}
				}
				iEntity.setSfsjyc(sfsjyc);
				iEntity.setDkbz(dkbz);
			}else {//签退
				if(cTime.before(xwsbjs)) {//上午下班打卡
					iEntity.setQtsj(cTime);
					//处理签退位置，说明问题
					iEntity.setOutlatitude(latitude);
					iEntity.setOutlongitude(longitude);
					iEntity.setOutaccuracy(accuracy);
					iEntity.setOutaddress(address);
					iEntity.setOutdirtype(dirtype);
					
					//考勤地点处理
					if(!"".equals(bz)) {//如果备注不为空，则添加备注到记录
						bz="上午签退（"+sTime+"）："+bz;
						iEntity.setBz(bz);
					}
					if(cTime.before(swxbjs) && cTime.after(xwsbks)) {//正常
						sfsjyc="否";
						result.status=true;
						result.message="打卡成功，上午签退时间："+this.getDateFormate(cTime)+"！";
					}else if(cTime.before(xwxbks)){//早退
						dkbz="上午签退（"+sTime+"）：早退；";
						sfsjyc="是";
						result.status=true;
						result.type="opentext";
						result.message="打卡成功，上午签退时间："+this.getDateFormate(cTime)+"！早退，是否填写备注？";
					}else if(cTime.after(swxbjs)) {//已过上午下班打卡时间
						result.status=false;
						result.message="打卡失败，上午签退时间已过！";
						return result;
					}
				}else {//下午下班打卡
					iEntity.setXwqtsj(cTime);
					//处理签退位置，说明问题
					iEntity.setXwoutlatitude(latitude);
					iEntity.setXwoutlongitude(longitude);
					iEntity.setXwoutaccuracy(accuracy);
					iEntity.setXwoutaddress(address);
					iEntity.setXwoutdirtype(dirtype);
					
					//考勤地点处理
					if(!"".equals(bz)) {//如果备注不为空，则添加备注到记录
						bz="下午签退（"+sTime+"）："+bz;
						iEntity.setBz(bz);
					}
					if(cTime.after(xwxbks)) {//正常
						sfsjyc="否";
						result.status=true;
						result.message="打卡成功，下午签退时间："+this.getDateFormate(cTime)+"！";
					}else if(cTime.before(xwxbks)){//早退
						dkbz="下午签退（"+sTime+"）：早退；";
						sfsjyc="是";
						result.status=true;
						result.type="opentext";
						result.message="打卡成功，下午签退时间："+this.getDateFormate(cTime)+"！早退，是否填写备注？";
					}
				}
				iEntity.setSfsjyc(sfsjyc);
				iEntity.setDkbz(dkbz);
			}
			//添加记录
			iEntity.setCreatetime(new Date());
			this.service.add(iEntity);
			if("opentext".equals(result.type)) {
				result.data=iEntity;
			}
		}
		
		return result;
//		if(lst!=null && lst.size()>0) {//若有打卡记录
//			GzqdglEntity iEntity=lst.get(0);
//			
//			if("update".equals(method)) {//更新打卡时间
//				if(cTime.getHours()<12) {
//					iEntity.setQdsj(cTime);
//					//处理位置，说明问题
//					iEntity.setLatitude(latitude);
//					iEntity.setLongitude(longitude);
//					iEntity.setAccuracy(accuracy);
//					iEntity.setAddress(address);
//					iEntity.setDirtype(dirtype);
//					
//					result.status=true;
//					result.type="create";
//					result.message="已更新打卡，上班时间："+this.getDateFormate(cTime)+"！";
//				}else {
//					iEntity.setQtsj(cTime);
//					
//					//处理位置，说明问题
//					iEntity.setOutlatitude(latitude);
//					iEntity.setOutlongitude(longitude);
//					iEntity.setOutaccuracy(accuracy);
//					iEntity.setOutaddress(address);
//					iEntity.setOutdirtype(dirtype);
//					
//					result.status=true;
//					result.type="create";
//					result.message="已更新打卡，下班时间："+this.getDateFormate(cTime)+"！";
//				}
//				iEntity.setCreatetime(new Date());
//	
//				this.service.update(iEntity);
//				return result;
//			}
//			
//			
//			if(cTime.getHours()<12) {
//				if(iEntity.getQdsj()!=null && !this.timeEquals(iEntity.getQdsj(), cTime)) {
//
//					result.status=true;
//					result.type="create";
//					result.message="已打卡，打卡时间："+this.getDateFormate(iEntity.getQdsj())+"！";
//					return result;
//				}else {
//					iEntity.setQdsj(cTime);
//					//处理位置，说明问题
//					iEntity.setLatitude(latitude);
//					iEntity.setLongitude(longitude);
//					iEntity.setAccuracy(accuracy);
//					iEntity.setAddress(address);
//					iEntity.setDirtype(dirtype);
//					
//					this.service.update(iEntity);
//					result.status=true;
//					result.type="create";
//					result.message="打卡成功，打卡时间："+this.getDateFormate(cTime)+"！";
//					return result;
//				}
//				
//				
//			}else {
//				if(iEntity.getQtsj()!=null  && !this.timeEquals(iEntity.getQtsj(), cTime)) {
//					result.status=true;
//					result.type="update";
//					result.message="已打卡，已签退时间："+this.getDateFormate(cTime)+",是否更新下班时间?";
//					return result;
//				}else {
//					iEntity.setQtsj(cTime);
//					
//					//处理位置，说明问题
//					iEntity.setOutlatitude(latitude);
//					iEntity.setOutlongitude(longitude);
//					iEntity.setOutaccuracy(accuracy);
//					iEntity.setOutaddress(address);
//					iEntity.setOutdirtype(dirtype);
//					
//					result.status=true;
//					result.type="create";
//					result.message="已打卡，下班时间："+this.getDateFormate(cTime)+"！";
//					iEntity.setCreatetime(new Date());
//	
//					this.service.update(iEntity);
//				}	
//			}
//			
//		}else {
//			GzqdglEntity iEntity=new GzqdglEntity();
//			iEntity.setId(this.createUUid());
//			iEntity.setDanweiid(userEntity.getDepartmentId());
//			iEntity.setDanweimc(userEntity.getDepartmentId());
//			iEntity.setNandu(now.get(Calendar.YEAR));
//			iEntity.setYuefen((now.get(Calendar.MONTH) + 1));
//			iEntity.setDay(now.get(Calendar.DAY_OF_MONTH));
//			iEntity.setRiqi(cTime);
//			
//			List<String> xq=new ArrayList<String>();
//			xq.add("星期日");
//			xq.add("星期一");
//			xq.add("星期二");
//			xq.add("星期三");
//			xq.add("星期四");
//			xq.add("星期五");
//			xq.add("星期六");
//			System.out.println("当前时间星期时间数："+now.get(Calendar.DAY_OF_WEEK));
//			iEntity.setXingqi(xq.get(now.get(Calendar.DAY_OF_WEEK)-1));
//			
//			iEntity.setKaoqrid(userEntity.getUserid());
//			iEntity.setKaoqrmc(userEntity.getUserName());
//			
//			if(cTime.getHours()<12) {
//				iEntity.setQdsj(cTime);
//				
//				//处理位置，说明问题
//				iEntity.setLatitude(latitude);
//				iEntity.setLongitude(longitude);
//				iEntity.setAccuracy(accuracy);
//				iEntity.setAddress(address);
//				iEntity.setDirtype(dirtype);
//				
//				result.status=true;
//				result.type="create";
//				result.message="打卡成功，签到时间："+this.getDateFormate(cTime)+"！";
//			}else {
//				iEntity.setQtsj(cTime);
//				
//				//处理位置，说明问题
//				iEntity.setOutlatitude(latitude);
//				iEntity.setOutlongitude(longitude);
//				iEntity.setOutaccuracy(accuracy);
//				iEntity.setOutaddress(address);
//				iEntity.setOutdirtype(dirtype);
//				
//				result.status=true;
//				result.type="create";
//				result.message="打卡成功，下班时间："+this.getDateFormate(cTime)+"！";
//			}
//			iEntity.setCreatetime(new Date());
//			
//			this.service.add(iEntity);
//		}
		
		
	}
	
	/**
	 * 检查打卡时间、地点是否匹配
	 * @param signInfo
	 * 
	 * @return
	 * @throws ParseException 
	 */
	@SuppressWarnings("deprecation")
	public Map<String,Object> checkSign(Map<String,Object> signInfo,List<GzqdglEntity> lst) throws ParseException{
		//获取正常上下班时间
		Date cTime=(Date) signInfo.get("cTime");//打卡的时间
//		String type=signInfo.get("type").toString();//签到类型
		Calendar now = Calendar.getInstance(); 
		int year=now.get(Calendar.YEAR);
		int month=now.get(Calendar.MONTH)+1;
		int day=now.get(Calendar.DAY_OF_MONTH);
		List<GzgzrqdsjEntity> qlist=this.gzgzrqdsj.getEntitys(null, null, null, 1000, 1);
		if(qlist!=null && qlist.size()>0) {
			for(GzgzrqdsjEntity entity:qlist) {
				Date kssj=new SimpleDateFormat("yyyy-MM-dd").parse(year+"-"+reValue(entity.getKsyf(),"1")+"-"+reValue(entity.getKsrq(),"1"));//开始日期
				Date jssj=new SimpleDateFormat("yyyy-MM-dd").parse(year+"-"+reValue(entity.getJsyf(),"12")+"-"+reValue(entity.getJsrq(),"31"));//结束日期
				if((cTime.after(kssj) && cTime.before(jssj)) || cTime==kssj || cTime == jssj ) {//如果在日期范围内
					String date=year+"-"+month+"-"+day+" ";
					SimpleDateFormat sdfm=new SimpleDateFormat("yyyy-MM-dd HH:mm");
					Date swsbks=sdfm.parse(date+reValue(entity.getSwsbs(),"07")+":"+reValue(entity.getSwsbf(),"00"));//本日上午上班开始时间
					Date swsbjs=sdfm.parse(date+reValue(entity.getSwsbjss(),"12")+":"+reValue(entity.getSwsbjsf(),"00"));//本日上午上班结束时间
					Date swxbks=sdfm.parse(date+reValue(entity.getSwxbs(),"12")+":"+reValue(entity.getSwxbf(),"00"));//本日上午下班开始时间
					Date swxbjs=sdfm.parse(date+reValue(entity.getSwxbjss(),"14")+":"+reValue(entity.getSwxbjsf(),"00"));//本日上午下班结束时间
					Date xwsbks=sdfm.parse(date+reValue(entity.getXwsbs(),"13")+":"+reValue(entity.getXwsbf(),"00"));//本日下午上班开始时间
					Date xwsbjs=sdfm.parse(date+reValue(entity.getXwsbjss(),"14")+":"+reValue(entity.getXwsbjsf(),"00"));//本日下午上班结束时间
					Date xwxbks=sdfm.parse(date+reValue(entity.getXwxbs(),"18")+":"+reValue(entity.getXwxbf(),"00"));//本日下午下班开始时间
					Date xwxbjs=sdfm.parse(date+reValue(entity.getXwxbjss(),"23")+":"+reValue(entity.getXwxbjsf(),"59"));//本日下午下班结束时间
					
					signInfo.put("swsbjs", swsbjs);
					signInfo.put("swsbks", swsbks);
					signInfo.put("swxbjs", swxbjs);
					signInfo.put("swxbks", swxbks);
					signInfo.put("xwsbjs", xwsbjs);
					signInfo.put("xwsbks", xwsbks);
					signInfo.put("xwxbjs", xwxbjs);
					signInfo.put("xwxbks", xwxbks);
					break;
				}
			}
		}
		
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		sdf.parse(year+"-"+month+"-"+day+" 07:00");
		if (!signInfo.containsKey("swsbks") && signInfo.get("swsbks") == null)
			signInfo.put("swsbks", sdf.parse(year+"-"+month+"-"+day+" 07:00"));
		if (!signInfo.containsKey("swsbjs") && signInfo.get("swsbjs") == null)
			signInfo.put("swsbjs", sdf.parse(year+"-"+month+"-"+day+" 09:00"));
		if (!signInfo.containsKey("swxbks") && signInfo.get("swxbks") == null)
			signInfo.put("swxbks", sdf.parse(year+"-"+month+"-"+day+" 12:00"));
		if (!signInfo.containsKey("swxbjs") && signInfo.get("swxbjs") == null)
			signInfo.put("swxbjs", sdf.parse(year+"-"+month+"-"+day+" 14:00"));
		if (!signInfo.containsKey("xwsbks") && signInfo.get("xwsbks") == null)
			signInfo.put("xwsbks", sdf.parse(year+"-"+month+"-"+day+" 13:00"));
		if (!signInfo.containsKey("xwsbjs") && signInfo.get("xwsbjs") == null)
			signInfo.put("xwsbjs", sdf.parse(year+"-"+month+"-"+day+" 14:00"));
		if (!signInfo.containsKey("xwxbks") && signInfo.get("xwxbks") == null)
			signInfo.put("xwxbks", sdf.parse(year+"-"+month+"-"+day+" 18:00"));
		if (!signInfo.containsKey("xwxbjs") && signInfo.get("xwxbjs") == null)
			signInfo.put("xwxbjs", sdf.parse(year+"-"+month+"-"+day+" 23:59"));
		
		return signInfo;
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Long getDistance(String startLonLat, String endLonLat,String key) throws IOException{
		//返回起始地startAddr与目的地endAddr之间的距离，单位：米
		Long distance = 0L;
		String queryUrl = "http://restapi.amap.com/v3/distance?key="+key+"&origins="+startLonLat+"&destination="+endLonLat;
		String queryResult = getResponse(queryUrl);
		HashMap data = JsonUtils.toBean(queryResult, HashMap.class);
		List<HashMap> results=(List<HashMap>) data.get("results");
		
		Object obj=results.get(0).get("distance");
		if(obj==null){
			return distance;
		}
		distance = Long.parseLong(obj.toString());
		return distance;
	}
	
	private static String getResponse(String serverUrl){
        //用JAVA发起http请求，并返回json格式的结果
        StringBuffer result = new StringBuffer();
        try {
            URL url = new URL(serverUrl);
            URLConnection conn = url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
 
            String line;
            while((line = in.readLine()) != null){
                result.append(line);
            }
            in.close();
 
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }
	
	/**
	 * 获取格式化时间格式
	 * @param date
	 * 
	 * @return
	 */
	public String getDateFormate(Date date) {
		SimpleDateFormat formate=new SimpleDateFormat("HH时mm分ss秒");
		return formate.format(date);
	}
	
	public String reValue(Object value,String result) {
		if(value != null && !"".equals(value)) {//值为空时返回第二个值
			return value.toString();
		}else {
			return result;
		}
	}
	
	public String createUUid() {
		return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()).toString()+ 
				UUID.randomUUID().toString().replaceAll("-", "");
	}
	
	public boolean timeEquals(Date time1,Date time2) {
		SimpleDateFormat formate=new SimpleDateFormat("yyyyMMddHHmm");
		if(formate.format(time1).equals(formate.format(time2))) {
			return true;
		}else {
			return false;
		}
	}
}
