package com.itfreer.gzrskqtj.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzrskqtj.entity.GzrskqtjEntity;
import com.itfreer.gzrskqtj.service.GzrskqtjService;

/**
 * 定义考勤统计Rest服务接口
 */
@Component
@Path("/gzrskqtj/")
public class RestGzrskqtjService extends BaseRestService<GzrskqtjEntity> {
	
	@Autowired
	private GzrskqtjService service;

	@Override
	protected BaseService<GzrskqtjEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzrskqtj";
	}
}
