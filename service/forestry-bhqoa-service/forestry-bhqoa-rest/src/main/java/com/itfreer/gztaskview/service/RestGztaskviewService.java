package com.itfreer.gztaskview.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gztaskview.entity.GztaskviewEntity;
import com.itfreer.gztaskview.service.GztaskviewService;

/**
 * 定义收文视图Rest服务接口
 */
@Component
@Path("/gztaskview/")
public class RestGztaskviewService extends BaseRestService<GztaskviewEntity> {
	
	@Autowired
	private GztaskviewService service;

	@Override
	protected BaseService<GztaskviewEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gztaskview";
	}
}
