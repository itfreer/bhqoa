package com.itfreer.gzyzgl.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.api.BpmBaseRestService;
import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.gzyzgl.entity.GzyzglEntity;

/**
 * 定义用章管理Rest服务接口
 */
@Component
@Path("/gzyzgl/")
public class RestGzyzglService extends BpmBaseRestService<GzyzglEntity> {
	
	@Autowired
	private GzyzglService service;

	@Override
	public IBaseWorkFlowService<GzyzglEntity> getWorkFlowService() {
		return this.service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzyzgl";
	}
}
