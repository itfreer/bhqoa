package com.itfreer.gzqdglqddd.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzqdglqddd.entity.GzqdglqdddEntity;
import com.itfreer.gzqdglqddd.service.GzqdglqdddService;

/**
 * 定义签到地点Rest服务接口
 */
@Component
@Path("/gzqdglqddd/")
public class RestGzqdglqdddService extends BaseRestService<GzqdglqdddEntity> {
	
	@Autowired
	private GzqdglqdddService service;

	@Override
	protected BaseService<GzqdglqdddEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzqdglqddd";
	}
}
