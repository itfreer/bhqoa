package com.itfreer.gzhlyndkh.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzhlyndkh.entity.GzhlyndkhEntity;
import com.itfreer.gzhlyndkh.service.GzhlyndkhService;

/**
 * 定义护林员年度考核Rest服务接口
 */
@Component
@Path("/gzhlyndkh/")
public class RestGzhlyndkhService extends BaseRestService<GzhlyndkhEntity> {
	
	@Autowired
	private GzhlyndkhService service;

	@Override
	protected BaseService<GzhlyndkhEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzhlyndkh";
	}
}
