package com.itfreer.gzwxtj.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzwxtj.entity.GzwxtjEntity;
import com.itfreer.gzwxtj.service.GzwxtjService;

/**
 * 定义维修统计Rest服务接口
 */
@Component
@Path("/gzwxtj/")
public class RestGzwxtjService extends BaseRestService<GzwxtjEntity> {
	
	@Autowired
	private GzwxtjService service;

	@Override
	protected BaseService<GzwxtjEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzwxtj";
	}
}
