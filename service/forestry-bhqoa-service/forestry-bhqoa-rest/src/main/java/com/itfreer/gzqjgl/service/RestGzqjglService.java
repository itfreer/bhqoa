package com.itfreer.gzqjgl.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.api.BpmBaseRestService;
import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzqjgl.entity.GzqjglEntity;
import com.itfreer.gzqjgl.service.GzqjglService;

/**
 * 定义请假管理Rest服务接口
 */
@Component
@Path("/gzqjgl/")
public class RestGzqjglService extends BpmBaseRestService<GzqjglEntity> {
	
	@Autowired
	private GzqjglService service;


	
	@Override
	protected String getServiceName() {
		return "gzqjgl";
	}



	@Override
	public IBaseWorkFlowService getWorkFlowService() {
		return this.service;
	}
	
	
}
