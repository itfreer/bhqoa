package com.itfreer.gzdevicepurchase.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzdevicepurchase.entity.GzdevicepurchaseEntity;
import com.itfreer.gzdevicepurchase.service.GzdevicepurchaseService;

/**
 * 定义设备采购管理Rest服务接口
 */
@Component
@Path("/gzdevicepurchase/")
public class RestGzdevicepurchaseService extends BaseRestService<GzdevicepurchaseEntity> {
	
	@Autowired
	private GzdevicepurchaseService service;

	@Override
	protected BaseService<GzdevicepurchaseEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzdevicepurchase";
	}
}
