package com.itfreer.gztask.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gztask.entity.gztaskEntity;
import com.itfreer.gztask.service.gztaskService;

/**
 * 定义任务安排Rest服务接口
 */
@Component
@Path("/gztask/")
public class RestgztaskService extends BaseRestService<gztaskEntity> {
	
	@Autowired
	private gztaskService service;

	@Override
	protected BaseService<gztaskEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gztask";
	}
}
