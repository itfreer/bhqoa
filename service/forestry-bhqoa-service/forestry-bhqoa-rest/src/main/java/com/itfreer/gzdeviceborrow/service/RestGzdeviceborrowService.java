package com.itfreer.gzdeviceborrow.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzdeviceborrow.entity.GzdeviceborrowEntity;
import com.itfreer.gzdeviceborrow.service.GzdeviceborrowService;

/**
 * 定义设备借还Rest服务接口
 */
@Component
@Path("/gzdeviceborrow/")
public class RestGzdeviceborrowService extends BaseRestService<GzdeviceborrowEntity> {
	
	@Autowired
	private GzdeviceborrowService service;

	@Override
	protected BaseService<GzdeviceborrowEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzdeviceborrow";
	}
}
