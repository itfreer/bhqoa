package com.itfreer.gzwlkyxmcg.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzwlkyxmcg.entity.GzwlkyxmcgEntity;
import com.itfreer.gzwlkyxmcg.service.GzwlkyxmcgService;

/**
 * 定义外来科研项目成果Rest服务接口
 */
@Component
@Path("/gzwlkyxmcg/")
public class RestGzwlkyxmcgService extends BaseRestService<GzwlkyxmcgEntity> {
	
	@Autowired
	private GzwlkyxmcgService service;

	@Override
	protected BaseService<GzwlkyxmcgEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzwlkyxmcg";
	}
}
