package com.itfreer.gzreceiveview.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzreceiveview.entity.GzreceiveviewEntity;
import com.itfreer.gzreceiveview.service.GzreceiveviewService;

/**
 * 定义收文管理Rest服务接口
 */
@Component
@Path("/gzreceiveview/")
public class RestGzreceiveviewService extends BaseRestService<GzreceiveviewEntity> {
	
	@Autowired
	private GzreceiveviewService service;

	@Override
	protected BaseService<GzreceiveviewEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzreceiveview";
	}
}
