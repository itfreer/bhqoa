package com.itfreer.gzclda.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzclda.entity.GzcldaEntity;
import com.itfreer.gzclda.service.GzcldaService;

/**
 * 定义车辆档案Rest服务接口
 */
@Component
@Path("/gzclda/")
public class RestGzcldaService extends BaseRestService<GzcldaEntity> {
	
	@Autowired
	private GzcldaService service;

	@Override
	protected BaseService<GzcldaEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzclda";
	}
}
