package com.itfreer.gzglzda.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzglzda.entity.GzglzdaEntity;
import com.itfreer.gzglzda.service.GzglzdaService;

/**
 * 定义管理站档案Rest服务接口
 */
@Component
@Path("/gzglzda/")
public class RestGzglzdaService extends BaseRestService<GzglzdaEntity> {
	
	@Autowired
	private GzglzdaService service;

	@Override
	protected BaseService<GzglzdaEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzglzda";
	}
}
