package com.itfreer.gzwjryrqxk.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.api.BpmBaseRestService;
import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.form.api.BaseService;
import com.itfreer.gzwjryrqxk.entity.GzwjryrqxkEntity;

/**
 * 定义外籍人员入区许可Rest服务接口
 */
@Component
@Path("/gzwjryrqxk/")
public class RestGzwjryrqxkService extends BpmBaseRestService<GzwjryrqxkEntity> {
	
	@Autowired
	private GzwjryrqxkService service;

	@Override
	protected String getServiceName() {
		return "gzwjryrqxk";
	}

	@Override
	public IBaseWorkFlowService getWorkFlowService() {
		return this.service;
	}
}
