package com.itfreer.gzxmjsxk.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzxmjsxk.entity.GzxmjsxkEntity;
import com.itfreer.gzxmjsxk.service.GzxmjsxkService;

/**
 * 定义项目建设许可Rest服务接口
 */
@Component
@Path("/gzxmjsxk/")
public class RestGzxmjsxkService extends BaseRestService<GzxmjsxkEntity> {
	
	@Autowired
	private GzxmjsxkService service;

	@Override
	protected BaseService<GzxmjsxkEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzxmjsxk";
	}
}
