package com.itfreer.gzqdgl.service;

import com.itfreer.gzqdgl.entity.GzqdglEntity;

public class QdResultParam {
	
	/**
	 * 直接是否成功
	 */
	public boolean status=true;
	/**
	 * create表示签到成功，update提示是否更新签到时间
	 */
	public String type="";
	/**
	 * 后台返回前端提示信息
	 */
	public String message="";
	
	public GzqdglEntity data;
	
	public GzqdglEntity getData() {
		return data;
	}
	public void setData(GzqdglEntity data) {
		this.data = data;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
