package com.itfreer.gzprojectmonthly.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzprojectmonthly.entity.GzprojectmonthlyEntity;
import com.itfreer.gzprojectmonthly.service.GzprojectmonthlyService;

/**
 * 定义项目月报Rest服务接口
 */
@Component
@Path("/gzprojectmonthly/")
public class RestGzprojectmonthlyService extends BaseRestService<GzprojectmonthlyEntity> {
	
	@Autowired
	private GzprojectmonthlyService service;

	@Override
	protected BaseService<GzprojectmonthlyEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzprojectmonthly";
	}
}
