package com.itfreer.gzaqbw.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzaqbw.entity.GzaqbwEntity;
import com.itfreer.gzaqbw.service.GzaqbwService;

/**
 * 定义安全保卫Rest服务接口
 */
@Component
@Path("/gzaqbw/")
public class RestGzaqbwService extends BaseRestService<GzaqbwEntity> {
	
	@Autowired
	private GzaqbwService service;

	@Override
	protected BaseService<GzaqbwEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzaqbw";
	}
}
