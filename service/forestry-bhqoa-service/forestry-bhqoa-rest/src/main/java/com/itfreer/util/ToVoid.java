package com.itfreer.util;

import com.itfreer.bpm.flow.findwork.para.FindWorkInfo;
import com.itfreer.bpm.history.entity.BpmHistoryEntity;
import com.itfreer.bpm.messager.BpmMessage;
import com.itfreer.bpm.option.para.OptionInfo;
import com.itfreer.bpm.power.para.UserInfo;
import com.itfreer.gzccgl.dao.GzccglDao;
import com.itfreer.gzccgl.entity.GzccglEntity;
import com.itfreer.gzccgl.service.GzccglService;
import com.itfreer.gzqjgl.dao.GzqjglDao;
import com.itfreer.gzqjgl.entity.GzqjglEntity;
import com.itfreer.gzqjgl.service.GzqjglService;
import com.itfreer.utils.json.JsonUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 发布rest调用服务
 */
@Component
@Path("/toVoid/")
public class ToVoid {
	@Autowired
	GzccglDao ccglDao;
	@Autowired
	GzqjglDao qjglDao;
	@Autowired
	GzccglService ccWorkFlow;
	@Autowired
	GzqjglService qjWorkFlow;
	
	@POST
	@Path("/dispose")
	public BpmMessage dispose(@Context HttpServletRequest request,String data) throws Exception {
		System.out.println(data);
		String bpmKey=null;
		String id=null;
		String fName=null;
		UserInfo user=null;
		HashMap<String, Object> args = getRestParam(data);
		//获取参数
		if(args!=null) {
			if (args.containsKey("user") && args.get("user") != null) {
				user=JsonUtils.toBean(args.get("user").toString(), UserInfo.class);
			}
			if (args.containsKey("bpmKey") && args.get("bpmKey") != null) {
				bpmKey =args.get("bpmKey").toString();
			}
			if (args.containsKey("id") && args.get("id") != null) {
				id = args.get("id").toString();
			}
			if (args.containsKey("fName") && args.get("fName") != null) {
				fName = args.get("fName").toString();
			}
		}
		
		OptionInfo oInfo=new OptionInfo();
		oInfo.setUserOption("作废");
		BpmMessage message;
		System.out.println("gzccgl".equals(fName.toString())+"\"gzccgl\".equals(fName.toString())");
		if("gzccgl".equals(fName.toString())) {
			FindWorkInfo<GzccglEntity> info =ccWorkFlow.findWork(fName, id, user);
			oInfo.setSexeid(info.sexeid);
			oInfo.setbpmkey(bpmKey);
			List<BpmHistoryEntity> list=new ArrayList<BpmHistoryEntity>();
			for(int i=0;i<info.getUniversalTask().size();i++) {
				if(info.getUniversalTask().get(i).getEditRow()!=null) {
					list.add(info.getUniversalTask().get(i).getEditRow());
				}
			}
			oInfo.setEditHistory(list);
			GzccglEntity entity=info.getEntity();
			message= ccWorkFlow.disposal(oInfo, entity, user);
			ccWorkFlow.update(entity);
			message.setEntity(entity);
		}else {
			FindWorkInfo<GzqjglEntity> info =qjWorkFlow.findWork(fName, id, user);
			oInfo.setSexeid(info.sexeid);
			oInfo.setbpmkey(bpmKey);
			List<BpmHistoryEntity> list=new ArrayList<BpmHistoryEntity>();
			for(int i=0;i<info.getUniversalTask().size();i++) {
				if(info.getUniversalTask().get(i).getEditRow()!=null) {
					list.add(info.getUniversalTask().get(i).getEditRow());
				}
			}
			oInfo.setEditHistory(list);
			GzqjglEntity entity=info.getEntity();
			message= qjWorkFlow.disposal(oInfo, entity, user);
			qjWorkFlow.update(entity);
			message.setEntity(entity);
		}
		return message;
	}
	
	/**
	 * 解决请求参数问题
	 * 
	 * @param data
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<String, Object> getRestParam(String data) {
		HashMap<String, Object> args = null;
		if (data == null || "".equals(data.trim()))
			return args;
		// 解决不同浏览器返回传递参数不一致问题-s-
		try {
			args = JsonUtils.toBean(data, HashMap.class);

		} catch (Exception e) {
			try {
				String ENCODE = "UTF-8";
				data = java.net.URLDecoder.decode(data, ENCODE);
				if (data != null && !"".equals(data.trim())) {
					String[] lst = data.split("&");
					args = new HashMap<String, Object>();
					for (String item : lst) {
						if (item == null || "".equals(item.trim()))
							continue;
						String[] ivalue = item.split("=");
						if (ivalue != null) {
							if (ivalue.length > 1) {
								args.put(ivalue[0], ivalue[1]);
							} else {
								args.put(ivalue[0], null);
							}
						}

					}
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		// 解决不同浏览器返回传递参数不一致问题-e-
		return args;
	}
	
}
