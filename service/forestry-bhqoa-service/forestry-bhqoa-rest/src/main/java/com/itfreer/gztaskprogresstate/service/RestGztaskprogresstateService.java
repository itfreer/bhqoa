package com.itfreer.gztaskprogresstate.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gztaskprogresstate.entity.GztaskprogresstateEntity;
import com.itfreer.gztaskprogresstate.service.GztaskprogresstateService;

/**
 * 定义任务监测Rest服务接口
 */
@Component
@Path("/gztaskprogresstate/")
public class RestGztaskprogresstateService extends BaseRestService<GztaskprogresstateEntity> {
	
	@Autowired
	private GztaskprogresstateService service;

	@Override
	protected BaseService<GztaskprogresstateEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gztaskprogresstate";
	}
}
