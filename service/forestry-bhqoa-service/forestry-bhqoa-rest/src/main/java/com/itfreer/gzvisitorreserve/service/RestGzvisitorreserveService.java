package com.itfreer.gzvisitorreserve.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzvisitorreserve.entity.GzvisitorreserveEntity;
import com.itfreer.gzvisitorreserve.service.GzvisitorreserveService;

/**
 * 定义访客预约管理Rest服务接口
 */
@Component
@Path("/gzvisitorreserve/")
public class RestGzvisitorreserveService extends BaseRestService<GzvisitorreserveEntity> {
	
	@Autowired
	private GzvisitorreserveService service;

	@Override
	protected BaseService<GzvisitorreserveEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzvisitorreserve";
	}
}
