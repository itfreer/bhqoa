package com.itfreer.gzsqxm.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzsqxm.entity.GzsqxmEntity;
import com.itfreer.gzsqxm.service.GzsqxmService;

/**
 * 定义社区项目申请/台账Rest服务接口
 */
@Component
@Path("/gzsqxm/")
public class RestGzsqxmService extends BaseRestService<GzsqxmEntity> {
	
	@Autowired
	private GzsqxmService service;

	@Override
	protected BaseService<GzsqxmEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzsqxm";
	}
}
