package com.itfreer.gzhlyjdkh.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzhlyjdkh.entity.GzhlyjdkhEntity;
import com.itfreer.gzhlyjdkh.service.GzhlyjdkhService;

/**
 * 定义护林员季度考核Rest服务接口
 */
@Component
@Path("/gzhlyjdkh/")
public class RestGzhlyjdkhService extends BaseRestService<GzhlyjdkhEntity> {
	
	@Autowired
	private GzhlyjdkhService service;

	@Override
	protected BaseService<GzhlyjdkhEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzhlyjdkh";
	}
}
