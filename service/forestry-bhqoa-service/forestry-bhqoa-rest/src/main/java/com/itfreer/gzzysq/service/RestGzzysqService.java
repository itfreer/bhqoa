package com.itfreer.gzzysq.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.api.BpmBaseRestService;
import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.form.api.BaseService;
import com.itfreer.gzzysq.entity.GzzysqEntity;

/**
 * 定义资源申请Rest服务接口
 */
@Component
@Path("/gzzysq/")
public class RestGzzysqService extends BpmBaseRestService<GzzysqEntity> {
	
	@Autowired
	private GzzysqService service;
	@Override
	protected String getServiceName() {
		return "gzzysq";
	}

	@Override
	public IBaseWorkFlowService getWorkFlowService() {
		return this.service;
	}
}
