package com.itfreer.gzccsxry.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzccsxry.entity.GzccsxryEntity;
import com.itfreer.gzccsxry.service.GzccsxryService;

/**
 * 定义出差随行人员Rest服务接口
 */
@Component
@Path("/gzccsxry/")
public class RestGzccsxryService extends BaseRestService<GzccsxryEntity> {
	
	@Autowired
	private GzccsxryService service;

	@Override
	protected BaseService<GzccsxryEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzccsxry";
	}
}
