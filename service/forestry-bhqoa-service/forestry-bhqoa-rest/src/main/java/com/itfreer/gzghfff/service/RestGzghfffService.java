package com.itfreer.gzghfff.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzghfff.entity.GzghfffEntity;
import com.itfreer.gzghfff.service.GzghfffService;

/**
 * 定义管护费发放Rest服务接口
 */
@Component
@Path("/gzghfff/")
public class RestGzghfffService extends BaseRestService<GzghfffEntity> {
	
	@Autowired
	private GzghfffService service;

	@Override
	protected BaseService<GzghfffEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzghfff";
	}
}
