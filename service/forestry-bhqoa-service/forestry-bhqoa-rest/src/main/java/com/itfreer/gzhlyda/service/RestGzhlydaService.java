package com.itfreer.gzhlyda.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzhlyda.entity.GzhlydaEntity;
import com.itfreer.gzhlyda.service.GzhlydaService;

/**
 * 定义护林员档案Rest服务接口
 */
@Component
@Path("/gzhlyda/")
public class RestGzhlydaService extends BaseRestService<GzhlydaEntity> {
	
	@Autowired
	private GzhlydaService service;

	@Override
	protected BaseService<GzhlydaEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzhlyda";
	}
}
