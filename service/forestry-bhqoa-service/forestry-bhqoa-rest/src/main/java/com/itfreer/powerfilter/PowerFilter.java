package com.itfreer.powerfilter;

import org.springframework.stereotype.Component;

import com.itfreer.form.api.PowerReplace;
import com.itfreer.jwt.JwtTicket;
import com.itfreer.power.entity.user.VUserInfoEntity;

@Component
public class PowerFilter  implements PowerReplace {
	private final static String s_user = "user";
	@Override
	public String replace(JwtTicket jwtTicket, String item) {
		VUserInfoEntity userEntity = jwtTicket != null ? (VUserInfoEntity) jwtTicket.getAttribute().get(s_user) : null;
		if (userEntity != null) {
			if(userEntity.getIsSuperAdmin()!=null && userEntity.getIsSuperAdmin()) {
				return null;
			}
			item = item.replace("${userId}", userEntity.getUserid());
			item=item.replace("${depId}", userEntity.getDepartmentId());
			item=item.replace("${orgId}", userEntity.getOrganizationId());
			return item;
		}
		return null;
	}
}
