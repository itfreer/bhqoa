package com.itfreer.vkqtj.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.vkqtj.entity.VkqtjEntity;
import com.itfreer.vkqtj.service.VkqtjService;

/**
 * 定义考勤统计Rest服务接口
 */
@Component
@Path("/vkqtj/")
public class RestVkqtjService extends BaseRestService<VkqtjEntity> {
	
	@Autowired
	private VkqtjService service;

	@Override
	protected BaseService<VkqtjEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "vkqtj";
	}
}
