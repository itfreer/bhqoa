package com.itfreer.gzdevice.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzdevice.entity.GzdeviceEntity;
import com.itfreer.gzdevice.service.GzdeviceService;

/**
 * 定义设备Rest服务接口
 */
@Component
@Path("/gzdevice/")
public class RestGzdeviceService extends BaseRestService<GzdeviceEntity> {
	
	@Autowired
	private GzdeviceService service;

	@Override
	protected BaseService<GzdeviceEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzdevice";
	}
}
