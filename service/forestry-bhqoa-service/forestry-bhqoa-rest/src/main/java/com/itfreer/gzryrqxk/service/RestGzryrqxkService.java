package com.itfreer.gzryrqxk.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.api.BpmBaseRestService;
import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.form.api.BaseService;
import com.itfreer.gzryrqxk.entity.GzryrqxkEntity;

/**
 * 定义外籍人员入区许可、外来科研人员入区许可Rest服务接口
 */
@Component
@Path("/gzryrqxk/")
public class RestGzryrqxkService extends BpmBaseRestService<GzryrqxkEntity> {
	
	@Autowired
	private GzryrqxkService service;
	
	@Override
	protected String getServiceName() {
		return "gzryrqxk";
	}

	@Override
	public IBaseWorkFlowService getWorkFlowService() {
		return service;
	}
}
