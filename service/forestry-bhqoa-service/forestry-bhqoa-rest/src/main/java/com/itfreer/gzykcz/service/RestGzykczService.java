package com.itfreer.gzykcz.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzykcz.entity.GzykczEntity;
import com.itfreer.gzykcz.service.GzykczService;

/**
 * 定义油卡充值Rest服务接口
 */
@Component
@Path("/gzykcz/")
public class RestGzykczService extends BaseRestService<GzykczEntity> {
	
	@Autowired
	private GzykczService service;

	@Override
	protected BaseService<GzykczEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzykcz";
	}
}
