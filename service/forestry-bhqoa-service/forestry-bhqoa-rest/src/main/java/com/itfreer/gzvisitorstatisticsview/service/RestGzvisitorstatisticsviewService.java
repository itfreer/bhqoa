package com.itfreer.gzvisitorstatisticsview.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzvisitorstatisticsview.entity.GzvisitorstatisticsviewEntity;
import com.itfreer.gzvisitorstatisticsview.service.GzvisitorstatisticsviewService;

/**
 * 定义访客统计（视图）Rest服务接口
 */
@Component
@Path("/gzvisitorstatisticsview/")
public class RestGzvisitorstatisticsviewService extends BaseRestService<GzvisitorstatisticsviewEntity> {
	
	@Autowired
	private GzvisitorstatisticsviewService service;

	@Override
	protected BaseService<GzvisitorstatisticsviewEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzvisitorstatisticsview";
	}
}
