package com.itfreer.gzccgl.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.api.BpmBaseRestService;
import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.form.api.BaseService;
import com.itfreer.gzccgl.entity.GzccglEntity;

/**
 * 定义出差管理Rest服务接口
 */
@Component
@Path("/gzccgl/")
public class RestGzccglService extends BpmBaseRestService<GzccglEntity> {
	
	@Autowired
	private GzccglService service;
	
	@Override
	protected String getServiceName() {
		return "gzccgl";
	}

	@Override
	public IBaseWorkFlowService getWorkFlowService() {
		return service;
	}
}
