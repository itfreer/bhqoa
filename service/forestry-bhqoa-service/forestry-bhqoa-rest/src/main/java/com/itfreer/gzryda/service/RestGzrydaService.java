package com.itfreer.gzryda.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzryda.entity.GzrydaEntity;
import com.itfreer.gzryda.service.GzrydaService;

/**
 * 定义人事档案Rest服务接口
 */
@Component
@Path("/gzryda/")
public class RestGzrydaService extends BaseRestService<GzrydaEntity> {
	
	@Autowired
	private GzrydaService service;

	@Override
	protected BaseService<GzrydaEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzryda";
	}
}
