package com.itfreer.gzsqclqd.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzsqclqd.entity.GzsqclqdEntity;
import com.itfreer.gzsqclqd.service.GzsqclqdService;

/**
 * 定义申请材料清单Rest服务接口
 */
@Component
@Path("/gzsqclqd/")
public class RestGzsqclqdService extends BaseRestService<GzsqclqdEntity> {
	
	@Autowired
	private GzsqclqdService service;

	@Override
	protected BaseService<GzsqclqdEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzsqclqd";
	}
}
