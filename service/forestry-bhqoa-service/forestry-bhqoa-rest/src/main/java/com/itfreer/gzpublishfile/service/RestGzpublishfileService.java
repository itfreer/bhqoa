package com.itfreer.gzpublishfile.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.api.BpmBaseRestService;
import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.gzpublishfile.entity.GzpublishfileEntity;

/**
 * 定义发文管理Rest服务接口
 */
@Component
@Path("/gzpublishfile/")
public class RestGzpublishfileService extends BpmBaseRestService<GzpublishfileEntity> {
	
	@Autowired
	private GzpublishfileService service;

	@Override
	public IBaseWorkFlowService<GzpublishfileEntity> getWorkFlowService() {
		return this.service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzpublishfile";
	}
}
