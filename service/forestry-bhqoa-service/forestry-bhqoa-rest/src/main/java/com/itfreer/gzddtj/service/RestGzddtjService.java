package com.itfreer.gzddtj.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzddtj.entity.GzddtjEntity;
import com.itfreer.gzddtj.service.GzddtjService;

/**
 * 定义调度统计Rest服务接口
 */
@Component
@Path("/gzddtj/")
public class RestGzddtjService extends BaseRestService<GzddtjEntity> {
	
	@Autowired
	private GzddtjService service;

	@Override
	protected BaseService<GzddtjEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzddtj";
	}
}
