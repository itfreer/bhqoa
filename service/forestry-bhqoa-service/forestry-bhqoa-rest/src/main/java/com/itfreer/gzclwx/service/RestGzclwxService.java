package com.itfreer.gzclwx.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzclwx.entity.GzclwxEntity;
import com.itfreer.gzclwx.service.GzclwxService;

/**
 * 定义车辆维修Rest服务接口
 */
@Component
@Path("/gzclwx/")
public class RestGzclwxService extends BaseRestService<GzclwxEntity> {
	
	@Autowired
	private GzclwxService service;

	@Override
	protected BaseService<GzclwxEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzclwx";
	}
}
