package com.itfreer.gzreceivefile.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzreceivefile.entity.GzreceivefileEntity;
import com.itfreer.gzreceivefile.service.GzreceivefileService;

/**
 * 定义收文管理Rest服务接口
 */
@Component
@Path("/gzreceivefile/")
public class RestGzreceivefileService extends BaseRestService<GzreceivefileEntity> {
	
	@Autowired
	private GzreceivefileService service;

	@Override
	protected BaseService<GzreceivefileEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzreceivefile";
	}
}
