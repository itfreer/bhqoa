package com.itfreer.gzhlygzgl.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzhlygzgl.entity.GzhlygzglEntity;
import com.itfreer.gzhlygzgl.service.GzhlygzglService;

/**
 * 定义护林员工资管理Rest服务接口
 */
@Component
@Path("/gzhlygzgl/")
public class RestGzhlygzglService extends BaseRestService<GzhlygzglEntity> {
	
	@Autowired
	private GzhlygzglService service;

	@Override
	protected BaseService<GzhlygzglEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzhlygzgl";
	}
}
