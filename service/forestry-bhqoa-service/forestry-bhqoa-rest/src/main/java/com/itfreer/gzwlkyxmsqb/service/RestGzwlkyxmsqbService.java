package com.itfreer.gzwlkyxmsqb.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.api.BpmBaseRestService;
import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzwlkyxmsqb.entity.GzwlkyxmsqbEntity;
import com.itfreer.gzwlkyxmsqb.service.GzwlkyxmsqbService;

/**
 * 定义外来科研项目申请表Rest服务接口
 */
@Component
@Path("/gzwlkyxmsqb/")
public class RestGzwlkyxmsqbService extends BpmBaseRestService<GzwlkyxmsqbEntity> {
	
	@Autowired
	private GzwlkyxmsqbService service;

	@Override
	protected BaseService<GzwlkyxmsqbEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzwlkyxmsqb";
	}

	@Override
	public IBaseWorkFlowService getWorkFlowService() {
		return this.service;
	}
}
