package com.itfreer.gzclsq.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.api.BpmBaseRestService;
import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.form.api.BaseService;
import com.itfreer.gzclsq.entity.GzclsqEntity;

/**
 * 定义车辆使用Rest服务接口
 */
@Component
@Path("/gzclsq/")
public class RestGzclsqService extends BpmBaseRestService<GzclsqEntity> {
	
	@Autowired
	private GzclsqService service;
	@Override
	protected String getServiceName() {
		return "gzclsq";
	}

	@Override
	public IBaseWorkFlowService getWorkFlowService() {
		return service;
	}
}
