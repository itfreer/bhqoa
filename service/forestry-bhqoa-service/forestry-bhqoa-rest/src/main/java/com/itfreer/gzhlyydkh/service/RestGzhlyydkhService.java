package com.itfreer.gzhlyydkh.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzhlyydkh.entity.GzhlyydkhEntity;
import com.itfreer.gzhlyydkh.service.GzhlyydkhService;

/**
 * 定义护林员月度考核Rest服务接口
 */
@Component
@Path("/gzhlyydkh/")
public class RestGzhlyydkhService extends BaseRestService<GzhlyydkhEntity> {
	
	@Autowired
	private GzhlyydkhService service;

	@Override
	protected BaseService<GzhlyydkhEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzhlyydkh";
	}
}
