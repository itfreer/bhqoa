package com.itfreer.gzplanpersonnel.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzplanpersonnel.entity.GzplanpersonnelEntity;
import com.itfreer.gzplanpersonnel.service.GzplanpersonnelService;

/**
 * 定义项目规划人员Rest服务接口
 */
@Component
@Path("/gzplanpersonnel/")
public class RestGzplanpersonnelService extends BaseRestService<GzplanpersonnelEntity> {
	
	@Autowired
	private GzplanpersonnelService service;

	@Override
	protected BaseService<GzplanpersonnelEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzplanpersonnel";
	}
}
