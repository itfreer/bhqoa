package com.itfreer.gzlytjaj01.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzlytjaj01.entity.Gzlytjaj01Entity;
import com.itfreer.gzlytjaj01.service.Gzlytjaj01Service;

/**
 * 定义林业统计案件Rest服务接口
 */
@Component
@Path("/gzlytjaj01/")
public class RestGzlytjaj01Service extends BaseRestService<Gzlytjaj01Entity> {
	
	@Autowired
	private Gzlytjaj01Service service;

	@Override
	protected BaseService<Gzlytjaj01Entity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzlytjaj01";
	}
}
