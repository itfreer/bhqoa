package com.itfreer.gzsqxmtjview.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzsqxmtjview.entity.GzsqxmtjviewEntity;
import com.itfreer.gzsqxmtjview.service.GzsqxmtjviewService;

/**
 * 定义社区项目统计(视图)Rest服务接口
 */
@Component
@Path("/gzsqxmtjview/")
public class RestGzsqxmtjviewService extends BaseRestService<GzsqxmtjviewEntity> {
	
	@Autowired
	private GzsqxmtjviewService service;

	@Override
	protected BaseService<GzsqxmtjviewEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzsqxmtjview";
	}
}
