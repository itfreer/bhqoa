package com.itfreer.gzprojectinfo.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.bpm.api.BpmBaseRestService;
import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.gzprojectinfo.entity.GzprojectinfoEntity;

/**
 * 定义项目立项管理Rest服务接口
 */
@Component
@Path("/gzprojectinfo/")
public class RestGzprojectinfoService extends BpmBaseRestService<GzprojectinfoEntity> {
	
	@Autowired
	private GzprojectinfoService service;

	@Override
	protected String getServiceName() {
		return "gzprojectinfo";
	}

	@Override
	public IBaseWorkFlowService getWorkFlowService() {
		return service;
	}
}
