package com.itfreer.gzschedule.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzschedule.entity.ScheduleEntity;
import com.itfreer.gzschedule.service.ScheduleService;

/**
 * 定义日常安排Rest服务接口
 */
@Component
@Path("/schedule/")
public class RestScheduleService extends BaseRestService<ScheduleEntity> {
	
	@Autowired
	private ScheduleService service;

	@Override
	protected BaseService<ScheduleEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "schedule";
	}
}
