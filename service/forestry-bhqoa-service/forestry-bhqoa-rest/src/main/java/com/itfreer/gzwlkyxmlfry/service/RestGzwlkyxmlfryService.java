package com.itfreer.gzwlkyxmlfry.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzwlkyxmlfry.entity.GzwlkyxmlfryEntity;
import com.itfreer.gzwlkyxmlfry.service.GzwlkyxmlfryService;

/**
 * 定义外来科研项目人随访人员Rest服务接口
 */
@Component
@Path("/gzwlkyxmlfry/")
public class RestGzwlkyxmlfryService extends BaseRestService<GzwlkyxmlfryEntity> {
	
	@Autowired
	private GzwlkyxmlfryService service;

	@Override
	protected BaseService<GzwlkyxmlfryEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzwlkyxmlfry";
	}
}
