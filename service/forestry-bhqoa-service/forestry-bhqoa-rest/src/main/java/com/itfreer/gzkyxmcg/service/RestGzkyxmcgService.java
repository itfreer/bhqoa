package com.itfreer.gzkyxmcg.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzkyxmcg.entity.GzkyxmcgEntity;
import com.itfreer.gzkyxmcg.service.GzkyxmcgService;

/**
 * 定义科研项目成果Rest服务接口
 */
@Component
@Path("/gzkyxmcg/")
public class RestGzkyxmcgService extends BaseRestService<GzkyxmcgEntity> {
	
	@Autowired
	private GzkyxmcgService service;

	@Override
	protected BaseService<GzkyxmcgEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzkyxmcg";
	}
}
