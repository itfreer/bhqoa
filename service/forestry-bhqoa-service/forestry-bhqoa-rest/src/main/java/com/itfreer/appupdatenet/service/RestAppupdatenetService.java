package com.itfreer.appupdatenet.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.appupdatenet.entity.AppupdatenetEntity;
import com.itfreer.appupdatenet.service.AppupdatenetService;

/**
 * 定义APP更新Rest服务接口
 */
@Component
@Path("/appupdatenet/")
public class RestAppupdatenetService extends BaseRestService<AppupdatenetEntity> {
	
	@Autowired
	private AppupdatenetService service;

	@Override
	protected BaseService<AppupdatenetEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "appupdatenet";
	}
}
