package com.itfreer.gzvisitorreception.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzvisitorreception.entity.GzvisitorreceptionEntity;
import com.itfreer.gzvisitorreception.service.GzvisitorreceptionService;

/**
 * 定义访问接待管理Rest服务接口
 */
@Component
@Path("/gzvisitorreception/")
public class RestGzvisitorreceptionService extends BaseRestService<GzvisitorreceptionEntity> {
	
	@Autowired
	private GzvisitorreceptionService service;

	@Override
	protected BaseService<GzvisitorreceptionEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzvisitorreception";
	}
}
