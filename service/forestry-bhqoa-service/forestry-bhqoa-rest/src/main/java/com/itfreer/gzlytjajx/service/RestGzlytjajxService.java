package com.itfreer.gzlytjajx.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzlytjajx.entity.GzlytjajxEntity;
import com.itfreer.gzlytjajx.service.GzlytjajxService;

/**
 * 定义林业统计案件续Rest服务接口
 */
@Component
@Path("/gzlytjajx/")
public class RestGzlytjajxService extends BaseRestService<GzlytjajxEntity> {
	
	@Autowired
	private GzlytjajxService service;

	@Override
	protected BaseService<GzlytjajxEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzlytjajx";
	}
}
