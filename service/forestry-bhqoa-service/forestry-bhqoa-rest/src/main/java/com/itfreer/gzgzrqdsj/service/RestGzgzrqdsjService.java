package com.itfreer.gzgzrqdsj.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzgzrqdsj.entity.GzgzrqdsjEntity;
import com.itfreer.gzgzrqdsj.service.GzgzrqdsjService;

/**
 * 定义签到时间Rest服务接口
 */
@Component
@Path("/gzgzrqdsj/")
public class RestGzgzrqdsjService extends BaseRestService<GzgzrqdsjEntity> {
	
	@Autowired
	private GzgzrqdsjService service;

	@Override
	protected BaseService<GzgzrqdsjEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzgzrqdsj";
	}
}
