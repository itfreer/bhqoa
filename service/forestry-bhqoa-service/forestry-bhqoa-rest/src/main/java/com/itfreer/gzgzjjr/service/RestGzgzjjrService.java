package com.itfreer.gzgzjjr.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzgzjjr.entity.GzgzjjrEntity;
import com.itfreer.gzgzjjr.service.GzgzjjrService;

/**
 * 定义工作节假日Rest服务接口
 */
@Component
@Path("/gzgzjjr/")
public class RestGzgzjjrService extends BaseRestService<GzgzjjrEntity> {
	
	@Autowired
	private GzgzjjrService service;

	@Override
	protected BaseService<GzgzjjrEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzgzjjr";
	}
}
