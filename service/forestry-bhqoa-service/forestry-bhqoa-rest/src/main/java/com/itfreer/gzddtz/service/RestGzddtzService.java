package com.itfreer.gzddtz.service;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.itfreer.form.api.BaseRestService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzddtz.entity.GzddtzEntity;
import com.itfreer.gzddtz.service.GzddtzService;

/**
 * 定义调度台账Rest服务接口
 */
@Component
@Path("/gzddtz/")
public class RestGzddtzService extends BaseRestService<GzddtzEntity> {
	
	@Autowired
	private GzddtzService service;

	@Override
	protected BaseService<GzddtzEntity> getService() {
		return service;
	}
	
	@Override
	protected String getServiceName() {
		return "gzddtz";
	}
}
