package com.itfreer.gzreceivefile.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzreceivefile.entity.GzreceivefileEntity;

/**
 * 定义收文管理接口
 */
public interface GzreceivefileService extends BaseService<GzreceivefileEntity> {
	
}
