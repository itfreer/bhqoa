package com.itfreer.gzrskqtj.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzrskqtj.entity.GzrskqtjEntity;

/**
 * 定义考勤统计接口
 */
public interface GzrskqtjService extends BaseService<GzrskqtjEntity> {
	
}
