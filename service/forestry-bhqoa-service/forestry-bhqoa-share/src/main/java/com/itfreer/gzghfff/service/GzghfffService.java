package com.itfreer.gzghfff.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzghfff.entity.GzghfffEntity;

/**
 * 定义管护费发放接口
 */
public interface GzghfffService extends BaseService<GzghfffEntity> {
	
}
