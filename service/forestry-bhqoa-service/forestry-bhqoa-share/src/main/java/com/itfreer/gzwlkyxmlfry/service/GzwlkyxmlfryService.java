package com.itfreer.gzwlkyxmlfry.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzwlkyxmlfry.entity.GzwlkyxmlfryEntity;

/**
 * 定义外来科研项目人随访人员接口
 */
public interface GzwlkyxmlfryService extends BaseService<GzwlkyxmlfryEntity> {
	
}
