package com.itfreer.gzclda.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzclda.entity.GzcldaEntity;

/**
 * 定义车辆档案接口
 */
public interface GzcldaService extends BaseService<GzcldaEntity> {
	
}
