package com.itfreer.gzhlyydkh.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzhlyydkh.entity.GzhlyydkhEntity;

/**
 * 定义护林员月度考核接口
 */
public interface GzhlyydkhService extends BaseService<GzhlyydkhEntity> {
	
}
