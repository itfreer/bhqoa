package com.itfreer.gzclwx.entity;

import java.io.Serializable;

import javax.persistence.Column;

import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义车辆维修实体
 */
public class GzclwxEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	private String id;
	
	
	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	private String sclxx;
	
	/**
	 * 维修时间
	 */
	private java.util.Date dwxsj;
	
	
	/**
	 * 维修原因
	 */
	private String swxyy;
	
	
	/**
	 * 维修价格
	 */
	private String swxjg;
	
	
	/**
	 * 维修地点
	 */
	private String swxdd;
	
	
	/**
	 * 负责人
	 */
	private String sfzr;
	
	
	/**
	 * 负责人名称
	 */
	private String sfzrmc;
	
	
	public String getSfzrmc() {
		return sfzrmc;
	}

	public void setSfzrmc(String sfzrmc) {
		this.sfzrmc = sfzrmc;
	}
	
	
	/**
	 * 联系方式
	 */
	private String slxff;
	
	
	/**
	 * 维修费用
	 */
	private String swxfy;
	
	
	/**
	 * 电子发票
	 */
	private String sdzfp;
	
	
	/**
	 * 车辆型号
	 */
	private String clxh;
	
	/**
	 * 车牌号
	 */
	private String cph;
	
	
	/**
	 * 维修状态
	 */
	@DictionaryField(dictionaryName = "p_wxzt" ,toFieldName = "wxztName")
	private String wxzt;
	
	
	/**
	 * 维修状态
	 */
	private String wxztName;
	

	public String getWxztName() {
		return wxztName;
	}

	public void setWxztName(String wxztName) {
		this.wxztName = wxztName;
	}

	public String getWxzt() {
		return wxzt;
	}

	public void setWxzt(String wxzt) {
		this.wxzt = wxzt;
	}
	

	public String getClxh() {
		return clxh;
	}

	public void setClxh(String clxh) {
		this.clxh = clxh;
	}

	public String getCph() {
		return cph;
	}

	public void setCph(String cph) {
		this.cph = cph;
	}

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	public String getSclxx() {
		return sclxx;
	}

	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	public void setSclxx(String value) {
		this.sclxx = value;
	}
	/**
	 * 维修时间
	 */
	public java.util.Date getDwxsj() {
		return dwxsj;
	}

	/**
	 * 维修时间
	 */
	public void setDwxsj(java.util.Date value) {
		this.dwxsj = value;
	}
	/**
	 * 维修原因
	 */
	public String getSwxyy() {
		return swxyy;
	}

	/**
	 * 维修原因
	 */
	public void setSwxyy(String value) {
		this.swxyy = value;
	}
	/**
	 * 维修价格
	 */
	public String getSwxjg() {
		return swxjg;
	}

	/**
	 * 维修价格
	 */
	public void setSwxjg(String value) {
		this.swxjg = value;
	}
	/**
	 * 维修地点
	 */
	public String getSwxdd() {
		return swxdd;
	}

	/**
	 * 维修地点
	 */
	public void setSwxdd(String value) {
		this.swxdd = value;
	}
	/**
	 * 负责人
	 */
	public String getSfzr() {
		return sfzr;
	}

	/**
	 * 负责人
	 */
	public void setSfzr(String value) {
		this.sfzr = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxff() {
		return slxff;
	}

	/**
	 * 联系方式
	 */
	public void setSlxff(String value) {
		this.slxff = value;
	}
	/**
	 * 维修费用
	 */
	public String getSwxfy() {
		return swxfy;
	}

	/**
	 * 维修费用
	 */
	public void setSwxfy(String value) {
		this.swxfy = value;
	}
	/**
	 * 电子发票
	 */
	public String getSdzfp() {
		return sdzfp;
	}

	/**
	 * 电子发票
	 */
	public void setSdzfp(String value) {
		this.sdzfp = value;
	}
}