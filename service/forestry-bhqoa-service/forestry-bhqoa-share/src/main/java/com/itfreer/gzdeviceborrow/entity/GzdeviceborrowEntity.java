package com.itfreer.gzdeviceborrow.entity;

import java.io.Serializable;

import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义设备借还实体
 */
public class GzdeviceborrowEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	/**
	 * I_VERSION
	 */
	private Integer iversion;
	
	
	/**
	 * 申请编号
	 */
	private String ssqbh;
	
	/**
	 * 申请日期
	 */
	private java.util.Date dsqrq;
	
	
	/**
	 * 使用单位
	 */
	@DictionaryField(dictionaryName="p_department", toFieldName="ssydwName")
	private String ssydw;
	
	
	/**
	 * 使用单位
	 */
	private String ssydwName;
	
	
	public String getSsyxmName() {
		return ssydwName;
	}

	public void setSsyxmName(String ssyxmName) {
		this.ssydwName = ssyxmName;
	}
	
	/**
	 * 使用项目
	 */
	private String ssyxm;
	
	/**
	 * 申请人
	 */
	private String ssqr;
	
	/**
	 * 预还日期
	 */
	private java.util.Date dyhrq;
	
	/**
	 * 归还日期
	 */
	private java.util.Date dghrq;
	
	
	/**
	 * 归还人
	 */
	private String sghr;
	
	
	/**
	 * 备注
	 */
	private String sbz;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * I_VERSION
	 */
	public Integer getIversion() {
		return iversion;
	}

	/**
	 * I_VERSION
	 */
	public void setIversion(Integer value) {
		this.iversion = value;
	}
	/**
	 * 申请编号
	 */
	public String getSsqbh() {
		return ssqbh;
	}

	/**
	 * 申请编号
	 */
	public void setSsqbh(String value) {
		this.ssqbh = value;
	}
	/**
	 * 申请日期
	 */
	public java.util.Date getDsqrq() {
		return dsqrq;
	}

	/**
	 * 申请日期
	 */
	public void setDsqrq(java.util.Date value) {
		this.dsqrq = value;
	}
	/**
	 * 使用单位
	 */
	public String getSsydw() {
		return ssydw;
	}

	/**
	 * 使用单位
	 */
	public void setSsydw(String value) {
		this.ssydw = value;
	}
	/**
	 * 使用项目
	 */
	public String getSsyxm() {
		return ssyxm;
	}

	/**
	 * 使用项目
	 */
	public void setSsyxm(String value) {
		this.ssyxm = value;
	}
	/**
	 * 申请人
	 */
	public String getSsqr() {
		return ssqr;
	}

	/**
	 * 申请人
	 */
	public void setSsqr(String value) {
		this.ssqr = value;
	}
	/**
	 * 预还日期
	 */
	public java.util.Date getDyhrq() {
		return dyhrq;
	}

	/**
	 * 预还日期
	 */
	public void setDyhrq(java.util.Date value) {
		this.dyhrq = value;
	}
	/**
	 * 归还日期
	 */
	public java.util.Date getDghrq() {
		return dghrq;
	}

	/**
	 * 归还日期
	 */
	public void setDghrq(java.util.Date value) {
		this.dghrq = value;
	}
	/**
	 * 归还人
	 */
	public String getSghr() {
		return sghr;
	}

	/**
	 * 归还人
	 */
	public void setSghr(String value) {
		this.sghr = value;
	}
	/**
	 * 备注
	 */
	public String getSbz() {
		return sbz;
	}

	/**
	 * 备注
	 */
	public void setSbz(String value) {
		this.sbz = value;
	}
}