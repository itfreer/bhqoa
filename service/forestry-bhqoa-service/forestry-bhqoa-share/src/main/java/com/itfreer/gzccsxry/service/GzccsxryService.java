package com.itfreer.gzccsxry.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzccsxry.entity.GzccsxryEntity;

/**
 * 定义出差随行人员接口
 */
public interface GzccsxryService extends BaseService<GzccsxryEntity> {
	
}
