package com.itfreer.gzgzjjr.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzgzjjr.entity.GzgzjjrEntity;

/**
 * 定义工作节假日接口
 */
public interface GzgzjjrService extends BaseService<GzgzjjrEntity> {
	
}
