package com.itfreer.gzhlygzgl.entity;

import java.io.Serializable;

/**
 * 定义护林员工资管理实体
 */
public class GzhlygzglEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 姓名
	 */
	private String xm;
	
	
	/**
	 * 用户ID
	 */
	private String yhid;
	
	/**
	 * 基础工资
	 */
	private Double jcgz;
	
	/**
	 * 浮动工资
	 */
	private Double fdgz;
	
	/**
	 * 绩效工资
	 */
	private Double jxgz;
	
	/**
	 * 工龄工资
	 */
	private Double glgz;
	
	/**
	 * 工作年限
	 */
	private Double gznx;
	
	/**
	 * 参与工作时间
	 */
	private java.util.Date cygzsj;
	
	
	/**
	 * 地点林班
	 */
	private String ddlb;
	
	
	/**
	 * 小班号
	 */
	private String xbh;
	
	/**
	 * 面积
	 */
	private Double mj;
	
	
	/**
	 * 合同号
	 */
	private String hth;
	
	
	/**
	 * 合计
	 */
	private String hj;
	
	/**
	 * 年度
	 */
	private Integer nd;
	
	/**
	 * 月份
	 */
	private Integer yf;
	
	/**
	 * 制表时间
	 */
	private java.util.Date zbsj;
	
	
	/**
	 * 单位负责人ID
	 */
	private String dwfzrid;
	
	
	/**
	 * 单位负责人名称
	 */
	private String dwfzrmc;
	
	
	/**
	 * 会计ID
	 */
	private String kjid;
	
	
	/**
	 * 会计名称
	 */
	private String kjmc;
	
	
	/**
	 * 出纳ID
	 */
	private String cnid;
	
	
	/**
	 * 出纳名称
	 */
	private String cnmc;
	
	
	/**
	 * 护林站ID
	 */
	private String hlzid;
	
	
	/**
	 * 护林站名称
	 */
	private String hlzmc;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 姓名
	 */
	public String getXm() {
		return xm;
	}

	/**
	 * 姓名
	 */
	public void setXm(String value) {
		this.xm = value;
	}
	/**
	 * 用户ID
	 */
	public String getYhid() {
		return yhid;
	}

	/**
	 * 用户ID
	 */
	public void setYhid(String value) {
		this.yhid = value;
	}
	/**
	 * 基础工资
	 */
	public Double getJcgz() {
		return jcgz;
	}

	/**
	 * 基础工资
	 */
	public void setJcgz(Double value) {
		this.jcgz = value;
	}
	/**
	 * 浮动工资
	 */
	public Double getFdgz() {
		return fdgz;
	}

	/**
	 * 浮动工资
	 */
	public void setFdgz(Double value) {
		this.fdgz = value;
	}
	/**
	 * 绩效工资
	 */
	public Double getJxgz() {
		return jxgz;
	}

	/**
	 * 绩效工资
	 */
	public void setJxgz(Double value) {
		this.jxgz = value;
	}
	/**
	 * 工龄工资
	 */
	public Double getGlgz() {
		return glgz;
	}

	/**
	 * 工龄工资
	 */
	public void setGlgz(Double value) {
		this.glgz = value;
	}
	/**
	 * 工作年限
	 */
	public Double getGznx() {
		return gznx;
	}

	/**
	 * 工作年限
	 */
	public void setGznx(Double value) {
		this.gznx = value;
	}
	/**
	 * 参与工作时间
	 */
	public java.util.Date getCygzsj() {
		return cygzsj;
	}

	/**
	 * 参与工作时间
	 */
	public void setCygzsj(java.util.Date value) {
		this.cygzsj = value;
	}
	/**
	 * 地点林班
	 */
	public String getDdlb() {
		return ddlb;
	}

	/**
	 * 地点林班
	 */
	public void setDdlb(String value) {
		this.ddlb = value;
	}
	/**
	 * 小班号
	 */
	public String getXbh() {
		return xbh;
	}

	/**
	 * 小班号
	 */
	public void setXbh(String value) {
		this.xbh = value;
	}
	/**
	 * 面积
	 */
	public Double getMj() {
		return mj;
	}

	/**
	 * 面积
	 */
	public void setMj(Double value) {
		this.mj = value;
	}
	/**
	 * 合同号
	 */
	public String getHth() {
		return hth;
	}

	/**
	 * 合同号
	 */
	public void setHth(String value) {
		this.hth = value;
	}
	/**
	 * 合计
	 */
	public String getHj() {
		return hj;
	}

	/**
	 * 合计
	 */
	public void setHj(String value) {
		this.hj = value;
	}
	/**
	 * 年度
	 */
	public Integer getNd() {
		return nd;
	}

	/**
	 * 年度
	 */
	public void setNd(Integer value) {
		this.nd = value;
	}
	/**
	 * 月份
	 */
	public Integer getYf() {
		return yf;
	}

	/**
	 * 月份
	 */
	public void setYf(Integer value) {
		this.yf = value;
	}
	/**
	 * 制表时间
	 */
	public java.util.Date getZbsj() {
		return zbsj;
	}

	/**
	 * 制表时间
	 */
	public void setZbsj(java.util.Date value) {
		this.zbsj = value;
	}
	/**
	 * 单位负责人ID
	 */
	public String getDwfzrid() {
		return dwfzrid;
	}

	/**
	 * 单位负责人ID
	 */
	public void setDwfzrid(String value) {
		this.dwfzrid = value;
	}
	/**
	 * 单位负责人名称
	 */
	public String getDwfzrmc() {
		return dwfzrmc;
	}

	/**
	 * 单位负责人名称
	 */
	public void setDwfzrmc(String value) {
		this.dwfzrmc = value;
	}
	/**
	 * 会计ID
	 */
	public String getKjid() {
		return kjid;
	}

	/**
	 * 会计ID
	 */
	public void setKjid(String value) {
		this.kjid = value;
	}
	/**
	 * 会计名称
	 */
	public String getKjmc() {
		return kjmc;
	}

	/**
	 * 会计名称
	 */
	public void setKjmc(String value) {
		this.kjmc = value;
	}
	/**
	 * 出纳ID
	 */
	public String getCnid() {
		return cnid;
	}

	/**
	 * 出纳ID
	 */
	public void setCnid(String value) {
		this.cnid = value;
	}
	/**
	 * 出纳名称
	 */
	public String getCnmc() {
		return cnmc;
	}

	/**
	 * 出纳名称
	 */
	public void setCnmc(String value) {
		this.cnmc = value;
	}
	/**
	 * 护林站ID
	 */
	public String getHlzid() {
		return hlzid;
	}

	/**
	 * 护林站ID
	 */
	public void setHlzid(String value) {
		this.hlzid = value;
	}
	/**
	 * 护林站名称
	 */
	public String getHlzmc() {
		return hlzmc;
	}

	/**
	 * 护林站名称
	 */
	public void setHlzmc(String value) {
		this.hlzmc = value;
	}
}