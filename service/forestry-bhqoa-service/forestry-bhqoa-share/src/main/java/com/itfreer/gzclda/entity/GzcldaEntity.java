package com.itfreer.gzclda.entity;

import java.io.Serializable;

import javax.persistence.Column;

import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义车辆档案实体
 */
public class GzcldaEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	private String id;
	
	
	/**
	 * 车辆型号
	 */
	private String sclxh;
	
	
	/**
	 * 车辆信息
	 */
	@DictionaryField(dictionaryName="p_zuoci", toFieldName="sclxxName")
	private String sclxx;
	
	
	/**
	 * 车辆信息
	 */
	private String sclxxName;
	
	
	public String getSclxxName() {
		return sclxxName;
	}

	public void setSclxxName(String sclxxName) {
		this.sclxxName = sclxxName;
	}

	/**
	 * 车牌号
	 */
	private String scph;
	
	
	/**
	 * 保修期限
	 */
	private String sbxqx;
	
	
	/**
	 * 采购日期
	 */
	private String dcgrq;
	
	
	/**
	 * 采购价格
	 */
	private String scgjg;
	
	
	/**
	 * 采购部门
	 */
	@DictionaryField(dictionaryName = "p_department" ,toFieldName = "scgbmName")
	private String scgbm;
	
	
	/**
	 * 采购部门
	 */
	private String scgbmName;
	
	
	public String getScgbmName() {
		return scgbmName;
	}

	public void setScgbmName(String scgbmName) {
		this.scgbmName = scgbmName;
	}

	/**
	 * 采购人
	 */
	private String scgr;
	
	
	/**
	 * 采购人名称
	 */
	private String scgrmc;
	
	
	public String getScgrmc() {
		return scgrmc;
	}

	public void setScgrmc(String scgrmc) {
		this.scgrmc = scgrmc;
	}

	/**
	 * 采购原因
	 */
	private String scgyy;
	
	
	/**
	 * 联系方式
	 */
	private String slxff;
	
	
	/**
	 * 使用情况
	 */
	private String ssyqk;
	
	
	/**
	 * 维修情况
	 */
	private String swxqk;
	
	
	/**
	 * 油卡充值情况
	 */
	private String sykczqk;
	
	
	/**
	 * 车辆信息附件
	 */
	private String clxxfj;
	
	
	/**
	 * 车辆状态
	 */
	@DictionaryField(dictionaryName = "p_clzt" ,toFieldName = "clztName")
	private String clzt;
	
	
	/**
	 * 车辆状态
	 */
	private String clztName;
	

	public String getClztName() {
		return clztName;
	}

	public void setClztName(String clztName) {
		this.clztName = clztName;
	}

	public String getClxxfj() {
		return clxxfj;
	}

	public void setClxxfj(String clxxfj) {
		this.clxxfj = clxxfj;
	}

	public String getClzt() {
		return clzt;
	}

	public void setClzt(String clzt) {
		this.clzt = clzt;
	}
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 车辆型号
	 */
	public String getSclxh() {
		return sclxh;
	}

	/**
	 * 车辆型号
	 */
	public void setSclxh(String value) {
		this.sclxh = value;
	}
	/**
	 * 车辆信息
	 */
	public String getSclxx() {
		return sclxx;
	}

	/**
	 * 车辆信息
	 */
	public void setSclxx(String value) {
		this.sclxx = value;
	}
	/**
	 * 车牌号
	 */
	public String getScph() {
		return scph;
	}

	/**
	 * 车牌号
	 */
	public void setScph(String value) {
		this.scph = value;
	}
	/**
	 * 保修期限
	 */
	public String getSbxqx() {
		return sbxqx;
	}

	/**
	 * 保修期限
	 */
	public void setSbxqx(String value) {
		this.sbxqx = value;
	}
	/**
	 * 采购日期
	 */
	public String getDcgrq() {
		return dcgrq;
	}

	/**
	 * 采购日期
	 */
	public void setDcgrq(String value) {
		this.dcgrq = value;
	}
	/**
	 * 采购价格
	 */
	public String getScgjg() {
		return scgjg;
	}

	/**
	 * 采购价格
	 */
	public void setScgjg(String value) {
		this.scgjg = value;
	}
	/**
	 * 采购部门
	 */
	public String getScgbm() {
		return scgbm;
	}

	/**
	 * 采购部门
	 */
	public void setScgbm(String value) {
		this.scgbm = value;
	}
	/**
	 * 采购人
	 */
	public String getScgr() {
		return scgr;
	}

	/**
	 * 采购人
	 */
	public void setScgr(String value) {
		this.scgr = value;
	}
	/**
	 * 采购原因
	 */
	public String getScgyy() {
		return scgyy;
	}

	/**
	 * 采购原因
	 */
	public void setScgyy(String value) {
		this.scgyy = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxff() {
		return slxff;
	}

	/**
	 * 联系方式
	 */
	public void setSlxff(String value) {
		this.slxff = value;
	}
	/**
	 * 使用情况
	 */
	public String getSsyqk() {
		return ssyqk;
	}

	/**
	 * 使用情况
	 */
	public void setSsyqk(String value) {
		this.ssyqk = value;
	}
	/**
	 * 维修情况
	 */
	public String getSwxqk() {
		return swxqk;
	}

	/**
	 * 维修情况
	 */
	public void setSwxqk(String value) {
		this.swxqk = value;
	}
	/**
	 * 油卡充值情况
	 */
	public String getSykczqk() {
		return sykczqk;
	}

	/**
	 * 油卡充值情况
	 */
	public void setSykczqk(String value) {
		this.sykczqk = value;
	}
}