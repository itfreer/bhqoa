package com.itfreer.gzddtj.entity;

import java.io.Serializable;

/**
 * 定义调度统计实体
 */
public class GzddtjEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	private String id;
	
	
	/**
	 * 调度年度
	 */
	private String sddnd;
	
	
	/**
	 * 调度月份
	 */
	private String sddyf;
	
	
	/**
	 * 资源类型
	 */
	private String szylx;
	
	
	/**
	 * 使用次数
	 */
	private String ssycs;
	
	
	/**
	 * 成本消耗
	 */
	private String scbxh;
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 调度年度
	 */
	public String getSddnd() {
		return sddnd;
	}

	/**
	 * 调度年度
	 */
	public void setSddnd(String value) {
		this.sddnd = value;
	}
	/**
	 * 调度月份
	 */
	public String getSddyf() {
		return sddyf;
	}

	/**
	 * 调度月份
	 */
	public void setSddyf(String value) {
		this.sddyf = value;
	}
	/**
	 * 资源类型
	 */
	public String getSzylx() {
		return szylx;
	}

	/**
	 * 资源类型
	 */
	public void setSzylx(String value) {
		this.szylx = value;
	}
	/**
	 * 使用次数
	 */
	public String getSsycs() {
		return ssycs;
	}

	/**
	 * 使用次数
	 */
	public void setSsycs(String value) {
		this.ssycs = value;
	}
	/**
	 * 成本消耗
	 */
	public String getScbxh() {
		return scbxh;
	}

	/**
	 * 成本消耗
	 */
	public void setScbxh(String value) {
		this.scbxh = value;
	}
}