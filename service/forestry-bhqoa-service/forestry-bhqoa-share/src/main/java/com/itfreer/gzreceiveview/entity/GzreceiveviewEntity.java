package com.itfreer.gzreceiveview.entity;

import java.io.Serializable;

import javax.persistence.Column;

import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义收文管理实体
 */
public class GzreceiveviewEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;
	
	/**
	 * 紧急等级
	 */
	@DictionaryField(dictionaryName = "p_jjdj" ,toFieldName = "jjdjName")
	private String jjdj;
	
	
	/**
	 * 紧急等级
	 */
	private String jjdjName;
	
	
	public String getJjdjName() {
		return jjdjName;
	}

	public void setJjdjName(String jjdjName) {
		this.jjdjName = jjdjName;
	}

	public String getJjdj() {
		return jjdj;
	}

	public void setJjdj(String jjdj) {
		this.jjdj = jjdj;
	}
	
	/**
	 * 部门名称
	 */
	@DictionaryField(dictionaryName = "p_department" ,toFieldName = "bmmcName")
	private String bmmc;
	
	private String bmmcName;
	
	public String getBmmc() {
		return bmmc;
	}

	public void setBmmc(String bmmc) {
		this.bmmc = bmmc;
	}

	public String getBmmcName() {
		return bmmcName;
	}

	public void setBmmcName(String bmmcName) {
		this.bmmcName = bmmcName;
	}

	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 发文主题
	 */
	private String fwzt;
	
	
	/**
	 * 发文说明
	 */
	private String fwsm;
	
	
	/**
	 * 创建人ID
	 */
	private String cjrid;
	
	
	/**
	 * 创建人名称
	 */
	private String cjrmc;
	
	/**
	 * 创建时间
	 */
	private java.util.Date cjsj;
	
	
	/**
	 * 附件
	 */
	private String fj;
	
	
	/**
	 * 发文ID
	 */
	private String fwid;
	
	
	/**
	 * 接收人ID
	 */
	private String jsrid;
	
	
	/**
	 * 接收人名称
	 */
	private String jsrmc;
	
	
	/**
	 * 是否已读
	 */
	@DictionaryField(dictionaryName="p_yesorno", toFieldName="sfydName")
	private String sfyd;
	
	
	/**
	 * 是否已读
	 */
	private String sfydName;
	
	/**
	 * 阅读时间
	 */
	private java.util.Date ydsj;
	
	
	/**
	 * 是否已确认
	 */
	@DictionaryField(dictionaryName="p_yesorno", toFieldName="sfqrName")
	private String sfqr;
	
	
	/**
	 * 是否已确认
	 */
	private String sfqrName;
	
	
	public String getSfydName() {
		return sfydName;
	}

	public void setSfydName(String sfydName) {
		this.sfydName = sfydName;
	}

	public String getSfqrName() {
		return sfqrName;
	}

	public void setSfqrName(String sfqrName) {
		this.sfqrName = sfqrName;
	}
	
	
	/**
	 * 接收人机构
	 */
	@DictionaryField(dictionaryName = "p_department" ,toFieldName = "jsrjgmc")
	private String jsrjg;
	
	/**
	 * 接收人机构
	 */
	private String jsrjgmc;
	
	
	public String getJsrjgmc() {
		return jsrjgmc;
	}

	public void setJsrjgmc(String jsrjgmc) {
		this.jsrjgmc = jsrjgmc;
	}

	/**
	 * 接收人意见
	 */
	private String jsryj;
	
	
	public String getJsryj() {
		return jsryj;
	}

	public void setJsryj(String jsryj) {
		this.jsryj = jsryj;
	}
	
	/**
	 * 接收人职位
	 */
	private String jsrzw;
	
	public String getJsrzw() {
		return jsrzw;
	}

	public void setJsrzw(String jsrzw) {
		this.jsrzw = jsrzw;
	}
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 发文主题
	 */
	public String getFwzt() {
		return fwzt;
	}

	/**
	 * 发文主题
	 */
	public void setFwzt(String value) {
		this.fwzt = value;
	}
	/**
	 * 发文说明
	 */
	public String getFwsm() {
		return fwsm;
	}

	/**
	 * 发文说明
	 */
	public void setFwsm(String value) {
		this.fwsm = value;
	}
	/**
	 * 创建人ID
	 */
	public String getCjrid() {
		return cjrid;
	}

	/**
	 * 创建人ID
	 */
	public void setCjrid(String value) {
		this.cjrid = value;
	}
	/**
	 * 创建人名称
	 */
	public String getCjrmc() {
		return cjrmc;
	}

	/**
	 * 创建人名称
	 */
	public void setCjrmc(String value) {
		this.cjrmc = value;
	}
	/**
	 * 创建时间
	 */
	public java.util.Date getCjsj() {
		return cjsj;
	}

	/**
	 * 创建时间
	 */
	public void setCjsj(java.util.Date value) {
		this.cjsj = value;
	}
	/**
	 * 附件
	 */
	public String getFj() {
		return fj;
	}

	/**
	 * 附件
	 */
	public void setFj(String value) {
		this.fj = value;
	}
	/**
	 * 发文ID
	 */
	public String getFwid() {
		return fwid;
	}

	/**
	 * 发文ID
	 */
	public void setFwid(String value) {
		this.fwid = value;
	}
	/**
	 * 接收人ID
	 */
	public String getJsrid() {
		return jsrid;
	}

	/**
	 * 接收人ID
	 */
	public void setJsrid(String value) {
		this.jsrid = value;
	}
	/**
	 * 接收人名称
	 */
	public String getJsrmc() {
		return jsrmc;
	}

	/**
	 * 接收人名称
	 */
	public void setJsrmc(String value) {
		this.jsrmc = value;
	}
	/**
	 * 是否已读
	 */
	public String getSfyd() {
		return sfyd;
	}

	/**
	 * 是否已读
	 */
	public void setSfyd(String value) {
		this.sfyd = value;
	}
	/**
	 * 阅读时间
	 */
	public java.util.Date getYdsj() {
		return ydsj;
	}

	/**
	 * 阅读时间
	 */
	public void setYdsj(java.util.Date value) {
		this.ydsj = value;
	}
	/**
	 * 是否已确认
	 */
	public String getSfqr() {
		return sfqr;
	}

	/**
	 * 是否已确认
	 */
	public void setSfqr(String value) {
		this.sfqr = value;
	}
	/**
	 * 接收人机构
	 */
	public String getJsrjg() {
		return jsrjg;
	}

	/**
	 * 接收人机构
	 */
	public void setJsrjg(String value) {
		this.jsrjg = value;
	}
	
	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	
	/**
	 * 办理情况
	 */
	@DictionaryField(dictionaryName = "GZ_GWCLQK" ,toFieldName = "blqkName")
	private String blqk;
	
	/**
	 * 办理情况
	 */
	private String blqkName;
	
	public String getBlqk() {
		return blqk;
	}

	public void setBlqk(String blqk) {
		this.blqk = blqk;
	}
	
	public String getBlqkName() {
		return blqkName;
	}

	public void setBlqkName(String blqkName) {
		this.blqkName = blqkName;
	}
	
	/**
	 * 主要负责人
	 */
	private String zyfzrid;
	
	/**
	 * 主要负责人
	 */
	private String zyfzrname;


	public String getZyfzrid() {
		return zyfzrid;
	}

	public void setZyfzrid(String zyfzrid) {
		this.zyfzrid = zyfzrid;
	}

	public String getZyfzrname() {
		return zyfzrname;
	}

	public void setZyfzrname(String zyfzrname) {
		this.zyfzrname = zyfzrname;
	}
}