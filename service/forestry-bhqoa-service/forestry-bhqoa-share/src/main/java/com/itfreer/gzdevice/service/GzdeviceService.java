package com.itfreer.gzdevice.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzdevice.entity.GzdeviceEntity;

/**
 * 定义设备接口
 */
public interface GzdeviceService extends BaseService<GzdeviceEntity> {
	
}
