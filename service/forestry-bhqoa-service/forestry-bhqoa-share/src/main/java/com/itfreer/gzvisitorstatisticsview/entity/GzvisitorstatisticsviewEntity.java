package com.itfreer.gzvisitorstatisticsview.entity;

import java.io.Serializable;

import javax.persistence.Column;

/**
 * 定义访客统计（视图）实体
 */
public class GzvisitorstatisticsviewEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;
	/**
	 * 接待部门
	 */
	private String jdbmmc;
	
	
	/**
	 * 来访总人数
	 */
	private String lfzrs;
	
	/**
	 * 年份
	 */
	private String nf;
	

	/**
	 * 接待部门
	 */
	public String getJdbmmc() {
		return jdbmmc;
	}

	/**
	 * 接待部门
	 */
	public void setJdbmmc(String value) {
		this.jdbmmc = value;
	}
	/**
	 * 来访总人数
	 */
	public String getLfzrs() {
		return lfzrs;
	}

	/**
	 * 来访总人数
	 */
	public void setLfzrs(String value) {
		this.lfzrs = value;
	}
	/**
	 * 年份
	 */
	public String getNf() {
		return nf;
	}

	/**
	 * 年份
	 */
	public void setNf(String value) {
		this.nf = value;
	}
}