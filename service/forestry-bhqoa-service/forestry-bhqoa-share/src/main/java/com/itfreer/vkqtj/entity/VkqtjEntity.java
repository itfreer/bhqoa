package com.itfreer.vkqtj.entity;

import java.io.Serializable;

import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义考勤统计实体
 */
public class VkqtjEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * id
	 */
	private String id;
	
	/**
	 * 部门id
	 */
	@DictionaryField(dictionaryName = "p_department" ,toFieldName = "dptname")
	private String dptid;
	
	private String dptname;
	
	public String getDptname() {
		return dptname;
	}

	public void setDptname(String dptname) {
		this.dptname = dptname;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDptid() {
		return dptid;
	}

	public void setDptid(String dptid) {
		this.dptid = dptid;
	}

	public String getId() {
		return id;
	}
	
	/**
	 * 类型
	 */
	private String type;
	
	
	/**
	 * 用户id
	 */
	private String userid;
	
	
	/**
	 * 用户姓名
	 */
	private String username;
	
	/**
	 * 时间
	 */
	private java.util.Date sj;
	
	/**
	 * 开始时间
	 */
	private java.util.Date kssj;
	
	/**
	 * 结束时间
	 */
	private java.util.Date jssj;
	

	/**
	 * 类型
	 */
	public String getType() {
		return type;
	}

	/**
	 * 类型
	 */
	public void setType(String value) {
		this.type = value;
	}
	/**
	 * 用户id
	 */
	public String getUserid() {
		return userid;
	}

	/**
	 * 用户id
	 */
	public void setUserid(String value) {
		this.userid = value;
	}
	/**
	 * 用户姓名
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * 用户姓名
	 */
	public void setUsername(String value) {
		this.username = value;
	}
	/**
	 * 时间
	 */
	public java.util.Date getSj() {
		return sj;
	}

	/**
	 * 时间
	 */
	public void setSj(java.util.Date value) {
		this.sj = value;
	}
	/**
	 * 开始时间
	 */
	public java.util.Date getKssj() {
		return kssj;
	}

	/**
	 * 开始时间
	 */
	public void setKssj(java.util.Date value) {
		this.kssj = value;
	}
	/**
	 * 结束时间
	 */
	public java.util.Date getJssj() {
		return jssj;
	}

	/**
	 * 结束时间
	 */
	public void setJssj(java.util.Date value) {
		this.jssj = value;
	}
}