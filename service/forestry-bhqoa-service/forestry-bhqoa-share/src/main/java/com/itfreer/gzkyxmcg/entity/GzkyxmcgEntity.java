package com.itfreer.gzkyxmcg.entity;

import java.io.Serializable;

/**
 * 定义科研项目成果实体
 */
public class GzkyxmcgEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 附件编号
	 */
	private String fjbh;
	
	
	/**
	 * 附件名称
	 */
	private String fjmc;
	
	/**
	 * 上传时间
	 */
	private java.util.Date scsj;
	
	
	/**
	 * 上传人员
	 */
	private String scryid;
	
	
	/**
	 * 上传人员名称
	 */
	private String scrymc;
	
	
	/**
	 * 项目编号
	 */
	private String xmbh;
	
	
	/**
	 * 项目名称
	 */
	private String xmmc;
	
	
	/**
	 * 存档位置
	 */
	private String cdwz;
	
	
	/**
	 * 备注
	 */
	private String bz;
	
	public String getXmmc() {
		return xmmc;
	}

	public void setXmmc(String xmmc) {
		this.xmmc = xmmc;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 附件编号
	 */
	public String getFjbh() {
		return fjbh;
	}

	/**
	 * 附件编号
	 */
	public void setFjbh(String value) {
		this.fjbh = value;
	}
	/**
	 * 附件名称
	 */
	public String getFjmc() {
		return fjmc;
	}

	/**
	 * 附件名称
	 */
	public void setFjmc(String value) {
		this.fjmc = value;
	}
	/**
	 * 上传时间
	 */
	public java.util.Date getScsj() {
		return scsj;
	}

	/**
	 * 上传时间
	 */
	public void setScsj(java.util.Date value) {
		this.scsj = value;
	}
	/**
	 * 上传人员
	 */
	public String getScryid() {
		return scryid;
	}

	/**
	 * 上传人员
	 */
	public void setScryid(String value) {
		this.scryid = value;
	}
	/**
	 * 上传人员名称
	 */
	public String getScrymc() {
		return scrymc;
	}

	/**
	 * 上传人员名称
	 */
	public void setScrymc(String value) {
		this.scrymc = value;
	}
	/**
	 * 项目编号
	 */
	public String getXmbh() {
		return xmbh;
	}

	/**
	 * 项目编号
	 */
	public void setXmbh(String value) {
		this.xmbh = value;
	}
	/**
	 * 存档位置
	 */
	public String getCdwz() {
		return cdwz;
	}

	/**
	 * 存档位置
	 */
	public void setCdwz(String value) {
		this.cdwz = value;
	}
	/**
	 * 备注
	 */
	public String getBz() {
		return bz;
	}

	/**
	 * 备注
	 */
	public void setBz(String value) {
		this.bz = value;
	}
}