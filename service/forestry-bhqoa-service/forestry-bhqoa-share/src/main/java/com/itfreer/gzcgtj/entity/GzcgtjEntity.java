package com.itfreer.gzcgtj.entity;

import java.io.Serializable;

/**
 * 定义采购统计实体
 */
public class GzcgtjEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	private String id;
	
	
	/**
	 * 采购年度
	 */
	private String scgnd;
	
	
	/**
	 * 采购月份
	 */
	private String scgyf;
	
	
	/**
	 * 采购设备名称
	 */
	private String scgsbmc;
	
	
	/**
	 * 采购设备数量
	 */
	private String dcgsbsl;
	
	
	/**
	 * 采购费用
	 */
	private String scgfy;
	
	
	/**
	 * 采购用途
	 */
	private String scgyt;
	
	
	/**
	 * 采购部门
	 */
	private String scgbm;
	
	
	/**
	 * 采购人
	 */
	private String scgr;
	
	
	/**
	 * 联系方式
	 */
	private String slxfs;
	
	
	/**
	 * 是否同意采购
	 */
	private String itycg;
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 采购年度
	 */
	public String getScgnd() {
		return scgnd;
	}

	/**
	 * 采购年度
	 */
	public void setScgnd(String value) {
		this.scgnd = value;
	}
	/**
	 * 采购月份
	 */
	public String getScgyf() {
		return scgyf;
	}

	/**
	 * 采购月份
	 */
	public void setScgyf(String value) {
		this.scgyf = value;
	}
	/**
	 * 采购设备名称
	 */
	public String getScgsbmc() {
		return scgsbmc;
	}

	/**
	 * 采购设备名称
	 */
	public void setScgsbmc(String value) {
		this.scgsbmc = value;
	}
	/**
	 * 采购设备数量
	 */
	public String getDcgsbsl() {
		return dcgsbsl;
	}

	/**
	 * 采购设备数量
	 */
	public void setDcgsbsl(String value) {
		this.dcgsbsl = value;
	}
	/**
	 * 采购费用
	 */
	public String getScgfy() {
		return scgfy;
	}

	/**
	 * 采购费用
	 */
	public void setScgfy(String value) {
		this.scgfy = value;
	}
	/**
	 * 采购用途
	 */
	public String getScgyt() {
		return scgyt;
	}

	/**
	 * 采购用途
	 */
	public void setScgyt(String value) {
		this.scgyt = value;
	}
	/**
	 * 采购部门
	 */
	public String getScgbm() {
		return scgbm;
	}

	/**
	 * 采购部门
	 */
	public void setScgbm(String value) {
		this.scgbm = value;
	}
	/**
	 * 采购人
	 */
	public String getScgr() {
		return scgr;
	}

	/**
	 * 采购人
	 */
	public void setScgr(String value) {
		this.scgr = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxfs() {
		return slxfs;
	}

	/**
	 * 联系方式
	 */
	public void setSlxfs(String value) {
		this.slxfs = value;
	}
	/**
	 * 是否同意采购
	 */
	public String getItycg() {
		return itycg;
	}

	/**
	 * 是否同意采购
	 */
	public void setItycg(String value) {
		this.itycg = value;
	}
}