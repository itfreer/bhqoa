package com.itfreer.gzschedule.entity;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;

import com.itfreer.form.dictionary.reflect.DictionaryField;
import com.itfreer.gzreceivefile.entity.GzreceivefileEntity;
import com.itfreer.gztask.entity.gztaskEntity;

/**
 * 定义日常安排实体
 */
public class ScheduleEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 参与人ID
	 */
	private String cyrid;
	
	public String getCyrid() {
		return cyrid;
	}

	public void setCyrid(String cyrid) {
		this.cyrid = cyrid;
	}

	/**
	 * 参与人
	 */
	private String cyr;
	
	/**
	 * 开始时间
	 */
	private java.util.Date kssj;
	
	/**
	 * 结束时间
	 */
	private java.util.Date jssj;
	
	
	/**
	 * 创建人ID
	 */
	private String cjrid;
	
	
	/**
	 * 创建人名称
	 */
	private String cjrmc;
	
	/**
	 * 创建时间
	 */
	private java.util.Date cjsj;
	
	
	/**
	 * 组织机构ID
	 */
	private String zzjgid;
	
	
	/**
	 * 组织机构名称
	 */
	@DictionaryField(dictionaryName = "p_organization" ,toFieldName = "zzjgmcName")
	private String zzjgmc;
	
	private String zzjgmcName;
	
	
	public String getZzjgmcName() {
		return zzjgmcName;
	}

	public void setZzjgmcName(String zzjgmcName) {
		this.zzjgmcName = zzjgmcName;
	}

	/**
	 * 部门ID
	 */
	private String bmid;
	
	
	/**
	 * 完成时间
	 */
	private java.util.Date wcsj;
	

	public java.util.Date getWcsj() {
		return wcsj;
	}

	public void setWcsj(java.util.Date wcsj) {
		this.wcsj = wcsj;
	}
	
	
	/**
	 * 部门名称
	 */
	@DictionaryField(dictionaryName = "p_department" ,toFieldName = "bmmcName")
	private String bmmc;
	
	private String bmmcName;
	
	
	public String getBmmcName() {
		return bmmcName;
	}

	public void setBmmcName(String bmmcName) {
		this.bmmcName = bmmcName;
	}
	
	
	/**
	 * 项目ID
	 */
	private String xmid;
	
	
	/**
	 * 项目名称
	 */
	private String xmmc;
	
	
	/**
	 * 事项说明
	 */
	private String sxsm;
	

	/**
	 * 状态
	 */
	@DictionaryField(dictionaryName = "p_rwzt" ,toFieldName = "ztName")
	private String zt;
	
	
	/**
	 * 状态
	 */
	private String ztName;
	
	
	public String getZtName() {
		return ztName;
	}

	public void setZtName(String ztName) {
		this.ztName = ztName;
	}

	/**
	 * 单位名称
	 */
	private String dwmc;
	
	
	/**
	 * 负责人
	 */
	private String fzr;
	
	
	/**
	 * 负责人
	 */
	private String fzrmc;
	
	
	public String getFzrmc() {
		return fzrmc;
	}

	public void setFzrmc(String fzrmc) {
		this.fzrmc = fzrmc;
	}

	/**
	 * 主题
	 */
	private String zhuti;
	
	
	public String getZhuti() {
		return zhuti;
	}

	public void setZhuti(String zhuti) {
		this.zhuti = zhuti;
	}
	
	
	/**
	 * 指派任务
	 */
	private Set<gztaskEntity> gztask=new LinkedHashSet<gztaskEntity>();
	
	
	public Set<gztaskEntity> getGztask() {
		return gztask;
	}

	public void setGztask(Set<gztaskEntity> gztask) {
		this.gztask = gztask;
	}

	public String getDwmc() {
		return dwmc;
	}

	public void setDwmc(String dwmc) {
		this.dwmc = dwmc;
	}

	public String getFzr() {
		return fzr;
	}

	public void setFzr(String fzr) {
		this.fzr = fzr;
	}
	
	public String getZt() {
		return zt;
	}

	public void setZt(String zt) {
		this.zt = zt;
	}
	
	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 参与人
	 */
	public String getCyr() {
		return cyr;
	}

	/**
	 * 参与人
	 */
	public void setCyr(String value) {
		this.cyr = value;
	}
	/**
	 * 开始时间
	 */
	public java.util.Date getKssj() {
		return kssj;
	}

	/**
	 * 开始时间
	 */
	public void setKssj(java.util.Date value) {
		this.kssj = value;
	}
	/**
	 * 结束时间
	 */
	public java.util.Date getJssj() {
		return jssj;
	}

	/**
	 * 结束时间
	 */
	public void setJssj(java.util.Date value) {
		this.jssj = value;
	}
	/**
	 * 创建人ID
	 */
	public String getCjrid() {
		return cjrid;
	}

	/**
	 * 创建人ID
	 */
	public void setCjrid(String value) {
		this.cjrid = value;
	}
	/**
	 * 创建人名称
	 */
	public String getCjrmc() {
		return cjrmc;
	}

	/**
	 * 创建人名称
	 */
	public void setCjrmc(String value) {
		this.cjrmc = value;
	}
	/**
	 * 创建时间
	 */
	public java.util.Date getCjsj() {
		return cjsj;
	}

	/**
	 * 创建时间
	 */
	public void setCjsj(java.util.Date value) {
		this.cjsj = value;
	}
	/**
	 * 组织机构ID
	 */
	public String getZzjgid() {
		return zzjgid;
	}

	/**
	 * 组织机构ID
	 */
	public void setZzjgid(String value) {
		this.zzjgid = value;
	}
	/**
	 * 组织机构名称
	 */
	public String getZzjgmc() {
		return zzjgmc;
	}

	/**
	 * 组织机构名称
	 */
	public void setZzjgmc(String value) {
		this.zzjgmc = value;
	}
	/**
	 * 部门ID
	 */
	public String getBmid() {
		return bmid;
	}

	/**
	 * 部门ID
	 */
	public void setBmid(String value) {
		this.bmid = value;
	}
	/**
	 * 部门名称
	 */
	public String getBmmc() {
		return bmmc;
	}

	/**
	 * 部门名称
	 */
	public void setBmmc(String value) {
		this.bmmc = value;
	}
	/**
	 * 项目ID
	 */
	public String getXmid() {
		return xmid;
	}

	/**
	 * 项目ID
	 */
	public void setXmid(String value) {
		this.xmid = value;
	}
	/**
	 * 项目名称
	 */
	public String getXmmc() {
		return xmmc;
	}

	/**
	 * 项目名称
	 */
	public void setXmmc(String value) {
		this.xmmc = value;
	}
	/**
	 * 事项说明
	 */
	public String getSxsm() {
		return sxsm;
	}

	/**
	 * 事项说明
	 */
	public void setSxsm(String value) {
		this.sxsm = value;
	}
}