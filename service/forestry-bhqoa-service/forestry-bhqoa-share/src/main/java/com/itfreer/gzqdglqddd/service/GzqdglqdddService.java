package com.itfreer.gzqdglqddd.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzqdglqddd.entity.GzqdglqdddEntity;

/**
 * 定义签到地点接口
 */
public interface GzqdglqdddService extends BaseService<GzqdglqdddEntity> {
	
}
