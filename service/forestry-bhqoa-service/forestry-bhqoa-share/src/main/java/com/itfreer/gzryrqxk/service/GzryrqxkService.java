package com.itfreer.gzryrqxk.service;

import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.gzryrqxk.entity.GzryrqxkEntity;

/**
 * 定义外籍人员入区许可、外来科研人员入区许可接口
 */
public interface GzryrqxkService extends IBaseWorkFlowService<GzryrqxkEntity> {
	
}
