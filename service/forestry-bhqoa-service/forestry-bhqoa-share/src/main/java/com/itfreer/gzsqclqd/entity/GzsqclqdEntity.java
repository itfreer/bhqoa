package com.itfreer.gzsqclqd.entity;

import java.io.Serializable;

/**
 * 定义申请材料清单实体
 */
public class GzsqclqdEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	private String id;
	
	
	/**
	 * 项目ID
	 */
	private String xmid;
	
	
	/**
	 * 材料名称
	 */
	private String clmc;
	
	
	/**
	 * 材料数量
	 */
	private String clsl;
	
	
	/**
	 * 材料备注
	 */
	private String clbz;
	
	
	/**
	 * 材料附件
	 */
	private String clfj;
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 项目ID
	 */
	public String getXmid() {
		return xmid;
	}

	/**
	 * 项目ID
	 */
	public void setXmid(String value) {
		this.xmid = value;
	}
	/**
	 * 材料名称
	 */
	public String getClmc() {
		return clmc;
	}

	/**
	 * 材料名称
	 */
	public void setClmc(String value) {
		this.clmc = value;
	}
	/**
	 * 材料数量
	 */
	public String getClsl() {
		return clsl;
	}

	/**
	 * 材料数量
	 */
	public void setClsl(String value) {
		this.clsl = value;
	}
	/**
	 * 材料备注
	 */
	public String getClbz() {
		return clbz;
	}

	/**
	 * 材料备注
	 */
	public void setClbz(String value) {
		this.clbz = value;
	}
	/**
	 * 材料附件
	 */
	public String getClfj() {
		return clfj;
	}

	/**
	 * 材料附件
	 */
	public void setClfj(String value) {
		this.clfj = value;
	}
}