package com.itfreer.gzhlyndkh.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzhlyndkh.entity.GzhlyndkhEntity;

/**
 * 定义护林员年度考核接口
 */
public interface GzhlyndkhService extends BaseService<GzhlyndkhEntity> {
	
}
