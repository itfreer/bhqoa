package com.itfreer.gzghfff.entity;

import java.io.Serializable;

/**
 * 定义管护费发放实体
 */
public class GzghfffEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 市
	 */
	private String shi;
	
	
	/**
	 * 县
	 */
	private String xian;
	
	
	/**
	 * 乡
	 */
	private String xiang;
	
	
	/**
	 * 村
	 */
	private String cun;
	
	
	/**
	 * 组
	 */
	private String zu;
	
	
	/**
	 * 面积（亩）
	 */
	private String mianji;
	
	
	/**
	 * 合同号
	 */
	private String heth;
	
	
	/**
	 * 兑现金额
	 */
	private String dxje;
	
	
	/**
	 * 领款人
	 */
	private String lkr;
	
	
	/**
	 * 群众代表
	 */
	private String qzdb;
	
	/**
	 * 领款日期
	 */
	private java.util.Date lkrq;
	
	
	/**
	 * 备注
	 */
	private String beizhu;
	
	
	/**
	 * 监督员
	 */
	private String jdy;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 市
	 */
	public String getShi() {
		return shi;
	}

	/**
	 * 市
	 */
	public void setShi(String value) {
		this.shi = value;
	}
	/**
	 * 县
	 */
	public String getXian() {
		return xian;
	}

	/**
	 * 县
	 */
	public void setXian(String value) {
		this.xian = value;
	}
	/**
	 * 乡
	 */
	public String getXiang() {
		return xiang;
	}

	/**
	 * 乡
	 */
	public void setXiang(String value) {
		this.xiang = value;
	}
	/**
	 * 村
	 */
	public String getCun() {
		return cun;
	}

	/**
	 * 村
	 */
	public void setCun(String value) {
		this.cun = value;
	}
	/**
	 * 组
	 */
	public String getZu() {
		return zu;
	}

	/**
	 * 组
	 */
	public void setZu(String value) {
		this.zu = value;
	}
	/**
	 * 面积（亩）
	 */
	public String getMianji() {
		return mianji;
	}

	/**
	 * 面积（亩）
	 */
	public void setMianji(String value) {
		this.mianji = value;
	}
	/**
	 * 合同号
	 */
	public String getHeth() {
		return heth;
	}

	/**
	 * 合同号
	 */
	public void setHeth(String value) {
		this.heth = value;
	}
	/**
	 * 兑现金额
	 */
	public String getDxje() {
		return dxje;
	}

	/**
	 * 兑现金额
	 */
	public void setDxje(String value) {
		this.dxje = value;
	}
	/**
	 * 领款人
	 */
	public String getLkr() {
		return lkr;
	}

	/**
	 * 领款人
	 */
	public void setLkr(String value) {
		this.lkr = value;
	}
	/**
	 * 群众代表
	 */
	public String getQzdb() {
		return qzdb;
	}

	/**
	 * 群众代表
	 */
	public void setQzdb(String value) {
		this.qzdb = value;
	}
	/**
	 * 领款日期
	 */
	public java.util.Date getLkrq() {
		return lkrq;
	}

	/**
	 * 领款日期
	 */
	public void setLkrq(java.util.Date value) {
		this.lkrq = value;
	}
	/**
	 * 备注
	 */
	public String getBeizhu() {
		return beizhu;
	}

	/**
	 * 备注
	 */
	public void setBeizhu(String value) {
		this.beizhu = value;
	}
	/**
	 * 监督员
	 */
	public String getJdy() {
		return jdy;
	}

	/**
	 * 监督员
	 */
	public void setJdy(String value) {
		this.jdy = value;
	}
}