package com.itfreer.gzccgl.service;

import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.gzccgl.entity.GzccglEntity;

/**
 * 定义出差管理接口
 */
public interface GzccglService extends  IBaseWorkFlowService<GzccglEntity> {
	
}
