package com.itfreer.gzsqxm.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzsqxm.entity.GzsqxmEntity;

/**
 * 定义社区项目申请/台账接口
 */
public interface GzsqxmService extends BaseService<GzsqxmEntity> {
	
}
