package com.itfreer.gzreceivefile.entity;

import java.io.Serializable;

import javax.persistence.Column;

import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义收文管理实体
 */
public class GzreceivefileEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 发文ID
	 */
	private String fwid;
	
	
	/**
	 * 接收人ID
	 */
	private String jsrid;
	
	
	/**
	 * 接收人名称
	 */
	private String jsrmc;
	
	
	/**
	 * 是否已读
	 */
	@DictionaryField(dictionaryName="p_yesorno", toFieldName="sfydName")
	private String sfyd;
	
	
	/**
	 * 是否已读
	 */
	private String sfydName;
	
	
	/**
	 * 阅读时间
	 */
	private java.util.Date ydsj;
	
	
	/**
	 * 是否已确认
	 */
	@DictionaryField(dictionaryName="p_yesorno", toFieldName="sfqrName")
	private String sfqr;
	
	
	/**
	 * 是否已确认
	 */
	private String sfqrName;
	
	
	public String getSfydName() {
		return sfydName;
	}

	public void setSfydName(String sfydName) {
		this.sfydName = sfydName;
	}

	public String getSfqrName() {
		return sfqrName;
	}

	public void setSfqrName(String sfqrName) {
		this.sfqrName = sfqrName;
	}

	/**
	 * 接收人机构
	 */
	private String jsrjg;
	
	
	/**
	 * 接收人意见
	 */
	private String jsryj;
	
	
	public String getJsryj() {
		return jsryj;
	}

	public void setJsryj(String jsryj) {
		this.jsryj = jsryj;
	}
	
	/**
	 * 接收人职位
	 */
	private String jsrzw;
	
	public String getJsrzw() {
		return jsrzw;
	}

	public void setJsrzw(String jsrzw) {
		this.jsrzw = jsrzw;
	}
	
	
	/**
	 * 接收人机构名称
	 */
	private String jsrjgmc;
	
	

	public String getJsrjgmc() {
		return jsrjgmc;
	}

	public void setJsrjgmc(String jsrjgmc) {
		this.jsrjgmc = jsrjgmc;
	}

	public String getJsrjg() {
		return jsrjg;
	}

	public void setJsrjg(String jsrjg) {
		this.jsrjg = jsrjg;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 发文ID
	 */
	public String getFwid() {
		return fwid;
	}

	/**
	 * 发文ID
	 */
	public void setFwid(String value) {
		this.fwid = value;
	}
	/**
	 * 接收人ID
	 */
	public String getJsrid() {
		return jsrid;
	}

	/**
	 * 接收人ID
	 */
	public void setJsrid(String value) {
		this.jsrid = value;
	}
	/**
	 * 接收人名称
	 */
	public String getJsrmc() {
		return jsrmc;
	}

	/**
	 * 接收人名称
	 */
	public void setJsrmc(String value) {
		this.jsrmc = value;
	}
	/**
	 * 是否已读
	 */
	public String getSfyd() {
		return sfyd;
	}

	/**
	 * 是否已读
	 */
	public void setSfyd(String value) {
		this.sfyd = value;
	}
	/**
	 * 阅读时间
	 */
	public java.util.Date getYdsj() {
		return ydsj;
	}

	/**
	 * 阅读时间
	 */
	public void setYdsj(java.util.Date value) {
		this.ydsj = value;
	}
	/**
	 * 是否已确认
	 */
	public String getSfqr() {
		return sfqr;
	}

	/**
	 * 是否已确认
	 */
	public void setSfqr(String value) {
		this.sfqr = value;
	}
}