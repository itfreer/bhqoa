package com.itfreer.gzccsxry.entity;

import java.io.Serializable;

/**
 * 定义出差随行人员实体
 */
public class GzccsxryEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	
	/**
	 * ID
	 */
	private String id;
	
	/**
	 * 出差管理记录id
	 */
	private String ccglid;
	
	
	/**
	 * 随行人员id
	 */
	private String jsrid;
	
	
	/**
	 * 随行人员名称
	 */
	private String jsrmc;
	
	
	/**
	 * 机构名称
	 */
	private String jsrjg;
	
	
	/**
	 * 机构名称
	 */
	private String jsrjgmc;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 随行人员id
	 */
	public String getJsrid() {
		return jsrid;
	}

	/**
	 * 随行人员id
	 */
	public void setJsrid(String value) {
		this.jsrid = value;
	}
	/**
	 * 随行人员名称
	 */
	public String getJsrmc() {
		return jsrmc;
	}

	/**
	 * 随行人员名称
	 */
	public void setJsrmc(String value) {
		this.jsrmc = value;
	}
	/**
	 * 机构名称
	 */
	public String getJsrjg() {
		return jsrjg;
	}

	/**
	 * 机构名称
	 */
	public void setJsrjg(String value) {
		this.jsrjg = value;
	}
	/**
	 * 机构名称
	 */
	public String getJsrjgmc() {
		return jsrjgmc;
	}

	/**
	 * 机构名称
	 */
	public void setJsrjgmc(String value) {
		this.jsrjgmc = value;
	}

	public String getCcglid() {
		return ccglid;
	}

	public void setCcglid(String ccglid) {
		this.ccglid = ccglid;
	}
}