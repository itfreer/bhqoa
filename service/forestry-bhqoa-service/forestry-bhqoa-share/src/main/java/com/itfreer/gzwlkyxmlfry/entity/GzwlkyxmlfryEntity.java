package com.itfreer.gzwlkyxmlfry.entity;

import java.io.Serializable;

import javax.persistence.Column;

import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义外来科研项目人随访人员实体
 */
public class GzwlkyxmlfryEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 姓名
	 */
	private String xingming;
	
	
	/**
	 * 性别
	 */
	@DictionaryField(dictionaryName="p_sex", toFieldName="xingbieName")
	private String xingbie;
	
	
	/**
	 * 性别
	 */
	private String xingbieName;
	
	
	public String getXingbieName() {
		return xingbieName;
	}

	public void setXingbieName(String xingbieName) {
		this.xingbieName = xingbieName;
	}

	/**
	 * 身份证号
	 */
	private String shenfzh;
	
	
	/**
	 * 联系方式
	 */
	private String lxfs;
	
	
	/**
	 * 申请项目ID
	 */
	private String xmid;
	
	
	/**
	 * 项目编号
	 */
	private String sxmbh;
	
	
	/**
	 * 项目名称
	 */
	private String sxmmc;
	
	
	/**
	 * 负责部门
	 */
	@DictionaryField(dictionaryName = "p_department" ,toFieldName = "fzbmName")
	private String fzbm;
	
	/**
	 * 负责部门
	 */
	private String fzbmName;
	
	/**
	 * 负责人
	 */
	private String fzr;
	
	/**
	 * 负责人ID
	 */
	private String fzrid;
	
	/**
	 * 来访时间开始时间
	 */
	private java.util.Date lfsj;
	
	/**
	 * 来访时间结束时间
	 */
	private java.util.Date lfsjjssj;
	
	public String getFzbm() {
		return fzbm;
	}

	public void setFzbm(String fzbm) {
		this.fzbm = fzbm;
	}
	
	public String getFzbmName() {
		return fzbmName;
	}

	public void setFzbmName(String fzbmName) {
		this.fzbmName = fzbmName;
	}

	public String getFzr() {
		return fzr;
	}

	public void setFzr(String fzr) {
		this.fzr = fzr;
	}

	public String getFzrid() {
		return fzrid;
	}

	public void setFzrid(String fzrid) {
		this.fzrid = fzrid;
	}

	public java.util.Date getLfsj() {
		return lfsj;
	}

	public void setLfsj(java.util.Date lfsj) {
		this.lfsj = lfsj;
	}

	public java.util.Date getLfsjjssj() {
		return lfsjjssj;
	}

	public void setLfsjjssj(java.util.Date lfsjjssj) {
		this.lfsjjssj = lfsjjssj;
	}
	
	
	/**
	 * 项目编号
	 */
	public String getSxmbh() {
		return sxmbh;
	}

	/**
	 * 项目编号
	 */
	public void setSxmbh(String value) {
		this.sxmbh = value;
	}
	/**
	 * 项目名称
	 */
	public String getSxmmc() {
		return sxmmc;
	}

	/**
	 * 项目名称
	 */
	public void setSxmmc(String value) {
		this.sxmmc = value;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 姓名
	 */
	public String getXingming() {
		return xingming;
	}

	/**
	 * 姓名
	 */
	public void setXingming(String value) {
		this.xingming = value;
	}
	/**
	 * 性别
	 */
	public String getXingbie() {
		return xingbie;
	}

	/**
	 * 性别
	 */
	public void setXingbie(String value) {
		this.xingbie = value;
	}
	/**
	 * 身份证号
	 */
	public String getShenfzh() {
		return shenfzh;
	}

	/**
	 * 身份证号
	 */
	public void setShenfzh(String value) {
		this.shenfzh = value;
	}
	/**
	 * 联系方式
	 */
	public String getLxfs() {
		return lxfs;
	}

	/**
	 * 联系方式
	 */
	public void setLxfs(String value) {
		this.lxfs = value;
	}
	/**
	 * 申请项目ID
	 */
	public String getXmid() {
		return xmid;
	}

	/**
	 * 申请项目ID
	 */
	public void setXmid(String value) {
		this.xmid = value;
	}
}