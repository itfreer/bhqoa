package com.itfreer.appupdatenet.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.appupdatenet.entity.AppupdatenetEntity;

/**
 * 定义APP更新接口
 */
public interface AppupdatenetService extends BaseService<AppupdatenetEntity> {
	
}
