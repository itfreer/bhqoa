package com.itfreer.gzccgl.entity;

import java.util.LinkedHashSet;
import java.util.Set;

import com.itfreer.bpm.flow.api.IBpmProjectEntity;
import com.itfreer.form.dictionary.reflect.DictionaryField;
import com.itfreer.gzccsxry.entity.GzccsxryEntity;

/**
 * 定义出差管理实体
 */
public class GzccglEntity implements IBpmProjectEntity{
	//private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 所在单位
	 */
	private String szdw;
	
	
	/**
	 * 单位ID
	 */
	@DictionaryField(dictionaryName = "p_department" ,toFieldName = "szdw")
	private String dwid;
	
	
	/**
	 * 出差人姓名
	 */
	private String ccrxm;
	
	
	/**
	 * 出差人ID
	 */
	private String ccrid;
	
	/**
	 * 开始日期
	 */
	private java.util.Date ksrq;
	
	/**
	 * 开始时间
	 */
	private java.util.Date kssj;
	
	/**
	 * 结束时间
	 */
	private java.util.Date jssj;
	
	/**
	 * 结束日期
	 */
	private java.util.Date jsrq;
	
	
	/**
	 * 出差类型
	 */
	private String cclx;
	
	/**
	 * 出差天数
	 */
	private Double ccts;
	
	
	/**
	 * 外出事由
	 */
	private String wcsy;
	
	
	/**
	 * 关联项目ID
	 */
	private String glxmid;
	
	
	/**
	 * 关联项目名称
	 */
	private String glxmmc;
	
	
	/**
	 * 出差报告
	 */
	private String ccbg;
	
	/**
	 * 申请日期
	 */
	private java.util.Date sqrq;
	
	
	/**
	 * 申请人ID
	 */
	private String sqrid;
	
	
	/**
	 * 申请人名称
	 */
	private String sqrmc;
	
	
	/**
	 * 核对人ID
	 */
	private String hdrid;
	
	
	/**
	 * 核对人名称
	 */
	private String hdrmc;
	
	
	/**
	 * 核对结果
	 */
	private String hdjg;
	
	/**
	 * 核对时间
	 */
	private java.util.Date hdsj;
	
	
	/**
	 * 审核人
	 */
	private String shr;
	
	
	/**
	 * 审核意见
	 */
	private String shyj;
	
	
	/**
	 * 审核结果
	 */
	private String shjg;
	
	
	/**
	 * 审核人ID
	 */
	private String shrid;
	
	/**
	 * 审核时间
	 */
	private java.util.Date shsj;
	
	/**
	 * 撤销时间
	 */
	private java.util.Date cxsj;
	
	
	/**
	 * 撤销人
	 */
	private String cxrmc;
	
	
	/**
	 * 撤销原因
	 */
	private String cxyy;
	
	
	/**
	 * 撤销人ID
	 */
	private String cxrid;
	
	
	/**
	 * 出差人职务
	 */
	private String ccrzw;
	
	/**
	 * 出行方式
	 */
	@DictionaryField(dictionaryName = "GZ_RSGL_CCGL_CXFS", toFieldName = "cxfsname")
	public String cxfs;
	
	/**
	 * 审批状态
	 */
	@DictionaryField(dictionaryName = "spzt_zd", toFieldName = "spztName")
	public String spzt;
	
	/**
	 * 审批状态
	 */
	public String spztName;
	
	
	
	
	public String getSpzt() {
		return spzt;
	}

	public void setSpzt(String spzt) {
		this.spzt = spzt;
	}

	public String getSpztName() {
		return spztName;
	}

	public void setSpztName(String spztName) {
		this.spztName = spztName;
	}
	/**
	 * 出行方式（新）
	 */
	public String cxfsname;
	
	public String getCxfs() {
		return cxfs;
	}

	public void setCxfs(String cxfs) {
		this.cxfs = cxfs;
	}

	public String getCxfsname() {
		return cxfsname;
	}

	public void setCxfsname(String cxfsname) {
		this.cxfsname = cxfsname;
	}
	public Boolean cxfsfj;
	
	public Boolean cxfshc;
	
	public Boolean cxfsdwpc;
	
	public Boolean cxfsqc;
	
	public Boolean cxfszdc;
	
	public Boolean cxfsqt;
	
	public Boolean getCxfsfj() {
		return cxfsfj;
	}

	public void setCxfsfj(Boolean cxfsfj) {
		this.cxfsfj = cxfsfj;
	}

	public Boolean getCxfshc() {
		return cxfshc;
	}

	public void setCxfshc(Boolean cxfshc) {
		this.cxfshc = cxfshc;
	}

	public Boolean getCxfsdwpc() {
		return cxfsdwpc;
	}

	public void setCxfsdwpc(Boolean cxfsdwpc) {
		this.cxfsdwpc = cxfsdwpc;
	}

	public Boolean getCxfsqc() {
		return cxfsqc;
	}

	public void setCxfsqc(Boolean cxfsqc) {
		this.cxfsqc = cxfsqc;
	}

	public Boolean getCxfszdc() {
		return cxfszdc;
	}

	public void setCxfszdc(Boolean cxfszdc) {
		this.cxfszdc = cxfszdc;
	}

	public Boolean getCxfsqt() {
		return cxfsqt;
	}

	public void setCxfsqt(Boolean cxfsqt) {
		this.cxfsqt = cxfsqt;
	}

	public String getCcrzw() {
		return ccrzw;
	}

	public void setCcrzw(String ccrzw) {
		this.ccrzw = ccrzw;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 所在单位
	 */
	public String getSzdw() {
		return szdw;
	}

	/**
	 * 所在单位
	 */
	public void setSzdw(String value) {
		this.szdw = value;
	}
	/**
	 * 单位ID
	 */
	public String getDwid() {
		return dwid;
	}

	/**
	 * 单位ID
	 */
	public void setDwid(String value) {
		this.dwid = value;
	}
	/**
	 * 出差人姓名
	 */
	public String getCcrxm() {
		return ccrxm;
	}

	/**
	 * 出差人姓名
	 */
	public void setCcrxm(String value) {
		this.ccrxm = value;
	}
	/**
	 * 出差人ID
	 */
	public String getCcrid() {
		return ccrid;
	}

	/**
	 * 出差人ID
	 */
	public void setCcrid(String value) {
		this.ccrid = value;
	}
	/**
	 * 开始日期
	 */
	public java.util.Date getKsrq() {
		return ksrq;
	}

	/**
	 * 开始日期
	 */
	public void setKsrq(java.util.Date value) {
		this.ksrq = value;
	}
	/**
	 * 开始时间
	 */
	public java.util.Date getKssj() {
		return kssj;
	}

	/**
	 * 开始时间
	 */
	public void setKssj(java.util.Date value) {
		this.kssj = value;
	}
	/**
	 * 结束时间
	 */
	public java.util.Date getJssj() {
		return jssj;
	}

	/**
	 * 结束时间
	 */
	public void setJssj(java.util.Date value) {
		this.jssj = value;
	}
	/**
	 * 结束日期
	 */
	public java.util.Date getJsrq() {
		return jsrq;
	}

	/**
	 * 结束日期
	 */
	public void setJsrq(java.util.Date value) {
		this.jsrq = value;
	}
	/**
	 * 出差类型
	 */
	public String getCclx() {
		return cclx;
	}

	/**
	 * 出差类型
	 */
	public void setCclx(String value) {
		this.cclx = value;
	}
	/**
	 * 出差天数
	 */
	public Double getCcts() {
		return ccts;
	}

	/**
	 * 出差天数
	 */
	public void setCcts(Double value) {
		this.ccts = value;
	}
	/**
	 * 外出事由
	 */
	public String getWcsy() {
		return wcsy;
	}

	/**
	 * 外出事由
	 */
	public void setWcsy(String value) {
		this.wcsy = value;
	}
	/**
	 * 关联项目ID
	 */
	public String getGlxmid() {
		return glxmid;
	}

	/**
	 * 关联项目ID
	 */
	public void setGlxmid(String value) {
		this.glxmid = value;
	}
	/**
	 * 关联项目名称
	 */
	public String getGlxmmc() {
		return glxmmc;
	}

	/**
	 * 关联项目名称
	 */
	public void setGlxmmc(String value) {
		this.glxmmc = value;
	}
	/**
	 * 出差报告
	 */
	public String getCcbg() {
		return ccbg;
	}

	/**
	 * 出差报告
	 */
	public void setCcbg(String value) {
		this.ccbg = value;
	}
	/**
	 * 申请日期
	 */
	public java.util.Date getSqrq() {
		return sqrq;
	}

	/**
	 * 申请日期
	 */
	public void setSqrq(java.util.Date value) {
		this.sqrq = value;
	}
	/**
	 * 申请人ID
	 */
	public String getSqrid() {
		return sqrid;
	}

	/**
	 * 申请人ID
	 */
	public void setSqrid(String value) {
		this.sqrid = value;
	}
	/**
	 * 申请人名称
	 */
	public String getSqrmc() {
		return sqrmc;
	}

	/**
	 * 申请人名称
	 */
	public void setSqrmc(String value) {
		this.sqrmc = value;
	}
	/**
	 * 核对人ID
	 */
	public String getHdrid() {
		return hdrid;
	}

	/**
	 * 核对人ID
	 */
	public void setHdrid(String value) {
		this.hdrid = value;
	}
	/**
	 * 核对人名称
	 */
	public String getHdrmc() {
		return hdrmc;
	}

	/**
	 * 核对人名称
	 */
	public void setHdrmc(String value) {
		this.hdrmc = value;
	}
	/**
	 * 核对结果
	 */
	public String getHdjg() {
		return hdjg;
	}

	/**
	 * 核对结果
	 */
	public void setHdjg(String value) {
		this.hdjg = value;
	}
	/**
	 * 核对时间
	 */
	public java.util.Date getHdsj() {
		return hdsj;
	}

	/**
	 * 核对时间
	 */
	public void setHdsj(java.util.Date value) {
		this.hdsj = value;
	}
	/**
	 * 审核人
	 */
	public String getShr() {
		return shr;
	}

	/**
	 * 审核人
	 */
	public void setShr(String value) {
		this.shr = value;
	}
	/**
	 * 审核意见
	 */
	public String getShyj() {
		return shyj;
	}

	/**
	 * 审核意见
	 */
	public void setShyj(String value) {
		this.shyj = value;
	}
	/**
	 * 审核结果
	 */
	public String getShjg() {
		return shjg;
	}

	/**
	 * 审核结果
	 */
	public void setShjg(String value) {
		this.shjg = value;
	}
	/**
	 * 审核人ID
	 */
	public String getShrid() {
		return shrid;
	}

	/**
	 * 审核人ID
	 */
	public void setShrid(String value) {
		this.shrid = value;
	}
	/**
	 * 审核时间
	 */
	public java.util.Date getShsj() {
		return shsj;
	}

	/**
	 * 审核时间
	 */
	public void setShsj(java.util.Date value) {
		this.shsj = value;
	}
	/**
	 * 撤销时间
	 */
	public java.util.Date getCxsj() {
		return cxsj;
	}

	/**
	 * 撤销时间
	 */
	public void setCxsj(java.util.Date value) {
		this.cxsj = value;
	}
	/**
	 * 撤销人
	 */
	public String getCxrmc() {
		return cxrmc;
	}

	/**
	 * 撤销人
	 */
	public void setCxrmc(String value) {
		this.cxrmc = value;
	}
	/**
	 * 撤销原因
	 */
	public String getCxyy() {
		return cxyy;
	}

	/**
	 * 撤销原因
	 */
	public void setCxyy(String value) {
		this.cxyy = value;
	}
	/**
	 * 撤销人ID
	 */
	public String getCxrid() {
		return cxrid;
	}

	/**
	 * 撤销人ID
	 */
	public void setCxrid(String value) {
		this.cxrid = value;
	}

	@Override
	public String getProjectname() {
		
		return this.ccrxm;
	}
	
	private String sexeid;

	@Override
	public String getSexeid() {
		// TODO Auto-generated method stub
		return this.sexeid;
	}

	@Override
	public void setSexeid(String arg0) {
		this.sexeid=arg0;
		
	}
	
	/**
	 * 出差随行人员
	 */
	private Set<GzccsxryEntity> ccsxry = new LinkedHashSet<GzccsxryEntity>();
	public Set<GzccsxryEntity> getCcsxry() {
		return ccsxry;
	}
	public void setCcsxry(Set<GzccsxryEntity> ccsxry) {
		this.ccsxry = ccsxry;
	}
	
	/**
	 * 出差目的地
	 */
	private String mdd;

	public String getMdd() {
		return mdd;
	}

	public void setMdd(String mdd) {
		this.mdd = mdd;
	}
	
	/**
	 * 出差目的地
	 */
	private String cfd;

	public String getCfd() {
		return cfd;
	}

	public void setCfd(String cfd) {
		this.cfd = cfd;
	}
	
}