package com.itfreer.gzsqclqd.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzsqclqd.entity.GzsqclqdEntity;

/**
 * 定义申请材料清单接口
 */
public interface GzsqclqdService extends BaseService<GzsqclqdEntity> {
	
}
