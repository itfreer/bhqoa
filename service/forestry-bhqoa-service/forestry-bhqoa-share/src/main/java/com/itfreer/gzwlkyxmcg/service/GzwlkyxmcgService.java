package com.itfreer.gzwlkyxmcg.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzwlkyxmcg.entity.GzwlkyxmcgEntity;

/**
 * 定义外来科研项目成果接口
 */
public interface GzwlkyxmcgService extends BaseService<GzwlkyxmcgEntity> {
	
}
