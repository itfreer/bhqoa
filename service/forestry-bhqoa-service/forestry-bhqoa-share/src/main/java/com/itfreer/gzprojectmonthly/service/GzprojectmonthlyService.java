package com.itfreer.gzprojectmonthly.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzprojectmonthly.entity.GzprojectmonthlyEntity;

/**
 * 定义项目月报接口
 */
public interface GzprojectmonthlyService extends BaseService<GzprojectmonthlyEntity> {
	
}
