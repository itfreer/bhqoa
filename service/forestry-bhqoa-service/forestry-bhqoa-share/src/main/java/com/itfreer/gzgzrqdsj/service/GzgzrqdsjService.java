package com.itfreer.gzgzrqdsj.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzgzrqdsj.entity.GzgzrqdsjEntity;

/**
 * 定义签到时间接口
 */
public interface GzgzrqdsjService extends BaseService<GzgzrqdsjEntity> {
	
}
