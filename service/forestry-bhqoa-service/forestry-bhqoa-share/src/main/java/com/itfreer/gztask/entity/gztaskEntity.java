package com.itfreer.gztask.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;

import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义任务安排实体
 */
public class gztaskEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	@Id
	private String id;
	
	
	/**
	 * 任务ID
	 */
	private String fwid;
	
	
	/**
	 * 接收人ID
	 */
	private String jsrid;
	
	/**
	 * 接收人名称
	 */
	private String jsrmc;
	
	
	/**
	 * 接收人部门
	 */
	@DictionaryField(dictionaryName = "p_department" ,toFieldName = "jsrjgName")
	private String jsrjg;
	
	/**
	 * 接收人部门名称
	 */
	private String jsrjgName;
	
	public String getJsrjgName() {
		return jsrjgName;
	}

	public void setJsrjgName(String jsrjgName) {
		this.jsrjgName = jsrjgName;
	}
	
	
	/**
	 * 接收人部门
	 */
	private String jsrjgmc;
	

	public String getJsrjgmc() {
		return jsrjgmc;
	}

	public void setJsrjgmc(String jsrjgmc) {
		this.jsrjgmc = jsrjgmc;
	}


	/**
	 * 确认时间
	 */
	private java.util.Date qrsj;
	
	
	/**
	 * 是否确认
	 */
	@DictionaryField(dictionaryName="p_yesorno", toFieldName="sfqrName")
	private String sfqr;
	
	
	/**
	 * 是否确认
	 */
	private String sfqrName;
	
	
	public String getSfqrName() {
		return sfqrName;
	}

	public void setSfqrName(String sfqrName) {
		this.sfqrName = sfqrName;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	
	public String getFwid() {
		return fwid;
	}

	public void setFwid(String fwid) {
		this.fwid = fwid;
	}

	public String getJsrid() {
		return jsrid;
	}

	public void setJsrid(String jsrid) {
		this.jsrid = jsrid;
	}

	public String getJsrmc() {
		return jsrmc;
	}

	public void setJsrmc(String jsrmc) {
		this.jsrmc = jsrmc;
	}

	public String getJsrjg() {
		return jsrjg;
	}

	public void setJsrjg(String jsrjg) {
		this.jsrjg = jsrjg;
	}

	public java.util.Date getQrsj() {
		return qrsj;
	}

	public void setQrsj(java.util.Date qrsj) {
		this.qrsj = qrsj;
	}

	public String getSfqr() {
		return sfqr;
	}

	public void setSfqr(String sfqr) {
		this.sfqr = sfqr;
	}
}