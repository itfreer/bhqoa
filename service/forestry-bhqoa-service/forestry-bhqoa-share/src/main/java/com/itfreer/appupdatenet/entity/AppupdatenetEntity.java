package com.itfreer.appupdatenet.entity;

import java.io.Serializable;

/**
 * 定义APP更新实体
 */
public class AppupdatenetEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 版本
	 */
	private String version;
	
	/**
	 * 版本编号
	 */
	private Integer numversion;
	
	
	/**
	 * 更新内容
	 */
	private String content;
	
	
	/**
	 * 下载地址
	 */
	private String url;
	
	/**
	 * 创建时间
	 */
	private java.util.Date createtime;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 版本
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * 版本
	 */
	public void setVersion(String value) {
		this.version = value;
	}
	/**
	 * 版本编号
	 */
	public Integer getNumversion() {
		return numversion;
	}

	/**
	 * 版本编号
	 */
	public void setNumversion(Integer value) {
		this.numversion = value;
	}
	/**
	 * 更新内容
	 */
	public String getContent() {
		return content;
	}

	/**
	 * 更新内容
	 */
	public void setContent(String value) {
		this.content = value;
	}
	/**
	 * 下载地址
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * 下载地址
	 */
	public void setUrl(String value) {
		this.url = value;
	}
	/**
	 * 创建时间
	 */
	public java.util.Date getCreatetime() {
		return createtime;
	}

	/**
	 * 创建时间
	 */
	public void setCreatetime(java.util.Date value) {
		this.createtime = value;
	}
}