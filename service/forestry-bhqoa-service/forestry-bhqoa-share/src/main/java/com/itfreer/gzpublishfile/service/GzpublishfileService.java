package com.itfreer.gzpublishfile.service;

import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.gzpublishfile.entity.GzpublishfileEntity;

/**
 * 定义发文管理接口
 */
public interface GzpublishfileService extends IBaseWorkFlowService<GzpublishfileEntity> {
	
}
