package com.itfreer.gzglzda.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzglzda.entity.GzglzdaEntity;

/**
 * 定义管理站档案接口
 */
public interface GzglzdaService extends BaseService<GzglzdaEntity> {
	
}
