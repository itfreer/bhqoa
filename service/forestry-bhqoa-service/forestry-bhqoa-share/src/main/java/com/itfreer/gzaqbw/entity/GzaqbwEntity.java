package com.itfreer.gzaqbw.entity;

import java.io.Serializable;

import javax.persistence.Column;

/**
 * 定义安全保卫实体
 */
public class GzaqbwEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;
	
	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	private String id;
	
	/**
	 * 记录时间
	 */
	private java.util.Date djlsj;
	
	
	/**
	 * 值班人
	 */
	private String szbr;
	
	
	/**
	 * 巡查范围
	 */
	private String sxcfw;
	
	
	/**
	 * 巡查时间段
	 */
	private String sxcsjd;
	
	
	/**
	 * 异常情况
	 */
	private String sycqk;
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 记录时间
	 */
	public java.util.Date getDjlsj() {
		return djlsj;
	}

	/**
	 * 记录时间
	 */
	public void setDjlsj(java.util.Date value) {
		this.djlsj = value;
	}
	/**
	 * 值班人
	 */
	public String getSzbr() {
		return szbr;
	}

	/**
	 * 值班人
	 */
	public void setSzbr(String value) {
		this.szbr = value;
	}
	/**
	 * 巡查范围
	 */
	public String getSxcfw() {
		return sxcfw;
	}

	/**
	 * 巡查范围
	 */
	public void setSxcfw(String value) {
		this.sxcfw = value;
	}
	/**
	 * 巡查时间段
	 */
	public String getSxcsjd() {
		return sxcsjd;
	}

	/**
	 * 巡查时间段
	 */
	public void setSxcsjd(String value) {
		this.sxcsjd = value;
	}
	/**
	 * 异常情况
	 */
	public String getSycqk() {
		return sycqk;
	}

	/**
	 * 异常情况
	 */
	public void setSycqk(String value) {
		this.sycqk = value;
	}
}