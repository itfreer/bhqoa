package com.itfreer.gzdevicemaintain.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 定义设备维修记录实体
 */
public class GzdevicemaintainEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	/**
	 * I_VERSION
	 */
	private Integer iversion;
	
	
	/**
	 * 设备编号
	 */
	private String ssbbh;
	
	/**
	 * 设备名称
	 */
	private String ssbmc;
	
	
	/**
	 * 维修单位
	 */
	private String swxdw;
	
	
	/**
	 * 故障描述
	 */
	private String sgzms;
	
	
	/**
	 * 申请人
	 */
	private String ssqr;
	
	/**
	 * 申请时间
	 */
	private java.util.Date dsqsj;
	
	
	
	
	/**
	 * 故障报告接收人
	 */
	private String sgzbgjsr;
	
	/**
	 * 报告接收日期
	 */
	private java.util.Date dbgjsrq;
	
	
	/**
	 * 故障维修工程师
	 */
	private String sgzwxgcs;
	
	/**
	 * 故障维修日期
	 */
	private java.util.Date dgzwxrq;
	
	
	/**
	 * 故障诊断报告
	 */
	private String sgzzdbg;
	
	
	/**
	 * 故障维修报告
	 */
	private String sgzwxbg;
	
	
	/**
	 * 维修工程师
	 */
	private String swxgcs;
	
	/**
	 * 维修日期
	 */
	private java.util.Date dwxrq;
	
	
	/**
	 * 维修完成情况
	 */
	private String swxwcqk;
	
	
	/**
	 * 服务态度
	 */
	private String sfwtd;
	
	
	/**
	 * 维修效率
	 */
	private String swxxl;
	
	
	/**
	 * 维修单位负责人
	 */
	private String swxdwfzr;
	
	/**
	 * 维修费用
	 */
	private Double wxfy;
	
	/**
	 * 维修费用附件
	 */
	private String wxfyfj;
	
	/**
	 * 维修审批意见
	 */
	private String spyj;
	
	/**
	 * 审批人
	 */
	private String sprid;
	
	/**
	 * 审批人名称
	 */
	private String sprname;
	/**
	 * 审批时间
	 */
	private Date sprq;
	
	/**
	 * 维修验收人
	 */
	private String wxysrid;
	
	/**
	 * 维修验收人名称
	 */
	private String wxysrname;
	/**
	 * 验收时间
	 */
	private Date wxyssj;
	
	/**
	 * 维修验收意见
	 */
	private  String wxysyj;
	/**
	 * 维修单位联系方式
	 */
	private String swxdwlxfs;
	
	
	
	public String getSwxdwlxfs() {
		return swxdwlxfs;
	}

	public void setSwxdwlxfs(String swxdwlxfs) {
		this.swxdwlxfs = swxdwlxfs;
	}

	public Double getWxfy() {
		return wxfy;
	}

	public void setWxfy(Double wxfy) {
		this.wxfy = wxfy;
	}

	public String getSpyj() {
		return spyj;
	}

	public void setSpyj(String spyj) {
		this.spyj = spyj;
	}

	public String getSprid() {
		return sprid;
	}

	public void setSprid(String sprid) {
		this.sprid = sprid;
	}

	public String getSprname() {
		return sprname;
	}

	public void setSprname(String sprname) {
		this.sprname = sprname;
	}

	public Date getSprq() {
		return sprq;
	}

	public void setSprq(Date sprq) {
		this.sprq = sprq;
	}

	public String getWxysrid() {
		return wxysrid;
	}

	public void setWxysrid(String wxysrid) {
		this.wxysrid = wxysrid;
	}

	public String getWxysrname() {
		return wxysrname;
	}

	public void setWxysrname(String wxysrname) {
		this.wxysrname = wxysrname;
	}

	public Date getWxyssj() {
		return wxyssj;
	}

	public void setWxyssj(Date wxyssj) {
		this.wxyssj = wxyssj;
	}

	public String getWxysyj() {
		return wxysyj;
	}

	public void setWxysyj(String wxysyj) {
		this.wxysyj = wxysyj;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * I_VERSION
	 */
	public Integer getIversion() {
		return iversion;
	}

	/**
	 * I_VERSION
	 */
	public void setIversion(Integer value) {
		this.iversion = value;
	}
	/**
	 * 设备编号
	 */
	public String getSsbbh() {
		return ssbbh;
	}

	/**
	 * 设备编号
	 */
	public void setSsbbh(String value) {
		this.ssbbh = value;
	}
	/**
	 * 维修单位
	 */
	public String getSwxdw() {
		return swxdw;
	}

	/**
	 * 维修单位
	 */
	public void setSwxdw(String value) {
		this.swxdw = value;
	}
	/**
	 * 故障描述
	 */
	public String getSgzms() {
		return sgzms;
	}

	/**
	 * 故障描述
	 */
	public void setSgzms(String value) {
		this.sgzms = value;
	}
	/**
	 * 申请人
	 */
	public String getSsqr() {
		return ssqr;
	}

	/**
	 * 申请人
	 */
	public void setSsqr(String value) {
		this.ssqr = value;
	}
	/**
	 * 申请时间
	 */
	public java.util.Date getDsqsj() {
		return dsqsj;
	}

	/**
	 * 申请时间
	 */
	public void setDsqsj(java.util.Date value) {
		this.dsqsj = value;
	}
	/**
	 * 故障报告接收人
	 */
	public String getSgzbgjsr() {
		return sgzbgjsr;
	}

	/**
	 * 故障报告接收人
	 */
	public void setSgzbgjsr(String value) {
		this.sgzbgjsr = value;
	}
	/**
	 * 报告接收日期
	 */
	public java.util.Date getDbgjsrq() {
		return dbgjsrq;
	}

	/**
	 * 报告接收日期
	 */
	public void setDbgjsrq(java.util.Date value) {
		this.dbgjsrq = value;
	}
	/**
	 * 故障维修工程师
	 */
	public String getSgzwxgcs() {
		return sgzwxgcs;
	}

	/**
	 * 故障维修工程师
	 */
	public void setSgzwxgcs(String value) {
		this.sgzwxgcs = value;
	}
	/**
	 * 故障维修日期
	 */
	public java.util.Date getDgzwxrq() {
		return dgzwxrq;
	}

	/**
	 * 故障维修日期
	 */
	public void setDgzwxrq(java.util.Date value) {
		this.dgzwxrq = value;
	}
	/**
	 * 故障诊断报告
	 */
	public String getSgzzdbg() {
		return sgzzdbg;
	}

	/**
	 * 故障诊断报告
	 */
	public void setSgzzdbg(String value) {
		this.sgzzdbg = value;
	}
	/**
	 * 故障维修报告
	 */
	public String getSgzwxbg() {
		return sgzwxbg;
	}

	/**
	 * 故障维修报告
	 */
	public void setSgzwxbg(String value) {
		this.sgzwxbg = value;
	}
	/**
	 * 维修工程师
	 */
	public String getSwxgcs() {
		return swxgcs;
	}

	/**
	 * 维修工程师
	 */
	public void setSwxgcs(String value) {
		this.swxgcs = value;
	}
	/**
	 * 维修日期
	 */
	public java.util.Date getDwxrq() {
		return dwxrq;
	}

	/**
	 * 维修日期
	 */
	public void setDwxrq(java.util.Date value) {
		this.dwxrq = value;
	}
	/**
	 * 维修完成情况
	 */
	public String getSwxwcqk() {
		return swxwcqk;
	}

	/**
	 * 维修完成情况
	 */
	public void setSwxwcqk(String value) {
		this.swxwcqk = value;
	}
	/**
	 * 服务态度
	 */
	public String getSfwtd() {
		return sfwtd;
	}

	/**
	 * 服务态度
	 */
	public void setSfwtd(String value) {
		this.sfwtd = value;
	}
	/**
	 * 维修效率
	 */
	public String getSwxxl() {
		return swxxl;
	}

	/**
	 * 维修效率
	 */
	public void setSwxxl(String value) {
		this.swxxl = value;
	}
	/**
	 * 维修单位负责人
	 */
	public String getSwxdwfzr() {
		return swxdwfzr;
	}
	
	public String getWxfyfj() {
		return wxfyfj;
	}

	public void setWxfyfj(String wxfyfj) {
		this.wxfyfj = wxfyfj;
	}


	/**
	 * 维修单位负责人
	 */
	public void setSwxdwfzr(String value) {
		this.swxdwfzr = value;
	}
	
	public String getSsbmc() {
		return ssbmc;
	}

	public void setSsbmc(String ssbmc) {
		this.ssbmc = ssbmc;
	}
}