package com.itfreer.gzgzjjr.entity;

import java.io.Serializable;

import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义工作节假日实体
 */
public class GzgzjjrEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	
	/**
	 * id
	 */
	private String id;
	
	
	/**
	 * 类型
	 */
	@DictionaryField(dictionaryName="p_gzjjr", toFieldName="typeName")
	private String type;
	
	
	/**
	 * 类型
	 */
	private String typeName;
	
	
	/**
	 * 日期
	 */
	private java.util.Date spedate;
	
	
	/**
	 * 备注
	 */
	private String bz;
	

	/**
	 * id
	 */
	public String getId() {
		return id;
	}

	/**
	 * id
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 类型
	 */
	public String getType() {
		return type;
	}

	/**
	 * 类型
	 */
	public void setType(String value) {
		this.type = value;
	}
	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	/**
	 * 日期
	 */
	public java.util.Date getSpedate() {
		return spedate;
	}

	/**
	 * 日期
	 */
	public void setSpedate(java.util.Date value) {
		this.spedate = value;
	}
	/**
	 * 备注
	 */
	public String getBz() {
		return bz;
	}

	/**
	 * 备注
	 */
	public void setBz(String value) {
		this.bz = value;
	}
}