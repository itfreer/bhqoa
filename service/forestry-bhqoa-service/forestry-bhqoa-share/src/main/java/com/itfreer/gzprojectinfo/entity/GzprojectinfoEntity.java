package com.itfreer.gzprojectinfo.entity;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;

import com.itfreer.bpm.flow.api.IBpmProjectEntity;
import com.itfreer.form.dictionary.reflect.DictionaryField;
import com.itfreer.gzkyxmcg.entity.GzkyxmcgEntity;
import com.itfreer.gzplanpersonnel.entity.GzplanpersonnelEntity;

/**
 * 定义项目立项管理实体
 */
public class GzprojectinfoEntity implements IBpmProjectEntity{
	private static final long serialVersionUID = -8443497714033818376L;
	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 项目编号
	 */
	private String sxmbh;
	
	
	/**
	 * 项目名称
	 */
	private String sxmmc;
	
	/**
	 * 项目内容
	 */
	private String sxmnr;
	
	
	/**
	 * 项目规模
	 */
	private String sxmgm;
	
	
	/**
	 * 项目状态
	 */
	@DictionaryField(dictionaryName = "PRO_XMZT" ,toFieldName = "sxmztname")
	private String sxmzt;
	
	/**
	 * 项目状态名称
	 */
	private String sxmztname;
	
	/**
	 * 项目经理（项目负责人）
	 */
	private String sxmjl;
	
	/**
	 * 项目经理（项目负责人）名称
	 */
	private String sxmjlname;
	
	/**
	 * 联系电话
	 */
	private String slxdh;
	
	/**
	 * 办公电话
	 */
	private String sbgdh;
	
	/**
	 * 电子邮箱
	 */
	private String sdzyx;
	
	/**
	 * 备注
	 */
	private String sbz;
	
	
	/**
	 *申请部门
	 */
	@DictionaryField(dictionaryName = "p_department" ,toFieldName = "sqbmName")
	private String sqbm;
	/**
	 *申请部门
	 */
	private String sqbmName;
	/**
	 *申请人
	 */
	private String sqr;
	/**
	 *申请人ID
	 */
	private String sqrid;
	/**
	 *项目来源
	 */
	private String xmly;
	/**
	 *立项情况
	 */
	private String lxqk;
	/**
	 * 申请时间
	 */
	private java.util.Date sqsj;
	/**
	 * 结项时间
	 */
	private java.util.Date jxsj;
	
	
	public String getSqbm() {
		return sqbm;
	}

	public void setSqbm(String sqbm) {
		this.sqbm = sqbm;
	}

	public String getSqbmName() {
		return sqbmName;
	}

	public void setSqbmName(String sqbmName) {
		this.sqbmName = sqbmName;
	}

	public String getSqr() {
		return sqr;
	}

	public void setSqr(String sqr) {
		this.sqr = sqr;
	}
	
	public String getSqrid() {
		return sqrid;
	}

	public void setSqrid(String sqrid) {
		this.sqrid = sqrid;
	}

	public String getXmly() {
		return xmly;
	}

	public void setXmly(String xmly) {
		this.xmly = xmly;
	}

	public String getLxqk() {
		return lxqk;
	}

	public void setLxqk(String lxqk) {
		this.lxqk = lxqk;
	}

	public java.util.Date getSqsj() {
		return sqsj;
	}

	public void setSqsj(java.util.Date sqsj) {
		this.sqsj = sqsj;
	}

	public java.util.Date getJxsj() {
		return jxsj;
	}

	public void setJxsj(java.util.Date jxsj) {
		this.jxsj = jxsj;
	}

	public String getSxmztname() {
		return sxmztname;
	}

	public void setSxmztname(String sxmztname) {
		this.sxmztname = sxmztname;
	}

	/**
	 *项目成效
	 */
	private String Sxmcx;
	public String getSxmjlname() {
		return sxmjlname;
	}

	public void setSxmjlname(String sxmjlname) {
		this.sxmjlname = sxmjlname;
	}

	public String getSxmnr() {
		return sxmnr;
	}

	public void setSxmnr(String sxmnr) {
		this.sxmnr = sxmnr;
	}

	public String getSlxdh() {
		return slxdh;
	}

	public void setSlxdh(String slxdh) {
		this.slxdh = slxdh;
	}

	public String getSbgdh() {
		return sbgdh;
	}

	public void setSbgdh(String sbgdh) {
		this.sbgdh = sbgdh;
	}

	public String getSdzyx() {
		return sdzyx;
	}

	public void setSdzyx(String sdzyx) {
		this.sdzyx = sdzyx;
	}

	public String getSbz() {
		return sbz;
	}

	public void setSbz(String sbz) {
		this.sbz = sbz;
	}

	public String getSxmcx() {
		return Sxmcx;
	}

	public void setSxmcx(String sxmcx) {
		Sxmcx = sxmcx;
	}

	/**
	 * 项目类型
	 */
	@DictionaryField(dictionaryName="p_projectlx", toFieldName="sxmlxName")
	private String sxmlx;
	
	
	/**
	 * 项目类型
	 */
	private String sxmlxName;
	
	
	/**
	 * 项目性质
	 */
	@DictionaryField(dictionaryName="p_projectxz", toFieldName="sxmxzName")
	private String sxmxz;
	
	
	/**
	 * 项目性质
	 */
	private String sxmxzName;
	
	

	
	
	/**
	 * 工作区域省
	 */
	private String ssheng;
	
	
	/**
	 * 工作区域市
	 */
	private String sshi;
	
	
	/**
	 * 工作区域县
	 */
	private String sxian;
	
	
	/**
	 * 工作地点
	 */
	private String sgzdd;
	
	
	/**
	 * 是否野外项目
	 */
	@DictionaryField(dictionaryName="p_yesorno", toFieldName="ssfywxmName")
	private String ssfywxm;
	
	
	/**
	 * 是否野外项目
	 */
	private String ssfywxmName;
	
	
	public String getSsfywxmName() {
		return ssfywxmName;
	}

	public void setSsfywxmName(String ssfywxmName) {
		this.ssfywxmName = ssfywxmName;
	}


	/**
	 * 作业部门
	 */
	@DictionaryField(dictionaryName="p_department", toFieldName="szybmName")
	private String szybm;
	
	
	/**
	 * 作业部门
	 */
	private String szybmName;
	
	
	public String getSxmlxName() {
		return sxmlxName;
	}

	public void setSxmlxName(String sxmlxName) {
		this.sxmlxName = sxmlxName;
	}

	public String getSxmxzName() {
		return sxmxzName;
	}

	public void setSxmxzName(String sxmxzName) {
		this.sxmxzName = sxmxzName;
	}

	public String getSzybmName() {
		return szybmName;
	}

	public void setSzybmName(String szybmName) {
		this.szybmName = szybmName;
	}

	/**
	 * 标书
	 */
	private String sbs;
	
	
	/**
	 * 合同
	 */
	private String sht;
	
	
	/**
	 * 合同期限
	 */
	private String shtqx;
	
	/**
	 * 合同签订日期
	 */
	private java.util.Date dhtqdrq;
	
	
	/**
	 * 委托单位
	 */
	private String swtdw;
	
	/**
	 * 登记日期
	 */
	private java.util.Date sdjrq;
	
	
	/**
	 * 实施规划书
	 */
	private String sssghs;
	
	
	/**
	 * 技术设计书
	 */
	private String sjssjs;
	
	
	/**
	 * 过程资料
	 */
	private String sgczl;
	
	/**
	 * 开工日期
	 */
	private java.util.Date dkgrq;
	
	/**
	 * 竣工日期
	 */
	private java.util.Date djgrq;
	
	
	/**
	 * 产品质量
	 */
	private String scpzl;
	
	
	/**
	 * 服务质量
	 */
	private String sfwzl;
	
	
	/**
	 * 合同履约
	 */
	private String shtly;
	
	
	/**
	 * 收费合理性
	 */
	private String ssfhlx;
	
	
	/**
	 * 调查方式
	 */
	private String sdcfs;
	

	
	
	/**
	 * 流程编号
	 */
	private String slcbh;
	
	
	/**
	 * ID
	 */
	private String id;
	
	/**
	 * 附件
	 */
	private String fj;

	public String getFj() {
		return fj;
	}

	public void setFj(String fj) {
		this.fj = fj;
	}
	
	
	/**
	 * 成果材料
	 */
	private String cgcl;
	
	public String getCgcl() {
		return cgcl;
	}

	public void setCgcl(String cgcl) {
		this.cgcl = cgcl;
	}
	
	
	/**
	 * 项目人员
	 */
	private Set<GzplanpersonnelEntity> gzplanpersonnel=new LinkedHashSet<GzplanpersonnelEntity>();
	
	/**
	 * 项目人员
	 * @return
	 */
	public Set<GzplanpersonnelEntity> getGzplanpersonnel() {
		return gzplanpersonnel;
	}

	/**
	 * 项目人员
	 * @return
	 */
	public void setGzplanpersonnel(Set<GzplanpersonnelEntity> gzplanpersonnel) {
		this.gzplanpersonnel = gzplanpersonnel;
	}
	
	/**
	 * 项目成果
	 */
	private Set<GzkyxmcgEntity> xmcg=new LinkedHashSet<GzkyxmcgEntity>();
	
	public Set<GzkyxmcgEntity> getXmcg() {
		return xmcg;
	}

	public void setXmcg(Set<GzkyxmcgEntity> xmcg) {
		this.xmcg = xmcg;
	}

	/**
	 * 项目编号
	 */
	public String getSxmbh() {
		return sxmbh;
	}

	/**
	 * 项目编号
	 */
	public void setSxmbh(String value) {
		this.sxmbh = value;
	}
	/**
	 * 项目名称
	 */
	public String getSxmmc() {
		return sxmmc;
	}

	/**
	 * 项目名称
	 */
	public void setSxmmc(String value) {
		this.sxmmc = value;
	}
	/**
	 * 项目类型
	 */
	public String getSxmlx() {
		return sxmlx;
	}

	/**
	 * 项目类型
	 */
	public void setSxmlx(String value) {
		this.sxmlx = value;
	}
	/**
	 * 项目性质
	 */
	public String getSxmxz() {
		return sxmxz;
	}

	/**
	 * 项目性质
	 */
	public void setSxmxz(String value) {
		this.sxmxz = value;
	}
	/**
	 * 项目规模
	 */
	public String getSxmgm() {
		return sxmgm;
	}

	/**
	 * 项目规模
	 */
	public void setSxmgm(String value) {
		this.sxmgm = value;
	}
	/**
	 * 工作区域省
	 */
	public String getSsheng() {
		return ssheng;
	}

	/**
	 * 工作区域省
	 */
	public void setSsheng(String value) {
		this.ssheng = value;
	}
	/**
	 * 工作区域市
	 */
	public String getSshi() {
		return sshi;
	}

	/**
	 * 工作区域市
	 */
	public void setSshi(String value) {
		this.sshi = value;
	}
	/**
	 * 工作区域县
	 */
	public String getSxian() {
		return sxian;
	}

	/**
	 * 工作区域县
	 */
	public void setSxian(String value) {
		this.sxian = value;
	}
	/**
	 * 工作地点
	 */
	public String getSgzdd() {
		return sgzdd;
	}

	/**
	 * 工作地点
	 */
	public void setSgzdd(String value) {
		this.sgzdd = value;
	}
	/**
	 * 是否野外项目
	 */
	public String getSsfywxm() {
		return ssfywxm;
	}

	/**
	 * 是否野外项目
	 */
	public void setSsfywxm(String value) {
		this.ssfywxm = value;
	}
	/**
	 * 作业部门
	 */
	public String getSzybm() {
		return szybm;
	}

	/**
	 * 作业部门
	 */
	public void setSzybm(String value) {
		this.szybm = value;
	}
	/**
	 * 项目经理
	 */
	public String getSxmjl() {
		return sxmjl;
	}

	/**
	 * 项目经理
	 */
	public void setSxmjl(String value) {
		this.sxmjl = value;
	}
	/**
	 * 标书
	 */
	public String getSbs() {
		return sbs;
	}

	/**
	 * 标书
	 */
	public void setSbs(String value) {
		this.sbs = value;
	}
	/**
	 * 合同
	 */
	public String getSht() {
		return sht;
	}

	/**
	 * 合同
	 */
	public void setSht(String value) {
		this.sht = value;
	}
	/**
	 * 合同期限
	 */
	public String getShtqx() {
		return shtqx;
	}

	/**
	 * 合同期限
	 */
	public void setShtqx(String value) {
		this.shtqx = value;
	}
	/**
	 * 合同签订日期
	 */
	public java.util.Date getDhtqdrq() {
		return dhtqdrq;
	}

	/**
	 * 合同签订日期
	 */
	public void setDhtqdrq(java.util.Date value) {
		this.dhtqdrq = value;
	}
	/**
	 * 委托单位
	 */
	public String getSwtdw() {
		return swtdw;
	}

	/**
	 * 委托单位
	 */
	public void setSwtdw(String value) {
		this.swtdw = value;
	}
	/**
	 * 登记日期
	 */
	public java.util.Date getSdjrq() {
		return sdjrq;
	}

	/**
	 * 登记日期
	 */
	public void setSdjrq(java.util.Date value) {
		this.sdjrq = value;
	}
	/**
	 * 实施规划书
	 */
	public String getSssghs() {
		return sssghs;
	}

	/**
	 * 实施规划书
	 */
	public void setSssghs(String value) {
		this.sssghs = value;
	}
	/**
	 * 技术设计书
	 */
	public String getSjssjs() {
		return sjssjs;
	}

	/**
	 * 技术设计书
	 */
	public void setSjssjs(String value) {
		this.sjssjs = value;
	}
	/**
	 * 过程资料
	 */
	public String getSgczl() {
		return sgczl;
	}

	/**
	 * 过程资料
	 */
	public void setSgczl(String value) {
		this.sgczl = value;
	}
	/**
	 * 开工日期
	 */
	public java.util.Date getDkgrq() {
		return dkgrq;
	}

	/**
	 * 开工日期
	 */
	public void setDkgrq(java.util.Date value) {
		this.dkgrq = value;
	}
	/**
	 * 竣工日期
	 */
	public java.util.Date getDjgrq() {
		return djgrq;
	}

	/**
	 * 竣工日期
	 */
	public void setDjgrq(java.util.Date value) {
		this.djgrq = value;
	}
	/**
	 * 产品质量
	 */
	public String getScpzl() {
		return scpzl;
	}

	/**
	 * 产品质量
	 */
	public void setScpzl(String value) {
		this.scpzl = value;
	}
	/**
	 * 服务质量
	 */
	public String getSfwzl() {
		return sfwzl;
	}

	/**
	 * 服务质量
	 */
	public void setSfwzl(String value) {
		this.sfwzl = value;
	}
	/**
	 * 合同履约
	 */
	public String getShtly() {
		return shtly;
	}

	/**
	 * 合同履约
	 */
	public void setShtly(String value) {
		this.shtly = value;
	}
	/**
	 * 收费合理性
	 */
	public String getSsfhlx() {
		return ssfhlx;
	}

	/**
	 * 收费合理性
	 */
	public void setSsfhlx(String value) {
		this.ssfhlx = value;
	}
	/**
	 * 调查方式
	 */
	public String getSdcfs() {
		return sdcfs;
	}

	/**
	 * 调查方式
	 */
	public void setSdcfs(String value) {
		this.sdcfs = value;
	}
	/**
	 * 项目状态
	 */
	public String getSxmzt() {
		return sxmzt;
	}

	/**
	 * 项目状态
	 */
	public void setSxmzt(String value) {
		this.sxmzt = value;
	}
	/**
	 * 流程编号
	 */
	public String getSlcbh() {
		return slcbh;
	}

	/**
	 * 流程编号
	 */
	public void setSlcbh(String value) {
		this.slcbh = value;
	}
	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	
	private String sexeid;

	@Override
	public String getProjectname() {
		return this.sxmmc;
	}

	@Override
	public String getSexeid() {
		return this.sexeid;
	}

	@Override
	public void setSexeid(String sexeid) {
		this.sexeid=sexeid;
	}
}