package com.itfreer.gzykcz.entity;

import java.io.Serializable;

/**
 * 定义油卡充值实体
 */
public class GzykczEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	private String id;
	
	
	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	private String sclxx;
	
	/**
	 * 充值时间
	 */
	private java.util.Date dczsj;
	
	
	/**
	 * 充值地点
	 */
	private String sczdd;
	
	
	/**
	 * 充值人
	 */
	private String sczr;
	
	
	/**
	 * 联系方式
	 */
	private String slxfs;
	
	
	/**
	 * 充值金额
	 */
	private String sczje;
	
	
	/**
	 * 电子发票
	 */
	private String sdzfp;
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	public String getSclxx() {
		return sclxx;
	}

	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	public void setSclxx(String value) {
		this.sclxx = value;
	}
	/**
	 * 充值时间
	 */
	public java.util.Date getDczsj() {
		return dczsj;
	}

	/**
	 * 充值时间
	 */
	public void setDczsj(java.util.Date value) {
		this.dczsj = value;
	}
	/**
	 * 充值地点
	 */
	public String getSczdd() {
		return sczdd;
	}

	/**
	 * 充值地点
	 */
	public void setSczdd(String value) {
		this.sczdd = value;
	}
	/**
	 * 充值人
	 */
	public String getSczr() {
		return sczr;
	}

	/**
	 * 充值人
	 */
	public void setSczr(String value) {
		this.sczr = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxfs() {
		return slxfs;
	}

	/**
	 * 联系方式
	 */
	public void setSlxfs(String value) {
		this.slxfs = value;
	}
	/**
	 * 充值金额
	 */
	public String getSczje() {
		return sczje;
	}

	/**
	 * 充值金额
	 */
	public void setSczje(String value) {
		this.sczje = value;
	}
	/**
	 * 电子发票
	 */
	public String getSdzfp() {
		return sdzfp;
	}

	/**
	 * 电子发票
	 */
	public void setSdzfp(String value) {
		this.sdzfp = value;
	}
}