package com.itfreer.gzwjryrqxk.entity;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import com.itfreer.bpm.flow.api.IBpmProjectEntity;
import com.itfreer.form.dictionary.reflect.DictionaryField;
import com.itfreer.gzplanpersonnel.entity.GzplanpersonnelEntity;
import com.itfreer.gzsqclqd.entity.GzsqclqdEntity;

/**
 * 定义外籍人员入区许可实体
 */
public class GzwjryrqxkEntity  implements IBpmProjectEntity{
	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	private String sexeid;
	
	/**
	 * 唯一编号
	 */
	private String id;
	
	
	/**
	 * 申请编号
	 */
	private String ssqbh;
	
	
	/**
	 * 单位名称
	 */
	private String sdwmc;
	
	
	/**
	 * 项目名称
	 */
	private String sxmmc;
	
	
	/**
	 * 是否有介绍信(函)
	 */
	@DictionaryField(dictionaryName="p_yesorno", toFieldName="ijsxName")
	private String ijsx;
	
	
	/**
	 * 是否有介绍信(函)
	 */
	private String ijsxName;
	
	
	/**
	 * 提供活动方案情况
	 */
	private String shdfaqk;
	
	
	/**
	 * 是否鉴定合作协议
	 */
	@DictionaryField(dictionaryName="p_yesorno", toFieldName="ijdhzxyName")
	private String ijdhzxy;
	
	
	/**
	 * 是否鉴定合作协议
	 */
	private String ijdhzxyName;
	
	
	public String getIjsxName() {
		return ijsxName;
	}

	public void setIjsxName(String ijsxName) {
		this.ijsxName = ijsxName;
	}

	public String getIjdhzxyName() {
		return ijdhzxyName;
	}

	public void setIjdhzxyName(String ijdhzxyName) {
		this.ijdhzxyName = ijdhzxyName;
	}

	/**
	 * 协议编号
	 */
	private String sxybh;
	
	
	/**
	 * 带队负责人
	 */
	private String sddfzr;
	
	
	/**
	 * 职务
	 */
	private String szw;
	
	
	/**
	 * 联系电话
	 */
	private String slxdh;
	
	
	/**
	 * 人数
	 */
	private String srs;
	
	/**
	 * 来访时间
	 */
	private java.util.Date dlfsj;
	
	
	/**
	 * 活动计划时限
	 */
	private String shdjhsx;
	
	
	/**
	 * 项目简介
	 */
	private String sxmjj;
	
	
	/**
	 * 部门审批意见
	 */
	private String sbmspyj;
	
	/**
	 * 部门审批时间
	 */
	private java.util.Date dbmspsj;
	
	
	/**
	 * 部门审批人
	 */
	private String sbmspr;
	
	
	/**
	 * 单位审批意见
	 */
	private String sdwspyj;
	
	/**
	 * 单位审批时间
	 */
	private java.util.Date ddwspsj;
	
	
	/**
	 * 单位审批人
	 */
	private String sdwspr;
	

	/**
	 * 项目编号
	 */
	private String sxmbh;
	
	
	/**
	 * 外来项目类型
	 */
	private String wlxmlx;
	
	
	public String getWlxmlx() {
		return wlxmlx;
	}

	public void setWlxmlx(String wlxmlx) {
		this.wlxmlx = wlxmlx;
	}
	
	public String getSxmbh() {
		return sxmbh;
	}

	public void setSxmbh(String sxmbh) {
		this.sxmbh = sxmbh;
	}

	public String getXmid() {
		return xmid;
	}

	public void setXmid(String xmid) {
		this.xmid = xmid;
	}

	/**
	 * 项目id
	 */
	private String xmid;
	
	
	/**
	 * 项目人员
	 */
	private Set<GzplanpersonnelEntity> gzplanpersonnel=new LinkedHashSet<GzplanpersonnelEntity>();
	
	/**
	 * 项目人员
	 * @return
	 */
	public Set<GzplanpersonnelEntity> getGzplanpersonnel() {
		return gzplanpersonnel;
	}

	/**
	 * 项目人员
	 * @return
	 */
	public void setGzplanpersonnel(Set<GzplanpersonnelEntity> gzplanpersonnel) {
		this.gzplanpersonnel = gzplanpersonnel;
	}
	
	
	/**
	 * 申请材料清单
	 */
	private Set<GzsqclqdEntity> gzsqclqd = new LinkedHashSet<GzsqclqdEntity>();
	
	public Set<GzsqclqdEntity> getGzsqclqd() {
		return gzsqclqd;
	}

	public void setGzsqclqd(Set<GzsqclqdEntity> gzsqclqd) {
		this.gzsqclqd = gzsqclqd;
	}
	
	/**
	 * 附件
	 */
	private String fj;
	
	
	/**
	 * 法定代表人名称
	 */
	private String fddbrmc;
	
	/**
	 * 地址
	 */
	private String sqdwdz;
	
	/**
	 * 联系电话
	 */
	private String sqdwlxdh;
	
	/**
	 * 邮编
	 */
	private String sqdwlxyb;
	
	/**
	 * 申请时间
	 */
	private java.util.Date xzxksqsj;
	
	/**
	 * 行政许可申请事项
	 */
	private String xzxksqsx;
	
	/**
	 * 受理声明
	 */
	private String slsm;
	
	/**
	 * 受理序号
	 */
	private String slxh;
	
	/**
	 * 受理编号
	 */
	private String slbh;
	
	/**
	 * 行政许可审查情况
	 */
	private String xzxkscqk;
	
	/**
	 * 承办人意见
	 */
	private String cbryj;
	
	/**
	 * 审批时间
	 */
	private java.util.Date cbrsj;
	
	/**
	 * 审批人
	 */
	private String cbrmc;
	
	/**
	 * 委托代理人姓名
	 */
	private String wtdlrxm;
	
	/**
	 * 委托代理人身份证号
	 */
	private String wtdlrsfzh;
	
	/**
	 * 委托代理人住址
	 */
	private String wtdlrzz;
	
	/**
	 * 委托代理人电话
	 */
	private String wtdlrdh;
	
	/**
	 * 承办人审批签字
	 */
	private String chrspqz;
	
	/**
	 * 科研科审批签字
	 */
	private String kyksqqz;
	
	/**
	 * 办公室审批签字
	 */
	private String bgssqqz;
	
	/**
	 * 申请书附件
	 */
	private String fjsqs;
	
	/**
	 * 申请材料清单附件
	 */
	private String fjsqclqd;
	
	/**
	 * 受理凭证附件
	 */
	private String fjslpz;
	
	/**
	 * 受理审批表附件
	 */
	private String fjslspb;
	
	/**
	 * 决定审批表附件
	 */
	private String fjjdspb;
	
	/**
	 * 决定书附件
	 */
	private String fjjds;
	
	/**
	 * 办结报告附件
	 */
	private String fjbjbg;
	
	public String getFjsqs() {
		return fjsqs;
	}

	public void setFjsqs(String fjsqs) {
		this.fjsqs = fjsqs;
	}
	
	public String getFjsqclqd() {
		return fjsqclqd;
	}

	public void setFjsqclqd(String fjsqclqd) {
		this.fjsqclqd = fjsqclqd;
	}
	
	public String getFjslpz() {
		return fjslpz;
	}

	public void setFjslpz(String fjslpz) {
		this.fjslpz = fjslpz;
	}
	
	public String getFjslspb() {
		return fjslspb;
	}

	public void setFjslspb(String fjslspb) {
		this.fjslspb = fjslspb;
	}
	
	public String getFjjdspb() {
		return fjjdspb;
	}

	public void setFjjdspb(String fjjdspb) {
		this.fjjdspb = fjjdspb;
	}
	
	public String getFjjds() {
		return fjjds;
	}

	public void setFjjds(String fjjds) {
		this.fjjds = fjjds;
	}
	
	public String getFjbjbg() {
		return fjbjbg;
	}

	public void setFjbjbg(String fjbjbg) {
		this.fjbjbg = fjbjbg;
	}
	
	public String getChrspqz() {
		return chrspqz;
	}

	public void setChrspqz(String chrspqz) {
		this.chrspqz = chrspqz;
	}

	public String getKyksqqz() {
		return kyksqqz;
	}

	public void setKyksqqz(String kyksqqz) {
		this.kyksqqz = kyksqqz;
	}

	public String getBgssqqz() {
		return bgssqqz;
	}

	public void setBgssqqz(String bgssqqz) {
		this.bgssqqz = bgssqqz;
	}
	
	
	
	
	public String getFddbrmc() {
		return fddbrmc;
	}

	public void setFddbrmc(String fddbrmc) {
		this.fddbrmc = fddbrmc;
	}

	public String getSqdwdz() {
		return sqdwdz;
	}

	public void setSqdwdz(String sqdwdz) {
		this.sqdwdz = sqdwdz;
	}

	public String getSqdwlxdh() {
		return sqdwlxdh;
	}

	public void setSqdwlxdh(String sqdwlxdh) {
		this.sqdwlxdh = sqdwlxdh;
	}

	public String getSqdwlxyb() {
		return sqdwlxyb;
	}

	public void setSqdwlxyb(String sqdwlxyb) {
		this.sqdwlxyb = sqdwlxyb;
	}

	public java.util.Date getXzxksqsj() {
		return xzxksqsj;
	}

	public void setXzxksqsj(java.util.Date xzxksqsj) {
		this.xzxksqsj = xzxksqsj;
	}

	public String getXzxksqsx() {
		return xzxksqsx;
	}

	public void setXzxksqsx(String xzxksqsx) {
		this.xzxksqsx = xzxksqsx;
	}

	public String getSlsm() {
		return slsm;
	}

	public void setSlsm(String slsm) {
		this.slsm = slsm;
	}

	public String getSlxh() {
		return slxh;
	}

	public void setSlxh(String slxh) {
		this.slxh = slxh;
	}

	public String getSlbh() {
		return slbh;
	}

	public void setSlbh(String slbh) {
		this.slbh = slbh;
	}

	public String getXzxkscqk() {
		return xzxkscqk;
	}

	public void setXzxkscqk(String xzxkscqk) {
		this.xzxkscqk = xzxkscqk;
	}

	public String getCbryj() {
		return cbryj;
	}

	public void setCbryj(String cbryj) {
		this.cbryj = cbryj;
	}

	public java.util.Date getCbrsj() {
		return cbrsj;
	}

	public void setCbrsj(java.util.Date cbrsj) {
		this.cbrsj = cbrsj;
	}

	public String getCbrmc() {
		return cbrmc;
	}

	public void setCbrmc(String cbrmc) {
		this.cbrmc = cbrmc;
	}

	public String getWtdlrxm() {
		return wtdlrxm;
	}

	public void setWtdlrxm(String wtdlrxm) {
		this.wtdlrxm = wtdlrxm;
	}

	public String getWtdlrsfzh() {
		return wtdlrsfzh;
	}

	public void setWtdlrsfzh(String wtdlrsfzh) {
		this.wtdlrsfzh = wtdlrsfzh;
	}

	public String getWtdlrzz() {
		return wtdlrzz;
	}

	public void setWtdlrzz(String wtdlrzz) {
		this.wtdlrzz = wtdlrzz;
	}

	public String getWtdlrdh() {
		return wtdlrdh;
	}

	public void setWtdlrdh(String wtdlrdh) {
		this.wtdlrdh = wtdlrdh;
	}
	
	public String getFj() {
		return fj;
	}

	public void setFj(String fj) {
		this.fj = fj;
	}
	
	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 申请编号
	 */
	public String getSsqbh() {
		return ssqbh;
	}

	/**
	 * 申请编号
	 */
	public void setSsqbh(String value) {
		this.ssqbh = value;
	}
	/**
	 * 单位名称
	 */
	public String getSdwmc() {
		return sdwmc;
	}

	/**
	 * 单位名称
	 */
	public void setSdwmc(String value) {
		this.sdwmc = value;
	}
	/**
	 * 项目名称
	 */
	public String getSxmmc() {
		return sxmmc;
	}

	/**
	 * 项目名称
	 */
	public void setSxmmc(String value) {
		this.sxmmc = value;
	}
	/**
	 * 是否有介绍信(函)
	 */
	public String getIjsx() {
		return ijsx;
	}

	/**
	 * 是否有介绍信(函)
	 */
	public void setIjsx(String value) {
		this.ijsx = value;
	}
	/**
	 * 提供活动方案情况
	 */
	public String getShdfaqk() {
		return shdfaqk;
	}

	/**
	 * 提供活动方案情况
	 */
	public void setShdfaqk(String value) {
		this.shdfaqk = value;
	}
	/**
	 * 是否鉴定合作协议
	 */
	public String getIjdhzxy() {
		return ijdhzxy;
	}

	/**
	 * 是否鉴定合作协议
	 */
	public void setIjdhzxy(String value) {
		this.ijdhzxy = value;
	}
	/**
	 * 协议编号
	 */
	public String getSxybh() {
		return sxybh;
	}

	/**
	 * 协议编号
	 */
	public void setSxybh(String value) {
		this.sxybh = value;
	}
	/**
	 * 带队负责人
	 */
	public String getSddfzr() {
		return sddfzr;
	}

	/**
	 * 带队负责人
	 */
	public void setSddfzr(String value) {
		this.sddfzr = value;
	}
	/**
	 * 职务
	 */
	public String getSzw() {
		return szw;
	}

	/**
	 * 职务
	 */
	public void setSzw(String value) {
		this.szw = value;
	}
	/**
	 * 联系电话
	 */
	public String getSlxdh() {
		return slxdh;
	}

	/**
	 * 联系电话
	 */
	public void setSlxdh(String value) {
		this.slxdh = value;
	}
	/**
	 * 人数
	 */
	public String getSrs() {
		return srs;
	}

	/**
	 * 人数
	 */
	public void setSrs(String value) {
		this.srs = value;
	}
	/**
	 * 来访时间
	 */
	public java.util.Date getDlfsj() {
		return dlfsj;
	}

	/**
	 * 来访时间
	 */
	public void setDlfsj(java.util.Date value) {
		this.dlfsj = value;
	}
	/**
	 * 活动计划时限
	 */
	public String getShdjhsx() {
		return shdjhsx;
	}

	/**
	 * 活动计划时限
	 */
	public void setShdjhsx(String value) {
		this.shdjhsx = value;
	}
	/**
	 * 项目简介
	 */
	public String getSxmjj() {
		return sxmjj;
	}

	/**
	 * 项目简介
	 */
	public void setSxmjj(String value) {
		this.sxmjj = value;
	}
	/**
	 * 部门审批意见
	 */
	public String getSbmspyj() {
		return sbmspyj;
	}

	/**
	 * 部门审批意见
	 */
	public void setSbmspyj(String value) {
		this.sbmspyj = value;
	}
	/**
	 * 部门审批时间
	 */
	public java.util.Date getDbmspsj() {
		return dbmspsj;
	}

	/**
	 * 部门审批时间
	 */
	public void setDbmspsj(java.util.Date value) {
		this.dbmspsj = value;
	}
	/**
	 * 部门审批人
	 */
	public String getSbmspr() {
		return sbmspr;
	}

	/**
	 * 部门审批人
	 */
	public void setSbmspr(String value) {
		this.sbmspr = value;
	}
	/**
	 * 单位审批意见
	 */
	public String getSdwspyj() {
		return sdwspyj;
	}

	/**
	 * 单位审批意见
	 */
	public void setSdwspyj(String value) {
		this.sdwspyj = value;
	}
	/**
	 * 单位审批时间
	 */
	public java.util.Date getDdwspsj() {
		return ddwspsj;
	}

	/**
	 * 单位审批时间
	 */
	public void setDdwspsj(java.util.Date value) {
		this.ddwspsj = value;
	}
	/**
	 * 单位审批人
	 */
	public String getSdwspr() {
		return sdwspr;
	}

	/**
	 * 单位审批人
	 */
	public void setSdwspr(String value) {
		this.sdwspr = value;
	}

	@Override
	public String getProjectname() {
		return this.sxmmc;
	}

	@Override
	public String getSexeid() {
		return this.sexeid;
	}

	@Override
	public void setSexeid(String arg0) {
		this.sexeid=arg0;
		
	}
}