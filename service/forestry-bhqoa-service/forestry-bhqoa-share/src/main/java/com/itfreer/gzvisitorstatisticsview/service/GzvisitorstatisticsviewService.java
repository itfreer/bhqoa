package com.itfreer.gzvisitorstatisticsview.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzvisitorstatisticsview.entity.GzvisitorstatisticsviewEntity;

/**
 * 定义访客统计（视图）接口
 */
public interface GzvisitorstatisticsviewService extends BaseService<GzvisitorstatisticsviewEntity> {
	
}
