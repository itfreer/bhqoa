package com.itfreer.gzhlyjdkh.entity;

import java.io.Serializable;

/**
 * 定义护林员季度考核实体
 */
public class GzhlyjdkhEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 保护区名称
	 */
	private String bhqmc;
	
	
	/**
	 * 保护区ID
	 */
	private String bhqid;
	
	
	/**
	 * 管理站名称
	 */
	private String glzmc;
	
	
	/**
	 * 管理站ID
	 */
	private String glzid;
	
	
	/**
	 * 负责人名称
	 */
	private String fzrmc;
	
	
	/**
	 * 负责人ID
	 */
	private String fzrid;
	
	
	/**
	 * 考核年度
	 */
	private String khnd;
	
	
	/**
	 * 考核季度
	 */
	private String khjd;
	
	/**
	 * 制表日期
	 */
	private java.util.Date zbrq;
	
	/**
	 * 出勤天数_巡山
	 */
	private Double cqtsxs;
	
	/**
	 * 出勤天数_在岗
	 */
	private Double cqtszg;
	
	/**
	 * 出勤天数_事假
	 */
	private Double cqtssj;
	
	/**
	 * 出勤天数_病假
	 */
	private Double cqtsbj;
	
	/**
	 * 出勤天数_旷工
	 */
	private Double cqtskg;
	
	/**
	 * 目标量化考核评分结果
	 */
	private Double mblhkhpfjg;
	
	/**
	 * 目标考核得分90%
	 */
	private Double mblhkhpf90;
	
	/**
	 * 民主测评分
	 */
	private Double mzcpf;
	
	/**
	 * 明主测评分10%
	 */
	private Double mzcpf10;
	
	/**
	 * 优秀护林员加分
	 */
	private Double yxhlyjf;
	
	/**
	 * 季度综合考核得分
	 */
	private Double jdzhkhdf;
	
	
	/**
	 * 管理站工资兑现意见
	 */
	private String glzgzdxyj;
	
	
	/**
	 * 管理局工资兑现意见
	 */
	private String gljgzdxyj;
	
	
	/**
	 * 被考核人
	 */
	private String bkhrmc;
	
	
	/**
	 * 被考核人ID
	 */
	private String bkhrid;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 保护区名称
	 */
	public String getBhqmc() {
		return bhqmc;
	}

	/**
	 * 保护区名称
	 */
	public void setBhqmc(String value) {
		this.bhqmc = value;
	}
	/**
	 * 保护区ID
	 */
	public String getBhqid() {
		return bhqid;
	}

	/**
	 * 保护区ID
	 */
	public void setBhqid(String value) {
		this.bhqid = value;
	}
	/**
	 * 管理站名称
	 */
	public String getGlzmc() {
		return glzmc;
	}

	/**
	 * 管理站名称
	 */
	public void setGlzmc(String value) {
		this.glzmc = value;
	}
	/**
	 * 管理站ID
	 */
	public String getGlzid() {
		return glzid;
	}

	/**
	 * 管理站ID
	 */
	public void setGlzid(String value) {
		this.glzid = value;
	}
	/**
	 * 负责人名称
	 */
	public String getFzrmc() {
		return fzrmc;
	}

	/**
	 * 负责人名称
	 */
	public void setFzrmc(String value) {
		this.fzrmc = value;
	}
	/**
	 * 负责人ID
	 */
	public String getFzrid() {
		return fzrid;
	}

	/**
	 * 负责人ID
	 */
	public void setFzrid(String value) {
		this.fzrid = value;
	}
	/**
	 * 考核年度
	 */
	public String getKhnd() {
		return khnd;
	}

	/**
	 * 考核年度
	 */
	public void setKhnd(String value) {
		this.khnd = value;
	}
	/**
	 * 考核季度
	 */
	public String getKhjd() {
		return khjd;
	}

	/**
	 * 考核季度
	 */
	public void setKhjd(String value) {
		this.khjd = value;
	}
	/**
	 * 制表日期
	 */
	public java.util.Date getZbrq() {
		return zbrq;
	}

	/**
	 * 制表日期
	 */
	public void setZbrq(java.util.Date value) {
		this.zbrq = value;
	}
	/**
	 * 出勤天数_巡山
	 */
	public Double getCqtsxs() {
		return cqtsxs;
	}

	/**
	 * 出勤天数_巡山
	 */
	public void setCqtsxs(Double value) {
		this.cqtsxs = value;
	}
	/**
	 * 出勤天数_在岗
	 */
	public Double getCqtszg() {
		return cqtszg;
	}

	/**
	 * 出勤天数_在岗
	 */
	public void setCqtszg(Double value) {
		this.cqtszg = value;
	}
	/**
	 * 出勤天数_事假
	 */
	public Double getCqtssj() {
		return cqtssj;
	}

	/**
	 * 出勤天数_事假
	 */
	public void setCqtssj(Double value) {
		this.cqtssj = value;
	}
	/**
	 * 出勤天数_病假
	 */
	public Double getCqtsbj() {
		return cqtsbj;
	}

	/**
	 * 出勤天数_病假
	 */
	public void setCqtsbj(Double value) {
		this.cqtsbj = value;
	}
	/**
	 * 出勤天数_旷工
	 */
	public Double getCqtskg() {
		return cqtskg;
	}

	/**
	 * 出勤天数_旷工
	 */
	public void setCqtskg(Double value) {
		this.cqtskg = value;
	}
	/**
	 * 目标量化考核评分结果
	 */
	public Double getMblhkhpfjg() {
		return mblhkhpfjg;
	}

	/**
	 * 目标量化考核评分结果
	 */
	public void setMblhkhpfjg(Double value) {
		this.mblhkhpfjg = value;
	}
	/**
	 * 目标考核得分90%
	 */
	public Double getMblhkhpf90() {
		return mblhkhpf90;
	}

	/**
	 * 目标考核得分90%
	 */
	public void setMblhkhpf90(Double value) {
		this.mblhkhpf90 = value;
	}
	/**
	 * 民主测评分
	 */
	public Double getMzcpf() {
		return mzcpf;
	}

	/**
	 * 民主测评分
	 */
	public void setMzcpf(Double value) {
		this.mzcpf = value;
	}
	/**
	 * 明主测评分10%
	 */
	public Double getMzcpf10() {
		return mzcpf10;
	}

	/**
	 * 明主测评分10%
	 */
	public void setMzcpf10(Double value) {
		this.mzcpf10 = value;
	}
	/**
	 * 优秀护林员加分
	 */
	public Double getYxhlyjf() {
		return yxhlyjf;
	}

	/**
	 * 优秀护林员加分
	 */
	public void setYxhlyjf(Double value) {
		this.yxhlyjf = value;
	}
	/**
	 * 季度综合考核得分
	 */
	public Double getJdzhkhdf() {
		return jdzhkhdf;
	}

	/**
	 * 季度综合考核得分
	 */
	public void setJdzhkhdf(Double value) {
		this.jdzhkhdf = value;
	}
	/**
	 * 管理站工资兑现意见
	 */
	public String getGlzgzdxyj() {
		return glzgzdxyj;
	}

	/**
	 * 管理站工资兑现意见
	 */
	public void setGlzgzdxyj(String value) {
		this.glzgzdxyj = value;
	}
	/**
	 * 管理局工资兑现意见
	 */
	public String getGljgzdxyj() {
		return gljgzdxyj;
	}

	/**
	 * 管理局工资兑现意见
	 */
	public void setGljgzdxyj(String value) {
		this.gljgzdxyj = value;
	}
	/**
	 * 被考核人
	 */
	public String getBkhrmc() {
		return bkhrmc;
	}

	/**
	 * 被考核人
	 */
	public void setBkhrmc(String value) {
		this.bkhrmc = value;
	}
	/**
	 * 被考核人ID
	 */
	public String getBkhrid() {
		return bkhrid;
	}

	/**
	 * 被考核人ID
	 */
	public void setBkhrid(String value) {
		this.bkhrid = value;
	}
}