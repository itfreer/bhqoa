package com.itfreer.gzqdgl.entity;

import java.io.Serializable;

import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义签到管理实体
 */
public class GzqdglEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;
	
	String latitude;
	String longitude;
	String address;
	String accuracy;
	String dirtype;
	
	String outlatitude;
	String outlongitude;
	String outaddress;
	String outaccuracy;
	String outdirtype;
	
	String xwlatitude;
	String xwlongitude;
	String xwaddress;
	String xwaccuracy;
	String xwdirtype;
	
	String xwoutlatitude;
	String xwoutlongitude;
	String xwoutaddress;
	String xwoutaccuracy;
	String xwoutdirtype;
	
	public String getXwlatitude() {
		return xwlatitude;
	}

	public void setXwlatitude(String xwlatitude) {
		this.xwlatitude = xwlatitude;
	}

	public String getXwlongitude() {
		return xwlongitude;
	}

	public void setXwlongitude(String xwlongitude) {
		this.xwlongitude = xwlongitude;
	}

	public String getXwaddress() {
		return xwaddress;
	}

	public void setXwaddress(String xwaddress) {
		this.xwaddress = xwaddress;
	}

	public String getXwaccuracy() {
		return xwaccuracy;
	}

	public void setXwaccuracy(String xwaccuracy) {
		this.xwaccuracy = xwaccuracy;
	}

	public String getXwdirtype() {
		return xwdirtype;
	}

	public void setXwdirtype(String xwdirtype) {
		this.xwdirtype = xwdirtype;
	}

	public String getXwoutlatitude() {
		return xwoutlatitude;
	}

	public void setXwoutlatitude(String xwoutlatitude) {
		this.xwoutlatitude = xwoutlatitude;
	}

	public String getXwoutlongitude() {
		return xwoutlongitude;
	}

	public void setXwoutlongitude(String xwoutlongitude) {
		this.xwoutlongitude = xwoutlongitude;
	}

	public String getXwoutaddress() {
		return xwoutaddress;
	}

	public void setXwoutaddress(String xwoutaddress) {
		this.xwoutaddress = xwoutaddress;
	}

	public String getXwoutaccuracy() {
		return xwoutaccuracy;
	}

	public void setXwoutaccuracy(String xwoutaccuracy) {
		this.xwoutaccuracy = xwoutaccuracy;
	}

	public String getXwoutdirtype() {
		return xwoutdirtype;
	}

	public void setXwoutdirtype(String xwoutdirtype) {
		this.xwoutdirtype = xwoutdirtype;
	}

	public String getOutlatitude() {
		return outlatitude;
	}

	public void setOutlatitude(String outlatitude) {
		this.outlatitude = outlatitude;
	}

	public String getOutlongitude() {
		return outlongitude;
	}

	public void setOutlongitude(String outlongitude) {
		this.outlongitude = outlongitude;
	}

	public String getOutaddress() {
		return outaddress;
	}

	public void setOutaddress(String outaddress) {
		this.outaddress = outaddress;
	}

	public String getOutaccuracy() {
		return outaccuracy;
	}

	public void setOutaccuracy(String outaccuracy) {
		this.outaccuracy = outaccuracy;
	}

	public String getOutdirtype() {
		return outdirtype;
	}

	public void setOutdirtype(String outdirtype) {
		this.outdirtype = outdirtype;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(String accuracy) {
		this.accuracy = accuracy;
	}

	public String getDirtype() {
		return dirtype;
	}

	public void setDirtype(String dirtype) {
		this.dirtype = dirtype;
	}
	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 所属单位
	 */
	
	private String danweimc;
	
	private String bz;
	
	private String dkbz;
	
	public String getDkbz() {
		return dkbz;
	}

	public void setDkbz(String dkbz) {
		this.dkbz = dkbz;
	}
	private String txbz;
	
	public String getTxbz() {
		return txbz;
	}

	public void setTxbz(String txbz) {
		this.txbz = txbz;
	}
	private String sfkqfw;
	
	private String sfsjyc;
	
	public String getSfsjyc() {
		return sfsjyc;
	}

	public void setSfsjyc(String sfsjyc) {
		this.sfsjyc = sfsjyc;
	}

	public String getSfkqfw() {
		return sfkqfw;
	}

	public void setSfkqfw(String sfkqfw) {
		this.sfkqfw = sfkqfw;
	}
	
	public String getBz() {
		return bz;
	}

	public void setBz(String bz) {
		this.bz = bz;
	}
	
	
	/**
	 * 单位ID
	 */
	/**
	 * 部门名称
	 */
	@DictionaryField(dictionaryName = "p_department" ,toFieldName = "danweimc")
	private String danweiid;
	
	
	/**
	 * 考勤人ID
	 */
	private String kaoqrid;
	
	
	/**
	 * 考勤人名称
	 */
	private String kaoqrmc;
	
	/**
	 * 年度
	 */
	private Integer nandu;
	
	/**
	 * 月份
	 */
	private Integer yuefen;
	
	/**
	 * 天
	 */
	private Integer day;
	
	/**
	 * 构建时间
	 */
	private java.util.Date createtime;
	
	
	
	
	
	public java.util.Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(java.util.Date createtime) {
		this.createtime = createtime;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	/**
	 * 日期
	 */
	private java.util.Date riqi;
	
	
	/**
	 * 星期
	 */
	private String xingqi;
	
	
	/**
	 * 工作日状态（工作日，节假日）
	 */
	private String gzrzt;
	
	/**
	 * 有效签入时间
	 */
	private java.util.Date yxqrsj;
	
	/**
	 * 有效迁出时间
	 */
	private java.util.Date yxqcsj;
	
	/**
	 * 上午签到时间
	 */
	private java.util.Date qdsj;
	
	/**
	 * 上午签退时间
	 */
	private java.util.Date qtsj;
	
	/**
	 * 下午签到时间
	 */
	private java.util.Date xwqdsj;
	
	/**
	 * 下午签退时间
	 */
	private java.util.Date xwqtsj;
	
	public java.util.Date getXwqdsj() {
		return xwqdsj;
	}

	public void setXwqdsj(java.util.Date xwqdsj) {
		this.xwqdsj = xwqdsj;
	}

	public java.util.Date getXwqtsj() {
		return xwqtsj;
	}

	public void setXwqtsj(java.util.Date xwqtsj) {
		this.xwqtsj = xwqtsj;
	}
	/**
	 * 工作时间
	 */
	private Double gzsj;
	
	/**
	 * 迟到
	 */
	private Double chidao;
	
	/**
	 * 早退
	 */
	private Double zaotui;
	
	/**
	 * 早来
	 */
	private Double zaolai;
	
	/**
	 * 晚走
	 */
	private Double wanzou;
	
	/**
	 * 未打卡次数
	 */
	private Double wdkcs;
	
	/**
	 * 请假
	 */
	private Double qingjia;
	
	/**
	 * 外出
	 */
	private Double waichu;
	
	/**
	 * 加班
	 */
	private Double jiaban;
	
	/**
	 * 旷工
	 */
	private Double kuanggong;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 所属单位
	 */
	public String getDanweimc() {
		return danweimc;
	}

	/**
	 * 所属单位
	 */
	public void setDanweimc(String value) {
		this.danweimc = value;
	}
	/**
	 * 单位ID
	 */
	public String getDanweiid() {
		return danweiid;
	}

	/**
	 * 单位ID
	 */
	public void setDanweiid(String value) {
		this.danweiid = value;
	}
	/**
	 * 考勤人ID
	 */
	public String getKaoqrid() {
		return kaoqrid;
	}

	/**
	 * 考勤人ID
	 */
	public void setKaoqrid(String value) {
		this.kaoqrid = value;
	}
	/**
	 * 考勤人名称
	 */
	public String getKaoqrmc() {
		return kaoqrmc;
	}

	/**
	 * 考勤人名称
	 */
	public void setKaoqrmc(String value) {
		this.kaoqrmc = value;
	}
	/**
	 * 年度
	 */
	public Integer getNandu() {
		return nandu;
	}

	/**
	 * 年度
	 */
	public void setNandu(Integer value) {
		this.nandu = value;
	}
	/**
	 * 月份
	 */
	public Integer getYuefen() {
		return yuefen;
	}

	/**
	 * 月份
	 */
	public void setYuefen(Integer value) {
		this.yuefen = value;
	}
	/**
	 * 日期
	 */
	public java.util.Date getRiqi() {
		return riqi;
	}

	/**
	 * 日期
	 */
	public void setRiqi(java.util.Date value) {
		this.riqi = value;
	}
	/**
	 * 星期
	 */
	public String getXingqi() {
		return xingqi;
	}

	/**
	 * 星期
	 */
	public void setXingqi(String value) {
		this.xingqi = value;
	}
	/**
	 * 工作日状态（工作日，节假日）
	 */
	public String getGzrzt() {
		return gzrzt;
	}

	/**
	 * 工作日状态（工作日，节假日）
	 */
	public void setGzrzt(String value) {
		this.gzrzt = value;
	}
	/**
	 * 有效签入时间
	 */
	public java.util.Date getYxqrsj() {
		return yxqrsj;
	}

	/**
	 * 有效签入时间
	 */
	public void setYxqrsj(java.util.Date value) {
		this.yxqrsj = value;
	}
	/**
	 * 有效迁出时间
	 */
	public java.util.Date getYxqcsj() {
		return yxqcsj;
	}

	/**
	 * 有效迁出时间
	 */
	public void setYxqcsj(java.util.Date value) {
		this.yxqcsj = value;
	}
	/**
	 * 签到时间
	 */
	public java.util.Date getQdsj() {
		return qdsj;
	}

	/**
	 * 签到时间
	 */
	public void setQdsj(java.util.Date value) {
		this.qdsj = value;
	}
	/**
	 * 签退时间
	 */
	public java.util.Date getQtsj() {
		return qtsj;
	}

	/**
	 * 签退时间
	 */
	public void setQtsj(java.util.Date value) {
		this.qtsj = value;
	}
	/**
	 * 工作时间
	 */
	public Double getGzsj() {
		return gzsj;
	}

	/**
	 * 工作时间
	 */
	public void setGzsj(Double value) {
		this.gzsj = value;
	}
	/**
	 * 迟到
	 */
	public Double getChidao() {
		return chidao;
	}

	/**
	 * 迟到
	 */
	public void setChidao(Double value) {
		this.chidao = value;
	}
	/**
	 * 早退
	 */
	public Double getZaotui() {
		return zaotui;
	}

	/**
	 * 早退
	 */
	public void setZaotui(Double value) {
		this.zaotui = value;
	}
	/**
	 * 早来
	 */
	public Double getZaolai() {
		return zaolai;
	}

	/**
	 * 早来
	 */
	public void setZaolai(Double value) {
		this.zaolai = value;
	}
	/**
	 * 晚走
	 */
	public Double getWanzou() {
		return wanzou;
	}

	/**
	 * 晚走
	 */
	public void setWanzou(Double value) {
		this.wanzou = value;
	}
	/**
	 * 未打卡次数
	 */
	public Double getWdkcs() {
		return wdkcs;
	}

	/**
	 * 未打卡次数
	 */
	public void setWdkcs(Double value) {
		this.wdkcs = value;
	}
	/**
	 * 请假
	 */
	public Double getQingjia() {
		return qingjia;
	}

	/**
	 * 请假
	 */
	public void setQingjia(Double value) {
		this.qingjia = value;
	}
	/**
	 * 外出
	 */
	public Double getWaichu() {
		return waichu;
	}

	/**
	 * 外出
	 */
	public void setWaichu(Double value) {
		this.waichu = value;
	}
	/**
	 * 加班
	 */
	public Double getJiaban() {
		return jiaban;
	}

	/**
	 * 加班
	 */
	public void setJiaban(Double value) {
		this.jiaban = value;
	}
	/**
	 * 旷工
	 */
	public Double getKuanggong() {
		return kuanggong;
	}

	/**
	 * 旷工
	 */
	public void setKuanggong(Double value) {
		this.kuanggong = value;
	}
}