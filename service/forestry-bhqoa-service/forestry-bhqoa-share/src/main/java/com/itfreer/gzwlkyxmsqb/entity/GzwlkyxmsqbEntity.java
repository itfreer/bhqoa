package com.itfreer.gzwlkyxmsqb.entity;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;

import com.itfreer.bpm.flow.api.IBpmProjectEntity;
import com.itfreer.form.dictionary.reflect.DictionaryField;
import com.itfreer.gzplanpersonnel.entity.GzplanpersonnelEntity;
import com.itfreer.gzwlkyxmcg.entity.GzwlkyxmcgEntity;
import com.itfreer.gzwlkyxmlfry.entity.GzwlkyxmlfryEntity;

/**
 * 定义外来科研项目申请表实体
 */
public class GzwlkyxmsqbEntity implements IBpmProjectEntity {

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 申请编号
	 */
	private String sqbh;
	
	
	/**
	 * 单位ID
	 */
	private String dwid;
	
	
	/**
	 * 单位名称
	 */
	private String dwmc;
	
	
	/**
	 * 项目名称
	 */
	private String xmmc;
	
	
	/**
	 * 项目ID
	 */
	private String xmid;
	
	
	/**
	 * 是否有介绍信（函）
	 */
	@DictionaryField(dictionaryName="p_yesorno", toFieldName="sfyjsxName")
	private String sfyjsx;
	
	
	/**
	 * 是否有介绍信（函）
	 */
	private String sfyjsxName;
	
	public String getSfyjsxName() {
		return sfyjsxName;
	}

	public void setSfyjsxName(String sfyjsxName) {
		this.sfyjsxName = sfyjsxName;
	}
	
	
	/**
	 * 提供活动方案情况
	 */
	private String tjhdfaqk;
	
	
	/**
	 * 是否鉴定合作协议
	 */
	@DictionaryField(dictionaryName="p_yesorno", toFieldName="sjqdhzxyName")
	private String sjqdhzxy;
	
	
	/**
	 * 是否鉴定合作协议
	 */
	private String sjqdhzxyName;
	
	
	public String getSjqdhzxyName() {
		return sjqdhzxyName;
	}

	public void setSjqdhzxyName(String sjqdhzxyName) {
		this.sjqdhzxyName = sjqdhzxyName;
	}


	/**
	 * 协议编号
	 */
	private String xybh;
	
	
	/**
	 * 带队负责人ID
	 */
	private String ddfzrid;
	
	
	/**
	 * 带队负责人名称
	 */
	private String ddfzrmc;
	
	
	/**
	 * 带队负责人职务ID
	 */
	private String ddfzrzwid;
	
	
	/**
	 * 带队负责人职务名称
	 */
	private String ddfzrzwmc;
	
	
	/**
	 * 带队负责人联系电话
	 */
	private String ddfzrlxdh;
	
	/**
	 * 人数
	 */
	private Double renshu;
	
	/**
	 * 来访时间开始时间
	 */
	private java.util.Date lfsj;
	
	/**
	 * 来访结束时间
	 */
	private java.util.Date lfsjjssj;
	
	
	/**
	 * 活动计划时限
	 */
	private String hdjhsx;
	
	
	/**
	 * 项目简介
	 */
	private String xmjj;
	
	
	/**
	 * 管理站审批意见
	 */
	private String glzspyj;
	
	
	/**
	 * 管理站审批人
	 */
	private String glzspr;
	
	/**
	 * 管理站审批时间
	 */
	private java.util.Date glzspsj;
	
	
	/**
	 * 管理局审批意见
	 */
	private String gljspyj;
	
	
	/**
	 * 管理局审批时间
	 */
	private String gljspsj;
	
	/**
	 * 管理局审批人
	 */
	private java.util.Date gljspr;
	
	
	/**
	 * 成果材料
	 */
	private String cgcl;
	
	
	/**
	 * 活动范围
	 */
	private String hdfw;
	
	
	/**
	 * 注意事项
	 */
	private String zysx;
	
	
	/**
	 * 编号
	 */
	private String bh;
	
	/**
	 * 负责部门
	 */
	@DictionaryField(dictionaryName = "p_department" ,toFieldName = "fzbmName")
	private String fzbm;
	
	/**
	 * 负责部门
	 */
	private String fzbmName;
	
	/**
	 * 负责人
	 */
	private String fzr;
	
	/**
	 * 负责人ID
	 */
	private String fzrid;
	
	public String getFzbm() {
		return fzbm;
	}

	public void setFzbm(String fzbm) {
		this.fzbm = fzbm;
	}

	public String getFzbmName() {
		return fzbmName;
	}

	public void setFzbmName(String fzbmName) {
		this.fzbmName = fzbmName;
	}

	public String getFzr() {
		return fzr;
	}

	public void setFzr(String fzr) {
		this.fzr = fzr;
	}

	public String getFzrid() {
		return fzrid;
	}

	public void setFzrid(String fzrid) {
		this.fzrid = fzrid;
	}
	
	
	public String getHdfw() {
		return hdfw;
	}

	public void setHdfw(String hdfw) {
		this.hdfw = hdfw;
	}

	public String getZysx() {
		return zysx;
	}

	public void setZysx(String zysx) {
		this.zysx = zysx;
	}

	public String getBh() {
		return bh;
	}

	public void setBh(String bh) {
		this.bh = bh;
	}
	
	public String getCgcl() {
		return cgcl;
	}

	public void setCgcl(String cgcl) {
		this.cgcl = cgcl;
	}
	

	/**
	 * 项目人员
	 */
	private Set<GzwlkyxmlfryEntity> gzwlkyxmlfry=new LinkedHashSet<GzwlkyxmlfryEntity>();
	
	/**
	 * 项目人员
	 * @return
	 */
	public Set<GzwlkyxmlfryEntity> getGzwlkyxmlfry() {
		return gzwlkyxmlfry;
	}

	/**
	 * 项目人员
	 * @return
	 */
	public void setGzwlkyxmlfry(Set<GzwlkyxmlfryEntity> gzwlkyxmlfry) {
		this.gzwlkyxmlfry = gzwlkyxmlfry;
	}
	
	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 申请编号
	 */
	public String getSqbh() {
		return sqbh;
	}

	/**
	 * 申请编号
	 */
	public void setSqbh(String value) {
		this.sqbh = value;
	}
	/**
	 * 单位ID
	 */
	public String getDwid() {
		return dwid;
	}

	/**
	 * 单位ID
	 */
	public void setDwid(String value) {
		this.dwid = value;
	}
	/**
	 * 单位名称
	 */
	public String getDwmc() {
		return dwmc;
	}

	/**
	 * 单位名称
	 */
	public void setDwmc(String value) {
		this.dwmc = value;
	}
	/**
	 * 项目名称
	 */
	public String getXmmc() {
		return xmmc;
	}

	/**
	 * 项目名称
	 */
	public void setXmmc(String value) {
		this.xmmc = value;
	}
	/**
	 * 项目ID
	 */
	public String getXmid() {
		return xmid;
	}

	/**
	 * 项目ID
	 */
	public void setXmid(String value) {
		this.xmid = value;
	}
	/**
	 * 是否有介绍信（函）
	 */
	public String getSfyjsx() {
		return sfyjsx;
	}

	/**
	 * 是否有介绍信（函）
	 */
	public void setSfyjsx(String value) {
		this.sfyjsx = value;
	}
	/**
	 * 提供活动方案情况
	 */
	public String getTjhdfaqk() {
		return tjhdfaqk;
	}

	/**
	 * 提供活动方案情况
	 */
	public void setTjhdfaqk(String value) {
		this.tjhdfaqk = value;
	}
	/**
	 * 是否鉴定合作协议
	 */
	public String getSjqdhzxy() {
		return sjqdhzxy;
	}

	/**
	 * 是否鉴定合作协议
	 */
	public void setSjqdhzxy(String value) {
		this.sjqdhzxy = value;
	}
	/**
	 * 协议编号
	 */
	public String getXybh() {
		return xybh;
	}

	/**
	 * 协议编号
	 */
	public void setXybh(String value) {
		this.xybh = value;
	}
	/**
	 * 带队负责人ID
	 */
	public String getDdfzrid() {
		return ddfzrid;
	}

	/**
	 * 带队负责人ID
	 */
	public void setDdfzrid(String value) {
		this.ddfzrid = value;
	}
	/**
	 * 带队负责人名称
	 */
	public String getDdfzrmc() {
		return ddfzrmc;
	}

	/**
	 * 带队负责人名称
	 */
	public void setDdfzrmc(String value) {
		this.ddfzrmc = value;
	}
	/**
	 * 带队负责人职务ID
	 */
	public String getDdfzrzwid() {
		return ddfzrzwid;
	}

	/**
	 * 带队负责人职务ID
	 */
	public void setDdfzrzwid(String value) {
		this.ddfzrzwid = value;
	}
	/**
	 * 带队负责人职务名称
	 */
	public String getDdfzrzwmc() {
		return ddfzrzwmc;
	}

	/**
	 * 带队负责人职务名称
	 */
	public void setDdfzrzwmc(String value) {
		this.ddfzrzwmc = value;
	}
	/**
	 * 带队负责人联系电话
	 */
	public String getDdfzrlxdh() {
		return ddfzrlxdh;
	}

	/**
	 * 带队负责人联系电话
	 */
	public void setDdfzrlxdh(String value) {
		this.ddfzrlxdh = value;
	}
	/**
	 * 人数
	 */
	public Double getRenshu() {
		return renshu;
	}

	/**
	 * 人数
	 */
	public void setRenshu(Double value) {
		this.renshu = value;
	}
	/**
	 * 来访时间
	 */
	public java.util.Date getLfsj() {
		return lfsj;
	}

	/**
	 * 来访时间
	 */
	public void setLfsj(java.util.Date value) {
		this.lfsj = value;
	}
	/**
	 * 活动计划时限
	 */
	public String getHdjhsx() {
		return hdjhsx;
	}

	/**
	 * 活动计划时限
	 */
	public void setHdjhsx(String value) {
		this.hdjhsx = value;
	}
	/**
	 * 项目简介
	 */
	public String getXmjj() {
		return xmjj;
	}

	/**
	 * 项目简介
	 */
	public void setXmjj(String value) {
		this.xmjj = value;
	}
	/**
	 * 管理站审批意见
	 */
	public String getGlzspyj() {
		return glzspyj;
	}

	/**
	 * 管理站审批意见
	 */
	public void setGlzspyj(String value) {
		this.glzspyj = value;
	}
	/**
	 * 管理站审批人
	 */
	public String getGlzspr() {
		return glzspr;
	}

	/**
	 * 管理站审批人
	 */
	public void setGlzspr(String value) {
		this.glzspr = value;
	}
	/**
	 * 管理站审批时间
	 */
	public java.util.Date getGlzspsj() {
		return glzspsj;
	}

	/**
	 * 管理站审批时间
	 */
	public void setGlzspsj(java.util.Date value) {
		this.glzspsj = value;
	}
	/**
	 * 管理局审批意见
	 */
	public String getGljspyj() {
		return gljspyj;
	}

	/**
	 * 管理局审批意见
	 */
	public void setGljspyj(String value) {
		this.gljspyj = value;
	}
	/**
	 * 管理局审批时间
	 */
	public String getGljspsj() {
		return gljspsj;
	}

	/**
	 * 管理局审批时间
	 */
	public void setGljspsj(String value) {
		this.gljspsj = value;
	}
	/**
	 * 管理局审批人
	 */
	public java.util.Date getGljspr() {
		return gljspr;
	}

	/**
	 * 管理局审批人
	 */
	public void setGljspr(java.util.Date value) {
		this.gljspr = value;
	}
	
	/**
	 * 项目成果
	 */
	private Set<GzwlkyxmcgEntity> wlxmcg=new LinkedHashSet<GzwlkyxmcgEntity>();
	
	public Set<GzwlkyxmcgEntity> getWlxmcg() {
		return wlxmcg;
	}

	public void setWlxmcg(Set<GzwlkyxmcgEntity> wlxmcg) {
		this.wlxmcg = wlxmcg;
	}
	
	private String sexeid;

	@Override
	public String getProjectname() {
		return this.xmmc;
	}

	@Override
	public String getSexeid() {
		return this.sexeid;
	}

	@Override
	public void setSexeid(String arg0) {
		this.sexeid=arg0;
	}

	public java.util.Date getLfsjjssj() {
		return lfsjjssj;
	}

	public void setLfsjjssj(java.util.Date lfsjjssj) {
		this.lfsjjssj = lfsjjssj;
	}
	
	
}