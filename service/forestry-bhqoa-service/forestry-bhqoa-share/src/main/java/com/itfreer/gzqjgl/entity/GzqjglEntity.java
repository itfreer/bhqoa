package com.itfreer.gzqjgl.entity;

import com.itfreer.bpm.flow.api.IBpmProjectEntity;
import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义请假管理实体
 */
public class GzqjglEntity implements IBpmProjectEntity {

	/**
	 * 流程ID
	 */
	private String bpmkey;

	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 审批状态
	 */
	@DictionaryField(dictionaryName = "spzt_zd", toFieldName = "spztName")
	public String spzt;
	
	/**
	 * 审批状态
	 */
	public String spztName;
	
	public String getSpzt() {
		return spzt;
	}

	public void setSpzt(String spzt) {
		this.spzt = spzt;
	}

	public String getSpztName() {
		return spztName;
	}

	public void setSpztName(String spztName) {
		this.spztName = spztName;
	}
	/**
	 * ID
	 */
	private String id;

	/**
	 * 所在单位
	 */
	private String szdw;

	/**
	 * 单位ID
	 */
	@DictionaryField(dictionaryName = "p_department", toFieldName = "szdw")
	private String dwid;

	/**
	 * 请假人姓名
	 */
	private String qjrxm;

	/**
	 * 请假人ID
	 */
	private String qjrid;

	/**
	 * 开始日期
	 */
	private java.util.Date ksrq;

	/**
	 * 开始时间
	 */
	private java.util.Date kssj;
	
	/**
	 * 开始时间节点
	 */
	@DictionaryField(dictionaryName = "gz_qjkssd", toFieldName = "sjsjjdname")
	private String sjsjjd;
	
	private String sjsjjdname;

	/**
	 * 结束时间
	 */
	private java.util.Date jssj;
	
	/**
	 * 结束时间节点
	 */
	@DictionaryField(dictionaryName = "gz_qjjssd", toFieldName = "jssjjdname")
	private String jssjjd;
	
	private String jssjjdname;

	/**
	 * 结束日期
	 */
	private java.util.Date jsrq;

	/**
	 * 假期类型
	 */
	private String jqlx;

	/**
	 * 请假天数
	 */
	private Double qjts;

	/**
	 * 考勤年份
	 */
	private Integer kqnf;

	/**
	 * 休假额度
	 */
	private Double xjed;

	/**
	 * 申请事由
	 */
	private String sqsy;

	/**
	 * 申请日期
	 */
	private java.util.Date sqrq;

	/**
	 * 申请人ID
	 */
	private String sqrid;

	/**
	 * 申请人名称
	 */
	private String sqrmc;

	/**
	 * 核对人ID
	 */
	private String hdrid;

	/**
	 * 核对人名称
	 */
	private String hdrmc;

	/**
	 * 核对结果
	 */
	private String hdjg;

	/**
	 * 核对时间
	 */
	private java.util.Date hdsj;

	/**
	 * 审核人
	 */
	private String shr;

	/**
	 * 审核意见
	 */
	private String shyj;

	/**
	 * 审核结果
	 */
	private String shjg;

	/**
	 * 审核人ID
	 */
	private String shrid;

	/**
	 * 审核时间
	 */
	private java.util.Date shsj;

	/**
	 * 撤销时间
	 */
	private java.util.Date cxsj;

	/**
	 * 撤销人
	 */
	private String cxrmc;

	/**
	 * 撤销原因
	 */
	private String cxyy;

	/**
	 * 撤销人ID
	 */
	private String cxrid;

	/**
	 * 局领导审批
	 */
	private String sjldsp;

	/**
	 * 分管领导审批
	 */
	private String sfgldsp;

	/**
	 * 科室审批
	 */
	private String skssp;

	/**
	 * 部门领导审批
	 */
	private String sbmldsp;

	@DictionaryField(dictionaryName = "GZ_QJLX", toFieldName = "qjlxname")
	private String qjlx;

	private String qjlxname;

	public String getQjlxname() {
		return qjlxname;
	}

	public void setQjlxname(String qjlxname) {
		this.qjlxname = qjlxname;
	}

	public String getQjlx() {
		return qjlx;
	}

	public void setQjlx(String qjlx) {
		this.qjlx = qjlx;
	}

	public String getSjldsp() {
		return sjldsp;
	}

	public void setSjldsp(String sjldsp) {
		this.sjldsp = sjldsp;
	}

	public String getSfgldsp() {
		return sfgldsp;
	}

	public void setSfgldsp(String sfgldsp) {
		this.sfgldsp = sfgldsp;
	}

	public String getSkssp() {
		return skssp;
	}

	public void setSkssp(String skssp) {
		this.skssp = skssp;
	}

	public String getSbmldsp() {
		return sbmldsp;
	}

	public void setSbmldsp(String sbmldsp) {
		this.sbmldsp = sbmldsp;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}

	/**
	 * 所在单位
	 */
	public String getSzdw() {
		return szdw;
	}

	/**
	 * 所在单位
	 */
	public void setSzdw(String value) {
		this.szdw = value;
	}

	/**
	 * 单位ID
	 */
	public String getDwid() {
		return dwid;
	}

	/**
	 * 单位ID
	 */
	public void setDwid(String value) {
		this.dwid = value;
	}

	/**
	 * 请假人姓名
	 */
	public String getQjrxm() {
		return qjrxm;
	}

	/**
	 * 请假人姓名
	 */
	public void setQjrxm(String value) {
		this.qjrxm = value;
	}

	/**
	 * 请假人ID
	 */
	public String getQjrid() {
		return qjrid;
	}

	/**
	 * 请假人ID
	 */
	public void setQjrid(String value) {
		this.qjrid = value;
	}

	/**
	 * 开始日期
	 */
	public java.util.Date getKsrq() {
		return ksrq;
	}

	/**
	 * 开始日期
	 */
	public void setKsrq(java.util.Date value) {
		this.ksrq = value;
	}

	/**
	 * 开始时间
	 */
	public java.util.Date getKssj() {
		return kssj;
	}

	/**
	 * 开始时间
	 */
	public void setKssj(java.util.Date value) {
		this.kssj = value;
	}

	/**
	 * 结束时间
	 */
	public java.util.Date getJssj() {
		return jssj;
	}

	/**
	 * 结束时间
	 */
	public void setJssj(java.util.Date value) {
		this.jssj = value;
	}

	/**
	 * 结束日期
	 */
	public java.util.Date getJsrq() {
		return jsrq;
	}

	/**
	 * 结束日期
	 */
	public void setJsrq(java.util.Date value) {
		this.jsrq = value;
	}

	/**
	 * 假期类型
	 */
	public String getJqlx() {
		return jqlx;
	}

	/**
	 * 假期类型
	 */
	public void setJqlx(String value) {
		this.jqlx = value;
	}

	/**
	 * 请假天数
	 */
	public Double getQjts() {
		return qjts;
	}

	/**
	 * 请假天数
	 */
	public void setQjts(Double value) {
		this.qjts = value;
	}

	/**
	 * 考勤年份
	 */
	public Integer getKqnf() {
		return kqnf;
	}

	/**
	 * 考勤年份
	 */
	public void setKqnf(Integer value) {
		this.kqnf = value;
	}

	/**
	 * 休假额度
	 */
	public Double getXjed() {
		return xjed;
	}

	/**
	 * 休假额度
	 */
	public void setXjed(Double value) {
		this.xjed = value;
	}

	/**
	 * 申请事由
	 */
	public String getSqsy() {
		return sqsy;
	}

	/**
	 * 申请事由
	 */
	public void setSqsy(String value) {
		this.sqsy = value;
	}

	/**
	 * 申请日期
	 */
	public java.util.Date getSqrq() {
		return sqrq;
	}

	/**
	 * 申请日期
	 */
	public void setSqrq(java.util.Date value) {
		this.sqrq = value;
	}

	/**
	 * 申请人ID
	 */
	public String getSqrid() {
		return sqrid;
	}

	/**
	 * 申请人ID
	 */
	public void setSqrid(String value) {
		this.sqrid = value;
	}

	/**
	 * 申请人名称
	 */
	public String getSqrmc() {
		return sqrmc;
	}

	/**
	 * 申请人名称
	 */
	public void setSqrmc(String value) {
		this.sqrmc = value;
	}

	/**
	 * 核对人ID
	 */
	public String getHdrid() {
		return hdrid;
	}

	/**
	 * 核对人ID
	 */
	public void setHdrid(String value) {
		this.hdrid = value;
	}

	/**
	 * 核对人名称
	 */
	public String getHdrmc() {
		return hdrmc;
	}

	/**
	 * 核对人名称
	 */
	public void setHdrmc(String value) {
		this.hdrmc = value;
	}

	/**
	 * 核对结果
	 */
	public String getHdjg() {
		return hdjg;
	}

	/**
	 * 核对结果
	 */
	public void setHdjg(String value) {
		this.hdjg = value;
	}

	/**
	 * 核对时间
	 */
	public java.util.Date getHdsj() {
		return hdsj;
	}

	/**
	 * 核对时间
	 */
	public void setHdsj(java.util.Date value) {
		this.hdsj = value;
	}

	/**
	 * 审核人
	 */
	public String getShr() {
		return shr;
	}

	/**
	 * 审核人
	 */
	public void setShr(String value) {
		this.shr = value;
	}

	/**
	 * 审核意见
	 */
	public String getShyj() {
		return shyj;
	}

	/**
	 * 审核意见
	 */
	public void setShyj(String value) {
		this.shyj = value;
	}

	/**
	 * 审核结果
	 */
	public String getShjg() {
		return shjg;
	}

	/**
	 * 审核结果
	 */
	public void setShjg(String value) {
		this.shjg = value;
	}

	/**
	 * 审核人ID
	 */
	public String getShrid() {
		return shrid;
	}

	/**
	 * 审核人ID
	 */
	public void setShrid(String value) {
		this.shrid = value;
	}

	/**
	 * 审核时间
	 */
	public java.util.Date getShsj() {
		return shsj;
	}

	/**
	 * 审核时间
	 */
	public void setShsj(java.util.Date value) {
		this.shsj = value;
	}

	/**
	 * 撤销时间
	 */
	public java.util.Date getCxsj() {
		return cxsj;
	}

	/**
	 * 撤销时间
	 */
	public void setCxsj(java.util.Date value) {
		this.cxsj = value;
	}

	/**
	 * 撤销人
	 */
	public String getCxrmc() {
		return cxrmc;
	}

	/**
	 * 撤销人
	 */
	public void setCxrmc(String value) {
		this.cxrmc = value;
	}

	/**
	 * 撤销原因
	 */
	public String getCxyy() {
		return cxyy;
	}

	/**
	 * 撤销原因
	 */
	public void setCxyy(String value) {
		this.cxyy = value;
	}

	/**
	 * 撤销人ID
	 */
	public String getCxrid() {
		return cxrid;
	}

	/**
	 * 撤销人ID
	 */
	public void setCxrid(String value) {
		this.cxrid = value;
	}

	private String sexeid;

	@Override
	public String getProjectname() {
		return this.qjrxm;
	}

	@Override
	public String getSexeid() {
		return this.sexeid;
	}

	@Override
	public void setSexeid(String arg0) {
		this.sexeid = arg0;
	}

	public String getSjsjjd() {
		return sjsjjd;
	}

	public void setSjsjjd(String sjsjjd) {
		this.sjsjjd = sjsjjd;
	}

	public String getSjsjjdname() {
		return sjsjjdname;
	}

	public void setSjsjjdname(String sjsjjdname) {
		this.sjsjjdname = sjsjjdname;
	}

	public String getJssjjd() {
		return jssjjd;
	}

	public void setJssjjd(String jssjjd) {
		this.jssjjd = jssjjd;
	}

	public String getJssjjdname() {
		return jssjjdname;
	}

	public void setJssjjdname(String jssjjdname) {
		this.jssjjdname = jssjjdname;
	}
}