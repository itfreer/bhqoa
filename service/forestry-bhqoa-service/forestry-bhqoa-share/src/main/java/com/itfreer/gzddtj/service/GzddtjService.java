package com.itfreer.gzddtj.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzddtj.entity.GzddtjEntity;

/**
 * 定义调度统计接口
 */
public interface GzddtjService extends BaseService<GzddtjEntity> {
	
}
