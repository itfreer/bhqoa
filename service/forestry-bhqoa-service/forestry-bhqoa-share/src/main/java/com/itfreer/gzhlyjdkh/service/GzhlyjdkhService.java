package com.itfreer.gzhlyjdkh.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzhlyjdkh.entity.GzhlyjdkhEntity;

/**
 * 定义护林员季度考核接口
 */
public interface GzhlyjdkhService extends BaseService<GzhlyjdkhEntity> {
	
}
