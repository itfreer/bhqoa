package com.itfreer.gzwlkyxmsqb.service;

import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.form.api.BaseService;

import com.itfreer.gzwlkyxmsqb.entity.GzwlkyxmsqbEntity;

/**
 * 定义外来科研项目申请表接口
 */
public interface GzwlkyxmsqbService extends IBaseWorkFlowService<GzwlkyxmsqbEntity> {
	
}
