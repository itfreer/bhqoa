package com.itfreer.gzzysq.entity;

import com.itfreer.bpm.flow.api.IBpmProjectEntity;

/**
 * 定义资源申请实体
 */
public class GzzysqEntity implements IBpmProjectEntity {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	private String id;
	
	
	/**
	 * 资源类型
	 */
	private String szylx;
	
	
	/**
	 * 使用人
	 */
	private String ssyr;
	
	
	/**
	 * 申请原因
	 */
	private String ssqyy;
	
	
	/**
	 * 联系方式
	 */
	private String slxfs;
	
	/**
	 * 使用时间
	 */
	private java.util.Date dsysj;
	
	
	/**
	 * 所属项目
	 */
	private String sssxm;
	

	/**
	 * 申请时间
	 */
	private java.util.Date dsqsj;
	
	/**
	 * 使用开始时间
	 */
	private java.util.Date dsykssj;
	
	/**
	 * 使用结束时间
	 */
	private java.util.Date dsyjssj;

	public java.util.Date getDsqsj() {
		return dsqsj;
	}

	public void setDsqsj(java.util.Date dsqsj) {
		this.dsqsj = dsqsj;
	}

	public java.util.Date getDsykssj() {
		return dsykssj;
	}

	public void setDsykssj(java.util.Date dsykssj) {
		this.dsykssj = dsykssj;
	}

	public java.util.Date getDsyjssj() {
		return dsyjssj;
	}

	public void setDsyjssj(java.util.Date dsyjssj) {
		this.dsyjssj = dsyjssj;
	}
	
	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 资源类型
	 */
	public String getSzylx() {
		return szylx;
	}

	/**
	 * 资源类型
	 */
	public void setSzylx(String value) {
		this.szylx = value;
	}
	/**
	 * 使用人
	 */
	public String getSsyr() {
		return ssyr;
	}

	/**
	 * 使用人
	 */
	public void setSsyr(String value) {
		this.ssyr = value;
	}
	/**
	 * 申请原因
	 */
	public String getSsqyy() {
		return ssqyy;
	}

	/**
	 * 申请原因
	 */
	public void setSsqyy(String value) {
		this.ssqyy = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxfs() {
		return slxfs;
	}

	/**
	 * 联系方式
	 */
	public void setSlxfs(String value) {
		this.slxfs = value;
	}
	/**
	 * 使用时间
	 */
	public java.util.Date getDsysj() {
		return dsysj;
	}

	/**
	 * 使用时间
	 */
	public void setDsysj(java.util.Date value) {
		this.dsysj = value;
	}
	/**
	 * 所属项目
	 */
	public String getSssxm() {
		return sssxm;
	}

	/**
	 * 所属项目
	 */
	public void setSssxm(String value) {
		this.sssxm = value;
	}
	
	private String sexeid;

	@Override
	public String getProjectname() {
		return this.szylx;
	}

	/**
	 * 流程实例主键
	 */
	@Override
	public String getSexeid() {
		return this.sexeid;
	}
	/**
	 * 流程实例主键
	 */
	@Override
	public void setSexeid(String sexeid) {
		this.sexeid=sexeid;
	}
}