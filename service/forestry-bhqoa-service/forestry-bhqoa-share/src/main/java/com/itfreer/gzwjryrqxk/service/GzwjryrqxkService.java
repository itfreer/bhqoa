package com.itfreer.gzwjryrqxk.service;

import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.gzwjryrqxk.entity.GzwjryrqxkEntity;

/**
 * 定义外籍人员入区许可接口
 */
public interface GzwjryrqxkService extends IBaseWorkFlowService<GzwjryrqxkEntity> {
	
}
