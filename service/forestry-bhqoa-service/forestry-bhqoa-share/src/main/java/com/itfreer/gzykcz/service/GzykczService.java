package com.itfreer.gzykcz.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzykcz.entity.GzykczEntity;

/**
 * 定义油卡充值接口
 */
public interface GzykczService extends BaseService<GzykczEntity> {
	
}
