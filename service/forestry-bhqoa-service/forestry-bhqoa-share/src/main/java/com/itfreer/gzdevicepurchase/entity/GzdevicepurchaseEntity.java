package com.itfreer.gzdevicepurchase.entity;

import java.io.Serializable;

import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义设备采购管理实体
 */
public class GzdevicepurchaseEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	/**
	 * I_VERSION
	 */
	private Integer iversion;
	
	
	/**
	 * 采购编号
	 */
	private String icgbh;
	
	
	/**
	 * 采购说明
	 */
	private String scgsm;
	
	
	/**
	 * 申请单位
	 */
	@DictionaryField(dictionaryName="p_department", toFieldName="ssqbmName")
	private String ssqbm;
	
	
	/**
	 * 申请单位
	 */
	private String ssqbmName;
	
	
	public String getSsqbmName() {
		return ssqbmName;
	}

	public void setSsqbmName(String ssqbmName) {
		this.ssqbmName = ssqbmName;
	}

	/**
	 * 申请时间
	 */
	private java.util.Date dsqsj;
	
	
	/**
	 * 经办人
	 */
	private String sjbr;
	
	
	/**
	 * 联系电话
	 */
	private String slxdh;
	
	
	/**
	 * 购买理由
	 */
	private String sgmly;
	
	/**
	 * 总价
	 */
	private Double szj;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * I_VERSION
	 */
	public Integer getIversion() {
		return iversion;
	}

	/**
	 * I_VERSION
	 */
	public void setIversion(Integer value) {
		this.iversion = value;
	}
	/**
	 * 采购编号
	 */
	public String getIcgbh() {
		return icgbh;
	}

	/**
	 * 采购编号
	 */
	public void setIcgbh(String value) {
		this.icgbh = value;
	}
	/**
	 * 采购说明
	 */
	public String getScgsm() {
		return scgsm;
	}

	/**
	 * 采购说明
	 */
	public void setScgsm(String value) {
		this.scgsm = value;
	}
	/**
	 * 申请单位
	 */
	public String getSsqbm() {
		return ssqbm;
	}

	/**
	 * 申请单位
	 */
	public void setSsqbm(String value) {
		this.ssqbm = value;
	}
	/**
	 * 申请时间
	 */
	public java.util.Date getDsqsj() {
		return dsqsj;
	}

	/**
	 * 申请时间
	 */
	public void setDsqsj(java.util.Date value) {
		this.dsqsj = value;
	}
	/**
	 * 经办人
	 */
	public String getSjbr() {
		return sjbr;
	}

	/**
	 * 经办人
	 */
	public void setSjbr(String value) {
		this.sjbr = value;
	}
	/**
	 * 联系电话
	 */
	public String getSlxdh() {
		return slxdh;
	}

	/**
	 * 联系电话
	 */
	public void setSlxdh(String value) {
		this.slxdh = value;
	}
	/**
	 * 购买理由
	 */
	public String getSgmly() {
		return sgmly;
	}

	/**
	 * 购买理由
	 */
	public void setSgmly(String value) {
		this.sgmly = value;
	}
	/**
	 * 总价
	 */
	public Double getSzj() {
		return szj;
	}

	/**
	 * 总价
	 */
	public void setSzj(Double value) {
		this.szj = value;
	}
}