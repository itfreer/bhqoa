package com.itfreer.gzdeviceborrow.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzdeviceborrow.entity.GzdeviceborrowEntity;

/**
 * 定义设备借还接口
 */
public interface GzdeviceborrowService extends BaseService<GzdeviceborrowEntity> {
	
}
