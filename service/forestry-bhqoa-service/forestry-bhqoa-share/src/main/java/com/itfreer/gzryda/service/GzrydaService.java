package com.itfreer.gzryda.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzryda.entity.GzrydaEntity;

/**
 * 定义人事档案接口
 */
public interface GzrydaService extends BaseService<GzrydaEntity> {
	
}
