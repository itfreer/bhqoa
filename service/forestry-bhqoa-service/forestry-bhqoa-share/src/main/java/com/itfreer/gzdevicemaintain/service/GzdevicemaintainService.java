package com.itfreer.gzdevicemaintain.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzdevicemaintain.entity.GzdevicemaintainEntity;

/**
 * 定义设备维修记录接口
 */
public interface GzdevicemaintainService extends BaseService<GzdevicemaintainEntity> {
	
}
