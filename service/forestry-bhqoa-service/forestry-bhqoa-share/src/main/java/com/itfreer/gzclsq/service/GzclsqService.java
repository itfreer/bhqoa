package com.itfreer.gzclsq.service;

import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.gzclsq.entity.GzclsqEntity;

/**
 * 定义车辆使用接口
 */
public interface GzclsqService extends IBaseWorkFlowService<GzclsqEntity> {
	
}
