package com.itfreer.gzsqxmtjview.entity;

import java.io.Serializable;

/**
 * 定义社区项目统计(视图)实体
 */
public class GzsqxmtjviewEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 项目状态
	 */
	private String sxmzt;
	
	
	/**
	 * 项目年度
	 */
	private String nf;
	
	
	/**
	 * 项目月份
	 */
	private String yf;
	
	/**
	 * 项目个数
	 */
	private Integer xmgs;
	
	/**
	 * 申请资金
	 */
	private Double sqzzj;
	
	/**
	 * 产生效益
	 */
	private Double cszxy;
	

	/**
	 * 项目状态
	 */
	public String getSxmzt() {
		return sxmzt;
	}

	/**
	 * 项目状态
	 */
	public void setSxmzt(String value) {
		this.sxmzt = value;
	}
	/**
	 * 项目年度
	 */
	public String getNf() {
		return nf;
	}

	/**
	 * 项目年度
	 */
	public void setNf(String value) {
		this.nf = value;
	}
	/**
	 * 项目月份
	 */
	public String getYf() {
		return yf;
	}

	/**
	 * 项目月份
	 */
	public void setYf(String value) {
		this.yf = value;
	}
	/**
	 * 项目个数
	 */
	public Integer getXmgs() {
		return xmgs;
	}

	/**
	 * 项目个数
	 */
	public void setXmgs(Integer value) {
		this.xmgs = value;
	}
	/**
	 * 申请资金
	 */
	public Double getSqzzj() {
		return sqzzj;
	}

	/**
	 * 申请资金
	 */
	public void setSqzzj(Double value) {
		this.sqzzj = value;
	}
	/**
	 * 产生效益
	 */
	public Double getCszxy() {
		return cszxy;
	}

	/**
	 * 产生效益
	 */
	public void setCszxy(Double value) {
		this.cszxy = value;
	}
}