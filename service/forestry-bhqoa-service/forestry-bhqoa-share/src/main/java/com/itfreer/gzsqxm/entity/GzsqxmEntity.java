package com.itfreer.gzsqxm.entity;

import java.io.Serializable;

/**
 * 定义社区项目申请/台账实体
 */
public class GzsqxmEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	private String id;
	
	
	/**
	 * 项目名称
	 */
	private String sxmmc;
	
	
	/**
	 * 项目编号
	 */
	private String sxmbh;
	
	/**
	 * 申请时间
	 */
	private java.util.Date dsqsj;
	
	
	/**
	 * 申请人
	 */
	private String ssqr;
	
	
	/**
	 * 申请资金
	 */
	private Double ssqzj;
	
	
	/**
	 * 联系方式
	 */
	private String slxfs;
	
	
	/**
	 * 申请材料
	 */
	private String ssqcl;
	
	
	/**
	 * 投资信息
	 */
	private String stzxx;
	
	
	/**
	 * 项目建设报告
	 */
	private String sxmjsbg;
	
	
	/**
	 * 项目状态
	 */
	private String sxmzt;
	
	
	/**
	 * 产生效益
	 */
	private Double scsxy;
	

	public Double getScsxy() {
		return scsxy;
	}

	public void setScsxy(Double scsxy) {
		this.scsxy = scsxy;
	}

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 项目名称
	 */
	public String getSxmmc() {
		return sxmmc;
	}

	/**
	 * 项目名称
	 */
	public void setSxmmc(String value) {
		this.sxmmc = value;
	}
	/**
	 * 项目编号
	 */
	public String getSxmbh() {
		return sxmbh;
	}

	/**
	 * 项目编号
	 */
	public void setSxmbh(String value) {
		this.sxmbh = value;
	}
	/**
	 * 申请时间
	 */
	public java.util.Date getDsqsj() {
		return dsqsj;
	}

	/**
	 * 申请时间
	 */
	public void setDsqsj(java.util.Date value) {
		this.dsqsj = value;
	}
	/**
	 * 申请人
	 */
	public String getSsqr() {
		return ssqr;
	}

	/**
	 * 申请人
	 */
	public void setSsqr(String value) {
		this.ssqr = value;
	}
	/**
	 * 申请资金
	 */
	public Double getSsqzj() {
		return ssqzj;
	}

	/**
	 * 申请资金
	 */
	public void setSsqzj(Double value) {
		this.ssqzj = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxfs() {
		return slxfs;
	}

	/**
	 * 联系方式
	 */
	public void setSlxfs(String value) {
		this.slxfs = value;
	}
	/**
	 * 申请材料
	 */
	public String getSsqcl() {
		return ssqcl;
	}

	/**
	 * 申请材料
	 */
	public void setSsqcl(String value) {
		this.ssqcl = value;
	}
	/**
	 * 投资信息
	 */
	public String getStzxx() {
		return stzxx;
	}

	/**
	 * 投资信息
	 */
	public void setStzxx(String value) {
		this.stzxx = value;
	}
	/**
	 * 项目建设报告
	 */
	public String getSxmjsbg() {
		return sxmjsbg;
	}

	/**
	 * 项目建设报告
	 */
	public void setSxmjsbg(String value) {
		this.sxmjsbg = value;
	}
	/**
	 * 项目状态
	 */
	public String getSxmzt() {
		return sxmzt;
	}

	/**
	 * 项目状态
	 */
	public void setSxmzt(String value) {
		this.sxmzt = value;
	}
}