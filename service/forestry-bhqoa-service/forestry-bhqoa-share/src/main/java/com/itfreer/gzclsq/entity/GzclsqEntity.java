package com.itfreer.gzclsq.entity;

import javax.persistence.Column;

import com.itfreer.bpm.flow.api.IBpmProjectEntity;
import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义车辆使用实体
 */
public class GzclsqEntity implements IBpmProjectEntity {


	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	
	/**
	 * 审批状态
	 */
	@DictionaryField(dictionaryName = "spzt_zd", toFieldName = "spztName")
	public String spzt;
	
	/**
	 * 审批状态
	 */
	public String spztName;
	public String getSpzt() {
		return spzt;
	}

	public void setSpzt(String spzt) {
		this.spzt = spzt;
	}

	public String getSpztName() {
		return spztName;
	}

	public void setSpztName(String spztName) {
		this.spztName = spztName;
	}
	
	/**
	 * 唯一编号
	 */
	private String id;
	
	/**
	 * 申请人id
	 */
	private String ssqrid;
	
	
	public String getSsqrid() {
		return ssqrid;
	}

	public void setSsqrid(String ssqrid) {
		this.ssqrid = ssqrid;
	}
	
	/**
	 * 车辆状态
	 */
	@DictionaryField(dictionaryName="p_clsyzt", toFieldName="clztName")
	private String clzt;
	
	
	/**
	 * 车辆状态
	 */
	private String clztName;
	

	public String getClztName() {
		return clztName;
	}

	public void setClztName(String clztName) {
		this.clztName = clztName;
	}

	public String getClzt() {
		return clzt;
	}

	public void setClzt(String clzt) {
		this.clzt = clzt;
	}
	
	/**
	 * 申请人
	 */
	private String ssqr;
	
	/**
	 * 申请时间
	 */
	private java.util.Date dsqsj;
	
	
	/**
	 * 申请原因
	 */
	private String ssqyy;
	
	
	/**
	 * 联系方式
	 */
	private String slxfs;
	
	/**
	 * 借用时间
	 */
	private java.util.Date djysj;
	
	/**
	 * 归还时间
	 */
	private java.util.Date dghsj;
	
	
	/**
	 * 是否同意申请
	 */
	@DictionaryField(dictionaryName="p_yesorno", toFieldName="itysqName")
	private String itysq;
	
	
	/**
	 * 是否同意申请
	 */
	private String itysqName;
	
	
	public String getItysqName() {
		return itysqName;
	}

	public void setItysqName(String itysqName) {
		this.itysqName = itysqName;
	}

	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	private String sclxx;
	
	
	/**
	 * 备注
	 */
	private String sbz;
	
	/**
	 * 用车部门
	 */
	@DictionaryField(dictionaryName = "p_department" ,toFieldName = "ycbmName")
	private String ycbm;
	
	private String ycbmName;
	
	public String getYcbmName() {
		return ycbmName;
	}

	public void setYcbmName(String ycbmName) {
		this.ycbmName = ycbmName;
	}

	/**
	 * 用车地点
	 */
	private String ycdd;
	
	/**
	 * 用车人数
	 */
	private String ycrs;
	
	/**
	 * 用车人员
	 */
	private String ycry;
	
	/**
	 * 车牌号
	 */
	private String cph;
	
	/**
	 * 驾驶员
	 */
	private String jsy;
	
	/**
	 * 部门审批意见
	 */
	private String sbmspyj;
	
	/**
	 * 现金加油
	 */
	private String sxjjy;
	
	/**
	 * 卡加油
	 */
	private String skjy;
	
	/**
	 * 加油经办人
	 */
	private String sjyjbr;
	
	/**
	 * 办公室调度意见
	 */
	private String sbgsddyj;
	
	/**
	 * 部门领导审批
	 */
	private String sbmldsp;

	public String getSbmspyj() {
		return sbmspyj;
	}

	public void setSbmspyj(String sbmspyj) {
		this.sbmspyj = sbmspyj;
	}

	public String getSxjjy() {
		return sxjjy;
	}

	public void setSxjjy(String sxjjy) {
		this.sxjjy = sxjjy;
	}

	public String getSkjy() {
		return skjy;
	}

	public void setSkjy(String skjy) {
		this.skjy = skjy;
	}

	public String getSjyjbr() {
		return sjyjbr;
	}

	public void setSjyjbr(String sjyjbr) {
		this.sjyjbr = sjyjbr;
	}

	public String getSbgsddyj() {
		return sbgsddyj;
	}

	public void setSbgsddyj(String sbgsddyj) {
		this.sbgsddyj = sbgsddyj;
	}

	public String getSbmldsp() {
		return sbmldsp;
	}

	public void setSbmldsp(String sbmldsp) {
		this.sbmldsp = sbmldsp;
	}

	public String getYcbm() {
		return ycbm;
	}

	public void setYcbm(String ycbm) {
		this.ycbm = ycbm;
	}

	public String getYcdd() {
		return ycdd;
	}

	public void setYcdd(String ycdd) {
		this.ycdd = ycdd;
	}

	public String getYcrs() {
		return ycrs;
	}

	public void setYcrs(String ycrs) {
		this.ycrs = ycrs;
	}

	public String getYcry() {
		return ycry;
	}

	public void setYcry(String ycry) {
		this.ycry = ycry;
	}

	public String getCph() {
		return cph;
	}

	public void setCph(String cph) {
		this.cph = cph;
	}

	public String getJsy() {
		return jsy;
	}

	public void setJsy(String jsy) {
		this.jsy = jsy;
	}
	
	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 申请人
	 */
	public String getSsqr() {
		return ssqr;
	}

	/**
	 * 申请人
	 */
	public void setSsqr(String value) {
		this.ssqr = value;
	}
	/**
	 * 申请时间
	 */
	public java.util.Date getDsqsj() {
		return dsqsj;
	}

	/**
	 * 申请时间
	 */
	public void setDsqsj(java.util.Date value) {
		this.dsqsj = value;
	}
	/**
	 * 申请原因
	 */
	public String getSsqyy() {
		return ssqyy;
	}

	/**
	 * 申请原因
	 */
	public void setSsqyy(String value) {
		this.ssqyy = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxfs() {
		return slxfs;
	}

	/**
	 * 联系方式
	 */
	public void setSlxfs(String value) {
		this.slxfs = value;
	}
	/**
	 * 借用时间
	 */
	public java.util.Date getDjysj() {
		return djysj;
	}

	/**
	 * 借用时间
	 */
	public void setDjysj(java.util.Date value) {
		this.djysj = value;
	}
	/**
	 * 归还时间
	 */
	public java.util.Date getDghsj() {
		return dghsj;
	}

	/**
	 * 归还时间
	 */
	public void setDghsj(java.util.Date value) {
		this.dghsj = value;
	}
	/**
	 * 是否同意申请
	 */
	public String getItysq() {
		return itysq;
	}

	/**
	 * 是否同意申请
	 */
	public void setItysq(String value) {
		this.itysq = value;
	}
	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	public String getSclxx() {
		return sclxx;
	}

	/**
	 * 车辆信息(与车辆档案挂接)
	 */
	public void setSclxx(String value) {
		this.sclxx = value;
	}
	/**
	 * 备注
	 */
	public String getSbz() {
		return sbz;
	}

	/**
	 * 备注
	 */
	public void setSbz(String value) {
		this.sbz = value;
	}

	@Override
	public String getProjectname() {
		return this.ssqyy;
	}
	private String sexeid;
	
	/**
	 * 流程实例主键
	 */
	@Override
	public String getSexeid() {
		return this.sexeid;
	}
	/**
	 * 流程实例主键
	 */
	@Override
	public void setSexeid(String sexeid) {
		this.sexeid=sexeid;
	}
}