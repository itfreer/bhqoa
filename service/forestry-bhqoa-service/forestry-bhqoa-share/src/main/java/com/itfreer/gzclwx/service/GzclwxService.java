package com.itfreer.gzclwx.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzclwx.entity.GzclwxEntity;

/**
 * 定义车辆维修接口
 */
public interface GzclwxService extends BaseService<GzclwxEntity> {
	
}
