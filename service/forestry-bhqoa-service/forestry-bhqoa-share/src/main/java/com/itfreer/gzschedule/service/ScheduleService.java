package com.itfreer.gzschedule.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzschedule.entity.ScheduleEntity;

/**
 * 定义日常安排接口
 */
public interface ScheduleService extends BaseService<ScheduleEntity> {
	
}
