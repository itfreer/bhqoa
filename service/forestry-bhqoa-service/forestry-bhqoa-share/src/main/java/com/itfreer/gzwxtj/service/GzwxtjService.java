package com.itfreer.gzwxtj.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzwxtj.entity.GzwxtjEntity;

/**
 * 定义维修统计接口
 */
public interface GzwxtjService extends BaseService<GzwxtjEntity> {
	
}
