package com.itfreer.gzplanpersonnel.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzplanpersonnel.entity.GzplanpersonnelEntity;

/**
 * 定义项目规划人员接口
 */
public interface GzplanpersonnelService extends BaseService<GzplanpersonnelEntity> {
	
}
