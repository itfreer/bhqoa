package com.itfreer.vkqtj.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.vkqtj.entity.VkqtjEntity;

/**
 * 定义考勤统计接口
 */
public interface VkqtjService extends BaseService<VkqtjEntity> {
	
}
