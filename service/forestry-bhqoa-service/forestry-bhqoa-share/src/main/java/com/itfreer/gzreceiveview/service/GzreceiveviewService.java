package com.itfreer.gzreceiveview.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzreceiveview.entity.GzreceiveviewEntity;

/**
 * 定义收文管理接口
 */
public interface GzreceiveviewService extends BaseService<GzreceiveviewEntity> {
	
}
