package com.itfreer.gzhlyda.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzhlyda.entity.GzhlydaEntity;

/**
 * 定义护林员档案接口
 */
public interface GzhlydaService extends BaseService<GzhlydaEntity> {
	
}
