package com.itfreer.gzaqbw.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzaqbw.entity.GzaqbwEntity;

/**
 * 定义安全保卫接口
 */
public interface GzaqbwService extends BaseService<GzaqbwEntity> {
	
}
