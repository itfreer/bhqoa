package com.itfreer.gzhlygzgl.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzhlygzgl.entity.GzhlygzglEntity;

/**
 * 定义护林员工资管理接口
 */
public interface GzhlygzglService extends BaseService<GzhlygzglEntity> {
	
}
