package com.itfreer.gzplanpersonnel.entity;

import java.io.Serializable;

import javax.persistence.Column;

import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义项目规划人员实体
 */
public class GzplanpersonnelEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 计划任务编号
	 */
	private String sjhrwbh;
	
	
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 人员编号
	 */
	private String srybh;
	
	
	/**
	 * 人员名称
	 */
	private String yrmc;
	

	/**
	 * 项目编号
	 */
	private String sxmbh;
	
	
	/**
	 * 项目名称
	 */
	private String sxmmc;
	
	
	/**
	 * 人员性别
	 */
	@DictionaryField(dictionaryName = "p_sex" ,toFieldName = "ssexName")
	private String ssex;
	
	
	/**
	 * 人员性别
	 */
	private String ssexName;
	

	public String getSsexName() {
		return ssexName;
	}

	public void setSsexName(String ssexName) {
		this.ssexName = ssexName;
	}

	/**
	 * 人员身份证
	 */
	private String ssfz;
	
	
	/**
	 * 人员联系方式
	 */
	private String slxfs;
	
	/**
	 * 项目ID
	 */
	private String xmid;
	
	
	/**
	 * 人员职称
	 */
	private String sryzc;
	
	/**
	 * 项目申请人
	 */
	private String sqr;
	/**
	 * 项目申请人ID
	 */
	private String sqrid;
	/**
	 *项目申请部门
	 */
	@DictionaryField(dictionaryName = "p_department" ,toFieldName = "sqbmName")
	private String sqbm;
	/**
	 *项目申请部门
	 */
	private String sqbmName;
	/**
	 * 项目申请时间
	 */
	private java.util.Date sqsj;
	/**
	 * 项目结项时间
	 */
	private java.util.Date jxsj;
	
	public String getSqr() {
		return sqr;
	}

	public void setSqr(String sqr) {
		this.sqr = sqr;
	}

	public String getSqrid() {
		return sqrid;
	}

	public void setSqrid(String sqrid) {
		this.sqrid = sqrid;
	}

	public String getSqbm() {
		return sqbm;
	}

	public void setSqbm(String sqbm) {
		this.sqbm = sqbm;
	}
	
	public String getSqbmName() {
		return sqbmName;
	}

	public void setSqbmName(String sqbmName) {
		this.sqbmName = sqbmName;
	}

	public java.util.Date getSqsj() {
		return sqsj;
	}

	public void setSqsj(java.util.Date sqsj) {
		this.sqsj = sqsj;
	}

	public java.util.Date getJxsj() {
		return jxsj;
	}

	public void setJxsj(java.util.Date jxsj) {
		this.jxsj = jxsj;
	}
	
	
	public String getSryzc() {
		return sryzc;
	}

	public void setSryzc(String sryzc) {
		this.sryzc = sryzc;
	}
	
	
	public String getXmid() {
		return xmid;
	}

	public void setXmid(String sxmid) {
		this.xmid = sxmid;
	}

	public String getSsex() {
		return ssex;
	}

	public void setSsex(String ssex) {
		this.ssex = ssex;
	}

	public String getSsfz() {
		return ssfz;
	}

	public void setSsfz(String ssfz) {
		this.ssfz = ssfz;
	}

	public String getSlxfs() {
		return slxfs;
	}

	public void setSlxfs(String slxfs) {
		this.slxfs = slxfs;
	}
	
	/**
	 * 项目编号
	 */
	public String getSxmbh() {
		return sxmbh;
	}

	/**
	 * 项目编号
	 */
	public void setSxmbh(String value) {
		this.sxmbh = value;
	}
	/**
	 * 项目名称
	 */
	public String getSxmmc() {
		return sxmmc;
	}

	/**
	 * 项目名称
	 */
	public void setSxmmc(String value) {
		this.sxmmc = value;
	}
	
	/**
	 * 计划任务编号
	 */
	public String getSjhrwbh() {
		return sjhrwbh;
	}

	/**
	 * 计划任务编号
	 */
	public void setSjhrwbh(String value) {
		this.sjhrwbh = value;
	}
	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 人员编号
	 */
	public String getSrybh() {
		return srybh;
	}

	/**
	 * 人员编号
	 */
	public void setSrybh(String value) {
		this.srybh = value;
	}
	/**
	 * 人员名称
	 */
	public String getYrmc() {
		return yrmc;
	}

	/**
	 * 人员名称
	 */
	public void setYrmc(String value) {
		this.yrmc = value;
	}
}