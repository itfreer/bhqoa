package com.itfreer.gzyzgl.entity;

import com.itfreer.bpm.flow.api.IBpmProjectEntity;
import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义用章管理实体
 */
public class GzyzglEntity implements IBpmProjectEntity {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 申请人ID
	 */
	private String sqrid;
	
	
	/**
	 * 申请人名称
	 */
	private String sqrmc;
	
	
	/**
	 * 部门ID
	 */
	@DictionaryField(dictionaryName = "p_department" ,toFieldName = "bmmc")
	private String bmid;
	
	
	/**
	 * 部门名称
	 */
	private String bmmc;
	
	/**
	 * 申请时间
	 */
	private java.util.Date sqsj;
	
	/**
	 * 申请使用开始时间
	 */
	private java.util.Date sqsykssj;
	
	/**
	 * 申请使用结束时间
	 */
	private java.util.Date sqsyjssj;
	
	
	/**
	 * 申请事由
	 */
	private String sqsy;
	
	
	/**
	 * 附件
	 */
	private String clfj;
	
	
	/**
	 * 申请状态
	 */
	private String sqzt;
	
	
	/**
	 * 申请事由说明
	 */
	private String sqsysm;
	
	
	/**
	 * 工作流主键
	 */
	private String sexeid;
	
	/**
	 * 是否包含法人代表章
	 */
	private Boolean sfbhfwdbz;
	
	
	/**
	 * 所属分管局领导
	 */
	private String fgjld;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 申请人ID
	 */
	public String getSqrid() {
		return sqrid;
	}

	/**
	 * 申请人ID
	 */
	public void setSqrid(String value) {
		this.sqrid = value;
	}
	/**
	 * 申请人名称
	 */
	public String getSqrmc() {
		return sqrmc;
	}

	/**
	 * 申请人名称
	 */
	public void setSqrmc(String value) {
		this.sqrmc = value;
	}
	/**
	 * 部门ID
	 */
	public String getBmid() {
		return bmid;
	}

	/**
	 * 部门ID
	 */
	public void setBmid(String value) {
		this.bmid = value;
	}
	/**
	 * 部门名称
	 */
	public String getBmmc() {
		return bmmc;
	}

	/**
	 * 部门名称
	 */
	public void setBmmc(String value) {
		this.bmmc = value;
	}
	/**
	 * 申请时间
	 */
	public java.util.Date getSqsj() {
		return sqsj;
	}

	/**
	 * 申请时间
	 */
	public void setSqsj(java.util.Date value) {
		this.sqsj = value;
	}
	/**
	 * 申请使用开始时间
	 */
	public java.util.Date getSqsykssj() {
		return sqsykssj;
	}

	/**
	 * 申请使用开始时间
	 */
	public void setSqsykssj(java.util.Date value) {
		this.sqsykssj = value;
	}
	/**
	 * 申请使用结束时间
	 */
	public java.util.Date getSqsyjssj() {
		return sqsyjssj;
	}

	/**
	 * 申请使用结束时间
	 */
	public void setSqsyjssj(java.util.Date value) {
		this.sqsyjssj = value;
	}
	/**
	 * 申请事由
	 */
	public String getSqsy() {
		return sqsy;
	}

	/**
	 * 申请事由
	 */
	public void setSqsy(String value) {
		this.sqsy = value;
	}
	/**
	 * 附件
	 */
	public String getClfj() {
		return clfj;
	}

	/**
	 * 附件
	 */
	public void setClfj(String value) {
		this.clfj = value;
	}
	/**
	 * 申请状态
	 */
	public String getSqzt() {
		return sqzt;
	}

	/**
	 * 申请状态
	 */
	public void setSqzt(String value) {
		this.sqzt = value;
	}
	/**
	 * 申请事由说明
	 */
	public String getSqsysm() {
		return sqsysm;
	}

	/**
	 * 申请事由说明
	 */
	public void setSqsysm(String value) {
		this.sqsysm = value;
	}
	/**
	 * 工作流主键
	 */
	public String getSexeid() {
		return sexeid;
	}

	/**
	 * 工作流主键
	 */
	public void setSexeid(String value) {
		this.sexeid = value;
	}
	/**
	 * 是否包含法人代表章
	 */
	public Boolean getSfbhfwdbz() {
		return sfbhfwdbz;
	}

	/**
	 * 是否包含法人代表章
	 */
	public void setSfbhfwdbz(Boolean value) {
		this.sfbhfwdbz = value;
	}
	
	/**
	 * 所属分管局领导
	 */
	public String getFgjld() {
		return fgjld;
	}

	/**
	 * 所属分管局领导
	 */
	public void setFgjld(String value) {
		this.fgjld = value;
	}

	@Override
	public String getProjectname() {
		return this.sqsy;
	}
}