package com.itfreer.gzgzrqdsj.entity;

import java.io.Serializable;

/**
 * 定义签到时间实体
 */
public class GzgzrqdsjEntity  implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;
	
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 开始月份
	 */
	private Integer ksyf;
	
	
	/**
	 * 开始日期
	 */
	private Integer ksrq;
	
	/**
	 * 结束月份
	 */
	private Integer jsyf;
	
	
	/**
	 * 结束日期
	 */
	private Integer jsrq;
	
	/**
	 * 上午上班时
	 */
	private Integer swsbs;
	
	
	/**
	 * 上午上班分
	 */
	private Integer swsbf;
	
	/**
	 * 上午下班时
	 */
	private Integer swxbs;
	
	
	/**
	 * 上午下班分
	 */
	private Integer swxbf;
	
	/**
	 * 下午上班时
	 */
	private Integer xwsbs;
	
	
	/**
	 * 下午上班分
	 */
	private Integer xwsbf;
	
	/**
	 * 下午下班时
	 */
	private Integer xwxbs;
	
	
	/**
	 * 下午下班分
	 */
	private Integer xwxbf;
	
	
	/**
	 * 上午上班结束时
	 */
	private Integer swsbjss;
	
	
	/**
	 * 上午上班结束分
	 */
	private Integer swsbjsf;
	
	/**
	 * 上午下班结束时
	 */
	private Integer swxbjss;
	
	
	/**
	 * 上午下班结束分
	 */
	private Integer swxbjsf;
	
	/**
	 * 下午上班结束时
	 */
	private Integer xwsbjss;
	
	
	/**
	 * 下午上班结束分
	 */
	private Integer xwsbjsf;
	
	/**
	 * 下午下班结束时
	 */
	private Integer xwxbjss;
	
	
	/**
	 * 下午下班结束分
	 */
	private Integer xwxbjsf;
	
	
	
	public Integer getSwsbjss() {
		return swsbjss;
	}

	public void setSwsbjss(Integer swsbjss) {
		this.swsbjss = swsbjss;
	}

	public Integer getSwsbjsf() {
		return swsbjsf;
	}

	public void setSwsbjsf(Integer swsbjsf) {
		this.swsbjsf = swsbjsf;
	}

	public Integer getSwxbjss() {
		return swxbjss;
	}

	public void setSwxbjss(Integer swxbjss) {
		this.swxbjss = swxbjss;
	}

	public Integer getSwxbjsf() {
		return swxbjsf;
	}

	public void setSwxbjsf(Integer swxbjsf) {
		this.swxbjsf = swxbjsf;
	}

	public Integer getXwsbjss() {
		return xwsbjss;
	}

	public void setXwsbjss(Integer xwsbjss) {
		this.xwsbjss = xwsbjss;
	}

	public Integer getXwsbjsf() {
		return xwsbjsf;
	}

	public void setXwsbjsf(Integer xwsbjsf) {
		this.xwsbjsf = xwsbjsf;
	}

	public Integer getXwxbjss() {
		return xwxbjss;
	}

	public void setXwxbjss(Integer xwxbjss) {
		this.xwxbjss = xwxbjss;
	}

	public Integer getXwxbjsf() {
		return xwxbjsf;
	}

	public void setXwxbjsf(Integer xwxbjsf) {
		this.xwxbjsf = xwxbjsf;
	}

	/**
	 * 创建时间
	 */
	private java.util.Date cjsj;
	
	/**
	 * 备注
	 */
	private String bz;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getKsyf() {
		return ksyf;
	}

	public void setKsyf(Integer ksyf) {
		this.ksyf = ksyf;
	}

	public Integer getKsrq() {
		return ksrq;
	}

	public void setKsrq(Integer ksrq) {
		this.ksrq = ksrq;
	}

	public Integer getJsyf() {
		return jsyf;
	}

	public void setJsyf(Integer jsyf) {
		this.jsyf = jsyf;
	}

	public Integer getJsrq() {
		return jsrq;
	}

	public void setJsrq(Integer jsrq) {
		this.jsrq = jsrq;
	}

	public Integer getSwsbs() {
		return swsbs;
	}

	public void setSwsbs(Integer swsbs) {
		this.swsbs = swsbs;
	}

	public Integer getXwsbf() {
		return xwsbf;
	}

	public void setXwsbf(Integer xwsbf) {
		this.xwsbf = xwsbf;
	}

	public Integer getSwxbs() {
		return swxbs;
	}

	public void setSwxbs(Integer swxbs) {
		this.swxbs = swxbs;
	}

	public Integer getSwxbf() {
		return swxbf;
	}

	public void setSwxbf(Integer swxbf) {
		this.swxbf = swxbf;
	}

	public Integer getXwsbs() {
		return xwsbs;
	}

	public void setXwsbs(Integer xwsbs) {
		this.xwsbs = xwsbs;
	}

	public Integer getSwsbf() {
		return swsbf;
	}

	public void setSwsbf(Integer swsbf) {
		this.swsbf = swsbf;
	}

	public Integer getXwxbs() {
		return xwxbs;
	}

	public void setXwxbs(Integer xwxbs) {
		this.xwxbs = xwxbs;
	}

	public Integer getXwxbf() {
		return xwxbf;
	}

	public void setXwxbf(Integer xwxbf) {
		this.xwxbf = xwxbf;
	}

	public java.util.Date getCjsj() {
		return cjsj;
	}

	public void setCjsj(java.util.Date cjsj) {
		this.cjsj = cjsj;
	}

	public String getBz() {
		return bz;
	}

	public void setBz(String bz) {
		this.bz = bz;
	}
	
}