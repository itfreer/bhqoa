package com.itfreer.gzryda.entity;

import java.io.Serializable;

import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义人事档案实体
 */
public class GzrydaEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 姓名
	 */
	private String xingming;
	
	
	/**
	 * 性别
	 */
	@DictionaryField(dictionaryName="p_sex", toFieldName="xingbieName")
	private String xingbie;
	
	
	/**
	 * 性别
	 */
	private String xingbieName;
	
	/**
	 * 出生日期
	 */
	private java.util.Date chushengrq;
	
	
	/**
	 * 身份证证号
	 */
	private String shengfenz;
	
	
	/**
	 * 民族
	 */
	@DictionaryField(dictionaryName="p_minzu", toFieldName="xingbieName")
	private String mingzu;
	
	
	/**
	 * 民族
	 */
	private String mingzuName;
	
	
	/**
	 * 籍贯
	 */
	private String jiguang;
	
	
	/**
	 * 政治面貌
	 */
	@DictionaryField(dictionaryName="p_zzmm", toFieldName="zhengzhimmName")
	private String zhengzhimm;
	
	
	/**
	 * 政治面貌
	 */
	private String zhengzhimmName;
	
	
	/**
	 * 工作时间
	 */
	private java.util.Date gongzuosj;
	
	
	/**
	 * 学历
	 */
	@DictionaryField(dictionaryName="p_xueli", toFieldName="xueliName")
	private String xueli;
	
	
	/**
	 * 学历
	 */
	private String xueliName;
	
	
	public String getXingbieName() {
		return xingbieName;
	}

	public void setXingbieName(String xingbieName) {
		this.xingbieName = xingbieName;
	}

	public String getMingzuName() {
		return mingzuName;
	}

	public void setMingzuName(String mingzuName) {
		this.mingzuName = mingzuName;
	}

	public String getZhengzhimmName() {
		return zhengzhimmName;
	}

	public void setZhengzhimmName(String zhengzhimmName) {
		this.zhengzhimmName = zhengzhimmName;
	}

	public String getXueliName() {
		return xueliName;
	}

	public void setXueliName(String xueliName) {
		this.xueliName = xueliName;
	}

	/**
	 * 毕业学校
	 */
	private String biyexx;
	
	/**
	 * 毕业时间
	 */
	private java.util.Date biyesj;
	
	
	/**
	 * 专业
	 */
	private String zhuangye;
	
	
	/**
	 * 行政职务
	 */
	private String xingzhengzw;
	
	
	/**
	 * 技术职务
	 */
	private String jishuzw;
	
	
	/**
	 * 部门
	 */
	@DictionaryField(dictionaryName="p_department", toFieldName="bumenName")
	private String bumen;
	
	
	/**
	 * 部门
	 */
	private String bumenName;
	
	
	public String getBumenName() {
		return bumenName;
	}

	public void setBumenName(String bumenName) {
		this.bumenName = bumenName;
	}

	/**
	 * 职务
	 */
	private String zhiwu;
	
	
	/**
	 * 岗位类型
	 */
	private String gagnweilx;
	
	
	/**
	 * 备注
	 */
	private String beizhu;
	

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 姓名
	 */
	public String getXingming() {
		return xingming;
	}

	/**
	 * 姓名
	 */
	public void setXingming(String value) {
		this.xingming = value;
	}
	/**
	 * 性别
	 */
	public String getXingbie() {
		return xingbie;
	}

	/**
	 * 性别
	 */
	public void setXingbie(String value) {
		this.xingbie = value;
	}
	/**
	 * 出生日期
	 */
	public java.util.Date getChushengrq() {
		return chushengrq;
	}

	/**
	 * 出生日期
	 */
	public void setChushengrq(java.util.Date value) {
		this.chushengrq = value;
	}
	/**
	 * 身份证证号
	 */
	public String getShengfenz() {
		return shengfenz;
	}

	/**
	 * 身份证证号
	 */
	public void setShengfenz(String value) {
		this.shengfenz = value;
	}
	/**
	 * 民族
	 */
	public String getMingzu() {
		return mingzu;
	}

	/**
	 * 民族
	 */
	public void setMingzu(String value) {
		this.mingzu = value;
	}
	/**
	 * 籍贯
	 */
	public String getJiguang() {
		return jiguang;
	}

	/**
	 * 籍贯
	 */
	public void setJiguang(String value) {
		this.jiguang = value;
	}
	/**
	 * 政治面貌
	 */
	public String getZhengzhimm() {
		return zhengzhimm;
	}

	/**
	 * 政治面貌
	 */
	public void setZhengzhimm(String value) {
		this.zhengzhimm = value;
	}
	/**
	 * 工作时间
	 */
	public java.util.Date getGongzuosj() {
		return gongzuosj;
	}

	/**
	 * 工作时间
	 */
	public void setGongzuosj(java.util.Date value) {
		this.gongzuosj = value;
	}
	/**
	 * 学历
	 */
	public String getXueli() {
		return xueli;
	}

	/**
	 * 学历
	 */
	public void setXueli(String value) {
		this.xueli = value;
	}
	/**
	 * 毕业学校
	 */
	public String getBiyexx() {
		return biyexx;
	}

	/**
	 * 毕业学校
	 */
	public void setBiyexx(String value) {
		this.biyexx = value;
	}
	/**
	 * 毕业时间
	 */
	public java.util.Date getBiyesj() {
		return biyesj;
	}

	/**
	 * 毕业时间
	 */
	public void setBiyesj(java.util.Date value) {
		this.biyesj = value;
	}
	/**
	 * 专业
	 */
	public String getZhuangye() {
		return zhuangye;
	}

	/**
	 * 专业
	 */
	public void setZhuangye(String value) {
		this.zhuangye = value;
	}
	/**
	 * 行政职务
	 */
	public String getXingzhengzw() {
		return xingzhengzw;
	}

	/**
	 * 行政职务
	 */
	public void setXingzhengzw(String value) {
		this.xingzhengzw = value;
	}
	/**
	 * 技术职务
	 */
	public String getJishuzw() {
		return jishuzw;
	}

	/**
	 * 技术职务
	 */
	public void setJishuzw(String value) {
		this.jishuzw = value;
	}
	/**
	 * 部门
	 */
	public String getBumen() {
		return bumen;
	}

	/**
	 * 部门
	 */
	public void setBumen(String value) {
		this.bumen = value;
	}
	/**
	 * 职务
	 */
	public String getZhiwu() {
		return zhiwu;
	}

	/**
	 * 职务
	 */
	public void setZhiwu(String value) {
		this.zhiwu = value;
	}
	/**
	 * 岗位类型
	 */
	public String getGagnweilx() {
		return gagnweilx;
	}

	/**
	 * 岗位类型
	 */
	public void setGagnweilx(String value) {
		this.gagnweilx = value;
	}
	/**
	 * 备注
	 */
	public String getBeizhu() {
		return beizhu;
	}

	/**
	 * 备注
	 */
	public void setBeizhu(String value) {
		this.beizhu = value;
	}
}