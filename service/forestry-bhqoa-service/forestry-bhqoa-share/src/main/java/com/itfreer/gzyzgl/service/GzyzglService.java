package com.itfreer.gzyzgl.service;

import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.gzyzgl.entity.GzyzglEntity;

/**
 * 定义用章管理接口
 */
public interface GzyzglService extends IBaseWorkFlowService<GzyzglEntity> {
	
}
