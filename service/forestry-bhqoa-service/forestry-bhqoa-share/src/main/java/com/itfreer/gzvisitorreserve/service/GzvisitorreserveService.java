package com.itfreer.gzvisitorreserve.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzvisitorreserve.entity.GzvisitorreserveEntity;

/**
 * 定义访客预约管理接口
 */
public interface GzvisitorreserveService extends BaseService<GzvisitorreserveEntity> {
	
}
