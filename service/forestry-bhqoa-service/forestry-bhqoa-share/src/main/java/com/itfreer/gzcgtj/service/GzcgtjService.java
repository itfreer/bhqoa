package com.itfreer.gzcgtj.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzcgtj.entity.GzcgtjEntity;

/**
 * 定义采购统计接口
 */
public interface GzcgtjService extends BaseService<GzcgtjEntity> {
	
}
