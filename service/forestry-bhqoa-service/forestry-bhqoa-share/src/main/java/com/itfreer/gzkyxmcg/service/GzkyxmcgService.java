package com.itfreer.gzkyxmcg.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzkyxmcg.entity.GzkyxmcgEntity;

/**
 * 定义科研项目成果接口
 */
public interface GzkyxmcgService extends BaseService<GzkyxmcgEntity> {
	
}
