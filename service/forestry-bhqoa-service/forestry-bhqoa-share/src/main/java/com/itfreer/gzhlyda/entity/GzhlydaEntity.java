package com.itfreer.gzhlyda.entity;

import java.io.Serializable;

/**
 * 定义护林员档案实体
 */
public class GzhlydaEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	private String id;
	
	
	/**
	 * 护林员名称
	 */
	private String shlymc;
	
	/**
	 * 护林员出生日期
	 */
	private java.util.Date dhlysr;
	
	
	/**
	 * 联系方式
	 */
	private String slxfs;
	
	
	/**
	 * 护林员类型
	 */
	private String shlylx;
	
	
	/**
	 * 所在管理站
	 */
	private String sszglz;
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 护林员名称
	 */
	public String getShlymc() {
		return shlymc;
	}

	/**
	 * 护林员名称
	 */
	public void setShlymc(String value) {
		this.shlymc = value;
	}
	/**
	 * 护林员出生日期
	 */
	public java.util.Date getDhlysr() {
		return dhlysr;
	}

	/**
	 * 护林员出生日期
	 */
	public void setDhlysr(java.util.Date value) {
		this.dhlysr = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxfs() {
		return slxfs;
	}

	/**
	 * 联系方式
	 */
	public void setSlxfs(String value) {
		this.slxfs = value;
	}
	/**
	 * 护林员类型
	 */
	public String getShlylx() {
		return shlylx;
	}

	/**
	 * 护林员类型
	 */
	public void setShlylx(String value) {
		this.shlylx = value;
	}
	/**
	 * 所在管理站
	 */
	public String getSszglz() {
		return sszglz;
	}

	/**
	 * 所在管理站
	 */
	public void setSszglz(String value) {
		this.sszglz = value;
	}
}