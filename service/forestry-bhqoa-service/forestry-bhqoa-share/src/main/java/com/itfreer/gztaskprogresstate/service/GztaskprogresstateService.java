package com.itfreer.gztaskprogresstate.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gztaskprogresstate.entity.GztaskprogresstateEntity;

/**
 * 定义任务监测接口
 */
public interface GztaskprogresstateService extends BaseService<GztaskprogresstateEntity> {
	
}
