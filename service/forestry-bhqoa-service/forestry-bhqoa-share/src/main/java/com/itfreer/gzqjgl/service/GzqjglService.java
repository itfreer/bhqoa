package com.itfreer.gzqjgl.service;

import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.gzqjgl.entity.GzqjglEntity;

/**
 * 定义请假管理接口
 */
public interface GzqjglService extends IBaseWorkFlowService<GzqjglEntity> {
	
}
