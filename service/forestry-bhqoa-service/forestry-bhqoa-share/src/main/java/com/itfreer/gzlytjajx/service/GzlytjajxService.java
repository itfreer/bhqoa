package com.itfreer.gzlytjajx.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzlytjajx.entity.GzlytjajxEntity;

/**
 * 定义林业统计案件续接口
 */
public interface GzlytjajxService extends BaseService<GzlytjajxEntity> {
	
}
