package com.itfreer.gzxmjsxk.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzxmjsxk.entity.GzxmjsxkEntity;

/**
 * 定义项目建设许可接口
 */
public interface GzxmjsxkService extends BaseService<GzxmjsxkEntity> {
	
}
