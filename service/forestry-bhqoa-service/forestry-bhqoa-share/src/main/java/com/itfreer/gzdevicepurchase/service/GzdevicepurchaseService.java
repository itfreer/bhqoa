package com.itfreer.gzdevicepurchase.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzdevicepurchase.entity.GzdevicepurchaseEntity;

/**
 * 定义设备采购管理接口
 */
public interface GzdevicepurchaseService extends BaseService<GzdevicepurchaseEntity> {
	
}
