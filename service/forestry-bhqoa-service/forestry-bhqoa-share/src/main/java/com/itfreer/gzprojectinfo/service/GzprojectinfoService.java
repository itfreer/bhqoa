package com.itfreer.gzprojectinfo.service;

import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.gzprojectinfo.entity.GzprojectinfoEntity;

/**
 * 定义项目立项管理接口
 */
public interface GzprojectinfoService extends IBaseWorkFlowService<GzprojectinfoEntity> {
	
}
