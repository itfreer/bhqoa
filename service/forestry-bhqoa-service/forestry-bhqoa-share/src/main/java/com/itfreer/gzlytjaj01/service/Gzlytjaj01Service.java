package com.itfreer.gzlytjaj01.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzlytjaj01.entity.Gzlytjaj01Entity;

/**
 * 定义林业统计案件接口
 */
public interface Gzlytjaj01Service extends BaseService<Gzlytjaj01Entity> {
	
}
