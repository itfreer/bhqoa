package com.itfreer.gzqdgl.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzqdgl.entity.GzqdglEntity;

/**
 * 定义签到管理接口
 */
public interface GzqdglService extends BaseService<GzqdglEntity> {
	
}
