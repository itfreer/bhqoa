package com.itfreer.gzddtz.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzddtz.entity.GzddtzEntity;

/**
 * 定义调度台账接口
 */
public interface GzddtzService extends BaseService<GzddtzEntity> {
	
}
