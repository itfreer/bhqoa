package com.itfreer.gzdevice.entity;

import java.io.Serializable;

import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义设备实体
 */
public class GzdeviceEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	/**
	 * FS__ID
	 */
	private Integer fsid;
	
	/**
	 * I_VERSION
	 */
	private Integer iversion;
	
	
	/**
	 * 设备编号
	 */
	private String ssbbh;
	
	
	/**
	 * 设备名称
	 */
	private String ssbmc;
	
	
	/**
	 * 型号
	 */
	private String sxh;
	
	
	/**
	 * 序列号
	 */
	private String sxlh;
	
	/**
	 * 购买日期
	 */
	private java.util.Date dgmrq;
	
	/**
	 * 是否有发票
	 */
	@DictionaryField(dictionaryName="p_yesorno", toFieldName="isfyfpName")
	private String isfyfp;
	
	
	/**
	 * 是否有发票
	 */
	private String isfyfpName;
	
	
	/**
	 * 发票附件
	 */
	private String sfpfj;
	
	
	/**
	 * 设备使用期限
	 */
	private String ssbsyqx;
	
	
	/**
	 * 详细参数
	 */
	private String sxxcs;
	
	/**
	 * 是否涉密设备
	 */
	@DictionaryField(dictionaryName="p_yesorno", toFieldName="isfsmsbName")
	private String isfsmsb;
	
	
	/**
	 * 是否涉密设备
	 */
	private String isfsmsbName;
	
	
	public String getIsfyfpName() {
		return isfyfpName;
	}

	public void setIsfyfpName(String isfyfpName) {
		this.isfyfpName = isfyfpName;
	}

	public String getIsfsmsbName() {
		return isfsmsbName;
	}

	public void setIsfsmsbName(String isfsmsbName) {
		this.isfsmsbName = isfsmsbName;
	}

	/**
	 * 最后年检
	 */
	private String szhnj;
	
	
	/**
	 * 状态
	 */
	@DictionaryField(dictionaryName="p_clzt", toFieldName="sztName")
	private String szt;
	
	
	/**
	 * 状态
	 */
	private String sztName;
	

	public String getSztName() {
		return sztName;
	}

	public void setSztName(String sztName) {
		this.sztName = sztName;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * FS__ID
	 */
	public Integer getFsid() {
		return fsid;
	}

	/**
	 * FS__ID
	 */
	public void setFsid(Integer value) {
		this.fsid = value;
	}
	/**
	 * I_VERSION
	 */
	public Integer getIversion() {
		return iversion;
	}

	/**
	 * I_VERSION
	 */
	public void setIversion(Integer value) {
		this.iversion = value;
	}
	/**
	 * 设备编号
	 */
	public String getSsbbh() {
		return ssbbh;
	}

	/**
	 * 设备编号
	 */
	public void setSsbbh(String value) {
		this.ssbbh = value;
	}
	/**
	 * 设备名称
	 */
	public String getSsbmc() {
		return ssbmc;
	}

	/**
	 * 设备名称
	 */
	public void setSsbmc(String value) {
		this.ssbmc = value;
	}
	/**
	 * 型号
	 */
	public String getSxh() {
		return sxh;
	}

	/**
	 * 型号
	 */
	public void setSxh(String value) {
		this.sxh = value;
	}
	/**
	 * 序列号
	 */
	public String getSxlh() {
		return sxlh;
	}

	/**
	 * 序列号
	 */
	public void setSxlh(String value) {
		this.sxlh = value;
	}
	/**
	 * 购买日期
	 */
	public java.util.Date getDgmrq() {
		return dgmrq;
	}

	/**
	 * 购买日期
	 */
	public void setDgmrq(java.util.Date value) {
		this.dgmrq = value;
	}
	/**
	 * 是否有发票
	 */
	public String getIsfyfp() {
		return isfyfp;
	}

	/**
	 * 是否有发票
	 */
	public void setIsfyfp(String value) {
		this.isfyfp = value;
	}
	/**
	 * 发票附件
	 */
	public String getSfpfj() {
		return sfpfj;
	}

	/**
	 * 发票附件
	 */
	public void setSfpfj(String value) {
		this.sfpfj = value;
	}
	/**
	 * 设备使用期限
	 */
	public String getSsbsyqx() {
		return ssbsyqx;
	}

	/**
	 * 设备使用期限
	 */
	public void setSsbsyqx(String value) {
		this.ssbsyqx = value;
	}
	/**
	 * 详细参数
	 */
	public String getSxxcs() {
		return sxxcs;
	}

	/**
	 * 详细参数
	 */
	public void setSxxcs(String value) {
		this.sxxcs = value;
	}
	/**
	 * 是否涉密设备
	 */
	public String getIsfsmsb() {
		return isfsmsb;
	}

	/**
	 * 是否涉密设备
	 */
	public void setIsfsmsb(String value) {
		this.isfsmsb = value;
	}
	/**
	 * 最后年检
	 */
	public String getSzhnj() {
		return szhnj;
	}

	/**
	 * 最后年检
	 */
	public void setSzhnj(String value) {
		this.szhnj = value;
	}
	/**
	 * 状态
	 */
	public String getSzt() {
		return szt;
	}

	/**
	 * 状态
	 */
	public void setSzt(String value) {
		this.szt = value;
	}
}