package com.itfreer.gzpublishfile.entity;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;

import com.itfreer.bpm.flow.api.IBpmProjectEntity;
import com.itfreer.form.dictionary.reflect.DictionaryField;
import com.itfreer.gzreceivefile.entity.GzreceivefileEntity;

/**
 * 定义发文管理实体
 */
public class GzpublishfileEntity implements IBpmProjectEntity {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 发文主题
	 */
	private String fwzt;
	
	
	/**
	 * 发文说明
	 */
	private String fwsm;
	
	/**
	 * 拟办意见
	 */
	private String nbyj;
	
	
	public String getNbyj() {
		return nbyj;
	}

	public void setNbyj(String nbyj) {
		this.nbyj = nbyj;
	}
	
	/**
	 * 负责人部门名称
	 */
	@DictionaryField(dictionaryName = "p_department" ,toFieldName = "fzrbmmcName")
	private String fzrbmmc;
	
	public String getFzrbmmc() {
		return fzrbmmc;
	}

	public void setFzrbmmc(String fzrbmmc) {
		this.fzrbmmc = fzrbmmc;
	}
	
	private String fzrbmmcName;
	
	public String getFzrbmmcName() {
		return fzrbmmcName;
	}

	public void setFzrbmmcName(String fzrbmmcName) {
		this.fzrbmmcName = fzrbmmcName;
	}
	
	/**
	 * 创建人ID
	 */
	private String cjrid;
	
	
	/**
	 * 创建人名称
	 */
	private String cjrmc;
	
	/**
	 * 创建时间
	 */
	private java.util.Date cjsj;
	
	
	/**
	 * 附件
	 */
	private String fj;
	
	
	/**
	 * 部门ID
	 */
	private String bmid;
	
	/**
	 * 分管局领导（用来区分姚局分管项目，还是王局分管项目）
	 */
	private String fgjld;
	
	public String getFgjld() {
		return fgjld;
	}

	public void setFgjld(String fgjld) {
		this.fgjld = fgjld;
	}

	/**
	 * 部门名称
	 */
	@DictionaryField(dictionaryName = "p_department" ,toFieldName = "bmmcName")
	private String bmmc;
	
	private String bmmcName;
	
	
	public String getBmmcName() {
		return bmmcName;
	}

	public void setBmmcName(String bmmcName) {
		this.bmmcName = bmmcName;
	}
	
	/**
	 * 办理情况
	 */
	@DictionaryField(dictionaryName = "GZ_GWCLQK" ,toFieldName = "blqkName")
	private String blqk;
	
	/**
	 * 办理情况
	 */
	private String blqkName;
	
	public String getBlqk() {
		return blqk;
	}

	public void setBlqk(String blqk) {
		this.blqk = blqk;
	}

	public String getBlqkName() {
		return blqkName;
	}

	public void setBlqkName(String blqkName) {
		this.blqkName = blqkName;
	}
	/**
	 * 收件人ID
	 */
	private String sjrid;
	
	
	/**
	 * 收件人名称
	 */
	private String sjrmc;
	
	
	/**
	 * 紧急等级
	 */
	@DictionaryField(dictionaryName = "p_jjdj" ,toFieldName = "jjdjName")
	private String jjdj;
	
	
	/**
	 * 紧急等级
	 */
	private String jjdjName;
	
	
	public String getJjdjName() {
		return jjdjName;
	}

	public void setJjdjName(String jjdjName) {
		this.jjdjName = jjdjName;
	}

	public String getJjdj() {
		return jjdj;
	}

	public void setJjdj(String jjdj) {
		this.jjdj = jjdj;
	}
	/**
	 * 收文主题
	 */
	private Set<GzreceivefileEntity> gzreceivefile=new LinkedHashSet<GzreceivefileEntity>();
	
	/**
	 * 收文主题
	 * @return
	 */
	public Set<GzreceivefileEntity> getGzreceivefile() {
		return gzreceivefile;
	}

	/**
	 * 收文主题
	 * @return
	 */
	public void setGzreceivefile(Set<GzreceivefileEntity> gzreceivefile) {
		this.gzreceivefile = gzreceivefile;
	}
	
	public String getSjrid() {
		return sjrid;
	}

	public void setSjrid(String sjrid) {
		this.sjrid = sjrid;
	}

	public String getSjrmc() {
		return sjrmc;
	}

	public void setSjrmc(String sjrmc) {
		this.sjrmc = sjrmc;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 发文主题
	 */
	public String getFwzt() {
		return fwzt;
	}

	/**
	 * 发文主题
	 */
	public void setFwzt(String value) {
		this.fwzt = value;
	}
	/**
	 * 发文说明
	 */
	public String getFwsm() {
		return fwsm;
	}

	/**
	 * 发文说明
	 */
	public void setFwsm(String value) {
		this.fwsm = value;
	}
	/**
	 * 创建人ID
	 */
	public String getCjrid() {
		return cjrid;
	}

	/**
	 * 创建人ID
	 */
	public void setCjrid(String value) {
		this.cjrid = value;
	}
	/**
	 * 创建人名称
	 */
	public String getCjrmc() {
		return cjrmc;
	}

	/**
	 * 创建人名称
	 */
	public void setCjrmc(String value) {
		this.cjrmc = value;
	}
	/**
	 * 创建时间
	 */
	public java.util.Date getCjsj() {
		return cjsj;
	}

	/**
	 * 创建时间
	 */
	public void setCjsj(java.util.Date value) {
		this.cjsj = value;
	}
	/**
	 * 附件
	 */
	public String getFj() {
		return fj;
	}

	/**
	 * 附件
	 */
	public void setFj(String value) {
		this.fj = value;
	}
	/**
	 * 部门ID
	 */
	public String getBmid() {
		return bmid;
	}

	/**
	 * 部门ID
	 */
	public void setBmid(String value) {
		this.bmid = value;
	}
	/**
	 * 部门名称
	 */
	public String getBmmc() {
		return bmmc;
	}

	/**
	 * 部门名称
	 */
	public void setBmmc(String value) {
		this.bmmc = value;
	}

	@Override
	public String getProjectname() {
		if(this.jjdjName!=null && !"".equals(this.jjdjName)) {
			return this.fwzt+"("+this.jjdjName+")";
		}else {
			return this.fwzt;
		}
	}
	
	private String sexeid;

	@Override
	public String getSexeid() {
		return this.sexeid;
	}

	/**
	 * 流程实例主键
	 */
	@Override
	public void setSexeid(String sexeid) {
		this.sexeid=sexeid;
	}
	
	/**
	 * 主要负责人
	 */
	private String zyfzrid;
	
	/**
	 * 主要负责人
	 */
	private String zyfzrname;


	public String getZyfzrid() {
		return zyfzrid;
	}

	public void setZyfzrid(String zyfzrid) {
		this.zyfzrid = zyfzrid;
	}

	public String getZyfzrname() {
		return zyfzrname;
	}

	public void setZyfzrname(String zyfzrname) {
		this.zyfzrname = zyfzrname;
	}
	
	/**
	 * 来文单位
	 */
	public String lwdw;
	
	/**
	 * 来文编号
	 */
	public String lwbh;
	
	/**
	 * 收文时间
	 */
	public Date swsj;
	
	/**
	 * 办理结果
	 */
	public String bljg;

	public String getLwdw() {
		return lwdw;
	}

	public void setLwdw(String lwdw) {
		this.lwdw = lwdw;
	}

	public String getLwbh() {
		return lwbh;
	}

	public void setLwbh(String lwbh) {
		this.lwbh = lwbh;
	}

	public Date getSwsj() {
		return swsj;
	}

	public void setSwsj(Date swsj) {
		this.swsj = swsj;
	}

	public String getBljg() {
		return bljg;
	}

	public void setBljg(String bljg) {
		this.bljg = bljg;
	}
}