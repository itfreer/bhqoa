package com.itfreer.gztaskview.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gztaskview.entity.GztaskviewEntity;

/**
 * 定义收文视图接口
 */
public interface GztaskviewService extends BaseService<GztaskviewEntity> {
	
}
