package com.itfreer.gzprojectmonthly.entity;

import java.io.Serializable;

import javax.persistence.Column;

/**
 * 定义项目月报实体
 */
public class GzprojectmonthlyEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	/**
	 * I_VERSION
	 */
	private Integer iversion;
	
	
	/**
	 * 项目编号
	 */
	private String sxmbh;
	
	/**
	 * 项目名称
	 */
	private String sxmmc;
	
	/**
	 * 进度月报编号
	 */
	private Integer ijdybbh;
	
	
	/**
	 * 月度（年度）
	 */
	private String syd;
	
	
	/**
	 * 项目经理
	 */
	private String sxmjl;
	
	
	/**
	 * 项目副经理
	 */
	private String sxmfjl;
	
	
	/**
	 * 技术负责人
	 */
	private String sjsfzr;
	
	/**
	 * 项目人员数
	 */
	private Integer ixmrys;
	
	
	/**
	 * 作业方法
	 */
	private String szyff;
	
	/**
	 * 进行测绘登记时间
	 */
	private java.util.Date ddjsj;
	
	
	/**
	 * 进行测绘登记地点
	 */
	private String sdjdd;
	
	/**
	 * 开始时间
	 */
	private java.util.Date dkssj;
	
	/**
	 * 结束时间
	 */
	private java.util.Date djssj;
	
	
	/**
	 * 问题与措施
	 */
	private String swd;
	
	
	/**
	 * 其他
	 */
	private String sqt;
	
	
	/**
	 * 提交人
	 */
	private String stjr;
	
	/**
	 * 提交时间
	 */
	private java.util.Date dtjsj;
	
	
	/**
	 * 项目id
	 */
	private String xmid;
	
	public String getXmid() {
		return xmid;
	}

	public void setXmid(String xmid) {
		this.xmid = xmid;
	}
	
	/**
	 * 项目进度
	 */
	private Double xmjd;
	
	
	
	public Double getXmjd() {
		return xmjd;
	}

	public void setXmjd(Double xmjd) {
		this.xmjd = xmjd;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * I_VERSION
	 */
	public Integer getIversion() {
		return iversion;
	}

	/**
	 * I_VERSION
	 */
	public void setIversion(Integer value) {
		this.iversion = value;
	}
	/**
	 * 项目编号
	 */
	public String getSxmbh() {
		return sxmbh;
	}

	/**
	 * 项目编号
	 */
	public void setSxmbh(String value) {
		this.sxmbh = value;
	}
	/**
	 * 项目名称
	 */
	public String getSxmmc() {
		return sxmmc;
	}

	/**
	 * 项目名称
	 */
	public void setSxmmc(String value) {
		this.sxmmc = value;
	}
	/**
	 * 进度月报编号
	 */
	public Integer getIjdybbh() {
		return ijdybbh;
	}

	/**
	 * 进度月报编号
	 */
	public void setIjdybbh(Integer value) {
		this.ijdybbh = value;
	}
	/**
	 * 月度（年度）
	 */
	public String getSyd() {
		return syd;
	}

	/**
	 * 月度（年度）
	 */
	public void setSyd(String value) {
		this.syd = value;
	}
	/**
	 * 项目经理
	 */
	public String getSxmjl() {
		return sxmjl;
	}

	/**
	 * 项目经理
	 */
	public void setSxmjl(String value) {
		this.sxmjl = value;
	}
	/**
	 * 项目副经理
	 */
	public String getSxmfjl() {
		return sxmfjl;
	}

	/**
	 * 项目副经理
	 */
	public void setSxmfjl(String value) {
		this.sxmfjl = value;
	}
	/**
	 * 技术负责人
	 */
	public String getSjsfzr() {
		return sjsfzr;
	}

	/**
	 * 技术负责人
	 */
	public void setSjsfzr(String value) {
		this.sjsfzr = value;
	}
	/**
	 * 项目人员数
	 */
	public Integer getIxmrys() {
		return ixmrys;
	}

	/**
	 * 项目人员数
	 */
	public void setIxmrys(Integer value) {
		this.ixmrys = value;
	}
	/**
	 * 作业方法
	 */
	public String getSzyff() {
		return szyff;
	}

	/**
	 * 作业方法
	 */
	public void setSzyff(String value) {
		this.szyff = value;
	}
	/**
	 * 进行测绘登记时间
	 */
	public java.util.Date getDdjsj() {
		return ddjsj;
	}

	/**
	 * 进行测绘登记时间
	 */
	public void setDdjsj(java.util.Date value) {
		this.ddjsj = value;
	}
	/**
	 * 进行测绘登记地点
	 */
	public String getSdjdd() {
		return sdjdd;
	}

	/**
	 * 进行测绘登记地点
	 */
	public void setSdjdd(String value) {
		this.sdjdd = value;
	}
	/**
	 * 开始时间
	 */
	public java.util.Date getDkssj() {
		return dkssj;
	}

	/**
	 * 开始时间
	 */
	public void setDkssj(java.util.Date value) {
		this.dkssj = value;
	}
	/**
	 * 结束时间
	 */
	public java.util.Date getDjssj() {
		return djssj;
	}

	/**
	 * 结束时间
	 */
	public void setDjssj(java.util.Date value) {
		this.djssj = value;
	}
	/**
	 * 问题与措施
	 */
	public String getSwd() {
		return swd;
	}

	/**
	 * 问题与措施
	 */
	public void setSwd(String value) {
		this.swd = value;
	}
	/**
	 * 其他
	 */
	public String getSqt() {
		return sqt;
	}

	/**
	 * 其他
	 */
	public void setSqt(String value) {
		this.sqt = value;
	}
	/**
	 * 提交人
	 */
	public String getStjr() {
		return stjr;
	}

	/**
	 * 提交人
	 */
	public void setStjr(String value) {
		this.stjr = value;
	}
	/**
	 * 提交时间
	 */
	public java.util.Date getDtjsj() {
		return dtjsj;
	}

	/**
	 * 提交时间
	 */
	public void setDtjsj(java.util.Date value) {
		this.dtjsj = value;
	}
}