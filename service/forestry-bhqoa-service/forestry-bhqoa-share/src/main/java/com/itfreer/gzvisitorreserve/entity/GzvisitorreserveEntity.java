package com.itfreer.gzvisitorreserve.entity;

import java.io.Serializable;

import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义访客预约管理实体
 */
public class GzvisitorreserveEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 来访单位
	 */
	private String lfdw;
	
	
	/**
	 * 来访主题
	 */
	private String lfzt;
	
	
	/**
	 * 来访人员
	 */
	private String lfry;
	
	
	/**
	 * 来访人数
	 */
	private String lfrs;
	
	/**
	 * 到访时间
	 */
	private java.util.Date dfsj;
	
	
	/**
	 * 是否需要安排住宿
	 */
	@DictionaryField(dictionaryName="p_yesorno", toFieldName="sfxyapzsName")
	private String sfxyapzs;
	
	
	/**
	 * 是否需要安排住宿
	 */
	private String sfxyapzsName;
	
	
	public String getSfxyapzsName() {
		return sfxyapzsName;
	}

	public void setSfxyapzsName(String sfxyapzsName) {
		this.sfxyapzsName = sfxyapzsName;
	}
	
	
	/**
	 * 男性人数
	 */
	private String nxrs;
	
	
	/**
	 * 女性人数
	 */
	private String wxrs;
	
	/**
	 * 到访结束时间
	 */
	private java.util.Date dfjssj;
	
	
	/**
	 * 创建人ID
	 */
	private String crjid;
	
	
	/**
	 * 创建人名称
	 */
	private String cjrmc;
	
	
	/**
	 * 领队联系电话
	 */
	private String tel;
	
	
	/**
	 * 考察内容
	 */
	private String kcnr;
	
	
	/**
	 * 接待负责人ID
	 */
	private String jdfzrid;
	
	
	/**
	 * 接待负责人名称
	 */
	private String jdfzrmc;
	
	
	/**
	 * 参与人
	 */
	private String cyr;
	
	
	/**
	 * 接待部门ID
	 */
	private String jdbmid;
	
	
	/**
	 * 接待部门名称
	 */
	@DictionaryField(dictionaryName="p_department", toFieldName="jdbmmcName")
	private String jdbmmc;
	
	
	/**
	 * 接待部门名称
	 */
	private String jdbmmcName;
	
	public String getJdbmmcName() {
		return jdbmmcName;
	}

	public void setJdbmmcName(String jdbmmcName) {
		this.jdbmmcName = jdbmmcName;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 来访单位
	 */
	public String getLfdw() {
		return lfdw;
	}

	/**
	 * 来访单位
	 */
	public void setLfdw(String value) {
		this.lfdw = value;
	}
	/**
	 * 来访主题
	 */
	public String getLfzt() {
		return lfzt;
	}

	/**
	 * 来访主题
	 */
	public void setLfzt(String value) {
		this.lfzt = value;
	}
	/**
	 * 来访人员
	 */
	public String getLfry() {
		return lfry;
	}

	/**
	 * 来访人员
	 */
	public void setLfry(String value) {
		this.lfry = value;
	}
	/**
	 * 来访人数
	 */
	public String getLfrs() {
		return lfrs;
	}

	/**
	 * 来访人数
	 */
	public void setLfrs(String value) {
		this.lfrs = value;
	}
	/**
	 * 到访时间
	 */
	public java.util.Date getDfsj() {
		return dfsj;
	}

	/**
	 * 到访时间
	 */
	public void setDfsj(java.util.Date value) {
		this.dfsj = value;
	}
	/**
	 * 是否需要安排住宿
	 */
	public String getSfxyapzs() {
		return sfxyapzs;
	}

	/**
	 * 是否需要安排住宿
	 */
	public void setSfxyapzs(String value) {
		this.sfxyapzs = value;
	}
	/**
	 * 男性人数
	 */
	public String getNxrs() {
		return nxrs;
	}

	/**
	 * 男性人数
	 */
	public void setNxrs(String value) {
		this.nxrs = value;
	}
	/**
	 * 女性人数
	 */
	public String getWxrs() {
		return wxrs;
	}

	/**
	 * 女性人数
	 */
	public void setWxrs(String value) {
		this.wxrs = value;
	}
	/**
	 * 到访结束时间
	 */
	public java.util.Date getDfjssj() {
		return dfjssj;
	}

	/**
	 * 到访结束时间
	 */
	public void setDfjssj(java.util.Date value) {
		this.dfjssj = value;
	}
	/**
	 * 创建人ID
	 */
	public String getCrjid() {
		return crjid;
	}

	/**
	 * 创建人ID
	 */
	public void setCrjid(String value) {
		this.crjid = value;
	}
	/**
	 * 创建人名称
	 */
	public String getCjrmc() {
		return cjrmc;
	}

	/**
	 * 创建人名称
	 */
	public void setCjrmc(String value) {
		this.cjrmc = value;
	}
	/**
	 * 领队联系电话
	 */
	public String getTel() {
		return tel;
	}

	/**
	 * 领队联系电话
	 */
	public void setTel(String value) {
		this.tel = value;
	}
	/**
	 * 考察内容
	 */
	public String getKcnr() {
		return kcnr;
	}

	/**
	 * 考察内容
	 */
	public void setKcnr(String value) {
		this.kcnr = value;
	}
	/**
	 * 接待负责人ID
	 */
	public String getJdfzrid() {
		return jdfzrid;
	}

	/**
	 * 接待负责人ID
	 */
	public void setJdfzrid(String value) {
		this.jdfzrid = value;
	}
	/**
	 * 接待负责人名称
	 */
	public String getJdfzrmc() {
		return jdfzrmc;
	}

	/**
	 * 接待负责人名称
	 */
	public void setJdfzrmc(String value) {
		this.jdfzrmc = value;
	}
	/**
	 * 参与人
	 */
	public String getCyr() {
		return cyr;
	}

	/**
	 * 参与人
	 */
	public void setCyr(String value) {
		this.cyr = value;
	}
	/**
	 * 接待部门ID
	 */
	public String getJdbmid() {
		return jdbmid;
	}

	/**
	 * 接待部门ID
	 */
	public void setJdbmid(String value) {
		this.jdbmid = value;
	}
	/**
	 * 接待部门名称
	 */
	public String getJdbmmc() {
		return jdbmmc;
	}

	/**
	 * 接待部门名称
	 */
	public void setJdbmmc(String value) {
		this.jdbmmc = value;
	}
}