package com.itfreer.gztaskview.entity;

import java.io.Serializable;

import com.itfreer.form.dictionary.reflect.DictionaryField;

/**
 * 定义收文视图实体
 */
public class GztaskviewEntity implements Serializable {
	
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * ID
	 */
	private String id;

	@DictionaryField(dictionaryName = "p_department", toFieldName = "bmmcName")
	private String bmmc;

	private String bmmcName;

	public String getBmmcName() {
		return bmmcName;
	}

	public void setBmmcName(String bmmcName) {
		this.bmmcName = bmmcName;
	}

	/**
	 * 事项说明
	 */
	private String sxsm;

	/**
	 * 完成时间
	 */
	private java.util.Date wcsj;

	public java.util.Date getWcsj() {
		return wcsj;
	}

	public void setWcsj(java.util.Date wcsj) {
		this.wcsj = wcsj;
	}

	/**
	 * 创建人ID
	 */
	private String cjrid;

	/**
	 * 创建人名称
	 */
	private String cjrmc;

	/**
	 * 创建时间
	 */
	private java.util.Date cjsj;

	/**
	 * 项目名称
	 */
	private String xmmc;

	/**
	 * 发步ID
	 */
	private String fwid;

	/**
	 * 接收人ID
	 */
	private String jsrid;

	/**
	 * 接收人名称
	 */
	private String jsrmc;

	/**
	 * 接收人机构
	 */
	private String jsrjg;

	/**
	 * 主题
	 */
	private String zhuti;

	public String getZhuti() {
		return zhuti;
	}

	public void setZhuti(String zhuti) {
		this.zhuti = zhuti;
	}

	/**
	 * 确认时间
	 */
	private java.util.Date qrsj;

	/**
	 * 是否已确认
	 */
	private String sfqr;

	/**
	 * 结束时间
	 */
	private java.util.Date jssj;

	/**
	 * 开始时间
	 */
	private java.util.Date kssj;

	/**
	 * 组织机构名称
	 */
	@DictionaryField(dictionaryName = "p_organization", toFieldName = "zzjgmcName")
	private String zzjgmc;

	private String zzjgmcName;

	public String getZzjgmcName() {
		return zzjgmcName;
	}

	public void setZzjgmcName(String zzjgmcName) {
		this.zzjgmcName = zzjgmcName;
	}

	/**
	 * 状态
	 */
	@DictionaryField(dictionaryName = "p_rwzt", toFieldName = "ztName")
	private String zt;

	/**
	 * 状态
	 */
	private String ztName;

	public String getZtName() {
		return ztName;
	}

	public void setZtName(String ztName) {
		this.ztName = ztName;
	}

	/**
	 * 单位名称
	 */
	private String dwmc;

	/**
	 * 负责人
	 */
	private String fzr;

	private String fzrmc;

	public String getFzrmc() {
		return fzrmc;
	}

	public void setFzrmc(String fzrmc) {
		this.fzrmc = fzrmc;
	}

	public String getDwmc() {
		return dwmc;
	}

	public void setDwmc(String dwmc) {
		this.dwmc = dwmc;
	}

	public String getFzr() {
		return fzr;
	}

	public void setFzr(String fzr) {
		this.fzr = fzr;
	}

	/**
	 * ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * ID
	 */
	public void setId(String value) {
		this.id = value;
	}

	/**
	 * 创建人部门名称
	 */
	public String getBmmc() {
		return bmmc;
	}

	/**
	 * 创建人部门名称
	 */
	public void setBmmc(String value) {
		this.bmmc = value;
	}

	/**
	 * 事项说明
	 */
	public String getSxsm() {
		return sxsm;
	}

	/**
	 * 事项说明
	 */
	public void setSxsm(String value) {
		this.sxsm = value;
	}

	/**
	 * 创建人ID
	 */
	public String getCjrid() {
		return cjrid;
	}

	/**
	 * 创建人ID
	 */
	public void setCjrid(String value) {
		this.cjrid = value;
	}

	/**
	 * 创建人名称
	 */
	public String getCjrmc() {
		return cjrmc;
	}

	/**
	 * 创建人名称
	 */
	public void setCjrmc(String value) {
		this.cjrmc = value;
	}

	/**
	 * 创建时间
	 */
	public java.util.Date getCjsj() {
		return cjsj;
	}

	/**
	 * 创建时间
	 */
	public void setCjsj(java.util.Date value) {
		this.cjsj = value;
	}

	/**
	 * 项目名称
	 */
	public String getXmmc() {
		return xmmc;
	}

	/**
	 * 项目名称
	 */
	public void setXmmc(String value) {
		this.xmmc = value;
	}

	/**
	 * 发步ID
	 */
	public String getFwid() {
		return fwid;
	}

	/**
	 * 发步ID
	 */
	public void setFwid(String value) {
		this.fwid = value;
	}

	/**
	 * 接收人ID
	 */
	public String getJsrid() {
		return jsrid;
	}

	/**
	 * 接收人ID
	 */
	public void setJsrid(String value) {
		this.jsrid = value;
	}

	/**
	 * 接收人名称
	 */
	public String getJsrmc() {
		return jsrmc;
	}

	/**
	 * 接收人名称
	 */
	public void setJsrmc(String value) {
		this.jsrmc = value;
	}

	/**
	 * 接收人机构
	 */
	public String getJsrjg() {
		return jsrjg;
	}

	/**
	 * 接收人机构
	 */
	public void setJsrjg(String value) {
		this.jsrjg = value;
	}

	/**
	 * 确认时间
	 */
	public java.util.Date getQrsj() {
		return qrsj;
	}

	/**
	 * 确认时间
	 */
	public void setQrsj(java.util.Date value) {
		this.qrsj = value;
	}

	/**
	 * 是否已确认
	 */
	public String getSfqr() {
		return sfqr;
	}

	/**
	 * 是否已确认
	 */
	public void setSfqr(String value) {
		this.sfqr = value;
	}

	/**
	 * 结束时间
	 */
	public java.util.Date getJssj() {
		return jssj;
	}

	/**
	 * 结束时间
	 */
	public void setJssj(java.util.Date value) {
		this.jssj = value;
	}

	/**
	 * 开始时间
	 */
	public java.util.Date getKssj() {
		return kssj;
	}

	/**
	 * 开始时间
	 */
	public void setKssj(java.util.Date value) {
		this.kssj = value;
	}

	/**
	 * 组织机构名称
	 */
	public String getZzjgmc() {
		return zzjgmc;
	}

	/**
	 * 组织机构名称
	 */
	public void setZzjgmc(String value) {
		this.zzjgmc = value;
	}

	/**
	 * 状态
	 */
	public String getZt() {
		return zt;
	}

	/**
	 * 状态
	 */
	public void setZt(String value) {
		this.zt = value;
	}
}