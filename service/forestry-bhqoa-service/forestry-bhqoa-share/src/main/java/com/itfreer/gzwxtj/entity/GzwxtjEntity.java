package com.itfreer.gzwxtj.entity;

import java.io.Serializable;

/**
 * 定义维修统计实体
 */
public class GzwxtjEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	private String id;
	
	
	/**
	 * 维修年度
	 */
	private String swxnd;
	
	
	/**
	 * 维修月份
	 */
	private String swxyf;
	
	
	/**
	 * 维修设备名称
	 */
	private String swxsbmc;
	
	
	/**
	 * 维修设备数量
	 */
	private String swxsbsl;
	
	
	/**
	 * 维修次数
	 */
	private String swxcs;
	
	
	/**
	 * 维修费用
	 */
	private String swxfy;
	
	
	/**
	 * 负责人
	 */
	private String sfzr;
	
	
	/**
	 * 联系方式
	 */
	private String slxfs;
	
	
	/**
	 * 电子发票
	 */
	private String sdzfp;
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 维修年度
	 */
	public String getSwxnd() {
		return swxnd;
	}

	/**
	 * 维修年度
	 */
	public void setSwxnd(String value) {
		this.swxnd = value;
	}
	/**
	 * 维修月份
	 */
	public String getSwxyf() {
		return swxyf;
	}

	/**
	 * 维修月份
	 */
	public void setSwxyf(String value) {
		this.swxyf = value;
	}
	/**
	 * 维修设备名称
	 */
	public String getSwxsbmc() {
		return swxsbmc;
	}

	/**
	 * 维修设备名称
	 */
	public void setSwxsbmc(String value) {
		this.swxsbmc = value;
	}
	/**
	 * 维修设备数量
	 */
	public String getSwxsbsl() {
		return swxsbsl;
	}

	/**
	 * 维修设备数量
	 */
	public void setSwxsbsl(String value) {
		this.swxsbsl = value;
	}
	/**
	 * 维修次数
	 */
	public String getSwxcs() {
		return swxcs;
	}

	/**
	 * 维修次数
	 */
	public void setSwxcs(String value) {
		this.swxcs = value;
	}
	/**
	 * 维修费用
	 */
	public String getSwxfy() {
		return swxfy;
	}

	/**
	 * 维修费用
	 */
	public void setSwxfy(String value) {
		this.swxfy = value;
	}
	/**
	 * 负责人
	 */
	public String getSfzr() {
		return sfzr;
	}

	/**
	 * 负责人
	 */
	public void setSfzr(String value) {
		this.sfzr = value;
	}
	/**
	 * 联系方式
	 */
	public String getSlxfs() {
		return slxfs;
	}

	/**
	 * 联系方式
	 */
	public void setSlxfs(String value) {
		this.slxfs = value;
	}
	/**
	 * 电子发票
	 */
	public String getSdzfp() {
		return sdzfp;
	}

	/**
	 * 电子发票
	 */
	public void setSdzfp(String value) {
		this.sdzfp = value;
	}
}