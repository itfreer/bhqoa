package com.itfreer.gzvisitorreception.service;

import com.itfreer.form.api.BaseService;

import com.itfreer.gzvisitorreception.entity.GzvisitorreceptionEntity;

/**
 * 定义访问接待管理接口
 */
public interface GzvisitorreceptionService extends BaseService<GzvisitorreceptionEntity> {
	
}
