package com.itfreer.gzqdglqddd.entity;

import java.io.Serializable;

/**
 * 定义签到地点实体
 */
public class GzqdglqdddEntity  implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;
	
	/**
	 * ID
	 */
	private String id;
	
	
	/**
	 * 地点坐标x
	 */
	private String kqzbx;
	
	/**
	 * 地点坐标y
	 */
	private String kqzby;
	
	
	/**
	 * 考勤地点
	 */
	private String kqdd;
	
	
	/**
	 * 创建时间
	 */
	private java.util.Date cjsj;
	
	/**
	 * 考勤范围
	 */
	private Integer kqfw;
	
	
	
	public Integer getKqfw() {
		return kqfw;
	}

	public void setKqfw(Integer kqfw) {
		this.kqfw = kqfw;
	}

	/**
	 * 备注
	 */
	private String bz;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKqzbx() {
		return kqzbx;
	}

	public void setKqzbx(String kqzbx) {
		this.kqzbx = kqzbx;
	}

	public String getKqzby() {
		return kqzby;
	}

	public void setKqzby(String kqzby) {
		this.kqzby = kqzby;
	}

	public String getKqdd() {
		return kqdd;
	}

	public void setKqdd(String kqdd) {
		this.kqdd = kqdd;
	}

	public java.util.Date getCjsj() {
		return cjsj;
	}

	public void setCjsj(java.util.Date cjsj) {
		this.cjsj = cjsj;
	}

	public String getBz() {
		return bz;
	}

	public void setBz(String bz) {
		this.bz = bz;
	}
	
	
	
}