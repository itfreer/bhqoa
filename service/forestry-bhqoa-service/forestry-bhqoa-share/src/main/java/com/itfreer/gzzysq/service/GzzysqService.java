package com.itfreer.gzzysq.service;

import com.itfreer.bpm.flow.api.IBaseWorkFlowService;
import com.itfreer.gzzysq.entity.GzzysqEntity;

/**
 * 定义资源申请接口
 */
public interface GzzysqService extends IBaseWorkFlowService<GzzysqEntity> {
	
}
