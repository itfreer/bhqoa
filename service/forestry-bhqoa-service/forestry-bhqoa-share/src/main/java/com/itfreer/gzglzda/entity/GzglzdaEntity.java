package com.itfreer.gzglzda.entity;

import java.io.Serializable;

/**
 * 定义管理站档案实体
 */
public class GzglzdaEntity implements Serializable {
	private static final long serialVersionUID = -8443497714033818376L;

	/**
	 * 流程ID
	 */
	private String bpmkey;
	
	public String getBpmkey() {
		return bpmkey;
	}

	public void setBpmkey(String bpmkey) {
		this.bpmkey = bpmkey;
	}
	/**
	 * 唯一编号
	 */
	private String id;
	
	
	/**
	 * 管理站名称
	 */
	private String sglzmc;
	
	
	/**
	 * 管理站地址
	 */
	private String sglzdz;
	
	
	/**
	 * 负责人
	 */
	private String sfzr;
	
	
	/**
	 * 负责人联系方式
	 */
	private String slxff;
	

	/**
	 * 唯一编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 唯一编号
	 */
	public void setId(String value) {
		this.id = value;
	}
	/**
	 * 管理站名称
	 */
	public String getSglzmc() {
		return sglzmc;
	}

	/**
	 * 管理站名称
	 */
	public void setSglzmc(String value) {
		this.sglzmc = value;
	}
	/**
	 * 管理站地址
	 */
	public String getSglzdz() {
		return sglzdz;
	}

	/**
	 * 管理站地址
	 */
	public void setSglzdz(String value) {
		this.sglzdz = value;
	}
	/**
	 * 负责人
	 */
	public String getSfzr() {
		return sfzr;
	}

	/**
	 * 负责人
	 */
	public void setSfzr(String value) {
		this.sfzr = value;
	}
	/**
	 * 负责人联系方式
	 */
	public String getSlxff() {
		return slxff;
	}

	/**
	 * 负责人联系方式
	 */
	public void setSlxff(String value) {
		this.slxff = value;
	}
}