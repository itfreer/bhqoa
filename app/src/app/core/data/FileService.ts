import { Injectable } from '@angular/core';
import { BaseService } from './BaseService';

// 文件处理服务类
@Injectable()
export class FileService {

	// 下载路径
	downloadUrl:string = '/file/plupload/download';

	constructor(private baseService:BaseService) { }

	// 转换下载文件
	getFiles(file:string):any[]{
		// plupload:3tvb3mb.png:zs-banner1.png|plupload:20c.png:zs-banner2.png
		let result:any[]=[];

		let files:string[] = file.split('|');
		files.forEach(e=>{
			let fp:string[] = e.split(':');
			// let dfu:string = this.baseService.baseUrl + this.downloadUrl + "?bucket="+fp[0]+"&key="+fp[1]+"&name="+encodeURIComponent(fp[2]);
			let dfu:string = this.baseService.fileUrl + "/"+fp[0]+"/"+fp[1];
			result.push({url:dfu,name:fp[0]});
		});
		return result;
	}

	// 转换下载文件
	// 1.只转换第一个文件；2.返回tomcat访问路径
	getFile(file:string):string{
		// plupload:3tvb3mb.png:zs-banner1.png|plupload:20c.png:zs-banner2.png
		let result:string=null;
		let files:string[] = file.split('|');
		if(files.length>0){
			let fp:string[] = files[0].split(':');
			result = this.baseService.fileUrl + "/"+fp[0]+"/"+fp[1];
		}
		return result;
	}
}