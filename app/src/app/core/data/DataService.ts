import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { BaseService } from './BaseService';

// 通用数据访问服务类
@Injectable()
export class DataService {

	constructor(private http:HttpClient,
		private baseService:BaseService) { }

	// 获取查询数据列表
	getEntitys(sn:string, fields:string,
		where:any, order:any,
		pageSize:number, pageIndex:number):Observable<any[]> {
		let args = {
			queryFields : fields,
			where : where,
			order : order,
			pageSize : pageSize,
			pageIndex : pageIndex
		};
		args.where = this.baseService.toJSONStr(args.where);
		args.order = this.baseService.toJSONStr(args.order);
		let as = this.baseService.toJSONStr(args);
		let url:string = this.baseService.baseUrl + "/" + sn + "/getEntitys";
		return this.http.post<any[]>(url, as, {headers:this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any[]>([])));
	}

	// 获取查询数据列表
	getEntitysByKey(sn:string, id:string):Observable<any[]>{
		let url:string = this.baseService.baseUrl + "/" + sn + "/getEntitys/"+id;
		return this.http.post<any[]>(url, '', {headers: this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any[]>([])));
	}

	// 获取数据总记录数
	getRowCounts(sn:string, where:any):Observable<number>{
		var args = { where : where };
		args.where = this.baseService.toJSONStr(args.where);
		let as = this.baseService.toJSONStr(args);
		let url:string = this.baseService.baseUrl + "/" + sn + "/getRowCounts";
		return this.http.post<number>(url, as, {headers: this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<number>(0)));
	}

	// 获取单个记录
	getEntity(sn:string, id:string):Observable<any>{
		let url:string = this.baseService.baseUrl + "/" + sn + "/getEntity/"+id;
		return this.http.post<any>(url, '', {headers: this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	}

	// 添加
	add(sn:string,row:any):Observable<any>{
		var args = { entity : row };
		args.entity = this.baseService.toJSONStr(args.entity);
		let as = this.baseService.toJSONStr(args);
		let url:string = this.baseService.baseUrl + "/" + sn + "/add";
		return this.http.post<any>(url, as, {headers: this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	}

	// 更新
	update(sn:string,row:any):Observable<any>{
		var args = { entity : row };
		args.entity = this.baseService.toJSONStr(args.entity);
		let as = this.baseService.toJSONStr(args);
		let url:string = this.baseService.baseUrl + "/" + sn + "/update";
		return this.http.post<any>(url, as, {headers: this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	}

	// 删除单个记录
	delete(sn:string, id:string):Observable<any>{
		let url:string = this.baseService.baseUrl + "/" + sn + "/delete/" + id;
		return this.http.delete<any>(url, {headers: this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	}

	// 删除单个记录
	deletes(sn:string, ids:string):Observable<any>{
		let url:string = this.baseService.baseUrl + "/" + sn + "/deletes/" + ids;
		return this.http.delete<any>(url, {headers: this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	}

	/**
	 * 工作流中的查找工作操作
	 * @param sn 表单
	 * @param id 数据主键
	 * @param bpmKey 流程主键
	 * @param user 用户信息
	 */
	findWork(sn:string,datakey:string,bpmKey:string,user:any):Observable<any>{
		let url:string=this.baseService.baseUrl + "/" + sn + "/findWork";
		let args = {
			bpmKey : bpmKey,
			id : datakey,
			user : user
		};
		args.user = this.baseService.toJSONStr(args.user);
		let as = this.baseService.toJSONStr(args);
		return this.http.post<any>(url, as, {headers:this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	};

	/**
	 * 工作流中的保存功能实现
	 * @param sn 
	 * @param optsave 
	 * @param editRow 
	 * @param user 
	 */
	bpmSave(sn:string,optsave:any,editRow:any,user:any):Observable<any>{

		let url= this.baseService.baseUrl +'/'+sn+'/processSave';
		let args = {
			optsave : optsave,
			entity : editRow,
			user : user
		};
		args.optsave = this.baseService.toJSONStr(args.optsave);
		args.entity = this.baseService.toJSONStr(args.entity);
		args.user = this.baseService.toJSONStr(args.user);
		let as=this.baseService.toJSONStr(args);

		return this.http.post<any>(url, as, {headers:this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	};

	/**
	 * 获取流程中的下一个环节节点
	 * @param sn 
	 * @param bpmKey 
	 * @param entity 
	 * @param editHistory 
	 * @param user 
	 */
	getNextTask(sn:string,bpmKey:string,entity:any,editHistory:any,user:any):Observable<any>{
		let url= this.baseService.baseUrl +'/'+sn+'/getNextTask';
		let args = {
			bpmKey : bpmKey,
			entity : entity,
			editHistory:editHistory,
			user : user
		};
		args.editHistory = this.baseService.toJSONStr(args.editHistory);
		args.entity = this.baseService.toJSONStr(args.entity);
		args.user = this.baseService.toJSONStr(args.user);
		let as=this.baseService.toJSONStr(args);
		return this.http.post<any>(url, as, {headers:this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	};

	/**
	 * 流程提交
	 * @param sn 
	 * @param bpmKey 
	 * @param entity 
	 * @param editHistory 
	 * @param user 
	 */
	signTask(sn:string,opttj:any,entity:any,user:any):Observable<any>{
		let url= this.baseService.baseUrl +'/'+sn+'/signTask';
		let args = {
			entity : entity,
			opttj:opttj,
			user : user
		};
		args.opttj = this.baseService.toJSONStr(args.opttj);
		args.entity = this.baseService.toJSONStr(args.entity);
		args.user = this.baseService.toJSONStr(args.user);
		let as=this.baseService.toJSONStr(args);
		return this.http.post<any>(url, as, {headers:this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	};

	/**
	 * 获取流程中的退回环节
	 * @param sn 
	 * @param bpmKey 
	 * @param entity 
	 * @param editHistory 
	 * @param user 
	 */
	getBackTask(sn:string,bpmKey:string,entity:any,editHistory:any,user:any):Observable<any>{
		let url= this.baseService.baseUrl +'/'+sn+'/getBackTask';
		let args = {
			bpmKey : bpmKey,
			entity : entity,
			editHistory:editHistory,
			user : user
		};
		args.editHistory = this.baseService.toJSONStr(args.editHistory);
		args.entity = this.baseService.toJSONStr(args.entity);
		args.user = this.baseService.toJSONStr(args.user);
		let as=this.baseService.toJSONStr(args);
		return this.http.post<any>(url, as, {headers:this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	};

	/**
	 * 流程提交
	 * @param sn 
	 * @param bpmKey 
	 * @param entity 
	 * @param editHistory 
	 * @param user 
	 */
	sendBack(sn:string,optback:any,entity:any,user:any):Observable<any>{
		let url= this.baseService.baseUrl +'/'+sn+'/sendBack';
		let args = {
			entity : entity,
			optback:optback,
			user : user
		};
		args.optback = this.baseService.toJSONStr(args.optback);
		args.entity = this.baseService.toJSONStr(args.entity);
		args.user = this.baseService.toJSONStr(args.user);
		let as=this.baseService.toJSONStr(args);
		return this.http.post<any>(url, as, {headers:this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	};

	/**
	 * 读取字典子表的数据
	 * @param dicName 
	 * @param id 
	 * @param nv 
	 */
	getDictionaryChilds(dicName:string,id:string,nv:boolean):Observable<any>{
		let url = this.baseService.baseUrl+'/dictionarymanage/getChild/'+dicName+'/'+id+'/'+nv;
		return this.http.post<any>(url, null, {headers:this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	};
}