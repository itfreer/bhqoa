import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { BaseService } from '../data/BaseService';
import { CookieService } from 'ngx-cookie';

// 安全模块数据服务
@Injectable()
export class SecurityDataService {
	constructor(private http:HttpClient,
		private baseService:BaseService,
		private _cookieService:CookieService) { 
	}

	// 创建tgt
	loginAjax(username:string, password:string):Observable<any>{
		let url = this.baseService.baseUrl+'/v1/tickets?username='+username+'&password='+password;
		return this.http.post<any>(url, '', {headers: this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	}

	// 验证tgt
	ticketsValidateAjax(tgt:string):Observable<any>{
		let url = this.baseService.baseUrl+'/ticketsValidate/'+tgt;
		return this.http.post<any>(url, '', {headers: this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	}

	// 创建ST
	serviceTicketsAjax(tgt:string, service:string):Observable<any>{
		let url = this.baseService.baseUrl+'/v1/serviceTickets/'+tgt+'?service='+service;
		return this.http.post<any>(url, '', {headers: this.baseService.httpOptions(),withCredentials:true})
			.pipe();
	}

	// 删除TGT
	deleteTicketsAjax(tgt:string):Observable<any>{
		let url = this.baseService.baseUrl+'/v1/tickets/'+tgt;
		return this.http.delete<any>(url, {headers: this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	}

	//TODO  模拟登录，解决服务端登录问题
	mklogin(ticket:string):Observable<any>{
		let url = this.baseService.hostUrl+'/sso/mklogin?ticket='+ticket;
		return this.http.get<any>(url, {headers: this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	}

	// 退出登录
	loginOut():Observable<any>{

		let url = this.baseService.hostUrl+'/sso/logout?ticket='+this._cookieService.get("jwt");
		return this.http.get<any>(url, {headers: this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	}

	// 用户注册信息
	userRegister(ac:string,pw:string,rpw:string):Observable<any>{
		let url = this.baseService.baseUrl+'/power/userRegister/'+ac+'/'+pw+'/'+rpw;
		return this.http.post<any>(url, '', {headers:this.baseService.httpOptions()})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	}

	// 获取当前用户信息
	getUserInfo():Observable<any>{
		let url = this.baseService.baseUrl+'/currentuser/getUserInfo';
		return this.http.post<any>(url, '', {headers:this.baseService.httpOptions(),
			withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	}

	// 设置密码
	setPassword(op:string,np:string):Observable<any>{
		let url = this.baseService.baseUrl+'/currentuser/setPassword/'+op+'/'+np;
		return this.http.post<any>(url, '', {headers: this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any>({})));
	}
}