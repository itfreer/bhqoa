import { Injectable } from '@angular/core';
import { BaseService } from '../data/BaseService';
import { DataService } from '../data/DataService';

// 登录服务
@Injectable()
export class DictionaryService {

    public datas=[];
    constructor(
		  public dataService:DataService,
      public baseService:BaseService,
		) { 
    }

    getDatas(dicName:string,id:string,nv:boolean,suc?:Function,fai?:Function){
        this.dataService.getDictionaryChilds(dicName,id,nv).subscribe((data)=>{
			if(data ){
                this.datas=data;
				if(suc) suc();
			} else {
				if(fai) fai();
			}
		});
    }
}