import { Injectable,ChangeDetectorRef } from '@angular/core';
import { BaseService } from '../core/data/BaseService';
import { DataService } from '../core/data/DataService';

import { LoadingController } from 'ionic-angular';

// 基础服务类，提供基础路径
@Injectable()
export class FormObject {

    constructor(
		public dataService:DataService,
        public baseService:BaseService,
        public changeDetectorRef:ChangeDetectorRef,
        public loadingCtrl: LoadingController
		) { 
    }
    
    // 服务名称，如果为空，则默认为表单名称
    public sn :string= "";
    // 查询服务名称，如果为空，则默认为服务名称
    public  ssn:string= "";
    // 编辑服务名称，如果为空，则默认为服务名称
    public esn:string= "";
    /**
     * 表单的主健，只能为一个，一般为GUID字段
     */
    public keyField :string=  '';
    /**
     * 表单需要随机赋值的字段
     */
    public randomFields = [];
    /**
     * 查询字段
     */
    public queryFields :string=  '';
    /**
     * 新值时的默认值
     */
    public defaultObject :any= {};
    /**
     * 默认的查询值
     */
    public defaultQueryObject:any= {};
    /**
     * 查询变量
     */
    public queryObject :any= {};
     /**
     * 排序对象
     */
    public orderObject :any= {};
    /**
     * 当前数据列表
     */
    public datas = [];
    /**
     * 当前数据列表选择的行
     */
    public sRows = [];
    /**
     * 分页大小
     */
    public pageSize = 10;
    /**
     * 分页索引
     */
    public pageIndex = 1;
    /**
     * 是否计算列表查询下的总记录数
     */
    public computeRowCounts = true;
    /**
     * 页数
     */
    public pageCount = 1;
    /**
     * 分页对象列表(1,2,3)
     */
    public page= [1];
    /**
     * 总的条数
     */
    public rowsCount= 0;
    /**
     * 当前编辑对象
     */
    public editRow :any= {};
    /**
     * 当前编辑对象的复制对象，用于解决编辑中的结果处理
     */
    public curOldRow :any= {};
    /**
     * 是否将分页数据滚动添加，默认为true
     */
    public addResult = true;
    // 当前表单在视图切换的时候，是否会被注销
    public canDestroy=true;

    public dataOver=false;

    /**
     * 数据正在操作提示符
     */
    public isOpting=false;

    /**
     * 是否是新的记录
     */
    public isNew:string="FIELD_ISNEW";

    getEntitys(success:Function){
      
        if(!this.addResult){
            this.datas=[];
        }
        //let loading = this.loadingCtrl.create({
            //content: '数据加载中，请稍后...'//数据加载中显示
        //});
        //loading.present();
        this.dataService.getEntitys(this.sn, null,
            this.queryObject, this.orderObject,
            this.pageSize, this.pageIndex).subscribe((data)=>{

               // setTimeout(function(){
                   // loading.dismiss();
               // },100);

                if(data!=null && data.length>0){
                    for(let item=0;item<data.length;item++){
                        let val=data[item];
                        this.datas.push(val);
                    }
                    this.pageIndex=this.pageIndex+1;
                   // this.editDatas(this.datas);
                    this.changeDetectorRef.markForCheck();
                    this.changeDetectorRef.detectChanges();
                    if(success){
                        success(data);
                    }
                }else{
                    this.dataOver=true;
                    if(success){
                        success(data);
                    }
                }
        });
    };

    /**
     * 滚动动态加载数据
     * @param infiniteScroll 
     */
    doInfinite(infiniteScroll,success:Function) {
        console.log('Begin async operation');

        setTimeout(() => {
            if(!this.dataOver){
                //this.addResult=true;
                this.getEntitys(success);
            }
            infiniteScroll.complete();
        }, 500);
    };

    /**
     * 上拉刷新数据
     * @param infiniteScroll 
     */
    doRefresh(infiniteScroll,success:Function) {
        console.log('Async operation has ended');
        setTimeout(() => {
            if(!this.dataOver){
                //this.addResult=true;
                this.datas=[];
                this.pageIndex=1;
                this.getEntitys(success);
            }
            infiniteScroll.complete();
        }, 500);
    };

    /**
     * 日期格式处理
     * @param date 
     */
    formatDate(date) {  
        var y = date.getFullYear();  
        var m = date.getMonth() + 1;  
        m = m < 10 ? '0' + m : m;  
        var d = date.getDate();  
        d = d < 10 ? ('0' + d) : d;  
        return y + '-' + m + '-' + d;  
    };

    /**
     * 到秒日期格式处理
     * @param date 
     */
    formatDateToSecond(date) {  
        var y = date.getFullYear();  
        var m = date.getMonth() + 1;  
        m = m < 10 ? '0' + m : m;  
        var d = date.getDate();  
        d = d < 10 ? ('0' + d) : d;  
        var h = date.getHours();
        h = h < 10 ? '0' + h : h; 
        var mi = date.getMinutes();
        mi = mi < 10 ? '0' + mi : mi; 
        var s = date.getSeconds();
        s = s < 10 ? '0' + s : s; 
        return y + '-' + m + '-' + d + ' ' + h + ':' + mi + ':' + s;  
    };

    /**
     * 获取GUID值
     */
    getGUID(){
        var a = function(a){
            return 0>a?NaN:30>=a?0|Math.random()*(1<<a):53>=a?(0|1073741824*Math.random())+1073741824*(0|Math.random()*(1<<a-30)):NaN
        };
        var b = function(a,b){
            for(var c=a.toString(16),d=b-c.length,e="0";0<d;d>>>=1,e+=e){
                d&1&&(c=e+c);
            }
            return c;
        };
        var guidString = b(a(32),8)+b(a(16),4)+b(16384|a(12),4)+b(32768|a(14),4)+b(a(48),12);
        return guidString;
    };
}
