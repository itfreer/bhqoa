import { Pipe, PipeTransform } from '@angular/core';
import{ DateUtil } from '../utils/DateUtil';
/**
 * 时间格式化
 */
@Pipe({ name: 'asDate' })
export class AsDate implements PipeTransform {

    constructor(private dateUtil:DateUtil){

    }

    transform(input: any): string {
        if (input == null) {
            return '';
        }
        var dType = typeof(input);
        var date = this.dateUtil.parseDate(input,dType);
        
        if(date){ // if(dType=='object')
        var y = date.getFullYear();
        var m = date.getMonth() + 1;
        var d = date.getDate();
        var result = "";
        result = y + '-' + m + '-' + d;
        return result;
        } else if (dType=='string'){ //解决时间显示 ie和chrome不一致问题
        var array = input.match(/\d+/g);  
            input = array[0]+'-'+array[1]+'-'+array[2];
            return input;
        }
        return '';
    }
} 