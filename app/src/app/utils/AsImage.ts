import { Pipe, PipeTransform } from '@angular/core';
import { BaseService } from '../../app/core/data/BaseService';
/**
 * 时间格式化
 */
@Pipe({ name: 'asImage' })
export class AsImage implements PipeTransform {
    constructor(private baseService:BaseService){

    }
    transform(fileInfo: any): string {
        if(fileInfo==null || fileInfo=="") return "";
        let fileInfoArr = fileInfo.split(":");
        if(fileInfoArr.length<3)return "";
        let bucket = fileInfoArr[0];
        let key = fileInfoArr[1];
        let name = fileInfoArr[2];

        let temp=this.baseService.baseUrl+'/file/plupload/download';
        temp= temp+"?bucket="+bucket+"&key="+key+"&name="+encodeURIComponent(name);
        return temp;
    }
} 