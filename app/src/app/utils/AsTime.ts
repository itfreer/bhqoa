import { Pipe, PipeTransform } from '@angular/core';
import{ DateUtil } from '../utils/DateUtil';
/**
 * 时间格式化
 */
@Pipe({ name: 'asTime' })
export class AsTime implements PipeTransform {

    constructor(private dateUtil:DateUtil){

    }

    transform(input: any): string {
        if (input == null) {
            return '';
        }
        var dType = typeof(input);
        var date = this.dateUtil.parseDate(input,dType);
        
        if(date){ // if(dType=='object')
            var hours1=date.getHours();
            var min1=date.getMinutes();
            var result1 = "";
            if(hours1<10){
                hours1="0"+hours1;
            }
            if(min1<10){
                min1="0"+min1;
            }
            result1 = hours1+":"+min1;
            return result1;
        } else if (dType=='string'){ //解决时间显示 ie和chrome不一致问题
        var array = input.match(/\d+/g);  
           
            var hours2=array[3];
            var min2=array[4];
            var result2 = "";
            if(hours2<10){
                hours2="0"+hours2;
            }
            if(min2<10){
                min2="0"+min2;
            }
            result2 = hours2+":"+min2;
            return result2;
        }
        return '';
    }
} 