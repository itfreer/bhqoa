import { Pipe, PipeTransform } from '@angular/core';
import{ DateUtil } from '../utils/DateUtil';
/**
 * 时间格式化拓展
 */
@Pipe({ name: 'asDateExtend' })
export class AsDateExtend implements PipeTransform {

    constructor(private dateUtil:DateUtil){

    }

    transform(input: any): string {
        if (input == null) {
            return '';
        }
        let dType = typeof(input);
        let date = this.dateUtil.parseDate(input,dType);
        
        if(date){ // if(dType=='object')
        let y = date.getFullYear();
        let m = date.getMonth() + 1;
        let d = date.getDate();
        let hours=date.getHours();
        let min=date.getMinutes();
        let result = "";
        if(hours<10){
            hours="0"+hours;
        }
        if(min<10){
            min="0"+min;
        }
        result = y + '-' + m + '-' + d+" "+hours+":"+min;
        return result;
        } else if (dType=='string'){ //解决时间显示 ie和chrome不一致问题
            let array = input.match(/\d+/g);  
            let hours=array[3];
            let min=array[4];
            if(hours<10){
                hours="0"+hours;
            }
            if(min<10){
                min="0"+min;
            }
            input = array[0]+'-'+array[1]+'-'+array[2]+" "+hours+":"+min;
            return input;
        }
        return '';
    }
} 