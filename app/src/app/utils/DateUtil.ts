import { Injectable } from '@angular/core';
// 基础服务类，提供基础路径
@Injectable()
export class DateUtil {
    constructor(){

    }

    /**
     * 获取固定时间格式
     * @param input 
     */
    getDateFormate(input:any):string{
        if (input == null) {
        return '';
        }
        var dType = typeof(input);
        var date = this.parseDate(input,dType);
        
        if(date){ // if(dType=='object')
        var y = date.getFullYear();
        var m = date.getMonth() + 1;
        var d = date.getDate();
        var result = "";
        result = y + '/' + m + '/' + d;
        return result;
        } else if (dType=='string'){ //解决时间显示 ie和chrome不一致问题
        var array = input.match(/\d+/g);  
            input = array[0]+'/'+array[1]+'/'+array[2];
            return input;
        }
        return '';
    }

     /**
     * 获取固定时间格式
     * @param input 
     */
    getDateFormateCommon(input:any):string{
        if (input == null) {
        return '';
        }
        var dType = typeof(input);
        var date = this.parseDate(input,dType);
        
        if(date){ // if(dType=='object')
        var y = date.getFullYear();
        var m = date.getMonth() + 1;
        var d = date.getDate();
        var result = "";
        result = y + '-' + m + '-' + d;
        return result;
        } else if (dType=='string'){ //解决时间显示 ie和chrome不一致问题
        var array = input.match(/\d+/g);  
            input = array[0]+'-'+array[1]+'-'+array[2];
            return input;
        }
        return '';
    }

    /**
     * 获取固定时间格式
     * @param input 
     */
    getDateFormateExtend(input:any):string{
        if (input == null) {
        return '';
        }
        var dType = typeof(input);
        var date = this.parseDate(input,dType);
        
        if(date){ // if(dType=='object')
        var y = date.getFullYear();
        var m = date.getMonth() + 1;
        var d = date.getDate();
        var hours=date.getHours();
        var min=date.getMinutes();
        var result = "";
        result = y + '/' + m + '/' + d+" "+hours+":"+min;
        return result;
        } else if (dType=='string'){ //解决时间显示 ie和chrome不一致问题
        var array = input.match(/\d+/g);  
            input = array[0]+'/'+array[1]+'/'+array[2]+" "+array[3]+":"+array[4];;
            return input;
        }
        return '';
    }

    /**
     * 解析数据
     * @param input 
     * @param dType 
     */
    parseDate(input:any,dType:string):any{
        try{
        var date = null;
        if (dType == 'date') {
            date = input;
        } else if (dType == 'object') {
            if (input.hasOwnProperty('time')) {
            date = new Date(input.time);
            } else {
            return null;
            }
        } else if (dType == 'string') {
            if(input.indexOf("-")>-1){
                date = new Date(input.replace(/-/,"/"));
            }else{
                date = new Date(Number(input));
            }
           // date = new Date(Number(input));
        } else if (dType == 'number') {
            date = new Date(input);
        } else if(dType == "undefined"){
            return "";
        }
        return date;
        }catch(e){
        return null;
        }
    };
}