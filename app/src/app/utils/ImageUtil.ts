import { Injectable } from '@angular/core';

import { BaseService } from '../../app/core/data/BaseService';
// 基础服务类，提供基础路径
@Injectable()
export class ImageUtil {
    constructor(private baseService:BaseService){

    }

    /**
    * 
    * @param image 转换图片的地址
    */
    getImagePath(image:string):string{
        let fileInfo=image;
        if(fileInfo==null || fileInfo=="") return "";
        let fileInfoArr = fileInfo.split(":");
        if(fileInfoArr.length<3)return "";
        let bucket = fileInfoArr[0];
        let key = fileInfoArr[1];

        return this.baseService.fileUrl + "/"+bucket+"/"+key;
    };

        /**
    * 
    * @param image 转换图片的地址
    */
   getVideoPath(image:string):string{
        let fileInfo=image;
        if(fileInfo==null || fileInfo=="") return "";
        let fileInfoArr = fileInfo.split(":");
        if(fileInfoArr.length<3)return "";
        let bucket = fileInfoArr[0];
        let key = fileInfoArr[1];

        return this.baseService.fileUrl + "/"+bucket+"/"+key;
    };

    /**
     * 获取视频文件类型
     * @param fileInfo 
     */
    getVideoType(fileInfo:string){
        if(fileInfo==null || fileInfo=="") return "";
        let fileInfoArr = fileInfo.split(":");
        if(fileInfoArr.length<3)return "";
        let name = fileInfoArr[2];
        
        let namearray=name.split(".");
        if(namearray!=null && namearray.length==2) return namearray[1];
        return null;
    }
}