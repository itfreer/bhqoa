import { Pipe, PipeTransform } from '@angular/core';
/**
 * 文本长度过滤类
 */
@Pipe({ name: 'asLength' })
export class AsLength implements PipeTransform {
  transform(value: string,length:number): string {
    if(!value) return value;
    if(typeof value !== 'string') {
      return null;
    }else{
        if(value.length<length) return value;
        else{
            let temp=value.substr(0,length)+"...";
            return temp;
        }
    }
  }
} 