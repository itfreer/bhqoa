import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { catchError, retry } from 'rxjs/operators';

import { BaseService } from '../../app/core/data/BaseService';
// 签到相关
@Injectable()
export class SignHttp {
    constructor(
        private http:HttpClient,
        private baseService:BaseService,
		) { 
    }
    
    /**
     * 签到
     */
	gzqdgldata(method:string,type:string,latitude:any,longitude:any,address:any,accuracy:any){
        let url=this.baseService.baseUrl+"/gzqdgl/qdgl";
        let data={
          method:method,
          type:type,
          latitude:latitude,
          longitude:longitude,
          address:address,
          accuracy:accuracy,
          dirtype:"yd"
        };
        let as = this.baseService.toJSONStr(data);
        return this.http.post(url,as,{headers:this.baseService.httpOptions(),withCredentials:true})
        .pipe(retry(3), catchError(this.baseService.handleError<{}>({})));
      };
}