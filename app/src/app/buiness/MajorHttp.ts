import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { BaseService } from '../../app/core/data/BaseService';

// 用户考题基础
@Injectable()
export class MajorHttp {
    constructor(private http:HttpClient,
        private baseService:BaseService
		) { 
    }

    /**
     * 获取专业
     */
	getMajors(
        ):Observable<any[]> {
        let url:string = this.baseService.baseUrl + "/dictionarymanage/getChild/education_major/0/false";
        return this.http.post<any[]>(url, "", {headers:this.baseService.httpOptions(),withCredentials:true})
        .pipe(retry(3), catchError(this.baseService.handleError<any[]>([])));
    }
}