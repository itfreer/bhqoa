import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { DateUtil } from '../../app/utils/DateUtil';
import { ImageUtil } from '../../app/utils/ImageUtil';

import { BaseService } from '../../app/core/data/BaseService';
// 基础服务类，提供基础路径
@Injectable()
export class CourseHttp {
    constructor(private http:HttpClient,
        private baseService:BaseService,
        private imageUtil:ImageUtil,
        private dateUtil:DateUtil
		) { 
    }
    
    /**
     * 设置课程是否被收藏
     * @param sn 
     * @param courseId 
     * @param userId 
     * @param collect 
     */
	setCollect(sn:string,courseId:string, userId:string,
		collect:string):Observable<any[]> {
		let args = {
			courseId : courseId,
			userId : userId,
			collect : collect,
		};
		let as = this.baseService.toJSONStr(args);
		let url:string = this.baseService.baseUrl + "/" + sn + "/setCollect";
		return this.http.post<any[]>(url, as, {headers:this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any[]>([])));
    }

    /**
     * 添加课程点击数目
     * @param sn 
     * @param courseId 
     * @param userId 
     * @param collect 
     */
    addCourseClicks(sn:string,courseId:string):Observable<any[]> {
		let args = {
			courseId : courseId
		};
		let as = this.baseService.toJSONStr(args);
		let url:string = this.baseService.baseUrl + "/" + sn + "/addCourseClicks";
		return this.http.post<any[]>(url, as, {headers:this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any[]>([])));
    }

    /**
     * 添加章节是否被学习
     * @param sn 
     * @param courseId 
     * @param userId 
     * @param collect 
     */
    addChapterLearn(sn:string,courseId:string, userId:string,
      chapterId:string):Observable<any[]> {
		let args = {
			courseId : courseId,
			userId : userId,
			chapterId : chapterId,
		};
		let as = this.baseService.toJSONStr(args);
		let url:string = this.baseService.baseUrl + "/" + sn + "/addChapterLearn";
		return this.http.post<any[]>(url, as, {headers:this.baseService.httpOptions(),withCredentials:true})
			.pipe(retry(3), catchError(this.baseService.handleError<any[]>([])));
    }
    
      /**
   * 格式化课程服务的数据
   * @param val 
   */
  foremateCourseValue(val:any):any{
    //处理图片路径
    val["imagePath"]=this.imageUtil.getImagePath(val["image"]);
    //处理时间格式
    val["dataForemate"]=this.dateUtil.getDateFormate(val["releaseTime"]);
    //已经学习完成的统计方式
    if(val["finshed"]==null || val["finshed"]==''){
      val["finshedCount"]=0;
    }else{
      let lcount=val["finshed"].split(",");
      if(lcount!=null){
        val["finshedCount"]=lcount.length;
      }else{
        val["finshedCount"]=0;
      }
    }
    //处理收藏记录的格式
    if(val["collect"]=="1"){
      val["iconName"]="star";
    }else{
      val["iconName"]="star-outline";
    }
    return val;
  };
}