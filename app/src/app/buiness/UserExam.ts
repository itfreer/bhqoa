import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { BaseService } from '../../app/core/data/BaseService';
// 用户考题基础
@Injectable()
export class UserExam {
    constructor(private http:HttpClient,
        private baseService:BaseService
		) { 
    }

    /**
     * 用户考题
     * @param sn 
     * @param courseId 
     * @param userId 
     * @param collect 
     */
	userExam(sn:string,topicId:string, userId:string,
		):Observable<any[]> {
		let args = {
			topicId : topicId,
			userId : userId
		};
		let as = this.baseService.toJSONStr(args);
		let url:string = this.baseService.baseUrl + "/" + sn + "/userExam";
        return this.http.post<any[]>(url, as, {headers:this.baseService.httpOptions(),withCredentials:true})
        .pipe(retry(3), catchError(this.baseService.handleError<any[]>([])));
    }
}