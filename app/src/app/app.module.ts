import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler,App } from 'ionic-angular';
import { HttpClientModule }    from '@angular/common/http';
import { CookieModule } from 'ngx-cookie';
import { MultiPickerModule } from 'ion-multi-picker';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//import { Camera } from '@ionic-native/camera';
//import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Badge } from '@ionic-native/badge';
import { AppVersion } from '@ionic-native/app-version';
import { FileOpener } from '@ionic-native/file-opener';
import { Transfer,TransferObject } from '@ionic-native/transfer';
import { AndroidPermissions } from '@ionic-native/android-permissions';

import { MyApp } from './app.component';
import { BaseService } from './core/data/BaseService';
import { DictionaryService } from './core/dictionary/DictionaryService';
import { FileService } from './core/data/FileService';
import { DataService } from './core/data/DataService';
import { FormObject } from './form/FormObject';
import { BpmObject } from './bpm/BpmObject';

import { AutoresizeDirective } from './form/AutoresizeDirective';

//CoureInfo
import { CourseHttp } from './buiness/CourseHttp';
import { UserExam } from './buiness/UserExam';
import { DateUtil } from './utils/DateUtil';
import { ImageUtil } from './utils/ImageUtil';
import { AsLength } from './utils/AsLength';
import { AsDateExtend } from './utils/AsDateExtend';
import { AsDate } from './utils/AsDate';
import { AsImage } from './utils/AsImage';
import { AsTime } from './utils/AsTime';

import { SecurityDataService } from './core/security/SecurityDataService';
import { SecurityService } from './core/security/SecurityService';

import { TabsPage } from '../pages/tabs/tabs';
import { TodoPage } from './../pages/todo/todo';
import { DonePage } from './../pages/done/done';
import { TaskPage } from './../pages/task/task';
import { JobsPage } from './../pages/jobs/jobs';
import { SignTextPage } from './../pages/jobs/form/sign_text';
import { MePage } from './../pages/me/me';
import { ProcessPage } from './../pages/process/process';
import { LeaveNewPage } from './../pages/leave-new/leave-new';
import { CalendarPage } from './../pages/calendar/calendar';
import { ApprovalPage } from './../pages/approval/approval';
import { HomePage } from '../pages/home/home';
import { SettingPage } from '../pages/setting/setting';
import { AboutPage } from '../pages/about/about';
import { HelpPage } from '../pages/help/help';
import { LoginPage } from '../pages/login/login';
import { PasswordPage } from '../pages/password/password';
import { SignPage } from '../pages/sign/sign';
import { SigndetailsPage } from '../pages/signdetails/signdetails';
import { BpmSignPage } from '../pages/bpmsign/bpmsign';
import { BpmBackPage } from '../pages/bpmback/bpmback';
import { BpmImgPage } from '../pages/bpmimg/bpmimg';
import { BpmProcessPage } from '../pages/bpmprocess/bpmprocess';
import { TripPage } from '../pages/trip/trip';
import { LeavePage } from '../pages/leave/leave';
import { VehiclePage } from '../pages/vehicle/vehicle';
import { VehicleannalPage } from '../pages/vehicleannal/vehicleannal';
import { ApprovePage } from '../pages/approve/approve';
import { CcglPage } from '../pages/ccgl/ccgl';
import { QjglPage } from '../pages/qjgl/qjgl';
import { RcapPage } from '../pages/rcap/rcap';
import { SwglPage } from '../pages/swgl/swgl';
import { WfbdPage } from '../pages/wfbd/wfbd';
import { WlkyxmPage } from '../pages/wlkyxm/wlkyxm';
import { YcsqPage } from '../pages/ycsq/ycsq';
import { YzglPage } from '../pages/yzgl/yzgl';
import { ZysqPage } from '../pages/zysq/zysq';
import { WjryPage } from '../pages/wjry/wjry';
import { KyxmPage } from '../pages/kyxm/kyxm';
import { AddUserPage } from '../pages/adduser/adduser';
import { SignNewPage } from '../pages/sign_new/sign_new';
import { SwglNewPage } from '../pages/swgl_new/swgl_new';
import { SwglDonePage } from '../pages/swgl_done/swgl_done';
import { FwglPage } from '../pages/fwgl/fwgl';
import {Camera} from "@ionic-native/camera"; 
import { Keyboard } from '@ionic-native/keyboard';
import { MajorHttp } from '../app/buiness/MajorHttp';
import { SignHttp } from '../app/buiness/SignHttp';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { ImagePicker } from '@ionic-native/image-picker';
import { IonicImageViewerModule } from 'ionic-img-viewer';

//import { LocalNotifications } from '@ionic-native/local-notifications';//消息栏推送

@NgModule({
  declarations: [
    MyApp,
    AsLength,
    AsDateExtend,
    AsDate,
    AsImage,
    AsTime,
    TabsPage,
    HomePage,
    TodoPage,
    DonePage,
    TaskPage,
    JobsPage,
    SignTextPage,
    MePage,
    ProcessPage,
    LeaveNewPage,
    CalendarPage,
    ApprovalPage,
    SettingPage,
    ApprovePage,

    AboutPage,
    HelpPage,
    LoginPage,
    PasswordPage,
    SignPage,
    SigndetailsPage,
    CcglPage,
    QjglPage,
    BpmSignPage,
    BpmBackPage,
    BpmImgPage,
    BpmProcessPage,
    TripPage,
    LeavePage,
    VehiclePage,
    VehicleannalPage,
    RcapPage,
    SwglPage,
    WfbdPage,
    WlkyxmPage,
    YcsqPage,
    YzglPage,
    ZysqPage,
    WjryPage,
    KyxmPage,
    AddUserPage,
    SignNewPage,
    SwglNewPage,
    SwglDonePage,
    FwglPage,
    AutoresizeDirective,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicImageViewerModule,
    MultiPickerModule,
    CookieModule.forRoot(),
    IonicModule.forRoot(MyApp, {
      mode:'ios',
      backButtonText:'',
      statusbarPadding: true,
      tabsHideOnSubPages: 'true'         //隐藏全部子页面tabs
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,

    TabsPage,
    HomePage,
    TodoPage,
    DonePage,
    TaskPage,
    JobsPage,
    SignTextPage,
    MePage,
    ProcessPage,
    LeaveNewPage,
    CalendarPage,
    ApprovalPage,
    SettingPage,
    ApprovePage,

    AboutPage,
    HelpPage,
    LoginPage,
    PasswordPage,
    SignPage,
    SigndetailsPage,
    CcglPage,
    QjglPage,
    BpmSignPage,
    BpmBackPage,
    BpmImgPage,
    BpmProcessPage,
    TripPage,
    LeavePage,
    VehiclePage,
    VehicleannalPage,
    RcapPage,
    SwglPage,
    WfbdPage,
    WlkyxmPage,
    YcsqPage,
    YzglPage,
    ZysqPage,
    WjryPage,
    KyxmPage,
    AddUserPage,
    SignNewPage,
    SwglNewPage,
    SwglDonePage,
    FwglPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    //Camera,相机暂时没有找到
    //BarcodeScanner,
    Badge,
    AppVersion,
    FileOpener,
    Transfer,
    TransferObject,
    AndroidPermissions,
    BaseService,
    DictionaryService,
    FileService,
    DataService,
    FormObject,
    BpmObject,
    AutoresizeDirective,
    SecurityDataService,
    SecurityService,
    CourseHttp,
    UserExam,
    DateUtil,
    ImageUtil,
    Keyboard,
    MajorHttp,
    SignHttp,
    File,
    FileTransfer, 
    FileTransferObject, 
    Camera,
    ImagePicker,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {
  constructor(
    private app: App,
    private securityService:SecurityService,
    private dataService:DataService,
    private badge:Badge,) {

    //监听待办数量
    this.app.viewDidEnter.subscribe(res=>{
      if(this.securityService.user!=null && this.securityService.user["userid"]!=null){
        if(res.id=="t0-0-0" || res.id=="t0-1-0" || res.id=="t0-2-0" || res.id=="t0-3-0"){
          let num:number = 0;
          if(window.localStorage.getItem("swglnums")!=null && window.localStorage.getItem("swglnums")!=""){
            window.localStorage.setItem("jobNews","new");
            num=parseInt(window.localStorage.getItem("swglnums"));
          }else{
            window.localStorage.setItem("jobNews","");
          }
          this.dataService.getRowCounts("worklist",null).subscribe((data)=>{
            if(data>0){
              console.log(window.localStorage.getItem("todoCount"));
              window.localStorage.setItem("todoCount",data+"");
              this.badge.set(data+num);
            }else if(data==0){
              console.log("？？？");
              window.localStorage.setItem("todoCount","");
              this.badge.clear();
            }
          });
        }
      }
      // do something
    });
  }
}
