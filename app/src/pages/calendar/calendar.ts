import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController, NavParams,AlertController,ActionSheetController  } from 'ionic-angular';
import { DataService } from '../../app/core/data/DataService';
import { BaseService } from '../../app/core/data/BaseService';
import { SecurityService } from '../../app/core/security/SecurityService';
import { LoginPage } from '../../pages/login/login';
import { BpmObject } from '../../app/bpm/BpmObject';
import { BpmBackPage } from '../../pages/bpmback/bpmback';
import { BpmSignPage } from '../../pages/bpmsign/bpmsign';
import { BpmProcessPage } from '../../pages/bpmprocess/bpmprocess';
import { LoadingController } from 'ionic-angular';
import { BpmImgPage } from '../../pages/bpmimg/bpmimg';
import { DictionaryService } from '../../app/core/dictionary/DictionaryService';
import { AddUserPage } from '../../pages/adduser/adduser';

@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
})
export class CalendarPage {
  user={};
  cxfs:DictionaryService=new DictionaryService(this.dataService,this.baseSErvice,);
  public gzccgl:BpmObject=new BpmObject(this.dataService,this.baseSErvice,
    this.changeDetectorRef,this.navCtrl,this.alert,this.loadingCtrl,);

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private dataService:DataService,
    private baseSErvice:BaseService,
    private alert:AlertController,
	private securityService:SecurityService,
  public changeDetectorRef:ChangeDetectorRef, 
  public loadingCtrl: LoadingController,
  public actionSheetCtrl: ActionSheetController ) {

    if(this.navParams.data!=null && this.navParams.data["bk"]!=null){
      this.gzccgl.editRow={};
      this.gzccgl.sn="gzccgl";
      this.gzccgl.bpmKey=this.navParams.data["bk"];
      this.gzccgl.id=this.navParams.data["dk"];
      this.gzccgl.editRow.id=this.navParams.data["dk"];
      this.gzccgl.editRow.bpmkey=this.navParams.data["bk"];
      this.gzccgl.editRow.FIELD_ISNEW=this.navParams.data["FIELD_ISNEW"];
      this.gzccgl.editRow.sqrq=this.gzccgl.formatDate(new Date());
      this.gzccgl.editRow.qjts="1";


      if(this.securityService.user!=null && this.securityService.user["userid"]!=null){
        this.gzccgl.editRow.ccrxm=this.securityService.user["userName"];
        this.gzccgl.editRow.ccrid=this.securityService.user["userid"];
        this.gzccgl.editRow.dwid=this.securityService.user["departmentId"];
        this.gzccgl.editRow.ccrzw=this.securityService.user["roleNames"];
      }
    }


    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.user = this.securityService.user;
        this.gzccgl.editRow.ccrid=this.securityService.user["userid"];
        if(this.navParams.data!=null && this.navParams.data["bk"]!=null){
          this.gzccgl.findWork("gzccgl",null,null);
        }
      },()=>{
        this.goLogin();
      })
    }else{
      this.user = this.securityService.user;
      this.gzccgl.editRow.ccrid=this.securityService.user["userid"];
      if(this.navParams.data!=null && this.navParams.data["bk"]!=null){
        this.gzccgl.findWork("gzccgl",null,null);
      }
    }
  }

  goLogin(){
    this.navCtrl.push(LoginPage);
  };
  backPage(){
    this.navCtrl.pop();
  };

  goSignPage(){
    this.navCtrl.push(BpmSignPage,this.gzccgl);
  };

  goBackPage(){
    this.navCtrl.push(BpmBackPage,this.gzccgl);
  };

  goProcessPage(){
    let actionSheet = this.actionSheetCtrl.create({
      title: '更多',
      buttons: [
        {
          text: '办理过程',
          handler: () => {
            this.navCtrl.push(BpmProcessPage,this.gzccgl);
          }
        },{
          text: '流程图',
          handler: () => {
            this.navCtrl.push(BpmImgPage,this.gzccgl);
          }
        },{
          text: '取消',
          role: '取消',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  };

  ionViewDidLoad() {
    //console.log('ionViewDidLoad LeaveNewPage');
    this.cxfs.getDatas("GZ_RSGL_CCGL_CXFS","0",false,null,null);
  }

  addUser(){
    if(this.gzccgl.editRow==null){
      this.gzccgl.editRow={};
    }
    if(this.gzccgl.editRow.ccsxry==null){
      this.gzccgl.editRow.ccsxry=[];
    }
    this.navCtrl.push(AddUserPage,this.gzccgl.editRow.ccsxry);
  }

  userFormValidate(){
    let message=[];
    if(this.gzccgl.editRow["cfd"]==null || this.gzccgl.editRow["cfd"]==""){
      message.push("<span style='color:red'>出发地<br></span>");
    }

    if(this.gzccgl.editRow["mdd"]==null || this.gzccgl.editRow["mdd"]==""){
      message.push("<span style='color:red'>目的地<br></span>");
    }

    if(this.gzccgl.editRow["wcsy"]==null || this.gzccgl.editRow["wcsy"]==""){
      message.push("<span style='color:red'>外出事由<br></span>");
    }

    if(this.gzccgl.editRow["kssj"]==null || this.gzccgl.editRow["kssj"]==""){
      message.push("<span style='color:red'>开始时间<br></span>");
    }

    if(this.gzccgl.editRow["jssj"]==null || this.gzccgl.editRow["jssj"]==""){
      message.push("<span style='color:red'>开始时间<br></span>");
    }

    if((this.gzccgl.editRow["cxfsfj"]==null || this.gzccgl.editRow["cxfsfj"]=="")
        && (this.gzccgl.editRow["cxfshc"]==null || this.gzccgl.editRow["cxfshc"]=="")
        && (this.gzccgl.editRow["cxfsdwpc"]==null || this.gzccgl.editRow["cxfsdwpc"]=="")
        && (this.gzccgl.editRow["cxfsqc"]==null || this.gzccgl.editRow["cxfsqc"]=="")
        && (this.gzccgl.editRow["cxfszdc"]==null || this.gzccgl.editRow["cxfszdc"]=="")
        && (this.gzccgl.editRow["cxfsqt"]==null || this.gzccgl.editRow["cxfsqt"]=="")){
      message.push("<span style='color:red'>交通工具<br></span>");
    }

    // if(this.gzccgl.editRow["cxfs"]==null || this.gzccgl.editRow["cxfs"]==""){
    //   message.push("<span style='color:red'>交通工具<br></span>");
    // }

    if(message.length>0){
      let strm=message.join("");
      let temp = this.alert.create({
        title: '系统提示',
        subTitle: '以下表单项没有填写正确! <br>'+strm,
        buttons: ['确定']
      });
      temp.present();
      return false;
    }else{
      return true;
    }
   
  }
}