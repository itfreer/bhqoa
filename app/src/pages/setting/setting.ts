import { Component } from '@angular/core';

import { NavController, NavParams, ToastController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { AboutPage } from '../about/about';
import { HelpPage } from '../help/help';
import { LoginPage } from '../login/login';
import { PasswordPage } from '../password/password';
import { SignPage } from '../sign/sign';
import { TripPage } from '../trip/trip';
import { LeavePage } from '../leave/leave';

import { SecurityService } from '../../app/core/security/SecurityService';

@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {

  constructor(public viewCtrl: ViewController,
    public navCtrl: NavController, 
    public navParams: NavParams,
    private toastCtrl: ToastController,
    private securityService:SecurityService) {
    this.securityService.init();
  }

  goAbout() {
    this.navCtrl.push(AboutPage);
  }

  goHelp() {
    this.navCtrl.push(HelpPage);
  }

  goLogin() {
    if(this.securityService.user){
      this.toastCtrl.create({
        message: '用户已登录，请先退出！',
        duration: 2000,
        position: 'top'
      }).present();
      return;
    }
    this.navCtrl.push(LoginPage);
  }

  goPassword() {
    if(!this.securityService.user){
      this.toastCtrl.create({
        message: '用户未登录，请先登录！',
        duration: 2000,
        position: 'top'
      }).present();
      return;
    }
    this.navCtrl.push(PasswordPage);
  }

  goSign() {
    // if(!this.securityService.user){
    //   this.toastCtrl.create({
    //     message: '用户未登录，请先登录！',
    //     duration: 2000,
    //     position: 'top'
    //   }).present();
    //   return;
    // }
    this.navCtrl.push(SignPage);
  }

  goTrip() {
    // if(!this.securityService.user){
    //   this.toastCtrl.create({
    //     message: '用户未登录，请先登录！',
    //     duration: 2000,
    //     position: 'top'
    //   }).present();
    //   return;
    // }
    this.navCtrl.push(TripPage);
  }

  goLeave() {
    // if(!this.securityService.user){
    //   this.toastCtrl.create({
    //     message: '用户未登录，请先登录！',
    //     duration: 2000,
    //     position: 'top'
    //   }).present();
    //   return;
    // }
    this.navCtrl.push(LeavePage);
  }
}
