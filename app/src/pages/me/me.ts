import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SecurityService } from '../../app/core/security/SecurityService';
import { LoginPage } from '../../pages/login/login';


/**
 * Generated class for the MePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-me',
  templateUrl: 'me.html',
})
export class MePage {

  user ={};

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private securityService:SecurityService,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MePage');
  }

   //跳转到登录界面
   goLogin(){
    this.navCtrl.push(LoginPage);
  };

  //初始化页面
  ionViewDidEnter(){
    this.user={};
    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.user= this.securityService.user;
      },()=>{
        this.user = {};
        this.goLogin();
      })
    }else{
      this.user= this.securityService.user;
 
    }
  };

}
