import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController,LoadingController,NavParams } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';
import { SecurityService } from '../../app/core/security/SecurityService';

@Component({
  templateUrl: 'signdetails.html'
})

export class SigndetailsPage {
    user:any={};
    details:any={};
  constructor(
    private navCtrl: NavController,
    private securityService:SecurityService,
    public changeDetectorRef:ChangeDetectorRef,
    public loadingCtrl:LoadingController,
    public navParams: NavParams,
    ) {
        this.details=this.navParams.data;
      this.ionViewDidEnter();
  }

  //初始化页面
  ionViewDidEnter(){
    this.user={};
    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.initSuccessSignPage();
      },()=>{
        this.user = null;
        this.goLogin();
      })
    }else{
      this.initSuccessSignPage();
    }
  };

  //跳转到登录界面
  goLogin(){
    this.navCtrl.push(LoginPage);
  };

  /**
   * 成功加载用户签到页面时处理
   */
  initSuccessSignPage(){
    var dkbzs=this.details.dkbz.split('；');
    var dkbz='';
    for(var i=0;i<dkbzs.length;i++){
      if(dkbzs.length>1 && i==0){
        dkbz+='\n';
      }
      if(i!=(dkbzs.length-1)){
        dkbz+=dkbzs[i]+'\n';
      }else{
        dkbz+=dkbzs[i];
      }
    }
    this.details.dkbz=dkbz;
    var bzs=this.details.bz.split('；');
    var bz='';
    for(var j=0;j<bzs.length;j++){
      if(bzs.length>1 && i==0){
        bz+='\n';
      }
      if(j!=(dkbzs.length-1)){
        bz+=bzs[j]+'\n';
      }else{
        bz+=bzs[j];
      }
    }
    this.details.bz=bz;
  };
}
