import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController,LoadingController  } from 'ionic-angular';

import { FormObject } from '../../app/form/FormObject';
import { LoginPage } from '../../pages/login/login';
import { SecurityService } from '../../app/core/security/SecurityService';
import { BaseService } from '../../app/core/data/BaseService';
import { DataService } from '../../app/core/data/DataService';
import { CcglPage } from '../../pages/ccgl/ccgl';
import { RcapPage } from '../../pages/rcap/rcap';
import { SwglPage } from '../../pages/swgl/swgl';
import { QjglPage } from '../../pages/qjgl/qjgl';
import { WlkyxmPage } from '../../pages/wlkyxm/wlkyxm';
import { YcsqPage } from '../../pages/ycsq/ycsq';
import { YzglPage } from '../../pages/yzgl/yzgl';
import { ZysqPage } from '../../pages/zysq/zysq';
import { WjryPage } from '../../pages/wjry/wjry';
import { KyxmPage } from '../../pages/kyxm/kyxm';

@Component({
  templateUrl: 'home.html'
})

export class HomePage {

  pet:string="待办事宜";
  datas=[];
  user={};
  tableName : string = 'worklist';
  showDetails= false;
  //构建formObject对象方式修改，这种方式建立了独立的对象数据
  public worklist:FormObject=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);
  public overworklist:FormObject=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);
  public gztaskview:FormObject=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);
  public gzreceiveview:FormObject=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);

  constructor(
    private navCtrl: NavController,
    private baseService:BaseService,
    private dataService:DataService,
    private securityService:SecurityService,
    public changeDetectorRef:ChangeDetectorRef,
    public loadingCtrl: LoadingController
    ) {
  }

  toQueryWork(data:any){
    let para={};
    para["bk"]=data.actdefid;
    para["fn"]=data.prjforname;
    para["vi"]=data.prjpopname;
    para["dk"]=data.sbussinessid;
    //出差
    if(para["bk"]=="ccgl" 
      || para["bk"]=="ccgl_jjgb"
      || para["bk"]=="ccgl_kjgb"
      || para["bk"]=="ccgl_zrjbgb"){
      this.navCtrl.push(CcglPage,para);
    }
    //收发文
    if(para["bk"]=="sfwgl"
      || para["bk"]=="sfwgl_jzgb"
      || para["bk"]=="sfwgl_kszysj"
      || para["bk"]=="sfwgl_zrjbgb" ){
      this.navCtrl.push(SwglPage,para);
    }
    //外来科研项目
    if(para["bk"]=="wlkyxmrqxk"){
      this.navCtrl.push(WlkyxmPage,para);
    }
    //用车申请
    if(para["bk"]=="clsysq"
      || para["bk"]=="clsysq_kjgb"
      || para["bk"]=="clsysq_zrjbgb"){
      this.navCtrl.push(YcsqPage,para);
    }
    //用章管理
    if(para["bk"]=="yzgl"
      || para["bk"]=="yzgl_zrjbgb"){
      this.navCtrl.push(YzglPage,para);
    }
     //资源申请
     if(para["bk"]=="zydd"
     || para["bk"]=="zydd"){
     this.navCtrl.push(ZysqPage,para);
    }

    //请假管理
    if(para["bk"]=="qjgl"
      || para["bk"]=="qjgl_jjgb"
      || para["bk"]=="qjgl_kjgb"){
      this.navCtrl.push(QjglPage,para);
    }
    //外籍人员入区许可办理
    if(para["bk"]=="wjryrqxk"){
      this.navCtrl.push(WjryPage,para);
    }
    //科研项目管理
    if(para["bk"]=="kyxmgl"){
      this.navCtrl.push(KyxmPage,para);
    }
  };

  toQueryReceive(data:any){
    let para={};
    para["bk"]=data.bpmkey;
    para["fn"]="gzpublishfile";
    para["vi"]="gzpublishfile";
    para["dk"]=data.fwid;
    this.navCtrl.push(SwglPage,para);
  };

  toQuerySchedule(data:any){
    let para=data;
    this.navCtrl.push(RcapPage,para);
  };

  //初始化页面
  ionViewDidEnter(){
    this.user={};
    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.initSuccessHomePage();
      },()=>{
        this.user = null;
        this.goLogin();
      })
    }else{
      this.initSuccessHomePage();
    }
  };
  
  //跳转到登录界面
  goLogin(){
    this.navCtrl.push(LoginPage);
  };

  editDatas(datas:any){
    for (let item = 0; item < datas.length; item++) {
      datas[item].icon='ios-arrow-forward';
    }
  };

  /**
   * 成功加载用户home页面时处理
   */
  initSuccessHomePage(){
    this.user = this.securityService.user;
    //构建formObject对象方式修改，这种方式建立了独立的对象数据
    if(this.worklist.sn==""){
      this.worklist=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);
      this.worklist.sn="worklist";
    }

    if(this.overworklist.sn==""){
      this.overworklist=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);
      this.overworklist.sn="overworklist";
    }
   
    if(this.gztaskview.sn==""){
      this.gztaskview=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);
      this.gztaskview.sn="gztaskview";
      this.gztaskview.queryObject={"zt:!=":"rwzt_ywc"};
      this.gztaskview.orderObject={"cjsj":1};
    }

    if(this.gzreceiveview.sn==""){
      this.gzreceiveview=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);
      this.gzreceiveview.sn="gzreceiveview";
      this.gzreceiveview.queryObject={"sfqr":"0"};
      this.gzreceiveview.orderObject={"jjdj":1,"cjsj":1};
    }

    if(this.tableName=="worklist" && this.worklist.datas.length==0){
      this.worklist.getEntitys(null);
    }else if(this.tableName=="worklist" && this.overworklist.datas.length==0){
      this.overworklist.getEntitys(null);
    }else if(this.tableName=="gztaskview" && this.gztaskview.datas.length==0){
      this.gztaskview.getEntitys(this.editDatas);
    }else if(this.tableName=="gztaskview" && this.gzreceiveview.datas.length==0){
      this.gzreceiveview.getEntitys(this.editDatas);
    }

   
 
   
    //if(this.worklist.datas && this.worklist.datas.length>0){
   // }else{
     // this.worklist.getEntitys(null);
   // }
  };

  setTable(formName: string){
    if(formName=="worklist"){
      if(this.worklist.datas && this.worklist.datas.length>0){

      }else{
        this.worklist.getEntitys(null);
      }
      
    }else if( formName=="overworklist"){
      if(this.overworklist.datas && this.overworklist.datas.length>0){

      }else{
        this.overworklist.getEntitys(null);
      }
    }else if(formName=="gztaskview"){
      if(this.gztaskview.datas && this.gztaskview.datas.length>0){

      }else{
        this.gztaskview.getEntitys(this.editDatas);
      }
     
    }else{
      if(this.gzreceiveview.datas && this.gzreceiveview.datas.length>0){

      }else{
        this.gzreceiveview.getEntitys(this.editDatas);
      }
    }
    this.tableName = formName;
  }

  //打开或关闭数据详细内容
  toggleDetails(data) {
    if (data.showDetails) {
      data.showDetails = false;
      data.icon='ios-arrow-forward';
    } else {
      data.showDetails = true;
      data.icon='ios-arrow-down';
    }
  }

  //查询
  onSearchKeyUp(event){
    if("Enter"==event.key){
      //if(this.user!=null && this.user["userid"]!=null){
       // this.formObject.pageIndex=0;
       // this.formObject.dataOver=false;
       // this.formObject.getEntitys();
      }
    //}else{
      //this.queryValue="";
    //}
  }
}
