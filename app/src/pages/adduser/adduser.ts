import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DataService } from '../../app/core/data/DataService';
import { SecurityService } from '../../app/core/security/SecurityService';
import { LoginPage } from '../../pages/login/login';
import { LoadingController } from 'ionic-angular';
@Component({
  selector: 'page-cyr',
  templateUrl: 'adduser.html'
})

export class AddUserPage {
  /**
   * 用户
   */
  user={};

  selectUser=[];
  workInput:string="";
  selectedAll=false;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private dataService:DataService,
	private securityService:SecurityService,
	public changeDetectorRef:ChangeDetectorRef, 
  public loadingCtrl: LoadingController) {

    console.log("constructor加载了");
	
    this.selectUser=this.navParams.data;
    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.datas=[];
        this.loadUser();
      },()=>{
        this.goLogin();
      })
    }else{
      this.datas=[];
      this.loadUser();
    }
  };

  selectAllUser(){
    if(this.datas!=null ){
      for(let u=0;u<this.datas.length;u++){
          this.datas[u]["userSelected"]=this.selectedAll;
      }
    }
  };

  initSelectUser(){
    if(this.datas!=null && this.selectUser!=null){
      for(let u=0;u<this.datas.length;u++){
        for(let s=0;s<this.selectUser.length;s++){
          if(this.selectUser[s]["jsrid"]==this.datas[u]["userid"]){
            this.datas[u]["userSelected"]=true;
          }
        }
      }
    }
  };

  
  ionViewDidLoad() {

  }

  /**
   * 获取GUID值
   */
  getGUID(){
    var a = function(a){
        return 0>a?NaN:30>=a?0|Math.random()*(1<<a):53>=a?(0|1073741824*Math.random())+1073741824*(0|Math.random()*(1<<a-30)):NaN
    };
    var b = function(a,b){
        for(var c=a.toString(16),d=b-c.length,e="0";0<d;d>>>=1,e+=e){
            d&1&&(c=e+c);
        }
        return c;
    };
    var guidString = b(a(32),8)+b(a(16),4)+b(16384|a(12),4)+b(32768|a(14),4)+b(a(48),12);
    return guidString;
  };

  chooseUser(){
   this.selectUser.splice(0,this.selectUser.length);  
    if(this.datas!=null){
      for(let u=0;u<this.datas.length;u++){
        if(this.datas[u]["userSelected"]){
          let cRow={};
          cRow["id"]=this.getGUID();
          cRow["jsrid"]=this.datas[u].userid;
          cRow["jsrmc"]=this.datas[u].userName;
          if(this.datas[u].departmentName=="局领导"){
						cRow["jsrzw"]=this.datas[u].departmentName;
					}else{
						cRow["jsrzw"]=this.datas[u].roleNames;
					}
          cRow["sfqr"]='0';
          // let organizationName=this.datas[u].organizationName?this.datas[u].organizationName:"";
          // let departmentName=this.datas[u].departmentName?this.datas[u].departmentName:"";
          // let operatingPostName=this.datas[u].operatingPostName?this.datas[u].operatingPostName:"";
          cRow["jsrjg"]=this.datas[u].departmentId;
          cRow["jsrjgmc"]=this.datas[u].departmentName;
          this.selectUser.push(cRow);
        }
      }
    }
    this.navCtrl.pop();
    //this.navCtrl.push(CalendarPage);
  }
  searchWork(){
    this.pageIndex=1;//重置查询条件的时候，需要重置页码
    this.datas=[];
    if(this.workInput==null || this.workInput==""){
      this.queryObject={};
      this.loadUser();
    }else{
      //this.queryObject["userName:like"]=this.workInput;\
      //添加单位过滤
      this.dataService.getEntitys("department", null,
        {"display:like":this.workInput}, this.orderObject,
        this.pageSize, this.pageIndex).subscribe((data)=>{
          this.queryObject["userName:like"]=this.workInput;
          if(data!=null && data.length>0){
              let val="";
              for(let item=0;item<data.length;item++){
                val+=data[item]["id"]+",";
              }
              //this.queryObject["departmentId:in"]=val.substr(0,val.length-1);
              this.queryObject={or:[{"userName:like":this.workInput},{"departmentId:in":val.substr(0,val.length-1)}]};
          }
          this.loadUser();
        });
    }
  }

  goLogin(){
    this.navCtrl.push(LoginPage);
  };
  backPage(){
    this.navCtrl.pop();
  };

  loadUser(){
    this.selectedAll=false;
    this.dataService.getEntitys(this.sn, null,
      this.queryObject, this.orderObject,
      this.pageSize, this.pageIndex).subscribe((data)=>{
          if(data!=null && data.length>0){
              for(let item=0;item<data.length;item++){
                  let val=data[item];
                  this.datas.push(val);
              }
              this.initSelectUser();
              //this.pageIndex=this.pageIndex+1;
              this.changeDetectorRef.markForCheck();
              this.changeDetectorRef.detectChanges();
          }else{
              this.dataOver=true;
          }
      });
  }
  whereString="";
  queryObject={};
  pageSize=10000;
  pageIndex=1;
  datas=[];
  dataOver=true;
  orderObject={};
  sn="vuserinfo";

}