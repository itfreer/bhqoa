import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController, NavParams,AlertController,ActionSheetController } from 'ionic-angular';
import { DataService } from '../../app/core/data/DataService';
import { BaseService } from '../../app/core/data/BaseService';
import { SecurityService } from '../../app/core/security/SecurityService';
import { LoginPage } from '../login/login';
import { BpmObject } from '../../app/bpm/BpmObject';
import { LoadingController } from 'ionic-angular';
import { BpmBackPage } from '../../pages/bpmback/bpmback';
import { BpmSignPage } from '../../pages/bpmsign/bpmsign';
import { BpmProcessPage } from '../../pages/bpmprocess/bpmprocess';
import { BpmImgPage } from '../../pages/bpmimg/bpmimg';
import { FormObject } from '../../app/form/FormObject';

@Component({
  selector: 'page-ycsq',
  templateUrl: 'ycsq.html'
})

export class YcsqPage {
  readonly="";
  solution="不显示";
  haveError=false;
  user={};

  //车辆档案选择车辆信息问题
  public clda:FormObject=new FormObject(this.dataService,this.baseSErvice,
    this.changeDetectorRef,this.loadingCtrl);

  public gzclsq:BpmObject=new BpmObject(this.dataService,this.baseSErvice,
    this.changeDetectorRef,this.navCtrl,this.alert,this.loadingCtrl,);
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private dataService:DataService,
    private baseSErvice:BaseService,
    private alert:AlertController,
	private securityService:SecurityService,
	public changeDetectorRef:ChangeDetectorRef, 
  public loadingCtrl: LoadingController,
  public actionSheetCtrl: ActionSheetController) {
	this.gzclsq.editRow={};
	this.gzclsq.sn="gzclsq";
	this.gzclsq.bpmKey=this.navParams.data["bk"];
	this.gzclsq.id=this.navParams.data["dk"];

    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.user = this.securityService.user;
		//this.initUserExam();
		this.gzclsq.findWork("gzclsq",null,null);
      },()=>{
        this.goLogin();
      })
    }else{
      this.user = this.securityService.user;
	  this.gzclsq.findWork("gzclsq",null,null);
    }
  }

  goLogin(){
    this.navCtrl.push(LoginPage);
  };
  backPage(){
    this.navCtrl.pop();
  };

  goSignPage(){
    this.navCtrl.push(BpmSignPage,this.gzclsq);
  };

  goBackPage(){
    this.navCtrl.push(BpmBackPage,this.gzclsq);
  };

  goProcessPage(){
    //this.navCtrl.push(BpmProcessPage,this.gzccgl);
    let actionSheet = this.actionSheetCtrl.create({
      title: '更多',
      buttons: [
        {
          text: '办理过程',
          handler: () => {
            //console.log('Destructive clicked');
            this.navCtrl.push(BpmProcessPage,this.gzclsq);
          }
        },{
          text: '流程图',
          handler: () => {
            this.navCtrl.push(BpmImgPage,this.gzclsq);
          }
        },{
          text: '取消',
          role: '取消',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  };

  selectClcph(){
    let that=this;
    this.clda.sn="gzclda";
    this.clda.ssn="gzclda";
    this.clda.dataOver=false;
    this.clda.datas=[];
    this.clda.pageSize=1000;
    this.clda.pageIndex=1;
    this.clda.orderObject={id:0};
    this.clda. queryObject={'clzt':'clzt_wsy'};
    this.clda.getEntitys(function(data){
      that.selectCarShow(that);
    });
  };

  /**
   * 显示选车信息
   * @param that 
   */
  selectCarShow(that:any){
    if(that.clda.datas==null || that.clda.datas.length<1){
      this.alert.create({
        title:"系统提示",
        subTitle:"当前无可用车辆！",
        buttons:["确定"]
      }).present();
      return ;
    }
    let alert = this.alert.create();
    alert.setTitle("车辆选择");
    for(let k=0;k< that.clda.datas.length;k++){
        let icar=that.clda.datas[k];
        alert.addInput({
          type: 'checkbox',
          label: icar.sclxh+"-"+icar.sclxxName+"-"+icar.scph,
          value: icar.id,
          checked: true
      });
    }
    alert.addButton('取消');
    alert.addButton({
    text: '确定',
    handler: data => {
        console.log('Checkbox data:', data);
        let find=false;
        if(data!=null && data.length>0){
          for(let j=0;j<that.clda.datas.length;j++){
            let jcar=that.clda.datas[j];
            for(let i=0;i<data.length;i++){
              if(jcar.id==data[i]){
                that.gzclsq.editRow.cph=jcar.scph;
                find=true;
              }
            }
          }
        }

        if(!find){
          let confirm =this.alert.create({
            title: '系统提示',
            message: "请先选择车辆！",
            buttons: [
              {
                text: '确定',
                handler: () => {
                  that.selectCarShow(that);
                }
              }
            ]
          });
          confirm.present();
        }

      }
    });
    alert.present();
  };
}