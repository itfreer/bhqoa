import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import { DataService } from '../../app/core/data/DataService';
import { BaseService } from '../../app/core/data/BaseService';
import { SecurityService } from '../../app/core/security/SecurityService';
import { LoginPage } from '../../pages/login/login';
import { BpmObject } from '../../app/bpm/BpmObject';
import { LoadingController } from 'ionic-angular';
@Component({
  templateUrl: 'bpmsign.html'
})

export class BpmSignPage {
  /**
   * 用户
   */
  user={};

  public bpmObject:BpmObject=new BpmObject(this.dataService,
    this.baseSErvice,this.changeDetectorRef,this.navCtrl,this.alert,this.loadingCtrl,);

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private dataService:DataService,
    private baseSErvice:BaseService,
    private alert:AlertController,
	private securityService:SecurityService,
	public changeDetectorRef:ChangeDetectorRef, 
  public loadingCtrl: LoadingController) {
	
    this.bpmObject=this.navParams.data;
    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.user = this.securityService.user;
        this.bpmObject.getNextTask("",null,null);
      },()=>{
        this.goLogin();
      })
    }else{
      this.bpmObject.getNextTask("",null,null);
    }
  }

  goLogin(){
    this.navCtrl.push(LoginPage);
  };
  backPage(){
    this.navCtrl.pop();
  };
}