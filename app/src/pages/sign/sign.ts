import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController, AlertController,LoadingController } from 'ionic-angular';

import { FormObject } from '../../app/form/FormObject';
import { LoginPage } from '../../pages/login/login';
import { SecurityService } from '../../app/core/security/SecurityService';
import { SignHttp } from '../../app/buiness/SignHttp';
import { BaseService } from '../../app/core/data/BaseService';
import { DataService } from '../../app/core/data/DataService';
import { SigndetailsPage } from '../../pages/signdetails/signdetails';

@Component({
  templateUrl: 'sign.html'
})

export class SignPage {
  datas=[];
  user={};
  tableName : string = 'gzqdgl';
  showDetails= false;
  public latitude: any = "";//维度
  public longitude: any = "";//经度
  public formattedAddress: string = "";
  public accuracy:string="";
  //构建formObject对象方式修改，这种方式建立了独立的对象数据
  public gzqdgl:FormObject=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);

  constructor(
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private baseService:BaseService,
    private dataService:DataService,
    private securityService:SecurityService,
    private signHttp:SignHttp,
    public changeDetectorRef:ChangeDetectorRef,
    public loadingCtrl:LoadingController
    ) {
      this.ionViewDidEnter();
  }

  //初始化页面
  ionViewDidEnter(){
    this.user={};
    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.initSuccessSignPage();
      },()=>{
        this.user = null;
        this.goLogin();
      })
    }else{
      this.initSuccessSignPage();
    }
  };

  //跳转到登录界面
  goLogin(){
    this.navCtrl.push(LoginPage);
  };

  /**
   * 成功加载用户签到页面时处理
   */
  initSuccessSignPage(){
    this.user = this.securityService.user;
    this.gzqdgl.sn=this.tableName;
    //根据时间排序
    this.gzqdgl.orderObject={"createtime":1};
    //根据登录用户过滤
    // if(this.user["userid"]!='admin'){
    //   this.gzqdgl.queryObject["kaoqrid:="]=this.user["userid"];
    // }
    this.gzqdgl.pageIndex=1;
    this.gzqdgl.datas=[];
    this.gzqdgl.getEntitys(this.editDatas);
  };

  editDatas(datas:any){
    for (let item = 0; item < datas.length; item++) {
      datas[item].icon='ios-arrow-forward';
    }
  };

  // setQueryUser(){
  //   this.queryObject["userID:="]=this.user["userid"];
  // };

  //打开或关闭数据详细内容
  toggleDetails(data) {
    if (data.showDetails) {
      data.showDetails = false;
      data.icon='ios-arrow-forward';
    } else {
      data.showDetails = true;
      data.icon='ios-arrow-down';
    }
  };

  gzqdgldata(method:string,type:string){
    this.signHttp.gzqdgldata(method,type,this.latitude,this.longitude,this.formattedAddress,this.accuracy).subscribe(data => {
      if(data){
        let time=new Date();
        let strtime=time.getHours()+"时"+time.getMinutes()+"分"+time.getSeconds()+"秒";
        let alert = this.alertCtrl.create({
          title: '签到成功',
          subTitle: '签到时间:'+strtime,
          buttons: ['确定']
        });
        this.gzqdgl.pageIndex=1;
        this.gzqdgl.datas=[];
        this.gzqdgl.getEntitys(this.editDatas);
        alert.present();
      }
    }, err => {
      let alert = this.alertCtrl.create({
        title: '系统提示',
        subTitle: '签到失败，请重试！',
        buttons: ['确定']
      });
      alert.present();
    });
  };

  backPage(){
    this.navCtrl.pop();
  };

  //查询
  onSearchKeyUp(event){
    if("Enter"==event.key){
      //if(this.user!=null && this.user["userid"]!=null){
        // this.formObject.pageIndex=0;
        // this.formObject.dataOver=false;
        // this.formObject.getEntitys();
      }
    //}else{
      //this.queryValue="";
    //}
  };

  goToDetails(row:any){
    this.navCtrl.push(SigndetailsPage,row);
  }
}
