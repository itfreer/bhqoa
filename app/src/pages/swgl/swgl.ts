import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController, NavParams,AlertController,ActionSheetController } from 'ionic-angular';
import { DataService } from '../../app/core/data/DataService';
import { BaseService } from '../../app/core/data/BaseService';
import { SecurityService } from '../../app/core/security/SecurityService';
import { LoginPage } from '../login/login';
import { BpmObject } from '../../app/bpm/BpmObject';
import { LoadingController } from 'ionic-angular';
import { BpmBackPage } from '../../pages/bpmback/bpmback';
import { BpmSignPage } from '../../pages/bpmsign/bpmsign';
import { BpmProcessPage } from '../../pages/bpmprocess/bpmprocess';
import { BpmImgPage } from '../../pages/bpmimg/bpmimg';
import { AddUserPage } from '../../pages/adduser/adduser';

@Component({
  selector: 'page-swgl',
  templateUrl: 'swgl.html'
})

export class SwglPage {
  readonly="";
  solution="不显示";
  haveError=false;
  user={};
  flag=false;

  public gzpublishfile:BpmObject=new BpmObject(this.dataService,this.baseService,
    this.changeDetectorRef,this.navCtrl,this.alert,this.loadingCtrl,);
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private dataService:DataService,
    private baseService:BaseService,
    private alert:AlertController,
	private securityService:SecurityService,
	public changeDetectorRef:ChangeDetectorRef, 
  public loadingCtrl: LoadingController,
  public actionSheetCtrl: ActionSheetController) {
    
	this.gzpublishfile.editRow={};
	this.gzpublishfile.sn="gzpublishfile";
	this.gzpublishfile.bpmKey=this.navParams.data["bk"];
  this.gzpublishfile.id=this.navParams.data["dk"];
  let that=this;

    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.user = this.securityService.user;
		//this.initUserExam();
		this.gzpublishfile.findWork("gzpublishfile",function(data){
      if(data.editRow.gzreceivefile){
        for(var cRow in data.editRow.gzreceivefile){
          if(data.editRow.gzreceivefile[cRow]["jsrid"] == that.user["userid"] || that.user["userid"]=="admin"){
            that.flag=true;
            break;
          }
        }
      }
      if(data.editRow.fj){
        let fj:string=data.editRow.fj;
        let fjs:string[]=fj.split(":");
        data.editRow.fjname=fjs[2];
        data.editRow.fjlj=that.baseService.baseUrl+"/file/plupload/download?bucket="+fjs[0]+"&key="+fjs[1]+"&name="+fjs[2];
      }
    },null);
      },()=>{
        this.goLogin();
      })
    }else{
      this.user = this.securityService.user;
	    this.gzpublishfile.findWork("gzpublishfile",function(data){
        data.editRow.fjlst=[];
        if(data.editRow.gzreceivefile){
          for(var cRow in data.editRow.gzreceivefile){
            if(data.editRow.gzreceivefile[cRow]["jsrid"] == that.user["userid"] || that.user["userid"]=="admin"){
              that.flag=true;
              break;
            }
          }
        }
        if(data.editRow.fj){
          let fj:string=data.editRow.fj;
          let fjlst=fj.split("|");
          for(let k=0;k<fjlst.length;k++){
            let item={fjname:'',fjlj:''};
            let fjs:string[]=fjlst[k].split(":");
            item.fjname=fjs[2];
            item.fjlj=that.baseService.baseUrl+"/file/plupload/download?bucket="+fjs[0]+"&key="+fjs[1]+"&name="+fjs[2];
            data.editRow.fjlst.push(item);
          }
         
        }
      },null);
    }
  }

  editData(data){
   
  }
  goLogin(){
    this.navCtrl.push(LoginPage);
  };
  backPage(){
    this.navCtrl.pop();
  };

  goSignPage(){
    this.navCtrl.push(BpmSignPage,this.gzpublishfile);
  };

  goBackPage(){
    this.navCtrl.push(BpmBackPage,this.gzpublishfile);
  };

  goProcessPage(){
  //this.navCtrl.push(BpmProcessPage,this.gzccgl);
  let actionSheet = this.actionSheetCtrl.create({
    title: '更多',
    buttons: [
      {
        text: '办理过程',
        handler: () => {
          //console.log('Destructive clicked');
          this.navCtrl.push(BpmProcessPage,this.gzpublishfile);
        }
      },{
        text: '流程图',
        handler: () => {
          this.navCtrl.push(BpmImgPage,this.gzpublishfile);
        }
      },{
        text: '取消',
        role: '取消',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
    ]
  });
  actionSheet.present();
};

  

  // query(){
  //   this.alert.create({
  //     title:"系统提示",
  //     message:"是否确认?",
  //     buttons:[{
  //       text:"确定",
  //       role:"确定",
  //       handler:()=>{
  //         let data=this.gzpublishfile.editRow;
  //         data.sfqr="1";
  //         this.dataService.update(this.gzpublishfile.sn,data);
  //         this.navCtrl.pop();
  //       }
  //     },{
  //       text:"取消",
  //       role:"取消",
  //       handler:()=>{
  //       }
  //     }]
  //   }).present();
  // };

  addUser(){
    if(this.gzpublishfile.editRow==null){
      this.gzpublishfile.editRow={};
    }
    if(this.gzpublishfile.editRow.ccsxry==null){
      this.gzpublishfile.editRow.ccsxry=[];
    }
    this.navCtrl.push(AddUserPage,this.gzpublishfile.editRow.gzreceivefile);
  }
}