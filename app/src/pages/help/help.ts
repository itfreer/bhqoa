import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  templateUrl: 'help.html'
})
export class HelpPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

}
