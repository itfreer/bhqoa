import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController, NavParams,AlertController,ActionSheetController } from 'ionic-angular';
import { DataService } from '../../app/core/data/DataService';
import { BaseService } from '../../app/core/data/BaseService';
import { SecurityService } from '../../app/core/security/SecurityService';
import { DictionaryService } from '../../app/core/dictionary/DictionaryService';
import { LoginPage } from '../../pages/login/login';
import { BpmObject } from '../../app/bpm/BpmObject';
import { BpmBackPage } from '../../pages/bpmback/bpmback';
import { BpmSignPage } from '../../pages/bpmsign/bpmsign';
import { BpmProcessPage } from '../../pages/bpmprocess/bpmprocess';
import { LoadingController } from 'ionic-angular';
import { BpmImgPage } from '../../pages/bpmimg/bpmimg';
import{ DateUtil } from '../../app/utils/DateUtil';

@Component({
  selector: 'page-vehicle',
  templateUrl: 'vehicle.html'
})

export class VehiclePage {
  user={};

    /**
   * 请假类别
   */
  qjlb:DictionaryService=new DictionaryService(this.dataService,this.baseSErvice,);

  public gzclsq:BpmObject=new BpmObject(this.dataService,this.baseSErvice,
    this.changeDetectorRef,this.navCtrl,this.alert,this.loadingCtrl,);
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private dataService:DataService,
    private baseSErvice:BaseService,
    private alert:AlertController,
	private securityService:SecurityService,
  public changeDetectorRef:ChangeDetectorRef, 
  public loadingCtrl: LoadingController,
  public actionSheetCtrl: ActionSheetController,
  public dateUtil:DateUtil) {
    if(this.navParams.data!=null && this.navParams.data["FIELD_ISNEW"]){
      this.gzclsq.editRow={};
      this.gzclsq.editRow.id=this.navParams.data["dk"];
      this.gzclsq.editRow.bpmkey=this.navParams.data["bk"];
      this.gzclsq.editRow.FIELD_ISNEW=this.navParams.data["FIELD_ISNEW"];
      /*
      this.gzclsq.editRow.sqrq=this.gzclsq.formatDateToSecond(new Date());
      this.gzclsq.editRow.qjts="1";
      this.gzclsq.editRow.sjsjjd="08";
      this.gzclsq.editRow.jssjjd="18";
      this.gzclsq.editRow.kssj=this.gzclsq.formatDate(new Date());
      this.gzclsq.editRow.jssj=this.gzclsq.formatDate(new Date());*/
    }

    this.gzclsq.sn="gzclsq";
    this.gzclsq.bpmKey=this.navParams.data["bk"];
    this.gzclsq.id=this.navParams.data["dk"];

    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.user = this.securityService.user;
        if(this.navParams.data!=null && this.navParams.data["bk"]!=null){
          this.gzclsq.findWork("gzclsq",null,null);
        }
      },()=>{
        this.goLogin();
      })
    }else{
      this.user = this.securityService.user;
      if(this.navParams.data!=null && this.navParams.data["bk"]!=null){
        this.gzclsq.findWork("gzclsq",null,null);
      }
    }
    if(this.navParams.data["FIELD_ISNEW"]){
      this.gzclsq.editRow.ycbm=this.user["departmentId"];
      this.gzclsq.editRow.ycbmName=this.user["departmentName"];
      this.gzclsq.editRow.ssqrid=this.user["userid"];
      this.gzclsq.editRow.ssqr=this.user["userName"];
      this.gzclsq.editRow.id= this.gzclsq.getGUID();
    }
  }

  goLogin(){
    this.navCtrl.push(LoginPage);
  };
  backPage(){
    this.navCtrl.pop();
  };

  goSignPage(){
    this.navCtrl.push(BpmSignPage,this.gzclsq);
  };

  goBackPage(){
    this.navCtrl.push(BpmBackPage,this.gzclsq);
  };

  goProcessPage(){
    let actionSheet = this.actionSheetCtrl.create({
      title: '更多',
      buttons: [
        {
          text: '办理过程',
          handler: () => {
            this.navCtrl.push(BpmProcessPage,this.gzclsq);
          }
        },{
          text: '流程图',
          handler: () => {
            this.navCtrl.push(BpmImgPage,this.gzclsq);
          }
        },{
          text: '取消',
          role: '取消',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  };

  ionViewDidLoad() {
    //console.log('ionViewDidLoad LeaveNewPage');
    this.qjlb.getDatas("GZ_QJLX","0",false,null,null);
  };

  userFormValidate(){
    let message=[];
    if(this.gzclsq.editRow["djysj"]==null || this.gzclsq.editRow["djysj"]==""){
      message.push("<span style='color:red'>开始时间<br></span>");
    }


    if(this.gzclsq.editRow["dghsj"]==null || this.gzclsq.editRow["dghsj"]==""){
      message.push("<span style='color:red'>结束时间<br></span>");
    }

    
    if(this.gzclsq.editRow["ssqyy"]==null || this.gzclsq.editRow["ssqyy"]==""){
      message.push("<span style='color:red'>用车事由<br></span>");
    }

    if(this.gzclsq.editRow["ycdd"]==null || this.gzclsq.editRow["ycdd"]==""){
      message.push("<span style='color:red'>用车地点<br></span>");
    }

    if(this.gzclsq.editRow["ycrs"]==null || this.gzclsq.editRow["ycrs"]==""){
      message.push("<span style='color:red'>用车人数<br></span>");
    }

    // if(this.gzclsq.editRow["jsy"]==null || this.gzclsq.editRow["jsy"]==""){
    //   message.push("<span style='color:red'>驾驶员<br></span>");
    // }

    // if(this.gzclsq.editRow["slxfs"]==null || this.gzclsq.editRow["slxfs"]==""){
    //   message.push("<span style='color:red'>驾驶员联系方式<br></span>");
    // }


    if(this.gzclsq.editRow["dsqsj"]==null || this.gzclsq.editRow["dsqsj"]==""){
      message.push("<span style='color:red'>申请时间<br></span>");
    }

    if(message.length>0){
      let strm=message.join("");
      let temp = this.alert.create({
        title: '系统提示',
        subTitle: '以下表单项没有填写正确! <br>'+strm,
        buttons: ['确定']
      });
      temp.present();
      return false;
    }else{
      return true;
    }
   
  };



  /**
   * 设置天数
   */
  setTs(e:any){
    let that = this;
    setTimeout(()=>{
      let start=that.dateUtil.parseDate(that.gzclsq.editRow.kssj,"string");
      // var fmstart = $rootScope.parseData(start,'string');
       let end=that.dateUtil.parseDate(that.gzclsq.editRow.jssj,"string");
      // var fmend = $rootScope.parseData(end,'string');
       let i=(end - start) / 1000 / 60 / 60 /24+1;
       
       let time=Number(i.toFixed(0));
       if(that.gzclsq.editRow.sjsjjd==14 || that.gzclsq.editRow.sjsjjd=='14'){
         time=time-0.5;
       }
       
       if(that.gzclsq.editRow.jssjjd==12 || that.gzclsq.editRow.jssjjd=='12'){
         time=time-0.5;
       }
       that.gzclsq.editRow.qjts=time;
    },200);

  };
}