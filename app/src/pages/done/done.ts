import { Component,ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { ProcessPage } from '../process/process';
import { FormObject } from '../../app/form/FormObject';
import { LoginPage } from '../../pages/login/login';
import { SecurityService } from '../../app/core/security/SecurityService';
import { BaseService } from '../../app/core/data/BaseService';
import { DataService } from '../../app/core/data/DataService';
import { CcglPage } from '../../pages/ccgl/ccgl';
import { SwglPage } from '../../pages/swgl/swgl';
import { QjglPage } from '../../pages/qjgl/qjgl';
import { WlkyxmPage } from '../../pages/wlkyxm/wlkyxm';
import { YcsqPage } from '../../pages/ycsq/ycsq';
import { YzglPage } from '../../pages/yzgl/yzgl';
import { ZysqPage } from '../../pages/zysq/zysq';
import { WjryPage } from '../../pages/wjry/wjry';
import { KyxmPage } from '../../pages/kyxm/kyxm';

/**
 * Generated class for the TodoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-done',
  templateUrl: 'done.html',
})
export class DonePage {
  workInput:string="";
  datas=[];
  user={};
  showDetails= false;
  //构建formObject对象方式修改，这种方式建立了独立的对象数据
  public overworklist:FormObject=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);


  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private baseService:BaseService,
    private dataService:DataService,
    private securityService:SecurityService,
    public changeDetectorRef:ChangeDetectorRef,
    public loadingCtrl: LoadingController) {
  }

  searchWork(){
    //this.worklist.queryObject={"sbusiness:like":this.workInput?this.workInput:""};
    if(this.workInput==null || this.workInput==""){
      this.overworklist.queryObject={};
    }else{
      this.overworklist.queryObject["sbusiness:like"]=this.workInput;
    }
    this.overworklist.pageIndex=1;//重置查询条件的时候，需要重置页码
   
    this.overworklist.datas=[];
    this.overworklist.getEntitys(null);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TodoPage');
  }
  buttonClick(){
    console.log('buttonClick');
  }

  toProcessPage(){
    this.navCtrl.push(ProcessPage);
  };

  toQueryWork(data:any){
    let para={};
    para["bk"]=data.actdefid;
    para["fn"]=data.prjforname;
    para["vi"]=data.prjpopname;
    para["dk"]=data.sbussinessid;
    //出差
    if(para["bk"]=="ccgl" 
      || para["bk"]=="ccgl_jjgb"
      || para["bk"]=="ccgl_kjgb"
      || para["bk"]=="ccgl_zrjbgb"){
      this.navCtrl.push(CcglPage,para);
    }
    //收发文
    if(para["bk"]=="sfwgl"
      || para["bk"]=="sfwgl_jzgb"
      || para["bk"]=="sfwgl_kszysj"
      || para["bk"]=="sfwgl_zrjbgb" ){
      this.navCtrl.push(SwglPage,para);
    }
    //外来科研项目
    if(para["bk"]=="wlkyxmrqxk"){
      this.navCtrl.push(WlkyxmPage,para);
    }
    //用车申请
    if(para["bk"]=="clsysq"
      || para["bk"]=="clsysq_kjgb"
      || para["bk"]=="clsysq_zrjbgb"){
      this.navCtrl.push(YcsqPage,para);
    }
    //用章管理
    if(para["bk"]=="yzgl"
      || para["bk"]=="yzgl_zrjbgb"){
      this.navCtrl.push(YzglPage,para);
    }

     //资源申请
    if(para["bk"]=="zydd"
     || para["bk"]=="zydd"){
     this.navCtrl.push(ZysqPage,para);
    }

    //请假管理
    if(para["bk"]=="qjgl"
      || para["bk"]=="qjgl_jjgb"
      || para["bk"]=="qjgl_kjgb"){
      this.navCtrl.push(QjglPage,para);
    }
    //外籍人员入区许可办理
    if(para["bk"]=="wjryrqxk"){
      this.navCtrl.push(WjryPage,para);
    }
    //科研项目管理
    if(para["bk"]=="kyxmgl"){
      this.navCtrl.push(KyxmPage,para);
    }
  };



  //初始化页面
  ionViewDidEnter(){
    this.user={};
    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.user= this.securityService.user;
        this.initSuccessHomePage();
      },()=>{
        this.user = {};
        this.goLogin();
      })
    }else{
      this.user= this.securityService.user;
      this.initSuccessHomePage();
    }
  };
  
  //跳转到登录界面
  goLogin(){
    this.navCtrl.push(LoginPage);
  };

  /**
   * 成功加载用户home页面时处理
   */
  initSuccessHomePage(){
    this.user = this.securityService.user;
    this.overworklist=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);
    this.overworklist.sn="overworklist";
    this.overworklist.orderObject={'fromtime':1};
    this.overworklist.getEntitys(null);
  };


}
