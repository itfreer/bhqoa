import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController,LoadingController  } from 'ionic-angular';

import { FormObject } from '../../app/form/FormObject';
import { LoginPage } from '../../pages/login/login';
import { SecurityService } from '../../app/core/security/SecurityService';
import { BaseService } from '../../app/core/data/BaseService';
import { DataService } from '../../app/core/data/DataService';
import { RcapPage } from '../../pages/rcap/rcap';
import { WfbdPage } from '../../pages/wfbd/wfbd';

@Component({
  templateUrl: 'approve.html'
})

export class ApprovePage {
  pet: string ="指派给我的";
  datas=[];
  user={};
  tableName : string = 'gztaskview';
  showDetails= false;
  //构建formObject对象方式修改，这种方式建立了独立的对象数据
  public gztaskview:FormObject=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);
  public schedule:FormObject=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);
  public gzccgl:FormObject=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);

  constructor(
    private navCtrl: NavController,
    private baseService:BaseService,
    private dataService:DataService,
    private securityService:SecurityService,
    public changeDetectorRef:ChangeDetectorRef,
    public loadingCtrl: LoadingController

    ) {
   
  }

  toQueryTask(data:any){
    let para=data;
    this.navCtrl.push(RcapPage,para);
  };

  toQuerySchedule(data:any){
    let para=data;
    this.navCtrl.push(WfbdPage,para);
  };

  //初始化页面
  ionViewDidEnter(){
    this.user={};
    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.initSuccessApprovePage();
      },()=>{
        this.user = null;
        this.goLogin();
      })
    }else{
      this.initSuccessApprovePage();
    }
  };

  //跳转到登录界面
  goLogin(){
    this.navCtrl.push(LoginPage);
  };

  /**
   * 成功加载用户任务管理页面时处理
   */
  initSuccessApprovePage(){

    this.user = this.securityService.user;
    //构建formObject对象方式修改，这种方式建立了独立的对象数据
    if(this.gztaskview.sn==""){
      this.gztaskview=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);
      this.gztaskview.sn="gztaskview";
      this.gztaskview.orderObject={"cjsj":1};
    }

    if(this.schedule.sn==""){
      this.schedule=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);
      this.schedule.sn="schedule";
    }

    if(this.gzccgl.sn==""){
      this.gzccgl=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);
      this.gzccgl.sn="gzccgl";
    }

    if(this.tableName=="gztaskview" && this.gztaskview.datas.length==0){
      this.gztaskview.getEntitys(this.editDatas);
    }else if(this.tableName=="schedule" && this.schedule.datas.length==0){
      this.schedule.getEntitys(this.scheduleEditDatas);
    }else if(this.tableName=="gzccgl" && this.schedule.datas.length==0){
      this.gzccgl.getEntitys(this.editDatas);
    }
  };

  editDatas(datas:any){
    for (let item = 0; item < datas.length; item++) {
      datas[item].icon='ios-arrow-forward';
    }
  };

  scheduleEditDatas(datas:any){
    for (let item = 0; item < datas.length; item++) {
      datas[item].icon='ios-arrow-forward';
      //发布将参与人合并为一个字段
      datas[item].cyr="";
      for(let item1 = 0; item1 < datas[item].gztask.length; item1++){
          datas[item].cyr+=datas[item].gztask[item1].jsrmc;
          if(item1!=datas[item].gztask.length-1){
              datas[item].cyr +="，";
          }
      }
    }
  }

  addScheduleRow(){
    let editRow:any={};
    editRow.isNew=true;
    editRow.id=this.schedule.getGUID();
    editRow.bmmc=this.user["departmentId"];
    editRow.bmmcName= this.user["departmentName"];
    editRow.cjrid=this.user["userid"];
    editRow.cjrmc=this.user["userName"];
    editRow.cjsj=new Date().getTime();;
    editRow.zt='rwzt_wyd';
    editRow.ztName= '未阅读';
    editRow.gztask=[];
    this.toQuerySchedule(editRow);
  }

  setTable(formName: string){
    if(formName=="gztaskview"){
      if(this.gztaskview.datas && this.gztaskview.datas.length>0){

      }else{
        this.gztaskview.getEntitys(this.editDatas);
      }
      
    }else if(formName=="schedule"){
      if(this.schedule.datas && this.schedule.datas.length>0){

      }else{
        this.schedule.getEntitys(this.scheduleEditDatas);
      }
    }else{
      if(this.gzccgl.datas && this.gzccgl.datas.length>0){

      }else{
        this.gzccgl.getEntitys(this.editDatas);
      }
    }
    this.tableName = formName;
  }

  /**
   * 重置并查询数据
   */
  resetAndLoadDatas(formName: string) {
    if(formName=="gztaskview"){
      this.gztaskview.pageIndex=1;
      this.gztaskview.dataOver=false;
      this.gztaskview.sn=formName;
      this.gztaskview.queryObject={};
      this.gztaskview.getEntitys(this.editDatas);
    }else{
      this.schedule.pageIndex=1;
      this.schedule.dataOver=false;
      this.schedule.sn=formName;
      this.schedule.queryObject={};
      this.schedule.getEntitys(this.scheduleEditDatas);
    }
    this.tableName = formName;
  };

  //打开或关闭数据详细内容
  toggleDetails(data) {
    if (data.showDetails) {
      data.showDetails = false;
      data.icon='ios-arrow-forward';
    } else {
      data.showDetails = true;
      data.icon='ios-arrow-down';
    }
  }

  //查询
  onSearchKeyUp(event){
    if("Enter"==event.key){
      //if(this.user!=null && this.user["userid"]!=null){
        // this.formObject.pageIndex=0;
        // this.formObject.dataOver=false;
        // this.formObject.getEntitys();
      }
    //}else{
      //this.queryValue="";
    //}
  }

}
