import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';

import { NavController, NavParams ,AlertController} from 'ionic-angular';
import { SecurityService } from '../../app/core/security/SecurityService';

@Component({
  selector: 'page-login',
  templateUrl: 'login2.html',
})
export class LoginPage {

  // 是否显示登录界面
  private username:string;
  private password:string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private securityService:SecurityService,
    private alterCtrl:AlertController,
    public platform:Platform) {
      this.securityService.showMessage=true;
      try{
        this.username=window.localStorage.getItem('bhqoa_username');
        this.password=window.localStorage.getItem('bhqoa_password');
      }catch(e){

      }
  }

  login(){

    let that=this;
    this.securityService.login(this.username, this.password, (data)=>{
     
      window.localStorage.setItem('bhqoa_username',that.username);
      window.localStorage.setItem('bhqoa_password',that.password);
      window.localStorage.setItem('bhqoa_initLogin',"true");
     // this.navCtrl.push(TabsPage);
      this.navCtrl.pop();
    });
  }

  exitApp(){
    //this.platform.exitApp();
    this.alterCtrl.create({
      title:"系统提示",
      message:"是否退出系统？",
      buttons:[{
        text:"确定",
        role:"确定",
        handler:()=>{
          this.platform.exitApp();
        }
      },{
        text:"取消",
        role:"取消",
        handler:()=>{
        }
      }]
      }).present();
  }

}