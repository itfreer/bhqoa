import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController,LoadingController } from 'ionic-angular';

import { FormObject } from '../../app/form/FormObject';
import { LoginPage } from '../../pages/login/login';
import { SecurityService } from '../../app/core/security/SecurityService';
import { BaseService } from '../../app/core/data/BaseService';
import { DataService } from '../../app/core/data/DataService';
import { CcglPage } from '../../pages/ccgl/ccgl';

@Component({
  templateUrl: 'trip.html'
})

export class TripPage {
  datas=[];
  user={};
  tableName : string = 'gzccgl';
  showDetails= false;
  //构建formObject对象方式修改，这种方式建立了独立的对象数据
  public gzccgl:FormObject=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);

  constructor(
    private navCtrl: NavController,
    private baseService:BaseService,
    private dataService:DataService,
    private securityService:SecurityService,
    public changeDetectorRef:ChangeDetectorRef,
    public loadingCtrl: LoadingController
    ) {
      this.user={};
      if(this.securityService.user==null || this.securityService.user["userid"]==null){
        this.securityService.getUserInfo(()=>{
          this.initSuccessTripPage();
        },()=>{
          this.user = null;
          this.goLogin();
        })
      }else{
        this.initSuccessTripPage();
      }
  }

  //初始化页面
  ionViewDidEnter(){
    this.user={};
    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.initSuccessTripPage();
      },()=>{
        this.user = null;
        this.goLogin();
      })
    }else{
      this.initSuccessTripPage();
    }
  };

  toQueryWork(data:any){
    let para={};
    para["bk"]=data.bpmkey;
    para["fn"]="gzccgl";
    para["vi"]="gzccgl";
    para["dk"]=data.id;
    this.navCtrl.push(CcglPage,para);
  };

  startNewBpm(bpmkey:string,formname:string){
    let para={};
    para["bk"]=bpmkey;
    para["fn"]=formname;
    para["vi"]="gzccgl";
    para["dk"]=this.gzccgl.getGUID();
    para["FIELD_ISNEW"]=true;
    this.navCtrl.push(CcglPage,para);
  }

  editDatas(datas:any){
    for (let item = 0; item < datas.length; item++) {
      datas[item].icon='ios-arrow-forward';
    }
  };
  
  //跳转到登录界面
  goLogin(){
    this.navCtrl.push(LoginPage);
  };

  /**
   * 成功加载用户出差页面时处理
   */
  initSuccessTripPage(){
    this.user = this.securityService.user;
    //根据登录用户过滤
    // if(this.user["userid"]!='admin'){
    //   this.formObject.queryObject["qjrid:="]=this.user["userid"];
    // }
    this.gzccgl.datas=[];
    this.gzccgl.dataOver=false;
    this.gzccgl.orderObject["kssj"]=1;
    this.gzccgl.pageIndex=1;
    this.gzccgl.sn=this.tableName;
    this.gzccgl.getEntitys(this.editDatas);
  };

  // setQueryUser(){
  //   this.queryObject["userID:="]=this.user["userid"];
  // };

  //打开或关闭数据详细内容
  toggleDetails(data) {
    if (data.showDetails) {
      data.showDetails = false;
      data.icon='ios-arrow-forward';
    } else {
      data.showDetails = true;
      data.icon='ios-arrow-down';
    }
  }

  //查询
  onSearchKeyUp(event){
    if("Enter"==event.key){
      //if(this.user!=null && this.user["userid"]!=null){
        // this.formObject.pageIndex=0;
        // this.formObject.dataOver=false;
        // this.formObject.getEntitys();
      }
    //}else{
      //this.queryValue="";
    //}
  }
}
