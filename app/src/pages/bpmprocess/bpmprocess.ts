import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import { DataService } from '../../app/core/data/DataService';
import { BaseService } from '../../app/core/data/BaseService';
import { SecurityService } from '../../app/core/security/SecurityService';
import { LoginPage } from '../../pages/login/login';
import { BpmObject } from '../../app/bpm/BpmObject';
import { BpmImgPage } from '../../pages/bpmimg/bpmimg';
import { LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-bpmprocess',
  templateUrl: 'bpmprocess.html',
})

export class BpmProcessPage {
  user={};
  /**
   * 定义一个工作流对象
   */
  public bpmObject:BpmObject=new BpmObject(this.dataService,
  this.baseSErvice,this.changeDetectorRef,this.navCtrl,this.alert,this.loadingCtrl,);

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private dataService:DataService,
    private baseSErvice:BaseService,
    private alert:AlertController,
	  private securityService:SecurityService,
	  public changeDetectorRef:ChangeDetectorRef, 
    public loadingCtrl: LoadingController) {

    this.bpmObject=this.navParams.data;

    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.user = this.securityService.user;
        this.bpmObject.loadBpmProcess();
      },()=>{
        this.goLogin();
      })
    }else{
      this.user = this.securityService.user;
      this.bpmObject.loadBpmProcess();
    }
  }

  goLogin(){
    this.navCtrl.push(LoginPage);
  };
  backPage(){
			this.navCtrl.pop();
  };

  //打开或关闭数据详细内容
  toggleDetails(data) {
    if (data.showDetails) {
      data.showDetails = false;
      data.icon='ios-arrow-forward';
    } else {
      data.showDetails = true;
      data.icon='ios-arrow-down';
    }
  };

  /**
   * 加载图片
   */
  loadProcessImage(){
    this.navCtrl.push(BpmImgPage,this.bpmObject);
  }
}