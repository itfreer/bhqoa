import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SecurityService } from '../../app/core/security/SecurityService';

@Component({
  templateUrl: 'password.html'
})
export class PasswordPage {

	private password:string;
  	private rpassword:string;

  	constructor(private navCtrl: NavController, 
  		private securityService:SecurityService) {

  	}

	setPassword(){
    	this.securityService.setPassword(this.password,this.rpassword,()=>{
      		this.navCtrl.pop();
    	});
  	}
}