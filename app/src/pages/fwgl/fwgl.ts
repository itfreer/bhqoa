import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController, NavParams,AlertController,ActionSheetController  } from 'ionic-angular';
import { DataService } from '../../app/core/data/DataService';
import { BaseService } from '../../app/core/data/BaseService';
import { SecurityService } from '../../app/core/security/SecurityService';
import { LoginPage } from '../../pages/login/login';
import { BpmObject } from '../../app/bpm/BpmObject';
import { BpmBackPage } from '../../pages/bpmback/bpmback';
import { BpmSignPage } from '../../pages/bpmsign/bpmsign';
import { BpmProcessPage } from '../../pages/bpmprocess/bpmprocess';
import { LoadingController } from 'ionic-angular';
import { BpmImgPage } from '../../pages/bpmimg/bpmimg';

@Component({
  selector: 'page-fwgl',
  templateUrl: 'fwgl.html',
})
export class FwglPage {
  user={};

  public gzpublishfile:BpmObject=new BpmObject(this.dataService,this.baseSErvice,
    this.changeDetectorRef,this.navCtrl,this.alert,this.loadingCtrl,);
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private dataService:DataService,
    private baseSErvice:BaseService,
    private alert:AlertController,
	private securityService:SecurityService,
  public changeDetectorRef:ChangeDetectorRef, 
  public loadingCtrl: LoadingController,
  public actionSheetCtrl: ActionSheetController ) {
    if(this.navParams.data!=null && this.navParams.data["bk"]!=null){
      this.gzpublishfile.editRow={};
      this.gzpublishfile.sn="gzpublishfile";
      this.gzpublishfile.bpmKey=this.navParams.data["bk"];
      this.gzpublishfile.id=this.navParams.data["dk"];
      this.gzpublishfile.editRow.id=this.navParams.data["dk"];
      this.gzpublishfile.editRow.bpmKey=this.navParams.data["bk"];
      this.gzpublishfile.editRow.FIELD_ISNEW=this.navParams.data["FIELD_ISNEW"];
      this.gzpublishfile.editRow.cjsj=this.gzpublishfile.formatDateToSecond(new Date());;
    }


    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
      this.user = this.securityService.user;
      this.gzpublishfile.editRow.cjrid=this.user["userid"];
      this.gzpublishfile.editRow.cjrmc=this.user["userName"];
      this.gzpublishfile.editRow.bmmc=this.user["departmentId"];
      this.gzpublishfile.editRow.bmmcName=this.user["departmentName"];
      if(this.navParams.data!=null && this.navParams.data["bk"]!=null){
        this.gzpublishfile.findWork("gzpublishfile",null,null);
      }
      },()=>{
        this.goLogin();
      })
    }else{
      this.user = this.securityService.user;
      this.gzpublishfile.editRow.cjrid=this.user["userid"];
      this.gzpublishfile.editRow.cjrmc=this.user["userName"];
      this.gzpublishfile.editRow.bmmc=this.user["departmentId"];
      this.gzpublishfile.editRow.bmmcName=this.user["departmentName"];
      if(this.navParams.data!=null && this.navParams.data["bk"]!=null){
        this.gzpublishfile.findWork("gzpublishfile",null,null);
      }
    }
  }

  goLogin(){
    this.navCtrl.push(LoginPage);
  };
  backPage(){
    this.navCtrl.pop();
  };

  goSignPage(){
    this.navCtrl.push(BpmSignPage,this.gzpublishfile);
  };

  goBackPage(){
    this.navCtrl.push(BpmBackPage,this.gzpublishfile);
  };

  goProcessPage(){
    //this.navCtrl.push(BpmProcessPage,this.gzpublishfile);
    let actionSheet = this.actionSheetCtrl.create({
      title: '更多',
      buttons: [
        {
          text: '办理过程',
          handler: () => {
            //console.log('Destructive clicked');
            this.navCtrl.push(BpmProcessPage,this.gzpublishfile);
          }
        },{
          text: '流程图',
          handler: () => {
            this.navCtrl.push(BpmImgPage,this.gzpublishfile);
          }
        },{
          text: '取消',
          role: '取消',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  };
}