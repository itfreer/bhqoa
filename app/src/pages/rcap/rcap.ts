import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController, NavParams,AlertController,LoadingController } from 'ionic-angular';
import { DataService } from '../../app/core/data/DataService';
import { BaseService } from '../../app/core/data/BaseService';
import { SecurityService } from '../../app/core/security/SecurityService';
import { LoginPage } from '../../pages/login/login';
import { FormObject } from '../../app/form/FormObject';
import{ DateUtil } from '../../app/utils/DateUtil';

@Component({
  selector: 'page-rcap',
  templateUrl: 'rcap.html'
})

export class RcapPage {
  readonly="";
  solution="不显示";
  haveError=false;
  user={};

  public gztaskview:FormObject=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private dataService:DataService,
    private baseService:BaseService,
    private alert:AlertController,
    private dateUtil:DateUtil,
	private securityService:SecurityService,
  public changeDetectorRef:ChangeDetectorRef,
  public loadingCtrl:LoadingController) {
    this.gztaskview.editRow={};
    this.gztaskview.sn="gztaskview";

    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.user = this.securityService.user;
        this.gztaskview.editRow=this.navParams.data;
      },()=>{
        this.goLogin();
      })
    }else{
      this.user = this.securityService.user;
	    this.gztaskview.editRow=this.navParams.data;
    }
  }

  goLogin(){
    this.navCtrl.push(LoginPage);
  };
  backPage(){
    this.navCtrl.pop();
  };

  query(){
      this.alert.create({
        title:"系统提示",
        message:"是否确认?",
        buttons:[{
          text:"确定",
          role:"确定",
          handler:()=>{
            let data=this.gztaskview.editRow;
            data.zt="rwzt_yyd";
            //data.wcsj=this.dateUtil.getDateFormateCommon(new Date());
            //this.dataService.update(this.gztaskview.sn,data).subscribe((data)=>{
              //this.navCtrl.pop();
            //});
          }
        },{
          text:"取消",
          role:"取消",
          handler:()=>{
          }
        }]
      }).present();
  };

  complete(){
    this.alert.create({
      title:"系统提示",
      message:"是否确认完成?",
      buttons:[{
        text:"确定",
        role:"确定",
        handler:()=>{
          let data=this.gztaskview.editRow;
          data.zt="rwzt_ywc";
          data.wcsj=this.dateUtil.getDateFormateCommon(new Date());
          this.dataService.update(this.gztaskview.sn,data).subscribe((data)=>{
            this.navCtrl.pop();
          });
        }
      },{
        text:"取消",
        role:"取消",
        handler:()=>{
        }
      }]
    }).present();
  };

}