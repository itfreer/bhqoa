import { Component } from '@angular/core';
import { NavParams, NavController, AlertController } from 'ionic-angular';

import { DataService } from './../../../app/core/data/DataService';

@Component({
    selector: 'page-sign-text',
    templateUrl: 'sign_text.html',
})
export class SignTextPage {

  data:any={};

  constructor(
      public navParams: NavParams,
      public navCtrl: NavController, 
      private alert:AlertController,
      public dataService:DataService,
    ) {
    this.data={};
    if(this.navParams.data!=null){
      this.data = this.navParams.data;
      var dkbzs=this.data.dkbz.split('；');
      var dkbz='';
      for(var i=0;i<dkbzs.length;i++){
        if(dkbzs.length>1 && i==0){
          dkbz+='\n';
        }
        if(i!=(dkbzs.length-1)){
          dkbz+=dkbzs[i]+'\n';
        }else{
          dkbz+=dkbzs[i];
        }
      }
      this.data.dkbz=dkbz;
      var bzs=this.data.bz.split('；');
      var bz='';
      for(var j=0;j<bzs.length;j++){
        if(bzs.length>1 && i==0){
          bz+='\n';
        }
        if(j!=(dkbzs.length-1)){
          bz+=bzs[j]+'\n';
        }else{
          bz+=bzs[j];
        }
      }
      this.data.bz=bz;
    }
  }


  update(sn:string){
    this.alert.create({
      title:"系统提示",
      message:"是否确认保存?",
      buttons:[{
        text:"确定",
        role:"确定",
        handler:()=>{
          this.dataService.update(sn,this.data).subscribe((data)=>{
            this.navCtrl.pop();
          });
        }
      },{
        text:"取消",
        role:"取消",
        handler:()=>{
        }
      }]
    }).present();
  };

}