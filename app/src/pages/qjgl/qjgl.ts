import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController, NavParams,AlertController,ActionSheetController } from 'ionic-angular';
import { DataService } from '../../app/core/data/DataService';
import { BaseService } from '../../app/core/data/BaseService';
import { DictionaryService } from '../../app/core/dictionary/DictionaryService';
import { SecurityService } from '../../app/core/security/SecurityService';
import { LoginPage } from '../../pages/login/login';
import { BpmObject } from '../../app/bpm/BpmObject';
import { BpmBackPage } from '../../pages/bpmback/bpmback';
import { BpmSignPage } from '../../pages/bpmsign/bpmsign';
import { BpmProcessPage } from '../../pages/bpmprocess/bpmprocess';
import { LoadingController } from 'ionic-angular';
import { BpmImgPage } from '../../pages/bpmimg/bpmimg';

@Component({
  selector: 'page-qjgl',
  templateUrl: 'qjgl.html'
})

export class QjglPage {
  user={};

  /**
   * 请假类别
   */
  qjlb:DictionaryService=new DictionaryService(this.dataService,this.baseSErvice,);

  public gzqjgl:BpmObject=new BpmObject(this.dataService,this.baseSErvice,
    this.changeDetectorRef,this.navCtrl,this.alert,this.loadingCtrl,);
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private dataService:DataService,
    private baseSErvice:BaseService,
    private alert:AlertController,
	private securityService:SecurityService,
  public changeDetectorRef:ChangeDetectorRef, 
  public loadingCtrl: LoadingController,
  public actionSheetCtrl: ActionSheetController) {

    this.gzqjgl.sn="gzqjgl";
    this.gzqjgl.bpmKey=this.navParams.data["bk"];
    this.gzqjgl.id=this.navParams.data["dk"];

    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.user = this.securityService.user;
    //this.initUserExam();
        if(this.navParams.data!=null && this.navParams.data["bk"]!=null){
          this.gzqjgl.findWork("gzqjgl",null,null);
        }
      },()=>{
        this.goLogin();
      })
    }else{
      this.user = this.securityService.user;
      
      if(this.navParams.data!=null && this.navParams.data["bk"]!=null){
        this.gzqjgl.findWork("gzqjgl",null,null);
      }
    }
  }

  goLogin(){
    this.navCtrl.push(LoginPage);
  };
  backPage(){
    this.navCtrl.pop();
  };

  goSignPage(){
    this.navCtrl.push(BpmSignPage,this.gzqjgl);
  };

  goBackPage(){
    this.navCtrl.push(BpmBackPage,this.gzqjgl);
  };

  goProcessPage(){
    let actionSheet = this.actionSheetCtrl.create({
      title: '更多',
      buttons: [
        {
          text: '办理过程',
          handler: () => {
            this.navCtrl.push(BpmProcessPage,this.gzqjgl);
          }
        },{
          text: '流程图',
          handler: () => {
            this.navCtrl.push(BpmImgPage,this.gzqjgl);
          }
        },{
          text: '取消',
          role: '取消',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  };

  ionViewDidLoad() {
    //console.log('ionViewDidLoad LeaveNewPage');
    this.qjlb.getDatas("GZ_QJLX","0",false,null,null);
  }
}