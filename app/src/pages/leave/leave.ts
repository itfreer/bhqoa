import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController, NavParams,AlertController,ActionSheetController } from 'ionic-angular';
import { DataService } from '../../app/core/data/DataService';
import { BaseService } from '../../app/core/data/BaseService';
import { SecurityService } from '../../app/core/security/SecurityService';
import { DictionaryService } from '../../app/core/dictionary/DictionaryService';
import { LoginPage } from '../../pages/login/login';
import { BpmObject } from '../../app/bpm/BpmObject';
import { BpmBackPage } from '../../pages/bpmback/bpmback';
import { BpmSignPage } from '../../pages/bpmsign/bpmsign';
import { BpmProcessPage } from '../../pages/bpmprocess/bpmprocess';
import { LoadingController } from 'ionic-angular';
import { BpmImgPage } from '../../pages/bpmimg/bpmimg';
import{ DateUtil } from '../../app/utils/DateUtil';

@Component({
  selector: 'page-leave',
  templateUrl: 'leave.html'
})

export class LeavePage {
  user={};

    /**
   * 请假类别
   */
  qjlb:DictionaryService=new DictionaryService(this.dataService,this.baseSErvice,);

  public gzqjgl:BpmObject=new BpmObject(this.dataService,this.baseSErvice,
    this.changeDetectorRef,this.navCtrl,this.alert,this.loadingCtrl,);
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private dataService:DataService,
    private baseSErvice:BaseService,
    private alert:AlertController,
	private securityService:SecurityService,
  public changeDetectorRef:ChangeDetectorRef, 
  public loadingCtrl: LoadingController,
  public actionSheetCtrl: ActionSheetController,
  public dateUtil:DateUtil) {
    if(this.navParams.data!=null && this.navParams.data["FIELD_ISNEW"]){
      this.gzqjgl.editRow={};
      this.gzqjgl.editRow.id=this.navParams.data["dk"];
      this.gzqjgl.editRow.bpmkey=this.navParams.data["bk"];
      this.gzqjgl.editRow.FIELD_ISNEW=this.navParams.data["FIELD_ISNEW"];
      this.gzqjgl.editRow.sqrq=this.gzqjgl.formatDateToSecond(new Date());
      this.gzqjgl.editRow.qjts="1";
      this.gzqjgl.editRow.sjsjjd="08";
      this.gzqjgl.editRow.jssjjd="18";
      this.gzqjgl.editRow.kssj=this.gzqjgl.formatDate(new Date());
      this.gzqjgl.editRow.jssj=this.gzqjgl.formatDate(new Date());
    }

    this.gzqjgl.sn="gzqjgl";
    this.gzqjgl.bpmKey=this.navParams.data["bk"];
    this.gzqjgl.id=this.navParams.data["dk"];

    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.user = this.securityService.user;
        if(this.navParams.data!=null && this.navParams.data["bk"]!=null){
          this.gzqjgl.findWork("gzqjgl",null,null);
        }
      },()=>{
        this.goLogin();
      })
    }else{
      this.user = this.securityService.user;
      
      if(this.navParams.data!=null && this.navParams.data["bk"]!=null){
        this.gzqjgl.findWork("gzqjgl",null,null);
      }
    }
    if(this.navParams.data["FIELD_ISNEW"]){
      this.gzqjgl.editRow.dwid=this.user["departmentId"];
      this.gzqjgl.editRow.szdw=this.user["departmentName"];
      this.gzqjgl.editRow.qjrid=this.user["userid"];
      this.gzqjgl.editRow.qjrxm=this.user["userName"];
    }
  }

  goLogin(){
    this.navCtrl.push(LoginPage);
  };
  backPage(){
    this.navCtrl.pop();
  };

  goSignPage(){
    this.navCtrl.push(BpmSignPage,this.gzqjgl);
  };

  goBackPage(){
    this.navCtrl.push(BpmBackPage,this.gzqjgl);
  };

  goProcessPage(){
    let actionSheet = this.actionSheetCtrl.create({
      title: '更多',
      buttons: [
        {
          text: '办理过程',
          handler: () => {
            this.navCtrl.push(BpmProcessPage,this.gzqjgl);
          }
        },{
          text: '流程图',
          handler: () => {
            this.navCtrl.push(BpmImgPage,this.gzqjgl);
          }
        },{
          text: '取消',
          role: '取消',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  };

  ionViewDidLoad() {
    //console.log('ionViewDidLoad LeaveNewPage');
    this.qjlb.getDatas("GZ_QJLX","0",false,null,null);
  };

  userFormValidate(){
    let message=[];
    if(this.gzqjgl.editRow["qjlx"]==null || this.gzqjgl.editRow["qjlx"]==""){
      message.push("<span style='color:red'>请假类型<br></span>");
    }


    if(this.gzqjgl.editRow["sqsy"]==null || this.gzqjgl.editRow["sqsy"]==""){
      message.push("<span style='color:red'>申请事由<br></span>");
    }

    
    if(this.gzqjgl.editRow["kssj"]==null || this.gzqjgl.editRow["kssj"]==""){
      message.push("<span style='color:red'>开始时间<br></span>");
    }

    if(this.gzqjgl.editRow["sjsjjd"]==null || this.gzqjgl.editRow["sjsjjd"]==""){
      message.push("<span style='color:red'>开始时段<br></span>");
    }

    if(this.gzqjgl.editRow["jssj"]==null || this.gzqjgl.editRow["jssj"]==""){
      message.push("<span style='color:red'>结束时间<br></span>");
    }

    if(this.gzqjgl.editRow["jssjjd"]==null || this.gzqjgl.editRow["jssjjd"]==""){
      message.push("<span style='color:red'>结束时段<br></span>");
    }

    if(this.gzqjgl.editRow["qjts"]==null || this.gzqjgl.editRow["qjts"]==""){
      message.push("<span style='color:red'>请假天数<br></span>");
    }

    if(message.length>0){
      let strm=message.join("");
      let temp = this.alert.create({
        title: '系统提示',
        subTitle: '以下表单项没有填写正确! <br>'+strm,
        buttons: ['确定']
      });
      temp.present();
      return false;
    }else{
      return true;
    }
   
  };



  /**
   * 设置天数
   */
  setTs(e:any){
    let that = this;
    setTimeout(()=>{
      let start=that.dateUtil.parseDate(that.gzqjgl.editRow.kssj,"string");
      // var fmstart = $rootScope.parseData(start,'string');
       let end=that.dateUtil.parseDate(that.gzqjgl.editRow.jssj,"string");
      // var fmend = $rootScope.parseData(end,'string');
       let i=(end - start) / 1000 / 60 / 60 /24+1;
       
       let time=Number(i.toFixed(0));
       if(that.gzqjgl.editRow.sjsjjd==14 || that.gzqjgl.editRow.sjsjjd=='14'){
         time=time-0.5;
       }
       
       if(that.gzqjgl.editRow.jssjjd==12 || that.gzqjgl.editRow.jssjjd=='12'){
         time=time-0.5;
       }
       that.gzqjgl.editRow.qjts=time;
    },200);

  };
}