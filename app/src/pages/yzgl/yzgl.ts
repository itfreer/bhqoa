import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController, NavParams,AlertController,ActionSheetController } from 'ionic-angular';
import { DataService } from '../../app/core/data/DataService';
import { BaseService } from '../../app/core/data/BaseService';
import { SecurityService } from '../../app/core/security/SecurityService';
import { LoginPage } from '../login/login';
import { BpmObject } from '../../app/bpm/BpmObject';
import { LoadingController } from 'ionic-angular';
import { BpmBackPage } from '../../pages/bpmback/bpmback';
import { BpmSignPage } from '../../pages/bpmsign/bpmsign';
import { BpmProcessPage } from '../../pages/bpmprocess/bpmprocess';
import { BpmImgPage } from '../../pages/bpmimg/bpmimg';

@Component({
  selector: 'page-yzgl',
  templateUrl: 'yzgl.html'
})

export class YzglPage {
  readonly="";
  solution="不显示";
  haveError=false;
  user={};

  public gzyzgl:BpmObject=new BpmObject(this.dataService,this.baseSErvice,
    this.changeDetectorRef,this.navCtrl,this.alert,this.loadingCtrl,);
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private dataService:DataService,
    private baseSErvice:BaseService,
    private alert:AlertController,
	private securityService:SecurityService,
	public changeDetectorRef:ChangeDetectorRef, 
  public loadingCtrl: LoadingController,
  public actionSheetCtrl: ActionSheetController) {
	this.gzyzgl.editRow={};
	this.gzyzgl.sn="gzyzgl";
	this.gzyzgl.bpmKey=this.navParams.data["bk"];
	this.gzyzgl.id=this.navParams.data["dk"];

    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.user = this.securityService.user;
		//this.initUserExam();
		this.gzyzgl.findWork("gzyzgl",null,null);
      },()=>{
        this.goLogin();
      })
    }else{
      this.user = this.securityService.user;
	  this.gzyzgl.findWork("gzyzgl",null,null);
    }
  }

  goLogin(){
    this.navCtrl.push(LoginPage);
  };
  backPage(){
    this.navCtrl.pop();
  };

  goSignPage(){
    this.navCtrl.push(BpmSignPage,this.gzyzgl);
  };

  goBackPage(){
    this.navCtrl.push(BpmBackPage,this.gzyzgl);
  };

  goProcessPage(){
    //this.navCtrl.push(BpmProcessPage,this.gzccgl);
    let actionSheet = this.actionSheetCtrl.create({
      title: '更多',
      buttons: [
        {
          text: '办理过程',
          handler: () => {
            //console.log('Destructive clicked');
            this.navCtrl.push(BpmProcessPage,this.gzyzgl);
          }
        },{
          text: '流程图',
          handler: () => {
            this.navCtrl.push(BpmImgPage,this.gzyzgl);
          }
        },{
          text: '取消',
          role: '取消',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  };

}