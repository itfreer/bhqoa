import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController,LoadingController  } from 'ionic-angular';

import { FormObject } from '../../app/form/FormObject';
import { LoginPage } from '../../pages/login/login';
import { SecurityService } from '../../app/core/security/SecurityService';
import { BaseService } from '../../app/core/data/BaseService';
import { DataService } from '../../app/core/data/DataService';
import { SwglPage } from '../../pages/swgl/swgl';
import{ DateUtil } from '../../app/utils/DateUtil';
import{ SwglDonePage } from '../../pages/swgl_done/swgl_done';

@Component({
  selector: 'page-swgl-new',
  templateUrl: 'swgl_new.html'
})

export class SwglNewPage {
  datas=[];
  user={};
  tableName : string = 'gzreceiveview';
  showDetails= false;

  public workInput:string="";
  //构建formObject对象方式修改，这种方式建立了独立的对象数据
  public gzreceiveview:FormObject=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);

  constructor(
    private navCtrl: NavController,
    private baseService:BaseService,
    private dataService:DataService,
    private securityService:SecurityService,
    public changeDetectorRef:ChangeDetectorRef,
    public loadingCtrl: LoadingController,
    private dateUtil:DateUtil,
    ) {
  }

  searchWork(){
    //this.worklist.queryObject={"sbusiness:like":this.workInput?this.workInput:""};
    if(this.workInput==null || this.workInput==""){
      this.gzreceiveview.queryObject={};
    }else{
      this.gzreceiveview.queryObject["fwzt:like"]=this.workInput;
    }
    this.gzreceiveview.pageIndex=1;//重置查询条件的时候，需要重置页码
   
    this.gzreceiveview.datas=[];
    this.gzreceiveview.getEntitys(null);
  }

  toQueryWork(data:any){
    let para={};
    para["bk"]=data.bpmkey;
    para["fn"]="gzpublishfile";
    para["vi"]="gzpublishfile";
    para["dk"]=data.fwid;

    data.sfqr="1";
    data.wcsj=this.dateUtil.getDateFormateCommon(new Date());
    this.dataService.update(this.gzreceiveview.sn,data).subscribe((data)=>{
      this.navCtrl.push(SwglPage,para);
    });
  };

  //初始化页面
  ionViewDidEnter(){
    this.user={};
    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.initSuccessHomePage();
      },()=>{
        this.user = null;
        this.goLogin();
      })
    }else{
      this.initSuccessHomePage();
    }
  };
  
  //跳转到登录界面
  goLogin(){
    this.navCtrl.push(LoginPage);
  };

  //
  toSwglDonePage(){
    this.navCtrl.push(SwglDonePage);
  };

  /**
   * 成功加载用户home页面时处理
   */
  initSuccessHomePage(){
    this.user = this.securityService.user;
    //构建formObject对象方式修改，这种方式建立了独立的对象数据
    if(this.gzreceiveview.sn==""){
      this.gzreceiveview=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);
      this.gzreceiveview.sn="gzreceiveview";
      this.gzreceiveview.queryObject={"sfqr":"0"};
      this.gzreceiveview.orderObject={"jjdj":1,"cjsj":1};
    }
    this.gzreceiveview.getEntitys(null);

  };
}
