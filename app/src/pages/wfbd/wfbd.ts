import { Component,ChangeDetectorRef  } from '@angular/core';
import { NavController, NavParams,AlertController,LoadingController } from 'ionic-angular';
import { DataService } from '../../app/core/data/DataService';
import { BaseService } from '../../app/core/data/BaseService';
import { SecurityService } from '../../app/core/security/SecurityService';
import { LoginPage } from '../../pages/login/login';
import { FormObject } from '../../app/form/FormObject';
import { AddUserPage } from '../../pages/adduser/adduser';

@Component({
  selector: 'page-wfbd',
  templateUrl: 'wfbd.html'
})

export class WfbdPage {
  readonly="";
  solution="不显示";
  haveError=false;
  user={};

  public schedule:FormObject=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private dataService:DataService,
    private baseService:BaseService,
    private alert:AlertController,
	private securityService:SecurityService,
  public changeDetectorRef:ChangeDetectorRef,
  public loadingCtrl:LoadingController) {
    this.schedule.editRow={};
    this.schedule.sn="schedule";

    if(this.navParams.data){
      this.navParams.data.cjsj=this.schedule.formatDate(new Date(this.navParams.data.cjsj));
    }
    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.user = this.securityService.user;
        this.schedule.editRow=this.navParams.data;
        
      },()=>{
        this.goLogin();
      })
    }else{
      this.user = this.securityService.user;
	    this.schedule.editRow=this.navParams.data;
    }
  }

  addUser(para:any){
    this.navCtrl.push(AddUserPage,para);
  }

  goLogin(){
    this.navCtrl.push(LoginPage);
  };
  backPage(){
      this.navCtrl.pop();
  };


  scheduleSave(sn:string,row:any){
    if(row.isNew){
        this.dataService.add(sn,row).subscribe((data)=>{
          if(data.id){
            this.alert.create({
                title:"系统提示",
                subTitle:"保存成功！",
                buttons:["确定"]
                }).present();
          }else{
              this.alert.create({
                  title:"系统提示",
                  subTitle:"保存失败，请重试！",
                  buttons:["确定"]
                }).present();
          }
        });
    }else{
        this.dataService.update(sn,row).subscribe((data)=>{
          if(data.id){
            this.alert.create({
                title:"系统提示",
                subTitle:"保存成功！",
                buttons:["确定"]
                }).present();
          }else{
            this.alert.create({
                title:"系统提示",
                subTitle:"保存失败，请重试！",
                buttons:["确定"]
              }).present();
          }
        });
    }
  }
  query(){
    this.alert.create({
      title:"系统提示",
      message:"是否确认?",
      buttons:[{
        text:"确定",
        role:"确定",
        handler:()=>{
          let data=this.schedule.editRow;
          data.zt="rwzt_yyd";
          this.dataService.update(this.schedule.sn,data);
          this.navCtrl.pop();
        }
      },{
        text:"取消",
        role:"取消",
        handler:()=>{
        }
      }]
    }).present();
};

complete(){
  this.alert.create({
    title:"系统提示",
    message:"是否确认完成?",
    buttons:[{
      text:"确定",
      role:"确定",
      handler:()=>{
        let data=this.schedule.editRow;
        data.zt="rwzt_ywc";
        this.dataService.update(this.schedule.sn,data);
        this.navCtrl.pop();
      }
    },{
      text:"取消",
      role:"取消",
      handler:()=>{
      }
    }]
  }).present();
};

}