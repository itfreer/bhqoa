import { Component, ViewChild } from '@angular/core';
import { Tabs } from 'ionic-angular';
import { DataService } from '../../app/core/data/DataService';
import { SecurityService } from '../../app/core/security/SecurityService';
import { TodoPage } from '../todo/todo';
import { TaskPage } from './../task/task';
import { JobsPage } from '../jobs/jobs';
import { MePage } from '../me/me';

@Component({
  // template:'<ion-tabs #mainTabs class="tabs-icon">'+
  //               '<ion-tab *ngFor="let tabRoot of tabRoots" [root]="tabRoot.root"'+
  //               'tabTitle="{{tabRoot.tabTitle}}" tabIcon="{{tabRoot.tabIcon}}" id="{{tabRoot.tabIcon}}" [tabBadge]="tabRoot.tabBadge" tabBadgeStyle="danger"></ion-tab>'+
  //           '</ion-tabs>'
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})

export class TabsPage {

  public tabRoots: Object[];
  @ViewChild('mainTabs') tabs : Tabs;

  constructor(public dataService:DataService,public securityService:SecurityService,) {
    
    this.tabRoots = [{
        root: JobsPage,
        tabTitle: '工作',
        tabIcon: 'bhq__jobs',
        tabBadge: "jobs",
      },{
        root: TodoPage,
        tabTitle: '待办',
        tabIcon: 'bhq__todo',
        tabBadge: "todo",
      },{
        root: TaskPage,
        tabTitle: '任务',
        tabIcon: 'bhq__task',
      },{
        root: MePage,
        tabTitle: '我的',
        tabIcon: 'bhq__me',
      }
    ];
  }

  getPage(){
    return TodoPage;
  }

  countCarItem(badgeName) {
    let itemNum:any = "";
    if(badgeName=="todo"){
      itemNum=window.localStorage.getItem("todoCount");
    }else{
      itemNum=window.localStorage.getItem("jobNews");
    }
    return itemNum;
  }

}
