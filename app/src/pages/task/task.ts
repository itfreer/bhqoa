import { Component,ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { DonePage } from '../done/done';
import { ProcessPage } from '../process/process';
import { FormObject } from '../../app/form/FormObject';
import { LoginPage } from '../../pages/login/login';
import { SecurityService } from '../../app/core/security/SecurityService';
import { BaseService } from '../../app/core/data/BaseService';
import { DataService } from '../../app/core/data/DataService';
import { WfbdPage } from '../../pages/wfbd/wfbd';
import { RcapPage } from '../../pages/rcap/rcap';

/**
 * Generated class for the TodoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-task',
  templateUrl: 'task.html',
})
export class TaskPage {

  datas=[];
  user={};
  tableName : string = 'gztaskview';
  showDetails= false;
  workInput:string="";
  //构建formObject对象方式修改，这种方式建立了独立的对象数据
  public gztaskview:FormObject=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);


  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private baseService:BaseService,
    private dataService:DataService,
    private securityService:SecurityService,
    public changeDetectorRef:ChangeDetectorRef,
    public loadingCtrl:LoadingController) {
  }

  searchWork(){
    //this.worklist.queryObject={"sbusiness:like":this.workInput?this.workInput:""};
    if(this.workInput==null || this.workInput==""){
      this.gztaskview.queryObject={};
    }else{
      this.gztaskview.queryObject["zhuti:like"]=this.workInput;
    }
    this.gztaskview.pageIndex=1;//重置查询条件的时候，需要重置页码
   
    this.gztaskview.datas=[];
    this.gztaskview.getEntitys(this.editData);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TodoPage');
  }
  buttonClick(){
    console.log('buttonClick');
  }

  toDonePage(){
    this.navCtrl.push(DonePage);
  };
  toProcessPage(){
    this.navCtrl.push(ProcessPage);
  };

  toQueryTask(data:any){
    let para=data;
    if(data.zt=="rwzt_yyd" || data.zt=="rwzt_ywc"){
      this.navCtrl.push(RcapPage,para);
    }else{
      data.zt="rwzt_yyd";
      this.dataService.update(this.gztaskview.sn,data).subscribe((data)=>{
        this.navCtrl.push(RcapPage,para);
      });
    }
   
  };

  //初始化页面
  ionViewDidEnter(){
    this.user={};
    if(this.securityService.user==null || this.securityService.user["userid"]==null){
      this.securityService.getUserInfo(()=>{
        this.user= this.securityService.user;
        this.initSuccessHomePage();
      },()=>{
        this.user = {};
        this.goLogin();
      })
    }else{
      this.user= this.securityService.user;
      this.initSuccessHomePage();
    }
  };
  
  //跳转到登录界面
  goLogin(){
    this.navCtrl.push(LoginPage);
  };

  /**
   * 成功加载用户home页面时处理
   */
  initSuccessHomePage(){
    this.user = this.securityService.user;
    //构建formObject对象方式修改，这种方式建立了独立的对象数据

    //if(this.gztaskview.sn==""){
      this.gztaskview=new FormObject(this.dataService,this.baseService,this.changeDetectorRef,this.loadingCtrl);
      this.gztaskview.sn="gztaskview";
      this.gztaskview.orderObject={"cjsj":1};
    //}

    this.gztaskview.getEntitys(this.editData);
  };

  //对任务数据进行图片标识处理
  editData(data){
    if(data.length>0){
      for(let item=0;item<data.length;item++){
        if(data[item].zt=="rwzt_ywc"){
          data[item].picname="complete";
        }else{
          data[item].picname="not";
        }
      }
    }
  };

  addScheduleRow(){
    let editRow:any={};
    editRow.isNew=true;
    editRow.id=this.gztaskview.getGUID();
    editRow.bmmc=this.user["departmentId"];
    editRow.bmmcName= this.user["departmentName"];
    editRow.cjrid=this.user["userid"];
    editRow.cjrmc=this.user["userName"];
    editRow.cjsj=new Date().getTime();;
    editRow.zt='rwzt_wyd';
    editRow.ztName= '未阅读';
    editRow.gztask=[];
    this.toQuerySchedule(editRow);
  }

  toQuerySchedule(data:any){
    let para=data;
    this.navCtrl.push(WfbdPage,para);
  };

}
