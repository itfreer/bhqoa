webpackJsonp([12],{

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddUserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AddUserPage = /** @class */ (function () {
    function AddUserPage(navCtrl, navParams, dataService, baseService, alert, securityService, changeDetectorRef, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseService = baseService;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        /**
         * 用户
         */
        this.user = {};
        this.selectUser = [];
        this.workInput = "";
        this.selectedAll = false;
        this.queryObject = {};
        this.pageSize = 10000;
        this.pageIndex = 1;
        this.datas = [];
        this.dataOver = true;
        this.orderObject = {};
        this.sn = "vuserinfo";
        console.log("constructor加载了");
        this.selectUser = this.navParams.data;
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.datas = [];
                _this.loadUser();
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.datas = [];
            this.loadUser();
        }
    }
    ;
    AddUserPage.prototype.selectAllUser = function () {
        if (this.datas != null) {
            for (var u = 0; u < this.datas.length; u++) {
                this.datas[u]["userSelected"] = this.selectedAll;
            }
        }
    };
    ;
    AddUserPage.prototype.initSelectUser = function () {
        if (this.datas != null && this.selectUser != null) {
            for (var u = 0; u < this.datas.length; u++) {
                for (var s = 0; s < this.selectUser.length; s++) {
                    if (this.selectUser[s]["jsrid"] == this.datas[u]["userid"]) {
                        this.datas[u]["userSelected"] = true;
                    }
                }
            }
        }
    };
    ;
    AddUserPage.prototype.ionViewDidLoad = function () {
    };
    /**
     * 获取GUID值
     */
    AddUserPage.prototype.getGUID = function () {
        var a = function (a) {
            return 0 > a ? NaN : 30 >= a ? 0 | Math.random() * (1 << a) : 53 >= a ? (0 | 1073741824 * Math.random()) + 1073741824 * (0 | Math.random() * (1 << a - 30)) : NaN;
        };
        var b = function (a, b) {
            for (var c = a.toString(16), d = b - c.length, e = "0"; 0 < d; d >>>= 1, e += e) {
                d & 1 && (c = e + c);
            }
            return c;
        };
        var guidString = b(a(32), 8) + b(a(16), 4) + b(16384 | a(12), 4) + b(32768 | a(14), 4) + b(a(48), 12);
        return guidString;
    };
    ;
    AddUserPage.prototype.chooseUser = function () {
        this.selectUser.splice(0, this.selectUser.length);
        if (this.datas != null) {
            for (var u = 0; u < this.datas.length; u++) {
                if (this.datas[u]["userSelected"]) {
                    var cRow = {};
                    cRow["id"] = this.getGUID();
                    cRow["jsrid"] = this.datas[u].userid;
                    cRow["jsrmc"] = this.datas[u].userName;
                    cRow["sfqr"] = '0';
                    var organizationName = this.datas[u].organizationName ? this.datas[u].organizationName : "";
                    var departmentName = this.datas[u].departmentName ? this.datas[u].departmentName : "";
                    var operatingPostName = this.datas[u].operatingPostName ? this.datas[u].operatingPostName : "";
                    cRow["jsrjg"] = this.datas[u].departmentId;
                    cRow["jsrjgmc"] = this.datas[u].departmentName;
                    this.selectUser.push(cRow);
                }
            }
        }
        this.navCtrl.pop();
        //this.navCtrl.push(CalendarPage);
    };
    AddUserPage.prototype.searchWork = function () {
        if (this.workInput == null || this.workInput == "") {
            this.queryObject = {};
        }
        else {
            this.queryObject["userName:like"] = this.workInput;
        }
        this.pageIndex = 1; //重置查询条件的时候，需要重置页码
        this.datas = [];
        this.loadUser();
    };
    AddUserPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    AddUserPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    AddUserPage.prototype.loadUser = function () {
        var _this = this;
        this.selectedAll = false;
        this.dataService.getEntitys(this.sn, null, this.queryObject, this.orderObject, this.pageSize, this.pageIndex).subscribe(function (data) {
            if (data != null && data.length > 0) {
                for (var item = 0; item < data.length; item++) {
                    var val = data[item];
                    _this.datas.push(val);
                }
                _this.initSelectUser();
                //this.pageIndex=this.pageIndex+1;
                _this.changeDetectorRef.markForCheck();
                _this.changeDetectorRef.detectChanges();
            }
            else {
                _this.dataOver = true;
            }
        });
    };
    AddUserPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-cyr',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\adduser\adduser.html"*/'\n\n<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>选择用户</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <div class="search-bar-content">\n\n        <ion-searchbar placeholder="搜索" (ionInput)="searchWork()" [(ngModel)]="workInput">\n\n        </ion-searchbar>\n\n    </div>\n\n    <ion-list class="approval-list">\n\n        <ion-item>\n\n            <ion-checkbox [(ngModel)]="selectedAll" (click)="selectAllUser();"></ion-checkbox>\n\n            <ion-label>全选</ion-label>\n\n        </ion-item>\n\n        <ion-item  *ngFor="let iuser of datas">\n\n            <ion-checkbox [(ngModel)]="iuser.userSelected" ></ion-checkbox>\n\n            <ion-label> {{iuser.userName}}({{iuser.departmentName}})</ion-label>\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>\n\n<ion-footer>\n\n    <button class="submit-btn" (click)="chooseUser();">确定</button>\n\n</ion-footer>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\adduser\adduser.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], AddUserPage);
    return AddUserPage;
}());

//# sourceMappingURL=adduser.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WlkyxmPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var WlkyxmPage = /** @class */ (function () {
    function WlkyxmPage(navCtrl, navParams, dataService, baseSErvice, alert, securityService, changeDetectorRef, loadingCtrl, actionSheetCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseSErvice = baseSErvice;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.readonly = "";
        this.solution = "不显示";
        this.haveError = false;
        this.user = {};
        this.gzwlkyxmsqb = new __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__["a" /* BpmObject */](this.dataService, this.baseSErvice, this.changeDetectorRef, this.navCtrl, this.alert, this.loadingCtrl, "gzwlkyxmsqb");
        this.gzwlkyxmsqb.editRow = {};
        this.gzwlkyxmsqb.sn = "gzwlkyxmsqb";
        this.gzwlkyxmsqb.bpmKey = this.navParams.data["bk"];
        this.gzwlkyxmsqb.id = this.navParams.data["dk"];
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                //this.initUserExam();
                _this.gzwlkyxmsqb.findWork(null, null);
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.gzwlkyxmsqb.findWork(null, null);
        }
    }
    WlkyxmPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
    };
    ;
    WlkyxmPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    WlkyxmPage.prototype.goSignPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__["a" /* BpmSignPage */], this.gzwlkyxmsqb);
    };
    ;
    WlkyxmPage.prototype.goBackPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__["a" /* BpmBackPage */], this.gzwlkyxmsqb);
    };
    ;
    WlkyxmPage.prototype.goProcessPage = function () {
        var _this = this;
        //this.navCtrl.push(BpmProcessPage,this.gzccgl);
        var actionSheet = this.actionSheetCtrl.create({
            title: '更多',
            buttons: [
                {
                    text: '办理过程',
                    handler: function () {
                        //console.log('Destructive clicked');
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__["a" /* BpmProcessPage */], _this.gzwlkyxmsqb);
                    }
                }, {
                    text: '流程图',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__["a" /* BpmImgPage */], _this.gzwlkyxmsqb);
                    }
                }, {
                    text: '取消',
                    role: '取消',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ;
    WlkyxmPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-wlkyxm',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\wlkyxm\wlkyxm.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>外来科研项目入区许可</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">申请信息</span>\n\n    </ion-list-header>\n\n    <ion-item>\n\n        <span class="bhq-title">单位名称: </span>\n\n        <span class="bhq-content">{{gzwlkyxmsqb.editRow.dwmc}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">项目名称: </span>\n\n        <span class="bhq-content">{{gzwlkyxmsqb.editRow.xmmc}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">是否有介绍信(函): </span>\n\n        <span class="bhq-content">{{gzwlkyxmsqb.editRow.sfyjsxName}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">申请编号: </span>\n\n        <span class="bhq-content">{{gzwlkyxmsqb.editRow.bh}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">是否鉴定合作协议: </span>\n\n        <span class="bhq-content">{{gzwlkyxmsqb.editRow.sjqdhzxyName}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">协议编号: </span>\n\n        <span class="bhq-content">{{gzwlkyxmsqb.editRow.xybh}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">带队负责人名称: </span>\n\n        <span class="bhq-content">{{gzwlkyxmsqb.editRow.ddfzrmc}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">带队负责人职务名称: </span>\n\n        <span class="bhq-content">{{gzwlkyxmsqb.editRow.ddfzrzwmc}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">带队负责人联系电话: </span>\n\n        <span class="bhq-content">{{gzwlkyxmsqb.editRow.ddfzrlxdh}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">人数: </span>\n\n        <span class="bhq-content">{{gzwlkyxmsqb.editRow.renshu}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">来访时间: </span>\n\n        <span class="bhq-content">{{gzwlkyxmsqb.editRow.lfsj | asDate}}~{{gzwlkyxmsqb.editRow.lfsjjssj | asDate}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">活动计划时限: </span>\n\n        <span class="bhq-content">{{gzwlkyxmsqb.editRow.hdjhsx}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">活动范围: </span>\n\n        <span class="bhq-content">{{gzwlkyxmsqb.editRow.hdfw}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">提供活动方案情况: </span>\n\n        <span class="bhq-content">{{gzwlkyxmsqb.editRow.tjhdfaqk}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">项目简介: </span>\n\n        <span class="bhq-content">{{gzwlkyxmsqb.editRow.xmjj}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">注意事项: </span>\n\n        <span class="bhq-content">{{gzwlkyxmsqb.editRow.zysx}}</span>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab2">科研科审批</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzwlkyxmsqb.process.usertask2!=null">\n\n      <div *ngFor="let item of  gzwlkyxmsqb.process.usertask2.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item *ngIf="gzwlkyxmsqb.process.usertask2.editRow!=null && gzwlkyxmsqb.proauth.kssp  && gzwlkyxmsqb.process.usertask2.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzwlkyxmsqb.process.usertask2.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab3">办公室主任审核</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzwlkyxmsqb.process.usertask3!=null">\n\n      <div *ngFor="let item of  gzwlkyxmsqb.process.usertask3.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzwlkyxmsqb.process.usertask3.editRow!=null && gzwlkyxmsqb.proauth.bgssp  && gzwlkyxmsqb.process.usertask3.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzwlkyxmsqb.process.usertask3.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab4">出具通行单</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzwlkyxmsqb.process.usertask4!=null">\n\n      <div *ngFor="let item of  gzwlkyxmsqb.process.usertask4.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzwlkyxmsqb.process.usertask4.editRow!=null && gzwlkyxmsqb.proauth.bgsctxd  && gzwlkyxmsqb.process.usertask4.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzwlkyxmsqb.process.usertask4.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab5">分配到管护站</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzwlkyxmsqb.process.usertask5!=null">\n\n      <div *ngFor="let item of  gzwlkyxmsqb.process.usertask5.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzwlkyxmsqb.process.usertask5.editRow!=null && gzwlkyxmsqb.proauth.fpdghz  && gzwlkyxmsqb.process.usertask5.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzwlkyxmsqb.process.usertask5.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n</ion-content>\n\n\n\n<ion-footer class="bhq-bpm-button">\n\n  <div class="footer-btn" *ngIf="gzwlkyxmsqb.auth.opt_th" (click)="gzwlkyxmsqb.getBackTask(null,null);">\n\n    <ion-icon name="arrow-dropleft-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>退回</p>\n\n  </div>\n\n  <div class="footer-btn" *ngIf="gzwlkyxmsqb.auth.opt_bc" (click)="gzwlkyxmsqb.bpmSave(null);">\n\n    <ion-icon name="lock" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>保存</p>\n\n  </div>\n\n  <div class="footer-btn" *ngIf="gzwlkyxmsqb.auth.opt_tj" (click)="gzwlkyxmsqb.getNextTask(null,null);">\n\n    <ion-icon name="arrow-dropright-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>通过</p>\n\n  </div>\n\n  <!-- <div class="footer-more" (click)="goProcessPage();">...</div> -->\n\n  <div class="footer-btn" (click)="goProcessPage();">\n\n    <ion-icon name="keypad" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>更多</p>\n\n</div>\n\n</ion-footer>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\wlkyxm\wlkyxm.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"]])
    ], WlkyxmPage);
    return WlkyxmPage;
}());

//# sourceMappingURL=wlkyxm.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return YzglPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var YzglPage = /** @class */ (function () {
    function YzglPage(navCtrl, navParams, dataService, baseSErvice, alert, securityService, changeDetectorRef, loadingCtrl, actionSheetCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseSErvice = baseSErvice;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.readonly = "";
        this.solution = "不显示";
        this.haveError = false;
        this.user = {};
        this.gzyzgl = new __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__["a" /* BpmObject */](this.dataService, this.baseSErvice, this.changeDetectorRef, this.navCtrl, this.alert, this.loadingCtrl, "gzyzgl");
        this.gzyzgl.editRow = {};
        this.gzyzgl.sn = "gzyzgl";
        this.gzyzgl.bpmKey = this.navParams.data["bk"];
        this.gzyzgl.id = this.navParams.data["dk"];
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                //this.initUserExam();
                _this.gzyzgl.findWork(null, null);
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.gzyzgl.findWork(null, null);
        }
    }
    YzglPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
    };
    ;
    YzglPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    YzglPage.prototype.goSignPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__["a" /* BpmSignPage */], this.gzyzgl);
    };
    ;
    YzglPage.prototype.goBackPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__["a" /* BpmBackPage */], this.gzyzgl);
    };
    ;
    YzglPage.prototype.goProcessPage = function () {
        var _this = this;
        //this.navCtrl.push(BpmProcessPage,this.gzccgl);
        var actionSheet = this.actionSheetCtrl.create({
            title: '更多',
            buttons: [
                {
                    text: '办理过程',
                    handler: function () {
                        //console.log('Destructive clicked');
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__["a" /* BpmProcessPage */], _this.gzyzgl);
                    }
                }, {
                    text: '流程图',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__["a" /* BpmImgPage */], _this.gzyzgl);
                    }
                }, {
                    text: '取消',
                    role: '取消',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ;
    YzglPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-yzgl',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\yzgl\yzgl.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>用章管理</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">申请信息</span>\n\n    </ion-list-header>\n\n    <ion-item>\n\n        <span class="bhq-title">申请人: </span>\n\n        <span class="bhq-content">{{gzyzgl.editRow.sqrmc}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">部门名称: </span>\n\n        <span class="bhq-content">{{gzyzgl.editRow.bmmc}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">事由: </span>\n\n        <span class="bhq-content">{{gzyzgl.editRow.sqsy}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">事由说明: </span>\n\n        <span class="bhq-content">{{gzyzgl.editRow.sqsysm}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">开始使用时间: </span>\n\n        <span class="bhq-content">{{gzyzgl.editRow.sqsykssj | asDate}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">结束使用时间: </span>\n\n        <span class="bhq-content">{{gzyzgl.editRow.sqsyjssj | asDate}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">是否包含法人章: </span>\n\n        <span class="bhq-content" *ngIf="gzyzgl.editRow.sfbhfwdbz">是</span>\n\n        <span class="bhq-content" *ngIf="!gzyzgl.editRow.sfbhfwdbz">否</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">申请时间: </span>\n\n        <span class="bhq-content">{{gzyzgl.editRow.sqsj | asDate}}</span>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list" *ngIf="gzyzgl.editRow.bpmkey==\'yzgl\'">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab2">办公室审批</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzyzgl.process.usertask2!=null">\n\n      <div *ngFor="let item of  gzyzgl.process.usertask2.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzyzgl.process.usertask2.editRow!=null && gzyzgl.proauth.bgssp  && gzyzgl.process.usertask2.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzyzgl.process.usertask2.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list" *ngIf="!gzyzgl.editRow.sfbhfwdbz">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab3">局领导审批</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzyzgl.process.usertask3!=null">\n\n      <div *ngFor="let item of  gzyzgl.process.usertask3.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzyzgl.process.usertask3.editRow!=null && gzyzgl.proauth.jldsp  && gzyzgl.process.usertask3.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzyzgl.process.usertask3.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list" *ngIf="gzyzgl.editRow.sfbhfwdbz">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab4">局长审批</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzyzgl.process.usertask4!=null">\n\n      <div *ngFor="let item of  gzyzgl.process.usertask4.history">\n\n        <ion-item>\n\n          <ion-label>{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzyzgl.process.usertask4.editRow!=null && gzyzgl.proauth.jzsp  && gzyzgl.process.usertask4.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzyzgl.process.usertask4.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n</ion-content>\n\n\n\n<ion-footer class="bhq-bpm-button">\n\n  <div class="footer-btn" *ngIf="gzyzgl.auth.opt_th" (click)="gzyzgl.getBackTask(null,null);">\n\n    <ion-icon name="arrow-dropleft-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>退回</p>\n\n  </div>\n\n  <div class="footer-btn" *ngIf="gzyzgl.auth.opt_bc" (click)="gzyzgl.bpmSave(null);">\n\n    <ion-icon name="lock" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>保存</p>\n\n  </div>\n\n  <div class="footer-btn" *ngIf="gzyzgl.auth.opt_tj" (click)="gzyzgl.getNextTask(null,null);">\n\n    <ion-icon name="arrow-dropright-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>通过</p>\n\n  </div>\n\n  <!-- <div class="footer-more" (click)="goProcessPage();">...</div> -->\n\n  <div class="footer-btn" (click)="goProcessPage();">\n\n    <ion-icon name="keypad" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>更多</p>\n\n</div>\n\n</ion-footer>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\yzgl\yzgl.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"]])
    ], YzglPage);
    return YzglPage;
}());

//# sourceMappingURL=yzgl.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ZysqPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var ZysqPage = /** @class */ (function () {
    function ZysqPage(navCtrl, navParams, dataService, baseSErvice, alert, securityService, changeDetectorRef, loadingCtrl, actionSheetCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseSErvice = baseSErvice;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.readonly = "";
        this.solution = "不显示";
        this.haveError = false;
        this.user = {};
        this.gzzysq = new __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__["a" /* BpmObject */](this.dataService, this.baseSErvice, this.changeDetectorRef, this.navCtrl, this.alert, this.loadingCtrl, "gzzysq");
        this.gzzysq.editRow = {};
        this.gzzysq.sn = "gzzysq";
        this.gzzysq.bpmKey = this.navParams.data["bk"];
        this.gzzysq.id = this.navParams.data["dk"];
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                //this.initUserExam();
                _this.gzzysq.findWork(null, null);
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.gzzysq.findWork(null, null);
        }
    }
    ZysqPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
    };
    ;
    ZysqPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    ZysqPage.prototype.goSignPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__["a" /* BpmSignPage */], this.gzzysq);
    };
    ;
    ZysqPage.prototype.goBackPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__["a" /* BpmBackPage */], this.gzzysq);
    };
    ;
    ZysqPage.prototype.goProcessPage = function () {
        var _this = this;
        //this.navCtrl.push(BpmProcessPage,this.gzccgl);
        var actionSheet = this.actionSheetCtrl.create({
            title: '更多',
            buttons: [
                {
                    text: '办理过程',
                    handler: function () {
                        //console.log('Destructive clicked');
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__["a" /* BpmProcessPage */], _this.gzzysq);
                    }
                }, {
                    text: '流程图',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__["a" /* BpmImgPage */], _this.gzzysq);
                    }
                }, {
                    text: '取消',
                    role: '取消',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ;
    ZysqPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-zysq',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\zysq\zysq.html"*/'<ion-header>\n\n    <ion-navbar>\n\n      <ion-title>资源申请</ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  <ion-content>\n\n    <ion-list class="approval-list">\n\n      <ion-list-header>\n\n        <span class="approval-list-header-text">申请信息</span>\n\n      </ion-list-header>\n\n      <ion-item>\n\n          <span class="bhq-title">资源类型: </span>\n\n          <span class="bhq-content">{{gzzysq.editRow.szylx}}</span>\n\n      </ion-item>\n\n      <ion-item>\n\n          <span class="bhq-title">使用人: </span>\n\n          <span class="bhq-content">{{gzzysq.editRow.ssyr}}</span>\n\n      </ion-item>\n\n      <ion-item>\n\n          <span class="bhq-title">申请原因: </span>\n\n          <span class="bhq-content">{{gzzysq.editRow.ssqyy}}</span>\n\n      </ion-item>\n\n      <ion-item>\n\n          <span class="bhq-title">联系方式: </span>\n\n          <span class="bhq-content">{{gzzysq.editRow.slxfs}}</span>\n\n      </ion-item>\n\n      <ion-item>\n\n          <span class="bhq-title">所属项目: </span>\n\n          <span class="bhq-content">{{gzzysq.editRow.gzzysq}}</span>\n\n      </ion-item>\n\n      <ion-item>\n\n          <span class="bhq-title">申请时间: </span>\n\n          <span class="bhq-content">{{gzzysq.editRow.dsqsj | asDate}}</span>\n\n      </ion-item>\n\n      <ion-item>\n\n        <span class="bhq-title">使用开始时间: </span>\n\n        <span class="bhq-content">{{gzzysq.editRow.dsykssj | asDate}}</span>\n\n        </ion-item>\n\n        <ion-item>\n\n            <span class="bhq-title">使用结束时间: </span>\n\n            <span class="bhq-content">{{gzzysq.editRow.dsyjssj | asDate}}</span>\n\n        </ion-item>\n\n        <ion-item>\n\n            <span class="bhq-title">使用结束时间: </span>\n\n            <span class="bhq-content">{{gzzysq.editRow.dsyjssj | asDate}}</span>\n\n        </ion-item>\n\n    </ion-list>\n\n  \n\n    <ion-list class="approval-list">\n\n      <ion-list-header>\n\n        <span class="approval-list-header-text" id="tab2">科长审核</span>\n\n      </ion-list-header>\n\n      <div *ngIf="gzzysq.process.usertask2!=null">\n\n        <div *ngFor="let item of  gzzysq.process.usertask2.history">\n\n          <ion-item>\n\n            <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n          </ion-item>\n\n          <ion-item class="bhq-last-child">\n\n            <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n            <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n          </ion-item>\n\n        </div>\n\n  \n\n        <ion-item class="bhq-last-child" *ngIf="gzzysq.process.usertask2.editRow!=null && gzzysq.proauth.kzsh  && gzzysq.process.usertask2.userEdit">\n\n          <ion-label class="bhq-title">审批意见:</ion-label>\n\n          <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzzysq.process.usertask2.editRow.optcontent">\n\n          </ion-textarea>\n\n        </ion-item>\n\n      </div>\n\n    </ion-list>\n\n  \n\n    <ion-list class="approval-list">\n\n      <ion-list-header>\n\n        <span class="approval-list-header-text" id="tab3">分管领导审核</span>\n\n      </ion-list-header>\n\n      <div *ngIf="gzzysq.process.usertask3!=null">\n\n        <div *ngFor="let item of  gzzysq.process.usertask3.history">\n\n          <ion-item>\n\n            <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n          </ion-item>\n\n          <ion-item class="bhq-last-child">\n\n            <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n            <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n          </ion-item>\n\n        </div>\n\n  \n\n        <ion-item class="bhq-last-child" *ngIf="gzzysq.process.usertask3.editRow!=null && gzzysq.proauth.fgldsh  && gzzysq.process.usertask3.userEdit">\n\n          <ion-label class="bhq-title">审批意见:</ion-label>\n\n          <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzzysq.process.usertask3.editRow.optcontent">\n\n          </ion-textarea>\n\n        </ion-item>\n\n      </div>\n\n    </ion-list>\n\n  \n\n    <ion-list class="approval-list" >\n\n      <ion-list-header>\n\n        <span class="approval-list-header-text" id="tab4">办公室主任审核</span>\n\n      </ion-list-header>\n\n      <div *ngIf="gzzysq.process.usertask4!=null">\n\n        <div *ngFor="let item of  gzzysq.process.usertask4.history">\n\n          <ion-item>\n\n            <ion-label>{{item.optcontent}}</ion-label>\n\n          </ion-item>\n\n          <ion-item class="bhq-last-child">\n\n            <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n            <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n          </ion-item>\n\n        </div>\n\n  \n\n        <ion-item class="bhq-last-child" *ngIf="gzzysq.process.usertask4.editRow!=null && gzzysq.proauth.bgszr  && gzzysq.process.usertask4.userEdit">\n\n          <ion-label class="bhq-title">审批意见:</ion-label>\n\n          <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzzysq.process.usertask4.editRow.optcontent">\n\n          </ion-textarea>\n\n        </ion-item>\n\n      </div>\n\n    </ion-list>\n\n  </ion-content>\n\n  \n\n  <ion-footer class="bhq-bpm-button">\n\n    <div class="footer-btn" *ngIf="gzzysq.auth.opt_th" (click)="gzzysq.getBackTask(null,null);">\n\n      <ion-icon name="arrow-dropleft-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n      <p>退回</p>\n\n    </div>\n\n    <div class="footer-btn" *ngIf="gzzysq.auth.opt_bc" (click)="gzzysq.bpmSave(null);">\n\n      <ion-icon name="lock" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n      <p>保存</p>\n\n    </div>\n\n    <div class="footer-btn" *ngIf="gzzysq.auth.opt_tj" (click)="gzzysq.getNextTask(null,null);">\n\n      <ion-icon name="arrow-dropright-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n      <p>通过</p>\n\n    </div>\n\n    <!-- <div class="footer-more" (click)="goProcessPage();">...</div> -->\n\n    <div class="footer-btn" (click)="goProcessPage();">\n\n      <ion-icon name="keypad" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n      <p>更多</p>\n\n  </div>\n\n  </ion-footer>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\zysq\zysq.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"]])
    ], ZysqPage);
    return ZysqPage;
}());

//# sourceMappingURL=zysq.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WjryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_file_transfer__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_file__ = __webpack_require__(344);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_camera__ = __webpack_require__(345);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_image_picker__ = __webpack_require__(346);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var WjryPage = /** @class */ (function () {
    function WjryPage(navCtrl, navParams, dataService, baseSErvice, alert, securityService, changeDetectorRef, loadingCtrl, actionSheetCtrl, privateactionSheetCtrl, camera, imagePicker, transfer, file, fileTransfer) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseSErvice = baseSErvice;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.imagePicker = imagePicker;
        this.transfer = transfer;
        this.file = file;
        this.fileTransfer = fileTransfer;
        this.readonly = "";
        this.solution = "不显示";
        this.haveError = false;
        this.user = {};
        this.gzwjryrqxk = new __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__["a" /* BpmObject */](this.dataService, this.baseSErvice, this.changeDetectorRef, this.navCtrl, this.alert, this.loadingCtrl, "gzwjryrqxk");
        this.cameraOpt = {
            quality: 30,
            destinationType: 1,
            sourceType: 1,
            encodingType: 0,
            mediaType: 0,
            allowEdit: false,
            correctOrientation: true,
            saveToPhotoAlbum: true
        };
        this.imagePickerOpt = {
            maximumImagesCount: 1,
            width: 800,
            height: 800,
            quality: 80
        };
        this.upload = {
            url: this.baseSErvice.baseUrl + "/userfile/plupload/appUploadFile",
            fileKey: 'upload',
            fileName: 'imageName.jpg',
            headers: {
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' //不加入 发生错误！！
            },
            params: {},
            success: function (data) { },
            error: function (err) { },
            listen: function () { } //监听上传过程
        };
        this.convertFilePath = function (fileInfo) {
            if (fileInfo == null || fileInfo == "")
                return null;
            var fileInfoArr = fileInfo.split(":");
            if (fileInfoArr.length < 3)
                return null;
            var bucket = fileInfoArr[0];
            var key = fileInfoArr[1];
            var name = fileInfoArr[2];
            var downloadUrl = this.baseSErvice.baseUrl + '/file/plupload/download';
            var _downloadUrl = downloadUrl + "?bucket=" + bucket + "&key=" + key + "&name=" + encodeURIComponent(name);
            return _downloadUrl;
        };
        this.gzwjryrqxk.editRow = {};
        this.gzwjryrqxk.sn = "gzwjryrqxk";
        this.gzwjryrqxk.bpmKey = this.navParams.data["bk"];
        this.gzwjryrqxk.id = this.navParams.data["dk"];
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                //this.initUserExam();
                _this.gzwjryrqxk.findWork(null, null);
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.gzwjryrqxk.findWork(null, null);
        }
    }
    WjryPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
    };
    ;
    WjryPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    WjryPage.prototype.goSignPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__["a" /* BpmSignPage */], this.gzwjryrqxk);
    };
    ;
    WjryPage.prototype.goBackPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__["a" /* BpmBackPage */], this.gzwjryrqxk);
    };
    ;
    WjryPage.prototype.goProcessPage = function () {
        var _this = this;
        //this.navCtrl.push(BpmProcessPage,this.gzccgl);
        var actionSheet = this.actionSheetCtrl.create({
            title: '更多',
            buttons: [
                {
                    text: '办理过程',
                    handler: function () {
                        //console.log('Destructive clicked');
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__["a" /* BpmProcessPage */], _this.gzwjryrqxk);
                    }
                }, {
                    text: '流程图',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__["a" /* BpmImgPage */], _this.gzwjryrqxk);
                    }
                }, {
                    text: '取消',
                    role: '取消',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ;
    /**constructor(privateactionSheetCtrl:ActionSheetController,
      private noticeSer:ToastService,
      private camera:Camera,
      private imagePicker:ImagePicker,
      private transfer:FileTransfer,
      private file:File,
      private fileTransfer:FileTransferObject) {
      
      this.fileTransfer= this.transfer.create();
      } */
    WjryPage.prototype.showPicActionSheet = function (field) {
        this.useASComponent(field);
    };
    // 使用ionic中的ActionSheet组件
    WjryPage.prototype.useASComponent = function (field) {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: '请选择',
            buttons: [
                {
                    text: '拍照',
                    handler: function () {
                        _this.startCamera(field);
                    }
                },
                {
                    text: '从手机相册选择',
                    handler: function () {
                        _this.openImgPicker(field);
                    }
                },
                {
                    text: '取消',
                    role: 'cancel',
                    handler: function () {
                    }
                }
            ]
        });
        actionSheet.present();
    };
    // 启动拍照功能
    WjryPage.prototype.startCamera = function (field) {
        var _this = this;
        this.camera.getPicture(this.cameraOpt).then(function (imageData) {
            _this.uploadImg(imageData, field);
        }, function (err) {
            //this.alert.create({
            // title:"系统提示",
            // subTitle:"无法打开相机，请授权！",
            // buttons:["确定"]
            // }).present();
        });
    };
    // 打开手机相册
    WjryPage.prototype.openImgPicker = function (field) {
        var _this = this;
        var temp = '';
        /*this.imagePicker.getPictures(this.imagePickerOpt)
          .then((results)=> {
            for (var i=0; i < results.length; i++) {
              temp = results[i];
            }
            this.uploadImg(temp,field);
        }, (err)=> {
          
        });*/
        var options = {
            /* quality: 100,
             //  destinationType: this.camera.DestinationType.FILE_URI,
            // destinationType: this.camera.DestinationType.DATA_URL,
            destinationType: 1,
             encodingType: this.camera.EncodingType.PNG,
             mediaType: this.camera.MediaType.PICTURE,
             sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
             correctOrientation:true,
             allowEdit: true,
             targetWidth: 100,
             targetHeight: 100,   */
            quality: 30,
            destinationType: 1,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            encodingType: 0,
            mediaType: 0,
            allowEdit: true,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.uploadImg(imageData, field);
        }, function (err) {
            // this.alert.create({
            //  title:"系统提示",
            //  subTitle:"无法打开相册，请授权！",
            //  buttons:["确定"]
            // }).present();
        });
    };
    // 上传图片
    WjryPage.prototype.uploadImg = function (path, field) {
        var _this = this;
        if (!path) {
            return;
        }
        this.upload.fileName = this.gzwjryrqxk.getGUID() + ".jpg";
        var options;
        options = {
            fileKey: this.upload.fileKey,
            headers: this.upload.headers,
            params: this.upload.params
        };
        var that = this;
        var loader = this.loadingCtrl.create({
            content: "正在上传..."
        });
        loader.present();
        this.fileTransfer.upload(path, this.upload.url, options)
            .then(function (data) {
            loader.dismiss();
            var temp = JSON.parse(data.response);
            that.gzwjryrqxk.editRow[field] = temp["bucket"] + ":" + temp["key"] + ":" + that.upload.fileName;
            document.getElementById(field).setAttribute("src", _this.convertFilePath(that.gzwjryrqxk.editRow[field]));
            that.changeDetectorRef.markForCheck();
            that.changeDetectorRef.detectChanges();
            if (_this.upload.success) {
                _this.upload.success(JSON.parse(data.response));
            }
        }, function (err) {
            loader.dismiss();
            if (_this.upload.error) {
                _this.upload.error(err);
            }
            else {
                _this.alert.create({
                    title: "系统提示",
                    subTitle: "抱歉，文件处理异常了，请重试！",
                    buttons: ["确定"]
                }).present();
            }
        });
    };
    // 停止上传
    WjryPage.prototype.stopUpload = function () {
        if (this.fileTransfer) {
            this.fileTransfer.abort();
        }
    };
    ;
    WjryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-wjry',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\wjry\wjry.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>外籍人员入区许可办理</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">申请书</span>\n\n    </ion-list-header>\n\n    <ion-item>\n\n        <span class="bhq-title">申请单位名称: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.sdwmc}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">法定代表人: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.fddbrmc}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">地址: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.sqdwdz}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">联系电话: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.sqdwlxdh}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">邮编: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.sqdwlxyb}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">项目名称: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.sxmmc}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">是否有介绍信(函): </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.ijsxName}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">提供活动方案情况: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.shdfaqk}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">是否鉴定合作协议: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.ijdhzxyName}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">协议编号: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.sxybh}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">带队负责人: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.sddfzr}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">职务: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.szw}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">联系电话: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.slxdh}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">人数: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.srs}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">来访时间: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.dlfsj | asDate}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">活动计划时限: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.shdjhsx}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">申请时间: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.xzxksqsj | asDate}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">项目简介: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.sxmjj}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">行政许可申请事项: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.xzxksqsx}}</span>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">同行人员基本情况</span>\n\n    </ion-list-header>\n\n    <ion-item *ngFor="let item of gzwjryrqxk.editRow.gzplanpersonnel">\n\n      <ion-label class="bhq-content">{{item.yrmc}}({{item.ssfz}})</ion-label>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">申请人信息</span>\n\n    </ion-list-header>\n\n    <ion-item>\n\n        <span class="bhq-title">委托代理人姓名: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.wtdlrxm}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">身份证号: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.wtdlrsfzh}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">住址: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.wtdlrzz}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">电话: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.wtdlrdh}}</span>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">申请人信息</span>\n\n    </ion-list-header>\n\n    <ion-item>\n\n        <span class="bhq-title">许可证号: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.slsm}}许受理字{{gzwjryrqxk.editRow.slxh}}第{{gzwjryrqxk.editRow.slbh}}号</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">行政许可审查情况: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.xzxkscqk}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">承办人意见: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.cbryj}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">审批时间: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.cbrsj | asDate}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">审批人: </span>\n\n        <span class="bhq-content">{{gzwjryrqxk.editRow.cbrmc}}</span>\n\n    </ion-item>\n\n    <ion-item class="bhq-last-child" style="height: 80px">\n\n      承办人签字:\n\n      <img src="{{gzwjryrqxk.editRow.chrspqz | asImage}}" height="70px"/>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab2">科研科审批</span>\n\n    </ion-list-header>\n\n      <div *ngIf="!gzwjryrqxk.proauth.kssh">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{gzwjryrqxk.editRow.sdwspyj}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{gzwjryrqxk.editRow.sbmspr}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{gzwjryrqxk.editRow.dbmspsj | asDate}}</ion-label>\n\n        </ion-item>\n\n        <!--测试代码-->\n\n        <ion-item class="bhq-last-child" style="height: 80px">\n\n          <!--<ion-icon ios="ios-add" md="md-add" ></ion-icon>-->\n\n\n\n        审批人签字:\n\n          <img src="{{gzwjryrqxk.editRow.kyksqqz | asImage}}" height="70px"/>\n\n        </ion-item>\n\n      </div>\n\n      <div *ngIf="gzwjryrqxk.proauth.kssh">\n\n        <ion-item class="bhq-last-child" >\n\n          <ion-label class="bhq-title">审批意见:</ion-label>\n\n          <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzwjryrqxk.editRow.sbmspyj"> </ion-textarea>\n\n         </ion-item>\n\n\n\n        <ion-item class="bhq-last-child" >\n\n          <ion-label class="bhq-title">审批人: </ion-label>\n\n          <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzwjryrqxk.editRow.sbmspr"> </ion-textarea>\n\n        </ion-item>\n\n\n\n        <ion-item class="bhq-last-child" >\n\n          <ion-label class="bhq-title">审批时间: </ion-label>\n\n          <ion-datetime displayFormat="YYYY-MM-DD" [(ngModel)]="gzwjryrqxk.editRow.dbmspsj" max="2100-10-31"\n\n          cancelText="取消" doneText="确认"></ion-datetime> \n\n\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child" style="height: 80px">\n\n        审批人签字:\n\n          <img src="{{gzwjryrqxk.editRow.kyksqqz | asImage}}" height="70px"/>\n\n          <button ion-button (click)="showPicActionSheet(\'kyksqqz\');">上传</button>\n\n        </ion-item>\n\n      </div>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab2">办公室主任审核</span>\n\n    </ion-list-header>\n\n    <div *ngIf="!gzwjryrqxk.proauth.bgszrsh">\n\n     \n\n      <ion-item>\n\n        <ion-label class="bhq-content">{{gzwjryrqxk.editRow.sdwspyj}}</ion-label>\n\n      </ion-item>\n\n      <ion-item class="bhq-last-child">\n\n        <ion-label class="bhq-content">审批人: {{gzwjryrqxk.editRow.sdwspr}}</ion-label>\n\n        <ion-label class="bhq-content">时间:{{gzwjryrqxk.editRow.ddwspsj | asDate}}</ion-label>\n\n      </ion-item>\n\n       <ion-item class="bhq-last-child" style="height: 80px">\n\n        审批人签字:\n\n        <img src="{{gzwjryrqxk.editRow.bgssqqz | asImage}}" height="70px"/>\n\n      </ion-item>\n\n    </div>\n\n    <div *ngIf="gzwjryrqxk.proauth.bgszrsh">\n\n      <ion-item class="bhq-last-child" >\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzwjryrqxk.editRow.sdwspyj"> </ion-textarea>\n\n      </ion-item>\n\n\n\n      <ion-item class="bhq-last-child" >\n\n        <ion-label class="bhq-title">审批人: </ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzwjryrqxk.editRow.sdwspr"> </ion-textarea>\n\n      </ion-item>\n\n\n\n      <ion-item class="bhq-last-child" >\n\n        <ion-label class="bhq-title">审批时间: </ion-label>\n\n        <ion-datetime displayFormat="YYYY-MM-DD" [(ngModel)]="gzwjryrqxk.editRow.ddwspsj" max="2100-10-31"\n\n        cancelText="取消" doneText="确认"></ion-datetime> \n\n      </ion-item>\n\n      <ion-item class="bhq-last-child" style="height: 80px">\n\n        审批人签字:\n\n        <img src="{{gzwjryrqxk.editRow.bgssqqz | asImage}}" height="70px"/>\n\n        <button ion-button (click)="showPicActionSheet(\'bgssqqz\');">上传</button>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n</ion-content>\n\n\n\n<ion-footer class="bhq-bpm-button">\n\n  <div class="footer-btn" *ngIf="gzwjryrqxk.auth.opt_th" (click)="gzwjryrqxk.getBackTask(null,null);">\n\n    <ion-icon name="arrow-dropleft-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>退回</p>\n\n  </div>\n\n  <div class="footer-btn" *ngIf="gzwjryrqxk.auth.opt_bc" (click)="gzwjryrqxk.bpmSave(null);">\n\n    <ion-icon name="lock" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>保存</p>\n\n  </div>\n\n  <div class="footer-btn" *ngIf="gzwjryrqxk.auth.opt_tj" (click)="gzwjryrqxk.getNextTask(null,null);">\n\n    <ion-icon name="arrow-dropright-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>通过</p>\n\n  </div>\n\n  <!-- <div class="footer-more" (click)="goProcessPage();">...</div> -->\n\n  <div class="footer-btn" (click)="goProcessPage();">\n\n    <ion-icon name="keypad" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>更多</p>\n\n</div>\n\n</ion-footer>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\wjry\wjry.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_13__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_14__ionic_native_image_picker__["a" /* ImagePicker */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_12__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_file_transfer__["b" /* FileTransferObject */]])
    ], WjryPage);
    return WjryPage;
}());

//# sourceMappingURL=wjry.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KyxmPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var KyxmPage = /** @class */ (function () {
    function KyxmPage(navCtrl, navParams, dataService, baseSErvice, alert, securityService, changeDetectorRef, loadingCtrl, actionSheetCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseSErvice = baseSErvice;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.readonly = "";
        this.solution = "不显示";
        this.haveError = false;
        this.user = {};
        this.gzprojectinfo = new __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__["a" /* BpmObject */](this.dataService, this.baseSErvice, this.changeDetectorRef, this.navCtrl, this.alert, this.loadingCtrl, "gzprojectinfo");
        this.gzprojectinfo.editRow = {};
        this.gzprojectinfo.sn = "gzprojectinfo";
        this.gzprojectinfo.bpmKey = this.navParams.data["bk"];
        this.gzprojectinfo.id = this.navParams.data["dk"];
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                //this.initUserExam();
                _this.gzprojectinfo.findWork(null, null);
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.gzprojectinfo.findWork(null, null);
        }
    }
    KyxmPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
    };
    ;
    KyxmPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    KyxmPage.prototype.goSignPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__["a" /* BpmSignPage */], this.gzprojectinfo);
    };
    ;
    KyxmPage.prototype.goBackPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__["a" /* BpmBackPage */], this.gzprojectinfo);
    };
    ;
    KyxmPage.prototype.goProcessPage = function () {
        var _this = this;
        //this.navCtrl.push(BpmProcessPage,this.gzccgl);
        var actionSheet = this.actionSheetCtrl.create({
            title: '更多',
            buttons: [
                {
                    text: '办理过程',
                    handler: function () {
                        //console.log('Destructive clicked');
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__["a" /* BpmProcessPage */], _this.gzprojectinfo);
                    }
                }, {
                    text: '流程图',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__["a" /* BpmImgPage */], _this.gzprojectinfo);
                    }
                }, {
                    text: '取消',
                    role: '取消',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ;
    KyxmPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-kyxm',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\kyxm\kyxm.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>科研项目申请</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">申请信息</span>\n\n    </ion-list-header>\n\n    <ion-item>\n\n        <span class="bhq-title">编号: </span>\n\n        <span class="bhq-content">{{gzprojectinfo.editRow.sxmbh}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">项目名称: </span>\n\n        <span class="bhq-content">{{gzprojectinfo.editRow.sxmmc}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">项目内容: </span>\n\n        <span class="bhq-content">{{gzprojectinfo.editRow.sxmnr}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">项目规模: </span>\n\n        <span class="bhq-content">{{gzprojectinfo.editRow.sxmgm}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">项目负责人: </span>\n\n        <span class="bhq-content">{{gzprojectinfo.editRow.sxmjlname}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">联系电话: </span>\n\n        <span class="bhq-content">{{gzprojectinfo.editRow.slxdh}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">办公电话: </span>\n\n        <span class="bhq-content">{{gzprojectinfo.editRow.sbgdh}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">邮箱: </span>\n\n        <span class="bhq-content">{{gzprojectinfo.editRow.sdzyx}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">项目状态: </span>\n\n        <span class="bhq-content">{{gzprojectinfo.editRow.sxmztname}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">备注: </span>\n\n        <span class="bhq-content">{{gzprojectinfo.editRow.sbz}}</span>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">项目参与人</span>\n\n    </ion-list-header>\n\n    <ion-item *ngFor="let item of gzprojectinfo.editRow.gzplanpersonnel">\n\n      <ion-label class="bhq-content">{{item.yrmc}}({{item.ssfz}})</ion-label>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab2">科室科长审核</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzprojectinfo.process.usertask2!=null">\n\n      <div *ngFor="let item of  gzprojectinfo.process.usertask2.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzprojectinfo.process.usertask2.editRow!=null && gzprojectinfo.proauth.kssh  && gzprojectinfo.process.usertask2.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzprojectinfo.process.usertask2.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab3">办公室主任审核</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzprojectinfo.process.usertask3!=null">\n\n      <div *ngFor="let item of  gzprojectinfo.process.usertask3.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzprojectinfo.process.usertask3.editRow!=null && gzprojectinfo.proauth.bgssh  && gzprojectinfo.process.usertask3.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzprojectinfo.process.usertask3.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n</ion-content>\n\n\n\n<ion-footer class="bhq-bpm-button">\n\n  <div class="footer-btn" *ngIf="gzprojectinfo.auth.opt_th" (click)="gzprojectinfo.getBackTask(null,null);">\n\n    <ion-icon name="arrow-dropleft-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>退回</p>\n\n  </div>\n\n  <div class="footer-btn" *ngIf="gzprojectinfo.auth.opt_bc" (click)="gzprojectinfo.bpmSave(null);">\n\n    <ion-icon name="lock" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>保存</p>\n\n  </div>\n\n  <div class="footer-btn" *ngIf="gzprojectinfo.auth.opt_tj" (click)="gzprojectinfo.getNextTask(null,null);">\n\n    <ion-icon name="arrow-dropright-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>通过</p>\n\n  </div>\n\n  <!-- <div class="footer-more" (click)="goProcessPage();">...</div> -->\n\n  <div class="footer-btn" (click)="goProcessPage();">\n\n      <ion-icon name="keypad" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n      <p>更多</p>\n\n  </div>\n\n</ion-footer>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\kyxm\kyxm.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"]])
    ], KyxmPage);
    return KyxmPage;
}());

//# sourceMappingURL=kyxm.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignHttp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// 签到相关
var SignHttp = /** @class */ (function () {
    function SignHttp(http, baseService) {
        this.http = http;
        this.baseService = baseService;
    }
    /**
     * 签到
     */
    SignHttp.prototype.gzqdgldata = function (method, latitude, longitude, address, accuracy) {
        var url = this.baseService.baseUrl + "/gzqdgl/qdgl";
        var data = {
            method: method,
            latitude: latitude,
            address: address,
            accuracy: accuracy,
            dirtype: "yd"
        };
        var as = this.baseService.toJSONStr(data);
        return this.http.post(url, as, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    ;
    SignHttp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */]])
    ], SignHttp);
    return SignHttp;
}());

//# sourceMappingURL=SignHttp.js.map

/***/ }),

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DonePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__process_process__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_form_FormObject__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_ccgl_ccgl__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_swgl_swgl__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_qjgl_qjgl__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_wlkyxm_wlkyxm__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_ycsq_ycsq__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_yzgl_yzgl__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_zysq_zysq__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_wjry_wjry__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_kyxm_kyxm__ = __webpack_require__(108);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















/**
 * Generated class for the TodoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DonePage = /** @class */ (function () {
    function DonePage(navCtrl, navParams, baseService, dataService, securityService, changeDetectorRef, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.baseService = baseService;
        this.dataService = dataService;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.workInput = "";
        this.datas = [];
        this.user = {};
        this.showDetails = false;
        //构建formObject对象方式修改，这种方式建立了独立的对象数据
        this.overworklist = new __WEBPACK_IMPORTED_MODULE_3__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
    }
    DonePage.prototype.searchWork = function () {
        //this.worklist.queryObject={"sbusiness:like":this.workInput?this.workInput:""};
        if (this.workInput == null || this.workInput == "") {
            this.overworklist.queryObject = {};
        }
        else {
            this.overworklist.queryObject["sbusiness:like"] = this.workInput;
        }
        this.overworklist.pageIndex = 1; //重置查询条件的时候，需要重置页码
        this.overworklist.datas = [];
        this.overworklist.getEntitys(null);
    };
    DonePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TodoPage');
    };
    DonePage.prototype.buttonClick = function () {
        console.log('buttonClick');
    };
    DonePage.prototype.toProcessPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__process_process__["a" /* ProcessPage */]);
    };
    ;
    DonePage.prototype.toQueryWork = function (data) {
        var para = {};
        para["bk"] = data.actdefid;
        para["fn"] = data.prjforname;
        para["vi"] = data.prjpopname;
        para["dk"] = data.sbussinessid;
        //出差
        if (para["bk"] == "ccgl"
            || para["bk"] == "ccgl_jjgb"
            || para["bk"] == "ccgl_kjgb"
            || para["bk"] == "ccgl_zrjbgb") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_ccgl_ccgl__["a" /* CcglPage */], para);
        }
        //收发文
        if (para["bk"] == "sfwgl"
            || para["bk"] == "sfwgl_jzgb"
            || para["bk"] == "sfwgl_kszysj"
            || para["bk"] == "sfwgl_zrjbgb") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_swgl_swgl__["a" /* SwglPage */], para);
        }
        //外来科研项目
        if (para["bk"] == "wlkyxmrqxk") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__pages_wlkyxm_wlkyxm__["a" /* WlkyxmPage */], para);
        }
        //用车申请
        if (para["bk"] == "clsysq"
            || para["bk"] == "clsysq_kjgb"
            || para["bk"] == "clsysq_zrjbgb") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_12__pages_ycsq_ycsq__["a" /* YcsqPage */], para);
        }
        //用章管理
        if (para["bk"] == "yzgl"
            || para["bk"] == "yzgl_zrjbgb") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__pages_yzgl_yzgl__["a" /* YzglPage */], para);
        }
        //资源申请
        if (para["bk"] == "zydd"
            || para["bk"] == "zydd") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_14__pages_zysq_zysq__["a" /* ZysqPage */], para);
        }
        //请假管理
        if (para["bk"] == "qjgl"
            || para["bk"] == "qjgl_jjgb"
            || para["bk"] == "qjgl_kjgb") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_qjgl_qjgl__["a" /* QjglPage */], para);
        }
        //外籍人员入区许可办理
        if (para["bk"] == "wjryrqxk") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_15__pages_wjry_wjry__["a" /* WjryPage */], para);
        }
        //科研项目管理
        if (para["bk"] == "kyxmgl") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_16__pages_kyxm_kyxm__["a" /* KyxmPage */], para);
        }
    };
    ;
    //初始化页面
    DonePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.user = {};
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                _this.initSuccessHomePage();
            }, function () {
                _this.user = {};
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.initSuccessHomePage();
        }
    };
    ;
    //跳转到登录界面
    DonePage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    /**
     * 成功加载用户home页面时处理
     */
    DonePage.prototype.initSuccessHomePage = function () {
        this.user = this.securityService.user;
        this.overworklist = new __WEBPACK_IMPORTED_MODULE_3__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
        this.overworklist.sn = "overworklist";
        this.overworklist.getEntitys(null);
    };
    ;
    DonePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-done',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\done\done.html"*/'<!--\n\n  Generated template for the DonePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>已办列表</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <div class="search-bar-content">\n\n    <ion-searchbar placeholder="搜索" (ionInput)="searchWork()" [(ngModel)]="workInput">\n\n    </ion-searchbar>\n\n  </div>\n\n\n\n  <ion-refresher (ionRefresh)="overworklist.doRefresh($event)">\n\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="下拉刷新" refreshingSpinner="circles" refreshingText="刷新中...">\n\n    </ion-refresher-content>\n\n  </ion-refresher>\n\n\n\n  <ion-list class="bhq-list">\n\n    <ion-item class="bhq-item" (click)="toQueryWork(item)" *ngFor="let item of overworklist.datas">\n\n      <ion-thumbnail item-start>\n\n        <img src="assets/imgs/{{item.prjforname}}.png">\n\n      </ion-thumbnail>\n\n      <h2 class="bhq-item-title">{{item.sbusiness | asLength:12}}</h2>\n\n      <div class="bhq-item-content">\n\n        <div class="bhq-item-info">\n\n          <p>[{{item.actdefname}}] {{item.taskdefname}}</p>\n\n          <p>{{item.fromtime | asDate}} {{item.fromtime | asTime}}</p>\n\n        </div>\n\n      </div>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <!-- 无级滚动效果实现 -->\n\n  <ion-infinite-scroll (ionInfinite)="overworklist.doInfinite($event)">\n\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n  </ion-infinite-scroll>\n\n</ion-content>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\done\done.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_6__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_7__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_5__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], DonePage);
    return DonePage;
}());

//# sourceMappingURL=done.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_buiness_SignHttp__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_core_data_DataService__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SignPage = /** @class */ (function () {
    function SignPage(navCtrl, alertCtrl, baseService, dataService, securityService, signHttp, changeDetectorRef, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.baseService = baseService;
        this.dataService = dataService;
        this.securityService = securityService;
        this.signHttp = signHttp;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.datas = [];
        this.user = {};
        this.tableName = 'gzqdgl';
        this.showDetails = false;
        this.latitude = ""; //维度
        this.longitude = ""; //经度
        this.formattedAddress = "";
        this.accuracy = "";
        //构建formObject对象方式修改，这种方式建立了独立的对象数据
        this.gzqdgl = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
        this.ionViewDidEnter();
    }
    //初始化页面
    SignPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.user = {};
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.initSuccessSignPage();
            }, function () {
                _this.user = null;
                _this.goLogin();
            });
        }
        else {
            this.initSuccessSignPage();
        }
    };
    ;
    //跳转到登录界面
    SignPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    /**
     * 成功加载用户签到页面时处理
     */
    SignPage.prototype.initSuccessSignPage = function () {
        this.user = this.securityService.user;
        this.gzqdgl.sn = this.tableName;
        //根据时间排序
        this.gzqdgl.orderObject = { "createtime": 1 };
        //根据登录用户过滤
        // if(this.user["userid"]!='admin'){
        //   this.gzqdgl.queryObject["kaoqrid:="]=this.user["userid"];
        // }
        this.gzqdgl.pageIndex = 1;
        this.gzqdgl.datas = [];
        this.gzqdgl.getEntitys(this.editDatas);
    };
    ;
    SignPage.prototype.editDatas = function (datas) {
        for (var item = 0; item < datas.length; item++) {
            datas[item].icon = 'ios-arrow-forward';
        }
    };
    ;
    // setQueryUser(){
    //   this.queryObject["userID:="]=this.user["userid"];
    // };
    //打开或关闭数据详细内容
    SignPage.prototype.toggleDetails = function (data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-forward';
        }
        else {
            data.showDetails = true;
            data.icon = 'ios-arrow-down';
        }
    };
    ;
    SignPage.prototype.gzqdgldata = function () {
        var _this = this;
        this.signHttp.gzqdgldata("create", this.latitude, this.longitude, this.formattedAddress, this.accuracy).subscribe(function (data) {
            if (data) {
                var time = new Date();
                var strtime = time.getHours() + "时" + time.getMinutes() + "分" + time.getSeconds() + "秒";
                var alert_1 = _this.alertCtrl.create({
                    title: '签到成功',
                    subTitle: '签到时间:' + strtime,
                    buttons: ['确定']
                });
                _this.gzqdgl.pageIndex = 1;
                _this.gzqdgl.datas = [];
                _this.gzqdgl.getEntitys(_this.editDatas);
                alert_1.present();
            }
        }, function (err) {
            var alert = _this.alertCtrl.create({
                title: '系统提示',
                subTitle: '签到失败，请重试！',
                buttons: ['确定']
            });
            alert.present();
        });
    };
    ;
    SignPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    //查询
    SignPage.prototype.onSearchKeyUp = function (event) {
        if ("Enter" == event.key) {
            //if(this.user!=null && this.user["userid"]!=null){
            // this.formObject.pageIndex=0;
            // this.formObject.dataOver=false;
            // this.formObject.getEntitys();
        }
        //}else{
        //this.queryValue="";
        //}
    };
    SignPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\sign\sign.html"*/'<ion-header>\n\n\n\n	<ion-navbar>\n\n		<ion-title>考勤记录</ion-title>\n\n	</ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content>\n\n	<ion-refresher (ionRefresh)="gzqdgl.doRefresh($event)">\n\n		<ion-refresher-content pullingIcon="arrow-dropdown"  pullingText="下拉刷新"  refreshingSpinner="circles"  refreshingText="刷新中...">\n\n		</ion-refresher-content>\n\n	</ion-refresher>\n\n	<ion-list>\n\n		<ion-item class="bhq-item"\n\n		*ngFor="let item of gzqdgl.datas">\n\n		\n\n		<div class="bhq-item-content">\n\n			<div class="bhq-item-info">\n\n				<p class="bhq-item-title"><strong>{{item.kaoqrmc}}({{item.danweimc}})</strong></p>\n\n				<p>{{item.nandu}}年{{item.yuefen}}月{{item.day}}日</p>\n\n			</div>\n\n			<div class="bhq-item-date">\n\n				<p>签到时间:{{item.qdsj | date:\'HH:mm:ss\'}}</p>\n\n				<p>签退时间:{{item.qtsj | date:\'HH:mm:ss\'}}</p>\n\n			</div>\n\n		</div>\n\n		<div >\n\n			<p *ngIf="item.dirtype==\'yd\'">{{item.address}}</p>\n\n			<p *ngIf="item.dirtype!=\'yd\' || item.dirtype==null ">网站打卡</p>\n\n		</div>\n\n		</ion-item>\n\n	</ion-list>\n\n	<!-- 无级滚动效果实现 -->\n\n	<ion-infinite-scroll (ionInfinite)="gzqdgl.doInfinite($event)">\n\n		<ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n	</ion-infinite-scroll>\n\n</ion-content>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\sign\sign.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_6__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_7__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_5__app_buiness_SignHttp__["a" /* SignHttp */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], SignPage);
    return SignPage;
}());

//# sourceMappingURL=sign.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TripPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_ccgl_ccgl__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var TripPage = /** @class */ (function () {
    function TripPage(navCtrl, baseService, dataService, securityService, changeDetectorRef, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.baseService = baseService;
        this.dataService = dataService;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.datas = [];
        this.user = {};
        this.tableName = 'gzccgl';
        this.showDetails = false;
        //构建formObject对象方式修改，这种方式建立了独立的对象数据
        this.gzccgl = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
        this.user = {};
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.initSuccessTripPage();
            }, function () {
                _this.user = null;
                _this.goLogin();
            });
        }
        else {
            this.initSuccessTripPage();
        }
    }
    //初始化页面
    TripPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.user = {};
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.initSuccessTripPage();
            }, function () {
                _this.user = null;
                _this.goLogin();
            });
        }
        else {
            this.initSuccessTripPage();
        }
    };
    ;
    TripPage.prototype.toQueryWork = function (data) {
        var para = {};
        para["bk"] = data.bpmkey;
        para["fn"] = "gzccgl";
        para["vi"] = "gzccgl";
        para["dk"] = data.id;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_ccgl_ccgl__["a" /* CcglPage */], para);
    };
    ;
    TripPage.prototype.startNewBpm = function (bpmkey, formname) {
        var para = {};
        para["bk"] = bpmkey;
        para["fn"] = formname;
        para["vi"] = "gzccgl";
        para["dk"] = this.gzccgl.getGUID();
        para["FIELD_ISNEW"] = true;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_ccgl_ccgl__["a" /* CcglPage */], para);
    };
    TripPage.prototype.editDatas = function (datas) {
        for (var item = 0; item < datas.length; item++) {
            datas[item].icon = 'ios-arrow-forward';
        }
    };
    ;
    //跳转到登录界面
    TripPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    /**
     * 成功加载用户出差页面时处理
     */
    TripPage.prototype.initSuccessTripPage = function () {
        this.user = this.securityService.user;
        //根据登录用户过滤
        // if(this.user["userid"]!='admin'){
        //   this.formObject.queryObject["qjrid:="]=this.user["userid"];
        // }
        this.gzccgl.datas = [];
        this.gzccgl.dataOver = false;
        this.gzccgl.orderObject["kssj"] = 1;
        this.gzccgl.pageIndex = 1;
        this.gzccgl.sn = this.tableName;
        this.gzccgl.getEntitys(this.editDatas);
    };
    ;
    // setQueryUser(){
    //   this.queryObject["userID:="]=this.user["userid"];
    // };
    //打开或关闭数据详细内容
    TripPage.prototype.toggleDetails = function (data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-forward';
        }
        else {
            data.showDetails = true;
            data.icon = 'ios-arrow-down';
        }
    };
    //查询
    TripPage.prototype.onSearchKeyUp = function (event) {
        if ("Enter" == event.key) {
            //if(this.user!=null && this.user["userid"]!=null){
            // this.formObject.pageIndex=0;
            // this.formObject.dataOver=false;
            // this.formObject.getEntitys();
        }
        //}else{
        //this.queryValue="";
        //}
    };
    TripPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\trip\trip.html"*/'<!--\n\n  Generated template for the LeaveNewPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n	<ion-navbar>\n\n		<ion-title>出差记录</ion-title>\n\n	</ion-navbar>\n\n\n\n</ion-header>\n\n	  \n\n	  \n\n<ion-content>\n\n	<ion-refresher (ionRefresh)="gzccgl.doRefresh($event)">\n\n		<ion-refresher-content pullingIcon="arrow-dropdown"  pullingText="下拉刷新"  refreshingSpinner="circles"  refreshingText="刷新中...">\n\n		</ion-refresher-content>\n\n	</ion-refresher>\n\n	<ion-list>\n\n		<ion-item class="bhq-item" (click)="toQueryWork(item);"\n\n		*ngFor="let item of gzccgl.datas">\n\n	\n\n		<div class="bhq-item-content">\n\n			<div class="bhq-item-info">\n\n				<p class="bhq-item-title"><strong>{{item.wcsy | asLength:12}}({{item.spztName}})</strong></p>\n\n				<p>{{item.ccrxm}}({{item.sqrq | asDate}})</p>\n\n			</div>\n\n			<div class="bhq-item-date">\n\n				<p>开始时间:{{item.kssj | asDate}}</p>\n\n				<p>结束时间:{{item.jssj | asDate}}</p>\n\n			</div>\n\n		</div>\n\n		</ion-item>\n\n	</ion-list>\n\n	<!-- 无级滚动效果实现 -->\n\n	<ion-infinite-scroll (ionInfinite)="gzccgl.doInfinite($event)">\n\n		<ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n	</ion-infinite-scroll>\n\n</ion-content>\n\n'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\trip\trip.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_5__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_6__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], TripPage);
    return TripPage;
}());

//# sourceMappingURL=trip.js.map

/***/ }),

/***/ 161:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeavePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_core_dictionary_DictionaryService__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_bpm_BpmObject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_bpmback_bpmback__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_bpmsign_bpmsign__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_bpmprocess_bpmprocess__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_bpmimg_bpmimg__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_utils_DateUtil__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var LeavePage = /** @class */ (function () {
    function LeavePage(navCtrl, navParams, dataService, baseSErvice, alert, securityService, changeDetectorRef, loadingCtrl, actionSheetCtrl, dateUtil) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseSErvice = baseSErvice;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.dateUtil = dateUtil;
        this.user = {};
        /**
       * 请假类别
       */
        this.qjlb = new __WEBPACK_IMPORTED_MODULE_5__app_core_dictionary_DictionaryService__["a" /* DictionaryService */](this.dataService, this.baseSErvice, "GZ_QJLX", "0", false);
        this.gzqjgl = new __WEBPACK_IMPORTED_MODULE_7__app_bpm_BpmObject__["a" /* BpmObject */](this.dataService, this.baseSErvice, this.changeDetectorRef, this.navCtrl, this.alert, this.loadingCtrl, "gzqjgl");
        if (this.navParams.data != null && this.navParams.data["FIELD_ISNEW"]) {
            this.gzqjgl.editRow = {};
            this.gzqjgl.editRow.id = this.navParams.data["dk"];
            this.gzqjgl.editRow.bpmkey = this.navParams.data["bk"];
            this.gzqjgl.editRow.FIELD_ISNEW = this.navParams.data["FIELD_ISNEW"];
            this.gzqjgl.editRow.sqrq = this.gzqjgl.formatDateToSecond(new Date());
            this.gzqjgl.editRow.qjts = "1";
            this.gzqjgl.editRow.sjsjjd = "08";
            this.gzqjgl.editRow.jssjjd = "18";
            this.gzqjgl.editRow.kssj = this.gzqjgl.formatDate(new Date());
            this.gzqjgl.editRow.jssj = this.gzqjgl.formatDate(new Date());
        }
        this.gzqjgl.sn = "gzqjgl";
        this.gzqjgl.bpmKey = this.navParams.data["bk"];
        this.gzqjgl.id = this.navParams.data["dk"];
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                if (_this.navParams.data != null && _this.navParams.data["bk"] != null) {
                    _this.gzqjgl.findWork(null, null);
                }
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            if (this.navParams.data != null && this.navParams.data["bk"] != null) {
                this.gzqjgl.findWork(null, null);
            }
        }
        if (this.navParams.data["FIELD_ISNEW"]) {
            this.gzqjgl.editRow.dwid = this.user["departmentId"];
            this.gzqjgl.editRow.szdw = this.user["departmentName"];
            this.gzqjgl.editRow.qjrid = this.user["userid"];
            this.gzqjgl.editRow.qjrxm = this.user["userName"];
        }
    }
    LeavePage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    LeavePage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    LeavePage.prototype.goSignPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_bpmsign_bpmsign__["a" /* BpmSignPage */], this.gzqjgl);
    };
    ;
    LeavePage.prototype.goBackPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_bpmback_bpmback__["a" /* BpmBackPage */], this.gzqjgl);
    };
    ;
    LeavePage.prototype.goProcessPage = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: '更多',
            buttons: [
                {
                    text: '办理过程',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_bpmprocess_bpmprocess__["a" /* BpmProcessPage */], _this.gzqjgl);
                    }
                }, {
                    text: '流程图',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__pages_bpmimg_bpmimg__["a" /* BpmImgPage */], _this.gzqjgl);
                    }
                }, {
                    text: '取消',
                    role: '取消',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ;
    LeavePage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad LeaveNewPage');
        this.qjlb.getDatas(null, null);
    };
    ;
    LeavePage.prototype.userFormValidate = function () {
        var message = [];
        if (this.gzqjgl.editRow["qjlx"] == null || this.gzqjgl.editRow["qjlx"] == "") {
            message.push("<span style='color:red'>请假类型<br></span>");
        }
        if (this.gzqjgl.editRow["sqsy"] == null || this.gzqjgl.editRow["sqsy"] == "") {
            message.push("<span style='color:red'>申请事由<br></span>");
        }
        if (this.gzqjgl.editRow["kssj"] == null || this.gzqjgl.editRow["kssj"] == "") {
            message.push("<span style='color:red'>开始时间<br></span>");
        }
        if (this.gzqjgl.editRow["sjsjjd"] == null || this.gzqjgl.editRow["sjsjjd"] == "") {
            message.push("<span style='color:red'>开始时段<br></span>");
        }
        if (this.gzqjgl.editRow["jssj"] == null || this.gzqjgl.editRow["jssj"] == "") {
            message.push("<span style='color:red'>结束时间<br></span>");
        }
        if (this.gzqjgl.editRow["jssjjd"] == null || this.gzqjgl.editRow["jssjjd"] == "") {
            message.push("<span style='color:red'>结束时段<br></span>");
        }
        if (this.gzqjgl.editRow["qjts"] == null || this.gzqjgl.editRow["qjts"] == "") {
            message.push("<span style='color:red'>请假天数<br></span>");
        }
        if (message.length > 0) {
            var strm = message.join("");
            var temp = this.alert.create({
                title: '系统提示',
                subTitle: '以下表单项没有填写正确! <br>' + strm,
                buttons: ['确定']
            });
            temp.present();
            return false;
        }
        else {
            return true;
        }
    };
    ;
    /**
     * 设置天数
     */
    LeavePage.prototype.setTs = function (e) {
        var that = this;
        setTimeout(function () {
            var start = that.dateUtil.parseDate(that.gzqjgl.editRow.kssj, "string");
            // var fmstart = $rootScope.parseData(start,'string');
            var end = that.dateUtil.parseDate(that.gzqjgl.editRow.jssj, "string");
            // var fmend = $rootScope.parseData(end,'string');
            var i = (end - start) / 1000 / 60 / 60 / 24 + 1;
            var time = Number(i.toFixed(0));
            if (that.gzqjgl.editRow.sjsjjd == 14 || that.gzqjgl.editRow.sjsjjd == '14') {
                time = time - 0.5;
            }
            if (that.gzqjgl.editRow.jssjjd == 12 || that.gzqjgl.editRow.jssjjd == '12') {
                time = time - 0.5;
            }
            that.gzqjgl.editRow.qjts = time;
        }, 200);
    };
    ;
    LeavePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-leave',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\leave\leave.html"*/'<ion-header>\n\n	<ion-navbar>\n\n		<ion-title>请假</ion-title>\n\n	</ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n	<ion-list class="approval-list" *ngIf="gzqjgl.proauth.qjsq">\n\n		<ion-list-header>\n\n			<span class="approval-list-header-text">申请信息</span>\n\n		</ion-list-header>\n\n		<ion-item>\n\n			<ion-label class="bhq-title">部门:</ion-label>\n\n			<ion-label class="bhq-content-right">{{gzqjgl.editRow.szdw}}</ion-label>\n\n		</ion-item>\n\n		<ion-item>\n\n			<ion-label class="bhq-title"><font color="red">*</font>请假人姓名:</ion-label>\n\n			<ion-label class="bhq-content-right">{{gzqjgl.editRow.qjrxm}}</ion-label>\n\n		</ion-item>\n\n		<ion-item>\n\n			<ion-label class="bhq-title"><font color="red">*</font>请假类型:</ion-label>\n\n			<ion-select class="bhq-content-right" [(ngModel)]="gzqjgl.editRow.qjlx" cancelText="取消" okText="确认">\n\n				<div *ngFor="let item of qjlb.datas">\n\n					<ion-option value="{{item.id}}">{{item.display}}</ion-option>\n\n				</div>\n\n			</ion-select>\n\n		</ion-item>\n\n		<ion-item>\n\n			<ion-label class="bhq-title"><font color="red">*</font>申请事由:</ion-label>\n\n			<ion-input class="bhq-content-right" [(ngModel)]="gzqjgl.editRow.sqsy"></ion-input>\n\n		</ion-item>\n\n		<ion-item>\n\n			<ion-label class="bhq-title"><font color="red">*</font>开始时间:</ion-label>\n\n			<ion-datetime class="bhq-content-right" (ngModelChange)="setTs($event)"\n\n			max="2100-10-31" displayFormat="YYYY-MM-DD" [(ngModel)]="gzqjgl.editRow.kssj" cancelText="取消"\n\n			 doneText="确认"></ion-datetime>\n\n		</ion-item>\n\n		<ion-item>\n\n			<ion-label class="bhq-title"><font color="red">*</font>开始时段:</ion-label>\n\n			<ion-select class="bhq-content-right" [(ngModel)]="gzqjgl.editRow.sjsjjd"\n\n			 cancelText="取消" okText="确认" (ngModelChange)="setTs($event)">\n\n				<ion-option value="08">08时</ion-option>\n\n				<ion-option value="14">14时</ion-option>\n\n			</ion-select>\n\n		</ion-item>\n\n		<ion-item>\n\n			<ion-label class="bhq-title"><font color="red">*</font>结束时间:</ion-label>\n\n			<ion-datetime class="bhq-content-right"  max="2100-10-31" (ngModelChange)="setTs($event)"\n\n			displayFormat="YYYY-MM-DD" [(ngModel)]="gzqjgl.editRow.jssj" cancelText="取消"\n\n			 doneText="确认"></ion-datetime>\n\n		</ion-item>\n\n		<ion-item>\n\n			<ion-label class="bhq-title"><font color="red">*</font>结束时段:</ion-label>\n\n			<ion-select class="bhq-content-right" [(ngModel)]="gzqjgl.editRow.jssjjd" \n\n			(ngModelChange)="setTs($event)"\n\n			cancelText="取消" okText="确认">\n\n				<ion-option value="12">12时</ion-option>\n\n				<ion-option value="18">18时</ion-option>\n\n			</ion-select>\n\n		</ion-item>\n\n		<ion-item>\n\n			<ion-label class="bhq-title"><font color="red">*</font>请假天数:</ion-label>\n\n			<ion-input class="bhq-content-right" [(ngModel)]="gzqjgl.editRow.qjts"></ion-input>\n\n		</ion-item>\n\n		<ion-item>\n\n			<ion-label class="bhq-title">申请日期:</ion-label>\n\n			<ion-label class="bhq-content-right">{{gzqjgl.editRow.sqrq | date:\'yyyy-MM-dd HH:mm:ss\'}}</ion-label>\n\n		</ion-item>\n\n	</ion-list>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n	<button class="submit-btn" (click)="userFormValidate() && gzqjgl.getNextTask(null,null);">提交</button>\n\n</ion-footer>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\leave\leave.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_12__app_utils_DateUtil__["a" /* DateUtil */]])
    ], LeavePage);
    return LeavePage;
}());

//# sourceMappingURL=leave.js.map

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WfbdPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_form_FormObject__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_adduser_adduser__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var WfbdPage = /** @class */ (function () {
    function WfbdPage(navCtrl, navParams, dataService, baseService, alert, securityService, changeDetectorRef, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseService = baseService;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.readonly = "";
        this.solution = "不显示";
        this.haveError = false;
        this.user = {};
        this.schedule = new __WEBPACK_IMPORTED_MODULE_6__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
        this.schedule.editRow = {};
        this.schedule.sn = "schedule";
        if (this.navParams.data) {
            this.navParams.data.cjsj = this.schedule.formatDate(new Date(this.navParams.data.cjsj));
        }
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                _this.schedule.editRow = _this.navParams.data;
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.schedule.editRow = this.navParams.data;
        }
    }
    WfbdPage.prototype.addUser = function (para) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_adduser_adduser__["a" /* AddUserPage */], para);
    };
    WfbdPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    WfbdPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    WfbdPage.prototype.scheduleSave = function (sn, row) {
        var _this = this;
        if (row.isNew) {
            this.dataService.add(sn, row).subscribe(function (data) {
                if (data.id) {
                    _this.alert.create({
                        title: "系统提示",
                        subTitle: "保存成功！",
                        buttons: ["确定"]
                    }).present();
                }
                else {
                    _this.alert.create({
                        title: "系统提示",
                        subTitle: "保存失败，请重试！",
                        buttons: ["确定"]
                    }).present();
                }
            });
        }
        else {
            this.dataService.update(sn, row).subscribe(function (data) {
                if (data.id) {
                    _this.alert.create({
                        title: "系统提示",
                        subTitle: "保存成功！",
                        buttons: ["确定"]
                    }).present();
                }
                else {
                    _this.alert.create({
                        title: "系统提示",
                        subTitle: "保存失败，请重试！",
                        buttons: ["确定"]
                    }).present();
                }
            });
        }
    };
    WfbdPage.prototype.query = function () {
        var _this = this;
        this.alert.create({
            title: "系统提示",
            message: "是否确认?",
            buttons: [{
                    text: "确定",
                    role: "确定",
                    handler: function () {
                        var data = _this.schedule.editRow;
                        data.zt = "rwzt_yyd";
                        _this.dataService.update(_this.schedule.sn, data);
                        _this.navCtrl.pop();
                    }
                }, {
                    text: "取消",
                    role: "取消",
                    handler: function () {
                    }
                }]
        }).present();
    };
    ;
    WfbdPage.prototype.complete = function () {
        var _this = this;
        this.alert.create({
            title: "系统提示",
            message: "是否确认完成?",
            buttons: [{
                    text: "确定",
                    role: "确定",
                    handler: function () {
                        var data = _this.schedule.editRow;
                        data.zt = "rwzt_ywc";
                        _this.dataService.update(_this.schedule.sn, data);
                        _this.navCtrl.pop();
                    }
                }, {
                    text: "取消",
                    role: "取消",
                    handler: function () {
                    }
                }]
        }).present();
    };
    ;
    WfbdPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-wfbd',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\wfbd\wfbd.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>任务发布</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content>\n\n\n\n	<ion-list class="approval-list">\n\n    <ion-item>\n\n      <ion-label>项目名称:</ion-label>\n\n      <ion-input [(ngModel)]="schedule.editRow.xmmc"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>主题:</ion-label>\n\n      <ion-input [(ngModel)]="schedule.editRow.zhuti"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>部门名称:</ion-label>\n\n      <ion-input [(ngModel)]="schedule.editRow.bmmcName"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>负责人:</ion-label>\n\n      <ion-input [(ngModel)]="schedule.editRow.fzrmc"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>开始时间:</ion-label>\n\n      <ion-datetime displayFormat="YYYY-MM-DD" [(ngModel)]="schedule.editRow.kssj" cancelText="取消" doneText="确认" ></ion-datetime>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>结束时间:</ion-label>\n\n      <ion-datetime displayFormat="YYYY-MM-DD" [(ngModel)]="schedule.editRow.jssj" cancelText="取消" doneText="确认" ></ion-datetime>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>备注:</ion-label>\n\n      <ion-textarea autoresize [(ngModel)]="schedule.editRow.sxsm" style="height:120px;"></ion-textarea>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>状态:</ion-label>\n\n      <ion-input [(ngModel)]="schedule.editRow.bmmcName"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>发布人:</ion-label>\n\n      <ion-label>{{schedule.editRow.cjrmc}}</ion-label>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>发布时间:</ion-label>\n\n      <ion-label>{{schedule.editRow.cjsj}}</ion-label>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>完成时间:</ion-label>\n\n      <ion-label>{{schedule.editRow.wcsj | asDate}}</ion-label>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <!-- <div class="zs-course-info" *ngIf="schedule.editRow.gztask">\n\n    <h2 class="zs-course-info-h2">参与人\n\n        <a style="margin-right: 14px;"><button ion-button (click)=\'addUser(schedule.editRow.gztask)\'>\n\n            添加\n\n        </button>\n\n        <button ion-button (click)=\'dropUser()\'>\n\n            删除\n\n        </button></a>\n\n    </h2>\n\n    <p class="zs-course-info-p" *ngFor="let item of schedule.editRow.gztask">\n\n      {{item.jsrmc}}</p>\n\n  </div> -->\n\n\n\n</ion-content>\n\n<ion-footer >\n\n  <button class="submit-btn" (click)="scheduleSave(\'schedule\',schedule.editRow)">保存</button>\n\n</ion-footer>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\wfbd\wfbd.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], WfbdPage);
    return WfbdPage;
}());

//# sourceMappingURL=wfbd.js.map

/***/ }),

/***/ 186:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApprovalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ApprovalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ApprovalPage = /** @class */ (function () {
    function ApprovalPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ApprovalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ApprovalPage');
    };
    ApprovalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-approval',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\approval\approval.html"*/'<!--\n\n  Generated template for the ApprovalPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>流程审批</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">申请信息</span>\n\n    </ion-list-header>\n\n    <ion-item>\n\n      <ion-label>部门:</ion-label>\n\n      <ion-input type="text"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>姓名:</ion-label>\n\n      <ion-input type="text"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>请假类型:</ion-label>\n\n      <ion-input type="text"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>申请事由:</ion-label>\n\n      <ion-input type="text"></ion-input>\n\n    </ion-item>\n\n  </ion-list>\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">同行人</span>\n\n    </ion-list-header>\n\n    <ion-item>\n\n      <ion-label>资源科:</ion-label>\n\n      <ion-input type="text"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>森林科:</ion-label>\n\n      <ion-input type="text"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>森防科:</ion-label>\n\n      <ion-input type="text"></ion-input>\n\n    </ion-item>\n\n  </ion-list>\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">科室审批</span>\n\n    </ion-list-header>\n\n    <ion-item>\n\n      <div class="approval-flex">\n\n        <p>第一次审批</p>\n\n        <p>审批人：张强</p>\n\n        <p>2018-09-08</p>\n\n      </div>\n\n    </ion-item>\n\n    <ion-item>\n\n      <div class="approval-flex">\n\n        <p>第二次审批</p>\n\n        <p>审批人：张强</p>\n\n        <p>2018-09-08</p>\n\n      </div>\n\n    </ion-item>\n\n  </ion-list>\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">分管领导审批</span>\n\n    </ion-list-header>\n\n    <ion-item>\n\n      <div class="approval-flex">\n\n        <p>第一次审批</p>\n\n        <p>审批人：张强</p>\n\n        <p>2018-09-08</p>\n\n      </div>\n\n    </ion-item>\n\n    <ion-item>\n\n      <div class="approval-flex">\n\n        <p>第二次审批</p>\n\n        <p>审批人：张强</p>\n\n        <p>2018-09-08</p>\n\n      </div>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>审批意见:</ion-label>\n\n    <ion-input type="text"></ion-input>\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>\n\n<ion-footer>\n\n  <div class="footer-btn">\n\n    <img src="assets/imgs/back.png"/>\n\n    <p>退回</p>\n\n  </div>\n\n  <div class="footer-btn">\n\n    <img src="assets/imgs/save.png"/>\n\n    <p>保存</p>\n\n  </div>\n\n  <div class="footer-btn">\n\n    <img src="assets/imgs/pass.png"/>\n\n    <p>通过</p>\n\n  </div>\n\n  <div class="footer-more">更多</div>\n\n</ion-footer>\n\n'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\approval\approval.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ApprovalPage);
    return ApprovalPage;
}());

//# sourceMappingURL=approval.js.map

/***/ }),

/***/ 187:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CalendarPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__app_core_dictionary_DictionaryService__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_adduser_adduser__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var CalendarPage = /** @class */ (function () {
    function CalendarPage(navCtrl, navParams, dataService, baseSErvice, alert, securityService, changeDetectorRef, loadingCtrl, actionSheetCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseSErvice = baseSErvice;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.user = {};
        this.cxfs = new __WEBPACK_IMPORTED_MODULE_11__app_core_dictionary_DictionaryService__["a" /* DictionaryService */](this.dataService, this.baseSErvice, "GZ_RSGL_CCGL_CXFS", "0", false);
        this.gzccgl = new __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__["a" /* BpmObject */](this.dataService, this.baseSErvice, this.changeDetectorRef, this.navCtrl, this.alert, this.loadingCtrl, "gzccgl");
        if (this.navParams.data != null && this.navParams.data["bk"] != null) {
            this.gzccgl.editRow = {};
            this.gzccgl.sn = "gzccgl";
            this.gzccgl.bpmKey = this.navParams.data["bk"];
            this.gzccgl.id = this.navParams.data["dk"];
            this.gzccgl.editRow.id = this.navParams.data["dk"];
            this.gzccgl.editRow.bpmkey = this.navParams.data["bk"];
            this.gzccgl.editRow.FIELD_ISNEW = this.navParams.data["FIELD_ISNEW"];
            this.gzccgl.editRow.sqrq = this.gzccgl.formatDate(new Date());
            this.gzccgl.editRow.qjts = "1";
            if (this.securityService.user != null && this.securityService.user["userid"] != null) {
                this.gzccgl.editRow.ccrxm = this.securityService.user["userName"];
                this.gzccgl.editRow.ccrid = this.securityService.user["userid"];
                this.gzccgl.editRow.dwid = this.securityService.user["departmentId"];
            }
        }
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                _this.gzccgl.editRow.ccrid = _this.securityService.user["userid"];
                if (_this.navParams.data != null && _this.navParams.data["bk"] != null) {
                    _this.gzccgl.findWork(null, null);
                }
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.gzccgl.editRow.ccrid = this.securityService.user["userid"];
            if (this.navParams.data != null && this.navParams.data["bk"] != null) {
                this.gzccgl.findWork(null, null);
            }
        }
    }
    CalendarPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    CalendarPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    CalendarPage.prototype.goSignPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__["a" /* BpmSignPage */], this.gzccgl);
    };
    ;
    CalendarPage.prototype.goBackPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__["a" /* BpmBackPage */], this.gzccgl);
    };
    ;
    CalendarPage.prototype.goProcessPage = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: '更多',
            buttons: [
                {
                    text: '办理过程',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__["a" /* BpmProcessPage */], _this.gzccgl);
                    }
                }, {
                    text: '流程图',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__["a" /* BpmImgPage */], _this.gzccgl);
                    }
                }, {
                    text: '取消',
                    role: '取消',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ;
    CalendarPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad LeaveNewPage');
        this.cxfs.getDatas(null, null);
    };
    CalendarPage.prototype.addUser = function () {
        if (this.gzccgl.editRow == null) {
            this.gzccgl.editRow = {};
        }
        if (this.gzccgl.editRow.ccsxry == null) {
            this.gzccgl.editRow.ccsxry = [];
        }
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_12__pages_adduser_adduser__["a" /* AddUserPage */], this.gzccgl.editRow.ccsxry);
    };
    CalendarPage.prototype.userFormValidate = function () {
        var message = [];
        if (this.gzccgl.editRow["cfd"] == null || this.gzccgl.editRow["cfd"] == "") {
            message.push("<span style='color:red'>出发地<br></span>");
        }
        if (this.gzccgl.editRow["mdd"] == null || this.gzccgl.editRow["mdd"] == "") {
            message.push("<span style='color:red'>目的地<br></span>");
        }
        if (this.gzccgl.editRow["wcsy"] == null || this.gzccgl.editRow["wcsy"] == "") {
            message.push("<span style='color:red'>外出事由<br></span>");
        }
        if (this.gzccgl.editRow["kssj"] == null || this.gzccgl.editRow["kssj"] == "") {
            message.push("<span style='color:red'>开始时间<br></span>");
        }
        if (this.gzccgl.editRow["jssj"] == null || this.gzccgl.editRow["jssj"] == "") {
            message.push("<span style='color:red'>开始时间<br></span>");
        }
        if (this.gzccgl.editRow["cxfs"] == null || this.gzccgl.editRow["cxfs"] == "") {
            message.push("<span style='color:red'>交通工具<br></span>");
        }
        if (message.length > 0) {
            var strm = message.join("");
            var temp = this.alert.create({
                title: '系统提示',
                subTitle: '以下表单项没有填写正确! <br>' + strm,
                buttons: ['确定']
            });
            temp.present();
            return false;
        }
        else {
            return true;
        }
    };
    CalendarPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-calendar',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\calendar\calendar.html"*/'<!--\n\n  Generated template for the CalendarPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>出差</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">申请信息</span>\n\n    </ion-list-header>\n\n    <ion-item>\n\n      \n\n      <ion-label class="bhq-title"><font color="red">*</font>申请人:</ion-label>\n\n      <ion-label class="bhq-content-right">{{gzccgl.editRow.ccrxm}}</ion-label>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label class="bhq-title">职务:</ion-label>\n\n      <ion-input class="bhq-content-right" [(ngModel)]="gzccgl.editRow.ccrzw"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n        \n\n      <ion-label class="bhq-title"><font color="red">*</font>出发地:</ion-label>\n\n      <ion-input class="bhq-content-right" [(ngModel)]="gzccgl.editRow.cfd" ></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n       \n\n      <ion-label class="bhq-title"> <font color="red">*</font>目的地:</ion-label>\n\n      <ion-input class="bhq-content-right" [(ngModel)]="gzccgl.editRow.mdd"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n       \n\n      <ion-label class="bhq-title"> <font color="red">*</font>外出事由:</ion-label>\n\n      <ion-input class="bhq-content-right" [(ngModel)]="gzccgl.editRow.wcsy"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n        \n\n      <ion-label class="bhq-title"><font color="red">*</font>开始时间:</ion-label>\n\n      <ion-datetime class="bhq-content-right" displayFormat="YYYY-MM-DD"\n\n          max="2100-10-31"\n\n         [(ngModel)]="gzccgl.editRow.kssj" cancelText="取消" doneText="确认"></ion-datetime>\n\n  \n\n    </ion-item>\n\n    <ion-item>\n\n      \n\n      <ion-label class="bhq-title">  <font color="red">*</font>结束时间:</ion-label>\n\n      <ion-datetime class="bhq-content-right" \n\n      max="2100-10-31" displayFormat="YYYY-MM-DD" [(ngModel)]="gzccgl.editRow.jssj" cancelText="取消" doneText="确认"></ion-datetime>\n\n    </ion-item>\n\n    <ion-item>\n\n      \n\n      <ion-label class="bhq-title">  <font color="red">*</font>出行方式:</ion-label>\n\n      <ion-select class="bhq-content-right" [(ngModel)]="gzccgl.editRow.cxfs" cancelText="取消" okText="确认">\n\n          <div *ngFor="let item of cxfs.datas">\n\n            <ion-option value="{{item.id}}">{{item.display}}</ion-option>\n\n          </div>\n\n      </ion-select>\n\n    </ion-item>\n\n    <!--<ion-item>\n\n      <ion-checkbox [(ngModel)]="gzccgl.editRow.cxfsfj"></ion-checkbox>\n\n      <ion-label>飞机</ion-label>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-checkbox [(ngModel)]="gzccgl.editRow.cxfshc"></ion-checkbox>\n\n      <ion-label>火车</ion-label>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-checkbox [(ngModel)]="gzccgl.editRow.cxfsdwpc"></ion-checkbox>\n\n      <ion-label>单位派车</ion-label>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-checkbox [(ngModel)]="gzccgl.editRow.cxfsqc"></ion-checkbox>\n\n      <ion-label>汽车</ion-label>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-checkbox [(ngModel)]="gzccgl.editRow.cxfszdc"></ion-checkbox>\n\n      <ion-label>自带车</ion-label>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-checkbox [(ngModel)]="gzccgl.editRow.cxfsqt"></ion-checkbox>\n\n      <ion-label>其他</ion-label>\n\n    </ion-item> -->\n\n    <ion-item>\n\n      <ion-label class="bhq-title">申请日期:</ion-label>\n\n      <ion-datetime class="bhq-content-right" displayFormat="YYYY-MM-DD" [(ngModel)]="gzccgl.editRow.sqrq" cancelText="取消" doneText="确认"></ion-datetime>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">同行出差人</span>\n\n      <span style="float: right;padding-right: 1rem;" (click)="addUser();"> \n\n       <!--<ion-icon name="ios-arrow-forward-outline"></ion-icon>--> \n\n       <ion-icon name="md-contacts"></ion-icon>\n\n      </span>\n\n    </ion-list-header>\n\n    <ion-item *ngFor="let item of gzccgl.editRow.ccsxry">\n\n      <ion-label class="bhq-content">{{item.jsrmc}}({{item.jsrjgmc}})</ion-label>\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>\n\n<ion-footer>\n\n  <button class="submit-btn" (click)="userFormValidate() && gzccgl.getNextTask(null,null);">提交</button>\n\n</ion-footer>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\calendar\calendar.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"]])
    ], CalendarPage);
    return CalendarPage;
}());

//# sourceMappingURL=calendar.js.map

/***/ }),

/***/ 188:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the MePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MePage = /** @class */ (function () {
    function MePage(navCtrl, navParams, baseService, dataService, securityService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.baseService = baseService;
        this.dataService = dataService;
        this.securityService = securityService;
        this.user = {};
    }
    MePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MePage');
    };
    //跳转到登录界面
    MePage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    //初始化页面
    MePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.user = {};
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
            }, function () {
                _this.user = {};
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
        }
    };
    ;
    MePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-me',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\me\me.html"*/'<!--\n\n  Generated template for the MePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>我的</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <div class="me-header">\n\n    <!-- <img src="assets/imgs/people.png"/> -->\n\n    <ion-icon name="person" style="font-size:5em;color: #5477b0;"></ion-icon>\n\n    <div class="user-info">\n\n      <p class="user-name">{{user.userName}}</p>\n\n      <p class="user-company">{{user.departmentName}}</p>\n\n    </div>\n\n  </div>\n\n  <ion-list class="bhq-list me-list">\n\n    <ion-item-group>\n\n      <ion-item class="bhq-item" *ngIf="securityService.user" (click)=\'securityService.logout();goLogin();\'>\n\n        <ion-thumbnail item-start>\n\n          <!-- <img src="assets/imgs/loginOut.png"> -->\n\n          <ion-icon name="power" style="color: #5477b0;"></ion-icon>\n\n        </ion-thumbnail>\n\n        <h2 class="bhq-item-title">退出登录</h2>\n\n      </ion-item>\n\n      <!-- <ion-item class="bhq-item">\n\n        <ion-thumbnail item-start>\n\n          <img src="assets/imgs/album.png">\n\n        </ion-thumbnail>\n\n        <h2 class="bhq-item-title">修改密码</h2>\n\n      </ion-item>-->\n\n      <ion-item class="bhq-item">\n\n        <ion-thumbnail item-start>\n\n          <!-- <img src="assets/imgs/xtbb.png"> -->\n\n          <ion-icon name="ribbon" style="color: #5477b0;"></ion-icon>\n\n        </ion-thumbnail>\n\n        <h2 class="bhq-item-title">系统版本</h2>\n\n      </ion-item>\n\n    </ion-item-group>\n\n    <!--<ion-item-group>\n\n      <ion-item class="bhq-item">\n\n        <ion-thumbnail item-start>\n\n          <img src="assets/imgs/setting.png">\n\n        </ion-thumbnail>\n\n        <h2 class="bhq-item-title">设置</h2>\n\n      </ion-item>\n\n    </ion-item-group>-->\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\me\me.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_4__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_2__app_core_security_SecurityService__["a" /* SecurityService */]])
    ], MePage);
    return MePage;
}());

//# sourceMappingURL=me.js.map

/***/ }),

/***/ 189:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TaskPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__done_done__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__process_process__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_form_FormObject__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_wfbd_wfbd__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_rcap_rcap__ = __webpack_require__(82);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the TodoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TaskPage = /** @class */ (function () {
    function TaskPage(navCtrl, navParams, baseService, dataService, securityService, changeDetectorRef, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.baseService = baseService;
        this.dataService = dataService;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.datas = [];
        this.user = {};
        this.tableName = 'gztaskview';
        this.showDetails = false;
        this.workInput = "";
        //构建formObject对象方式修改，这种方式建立了独立的对象数据
        this.gztaskview = new __WEBPACK_IMPORTED_MODULE_4__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
    }
    TaskPage.prototype.searchWork = function () {
        //this.worklist.queryObject={"sbusiness:like":this.workInput?this.workInput:""};
        if (this.workInput == null || this.workInput == "") {
            this.gztaskview.queryObject = {};
        }
        else {
            this.gztaskview.queryObject["zhuti:like"] = this.workInput;
        }
        this.gztaskview.pageIndex = 1; //重置查询条件的时候，需要重置页码
        this.gztaskview.datas = [];
        this.gztaskview.getEntitys(this.editData);
    };
    TaskPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TodoPage');
    };
    TaskPage.prototype.buttonClick = function () {
        console.log('buttonClick');
    };
    TaskPage.prototype.toDonePage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__done_done__["a" /* DonePage */]);
    };
    ;
    TaskPage.prototype.toProcessPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__process_process__["a" /* ProcessPage */]);
    };
    ;
    TaskPage.prototype.toQueryTask = function (data) {
        var _this = this;
        var para = data;
        if (data.zt == "rwzt_yyd" || data.zt == "rwzt_ywc") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_rcap_rcap__["a" /* RcapPage */], para);
        }
        else {
            data.zt = "rwzt_yyd";
            this.dataService.update(this.gztaskview.sn, data).subscribe(function (data) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_rcap_rcap__["a" /* RcapPage */], para);
            });
        }
    };
    ;
    //初始化页面
    TaskPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.user = {};
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                _this.initSuccessHomePage();
            }, function () {
                _this.user = {};
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.initSuccessHomePage();
        }
    };
    ;
    //跳转到登录界面
    TaskPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    /**
     * 成功加载用户home页面时处理
     */
    TaskPage.prototype.initSuccessHomePage = function () {
        this.user = this.securityService.user;
        //构建formObject对象方式修改，这种方式建立了独立的对象数据
        //if(this.gztaskview.sn==""){
        this.gztaskview = new __WEBPACK_IMPORTED_MODULE_4__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
        this.gztaskview.sn = "gztaskview";
        this.gztaskview.orderObject = { "cjsj": 1 };
        //}
        this.gztaskview.getEntitys(this.editData);
    };
    ;
    //对任务数据进行图片标识处理
    TaskPage.prototype.editData = function (data) {
        if (data.length > 0) {
            for (var item = 0; item < data.length; item++) {
                if (data[item].zt == "rwzt_ywc") {
                    data[item].picname = "complete";
                }
                else {
                    data[item].picname = "not";
                }
            }
        }
    };
    ;
    TaskPage.prototype.addScheduleRow = function () {
        var editRow = {};
        editRow.isNew = true;
        editRow.id = this.gztaskview.getGUID();
        editRow.bmmc = this.user["departmentId"];
        editRow.bmmcName = this.user["departmentName"];
        editRow.cjrid = this.user["userid"];
        editRow.cjrmc = this.user["userName"];
        editRow.cjsj = new Date().getTime();
        ;
        editRow.zt = 'rwzt_wyd';
        editRow.ztName = '未阅读';
        editRow.gztask = [];
        this.toQuerySchedule(editRow);
    };
    TaskPage.prototype.toQuerySchedule = function (data) {
        var para = data;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_wfbd_wfbd__["a" /* WfbdPage */], para);
    };
    ;
    TaskPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-task',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\task\task.html"*/'<!--\n\n  Generated template for the TaskPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <ion-navbar>\n\n    <ion-icon name="contact" style="font-size:2em;color: #FFF;margin-left:0.8rem;margin-top:0.3rem;"></ion-icon>\n\n    <span class="title-user">{{securityService.user.userName}}</span>\n\n    <ion-buttons end>\n\n      <!--<button ion-button clear small icon-end>\n\n        <span class="right-button">已完成</span>\n\n        <ion-icon class="task-arrow-down" name="arrow-down"></ion-icon>\n\n      </button>\n\n      <button class="task-add-btn" ion-button icon-only (click)="addScheduleRow()">\n\n        <ion-icon name="add"></ion-icon>\n\n      </button>-->\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <div class="search-bar-content">\n\n    <ion-searchbar placeholder="搜索" (ionInput)="searchWork()" [(ngModel)]="workInput">\n\n    </ion-searchbar>\n\n  </div>\n\n\n\n  <ion-refresher (ionRefresh)="gztaskview.doRefresh($event,editData)">\n\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="下拉刷新" refreshingSpinner="circles" refreshingText="刷新中...">\n\n    </ion-refresher-content>\n\n  </ion-refresher>\n\n  <ion-list class="bhq-list">\n\n    <ion-item class="bhq-item" *ngFor="let item of gztaskview.datas" (click)="toQueryTask(item)">\n\n      <ion-thumbnail item-start>\n\n        <img src="assets/imgs/{{item.picname}}.png">\n\n      </ion-thumbnail>\n\n      <h2 class="bhq-item-title">{{item.zhuti | asLength:12}}</h2>\n\n      <div class="bhq-item-content">\n\n        <div class="bhq-item-info">\n\n          <p>{{item.cjrmc}}</p>\n\n        </div>\n\n        <div class="bhq-item-date">\n\n          <p>{{item.cjsj | asDate}}</p>\n\n        </div>\n\n      </div>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <!-- 无级滚动效果实现 -->\n\n  <ion-infinite-scroll (ionInfinite)="worklist.doInfinite($event,editData)">\n\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n  </ion-infinite-scroll>\n\n</ion-content>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\task\task.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_7__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_8__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_6__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], TaskPage);
    return TaskPage;
}());

//# sourceMappingURL=task.js.map

/***/ }),

/***/ 190:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__sign_sign__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__sign_new_sign_new__ = __webpack_require__(350);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__leave_new_leave_new__ = __webpack_require__(351);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__vehicle_vehicle__ = __webpack_require__(352);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__vehicleannal_vehicleannal__ = __webpack_require__(353);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__trip_trip__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_form_FormObject__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__calendar_calendar__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__leave_leave__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__approval_approval__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__swgl_new_swgl_new__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__fwgl_fwgl__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__app_buiness_SignHttp__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



















/**
 * Generated class for the JobsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var JobsPage = /** @class */ (function () {
    function JobsPage(navCtrl, navParams, baseService, dataService, alertCtrl, securityService, changeDetectorRef, signHttp, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.baseService = baseService;
        this.dataService = dataService;
        this.alertCtrl = alertCtrl;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.signHttp = signHttp;
        this.loadingCtrl = loadingCtrl;
        this.user = {};
        this.mi = {};
        this.si = {};
        //declare let AMap;
        this.latitude = ""; //维度
        this.longitude = ""; //经度
        this.formattedAddress = "";
        this.accuracy = "";
    }
    /**
     * 调用高德的接口，获取高精度的地图位置服务
     */
    JobsPage.prototype.goLocation = function () {
        var that = this;
        var mapObj = new AMap.Map('iCenter');
        mapObj.plugin('AMap.Geolocation', function () {
            var _this = this;
            var geolocation = new AMap.Geolocation({
                enableHighAccuracy: true,
                timeout: 10000,
                maximumAge: 0,
                convert: true,
                showButton: true,
                buttonPosition: 'LB',
                buttonOffset: new AMap.Pixel(10, 20),
                showMarker: true,
                showCircle: true,
                panToLocation: true,
                zoomToAccuracy: true //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
            });
            mapObj.addControl(geolocation);
            geolocation.getCurrentPosition();
            AMap.event.addListener(geolocation, 'complete', that.onComplete.bind(that)); //返回定位信息
            AMap.event.addListener(geolocation, 'error', function (data) {
                //console.log('定位失败' + data);
                var alert = _this.alertCtrl.create({
                    title: '系统提示',
                    subTitle: "定位失败，请重试一下！",
                    buttons: ['确定']
                });
            }); //返回定位出错信息
        });
    }; //解析定位结果
    JobsPage.prototype.onComplete = function (data) {
        /* console.log(data);
         console.log(data.position.toString());
         console.log(data.formattedAddress);
         var str=['定位成功'];
         str.push('经度：' + data.position.getLng());
         str.push('纬度：' + data.position.getLat());
         if(data.accuracy){
           str.push('精度：' + data.accuracy + ' 米');
         }//如为IP精确定位结果则没有精度信息
         str.push('是否经过偏移：' + (data.isConverted ? '是' : '否'));
         let alert = this.alertCtrl.create({
           title: '系统提示',
           subTitle: "高德导航到坐标位置x:"+data.position.getLng()+",y:"+data.position.getLat(),
           buttons: ['确定']
         });
         alert.present();*/
        this.longitude = data.position.getLng();
        this.latitude = data.position.getLat();
        this.accuracy = data.accuracy;
        this.analysisPositionFun(this.longitude, this.latitude);
        // document.getElementById('tip').innerHTML = str.join('<br>');
    };
    JobsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad JobsPage');
    };
    JobsPage.prototype.toSignNewPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__sign_new_sign_new__["a" /* SignNewPage */]);
    };
    JobsPage.prototype.toLeaveNewPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__leave_new_leave_new__["a" /* LeaveNewPage */]);
    };
    JobsPage.prototype.startNewBpm = function (bpmkey, formname) {
        var para = {};
        para["bk"] = bpmkey;
        para["fn"] = formname;
        para["vi"] = formname;
        var formObject = new __WEBPACK_IMPORTED_MODULE_10__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
        para["dk"] = formObject.getGUID();
        para["FIELD_ISNEW"] = true;
        if (formname == "gzqjgl") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_12__leave_leave__["a" /* LeavePage */], para);
        }
        else if (formname == "gzccgl") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__calendar_calendar__["a" /* CalendarPage */], para);
        }
        else if (formname == "gzpublishfile") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_17__fwgl_fwgl__["a" /* FwglPage */], para);
        }
        else if (formname == "gzclsq") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__vehicle_vehicle__["a" /* VehiclePage */], para);
        }
    };
    JobsPage.prototype.toApprovalPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__approval_approval__["a" /* ApprovalPage */]);
    };
    ;
    JobsPage.prototype.toSwglNewPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_16__swgl_new_swgl_new__["a" /* SwglNewPage */]);
    };
    ;
    //初始化页面
    JobsPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.user = {};
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                _this.mi = _this.securityService.mi;
                _this.si = _this.securityService.si;
            }, function () {
                _this.user = {};
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.mi = this.securityService.mi;
            this.si = this.securityService.si;
        }
    };
    ;
    //跳转到登录界面
    JobsPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    JobsPage.prototype.goSign = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__sign_sign__["a" /* SignPage */]);
    };
    JobsPage.prototype.goVehicleAnnal = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__vehicleannal_vehicleannal__["a" /* VehicleannalPage */]);
    };
    JobsPage.prototype.goTrip = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__trip_trip__["a" /* TripPage */]);
    };
    JobsPage.prototype.goLeave = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__leave_new_leave_new__["a" /* LeaveNewPage */]);
    };
    /**
     * 用户签到同时定位服务
     */
    JobsPage.prototype.userQdAndPostion = function () {
        this.latitude = "";
        this.longitude = "";
        this.accuracy = "";
        this.formattedAddress = "";
        this.goLocation();
    };
    ;
    JobsPage.prototype.userQd = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: '系统提示?',
            message: "当前位置" + this.formattedAddress + ",确定签到？",
            buttons: [
                {
                    text: '确定',
                    handler: function () {
                        // console.log('Disagree clicked');
                        _this.gzqdgldata("create");
                    }
                },
                {
                    text: '取消',
                    handler: function () {
                        //console.log('Agree clicked');
                    }
                }
            ]
        });
        confirm.present();
    };
    JobsPage.prototype.gzqdgldata = function (method) {
        var _this = this;
        this.signHttp.gzqdgldata(method, this.latitude, this.longitude, this.formattedAddress, this.accuracy).subscribe(function (data) {
            if (data) {
                if (data["status"]) {
                    if (data["type"] == "create") {
                        var alert_1 = _this.alertCtrl.create({
                            title: '系统提示',
                            subTitle: data["message"],
                            buttons: ['确定']
                        });
                        alert_1.present();
                    }
                    else {
                        var confirm_1 = _this.alertCtrl.create({
                            title: '系统提示?',
                            message: data["message"],
                            buttons: [
                                {
                                    text: '确定',
                                    handler: function () {
                                        // console.log('Disagree clicked');
                                        _this.gzqdgldata("update");
                                    }
                                },
                                {
                                    text: '取消',
                                    handler: function () {
                                        //console.log('Agree clicked');
                                    }
                                }
                            ]
                        });
                        confirm_1.present();
                    }
                }
                else {
                    var alert_2 = _this.alertCtrl.create({
                        title: '系统提示',
                        subTitle: data["message"],
                        buttons: ['确定']
                    });
                    alert_2.present();
                }
            }
            else {
                var alert_3 = _this.alertCtrl.create({
                    title: '系统提示',
                    subTitle: '打卡失败，请重试！',
                    buttons: ['确定']
                });
                alert_3.present();
            }
        }, function (err) {
            var alert = _this.alertCtrl.create({
                title: '系统提示',
                subTitle: '打卡失败，请重试！',
                buttons: ['确定']
            });
            alert.present();
        });
    };
    // 获取经纬度，转换地址。(原始方式，定位精度不高)
    JobsPage.prototype.getPositionFun = function () {
        var _this = this;
        navigator.geolocation.getCurrentPosition(function (position) {
            console.log(position);
            console.log('纬度: ' + position.coords.latitude + '\n' +
                '经度: ' + position.coords.longitude + '\n' +
                '海拔: ' + position.coords.altitude + '\n' +
                '水平精度: ' + position.coords.accuracy + '\n' +
                '垂直精度: ' + position.coords.altitudeAccuracy + '\n' +
                '方向: ' + position.coords.heading + '\n' +
                '速度: ' + position.coords.speed + '\n' +
                '时间戳: ' + position.timestamp + '\n');
            // console.log(AMap);
            // 外面定义获取经纬度的变量，方便界面显示。
            _this.latitude = position.coords.latitude;
            _this.longitude = position.coords.longitude;
            var alert = _this.alertCtrl.create({
                title: '系统提示',
                subTitle: "导航到坐标位置x:" + _this.longitude + ",y:" + _this.latitude,
                buttons: ['确定']
            });
            alert.present();
        }, this.onError, { timeout: 30000 });
    };
    //定位数据获取失败响应
    JobsPage.prototype.onError = function (error) {
        alert('code: ' + error.code + '\n' +
            'message: ' + error.message + '\n');
    };
    JobsPage.prototype.testAnalysis = function () {
        // this.analysisPositionFun();
        this.analysisPositionFun(113.018258, 28.095051);
    };
    ;
    /**
     * 通过经纬度解析当前的位置
     * @param longitude
     * @param latitude
     */
    JobsPage.prototype.analysisPositionFun = function (longitude, latitude) {
        var _this = this;
        AMap.service("AMap.Geocoder", function () {
            var geocoder = new AMap.Geocoder({
                //city: "010",
                radius: 100 //范围，默认：500
            });
            console.log(geocoder, "fuwu");
            var positionInfo = [longitude + "", latitude + ""];
            console.log(positionInfo);
            geocoder.getAddress(positionInfo, function (status, result) {
                console.log(status, result, "转换定位信息");
                if (status === 'complete' && result.info === 'OK') {
                    //获得了有效的地址信息:
                    console.log(result.regeocode.formattedAddress);
                    _this.formattedAddress = result.regeocode.formattedAddress;
                    /*let alert = this.alertCtrl.create({
                      title: '系统提示',
                      subTitle: this.formattedAddress.toString(),
                      buttons: ['确定']
                    });
                    alert.present();*/
                    _this.userQd();
                }
                else {
                    //获取地址失败
                    var alert_4 = _this.alertCtrl.create({
                        title: '系统提示',
                        subTitle: "定位失败",
                        buttons: ['确定']
                    });
                    alert_4.present();
                }
            });
        });
    };
    JobsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-jobs',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\jobs\jobs.html"*/'<!--\n\n  Generated template for the JobsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <ion-navbar>\n\n    <!-- <img src="assets/imgs/peo_min22.png" /> -->\n\n    <ion-icon name="contact" style="font-size:2em;color: #FFF;margin-left:0.8rem;margin-top:0.3rem;"></ion-icon>\n\n    <span class="title-user">{{securityService.user.userName}}</span>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <div class="jobs-wrapper">\n\n    <div class="jobs-sub-title">\n\n      <span class="sub-title-icon"></span>\n\n      <p>考勤管理</p>\n\n    </div>\n\n    <div class="jobs-grid">\n\n      <!--<div class="job-item" (click)="toSignNewPage()">-->\n\n      <div class="job-item" (click)="userQdAndPostion()">\n\n        <img src="assets/imgs/sign_big.png" />\n\n        <p class="text">签到</p>\n\n      </div>\n\n      <!-- <div class="job-item" (click)="toLeaveNewPage()">\n\n        <img src="assets/imgs/leave_big.png"/>\n\n        <p class="text">请假</p>\n\n      </div> -->\n\n      <!--科员请假流程-->\n\n      <div class="job-item" (click)="startNewBpm(\'qjgl\',\'gzqjgl\')" *ngIf="mi.sa || mi.rsgl_kqgl_qjgl_ky">\n\n        <img src="assets/imgs/leave_big.png" />\n\n        <p class="text">请假</p>\n\n      </div>\n\n\n\n      <!--科级干部请假流程-->\n\n      <div class="job-item" (click)="startNewBpm(\'qjgl_kjgb\',\'gzqjgl\')" *ngIf="mi.rsgl_kqgl_qjgl_kz">\n\n        <img src="assets/imgs/leave_big.png" />\n\n        <p class="text">请假</p>\n\n      </div>\n\n\n\n      <!--副局请假流程-->\n\n      <div class="job-item" (click)="startNewBpm(\'qjgl_jjgb\',\'gzqjgl\')" *ngIf="mi.rsgl_kjgb_qjgl_jj">\n\n        <img src="assets/imgs/leave_big.png" />\n\n        <p class="text">请假</p>\n\n      </div>\n\n\n\n      <!--局长请假流程-->\n\n      <div class="job-item" (click)="startNewBpm(\'qjgl_jz\',\'gzqjgl\')" *ngIf="mi.rsgl_kjgb_qjgl_jz">\n\n        <img src="assets/imgs/leave_big.png" />\n\n        <p class="text">请假</p>\n\n      </div>\n\n\n\n      <!--科员非办公室科员-->\n\n      <div class="job-item" (click)="startNewBpm(\'ccgl\',\'gzccgl\')" *ngIf="mi.sa || mi.rsgl_kqgl_ccgl_ky">\n\n        <img src="assets/imgs/travel_big.png" />\n\n        <p class="text">出差</p>\n\n      </div>\n\n\n\n      <!--科长、办公室科员出差-->\n\n      <div class="job-item" (click)="startNewBpm(\'ccgl_kjgb\',\'gzccgl\')" *ngIf="mi.rsgl_kqgl_ccgl_kz">\n\n        <img src="assets/imgs/travel_big.png" />\n\n        <p class="text">出差</p>\n\n      </div>\n\n\n\n      <!--办公室主任-->\n\n      <div class="job-item" (click)="startNewBpm(\'ccgl_zrjbgb\',\'gzccgl\')" *ngIf="mi.rsgl_kqgl_ccgl_bgszr">\n\n        <img src="assets/imgs/travel_big.png" />\n\n        <p class="text">&nbsp;&nbsp;&nbsp;出差&nbsp;&nbsp;&nbsp;</p>\n\n      </div>\n\n\n\n      <!--副局出差-->\n\n      <div class="job-item" (click)="startNewBpm(\'ccgl_jjgb\',\'gzccgl\')" *ngIf="mi.rsgl_kqgl_ccgl_fj">\n\n        <img src="assets/imgs/travel_big.png" />\n\n        <p class="text">出差</p>\n\n      </div>\n\n\n\n       <!--局长出差-->\n\n      <div class="job-item" (click)="startNewBpm(\'ccgl_jz\',\'gzccgl\')" *ngIf="mi.rsgl_kqgl_ccgl_jz">\n\n        <img src="assets/imgs/travel_big.png" />\n\n        <p class="text">出差</p>\n\n      </div>\n\n\n\n    </div>\n\n    <div class="jobs-grid">\n\n      <div class="job-item" (click)="goSign()">\n\n        <img src="assets/imgs/qdjl.png" />\n\n        <p class="text">考勤记录</p>\n\n      </div>\n\n\n\n      <div class="job-item" (click)="goLeave()">\n\n        <img src="assets/imgs/qjjl.png" />\n\n        <p class="text">请假记录</p>\n\n      </div>\n\n\n\n      <div class="job-item" (click)="goTrip()">\n\n        <img src="assets/imgs/ccjl.png" />\n\n        <p class="text">出差记录</p>\n\n      </div>\n\n    </div>\n\n    <div class="jobs-grid">\n\n      <!--科员非办公室科员 -->\n\n      <div class="job-item" (click)="startNewBpm(\'clsysq\',\'gzclsq\')" *ngIf="mi.sa || mi.gdzc_clgl_clsy_ky">\n\n          <img src="assets/imgs/caruser.png" />\n\n          <p class="text">用车申请</p>\n\n        </div>\n\n  \n\n         <!--科长、办公室科员申请用车 -->\n\n         <div class="job-item" (click)="startNewBpm(\'clsysq_kjgb\',\'gzclsq\')" *ngIf=" mi.gdzc_clgl_clsy_kzbgsky">\n\n            <img src="assets/imgs/caruser.png" />\n\n            <p class="text">用车申请</p>\n\n          </div>\n\n  \n\n           <!--办公室主任申请用车 -->\n\n         <div class="job-item" (click)="startNewBpm(\'clsysq_zrjbgb\',\'gzclsq\')" *ngIf="mi.gdzc_clgl_clsy_bgszr">\n\n            <img src="assets/imgs/caruser.png" />\n\n            <p class="text">用车申请</p>\n\n          </div>\n\n  \n\n        <div class="job-item" (click)="goVehicleAnnal()" style="float: left;">\n\n          <img src="assets/imgs/carcording.png" />\n\n          <p class="text">用车记录</p>\n\n        </div>\n\n\n\n        <div class="job-item" style="float: left;">\n\n           \n\n          </div>\n\n    </div>\n\n    <div class="jobs-sub-title daily-jobs">\n\n      <span class="sub-title-icon"></span>\n\n      <p>日常办公</p>\n\n    </div>\n\n    <div class="jobs-grid">\n\n      <div class="job-item" (click)="toSwglNewPage()">\n\n        <img src="assets/imgs/income_big.png" />\n\n        <p class="text">收文</p>\n\n      </div>\n\n\n\n    <!--  <div class="job-item" (click)="getPositionFun()">\n\n        <img src="assets/imgs/income_big.png" />\n\n        <p class="text">定位</p>\n\n      </div>\n\n      <div class="job-item" (click)="goLocation()">\n\n        <img src="assets/imgs/income_big.png" />\n\n        <p class="text">高德定位</p>\n\n      </div>\n\n\n\n      <div class="job-item" (click)="testAnalysis()">\n\n        <img src="assets/imgs/income_big.png" />\n\n        <p class="text">位置</p>\n\n      </div>-->\n\n      <!-- <div class="job-item" (click)="startNewBpm(\'sfwgl_kszysj\',\'gzpublishfile\')" *ngIf="mi.sa || mi.xzgl_gwgl_fwgl_ksnbfw">\n\n        <img src="assets/imgs/income_big.png"/>\n\n        <p class="text">发文</p>\n\n      </div>\n\n      <div class="job-item" (click)="startNewBpm(\'sfwgl\',\'gzpublishfile\')" *ngIf="mi.sa || mi.xzgl_gwgl_fwgl_kykz">\n\n        <img src="assets/imgs/income_big.png"/>\n\n        <p class="text">发文</p>\n\n      </div>\n\n      <div class="job-item" (click)="startNewBpm(\'sfwgl_zrjbgb\',\'gzpublishfile\')" *ngIf="mi.sa || mi.xzgl_gwgl_fwgl_bgszrfj">\n\n        <img src="assets/imgs/income_big.png"/>\n\n        <p class="text">发文</p>\n\n      </div>\n\n      <div class="job-item" (click)="startNewBpm(\'sfwgl_jzgb\',\'gzpublishfile\')" *ngIf="mi.sa || mi.xzgl_gwgl_fwgl_jz">\n\n        <img src="assets/imgs/income_big.png"/>\n\n        <p class="text">发文</p>\n\n      </div>-->\n\n     \n\n    </div>\n\n    <!--<div class="jobs-sub-title daily-jobs">\n\n      <span class="sub-title-icon"></span>\n\n      <p>车辆管理</p>\n\n    </div>\n\n\n\n    <div class="jobs-grid">\n\n      \n\n    </div>-->\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\jobs\jobs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_15__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_18__app_buiness_SignHttp__["a" /* SignHttp */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], JobsPage);
    return JobsPage;
}());

//# sourceMappingURL=jobs.js.map

/***/ }),

/***/ 191:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TodoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__done_done__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__process_process__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_form_FormObject__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_ccgl_ccgl__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_rcap_rcap__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_swgl_swgl__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_qjgl_qjgl__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_wlkyxm_wlkyxm__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_ycsq_ycsq__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_yzgl_yzgl__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_zysq_zysq__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_wjry_wjry__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_kyxm_kyxm__ = __webpack_require__(108);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



















/**
 * Generated class for the TodoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TodoPage = /** @class */ (function () {
    function TodoPage(navCtrl, navParams, baseService, dataService, securityService, changeDetectorRef, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.baseService = baseService;
        this.dataService = dataService;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.pet = "待办事宜";
        this.datas = [];
        this.user = {};
        this.workInput = "";
        this.tableName = 'worklist';
        this.showDetails = false;
        //构建formObject对象方式修改，这种方式建立了独立的对象数据
        this.worklist = new __WEBPACK_IMPORTED_MODULE_4__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
        this.overworklist = new __WEBPACK_IMPORTED_MODULE_4__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
        this.gztaskview = new __WEBPACK_IMPORTED_MODULE_4__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
        this.gzreceiveview = new __WEBPACK_IMPORTED_MODULE_4__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
    }
    TodoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TodoPage');
    };
    TodoPage.prototype.buttonClick = function () {
        console.log('buttonClick');
    };
    TodoPage.prototype.toDonePage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__done_done__["a" /* DonePage */]);
    };
    ;
    TodoPage.prototype.toProcessPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__process_process__["a" /* ProcessPage */]);
    };
    ;
    TodoPage.prototype.searchWork = function () {
        //this.worklist.queryObject={"sbusiness:like":this.workInput?this.workInput:""};
        if (this.workInput == null || this.workInput == "") {
            this.worklist.queryObject = {};
        }
        else {
            this.worklist.queryObject["sbusiness:like"] = this.workInput;
        }
        this.worklist.pageIndex = 1; //重置查询条件的时候，需要重置页码
        this.worklist.datas = [];
        this.worklist.getEntitys(null);
    };
    TodoPage.prototype.toQueryWork = function (data) {
        var para = {};
        para["bk"] = data.actdefid;
        para["fn"] = data.prjforname;
        para["vi"] = data.prjpopname;
        para["dk"] = data.sbussinessid;
        //出差
        if (para["bk"] == "ccgl"
            || para["bk"] == "ccgl_jjgb"
            || para["bk"] == "ccgl_kjgb"
            || para["bk"] == "ccgl_zrjbgb") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_ccgl_ccgl__["a" /* CcglPage */], para);
        }
        //收发文
        if (para["bk"] == "sfwgl"
            || para["bk"] == "sfwgl_jzgb"
            || para["bk"] == "sfwgl_kszysj"
            || para["bk"] == "sfwgl_zrjbgb") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__pages_swgl_swgl__["a" /* SwglPage */], para);
        }
        //外来科研项目
        if (para["bk"] == "wlkyxmrqxk") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__pages_wlkyxm_wlkyxm__["a" /* WlkyxmPage */], para);
        }
        //用车申请
        if (para["bk"] == "clsysq"
            || para["bk"] == "clsysq_kjgb"
            || para["bk"] == "clsysq_zrjbgb") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_14__pages_ycsq_ycsq__["a" /* YcsqPage */], para);
        }
        //用章管理
        if (para["bk"] == "yzgl"
            || para["bk"] == "yzgl_zrjbgb") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_15__pages_yzgl_yzgl__["a" /* YzglPage */], para);
        }
        //资源申请
        if (para["bk"] == "zydd"
            || para["bk"] == "zydd") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_16__pages_zysq_zysq__["a" /* ZysqPage */], para);
        }
        //请假管理
        if (para["bk"] == "qjgl"
            || para["bk"] == "qjgl_jjgb"
            || para["bk"] == "qjgl_kjgb") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_12__pages_qjgl_qjgl__["a" /* QjglPage */], para);
        }
        //外籍人员入区许可办理
        if (para["bk"] == "wjryrqxk") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_17__pages_wjry_wjry__["a" /* WjryPage */], para);
        }
        //科研项目管理
        if (para["bk"] == "kyxmgl") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_18__pages_kyxm_kyxm__["a" /* KyxmPage */], para);
        }
    };
    ;
    TodoPage.prototype.toQueryReceive = function (data) {
        var para = {};
        para["bk"] = data.bpmkey;
        para["fn"] = "gzpublishfile";
        para["vi"] = "gzpublishfile";
        para["dk"] = data.fwid;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__pages_swgl_swgl__["a" /* SwglPage */], para);
    };
    ;
    TodoPage.prototype.toQuerySchedule = function (data) {
        var para = data;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_rcap_rcap__["a" /* RcapPage */], para);
    };
    ;
    //初始化页面
    TodoPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.user = {};
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                _this.initSuccessHomePage();
            }, function () {
                _this.user = {};
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.initSuccessHomePage();
        }
    };
    ;
    //跳转到登录界面
    TodoPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    TodoPage.prototype.editDatas = function (datas) {
        for (var item = 0; item < datas.length; item++) {
            datas[item].icon = 'ios-arrow-forward';
        }
    };
    ;
    /**
     * 成功加载用户home页面时处理
     */
    TodoPage.prototype.initSuccessHomePage = function () {
        this.user = this.securityService.user;
        //构建formObject对象方式修改，这种方式建立了独立的对象数据
        // if(this.worklist.sn==""){
        this.worklist = new __WEBPACK_IMPORTED_MODULE_4__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
        this.worklist.sn = "worklist";
        //}
        if (this.overworklist.sn == "") {
            this.overworklist = new __WEBPACK_IMPORTED_MODULE_4__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
            this.overworklist.sn = "overworklist";
        }
        if (this.gztaskview.sn == "") {
            this.gztaskview = new __WEBPACK_IMPORTED_MODULE_4__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
            this.gztaskview.sn = "gztaskview";
            this.gztaskview.queryObject = { "zt:!=": "rwzt_ywc" };
            this.gztaskview.orderObject = { "cjsj": 1 };
        }
        if (this.gzreceiveview.sn == "") {
            this.gzreceiveview = new __WEBPACK_IMPORTED_MODULE_4__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
            this.gzreceiveview.sn = "gzreceiveview";
            this.gzreceiveview.queryObject = { "sfqr": "0" };
            this.gzreceiveview.orderObject = { "jjdj": 1, "cjsj": 1 };
        }
        if (this.tableName == "worklist" && this.worklist.datas.length == 0) {
            this.worklist.getEntitys(null);
        }
        else if (this.tableName == "worklist" && this.overworklist.datas.length == 0) {
            this.overworklist.getEntitys(null);
        }
        else if (this.tableName == "gztaskview" && this.gztaskview.datas.length == 0) {
            this.gztaskview.getEntitys(this.editDatas);
        }
        else if (this.tableName == "gztaskview" && this.gzreceiveview.datas.length == 0) {
            this.gzreceiveview.getEntitys(this.editDatas);
        }
        //if(this.worklist.datas && this.worklist.datas.length>0){
        // }else{
        // this.worklist.getEntitys(null);
        // }
    };
    ;
    TodoPage.prototype.setTable = function (formName) {
        if (formName == "worklist") {
            if (this.worklist.datas && this.worklist.datas.length > 0) {
            }
            else {
                this.worklist.getEntitys(null);
            }
        }
        else if (formName == "overworklist") {
            if (this.overworklist.datas && this.overworklist.datas.length > 0) {
            }
            else {
                this.overworklist.getEntitys(null);
            }
        }
        else if (formName == "gztaskview") {
            if (this.gztaskview.datas && this.gztaskview.datas.length > 0) {
            }
            else {
                this.gztaskview.getEntitys(this.editDatas);
            }
        }
        else {
            if (this.gzreceiveview.datas && this.gzreceiveview.datas.length > 0) {
            }
            else {
                this.gzreceiveview.getEntitys(this.editDatas);
            }
        }
        this.tableName = formName;
    };
    //打开或关闭数据详细内容
    TodoPage.prototype.toggleDetails = function (data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-forward';
        }
        else {
            data.showDetails = true;
            data.icon = 'ios-arrow-down';
        }
    };
    //查询
    TodoPage.prototype.onSearchKeyUp = function (event) {
        if ("Enter" == event.key) {
            //if(this.user!=null && this.user["userid"]!=null){
            // this.formObject.pageIndex=0;
            // this.formObject.dataOver=false;
            // this.formObject.getEntitys();
        }
        //}else{
        //this.queryValue="";
        //}
    };
    TodoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-todo',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\todo\todo.html"*/'<!--\n\n  Generated template for the TodoPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <ion-navbar>\n\n    <ion-icon name="contact" style="font-size:2em;color: #FFF;margin-left:0.8rem;margin-top:0.3rem;"></ion-icon>\n\n    <span class="title-user">{{user.userName}}</span>\n\n    <ion-buttons end>\n\n      <button ion-button clear small (click)="toDonePage()">\n\n        <span class="right-button">已办列表</span>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <div class="search-bar-content">\n\n    <ion-searchbar placeholder="搜索" (ionInput)="searchWork()" [(ngModel)]="workInput">\n\n    </ion-searchbar>\n\n  </div>\n\n  <!--下拉事件时，刷新页面数据-->\n\n  <ion-refresher (ionRefresh)="worklist.doRefresh($event)">\n\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="下拉刷新" refreshingSpinner="circles" refreshingText="刷新中...">\n\n    </ion-refresher-content>\n\n  </ion-refresher>\n\n  <ion-list class="bhq-list">\n\n    <ion-item class="bhq-item" (click)="toQueryWork(item)" *ngFor="let item of worklist.datas">\n\n      <ion-thumbnail item-start>\n\n        <img src="assets/imgs/{{item.prjforname}}.png">\n\n      </ion-thumbnail>\n\n      <h2 class="bhq-item-title">{{item.sbusiness | asLength:12}}</h2>\n\n      <div class="bhq-item-content">\n\n        <div class="bhq-item-info">\n\n          <p>[{{item.actdefname}}] {{item.taskdefname}}</p>\n\n          <p>{{item.fromtime | asDate}} {{item.fromtime | asTime}}</p>\n\n        </div>\n\n      </div>\n\n    </ion-item>\n\n  </ion-list>\n\n  <!-- 无级滚动效果实现 -->\n\n  <ion-infinite-scroll (ionInfinite)="worklist.doInfinite($event)">\n\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n  </ion-infinite-scroll>\n\n</ion-content>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\todo\todo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_7__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_8__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_6__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], TodoPage);
    return TodoPage;
}());

//# sourceMappingURL=todo.js.map

/***/ }),

/***/ 202:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 202;

/***/ }),

/***/ 21:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BpmObject; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__form_FormObject__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(2);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * 添加用户导航器，方便其他的功能使用
 */

// 定义工作流的基础实现类，实现基础数据
var BpmObject = /** @class */ (function (_super) {
    __extends(BpmObject, _super);
    function BpmObject(dataService, baseService, changeDetectorRef, navCtrl, alert, loadingCtrl, sn) {
        var _this = _super.call(this, dataService, baseService, changeDetectorRef, loadingCtrl) || this;
        _this.dataService = dataService;
        _this.baseService = baseService;
        _this.changeDetectorRef = changeDetectorRef;
        _this.navCtrl = navCtrl;
        _this.alert = alert;
        _this.loadingCtrl = loadingCtrl;
        _this.sn = sn;
        /**
         * 是否使用页面
         */
        _this.usePage = false;
        /**
         * 当前用户信息
         */
        _this.user = {};
        //此处用来定义工作流里面的信息
        /**
         * 数据主键
         */
        _this.id = "";
        /**
         * 表单（单页面应用中的主页面）
         */
        _this.viewId = "";
        /**
         * 流程实例
         */
        _this.sexeid = "";
        /**
         * 当前环节id
         */
        _this.cTaskId = "";
        /**
         * 当前tab项
         */
        _this.ctab = "";
        /**
         * 当前环节名称
         */
        _this.cTaskName = "";
        /**
         * 流程授权
         */
        _this.auth = {};
        /**
         * 已执行结束的tab
         */
        _this.tabs = {};
        /**
         * 已执行结束的tab
         */
        _this.bpmKey = "";
        /**
         * 定义流程中的过程事项
         */
        _this.process = {};
        /**
         * 定义一下项目授权
         */
        _this.proauth = {};
        /**
         * 用户提交操作验证
         */
        _this.opttj = {};
        /**
         * 退回要素
         */
        _this.optback = {};
        /**
         * 办理过程
         */
        _this.historyLst = [];
        _this.loadImage = "";
        _this.sn = sn;
        return _this;
    }
    /**
     * 提取办理过程
     */
    BpmObject.prototype.loadBpmProcess = function () {
        var _this = this;
        var where = {};
        where["sexeid:="] = this.sexeid;
        var order = {};
        order["opttime"] = 0;
        var imagepath = this.baseService.baseUrl + "/processImage/loadImage?bpmkey=" + this.bpmKey;
        if (this.sexeid != null && this.sexeid != 'undefined' && this.sexeid != '') {
            imagepath = imagepath + "&sexeid=" + this.sexeid;
        }
        this.loadImage = imagepath + "&uuid=" + this.getGUID();
        this.dataService.getEntitys("bpmhistory", null, where, order, 10000, 1).subscribe(function (data) {
            if (data != null && data.length > 0) {
                for (var item = 0; item < data.length; item++) {
                    data[item].icon = 'ios-arrow-forward';
                }
            }
            if (data != null && data.length > 0) {
                _this.historyLst = data;
            }
            else {
                _this.historyLst = [];
            }
        });
    };
    ;
    /**
     * 查找数据服务
     */
    BpmObject.prototype.findWork = function (success, error) {
        var _this = this;
        /**
         * 暂时不处理用户信息
         */
        var user = {};
        var loading = this.loadingCtrl.create({
            content: '数据加载中，请稍后...' //数据加载中显示
        });
        loading.present();
        this.dataService.findWork(this.sn, this.id, this.bpmKey, user).subscribe(function (data) {
            //$state.go(data.viewId);
            setTimeout(function () {
                loading.dismiss();
            }, 300);
            if (data.entity != null) {
                _this.editRow = data.entity;
            }
            _this.bpmKey = data.bpmKey;
            _this.viewId = data.viewId;
            _this.sexeid = data.sexeid;
            _this.cTaskId = data.cTaskId;
            _this.ctab = data.ctab;
            _this.cTaskName = data.cTaskName;
            _this.id = data.id;
            if (data.auth != null) {
                _this.auth = data.auth;
            }
            else {
                _this.auth = {};
            }
            _this.tabs = {};
            if (data.tabs != null) {
                for (var k = 0; k < data.tabs.length; k++) {
                    //this.tabs[data.tabs[k]]=true;
                    var tabArr = data.tabs[k].split(",");
                    for (var i = 0; i < tabArr.length; i++) {
                        _this.tabs[tabArr[i]] = true;
                    }
                }
            }
            if (data.ctab != null) {
                var ctabArr = data.ctab.split(",");
                for (var k = 0; k < ctabArr.length; k++) {
                    _this.tabs[ctabArr[k]] = true;
                }
                //$rootScope.setTab(fName,ctabArr[0]);
                _this.toView(ctabArr[0]);
            }
            //构建审批历史子表信息
            _this.process = {};
            if (data.multiTask != null && data.multiTask.length > 0) {
                for (var k = 0; k < data.multiTask.length; k++) {
                    var item = data.multiTask[k];
                    _this.process[item.taskdefinedid] = item;
                }
            }
            if (data.universalTask != null && data.universalTask.length > 0) {
                for (var k = 0; k < data.universalTask.length; k++) {
                    var item = data.universalTask[k];
                    _this.process[item.taskdefinedid] = item;
                }
            }
            _this.proauth = {};
            if (data.prjshow != null && data.prjshow.length > 0) {
                var lst = data.prjshow.split(",");
                for (var k = 0; k < lst.length; k++) {
                    _this.proauth[lst[k]] = true;
                }
            }
            _this.changeDetectorRef.markForCheck();
            _this.changeDetectorRef.detectChanges();
            if (success) {
                success(_this);
            }
        });
    };
    ;
    /**
     *
     * @param tabId 切换工作流中的tab选项卡
     */
    BpmObject.prototype.setBpmTab = function (tabId) {
        if (this.tabs != null && this.tabs[tabId]) {
            //$rootScope.setTab(fName,tabId);
            //TODO 功能待实现
        }
    };
    ;
    /**
     * 流程提交之前的验证
     * @param {Object} fName
     */
    BpmObject.prototype.bpmValidate = function () {
        var findTask = false;
        if (this.opttj.optiontype == 'EndSign') {
            findTask = true;
        }
        for (var k = 0; k < this.opttj.nextTask.length; k++) {
            var ktask = this.opttj.nextTask[k];
            if (!ktask.taskSelect)
                continue;
            findTask = true;
            var unfind = true;
            for (var i = 0; i < ktask.user.length; i++) {
                if (ktask.user[i].userSelected) {
                    unfind = false;
                }
            }
            if (unfind) {
                // $.messager.alert("系统提示","节点"+ktask.taskName+"没有选择审批人");
                //TODO 提示功能按钮
                return false;
            }
        }
        if (!findTask) {
            // $.messager.alert("系统提示","请选择需要提交的下一个环节");
            //TODO 提示功能按钮
            return false;
        }
        return true;
    };
    ;
    /**
     * 保存草稿
     */
    BpmObject.prototype.bpmSave = function (success, error) {
        var _this = this;
        if (this.isOpting) {
            this.alert.create({
                title: "系统提示",
                subTitle: "数据正在处理中，请稍后！",
                buttons: ["确定"]
            }).present();
            return;
        }
        this.isOpting = true;
        var optsave = {};
        optsave["sexeid"] = this.sexeid;
        optsave["editHistory"] = [];
        optsave["bpmkey"] = this.bpmKey;
        //fo.optdisposal.id=fo.id;
        optsave["editHistory"] = this.createEditHistory();
        this.dataService.bpmSave(this.sn, optsave, this.editRow, this.user).subscribe(function (data) {
            _this.isOpting = false;
            if (data.status) {
                if (data.entity != null) {
                    data.entity[_this.isNew] = false;
                    _this.editRow = data.entity;
                }
                _this.changeDetectorRef.markForCheck();
                _this.changeDetectorRef.detectChanges();
                if (success) {
                    success(_this);
                }
                else {
                    _this.alert.create({
                        title: "系统提示",
                        subTitle: "保存成功！",
                        buttons: ["确定"]
                    }).present();
                }
            }
            else {
                if (error) {
                    error();
                }
                else {
                    _this.alert.create({
                        title: "系统提示",
                        subTitle: "保存失败，请重试！",
                        buttons: ["确定"]
                    }).present();
                }
            }
        });
    };
    ;
    BpmObject.prototype.showNextTaskSelect = function (success, error, data) {
        var _this = this;
        if (data.entity != null) {
            data.entity[this.isNew] = false;
            this.editRow = data.entity;
            delete data.entity;
        }
        this.opttj = {};
        if (data != null) {
            this.opttj = data;
        }
        else {
            this.opttj = {};
        }
        this.editRow.sexeid = data.sexeid;
        this.sexeid = data.sexeid;
        this.opttj.selectTask = false;
        if (data.optiontype == 'InclusiveSign' || data.optiontype == 'ExclusiveSign') {
            this.opttj.selectTask = true;
        }
        if (!this.usePage) {
            if (this.opttj.nextTask != null && this.opttj.nextTask.length > 0) {
                this.selectTjTask(this.opttj.nextTask[0]);
                var alert_1 = this.alert.create();
                alert_1.setTitle(this.opttj.nextTask[0].taskName);
                for (var k = 0; k < this.opttj.nextTask[0].user.length; k++) {
                    var iuser = this.opttj.nextTask[0].user[k];
                    alert_1.addInput({
                        type: 'checkbox',
                        label: iuser.userName,
                        value: iuser.userId,
                        checked: true
                    });
                }
                alert_1.addButton('取消');
                alert_1.addButton({
                    text: '确定',
                    handler: function (data) {
                        console.log('Checkbox data:', data);
                        //this.testCheckboxOpen = false;
                        // this.testCheckboxResult = data;
                        if (data != null && data.length > 0) {
                            for (var j = 0; j < _this.opttj.nextTask[0].user.length; j++) {
                                _this.opttj.nextTask[0].user[j].userSelected = false;
                                for (var i = 0; i < data.length; i++) {
                                    if (_this.opttj.nextTask[0].user[j].userId == data[i]) {
                                        _this.opttj.nextTask[0].user[j].userSelected = true;
                                    }
                                }
                            }
                            if (!_this.bpmValidate()) {
                                /*let tempAlert= this.alert.create({
                                    title: '系统提示!',
                                    subTitle: '请先选择审批人!',
                                    buttons: ['确定']
                                });
                                tempAlert.present();*/
                                var confirm_1 = _this.alert.create({
                                    title: '系统提示',
                                    message: "请先选择审批人！",
                                    buttons: [
                                        {
                                            text: '确定',
                                            handler: function () {
                                                // console.log('Disagree clicked');
                                                _this.showNextTaskSelect(success, error, data);
                                            }
                                        }
                                    ]
                                });
                                confirm_1.present();
                            }
                            else {
                                _this.signTask(null, null);
                            }
                        }
                        else {
                            /*let tempAlert= this.alert.create({
                                title: '系统提示!',
                                subTitle: '请先选择审批人!',
                                buttons: ['确定']
                            });
                            tempAlert.present();*/
                            var confirm_2 = _this.alert.create({
                                title: '系统提示',
                                message: "请先选择审批人！",
                                buttons: [
                                    {
                                        text: '确定',
                                        handler: function () {
                                            // console.log('Disagree clicked');
                                            //alert.present();
                                            _this.showNextTaskSelect(success, error, data);
                                        }
                                    }
                                ]
                            });
                            confirm_2.present();
                        }
                    }
                });
                alert_1.present();
            }
            else {
                var confirm_3 = this.alert.create({
                    title: '系统提示?',
                    message: '确定提交?',
                    buttons: [
                        {
                            text: '取消',
                            handler: function () {
                                console.log('Disagree clicked');
                            }
                        },
                        {
                            text: '确定',
                            handler: function () {
                                //console.log('Agree clicked');
                                _this.signTask(null, null);
                            }
                        }
                    ]
                });
                confirm_3.present();
            }
        }
        this.changeDetectorRef.markForCheck();
        this.changeDetectorRef.detectChanges();
        if (success) {
            success(this);
        }
    };
    /**
     * 获取项目的下一个环节
     */
    BpmObject.prototype.getNextTask = function (success, error) {
        var _this = this;
        if (this.isOpting) {
            this.alert.create({
                title: "系统提示",
                subTitle: "数据正在处理中，请稍后！",
                buttons: ["确定"]
            }).present();
            return;
        }
        this.isOpting = true;
        var editHistory = [];
        editHistory = this.createEditHistory();
        this.dataService.getNextTask(this.sn, this.bpmKey, this.editRow, editHistory, this.user).subscribe(function (data) {
            _this.isOpting = false;
            _this.showNextTaskSelect(success, error, data);
        });
    };
    ;
    /**
     * 项目提交通过到下一个环节
     */
    BpmObject.prototype.signTask = function (success, error) {
        var _this = this;
        if (this.isOpting) {
            this.alert.create({
                title: "系统提示",
                subTitle: "数据正在处理中，请稍后！",
                buttons: ["确定"]
            }).present();
            return;
        }
        this.isOpting = true;
        this.opttj.editHistory = [];
        this.opttj.editHistory = this.createEditHistory();
        this.dataService.signTask(this.sn, this.opttj, this.editRow, this.user).subscribe(function (data) {
            _this.isOpting = false;
            if (data.status) {
                if (data.entity != null) {
                    data.entity[_this.isNew] = false;
                    _this.editRow = data.entity;
                    // delete data.entity;
                }
                _this.changeDetectorRef.markForCheck();
                _this.changeDetectorRef.detectChanges();
                if (success) {
                    success(_this);
                }
                _this.goHome();
            }
            else {
                if (error) {
                    error();
                }
                else {
                    _this.alert.create({
                        title: "系统提示",
                        subTitle: "提交失败，请重试！",
                        buttons: ["确定"]
                    }).present();
                }
            }
        });
    };
    /**
     * 选择提交节点
     * @param iTask 选择提交节点
     */
    BpmObject.prototype.selectTjTask = function (iTask) {
        var opttj = this.opttj;
        var selected = iTask.taskSelect;
        //平行选择节点
        if (opttj.optiontype == 'InclusiveSign') {
            for (var k = 0; k < iTask.user.length; k++) {
                if (selected) {
                    iTask.user[k].userSelected = true;
                }
                else {
                    iTask.user[k].userSelected = false;
                }
            }
        }
        else if (opttj.optiontype == 'ExclusiveSign') {
            if (selected) {
                for (var j = 0; j < opttj.nextTask.length; j++) {
                    opttj.nextTask[j].taskSelect = false;
                    for (var k = 0; k < opttj.nextTask[j].user.length; k++) {
                        opttj.nextTask[j].user[k].userSelected = false;
                    }
                }
                iTask.taskSelect = true;
                for (var k = 0; k < iTask.user.length; k++) {
                    iTask.user[k].userSelected = true;
                }
            }
            else {
                //强制必须选择一个
                iTask.taskSelect = true;
            }
        }
    };
    ;
    /**
     * 选择退回节点
     * @param iTask
     */
    BpmObject.prototype.selectBackTask = function (iTask) {
        var optback = this.optback;
        var selected = iTask.taskSelect;
        if (optback.optiontype == 'ExclusiveSign') {
            if (selected) {
                for (var j = 0; j < optback.nextTask.length; j++) {
                    optback.nextTask[j].taskSelect = false;
                }
                iTask.taskSelect = true;
            }
            else {
                //强制必须选择一个
                iTask.taskSelect = true;
            }
        }
    };
    ;
    /**
     * 提交通过时选择用户问题
     * @param iTask
     * @param user
     */
    BpmObject.prototype.selectTjUser = function (iTask, user) {
        var selected = user.userSelected;
        var opttj = this.opttj;
        if (selected) {
            iTask.taskSelect = true;
            if (opttj.optiontype == 'ExclusiveSign') {
                for (var j = 0; j < opttj.nextTask.length; j++) {
                    if (iTask.taskdefinedid != opttj.nextTask[j].taskdefinedid) {
                        opttj.nextTask[j].taskSelect = false;
                        for (var k = 0; k < opttj.nextTask[j].user.length; k++) {
                            opttj.nextTask[j].user[k].userSelected = false;
                        }
                    }
                }
            }
        }
        else {
            if (opttj.optiontype == 'InclusiveSign') {
                var finduser = false;
                for (var k = 0; k < iTask.user.length; k++) {
                    if (iTask.user[k].userSelected) {
                        finduser = true;
                    }
                }
                if (!finduser) {
                    iTask.taskSelect = false;
                }
            }
        }
    };
    ;
    /**
     * 返回主页面功能
     */
    BpmObject.prototype.goHome = function () {
        // this.navCtrl.push(TabsPage);
        this.navCtrl.pop();
    };
    ;
    /**
     * 定位到指定页面点
     * @param id
     */
    BpmObject.prototype.toView = function (id) {
        setTimeout(function () {
            var element = document.getElementById(id);
            if (element) {
                element.scrollIntoView();
            }
        }, 300);
    };
    ;
    /**
     * 获取项目的退回环节
     */
    BpmObject.prototype.getBackTask = function (success, error) {
        var _this = this;
        if (this.isOpting) {
            this.alert.create({
                title: "系统提示",
                subTitle: "数据正在处理中，请稍后！",
                buttons: ["确定"]
            }).present();
            return;
        }
        this.isOpting = true;
        var editHistory = this.createEditHistory();
        this.dataService.getBackTask(this.sn, this.bpmKey, this.editRow, editHistory, this.user).subscribe(function (data) {
            _this.isOpting = false;
            if (data.entity != null) {
                data.entity[_this.isNew] = false;
                _this.editRow = data.entity;
                delete data.entity;
            }
            _this.optback = {};
            _this.editRow.sexeid = data.sexeid;
            _this.sexeid = data.sexeid;
            if (data.nextTask == null || data.nextTask.length < 1) {
                _this.alert.create({
                    title: "系统提示",
                    message: "没有退回节点!",
                    buttons: [{
                            text: "确定",
                            role: "确定",
                            handler: function () {
                                if (_this.usePage) {
                                    _this.navCtrl.pop();
                                }
                                else {
                                    return;
                                }
                            }
                        }]
                }).present();
                return;
            }
            if (data != null) {
                _this.optback = data;
            }
            else {
                _this.optback = {};
            }
            if (data.optiontype == 'ExclusiveBack') {
                _this.optback.selectTask = true;
            }
            else {
                _this.optback.selectTask = false;
            }
            if (!_this.usePage) {
                if (_this.optback.nextTask != null && _this.optback.nextTask.length > 0) {
                    //this.selectBackTask(this.optback.nextTask[0]);
                    if (_this.optback.nextTask.length == 1) {
                        var confirm_4 = _this.alert.create({
                            title: '系统提示?',
                            message: '确定退回' + _this.optback.nextTask[0].taskName + '?',
                            buttons: [
                                {
                                    text: '取消',
                                    handler: function () {
                                        console.log('Disagree clicked');
                                    }
                                },
                                {
                                    text: '确定',
                                    handler: function () {
                                        _this.sendBack(null, null);
                                    }
                                }
                            ]
                        });
                        confirm_4.present();
                    }
                    else {
                        var alert_2 = _this.alert.create();
                        alert_2.setTitle("退回");
                        for (var k = 0; k < _this.optback.nextTask.length; k++) {
                            var task = _this.optback.nextTask[k];
                            if (k < 1) {
                                alert_2.addInput({
                                    type: 'radio',
                                    label: task.taskName,
                                    value: task.taskdefinedid,
                                    checked: true
                                });
                            }
                            else {
                                alert_2.addInput({
                                    type: 'radio',
                                    label: task.taskName,
                                    value: task.taskdefinedid
                                });
                            }
                        }
                        alert_2.addButton('取消');
                        alert_2.addButton({
                            text: '确定',
                            handler: function (data) {
                                console.log('radio data:', data);
                                if (data != null && data.length > 0) {
                                    for (var j = 0; j < _this.optback.nextTask.length; j++) {
                                        _this.optback.nextTask[j].taskSelect = false;
                                        for (var i = 0; i < data.length; i++) {
                                            if (_this.optback.nextTask[j].taskdefinedid == data[i]) {
                                                //this.opttj.nextTask[j].taskSelect=true;
                                                _this.selectBackTask(_this.optback.nextTask[j]);
                                            }
                                        }
                                    }
                                    _this.sendBack(null, null);
                                }
                                else {
                                    var tempAlert = _this.alert.create({
                                        title: '系统提示!',
                                        subTitle: '请先退回环节!',
                                        buttons: ['确定']
                                    });
                                    tempAlert.present();
                                }
                            }
                        });
                        alert_2.present();
                    }
                }
                else {
                    var tempAlert = _this.alert.create({
                        title: '系统提示!',
                        subTitle: '无可退回环节!',
                        buttons: ['确定']
                    });
                    tempAlert.present();
                }
            }
            _this.changeDetectorRef.markForCheck();
            _this.changeDetectorRef.detectChanges();
            if (success) {
                success(_this);
            }
        });
    };
    ;
    /**
     * 构建当前编辑历史
     */
    BpmObject.prototype.createEditHistory = function () {
        var editHistory = [];
        if (this.process != null) {
            for (var ikey in this.process) {
                var iprocess = this.process[ikey];
                if (iprocess != null && iprocess.singleTask != null
                    && iprocess.singleTask.length > 0) {
                    for (var j = 0; j < iprocess.singleTask.length; j++) {
                        var jitem = iprocess.singleTask[j];
                        if (jitem != null && jitem.editRow != null && jitem.userEdit) {
                            editHistory.push(jitem.editRow);
                        }
                    }
                }
                else if (iprocess != null && iprocess.editRow != null && iprocess.userEdit) {
                    editHistory.push(iprocess.editRow);
                }
            }
        }
        return editHistory;
    };
    /**
    * 项目提交通过到下一个环节
    */
    BpmObject.prototype.sendBack = function (success, error) {
        var _this = this;
        if (this.isOpting) {
            this.alert.create({
                title: "系统提示",
                subTitle: "数据正在处理中，请稍后！",
                buttons: ["确定"]
            }).present();
            return;
        }
        this.isOpting = true;
        this.optback.editHistory = [];
        this.optback.editHistory = this.createEditHistory();
        this.dataService.sendBack(this.sn, this.optback, this.editRow, this.user).subscribe(function (data) {
            _this.isOpting = false;
            if (data.status) {
                if (data.entity != null) {
                    data.entity[_this.isNew] = false;
                    _this.editRow = data.entity;
                    // delete data.entity;
                }
                _this.changeDetectorRef.markForCheck();
                _this.changeDetectorRef.detectChanges();
                if (success) {
                    success(_this);
                }
                _this.goHome();
            }
            else {
                if (error) {
                    error();
                }
                else {
                    _this.alert.create({
                        title: "系统提示",
                        subTitle: "退回失败，请重试！",
                        buttons: ["确定"]
                    }).present();
                }
            }
        });
    };
    ;
    BpmObject = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_1__core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["LoadingController"], String])
    ], BpmObject);
    return BpmObject;
}(__WEBPACK_IMPORTED_MODULE_3__form_FormObject__["a" /* FormObject */]));

//# sourceMappingURL=BpmObject.js.map

/***/ }),

/***/ 24:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormObject; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// 基础服务类，提供基础路径
var FormObject = /** @class */ (function () {
    function FormObject(dataService, baseService, changeDetectorRef, loadingCtrl) {
        this.dataService = dataService;
        this.baseService = baseService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        // 服务名称，如果为空，则默认为表单名称
        this.sn = "";
        // 查询服务名称，如果为空，则默认为服务名称
        this.ssn = "";
        // 编辑服务名称，如果为空，则默认为服务名称
        this.esn = "";
        /**
         * 表单的主健，只能为一个，一般为GUID字段
         */
        this.keyField = '';
        /**
         * 表单需要随机赋值的字段
         */
        this.randomFields = [];
        /**
         * 查询字段
         */
        this.queryFields = '';
        /**
         * 新值时的默认值
         */
        this.defaultObject = {};
        /**
         * 默认的查询值
         */
        this.defaultQueryObject = {};
        /**
         * 查询变量
         */
        this.queryObject = {};
        /**
        * 排序对象
        */
        this.orderObject = {};
        /**
         * 当前数据列表
         */
        this.datas = [];
        /**
         * 当前数据列表选择的行
         */
        this.sRows = [];
        /**
         * 分页大小
         */
        this.pageSize = 10;
        /**
         * 分页索引
         */
        this.pageIndex = 1;
        /**
         * 是否计算列表查询下的总记录数
         */
        this.computeRowCounts = true;
        /**
         * 页数
         */
        this.pageCount = 1;
        /**
         * 分页对象列表(1,2,3)
         */
        this.page = [1];
        /**
         * 总的条数
         */
        this.rowsCount = 0;
        /**
         * 当前编辑对象
         */
        this.editRow = {};
        /**
         * 当前编辑对象的复制对象，用于解决编辑中的结果处理
         */
        this.curOldRow = {};
        /**
         * 是否将分页数据滚动添加，默认为true
         */
        this.addResult = true;
        // 当前表单在视图切换的时候，是否会被注销
        this.canDestroy = true;
        this.dataOver = false;
        /**
         * 数据正在操作提示符
         */
        this.isOpting = false;
        /**
         * 是否是新的记录
         */
        this.isNew = "FIELD_ISNEW";
    }
    FormObject.prototype.getEntitys = function (success) {
        var _this = this;
        if (!this.addResult) {
            this.datas = [];
        }
        //let loading = this.loadingCtrl.create({
        //content: '数据加载中，请稍后...'//数据加载中显示
        //});
        //loading.present();
        this.dataService.getEntitys(this.sn, null, this.queryObject, this.orderObject, this.pageSize, this.pageIndex).subscribe(function (data) {
            // setTimeout(function(){
            // loading.dismiss();
            // },100);
            if (data != null && data.length > 0) {
                for (var item = 0; item < data.length; item++) {
                    var val = data[item];
                    _this.datas.push(val);
                }
                _this.pageIndex = _this.pageIndex + 1;
                // this.editDatas(this.datas);
                _this.changeDetectorRef.markForCheck();
                _this.changeDetectorRef.detectChanges();
                if (success) {
                    success(data);
                }
            }
            else {
                _this.dataOver = true;
                if (success) {
                    success(data);
                }
            }
        });
    };
    ;
    /**
     * 滚动动态加载数据
     * @param infiniteScroll
     */
    FormObject.prototype.doInfinite = function (infiniteScroll, success) {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            if (!_this.dataOver) {
                //this.addResult=true;
                _this.getEntitys(success);
            }
            infiniteScroll.complete();
        }, 500);
    };
    ;
    /**
     * 上拉刷新数据
     * @param infiniteScroll
     */
    FormObject.prototype.doRefresh = function (infiniteScroll, success) {
        var _this = this;
        console.log('Async operation has ended');
        setTimeout(function () {
            if (!_this.dataOver) {
                //this.addResult=true;
                _this.datas = [];
                _this.pageIndex = 1;
                _this.getEntitys(success);
            }
            infiniteScroll.complete();
        }, 500);
    };
    ;
    /**
     * 日期格式处理
     * @param date
     */
    FormObject.prototype.formatDate = function (date) {
        var y = date.getFullYear();
        var m = date.getMonth() + 1;
        m = m < 10 ? '0' + m : m;
        var d = date.getDate();
        d = d < 10 ? ('0' + d) : d;
        return y + '-' + m + '-' + d;
    };
    ;
    /**
     * 到秒日期格式处理
     * @param date
     */
    FormObject.prototype.formatDateToSecond = function (date) {
        var y = date.getFullYear();
        var m = date.getMonth() + 1;
        m = m < 10 ? '0' + m : m;
        var d = date.getDate();
        d = d < 10 ? ('0' + d) : d;
        var h = date.getHours();
        h = h < 10 ? '0' + h : h;
        var mi = date.getMinutes();
        mi = mi < 10 ? '0' + mi : mi;
        var s = date.getSeconds();
        s = s < 10 ? '0' + s : s;
        return y + '-' + m + '-' + d + ' ' + h + ':' + mi + ':' + s;
    };
    ;
    /**
     * 获取GUID值
     */
    FormObject.prototype.getGUID = function () {
        var a = function (a) {
            return 0 > a ? NaN : 30 >= a ? 0 | Math.random() * (1 << a) : 53 >= a ? (0 | 1073741824 * Math.random()) + 1073741824 * (0 | Math.random() * (1 << a - 30)) : NaN;
        };
        var b = function (a, b) {
            for (var c = a.toString(16), d = b - c.length, e = "0"; 0 < d; d >>>= 1, e += e) {
                d & 1 && (c = e + c);
            }
            return c;
        };
        var guidString = b(a(32), 8) + b(a(16), 4) + b(16384 | a(12), 4) + b(32768 | a(14), 4) + b(a(48), 12);
        return guidString;
    };
    ;
    FormObject = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_1__core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["LoadingController"]])
    ], FormObject);
    return FormObject;
}());

//# sourceMappingURL=FormObject.js.map

/***/ }),

/***/ 245:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/approval/approval.module": [
		754,
		11
	],
	"../pages/bpmprocess/bpmprocess.module": [
		755,
		10
	],
	"../pages/calendar/calendar.module": [
		756,
		9
	],
	"../pages/ccgl/ccgl.module": [
		757,
		8
	],
	"../pages/done/done.module": [
		758,
		7
	],
	"../pages/jobs/jobs.module": [
		764,
		6
	],
	"../pages/login/login.module": [
		759,
		5
	],
	"../pages/me/me.module": [
		760,
		4
	],
	"../pages/process/process.module": [
		761,
		3
	],
	"../pages/setting/setting.module": [
		762,
		2
	],
	"../pages/task/task.module": [
		763,
		1
	],
	"../pages/todo/todo.module": [
		765,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 245;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 27:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BpmImgPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var BpmImgPage = /** @class */ (function () {
    function BpmImgPage(navCtrl, navParams, dataService, baseSErvice, alert, securityService, changeDetectorRef, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseSErvice = baseSErvice;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.user = {};
        /**
         * 定义一个工作流对象
         */
        this.bpmObject = new __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__["a" /* BpmObject */](this.dataService, this.baseSErvice, this.changeDetectorRef, this.navCtrl, this.alert, this.loadingCtrl, "");
        this.bpmObject = this.navParams.data;
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                _this.bpmObject.loadBpmProcess();
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.bpmObject.loadBpmProcess();
        }
    }
    BpmImgPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    BpmImgPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    BpmImgPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\bpmimg\bpmimg.html"*/'\n\n  <ion-header>\n\n\n\n    <ion-navbar>\n\n      <ion-title>办理过程</ion-title>\n\n    </ion-navbar>\n\n  \n\n  </ion-header>\n\n\n\n  <ion-content class="card-answerin-page" >\n\n    <div class="zs-course-info" >\n\n        <div class="content has-header ionic-pseudo">\n\n            <img src="{{bpmObject.loadImage}}" imageViewer="{{bpmObject.loadImage}}" />\n\n        </div>\n\n    </div>\n\n</ion-content>\n\n\n\n  \n\n'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\bpmimg\bpmimg.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], BpmImgPage);
    return BpmImgPage;
}());

//# sourceMappingURL=bpmimg.js.map

/***/ }),

/***/ 28:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BpmProcessPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_bpmimg_bpmimg__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var BpmProcessPage = /** @class */ (function () {
    function BpmProcessPage(navCtrl, navParams, dataService, baseSErvice, alert, securityService, changeDetectorRef, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseSErvice = baseSErvice;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.user = {};
        /**
         * 定义一个工作流对象
         */
        this.bpmObject = new __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__["a" /* BpmObject */](this.dataService, this.baseSErvice, this.changeDetectorRef, this.navCtrl, this.alert, this.loadingCtrl, "");
        this.bpmObject = this.navParams.data;
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                _this.bpmObject.loadBpmProcess();
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.bpmObject.loadBpmProcess();
        }
    }
    BpmProcessPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    BpmProcessPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    //打开或关闭数据详细内容
    BpmProcessPage.prototype.toggleDetails = function (data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-forward';
        }
        else {
            data.showDetails = true;
            data.icon = 'ios-arrow-down';
        }
    };
    ;
    /**
     * 加载图片
     */
    BpmProcessPage.prototype.loadProcessImage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_bpmimg_bpmimg__["a" /* BpmImgPage */], this.bpmObject);
    };
    BpmProcessPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-bpmprocess',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\bpmprocess\bpmprocess.html"*/'<!--\n\n  Generated template for the ProcessPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n      <ion-title>办理过程</ion-title>\n\n    </ion-navbar>\n\n  \n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content padding>\n\n    <ion-list class="bhq-list">\n\n      <ion-item class="bhq-item" *ngFor="let item of bpmObject.historyLst;let i=index" [attr.data-index]="i" >\n\n        <div class="bhq-item-title"><span class="num">{{i+1}}</span>{{item.taskdefname}}</div>\n\n        <div class="bhq-item-content">\n\n          <div class="bhq-item-left">\n\n            <p>业务来源：{{item.fromassigneesname}}</p>\n\n            <p>接收时间：{{item.fromtime | asDateExtend}}</p>\n\n            <p>办理时间：{{item.opttime | asDateExtend}}</p>\n\n          </div>\n\n          <div class="bhq-item-right">\n\n            <p>办理人员：{{item.optassigneesname}}</p>\n\n            <p>办理部门： {{item.optorgname}}</p>\n\n            <p>办理结果：{{item.optresult}}</p>\n\n          </div>\n\n        </div>\n\n      </ion-item>\n\n    </ion-list>\n\n  </ion-content>\n\n  '/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\bpmprocess\bpmprocess.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], BpmProcessPage);
    return BpmProcessPage;
}());

//# sourceMappingURL=bpmprocess.js.map

/***/ }),

/***/ 30:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BpmBackPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var BpmBackPage = /** @class */ (function () {
    function BpmBackPage(navCtrl, navParams, dataService, baseSErvice, alert, securityService, changeDetectorRef, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseSErvice = baseSErvice;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.user = {};
        /**
         * 定义一个工作流对象
         */
        this.bpmObject = new __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__["a" /* BpmObject */](this.dataService, this.baseSErvice, this.changeDetectorRef, this.navCtrl, this.alert, this.loadingCtrl, "");
        this.bpmObject = this.navParams.data;
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                _this.bpmObject.getBackTask(null, null);
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.bpmObject.getBackTask(null, null);
        }
    }
    BpmBackPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    BpmBackPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    BpmBackPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\bpmback\bpmback.html"*/'<ion-content class="card-answerin-page" >\n\n    <div class="zs-page-header"> \n\n        <a (click)="backPage();" class="back"></a> \n\n        退回\n\n    </div>\n\n\n\n    <div class="zs-course-info" >\n\n\n\n        <div class="content has-header ionic-pseudo">\n\n\n\n            <div class="list list-inset">\n\n      \n\n              <div class="item" *ngFor="let task of  bpmObject.optback.nextTask">\n\n                  <ion-checkbox [(ngModel)]="task.taskSelect" *ngIf="bpmObject.optback.selectTask"\n\n                  (click)="bpmObject.selectBackTask(task);"></ion-checkbox>\n\n                    {{task.taskName}}\n\n              </div>\n\n          </div>\n\n        </div>\n\n    </div>\n\n\n\n    <div class="zs-page-foot"> \n\n        <a (click)="bpmObject.sendBack(null,null);">退回</a>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\bpmback\bpmback.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], BpmBackPage);
    return BpmBackPage;
}());

//# sourceMappingURL=bpmback.js.map

/***/ }),

/***/ 31:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BpmSignPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var BpmSignPage = /** @class */ (function () {
    function BpmSignPage(navCtrl, navParams, dataService, baseSErvice, alert, securityService, changeDetectorRef, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseSErvice = baseSErvice;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        /**
         * 用户
         */
        this.user = {};
        this.bpmObject = new __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__["a" /* BpmObject */](this.dataService, this.baseSErvice, this.changeDetectorRef, this.navCtrl, this.alert, this.loadingCtrl, "");
        this.bpmObject = this.navParams.data;
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                _this.bpmObject.getNextTask(null, null);
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.bpmObject.getNextTask(null, null);
        }
    }
    BpmSignPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    BpmSignPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    BpmSignPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\bpmsign\bpmsign.html"*/'<ion-content class="card-answerin-page" >\n\n        <div class="zs-page-header"> \n\n            <a (click)="backPage();" class="back"></a> \n\n            提交\n\n        </div>\n\n    \n\n        <div class="zs-course-info" >\n\n\n\n            <div class="content has-header ionic-pseudo">\n\n\n\n                <div class="list list-inset">\n\n          \n\n                  <div class="item" *ngFor="let task of  bpmObject.opttj.nextTask">\n\n                      <ion-checkbox [(ngModel)]="task.taskSelect" *ngIf="bpmObject.opttj.selectTask"\n\n                      (click)="bpmObject.selectTjTask(task);"></ion-checkbox>\n\n                      \n\n                    \n\n                        {{task.taskName}}\n\n\n\n                      <div class="content has-header ionic-pseudo" style="margin-left: 20px;">\n\n\n\n                          <div class="list list-inset">\n\n                    \n\n                              <div class="item" *ngFor="let iuser of task.user">\n\n                                  <ion-checkbox [(ngModel)]="iuser.userSelected" (click)="bpmObject.selectTjUser(task,iuser);"\n\n                                  ></ion-checkbox>\n\n                                  \n\n                                 \n\n                                    {{iuser.userName}}\n\n                          </div>\n\n                        </div>\n\n                      </div>\n\n                  </div>\n\n              </div>\n\n            </div>\n\n        </div>\n\n\n\n        <div class="zs-page-foot"> \n\n            <a (click)="bpmObject.bpmValidate() && bpmObject.signTask(null,null);">提交</a>\n\n        </div>\n\n    </ion-content>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\bpmsign\bpmsign.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], BpmSignPage);
    return BpmSignPage;
}());

//# sourceMappingURL=bpmsign.js.map

/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SecurityDataService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_cookie__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_cookie___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ngx_cookie__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// 安全模块数据服务
var SecurityDataService = /** @class */ (function () {
    function SecurityDataService(http, baseService, _cookieService) {
        this.http = http;
        this.baseService = baseService;
        this._cookieService = _cookieService;
    }
    // 创建tgt
    SecurityDataService.prototype.loginAjax = function (username, password) {
        var url = this.baseService.baseUrl + '/v1/tickets?username=' + username + '&password=' + password;
        return this.http.post(url, '', { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    // 验证tgt
    SecurityDataService.prototype.ticketsValidateAjax = function (tgt) {
        var url = this.baseService.baseUrl + '/ticketsValidate/' + tgt;
        return this.http.post(url, '', { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    // 创建ST
    SecurityDataService.prototype.serviceTicketsAjax = function (tgt, service) {
        var url = this.baseService.baseUrl + '/v1/serviceTickets/' + tgt + '?service=' + service;
        return this.http.post(url, '', { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe();
    };
    // 删除TGT
    SecurityDataService.prototype.deleteTicketsAjax = function (tgt) {
        var url = this.baseService.baseUrl + '/v1/tickets/' + tgt;
        return this.http.delete(url, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    //TODO  模拟登录，解决服务端登录问题
    SecurityDataService.prototype.mklogin = function (ticket) {
        var url = this.baseService.hostUrl + '/sso/mklogin?ticket=' + ticket;
        return this.http.get(url, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    // 退出登录
    SecurityDataService.prototype.loginOut = function () {
        var url = this.baseService.hostUrl + '/sso/logout?ticket=' + this._cookieService.get("jwt");
        return this.http.get(url, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    // 用户注册信息
    SecurityDataService.prototype.userRegister = function (ac, pw, rpw) {
        var url = this.baseService.baseUrl + '/power/userRegister/' + ac + '/' + pw + '/' + rpw;
        return this.http.post(url, '', { headers: this.baseService.httpOptions() })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    // 获取当前用户信息
    SecurityDataService.prototype.getUserInfo = function () {
        var url = this.baseService.baseUrl + '/currentuser/getUserInfo';
        return this.http.post(url, '', { headers: this.baseService.httpOptions(),
            withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    // 设置密码
    SecurityDataService.prototype.setPassword = function (op, np) {
        var url = this.baseService.baseUrl + '/currentuser/setPassword/' + op + '/' + np;
        return this.http.post(url, '', { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    SecurityDataService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_4_ngx_cookie__["CookieService"]])
    ], SecurityDataService);
    return SecurityDataService;
}());

//# sourceMappingURL=SecurityDataService.js.map

/***/ }),

/***/ 347:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\about\about.html"*/'<!-- <ion-header>\n\n	<ion-toolbar style="min-height: 20px;">\n\n		<button ion-button clear small navPop style="padding: 0;">\n\n			<i class="icon ion-chevron-left"></i>\n\n			<span style="font-size: 1.6rem;color:black;">返回</span>\n\n		</button>\n\n	</ion-toolbar>  \n\n</ion-header> -->\n\n<ion-content padding>\n\n	<div class="zs-page-header">\n\n		<a (click)=\'navCtrl.pop()\' class="back"></a>\n\n		关于我们\n\n	</div>\n\n	<div class="zs-content" style="margin-top: 30px;">\n\n		<div class="zs-spilt" style="height: 10px;"></div>\n\n		<div class="zs-detail-content about">\n\n			<p>关于我们</p>\n\n		</div>\n\n	</div>\n\n</ion-content>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\about\about.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 348:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HelpPage = /** @class */ (function () {
    function HelpPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    HelpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\help\help.html"*/'<!-- <ion-header>\n\n	<ion-toolbar style="min-height: 20px;">\n\n		<button ion-button clear small navPop style="padding: 0;">\n\n			<i class="icon ion-chevron-left"></i>\n\n			<span style="font-size: 1.6rem;color:black;">返回</span>\n\n		</button>\n\n	</ion-toolbar>  \n\n</ion-header> -->\n\n<ion-content padding>\n\n	<div class="zs-page-header">\n\n		<a (click)=\'navCtrl.pop()\' class="back"></a>\n\n		系统帮助\n\n	</div>\n\n	<div class="zs-content" style="margin-top: 30px;">\n\n		<div class="zs-spilt" style="height: 10px;"></div>\n\n		<div class="zs-collapse">\n\n			<div class="item">\n\n				<div class="header">\n\n					<i class="icon"></i>\n\n					<span class="txt">如何使用此款APP?</span>\n\n					<i class="arr"></i>\n\n				</div>\n\n				<div class="context">\n\n					<p>您好，您可以点击详情页面中导航按钮，然后根据操作完成导航过程即可。</p>\n\n				</div>\n\n			</div>\n\n		</div>\n\n	</div>\n\n</ion-content>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\help\help.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], HelpPage);
    return HelpPage;
}());

//# sourceMappingURL=help.js.map

/***/ }),

/***/ 349:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_security_SecurityService__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PasswordPage = /** @class */ (function () {
    function PasswordPage(navCtrl, navParams, securityService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.securityService = securityService;
    }
    PasswordPage.prototype.setPassword = function () {
        var _this = this;
        this.securityService.setPassword(this.password, this.rpassword, function () {
            _this.navCtrl.pop();
        });
    };
    PasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\password\password.html"*/'<!-- <ion-header>\n\n	<ion-toolbar style="min-height: 20px;">\n\n		<button ion-button clear small navPop style="padding: 0;">\n\n			<i class="icon ion-chevron-left"></i>\n\n			<span style="font-size: 1.6rem;color:black;">返回</span>\n\n		</button>\n\n	</ion-toolbar>  \n\n</ion-header> -->\n\n<ion-content padding>\n\n	<div class="zs-page-header">\n\n		<a (click)=\'navCtrl.pop()\' class="back"></a>\n\n		修改密码\n\n	</div>\n\n\n\n	<ion-item style="margin-top: 68px;">\n\n      <ion-label fixed>旧密码</ion-label>\n\n      <ion-input type="password" [(ngModel)]="password" ></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label fixed>新密码</ion-label>\n\n      <ion-input type="password" [(ngModel)]="rpassword"></ion-input>\n\n    </ion-item>\n\n\n\n    <div style="text-align: center; margin-left: 30px; margin-right: 30px;">\n\n	    <button ion-button block (click)=\'setPassword()\'>\n\n	      登录\n\n	    </button>\n\n	</div>\n\n  \n\n</ion-content>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\password\password.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_security_SecurityService__["a" /* SecurityService */]])
    ], PasswordPage);
    return PasswordPage;
}());

//# sourceMappingURL=password.js.map

/***/ }),

/***/ 350:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignNewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_buiness_SignHttp__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_core_data_DataService__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SignNewPage = /** @class */ (function () {
    function SignNewPage(navCtrl, alertCtrl, baseService, dataService, securityService, signHttp, changeDetectorRef, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.baseService = baseService;
        this.dataService = dataService;
        this.securityService = securityService;
        this.signHttp = signHttp;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.datas = [];
        this.user = {};
        this.tableName = 'gzqdgl';
        this.showDetails = false;
        this.latitude = ""; //维度
        this.longitude = ""; //经度
        this.formattedAddress = "";
        this.accuracy = "";
        //构建formObject对象方式修改，这种方式建立了独立的对象数据
        this.gzqdgl = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
    }
    //初始化页面
    SignNewPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.user = {};
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.initSuccessSignPage();
            }, function () {
                _this.user = null;
                _this.goLogin();
            });
        }
        else {
            this.initSuccessSignPage();
        }
    };
    ;
    //跳转到登录界面
    SignNewPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    /**
     * 成功加载用户签到页面时处理
     */
    SignNewPage.prototype.initSuccessSignPage = function () {
        this.user = this.securityService.user;
        this.gzqdgl.sn = this.tableName;
        //根据时间排序
        this.gzqdgl.orderObject = { "nandu": 1, "yuefen": 1, "day": 1 };
        //根据登录用户过滤
        // if(this.user["userid"]!='admin'){
        //   this.gzqdgl.queryObject["kaoqrid:="]=this.user["userid"];
        // }
        this.gzqdgl.pageIndex = 1;
        this.gzqdgl.datas = [];
        this.gzqdgl.getEntitys(this.editDatas);
    };
    ;
    SignNewPage.prototype.editDatas = function (datas) {
        for (var item = 0; item < datas.length; item++) {
            datas[item].icon = 'ios-arrow-forward';
        }
    };
    ;
    // setQueryUser(){
    //   this.queryObject["userID:="]=this.user["userid"];
    // };
    //打开或关闭数据详细内容
    SignNewPage.prototype.toggleDetails = function (data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-forward';
        }
        else {
            data.showDetails = true;
            data.icon = 'ios-arrow-down';
        }
    };
    ;
    SignNewPage.prototype.gzqdgldata = function () {
        var _this = this;
        this.signHttp.gzqdgldata("create", this.latitude, this.longitude, this.formattedAddress, this.accuracy).subscribe(function (data) {
            if (data) {
                var time = new Date();
                var strtime = time.getHours() + "时" + time.getMinutes() + "分" + time.getSeconds() + "秒";
                var alert_1 = _this.alertCtrl.create({
                    title: '签到成功',
                    subTitle: '签到时间:' + strtime,
                    buttons: ['确定']
                });
                _this.gzqdgl.pageIndex = 1;
                _this.gzqdgl.datas = [];
                _this.gzqdgl.getEntitys(_this.editDatas);
                alert_1.present();
            }
        }, function (err) {
            var alert = _this.alertCtrl.create({
                title: '系统提示',
                subTitle: '签到失败，请重试！',
                buttons: ['确定']
            });
            alert.present();
        });
    };
    ;
    SignNewPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    //查询
    SignNewPage.prototype.onSearchKeyUp = function (event) {
        if ("Enter" == event.key) {
            //if(this.user!=null && this.user["userid"]!=null){
            // this.formObject.pageIndex=0;
            // this.formObject.dataOver=false;
            // this.formObject.getEntitys();
        }
        //}else{
        //this.queryValue="";
        //}
    };
    SignNewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\sign_new\sign_new.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n      <ion-title>签到</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <div class="sign-sucess">\n\n    <img src="assets/imgs/sign_suc.png" />\n\n  </div>\n\n  <div class="sign-buttons">\n\n    <div class="sign-button active">签到</div>\n\n    <div class="sign-button">更多</div>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\sign_new\sign_new.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_6__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_7__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_5__app_buiness_SignHttp__["a" /* SignHttp */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], SignNewPage);
    return SignNewPage;
}());

//# sourceMappingURL=sign_new.js.map

/***/ }),

/***/ 351:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeaveNewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_qjgl_qjgl__ = __webpack_require__(80);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var LeaveNewPage = /** @class */ (function () {
    function LeaveNewPage(navCtrl, baseService, dataService, securityService, changeDetectorRef, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.baseService = baseService;
        this.dataService = dataService;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.datas = [];
        this.user = {};
        this.tableName = 'gzqjgl';
        this.showDetails = false;
        //构建formObject对象方式修改，这种方式建立了独立的对象数据
        this.gzqjgl = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
    }
    //初始化页面
    LeaveNewPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.user = {};
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.initSuccessTripPage();
            }, function () {
                _this.user = null;
                _this.goLogin();
            });
        }
        else {
            this.initSuccessTripPage();
        }
    };
    ;
    LeaveNewPage.prototype.toQueryWork = function (data) {
        var para = {};
        para["bk"] = data.bpmkey;
        para["fn"] = "gzqjgl";
        para["vi"] = "gzqjgl";
        para["dk"] = data.id;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_qjgl_qjgl__["a" /* QjglPage */], para);
    };
    ;
    LeaveNewPage.prototype.startNewBpm = function (bpmkey, formname) {
        var para = {};
        para["bk"] = bpmkey;
        para["fn"] = formname;
        para["vi"] = "gzqjgl";
        para["dk"] = this.gzqjgl.getGUID();
        para["FIELD_ISNEW"] = true;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_qjgl_qjgl__["a" /* QjglPage */], para);
    };
    LeaveNewPage.prototype.editDatas = function (datas) {
        for (var item = 0; item < datas.length; item++) {
            datas[item].icon = 'ios-arrow-forward';
        }
    };
    ;
    //跳转到登录界面
    LeaveNewPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    /**
     * 成功加载用户出差页面时处理
     */
    LeaveNewPage.prototype.initSuccessTripPage = function () {
        this.user = this.securityService.user;
        //根据登录用户过滤
        // if(this.user["userid"]!='admin'){
        //   this.formObject.queryObject["qjrid:="]=this.user["userid"];
        // }
        this.gzqjgl.datas = [];
        this.gzqjgl.orderObject["kssj"] = 1;
        this.gzqjgl.pageIndex = 1;
        this.gzqjgl.dataOver = false;
        this.gzqjgl.sn = this.tableName;
        this.gzqjgl.getEntitys(this.editDatas);
    };
    ;
    // setQueryUser(){
    //   this.queryObject["userID:="]=this.user["userid"];
    // };
    //打开或关闭数据详细内容
    LeaveNewPage.prototype.toggleDetails = function (data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-forward';
        }
        else {
            data.showDetails = true;
            data.icon = 'ios-arrow-down';
        }
    };
    //查询
    LeaveNewPage.prototype.onSearchKeyUp = function (event) {
        if ("Enter" == event.key) {
            //if(this.user!=null && this.user["userid"]!=null){
            // this.formObject.pageIndex=0;
            // this.formObject.dataOver=false;
            // this.formObject.getEntitys();
        }
        //}else{
        //this.queryValue="";
        //}
    };
    LeaveNewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-leave-new',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\leave-new\leave-new.html"*/'<!--\n\n  Generated template for the LeaveNewPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n      <ion-title>请假记录</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <ion-refresher (ionRefresh)="gzqjgl.doRefresh($event)">\n\n    <ion-refresher-content pullingIcon="arrow-dropdown"  pullingText="下拉刷新"  refreshingSpinner="circles"  refreshingText="刷新中...">\n\n    </ion-refresher-content>\n\n  </ion-refresher>\n\n  <ion-list>\n\n    <ion-item class="bhq-item" (click)="toQueryWork(item);"\n\n      *ngFor="let item of gzqjgl.datas">\n\n    \n\n      <div class="bhq-item-content">\n\n          <div class="bhq-item-info">\n\n            <p class="bhq-item-title"><strong>{{item.sqsy | asLength:12}}({{item.qjlxname}})</strong></p>\n\n            <p>{{item.qjrxm}}({{item.spztName}})</p>\n\n          </div>\n\n          <div class="bhq-item-date">\n\n              <p>开始时间:{{item.kssj | asDate}}</p>\n\n              <p>结束时间:{{item.jssj | asDate}}</p>\n\n          </div>\n\n      </div>\n\n    </ion-item>\n\n  </ion-list>\n\n  <!-- 无级滚动效果实现 -->\n\n  <ion-infinite-scroll (ionInfinite)="gzqjgl.doInfinite($event)">\n\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n  </ion-infinite-scroll>\n\n</ion-content>\n\n'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\leave-new\leave-new.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_5__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_6__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], LeaveNewPage);
    return LeaveNewPage;
}());

//# sourceMappingURL=leave-new.js.map

/***/ }),

/***/ 352:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VehiclePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_core_dictionary_DictionaryService__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_bpm_BpmObject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_bpmback_bpmback__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_bpmsign_bpmsign__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_bpmprocess_bpmprocess__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_bpmimg_bpmimg__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_utils_DateUtil__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var VehiclePage = /** @class */ (function () {
    function VehiclePage(navCtrl, navParams, dataService, baseSErvice, alert, securityService, changeDetectorRef, loadingCtrl, actionSheetCtrl, dateUtil) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseSErvice = baseSErvice;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.dateUtil = dateUtil;
        this.user = {};
        /**
       * 请假类别
       */
        this.qjlb = new __WEBPACK_IMPORTED_MODULE_5__app_core_dictionary_DictionaryService__["a" /* DictionaryService */](this.dataService, this.baseSErvice, "GZ_QJLX", "0", false);
        this.gzclsq = new __WEBPACK_IMPORTED_MODULE_7__app_bpm_BpmObject__["a" /* BpmObject */](this.dataService, this.baseSErvice, this.changeDetectorRef, this.navCtrl, this.alert, this.loadingCtrl, "gzclsq");
        if (this.navParams.data != null && this.navParams.data["FIELD_ISNEW"]) {
            this.gzclsq.editRow = {};
            this.gzclsq.editRow.id = this.navParams.data["dk"];
            this.gzclsq.editRow.bpmkey = this.navParams.data["bk"];
            this.gzclsq.editRow.FIELD_ISNEW = this.navParams.data["FIELD_ISNEW"];
            /*
            this.gzclsq.editRow.sqrq=this.gzclsq.formatDateToSecond(new Date());
            this.gzclsq.editRow.qjts="1";
            this.gzclsq.editRow.sjsjjd="08";
            this.gzclsq.editRow.jssjjd="18";
            this.gzclsq.editRow.kssj=this.gzclsq.formatDate(new Date());
            this.gzclsq.editRow.jssj=this.gzclsq.formatDate(new Date());*/
        }
        this.gzclsq.sn = "gzclsq";
        this.gzclsq.bpmKey = this.navParams.data["bk"];
        this.gzclsq.id = this.navParams.data["dk"];
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                if (_this.navParams.data != null && _this.navParams.data["bk"] != null) {
                    _this.gzclsq.findWork(null, null);
                }
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            if (this.navParams.data != null && this.navParams.data["bk"] != null) {
                this.gzclsq.findWork(null, null);
            }
        }
        if (this.navParams.data["FIELD_ISNEW"]) {
            this.gzclsq.editRow.ycbm = this.user["departmentId"];
            this.gzclsq.editRow.ycbmName = this.user["departmentName"];
            this.gzclsq.editRow.ssqrid = this.user["userid"];
            this.gzclsq.editRow.ssqr = this.user["userName"];
            this.gzclsq.editRow.id = this.gzclsq.getGUID();
        }
    }
    VehiclePage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    VehiclePage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    VehiclePage.prototype.goSignPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_bpmsign_bpmsign__["a" /* BpmSignPage */], this.gzclsq);
    };
    ;
    VehiclePage.prototype.goBackPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_bpmback_bpmback__["a" /* BpmBackPage */], this.gzclsq);
    };
    ;
    VehiclePage.prototype.goProcessPage = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: '更多',
            buttons: [
                {
                    text: '办理过程',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_bpmprocess_bpmprocess__["a" /* BpmProcessPage */], _this.gzclsq);
                    }
                }, {
                    text: '流程图',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__pages_bpmimg_bpmimg__["a" /* BpmImgPage */], _this.gzclsq);
                    }
                }, {
                    text: '取消',
                    role: '取消',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ;
    VehiclePage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad LeaveNewPage');
        this.qjlb.getDatas(null, null);
    };
    ;
    VehiclePage.prototype.userFormValidate = function () {
        var message = [];
        if (this.gzclsq.editRow["djysj"] == null || this.gzclsq.editRow["djysj"] == "") {
            message.push("<span style='color:red'>开始时间<br></span>");
        }
        if (this.gzclsq.editRow["dghsj"] == null || this.gzclsq.editRow["dghsj"] == "") {
            message.push("<span style='color:red'>结束时间<br></span>");
        }
        if (this.gzclsq.editRow["ssqyy"] == null || this.gzclsq.editRow["ssqyy"] == "") {
            message.push("<span style='color:red'>用车事由<br></span>");
        }
        if (this.gzclsq.editRow["ycdd"] == null || this.gzclsq.editRow["ycdd"] == "") {
            message.push("<span style='color:red'>用车地点<br></span>");
        }
        if (this.gzclsq.editRow["ycrs"] == null || this.gzclsq.editRow["ycrs"] == "") {
            message.push("<span style='color:red'>用车人数<br></span>");
        }
        if (this.gzclsq.editRow["jsy"] == null || this.gzclsq.editRow["jsy"] == "") {
            message.push("<span style='color:red'>驾驶员<br></span>");
        }
        if (this.gzclsq.editRow["slxfs"] == null || this.gzclsq.editRow["slxfs"] == "") {
            message.push("<span style='color:red'>驾驶员联系方式<br></span>");
        }
        if (this.gzclsq.editRow["dsqsj"] == null || this.gzclsq.editRow["dsqsj"] == "") {
            message.push("<span style='color:red'>申请时间<br></span>");
        }
        if (message.length > 0) {
            var strm = message.join("");
            var temp = this.alert.create({
                title: '系统提示',
                subTitle: '以下表单项没有填写正确! <br>' + strm,
                buttons: ['确定']
            });
            temp.present();
            return false;
        }
        else {
            return true;
        }
    };
    ;
    /**
     * 设置天数
     */
    VehiclePage.prototype.setTs = function (e) {
        var that = this;
        setTimeout(function () {
            var start = that.dateUtil.parseDate(that.gzclsq.editRow.kssj, "string");
            // var fmstart = $rootScope.parseData(start,'string');
            var end = that.dateUtil.parseDate(that.gzclsq.editRow.jssj, "string");
            // var fmend = $rootScope.parseData(end,'string');
            var i = (end - start) / 1000 / 60 / 60 / 24 + 1;
            var time = Number(i.toFixed(0));
            if (that.gzclsq.editRow.sjsjjd == 14 || that.gzclsq.editRow.sjsjjd == '14') {
                time = time - 0.5;
            }
            if (that.gzclsq.editRow.jssjjd == 12 || that.gzclsq.editRow.jssjjd == '12') {
                time = time - 0.5;
            }
            that.gzclsq.editRow.qjts = time;
        }, 200);
    };
    ;
    VehiclePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-vehicle',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\vehicle\vehicle.html"*/'<ion-header>\n\n	<ion-navbar>\n\n		<ion-title>车辆使用申请</ion-title>\n\n	</ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n	<ion-list class="approval-list" >\n\n		<ion-list-header>\n\n			<span class="approval-list-header-text">申请信息</span>\n\n		</ion-list-header>\n\n		<ion-item>\n\n			<ion-label class="bhq-title">用车部门:</ion-label>\n\n			<ion-label class="bhq-content-right">{{gzclsq.editRow.ycbmName}}</ion-label>\n\n		</ion-item>\n\n		<ion-item>\n\n			<ion-label class="bhq-title"><font color="red">*</font>申请人:</ion-label>\n\n			<ion-label class="bhq-content-right">{{gzclsq.editRow.ssqr}}</ion-label>\n\n        </ion-item>\n\n        <ion-item>\n\n			<ion-label class="bhq-title"><font color="red">*</font>开始时间:</ion-label>\n\n			<ion-datetime class="bhq-content-right"\n\n			max="2100-10-31" displayFormat="YYYY-MM-DD" [(ngModel)]="gzclsq.editRow.djysj" cancelText="取消"\n\n			 doneText="确认"></ion-datetime>\n\n        </ion-item>\n\n        <ion-item>\n\n			<ion-label class="bhq-title"><font color="red">*</font>结束时间:</ion-label>\n\n			<ion-datetime class="bhq-content-right"\n\n			max="2100-10-31" displayFormat="YYYY-MM-DD" [(ngModel)]="gzclsq.editRow.dghsj" cancelText="取消"\n\n			 doneText="确认"></ion-datetime>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n			<ion-label class="bhq-title"><font color="red">*</font>用车事由:</ion-label>\n\n			<ion-input class="bhq-content-right" [(ngModel)]="gzclsq.editRow.ssqyy"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n			<ion-label class="bhq-title"><font color="red">*</font>用车地点:</ion-label>\n\n			<ion-input class="bhq-content-right" [(ngModel)]="gzclsq.editRow.ycdd"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n			<ion-label class="bhq-title"><font color="red">*</font>用车人数:</ion-label>\n\n			<ion-input class="bhq-content-right" [(ngModel)]="gzclsq.editRow.ycrs"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n			<ion-label class="bhq-title">用车人员:</ion-label>\n\n			<ion-input class="bhq-content-right" [(ngModel)]="gzclsq.editRow.ycry"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n			<ion-label class="bhq-title"><font color="red">*</font>驾驶员:</ion-label>\n\n			<ion-input class="bhq-content-right" [(ngModel)]="gzclsq.editRow.jsy"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n			<ion-label class="bhq-title"><font color="red">*</font>联系方式:</ion-label>\n\n			<ion-input class="bhq-content-right" [(ngModel)]="gzclsq.editRow.slxfs"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n			<ion-label class="bhq-title"><font color="red">*</font>申请时间:</ion-label>\n\n			<ion-datetime class="bhq-content-right"\n\n			max="2100-10-31" displayFormat="YYYY-MM-DD" [(ngModel)]="gzclsq.editRow.dsqsj" cancelText="取消"\n\n			 doneText="确认"></ion-datetime>\n\n        </ion-item>\n\n        <ion-item>\n\n			<ion-label class="bhq-title">备注:</ion-label>\n\n			<ion-input class="bhq-content-right" [(ngModel)]="gzclsq.editRow.sbz"></ion-input>\n\n        </ion-item>\n\n	</ion-list>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n	<button class="submit-btn" (click)="userFormValidate() && gzclsq.getNextTask(null,null);">提交</button>\n\n</ion-footer>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\vehicle\vehicle.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_12__app_utils_DateUtil__["a" /* DateUtil */]])
    ], VehiclePage);
    return VehiclePage;
}());

//# sourceMappingURL=vehicle.js.map

/***/ }),

/***/ 353:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VehicleannalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_ycsq_ycsq__ = __webpack_require__(81);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var VehicleannalPage = /** @class */ (function () {
    function VehicleannalPage(navCtrl, baseService, dataService, securityService, changeDetectorRef, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.baseService = baseService;
        this.dataService = dataService;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.datas = [];
        this.user = {};
        this.tableName = 'gzclsq';
        this.showDetails = false;
        //构建formObject对象方式修改，这种方式建立了独立的对象数据
        this.gzclsq = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
    }
    //初始化页面
    VehicleannalPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.user = {};
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.initSuccessTripPage();
            }, function () {
                _this.user = null;
                _this.goLogin();
            });
        }
        else {
            this.initSuccessTripPage();
        }
    };
    ;
    VehicleannalPage.prototype.toQueryWork = function (data) {
        var para = {};
        para["bk"] = data.bpmkey;
        para["fn"] = "gzclsq";
        para["vi"] = "gzclsq";
        para["dk"] = data.id;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_ycsq_ycsq__["a" /* YcsqPage */], para);
    };
    ;
    VehicleannalPage.prototype.startNewBpm = function (bpmkey, formname) {
        var para = {};
        para["bk"] = bpmkey;
        para["fn"] = formname;
        para["vi"] = "gzclsq";
        para["dk"] = this.gzclsq.getGUID();
        para["FIELD_ISNEW"] = true;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_ycsq_ycsq__["a" /* YcsqPage */], para);
    };
    VehicleannalPage.prototype.editDatas = function (datas) {
        for (var item = 0; item < datas.length; item++) {
            datas[item].icon = 'ios-arrow-forward';
        }
    };
    ;
    //跳转到登录界面
    VehicleannalPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    /**
     * 成功加载用户出差页面时处理
     */
    VehicleannalPage.prototype.initSuccessTripPage = function () {
        this.user = this.securityService.user;
        //根据登录用户过滤
        // if(this.user["userid"]!='admin'){
        //   this.formObject.queryObject["qjrid:="]=this.user["userid"];
        // }
        this.gzclsq.datas = [];
        this.gzclsq.orderObject["id"] = 0;
        this.gzclsq.pageIndex = 1;
        this.gzclsq.dataOver = false;
        this.gzclsq.sn = this.tableName;
        this.gzclsq.getEntitys(this.editDatas);
    };
    ;
    // setQueryUser(){
    //   this.queryObject["userID:="]=this.user["userid"];
    // };
    //打开或关闭数据详细内容
    VehicleannalPage.prototype.toggleDetails = function (data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-forward';
        }
        else {
            data.showDetails = true;
            data.icon = 'ios-arrow-down';
        }
    };
    //查询
    VehicleannalPage.prototype.onSearchKeyUp = function (event) {
        if ("Enter" == event.key) {
            //if(this.user!=null && this.user["userid"]!=null){
            // this.formObject.pageIndex=0;
            // this.formObject.dataOver=false;
            // this.formObject.getEntitys();
        }
        //}else{
        //this.queryValue="";
        //}
    };
    VehicleannalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-vehicleannal',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\vehicleannal\vehicleannal.html"*/'<!--\n\n  Generated template for the LeaveNewPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n        <ion-navbar>\n\n            <ion-title>车辆使用记录</ion-title>\n\n        </ion-navbar>\n\n      \n\n      </ion-header>\n\n      \n\n      \n\n      <ion-content>\n\n        <ion-refresher (ionRefresh)="gzclsq.doRefresh($event)">\n\n          <ion-refresher-content pullingIcon="arrow-dropdown"  pullingText="下拉刷新"  refreshingSpinner="circles"  refreshingText="刷新中...">\n\n          </ion-refresher-content>\n\n        </ion-refresher>\n\n        <ion-list>\n\n          <ion-item class="bhq-item" (click)="toQueryWork(item);"\n\n            *ngFor="let item of gzclsq.datas">\n\n          \n\n            <div class="bhq-item-content">\n\n                <div class="bhq-item-info">\n\n                  <p class="bhq-item-title"><strong>{{item.ssqyy | asLength:12}}</strong></p>\n\n                  <p>{{item.ssqr}}({{item.spztName}})</p>\n\n                </div>\n\n                <div class="bhq-item-date">\n\n                    <p>开始时间:{{item.djysj | asDate}}</p>\n\n                    <p>结束时间:{{item.dghsj | asDate}}</p>\n\n                </div>\n\n            </div>\n\n          </ion-item>\n\n        </ion-list>\n\n        <!-- 无级滚动效果实现 -->\n\n        <ion-infinite-scroll (ionInfinite)="gzclsq.doInfinite($event)">\n\n          <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n        </ion-infinite-scroll>\n\n      </ion-content>\n\n      '/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\vehicleannal\vehicleannal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_5__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_6__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], VehicleannalPage);
    return VehicleannalPage;
}());

//# sourceMappingURL=vehicleannal.js.map

/***/ }),

/***/ 354:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SwglNewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_swgl_swgl__ = __webpack_require__(79);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SwglNewPage = /** @class */ (function () {
    function SwglNewPage(navCtrl, baseService, dataService, securityService, changeDetectorRef, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.baseService = baseService;
        this.dataService = dataService;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.datas = [];
        this.user = {};
        this.tableName = 'gzreceiveview';
        this.showDetails = false;
        this.workInput = "";
        //构建formObject对象方式修改，这种方式建立了独立的对象数据
        this.gzreceiveview = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
    }
    SwglNewPage.prototype.searchWork = function () {
        //this.worklist.queryObject={"sbusiness:like":this.workInput?this.workInput:""};
        if (this.workInput == null || this.workInput == "") {
            this.gzreceiveview.queryObject = {};
        }
        else {
            this.gzreceiveview.queryObject["fwzt:like"] = this.workInput;
        }
        this.gzreceiveview.pageIndex = 1; //重置查询条件的时候，需要重置页码
        this.gzreceiveview.datas = [];
        this.gzreceiveview.getEntitys(null);
    };
    SwglNewPage.prototype.toQueryWork = function (data) {
        var para = {};
        para["bk"] = data.bpmkey;
        para["fn"] = "gzpublishfile";
        para["vi"] = "gzpublishfile";
        para["dk"] = data.fwid;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_swgl_swgl__["a" /* SwglPage */], para);
    };
    ;
    //初始化页面
    SwglNewPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.user = {};
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.initSuccessHomePage();
            }, function () {
                _this.user = null;
                _this.goLogin();
            });
        }
        else {
            this.initSuccessHomePage();
        }
    };
    ;
    //跳转到登录界面
    SwglNewPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    /**
     * 成功加载用户home页面时处理
     */
    SwglNewPage.prototype.initSuccessHomePage = function () {
        this.user = this.securityService.user;
        //构建formObject对象方式修改，这种方式建立了独立的对象数据
        if (this.gzreceiveview.sn == "") {
            this.gzreceiveview = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
            this.gzreceiveview.sn = "gzreceiveview";
            this.gzreceiveview.queryObject = { "sfqr": "0" };
            this.gzreceiveview.orderObject = { "jjdj": 1, "cjsj": 1 };
        }
        this.gzreceiveview.getEntitys(null);
    };
    ;
    SwglNewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-swgl-new',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\swgl_new\swgl_new.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>收文</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content>\n\n    <div class="search-bar-content">\n\n      <ion-searchbar placeholder="搜索" (ionInput)="searchWork()" [(ngModel)]="workInput">\n\n      </ion-searchbar>\n\n    </div>\n\n       <!--<ion-fab right middle>\n\n        <button ion-fab color="#3dbdf5"><ion-icon name="refresh"></ion-icon></button>\n\n      </ion-fab>-->\n\n  \n\n    <!--下拉事件时，刷新页面数据-->\n\n    <ion-refresher (ionRefresh)="gzreceiveview.doRefresh($event)">\n\n      <ion-refresher-content pullingIcon="arrow-dropdown"  \n\n      pullingText="下拉刷新"  refreshingSpinner="circles"  refreshingText="刷新中...">\n\n        </ion-refresher-content>\n\n    </ion-refresher>\n\n    <ion-list class="bhq-list"  >\n\n      <ion-item class="bhq-item"  (click)="toQueryWork(item)"\n\n        *ngFor="let item of gzreceiveview.datas">\n\n       \n\n        <div class="bhq-item-content">\n\n            <div class="bhq-item-info">\n\n              <p ><strong>{{item.fwzt | asLength:16}}({{item.jjdjName}})</strong></p>\n\n              <p>{{item.cjrmc}}({{item.bmmcName}})</p>\n\n            </div>\n\n            <div class="bhq-item-date">\n\n              <p>&nbsp;</p>\n\n              <p>{{item.cjsj | date:\'yyyy-MM-dd\'}}</p>\n\n            </div>\n\n        </div>\n\n      </ion-item>\n\n    </ion-list>\n\n    \n\n    <!-- 无级滚动效果实现 -->\n\n    <ion-infinite-scroll (ionInfinite)="gzreceiveview.doInfinite($event)">\n\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n  </ion-content>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\swgl_new\swgl_new.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_5__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_6__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], SwglNewPage);
    return SwglNewPage;
}());

//# sourceMappingURL=swgl_new.js.map

/***/ }),

/***/ 355:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FwglPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var FwglPage = /** @class */ (function () {
    function FwglPage(navCtrl, navParams, dataService, baseSErvice, alert, securityService, changeDetectorRef, loadingCtrl, actionSheetCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseSErvice = baseSErvice;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.user = {};
        this.gzpublishfile = new __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__["a" /* BpmObject */](this.dataService, this.baseSErvice, this.changeDetectorRef, this.navCtrl, this.alert, this.loadingCtrl, "gzpublishfile");
        if (this.navParams.data != null && this.navParams.data["bk"] != null) {
            this.gzpublishfile.editRow = {};
            this.gzpublishfile.sn = "gzpublishfile";
            this.gzpublishfile.bpmKey = this.navParams.data["bk"];
            this.gzpublishfile.id = this.navParams.data["dk"];
            this.gzpublishfile.editRow.id = this.navParams.data["dk"];
            this.gzpublishfile.editRow.bpmKey = this.navParams.data["bk"];
            this.gzpublishfile.editRow.FIELD_ISNEW = this.navParams.data["FIELD_ISNEW"];
            this.gzpublishfile.editRow.cjsj = this.gzpublishfile.formatDateToSecond(new Date());
            ;
        }
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                _this.gzpublishfile.editRow.cjrid = _this.user["userid"];
                _this.gzpublishfile.editRow.cjrmc = _this.user["userName"];
                _this.gzpublishfile.editRow.bmmc = _this.user["departmentId"];
                _this.gzpublishfile.editRow.bmmcName = _this.user["departmentName"];
                if (_this.navParams.data != null && _this.navParams.data["bk"] != null) {
                    _this.gzpublishfile.findWork(null, null);
                }
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.gzpublishfile.editRow.cjrid = this.user["userid"];
            this.gzpublishfile.editRow.cjrmc = this.user["userName"];
            this.gzpublishfile.editRow.bmmc = this.user["departmentId"];
            this.gzpublishfile.editRow.bmmcName = this.user["departmentName"];
            if (this.navParams.data != null && this.navParams.data["bk"] != null) {
                this.gzpublishfile.findWork(null, null);
            }
        }
    }
    FwglPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    FwglPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    FwglPage.prototype.goSignPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__["a" /* BpmSignPage */], this.gzpublishfile);
    };
    ;
    FwglPage.prototype.goBackPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__["a" /* BpmBackPage */], this.gzpublishfile);
    };
    ;
    FwglPage.prototype.goProcessPage = function () {
        var _this = this;
        //this.navCtrl.push(BpmProcessPage,this.gzpublishfile);
        var actionSheet = this.actionSheetCtrl.create({
            title: '更多',
            buttons: [
                {
                    text: '办理过程',
                    handler: function () {
                        //console.log('Destructive clicked');
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__["a" /* BpmProcessPage */], _this.gzpublishfile);
                    }
                }, {
                    text: '流程图',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__["a" /* BpmImgPage */], _this.gzpublishfile);
                    }
                }, {
                    text: '取消',
                    role: '取消',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ;
    FwglPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fwgl',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\fwgl\fwgl.html"*/'<!--\n\n  Generated template for the ApprovalPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>发文</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n    <ion-list class="approval-list">\n\n        <ion-list-header>\n\n          <span class="approval-list-header-text">申请信息</span>\n\n        </ion-list-header>\n\n        <ion-item>\n\n          <ion-label>发文主题:</ion-label>\n\n          <ion-input [(ngModel)]="gzpublishfile.editRow.fwzt"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>发文说明:</ion-label>\n\n          <ion-input [(ngModel)]="gzpublishfile.editRow.fwsm"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>紧急等级:</ion-label>\n\n          <ion-input [(ngModel)]="gzpublishfile.editRow.jjdj"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>负责人:</ion-label>\n\n          <ion-input [(ngModel)]="gzpublishfile.editRow.zyfzrname"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>创建人:</ion-label>\n\n          <ion-label>{{gzpublishfile.editRow.cjrmc}}</ion-label>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>创建时间:</ion-label>\n\n          <ion-label>{{gzpublishfile.editRow.cjsj}}</ion-label>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>部门名称:</ion-label>\n\n          <ion-label>{{gzpublishfile.editRow.bmmcName}}</ion-label>\n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label>处理情况:</ion-label>\n\n          <ion-input [(ngModel)]="gzpublishfile.editRow.blqk"></ion-input>\n\n        </ion-item>\n\n      </ion-list>\n\n    \n\n      <!-- <ion-list class="approval-list" *ngIf="gzpublishfile.editRow.bpmkey==\'sfwgl_jzgb\'">\n\n        <ion-list-header>\n\n          <span class="approval-list-header-text">收件人</span>\n\n        </ion-list-header>\n\n        <ion-item *ngFor="let item of gzpublishfile.editRow.gzreceivefile">\n\n          <ion-label>{{item.jsrmc}}({{item.jsrjgmc}})</ion-label>\n\n        </ion-item>\n\n      </ion-list> -->\n\n</ion-content>\n\n<ion-footer>\n\n  <div class="footer-btn"  *ngIf="gzpublishfile.auth.opt_th"  (click)="gzpublishfile.getBackTask(null,null);">\n\n    <img src="assets/imgs/back.png"/>\n\n    <p>退回</p>\n\n  </div>\n\n  <div class="footer-btn" *ngIf="gzpublishfile.auth.opt_bc" (click)="gzpublishfile.bpmSave(null);">\n\n    <img src="assets/imgs/save.png"/>\n\n    <p>保存</p>\n\n  </div>\n\n  <div class="footer-btn"  *ngIf="gzpublishfile.auth.opt_tj" (click)="gzpublishfile.getNextTask(null,null);">\n\n    <img src="assets/imgs/pass.png"/>\n\n    <p>通过</p>\n\n  </div>\n\n  \n\n  <div class="footer-more"  (click)="goProcessPage();">更多 </div>\n\n  \n\n</ion-footer>\n\n'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\fwgl\fwgl.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"]])
    ], FwglPage);
    return FwglPage;
}());

//# sourceMappingURL=fwgl.js.map

/***/ }),

/***/ 400:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__todo_todo__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__task_task__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__jobs_jobs__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__me_me__ = __webpack_require__(188);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TabsPage = /** @class */ (function () {
    function TabsPage(platform, navCtrl) {
        this.navCtrl = navCtrl;
        this.tabRoots = [{
                root: __WEBPACK_IMPORTED_MODULE_4__jobs_jobs__["a" /* JobsPage */],
                tabTitle: '工作',
                tabIcon: 'bhq__jobs'
            }, {
                root: __WEBPACK_IMPORTED_MODULE_2__todo_todo__["a" /* TodoPage */],
                tabTitle: '待办',
                tabIcon: 'bhq__todo'
            }, {
                root: __WEBPACK_IMPORTED_MODULE_3__task_task__["a" /* TaskPage */],
                tabTitle: '任务',
                tabIcon: 'bhq__task'
            }, {
                root: __WEBPACK_IMPORTED_MODULE_5__me_me__["a" /* MePage */],
                tabTitle: '我的',
                tabIcon: 'bhq__me'
            }
        ];
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('mainTabs'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Tabs"])
    ], TabsPage.prototype, "tabs", void 0);
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: '<ion-tabs #mainTabs class="tabs-icon">' +
                '<ion-tab *ngFor="let tabRoot of tabRoots" [root]="tabRoot.root"' +
                'tabTitle="{{tabRoot.tabTitle}}" tabIcon="{{tabRoot.tabIcon}}"></ion-tab>' +
                '</ion-tabs>'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 401:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageUtil; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_core_data_BaseService__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// 基础服务类，提供基础路径
var ImageUtil = /** @class */ (function () {
    function ImageUtil(baseService) {
        this.baseService = baseService;
    }
    /**
    *
    * @param image 转换图片的地址
    */
    ImageUtil.prototype.getImagePath = function (image) {
        var fileInfo = image;
        if (fileInfo == null || fileInfo == "")
            return "";
        var fileInfoArr = fileInfo.split(":");
        if (fileInfoArr.length < 3)
            return "";
        var bucket = fileInfoArr[0];
        var key = fileInfoArr[1];
        var name = fileInfoArr[2];
        return this.baseService.fileUrl + "/" + bucket + "/" + key;
    };
    ;
    /**
*
* @param image 转换图片的地址
*/
    ImageUtil.prototype.getVideoPath = function (image) {
        var fileInfo = image;
        if (fileInfo == null || fileInfo == "")
            return "";
        var fileInfoArr = fileInfo.split(":");
        if (fileInfoArr.length < 3)
            return "";
        var bucket = fileInfoArr[0];
        var key = fileInfoArr[1];
        var name = fileInfoArr[2];
        return this.baseService.fileUrl + "/" + bucket + "/" + key;
    };
    ;
    /**
     * 获取视频文件类型
     * @param fileInfo
     */
    ImageUtil.prototype.getVideoType = function (fileInfo) {
        if (fileInfo == null || fileInfo == "")
            return "";
        var fileInfoArr = fileInfo.split(":");
        if (fileInfoArr.length < 3)
            return "";
        var bucket = fileInfoArr[0];
        var key = fileInfoArr[1];
        var name = fileInfoArr[2];
        var namearray = name.split(".");
        if (namearray != null && namearray.length == 2)
            return namearray[1];
        return null;
    };
    ImageUtil = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__app_core_data_BaseService__["a" /* BaseService */]])
    ], ImageUtil);
    return ImageUtil;
}());

//# sourceMappingURL=ImageUtil.js.map

/***/ }),

/***/ 404:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__about_about__ = __webpack_require__(347);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__help_help__ = __webpack_require__(348);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__password_password__ = __webpack_require__(349);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__sign_sign__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__trip_trip__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__leave_leave__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_core_security_SecurityService__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var SettingPage = /** @class */ (function () {
    function SettingPage(viewCtrl, navCtrl, navParams, toastCtrl, securityService) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.securityService = securityService;
        this.securityService.init();
    }
    SettingPage.prototype.goAbout = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__about_about__["a" /* AboutPage */]);
    };
    SettingPage.prototype.goHelp = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__help_help__["a" /* HelpPage */]);
    };
    SettingPage.prototype.goLogin = function () {
        if (this.securityService.user) {
            this.toastCtrl.create({
                message: '用户已登录，请先退出！',
                duration: 2000,
                position: 'top'
            }).present();
            return;
        }
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
    };
    SettingPage.prototype.goPassword = function () {
        if (!this.securityService.user) {
            this.toastCtrl.create({
                message: '用户未登录，请先登录！',
                duration: 2000,
                position: 'top'
            }).present();
            return;
        }
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__password_password__["a" /* PasswordPage */]);
    };
    SettingPage.prototype.goSign = function () {
        // if(!this.securityService.user){
        //   this.toastCtrl.create({
        //     message: '用户未登录，请先登录！',
        //     duration: 2000,
        //     position: 'top'
        //   }).present();
        //   return;
        // }
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__sign_sign__["a" /* SignPage */]);
    };
    SettingPage.prototype.goTrip = function () {
        // if(!this.securityService.user){
        //   this.toastCtrl.create({
        //     message: '用户未登录，请先登录！',
        //     duration: 2000,
        //     position: 'top'
        //   }).present();
        //   return;
        // }
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__trip_trip__["a" /* TripPage */]);
    };
    SettingPage.prototype.goLeave = function () {
        // if(!this.securityService.user){
        //   this.toastCtrl.create({
        //     message: '用户未登录，请先登录！',
        //     duration: 2000,
        //     position: 'top'
        //   }).present();
        //   return;
        // }
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__leave_leave__["a" /* LeavePage */]);
    };
    SettingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-setting',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\setting\setting.html"*/'<ion-content class="bg-color">\n\n\n\n    <ion-list class="top-list">\n\n        <button ion-item *ngIf="securityService.user">\n\n            <ion-avatar item-left>\n\n                <img src="images/avatar-ts-jessie.png" />\n\n            </ion-avatar>\n\n            {{securityService.user.userName}}\n\n        </button>\n\n        <button ion-item *ngIf="!securityService.user" (click)=\'goLogin()\'>\n\n            系统登录\n\n            <ion-icon item-right ios="ios-arrow-dropright" \n\n                style="color:#488aff" md="md-arrow-dropright"></ion-icon>\n\n        </button>\n\n    </ion-list>\n\n\n\n    <ion-list *ngIf="securityService.user">\n\n        <button ion-item (click)=\'goPassword()\'>\n\n            修改密码\n\n            <ion-icon item-right ios="ios-arrow-dropright" \n\n                style="color:#488aff" md="md-arrow-dropright"></ion-icon>\n\n        </button>\n\n    </ion-list>\n\n\n\n    <ion-list>\n\n      <button ion-item (click)=\'goSign()\'>\n\n        签到管理\n\n        <ion-icon item-right ios="ios-arrow-dropright"\n\n                  style="color:#488aff" md="md-arrow-dropright"></ion-icon>\n\n      </button>\n\n      <button ion-item (click)=\'goLeave()\'>\n\n        出差管理\n\n        <ion-icon item-right ios="ios-arrow-dropright"\n\n                  style="color:#488aff" md="md-arrow-dropright"></ion-icon>\n\n      </button>\n\n      <button ion-item (click)=\'goTrip()\'>\n\n        请假管理\n\n        <ion-icon item-right ios="ios-arrow-dropright"\n\n                  style="color:#488aff" md="md-arrow-dropright"></ion-icon>\n\n      </button>\n\n    </ion-list>\n\n\n\n    <ion-list>\n\n        <button ion-item>\n\n            版本号\n\n            <span item-end>V1.1</span>\n\n        </button>\n\n        <!--<button ion-item (click)=\'goAbout()\'>\n\n            关于我们\n\n            <ion-icon item-right ios="ios-arrow-dropright" \n\n                style="color:#488aff" md="md-arrow-dropright"></ion-icon>\n\n        </button>\n\n        <button ion-item (click)=\'goHelp()\'>\n\n            系统帮助\n\n            <ion-icon item-right ios="ios-arrow-dropright" \n\n                style="color:#488aff" md="md-arrow-dropright"></ion-icon>\n\n        </button>-->\n\n    </ion-list>\n\n\n\n    <div style="text-align: center; margin-left: 30px; margin-right: 30px;margin-top: 30px;"\n\n        *ngIf="securityService.user">\n\n        <button ion-button block (click)=\'securityService.logout()\'>\n\n            退出登录\n\n        </button>\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\setting\setting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_9__app_core_security_SecurityService__["a" /* SecurityService */]])
    ], SettingPage);
    return SettingPage;
}());

//# sourceMappingURL=setting.js.map

/***/ }),

/***/ 405:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(410);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
var user = {}; //定义全局用户
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 410:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_cookie__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_cookie___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ngx_cookie__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ion_multi_picker__ = __webpack_require__(730);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ion_multi_picker___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ion_multi_picker__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(733);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__core_dictionary_DictionaryService__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__core_data_FileService__ = __webpack_require__(734);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__form_FormObject__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__bpm_BpmObject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__form_AutoresizeDirective__ = __webpack_require__(735);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__buiness_CourseHttp__ = __webpack_require__(736);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__buiness_UserExam__ = __webpack_require__(737);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__utils_DateUtil__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__utils_ImageUtil__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__utils_AsLength__ = __webpack_require__(738);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__utils_AsDateExtend__ = __webpack_require__(739);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__utils_AsDate__ = __webpack_require__(740);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__utils_AsImage__ = __webpack_require__(741);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__utils_AsTime__ = __webpack_require__(742);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__core_security_SecurityDataService__ = __webpack_require__(341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_tabs_tabs__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_todo_todo__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_done_done__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_task_task__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_jobs_jobs__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_me_me__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_process_process__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_leave_new_leave_new__ = __webpack_require__(351);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_calendar_calendar__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_approval_approval__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_home_home__ = __webpack_require__(743);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_setting_setting__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_about_about__ = __webpack_require__(347);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_help_help__ = __webpack_require__(348);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_password_password__ = __webpack_require__(349);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_sign_sign__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_bpmsign_bpmsign__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_bpmback_bpmback__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_bpmimg_bpmimg__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_bpmprocess_bpmprocess__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_trip_trip__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__pages_leave_leave__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__pages_vehicle_vehicle__ = __webpack_require__(352);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__pages_vehicleannal_vehicleannal__ = __webpack_require__(353);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__pages_approve_approve__ = __webpack_require__(744);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__pages_ccgl_ccgl__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__pages_qjgl_qjgl__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__pages_rcap_rcap__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__pages_swgl_swgl__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__pages_wfbd_wfbd__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__pages_wlkyxm_wlkyxm__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__pages_ycsq_ycsq__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__pages_yzgl_yzgl__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__pages_zysq_zysq__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__pages_wjry_wjry__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__pages_kyxm_kyxm__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__pages_adduser_adduser__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65__pages_sign_new_sign_new__ = __webpack_require__(350);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_66__pages_swgl_new_swgl_new__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_67__pages_fwgl_fwgl__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_68__ionic_native_camera__ = __webpack_require__(345);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_69__ionic_native_keyboard__ = __webpack_require__(745);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_70__app_buiness_MajorHttp__ = __webpack_require__(746);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_71__app_buiness_SignHttp__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_72__ionic_native_file_transfer__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_73__ionic_native_file__ = __webpack_require__(344);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_74__ionic_native_image_picker__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_75_ionic_img_viewer__ = __webpack_require__(747);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








//import { Camera } from '@ionic-native/camera';
//import { BarcodeScanner } from '@ionic-native/barcode-scanner';








//CoureInfo




























































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_20__utils_AsLength__["a" /* AsLength */],
                __WEBPACK_IMPORTED_MODULE_21__utils_AsDateExtend__["a" /* AsDateExtend */],
                __WEBPACK_IMPORTED_MODULE_22__utils_AsDate__["a" /* AsDate */],
                __WEBPACK_IMPORTED_MODULE_23__utils_AsImage__["a" /* AsImage */],
                __WEBPACK_IMPORTED_MODULE_24__utils_AsTime__["a" /* AsTime */],
                __WEBPACK_IMPORTED_MODULE_27__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_todo_todo__["a" /* TodoPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_done_done__["a" /* DonePage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_task_task__["a" /* TaskPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_jobs_jobs__["a" /* JobsPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_me_me__["a" /* MePage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_process_process__["a" /* ProcessPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_leave_new_leave_new__["a" /* LeaveNewPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_calendar_calendar__["a" /* CalendarPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_approval_approval__["a" /* ApprovalPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_setting_setting__["a" /* SettingPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_approve_approve__["a" /* ApprovePage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_help_help__["a" /* HelpPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_password_password__["a" /* PasswordPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_sign_sign__["a" /* SignPage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_ccgl_ccgl__["a" /* CcglPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_qjgl_qjgl__["a" /* QjglPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_bpmsign_bpmsign__["a" /* BpmSignPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_bpmback_bpmback__["a" /* BpmBackPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_bpmimg_bpmimg__["a" /* BpmImgPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_bpmprocess_bpmprocess__["a" /* BpmProcessPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_trip_trip__["a" /* TripPage */],
                __WEBPACK_IMPORTED_MODULE_49__pages_leave_leave__["a" /* LeavePage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_vehicle_vehicle__["a" /* VehiclePage */],
                __WEBPACK_IMPORTED_MODULE_51__pages_vehicleannal_vehicleannal__["a" /* VehicleannalPage */],
                __WEBPACK_IMPORTED_MODULE_55__pages_rcap_rcap__["a" /* RcapPage */],
                __WEBPACK_IMPORTED_MODULE_56__pages_swgl_swgl__["a" /* SwglPage */],
                __WEBPACK_IMPORTED_MODULE_57__pages_wfbd_wfbd__["a" /* WfbdPage */],
                __WEBPACK_IMPORTED_MODULE_58__pages_wlkyxm_wlkyxm__["a" /* WlkyxmPage */],
                __WEBPACK_IMPORTED_MODULE_59__pages_ycsq_ycsq__["a" /* YcsqPage */],
                __WEBPACK_IMPORTED_MODULE_60__pages_yzgl_yzgl__["a" /* YzglPage */],
                __WEBPACK_IMPORTED_MODULE_61__pages_zysq_zysq__["a" /* ZysqPage */],
                __WEBPACK_IMPORTED_MODULE_62__pages_wjry_wjry__["a" /* WjryPage */],
                __WEBPACK_IMPORTED_MODULE_63__pages_kyxm_kyxm__["a" /* KyxmPage */],
                __WEBPACK_IMPORTED_MODULE_64__pages_adduser_adduser__["a" /* AddUserPage */],
                __WEBPACK_IMPORTED_MODULE_65__pages_sign_new_sign_new__["a" /* SignNewPage */],
                __WEBPACK_IMPORTED_MODULE_66__pages_swgl_new_swgl_new__["a" /* SwglNewPage */],
                __WEBPACK_IMPORTED_MODULE_67__pages_fwgl_fwgl__["a" /* FwglPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_75_ionic_img_viewer__["a" /* IonicImageViewerModule */],
                __WEBPACK_IMPORTED_MODULE_5_ion_multi_picker__["MultiPickerModule"],
                __WEBPACK_IMPORTED_MODULE_4_ngx_cookie__["CookieModule"].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */], {
                    mode: 'ios',
                    backButtonText: '',
                    statusbarPadding: true,
                    tabsHideOnSubPages: 'true' //隐藏全部子页面tabs
                }, {
                    links: [
                        { loadChildren: '../pages/approval/approval.module#ApprovalPageModule', name: 'ApprovalPage', segment: 'approval', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/bpmprocess/bpmprocess.module#BpmProcessPageModule', name: 'BpmProcessPage', segment: 'bpmprocess', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/calendar/calendar.module#CalendarPageModule', name: 'CalendarPage', segment: 'calendar', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/ccgl/ccgl.module#CcglPageModule', name: 'CcglPage', segment: 'ccgl', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/done/done.module#DonePageModule', name: 'DonePage', segment: 'done', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/me/me.module#MePageModule', name: 'MePage', segment: 'me', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/process/process.module#ProcessPageModule', name: 'ProcessPage', segment: 'process', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/setting/setting.module#SettingPageModule', name: 'SettingPage', segment: 'setting', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/task/task.module#TaskPageModule', name: 'TaskPage', segment: 'task', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/jobs/jobs.module#JobsPageModule', name: 'JobsPage', segment: 'jobs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/todo/todo.module#TodoPageModule', name: 'TodoPage', segment: 'todo', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_27__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_todo_todo__["a" /* TodoPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_done_done__["a" /* DonePage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_task_task__["a" /* TaskPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_jobs_jobs__["a" /* JobsPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_me_me__["a" /* MePage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_process_process__["a" /* ProcessPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_leave_new_leave_new__["a" /* LeaveNewPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_calendar_calendar__["a" /* CalendarPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_approval_approval__["a" /* ApprovalPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_setting_setting__["a" /* SettingPage */],
                __WEBPACK_IMPORTED_MODULE_52__pages_approve_approve__["a" /* ApprovePage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_help_help__["a" /* HelpPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_password_password__["a" /* PasswordPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_sign_sign__["a" /* SignPage */],
                __WEBPACK_IMPORTED_MODULE_53__pages_ccgl_ccgl__["a" /* CcglPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_qjgl_qjgl__["a" /* QjglPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_bpmsign_bpmsign__["a" /* BpmSignPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_bpmback_bpmback__["a" /* BpmBackPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_bpmimg_bpmimg__["a" /* BpmImgPage */],
                __WEBPACK_IMPORTED_MODULE_47__pages_bpmprocess_bpmprocess__["a" /* BpmProcessPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_trip_trip__["a" /* TripPage */],
                __WEBPACK_IMPORTED_MODULE_49__pages_leave_leave__["a" /* LeavePage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_vehicle_vehicle__["a" /* VehiclePage */],
                __WEBPACK_IMPORTED_MODULE_51__pages_vehicleannal_vehicleannal__["a" /* VehicleannalPage */],
                __WEBPACK_IMPORTED_MODULE_55__pages_rcap_rcap__["a" /* RcapPage */],
                __WEBPACK_IMPORTED_MODULE_56__pages_swgl_swgl__["a" /* SwglPage */],
                __WEBPACK_IMPORTED_MODULE_57__pages_wfbd_wfbd__["a" /* WfbdPage */],
                __WEBPACK_IMPORTED_MODULE_58__pages_wlkyxm_wlkyxm__["a" /* WlkyxmPage */],
                __WEBPACK_IMPORTED_MODULE_59__pages_ycsq_ycsq__["a" /* YcsqPage */],
                __WEBPACK_IMPORTED_MODULE_60__pages_yzgl_yzgl__["a" /* YzglPage */],
                __WEBPACK_IMPORTED_MODULE_61__pages_zysq_zysq__["a" /* ZysqPage */],
                __WEBPACK_IMPORTED_MODULE_62__pages_wjry_wjry__["a" /* WjryPage */],
                __WEBPACK_IMPORTED_MODULE_63__pages_kyxm_kyxm__["a" /* KyxmPage */],
                __WEBPACK_IMPORTED_MODULE_64__pages_adduser_adduser__["a" /* AddUserPage */],
                __WEBPACK_IMPORTED_MODULE_65__pages_sign_new_sign_new__["a" /* SignNewPage */],
                __WEBPACK_IMPORTED_MODULE_66__pages_swgl_new_swgl_new__["a" /* SwglNewPage */],
                __WEBPACK_IMPORTED_MODULE_67__pages_fwgl_fwgl__["a" /* FwglPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
                //Camera,相机暂时没有找到
                //BarcodeScanner,
                __WEBPACK_IMPORTED_MODULE_9__core_data_BaseService__["a" /* BaseService */],
                __WEBPACK_IMPORTED_MODULE_10__core_dictionary_DictionaryService__["a" /* DictionaryService */],
                __WEBPACK_IMPORTED_MODULE_11__core_data_FileService__["a" /* FileService */],
                __WEBPACK_IMPORTED_MODULE_12__core_data_DataService__["a" /* DataService */],
                __WEBPACK_IMPORTED_MODULE_13__form_FormObject__["a" /* FormObject */],
                __WEBPACK_IMPORTED_MODULE_14__bpm_BpmObject__["a" /* BpmObject */],
                __WEBPACK_IMPORTED_MODULE_15__form_AutoresizeDirective__["a" /* AutoresizeDirective */],
                __WEBPACK_IMPORTED_MODULE_25__core_security_SecurityDataService__["a" /* SecurityDataService */],
                __WEBPACK_IMPORTED_MODULE_26__core_security_SecurityService__["a" /* SecurityService */],
                __WEBPACK_IMPORTED_MODULE_16__buiness_CourseHttp__["a" /* CourseHttp */],
                __WEBPACK_IMPORTED_MODULE_17__buiness_UserExam__["a" /* UserExam */],
                __WEBPACK_IMPORTED_MODULE_18__utils_DateUtil__["a" /* DateUtil */],
                __WEBPACK_IMPORTED_MODULE_19__utils_ImageUtil__["a" /* ImageUtil */],
                __WEBPACK_IMPORTED_MODULE_69__ionic_native_keyboard__["a" /* Keyboard */],
                __WEBPACK_IMPORTED_MODULE_70__app_buiness_MajorHttp__["a" /* MajorHttp */],
                __WEBPACK_IMPORTED_MODULE_71__app_buiness_SignHttp__["a" /* SignHttp */],
                __WEBPACK_IMPORTED_MODULE_73__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_72__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_72__ionic_native_file_transfer__["b" /* FileTransferObject */],
                __WEBPACK_IMPORTED_MODULE_68__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_74__ionic_native_image_picker__["a" /* ImagePicker */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 49:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DateUtil; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// 基础服务类，提供基础路径
var DateUtil = /** @class */ (function () {
    function DateUtil() {
    }
    /**
     * 获取固定时间格式
     * @param input
     */
    DateUtil.prototype.getDateFormate = function (input) {
        if (input == null) {
            return '';
        }
        var dType = typeof (input);
        var date = this.parseDate(input, dType);
        if (date) {
            var y = date.getFullYear();
            var m = date.getMonth() + 1;
            var d = date.getDate();
            var result = "";
            result = y + '/' + m + '/' + d;
            return result;
        }
        else if (dType == 'string') {
            var array = input.match(/\d+/g);
            input = array[0] + '/' + array[1] + '/' + array[2];
            return input;
        }
        return '';
    };
    /**
    * 获取固定时间格式
    * @param input
    */
    DateUtil.prototype.getDateFormateCommon = function (input) {
        if (input == null) {
            return '';
        }
        var dType = typeof (input);
        var date = this.parseDate(input, dType);
        if (date) {
            var y = date.getFullYear();
            var m = date.getMonth() + 1;
            var d = date.getDate();
            var result = "";
            result = y + '-' + m + '-' + d;
            return result;
        }
        else if (dType == 'string') {
            var array = input.match(/\d+/g);
            input = array[0] + '-' + array[1] + '-' + array[2];
            return input;
        }
        return '';
    };
    /**
     * 获取固定时间格式
     * @param input
     */
    DateUtil.prototype.getDateFormateExtend = function (input) {
        if (input == null) {
            return '';
        }
        var dType = typeof (input);
        var date = this.parseDate(input, dType);
        if (date) {
            var y = date.getFullYear();
            var m = date.getMonth() + 1;
            var d = date.getDate();
            var hours = date.getHours();
            var min = date.getMinutes();
            var second = date.getSeconds();
            var result = "";
            result = y + '/' + m + '/' + d + " " + hours + ":" + min;
            return result;
        }
        else if (dType == 'string') {
            var array = input.match(/\d+/g);
            input = array[0] + '/' + array[1] + '/' + array[2] + " " + array[3] + ":" + array[4];
            ;
            return input;
        }
        return '';
    };
    /**
     * 解析数据
     * @param input
     * @param dType
     */
    DateUtil.prototype.parseDate = function (input, dType) {
        try {
            var date = null;
            if (dType == 'date') {
                date = input;
            }
            else if (dType == 'object') {
                if (input.hasOwnProperty('time')) {
                    date = new Date(input.time);
                }
                else {
                    return null;
                }
            }
            else if (dType == 'string') {
                if (input.indexOf("-") > -1) {
                    date = new Date(input.replace(/-/, "/"));
                }
                else {
                    date = new Date(Number(input));
                }
                // date = new Date(Number(input));
            }
            else if (dType == 'number') {
                date = new Date(input);
            }
            else if (dType == "undefined") {
                return "";
            }
            return date;
        }
        catch (e) {
            return null;
        }
    };
    ;
    DateUtil = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], DateUtil);
    return DateUtil;
}());

//# sourceMappingURL=DateUtil.js.map

/***/ }),

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CcglPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var CcglPage = /** @class */ (function () {
    function CcglPage(navCtrl, navParams, dataService, baseSErvice, alert, securityService, changeDetectorRef, loadingCtrl, actionSheetCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseSErvice = baseSErvice;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.user = {};
        this.gzccgl = new __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__["a" /* BpmObject */](this.dataService, this.baseSErvice, this.changeDetectorRef, this.navCtrl, this.alert, this.loadingCtrl, "gzccgl");
        if (this.navParams.data != null && this.navParams.data["bk"] != null) {
            this.gzccgl.editRow = {};
            this.gzccgl.sn = "gzccgl";
            this.gzccgl.bpmKey = this.navParams.data["bk"];
            this.gzccgl.id = this.navParams.data["dk"];
        }
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                _this.gzccgl.editRow.ccrid = _this.securityService.user["userid"];
                if (_this.navParams.data != null && _this.navParams.data["bk"] != null) {
                    _this.gzccgl.findWork(null, null);
                }
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.gzccgl.editRow.ccrid = this.securityService.user["userid"];
            if (this.navParams.data != null && this.navParams.data["bk"] != null) {
                this.gzccgl.findWork(null, null);
            }
        }
    }
    CcglPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    CcglPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    CcglPage.prototype.goSignPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__["a" /* BpmSignPage */], this.gzccgl);
    };
    ;
    CcglPage.prototype.goBackPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__["a" /* BpmBackPage */], this.gzccgl);
    };
    ;
    CcglPage.prototype.goProcessPage = function () {
        var _this = this;
        //this.navCtrl.push(BpmProcessPage,this.gzccgl);
        var actionSheet = this.actionSheetCtrl.create({
            title: '更多',
            buttons: [
                {
                    text: '办理过程',
                    handler: function () {
                        //console.log('Destructive clicked');
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__["a" /* BpmProcessPage */], _this.gzccgl);
                    }
                }, {
                    text: '流程图',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__["a" /* BpmImgPage */], _this.gzccgl);
                    }
                }, {
                    text: '取消',
                    role: '取消',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ;
    CcglPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-ccgl',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\ccgl\ccgl.html"*/'<!--\n\n  Generated template for the ApprovalPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>出差管理</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">申请信息</span>\n\n    </ion-list-header>\n\n    <ion-item>\n\n        <span class="bhq-title">申请人: </span>\n\n        <span class="bhq-content">{{gzccgl.editRow.ccrxm}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">职务: </span>\n\n        <span class="bhq-content">{{gzccgl.editRow.ccrzw}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">目的地: </span>\n\n        <span class="bhq-content">{{gzccgl.editRow.mdd}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">外出事由: </span>\n\n        <span class="bhq-content">{{gzccgl.editRow.wcsy}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">开始时间: </span>\n\n        <span class="bhq-content">{{gzccgl.editRow.kssj | asDate}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">结束时间: </span>\n\n        <span class="bhq-content">{{gzccgl.editRow.jssj | asDate}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">出行方式: </span>\n\n        <span class="bhq-content">{{gzccgl.editRow.cxfsname}}</span>\n\n        <!-- <ion-checkbox [(ngModel)]="gzccgl.editRow.cxfsfj" disabled="true"></ion-checkbox>\n\n        <ion-checkbox [(ngModel)]="gzccgl.editRow.cxfshc" disabled="true"></ion-checkbox>\n\n        <ion-checkbox [(ngModel)]="gzccgl.editRow.cxfsdwpc" disabled="true"></ion-checkbox>\n\n        <ion-checkbox [(ngModel)]="gzccgl.editRow.cxfsqc" disabled="true"></ion-checkbox>\n\n        <ion-checkbox [(ngModel)]="gzccgl.editRow.cxfszdc" disabled="true"></ion-checkbox>\n\n        <ion-checkbox [(ngModel)]="gzccgl.editRow.cxfsqt" disabled="true"></ion-checkbox> -->\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">申请日期: </span>\n\n        <span class="bhq-content">{{gzccgl.editRow.sqrq | asDate}}</span>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">同行出差人</span>\n\n    </ion-list-header>\n\n    <ion-item *ngFor="let item of gzccgl.editRow.ccsxry">\n\n      <ion-label class="bhq-content">{{item.jsrmc}}({{item.jsrjgmc}})</ion-label>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list" *ngIf="gzccgl.editRow.bpmkey==\'ccgl\'">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab2">科室审批</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzccgl.process.usertask2!=null">\n\n      <div *ngFor="let item of  gzccgl.process.usertask2.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzccgl.process.usertask2.editRow!=null && gzccgl.proauth.kshs  && gzccgl.process.usertask2.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzccgl.process.usertask2.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n<!--\n\n  <ion-list class="approval-list" *ngIf="gzccgl.editRow.bpmkey==\'ccgl\' || gzccgl.editRow.bpmkey==\'ccgl_kjgb\'">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab5">办公室主任审批</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzccgl.process.usertask5!=null">\n\n      <div *ngFor="let item of  gzccgl.process.usertask5.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzccgl.process.usertask5.editRow!=null \n\n          && gzccgl.proauth.bgszrsp && gzccgl.process.usertask5.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzccgl.process.usertask5.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>-->\n\n\n\n  <ion-list class="approval-list" *ngIf="gzccgl.editRow.bpmkey!=\'ccgl_jjgb\' && gzccgl.editRow.bpmkey!=\'ccgl_jz\'">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab3">分管领导审批</span>\n\n    </ion-list-header>\n\n\n\n    <div *ngIf="gzccgl.process.usertask3!=null">\n\n      <div *ngFor="let item of  gzccgl.process.usertask3.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzccgl.process.usertask3.editRow!=null \n\n        && gzccgl.proauth.fgldsh \n\n        && gzccgl.process.usertask3.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzccgl.process.usertask3.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list" *ngIf="gzccgl.editRow.bpmkey!=\'ccgl_jz\'">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab4">局长审核</span>\n\n    </ion-list-header>\n\n\n\n    <div *ngIf="gzccgl.process.usertask4!=null">\n\n      <div *ngFor="let item of  gzccgl.process.usertask4.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzccgl.process.usertask4.editRow!=null && gzccgl.proauth.ldsh && gzccgl.process.usertask4.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzccgl.process.usertask4.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n</ion-content>\n\n\n\n<ion-footer class="bhq-bpm-button">\n\n  <div class="footer-btn" *ngIf="gzccgl.auth.opt_th" (click)="gzccgl.getBackTask(null,null);">\n\n    <ion-icon name="arrow-dropleft-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>退回</p>\n\n  </div>\n\n  <div class="footer-btn" *ngIf="gzccgl.auth.opt_bc" (click)="gzccgl.bpmSave(null);">\n\n    <ion-icon name="lock" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>保存</p>\n\n  </div>\n\n  <div class="footer-btn" *ngIf="gzccgl.auth.opt_tj" (click)="gzccgl.getNextTask(null,null);">\n\n    <ion-icon name="arrow-dropright-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>通过</p>\n\n  </div>\n\n  <!-- <div class="footer-more" (click)="goProcessPage();">...</div> -->\n\n  <div class="footer-btn" (click)="goProcessPage();">\n\n      <ion-icon name="keypad" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n      <p>更多</p>\n\n  </div>\n\n</ion-footer>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\ccgl\ccgl.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"]])
    ], CcglPage);
    return CcglPage;
}());

//# sourceMappingURL=ccgl.js.map

/***/ }),

/***/ 6:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaseService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_cookie__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_cookie___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ngx_cookie__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// 基础服务类，提供基础路径
var BaseService = /** @class */ (function () {
    function BaseService(errorHandler, _cookieService) {
        this.errorHandler = errorHandler;
        this._cookieService = _cookieService;
        // 当前sso的服务，和web前端地址一至
        this.service = "http://192.168.10.107:8017/";
        // 附件服务器地址，和web前端地址一至
        this.fileUrl = "http://192.168.10.107:8017/";
        // 基础数据服务地址，为tomcat服务端地址，不带/rest
        this.hostUrl = "http://192.168.10.107:9090/bhqoa";
        // 数据服务地址，为tomcat服务端地址
        this.baseUrl = "http://192.168.10.107:9090/bhqoa/rest";
    }
    BaseService.prototype.httpOptions = function () {
        var headers = null;
        var temptest = this._cookieService.get('jwt');
        if (temptest) {
            headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]({ jwt: temptest });
        }
        else {
            headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]();
        }
        headers.append('Content-Type', 'application/json;charset=utf-8');
        return headers;
    };
    ;
    // 将对象转换为json字符串
    BaseService.prototype.toJSONStr = function (obj) {
        if (obj == null)
            return '';
        if (typeof obj === 'string') {
            return obj;
        }
        if (typeof obj === 'object') {
            return JSON.stringify(obj);
        }
        return '';
    };
    // 错误处理
    BaseService.prototype.handleError = function (result) {
        var _this = this;
        return function (error) {
            _this.errorHandler.handleError(error);
            var data = new __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"](function (observer) {
                setTimeout(function () {
                    observer.next(result);
                }, -1);
            });
            return data;
        };
    };
    BaseService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ErrorHandler"],
            __WEBPACK_IMPORTED_MODULE_3_ngx_cookie__["CookieService"]])
    ], BaseService);
    return BaseService;
}());

//# sourceMappingURL=BaseService.js.map

/***/ }),

/***/ 7:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SecurityService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_cookie__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_cookie___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_cookie__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__SecurityDataService__ = __webpack_require__(341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__data_BaseService__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// 登录服务
var SecurityService = /** @class */ (function () {
    function SecurityService(_cookieService, loadingCtrl, toastCtrl, securityDataService, baseService) {
        this._cookieService = _cookieService;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.securityDataService = securityDataService;
        this.baseService = baseService;
        // 当前用户
        this.user = {};
        // 应用服务权限
        this.si = {};
        // 菜单权限
        this.mi = {};
        /**
         * 显示消息
         */
        this.showMessage = true;
    }
    // 初始化
    SecurityService.prototype.init = function () {
        var _this = this;
        // 查询一下，看是不是有用户登录过
        this.getUserInfo(null, function () {
            // 查询一下tgt是否存在
            var tgt = _this._cookieService.get("jwt");
            if (tgt)
                _this.innerLogin(tgt);
        });
    };
    // 获取当前用户信息
    SecurityService.prototype.getUserInfo = function (suc, fai) {
        var _this = this;
        this.securityDataService.getUserInfo().subscribe(function (ud) {
            if (ud && ud.user) {
                _this.user = ud.user;
                if (ud.services) {
                    //this.si=new Object();
                    var temp_1 = {};
                    ud.services.forEach(function (e) {
                        temp_1[e] = true;
                        console.log(e);
                    });
                    _this.si = temp_1;
                }
                if (ud.menus) {
                    _this.mi = new Object();
                    var temp_2 = {};
                    ud.menus.forEach(function (e) {
                        temp_2[e] = true;
                    });
                    _this.mi = temp_2;
                }
                if (suc)
                    suc();
            }
            else {
                _this.user = {};
                _this.mi = {};
                _this.si = {};
                if (fai)
                    fai();
            }
        });
    };
    // 系统登录
    SecurityService.prototype.login = function (username, password, logined) {
        var _this = this;
        if (!username || username == null || username === '') {
            if (this.showMessage) {
                this.toastCtrl.create({
                    message: '用户名不能为空！',
                    duration: 2000,
                    position: 'bottom'
                }).present();
            }
            return;
        }
        if (!password || password == null || password === '') {
            if (this.showMessage) {
                this.toastCtrl.create({
                    message: '密码不能为空！',
                    duration: 2000,
                    position: 'bottom'
                }).present();
            }
            return;
        }
        var loader = this.loadingCtrl.create({
            content: "登录中..."
        });
        loader.present();
        this._cookieService.removeAll();
        // 创建tgt
        this.securityDataService.loginAjax(username, password).subscribe(function (td) {
            if (!td.ok) {
                loader.dismiss();
                if (_this.showMessage) {
                    _this.toastCtrl.create({
                        message: '用户密码错误！',
                        duration: 2000,
                        position: 'bottom'
                    }).present();
                }
                return;
            }
            var tgt = td.ok;
            // tgt保存120分钟
            var cDate = new Date();
            cDate.setMinutes(cDate.getMinutes() + 120, cDate.getSeconds(), 0);
            _this._cookieService.put("SSO_TGT", tgt, { expires: cDate });
            _this.innerLogin(tgt, loader, logined);
        });
    };
    SecurityService.prototype.innerLogin = function (tgt, loader, logined) {
        var _this = this;
        // 创建st
        var service = this.baseService.service;
        this.securityDataService.serviceTicketsAjax(tgt, service).subscribe(function (sd) {
            if (!sd.ok) {
                if (loader)
                    loader.dismiss();
                if (_this.showMessage) {
                    _this.toastCtrl.create({
                        message: '用户密码错误！',
                        duration: 2000,
                        position: 'bottom'
                    }).present();
                }
                return;
            }
            var ticket = sd.ok;
            // 模拟登录
            _this.securityDataService.mklogin(ticket).subscribe(function (jsd) {
                if (loader)
                    loader.dismiss();
                if (jsd) {
                    // 获取当前用户
                    var cDate = new Date();
                    cDate.setMinutes(cDate.getMinutes() + 120, cDate.getSeconds(), 0);
                    _this._cookieService.put("jwt", jsd.jwt, { expires: cDate });
                    _this.getUserInfo(logined);
                }
                else {
                    if (_this.showMessage) {
                        _this.toastCtrl.create({
                            message: '用户密码错误！',
                            duration: 2000,
                            position: 'bottom'
                        }).present();
                    }
                }
            });
        });
    };
    // 系统退出
    SecurityService.prototype.logout = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "退出中..."
        });
        loader.present();
        this.securityDataService.loginOut().subscribe(function () {
            _this.user = null;
            _this._cookieService.removeAll();
            loader.dismiss();
            window.localStorage.setItem('bhqoa_initLogin', "false");
        });
    };
    SecurityService.prototype.isPoneAvailable = function (str) {
        var myreg = /^[1][3,4,5,7,8][0-9]{9}$/;
        return myreg.test(str);
    };
    // 用户注册信息
    SecurityService.prototype.userRegister = function (ac, pw, rpw, registed) {
        var _this = this;
        if (!ac || ac == null || ac === '') {
            this.toastCtrl.create({
                message: '用户名不能为空！',
                duration: 2000,
                position: 'bottom'
            }).present();
            return;
        }
        if (!this.isPoneAvailable(ac)) {
            this.toastCtrl.create({
                message: '请输入正确的手机号码！',
                duration: 2000,
                position: 'bottom'
            }).present();
            return;
        }
        if (!pw || pw == null || pw === ''
            || !rpw || rpw == null || rpw === ''
            || pw != rpw) {
            this.toastCtrl.create({
                message: '密码为空，或不一至！',
                duration: 2000,
                position: 'bottom'
            }).present();
            return;
        }
        if (pw.length < 6) {
            this.toastCtrl.create({
                message: '密码长度不能少于6位！',
                duration: 2000,
                position: 'bottom'
            }).present();
            return;
        }
        var loader = this.loadingCtrl.create({
            content: "注册中..."
        });
        loader.present();
        this.securityDataService.userRegister(ac, pw, rpw).subscribe(function (ok) {
            // 注册成功，就登录
            if (ok == true) {
                loader.dismiss();
                _this.login(ac, pw, registed);
            }
            else {
                _this.toastCtrl.create({
                    message: '注册失败：帐号已存在！',
                    duration: 2000,
                    position: 'bottom'
                }).present();
                loader.dismiss();
            }
        });
    };
    // 设置当前密码
    SecurityService.prototype.setPassword = function (pw, rpw, success) {
        var _this = this;
        if (!pw || pw == null || pw === ''
            || !rpw || rpw == null || rpw === ''
            || pw != rpw) {
            this.toastCtrl.create({
                message: '密码为空，或不一至！',
                duration: 2000,
                position: 'bottom'
            }).present();
            return;
        }
        var loader = this.loadingCtrl.create({
            content: "设置中..."
        });
        loader.present();
        this.securityDataService.setPassword(pw, rpw).subscribe(function (ok) {
            // 修改成功
            if (ok == true) {
                loader.dismiss();
                if (success)
                    success();
            }
            else {
                _this.toastCtrl.create({
                    message: '设置失败，请重新登录系统后重试！',
                    duration: 2000,
                    position: 'bottom'
                }).present();
                loader.dismiss();
            }
        });
    };
    SecurityService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ngx_cookie__["CookieService"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_3__SecurityDataService__["a" /* SecurityDataService */],
            __WEBPACK_IMPORTED_MODULE_4__data_BaseService__["a" /* BaseService */]])
    ], SecurityService);
    return SecurityService;
}());

//# sourceMappingURL=SecurityService.js.map

/***/ }),

/***/ 733:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_core_security_SecurityService__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = /** @class */ (function () {
    function MyApp(appCtrl, platform, statusBar, splashScreen, securityService, toastCtrl) {
        var _this = this;
        this.appCtrl = appCtrl;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.securityService = securityService;
        this.toastCtrl = toastCtrl;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        //用于判断返回键是否触发
        this.backButtonPressed = false;
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            statusBar.overlaysWebView(true);
            // statusBar.backgroundColorByHexString('#5276b2');
            try {
                var username = window.localStorage.getItem('bhqoa_username');
                var password = window.localStorage.getItem('bhqoa_password');
                var initLogin = window.localStorage.getItem('bhqoa_initLogin');
                if (initLogin == "true" && username != null && username != "" && password != null && password != "") {
                    _this.securityService.showMessage = false;
                    _this.securityService.login(username, password, function (data) {
                        _this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
                    });
                }
            }
            catch (e) {
            }
        });
        this.registerBackButtonAction();
    }
    MyApp.prototype.registerBackButtonAction = function () {
        var _this = this;
        this.platform.registerBackButtonAction(function () {
            // 控制modal、系统自带提示框
            var overlay = _this.appCtrl._appRoot._overlayPortal.getActive() || _this.appCtrl._appRoot._modalPortal.getActive();
            if (overlay) {
                overlay.dismiss();
                return;
            }
            var activeVC = _this.nav.getActive();
            var page = activeVC.instance;
            if (page.tabs) {
                var activeNav = page.tabs.getSelected();
                if (activeNav.canGoBack()) {
                    return activeNav.pop();
                }
                else {
                    return _this.showExit();
                }
            }
            //如果是登录页面的返回事件，直接退出系统
            if (page instanceof __WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]) {
                return _this.showExit();
            }
            //剩余的情况全部使用全局路由进行操作
            _this.appCtrl.getActiveNav().pop();
        }, 1);
    };
    //双击退出提示框
    MyApp.prototype.showExit = function () {
        var _this = this;
        //当触发标志为true时，即2秒内双击返回按键则退出APP
        if (this.backButtonPressed) {
            this.platform.exitApp();
        }
        else {
            this.toastCtrl.create({
                message: '再按一次退出应用',
                duration: 2000,
                position: 'center'
            }).present();
            this.backButtonPressed = true;
            //2秒内没有再次点击返回则将触发标志标记为false
            setTimeout(function () { return _this.backButtonPressed = false; }, 2000);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('myNav'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: '<ion-nav #myNav [root]="rootPage"></ion-nav>'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["App"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_6__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 734:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FileService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__BaseService__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// 文件处理服务类
var FileService = /** @class */ (function () {
    function FileService(baseService) {
        this.baseService = baseService;
        // 下载路径
        this.downloadUrl = '/file/plupload/download';
    }
    // 转换下载文件
    FileService.prototype.getFiles = function (file) {
        var _this = this;
        // plupload:3tvb3mb.png:zs-banner1.png|plupload:20c.png:zs-banner2.png
        var result = [];
        var files = file.split('|');
        files.forEach(function (e) {
            var fp = e.split(':');
            // let dfu:string = this.baseService.baseUrl + this.downloadUrl + "?bucket="+fp[0]+"&key="+fp[1]+"&name="+encodeURIComponent(fp[2]);
            var dfu = _this.baseService.fileUrl + "/" + fp[0] + "/" + fp[1];
            result.push({ url: dfu, name: fp[0] });
        });
        return result;
    };
    // 转换下载文件
    // 1.只转换第一个文件；2.返回tomcat访问路径
    FileService.prototype.getFile = function (file) {
        // plupload:3tvb3mb.png:zs-banner1.png|plupload:20c.png:zs-banner2.png
        var result = null;
        var files = file.split('|');
        if (files.length > 0) {
            var fp = files[0].split(':');
            result = this.baseService.fileUrl + "/" + fp[0] + "/" + fp[1];
        }
        return result;
    };
    FileService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__BaseService__["a" /* BaseService */]])
    ], FileService);
    return FileService;
}());

//# sourceMappingURL=FileService.js.map

/***/ }),

/***/ 735:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutoresizeDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AutoresizeDirective = /** @class */ (function () {
    function AutoresizeDirective(element) {
        this.element = element;
    }
    AutoresizeDirective.prototype.onInput = function (textArea) {
        this.adjust();
    };
    AutoresizeDirective.prototype.ngOnInit = function () {
        this.adjust();
    };
    AutoresizeDirective.prototype.adjust = function () {
        var ta = this.element.nativeElement.querySelector("textarea"), newHeight;
        if (ta) {
            ta.style.overflow = "hidden";
            ta.style.height = "auto";
            if (this.maxHeight) {
                console.log('this.maxHeight', this.maxHeight);
                newHeight = Math.min(ta.scrollHeight, this.maxHeight);
                console.log('newHeight', newHeight);
            }
            else {
                newHeight = ta.scrollHeight;
            }
            ta.style.height = newHeight + "px";
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('input', ['$event.target']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [HTMLTextAreaElement]),
        __metadata("design:returntype", void 0)
    ], AutoresizeDirective.prototype, "onInput", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('autoresize'),
        __metadata("design:type", Number)
    ], AutoresizeDirective.prototype, "maxHeight", void 0);
    AutoresizeDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: "ion-textarea[autoresize]" // Attribute selector
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]])
    ], AutoresizeDirective);
    return AutoresizeDirective;
}());

//# sourceMappingURL=AutoresizeDirective.js.map

/***/ }),

/***/ 736:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CourseHttp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_utils_DateUtil__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_utils_ImageUtil__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_core_data_BaseService__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// 基础服务类，提供基础路径
var CourseHttp = /** @class */ (function () {
    function CourseHttp(http, baseService, imageUtil, dateUtil) {
        this.http = http;
        this.baseService = baseService;
        this.imageUtil = imageUtil;
        this.dateUtil = dateUtil;
    }
    /**
     * 设置课程是否被收藏
     * @param sn
     * @param courseId
     * @param userId
     * @param collect
     */
    CourseHttp.prototype.setCollect = function (sn, courseId, userId, collect) {
        var args = {
            courseId: courseId,
            userId: userId,
            collect: collect,
        };
        var as = this.baseService.toJSONStr(args);
        var url = this.baseService.baseUrl + "/" + sn + "/setCollect";
        return this.http.post(url, as, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError([])));
    };
    /**
     * 添加课程点击数目
     * @param sn
     * @param courseId
     * @param userId
     * @param collect
     */
    CourseHttp.prototype.addCourseClicks = function (sn, courseId) {
        var args = {
            courseId: courseId
        };
        var as = this.baseService.toJSONStr(args);
        var url = this.baseService.baseUrl + "/" + sn + "/addCourseClicks";
        return this.http.post(url, as, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError([])));
    };
    /**
     * 添加章节是否被学习
     * @param sn
     * @param courseId
     * @param userId
     * @param collect
     */
    CourseHttp.prototype.addChapterLearn = function (sn, courseId, userId, chapterId) {
        var args = {
            courseId: courseId,
            userId: userId,
            chapterId: chapterId,
        };
        var as = this.baseService.toJSONStr(args);
        var url = this.baseService.baseUrl + "/" + sn + "/addChapterLearn";
        return this.http.post(url, as, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError([])));
    };
    /**
 * 格式化课程服务的数据
 * @param val
 */
    CourseHttp.prototype.foremateCourseValue = function (val) {
        //处理图片路径
        val["imagePath"] = this.imageUtil.getImagePath(val["image"]);
        //处理时间格式
        val["dataForemate"] = this.dateUtil.getDateFormate(val["releaseTime"]);
        //已经学习完成的统计方式
        if (val["finshed"] == null || val["finshed"] == '') {
            val["finshedCount"] = 0;
        }
        else {
            var lcount = val["finshed"].split(",");
            if (lcount != null) {
                val["finshedCount"] = lcount.length;
            }
            else {
                val["finshedCount"] = 0;
            }
        }
        //处理收藏记录的格式
        if (val["collect"] == "1") {
            val["iconName"] = "star";
        }
        else {
            val["iconName"] = "star-outline";
        }
        return val;
    };
    ;
    CourseHttp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_4__app_utils_ImageUtil__["a" /* ImageUtil */],
            __WEBPACK_IMPORTED_MODULE_3__app_utils_DateUtil__["a" /* DateUtil */]])
    ], CourseHttp);
    return CourseHttp;
}());

//# sourceMappingURL=CourseHttp.js.map

/***/ }),

/***/ 737:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserExam; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// 用户考题基础
var UserExam = /** @class */ (function () {
    function UserExam(http, baseService) {
        this.http = http;
        this.baseService = baseService;
    }
    /**
     * 用户考题
     * @param sn
     * @param courseId
     * @param userId
     * @param collect
     */
    UserExam.prototype.userExam = function (sn, topicId, userId) {
        var args = {
            topicId: topicId,
            userId: userId
        };
        var as = this.baseService.toJSONStr(args);
        var url = this.baseService.baseUrl + "/" + sn + "/userExam";
        return this.http.post(url, as, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError([])));
    };
    UserExam = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */]])
    ], UserExam);
    return UserExam;
}());

//# sourceMappingURL=UserExam.js.map

/***/ }),

/***/ 738:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AsLength; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * 文本长度过滤类
 */
var AsLength = /** @class */ (function () {
    function AsLength() {
    }
    AsLength.prototype.transform = function (value, length) {
        if (!value)
            return value;
        if (typeof value !== 'string') {
            return null;
        }
        else {
            if (value.length < length)
                return value;
            else {
                var temp = value.substr(0, length) + "...";
                return temp;
            }
        }
    };
    AsLength = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'asLength' })
    ], AsLength);
    return AsLength;
}());

//# sourceMappingURL=AsLength.js.map

/***/ }),

/***/ 739:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AsDateExtend; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_DateUtil__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * 时间格式化拓展
 */
var AsDateExtend = /** @class */ (function () {
    function AsDateExtend(dateUtil) {
        this.dateUtil = dateUtil;
    }
    AsDateExtend.prototype.transform = function (input) {
        if (input == null) {
            return '';
        }
        var dType = typeof (input);
        var date = this.dateUtil.parseDate(input, dType);
        if (date) {
            var y = date.getFullYear();
            var m = date.getMonth() + 1;
            var d = date.getDate();
            var hours = date.getHours();
            var min = date.getMinutes();
            var second = date.getSeconds();
            var result = "";
            if (hours < 10) {
                hours = "0" + hours;
            }
            if (min < 10) {
                min = "0" + min;
            }
            result = y + '-' + m + '-' + d + " " + hours + ":" + min;
            return result;
        }
        else if (dType == 'string') {
            var array = input.match(/\d+/g);
            var hours = array[3];
            var min = array[4];
            if (hours < 10) {
                hours = "0" + hours;
            }
            if (min < 10) {
                min = "0" + min;
            }
            input = array[0] + '-' + array[1] + '-' + array[2] + " " + hours + ":" + min;
            return input;
        }
        return '';
    };
    AsDateExtend = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'asDateExtend' }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__utils_DateUtil__["a" /* DateUtil */]])
    ], AsDateExtend);
    return AsDateExtend;
}());

//# sourceMappingURL=AsDateExtend.js.map

/***/ }),

/***/ 740:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AsDate; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_DateUtil__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * 时间格式化
 */
var AsDate = /** @class */ (function () {
    function AsDate(dateUtil) {
        this.dateUtil = dateUtil;
    }
    AsDate.prototype.transform = function (input) {
        if (input == null) {
            return '';
        }
        var dType = typeof (input);
        var date = this.dateUtil.parseDate(input, dType);
        if (date) {
            var y = date.getFullYear();
            var m = date.getMonth() + 1;
            var d = date.getDate();
            var hours = date.getHours();
            var min = date.getMinutes();
            var second = date.getSeconds();
            var result = "";
            result = y + '-' + m + '-' + d;
            return result;
        }
        else if (dType == 'string') {
            var array = input.match(/\d+/g);
            input = array[0] + '-' + array[1] + '-' + array[2];
            return input;
        }
        return '';
    };
    AsDate = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'asDate' }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__utils_DateUtil__["a" /* DateUtil */]])
    ], AsDate);
    return AsDate;
}());

//# sourceMappingURL=AsDate.js.map

/***/ }),

/***/ 741:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AsImage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_core_data_BaseService__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * 时间格式化
 */
var AsImage = /** @class */ (function () {
    function AsImage(baseService) {
        this.baseService = baseService;
    }
    AsImage.prototype.transform = function (fileInfo) {
        if (fileInfo == null || fileInfo == "")
            return "";
        var fileInfoArr = fileInfo.split(":");
        if (fileInfoArr.length < 3)
            return "";
        var bucket = fileInfoArr[0];
        var key = fileInfoArr[1];
        var name = fileInfoArr[2];
        var temp = this.baseService.baseUrl + '/file/plupload/download';
        temp = temp + "?bucket=" + bucket + "&key=" + key + "&name=" + encodeURIComponent(name);
        return temp;
    };
    AsImage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'asImage' }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__app_core_data_BaseService__["a" /* BaseService */]])
    ], AsImage);
    return AsImage;
}());

//# sourceMappingURL=AsImage.js.map

/***/ }),

/***/ 742:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AsTime; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_DateUtil__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * 时间格式化
 */
var AsTime = /** @class */ (function () {
    function AsTime(dateUtil) {
        this.dateUtil = dateUtil;
    }
    AsTime.prototype.transform = function (input) {
        if (input == null) {
            return '';
        }
        var dType = typeof (input);
        var date = this.dateUtil.parseDate(input, dType);
        if (date) {
            var hours = date.getHours();
            var min = date.getMinutes();
            var second = date.getSeconds();
            var result = "";
            if (hours < 10) {
                hours = "0" + hours;
            }
            if (min < 10) {
                min = "0" + min;
            }
            result = hours + ":" + min;
            return result;
        }
        else if (dType == 'string') {
            var array = input.match(/\d+/g);
            var hours = array[3];
            var min = array[4];
            if (hours < 10) {
                hours = "0" + hours;
            }
            if (min < 10) {
                min = "0" + min;
            }
            result = hours + ":" + min;
            return result;
        }
        return '';
    };
    AsTime = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'asTime' }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__utils_DateUtil__["a" /* DateUtil */]])
    ], AsTime);
    return AsTime;
}());

//# sourceMappingURL=AsTime.js.map

/***/ }),

/***/ 743:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_ccgl_ccgl__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_rcap_rcap__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_swgl_swgl__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_qjgl_qjgl__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_wlkyxm_wlkyxm__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_ycsq_ycsq__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_yzgl_yzgl__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_zysq_zysq__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_wjry_wjry__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_kyxm_kyxm__ = __webpack_require__(108);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, baseService, dataService, securityService, changeDetectorRef, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.baseService = baseService;
        this.dataService = dataService;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.pet = "待办事宜";
        this.datas = [];
        this.user = {};
        this.tableName = 'worklist';
        this.showDetails = false;
        //构建formObject对象方式修改，这种方式建立了独立的对象数据
        this.worklist = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
        this.overworklist = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
        this.gztaskview = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
        this.gzreceiveview = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
    }
    HomePage.prototype.toQueryWork = function (data) {
        var para = {};
        para["bk"] = data.actdefid;
        para["fn"] = data.prjforname;
        para["vi"] = data.prjpopname;
        para["dk"] = data.sbussinessid;
        //出差
        if (para["bk"] == "ccgl"
            || para["bk"] == "ccgl_jjgb"
            || para["bk"] == "ccgl_kjgb"
            || para["bk"] == "ccgl_zrjbgb") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_ccgl_ccgl__["a" /* CcglPage */], para);
        }
        //收发文
        if (para["bk"] == "sfwgl"
            || para["bk"] == "sfwgl_jzgb"
            || para["bk"] == "sfwgl_kszysj"
            || para["bk"] == "sfwgl_zrjbgb") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_swgl_swgl__["a" /* SwglPage */], para);
        }
        //外来科研项目
        if (para["bk"] == "wlkyxmrqxk") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__pages_wlkyxm_wlkyxm__["a" /* WlkyxmPage */], para);
        }
        //用车申请
        if (para["bk"] == "clsysq"
            || para["bk"] == "clsysq_kjgb"
            || para["bk"] == "clsysq_zrjbgb") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_12__pages_ycsq_ycsq__["a" /* YcsqPage */], para);
        }
        //用章管理
        if (para["bk"] == "yzgl"
            || para["bk"] == "yzgl_zrjbgb") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__pages_yzgl_yzgl__["a" /* YzglPage */], para);
        }
        //资源申请
        if (para["bk"] == "zydd"
            || para["bk"] == "zydd") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_14__pages_zysq_zysq__["a" /* ZysqPage */], para);
        }
        //请假管理
        if (para["bk"] == "qjgl"
            || para["bk"] == "qjgl_jjgb"
            || para["bk"] == "qjgl_kjgb") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_qjgl_qjgl__["a" /* QjglPage */], para);
        }
        //外籍人员入区许可办理
        if (para["bk"] == "wjryrqxk") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_15__pages_wjry_wjry__["a" /* WjryPage */], para);
        }
        //科研项目管理
        if (para["bk"] == "kyxmgl") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_16__pages_kyxm_kyxm__["a" /* KyxmPage */], para);
        }
    };
    ;
    HomePage.prototype.toQueryReceive = function (data) {
        var para = {};
        para["bk"] = data.bpmkey;
        para["fn"] = "gzpublishfile";
        para["vi"] = "gzpublishfile";
        para["dk"] = data.fwid;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_swgl_swgl__["a" /* SwglPage */], para);
    };
    ;
    HomePage.prototype.toQuerySchedule = function (data) {
        var para = data;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_rcap_rcap__["a" /* RcapPage */], para);
    };
    ;
    //初始化页面
    HomePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.user = {};
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.initSuccessHomePage();
            }, function () {
                _this.user = null;
                _this.goLogin();
            });
        }
        else {
            this.initSuccessHomePage();
        }
    };
    ;
    //跳转到登录界面
    HomePage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    HomePage.prototype.editDatas = function (datas) {
        for (var item = 0; item < datas.length; item++) {
            datas[item].icon = 'ios-arrow-forward';
        }
    };
    ;
    /**
     * 成功加载用户home页面时处理
     */
    HomePage.prototype.initSuccessHomePage = function () {
        this.user = this.securityService.user;
        //构建formObject对象方式修改，这种方式建立了独立的对象数据
        if (this.worklist.sn == "") {
            this.worklist = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
            this.worklist.sn = "worklist";
        }
        if (this.overworklist.sn == "") {
            this.overworklist = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
            this.overworklist.sn = "overworklist";
        }
        if (this.gztaskview.sn == "") {
            this.gztaskview = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
            this.gztaskview.sn = "gztaskview";
            this.gztaskview.queryObject = { "zt:!=": "rwzt_ywc" };
            this.gztaskview.orderObject = { "cjsj": 1 };
        }
        if (this.gzreceiveview.sn == "") {
            this.gzreceiveview = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
            this.gzreceiveview.sn = "gzreceiveview";
            this.gzreceiveview.queryObject = { "sfqr": "0" };
            this.gzreceiveview.orderObject = { "jjdj": 1, "cjsj": 1 };
        }
        if (this.tableName == "worklist" && this.worklist.datas.length == 0) {
            this.worklist.getEntitys(null);
        }
        else if (this.tableName == "worklist" && this.overworklist.datas.length == 0) {
            this.overworklist.getEntitys(null);
        }
        else if (this.tableName == "gztaskview" && this.gztaskview.datas.length == 0) {
            this.gztaskview.getEntitys(this.editDatas);
        }
        else if (this.tableName == "gztaskview" && this.gzreceiveview.datas.length == 0) {
            this.gzreceiveview.getEntitys(this.editDatas);
        }
        //if(this.worklist.datas && this.worklist.datas.length>0){
        // }else{
        // this.worklist.getEntitys(null);
        // }
    };
    ;
    HomePage.prototype.setTable = function (formName) {
        if (formName == "worklist") {
            if (this.worklist.datas && this.worklist.datas.length > 0) {
            }
            else {
                this.worklist.getEntitys(null);
            }
        }
        else if (formName == "overworklist") {
            if (this.overworklist.datas && this.overworklist.datas.length > 0) {
            }
            else {
                this.overworklist.getEntitys(null);
            }
        }
        else if (formName == "gztaskview") {
            if (this.gztaskview.datas && this.gztaskview.datas.length > 0) {
            }
            else {
                this.gztaskview.getEntitys(this.editDatas);
            }
        }
        else {
            if (this.gzreceiveview.datas && this.gzreceiveview.datas.length > 0) {
            }
            else {
                this.gzreceiveview.getEntitys(this.editDatas);
            }
        }
        this.tableName = formName;
    };
    //打开或关闭数据详细内容
    HomePage.prototype.toggleDetails = function (data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-forward';
        }
        else {
            data.showDetails = true;
            data.icon = 'ios-arrow-down';
        }
    };
    //查询
    HomePage.prototype.onSearchKeyUp = function (event) {
        if ("Enter" == event.key) {
            //if(this.user!=null && this.user["userid"]!=null){
            // this.formObject.pageIndex=0;
            // this.formObject.dataOver=false;
            // this.formObject.getEntitys();
        }
        //}else{
        //this.queryValue="";
        //}
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\home\home.html"*/'<ion-header class="card-background-page">\n\n  <div class="g-top">\n\n      <div class="m-logo">贵州茂兰国家级自然保护区电子办公系统</div>\n\n    </div>\n\n    <div class="zs-header" style="z-index: 1000">\n\n      <ion-segment [(ngModel)]="pet">\n\n        <ion-segment-button value="待办事宜" (click)="setTable(\'worklist\')">\n\n          待办事宜\n\n        </ion-segment-button>\n\n        <ion-segment-button value="已办事宜" (click)="setTable(\'overworklist\')">\n\n          已办事宜\n\n        </ion-segment-button>\n\n        <ion-segment-button value="日程安排" (click)="setTable(\'gztaskview\')">\n\n          日程安排\n\n        </ion-segment-button>\n\n        <ion-segment-button value="收文" (click)="setTable(\'gzreceiveview\')">\n\n          收文\n\n        </ion-segment-button>\n\n      </ion-segment>\n\n    </div>\n\n</ion-header>\n\n<ion-content class="card-background-page">\n\n  \n\n   <!--待办事项-s-->\n\n   <div style="margin-top: 80px;" *ngIf="tableName==\'worklist\'">\n\n    <ion-refresher (ionRefresh)="worklist.doRefresh($event)">\n\n      <ion-refresher-content pullingIcon="arrow-dropdown"  pullingText="下拉刷新"  refreshingSpinner="circles"  refreshingText="刷新中...">\n\n        </ion-refresher-content>\n\n    </ion-refresher>\n\n\n\n    <ion-list *ngFor="let item of worklist.datas" \n\n    style="padding-left: 1px;right: -7px;width: calc(100% - 15px);\n\n    margin-top:10px; ">\n\n      <button ion-item detail-none class="buttonscss" (click)="toQueryWork(item)">\n\n        <!--<ion-icon item-left color="#444" name="{{item.icon}}"></ion-icon>-->\n\n        <h3>{{item.sbusiness}}</h3>\n\n        <p item-right>{{item.taskdefname}}({{item.actdefname}})</p>\n\n      </button>\n\n    </ion-list>\n\n    \n\n    <!-- 无级滚动效果实现 -->\n\n    <ion-infinite-scroll (ionInfinite)="worklist.doInfinite($event)">\n\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n  </div>\n\n <!--待办事项-s-->\n\n\n\n <!--已办事项-s-->\n\n <div style="margin-top: 80px;" *ngIf="tableName==\'overworklist\'">\n\n  <ion-refresher (ionRefresh)="overworklist.doRefresh($event)">\n\n    <ion-refresher-content pullingIcon="arrow-dropdown"  pullingText="下拉刷新"  refreshingSpinner="circles"  refreshingText="刷新中...">\n\n      </ion-refresher-content>\n\n  </ion-refresher>\n\n\n\n  <ion-list *ngFor="let item of overworklist.datas" \n\n  style="padding-left: 1px;right: -7px;width: calc(100% - 15px);\n\n  margin-top:10px; ">\n\n    <button ion-item detail-none class="buttonscss" (click)="toQueryWork(item)">\n\n      <!--<ion-icon item-left color="#444" name="{{item.icon}}"></ion-icon>-->\n\n      <h3>{{item.sbusiness}}</h3>\n\n      <p item-right>{{item.taskdefname}}({{item.actdefname}})</p>\n\n    </button>\n\n  </ion-list>\n\n  \n\n  <!-- 无级滚动效果实现 -->\n\n  <ion-infinite-scroll (ionInfinite)="overworklist.doInfinite($event)">\n\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n  </ion-infinite-scroll>\n\n</div>\n\n<!--已办事项-e-->\n\n\n\n  <!--日程安排-->\n\n  <div style="margin-top: 80px;" *ngIf="tableName==\'gztaskview\'">\n\n    <ion-refresher (ionRefresh)="gztaskview.doRefresh($event,editDatas)">\n\n      <ion-refresher-content pullingIcon="arrow-dropdown"  pullingText="下拉刷新"  refreshingSpinner="circles"  refreshingText="刷新中...">\n\n        </ion-refresher-content>\n\n    </ion-refresher>\n\n\n\n    <ion-list *ngFor="let item of gztaskview.datas" \n\n    style="padding-left: 1px;right: -7px;width: calc(100% - 15px);margin-top:10px; ">\n\n      <button ion-item detail-none class="buttonscss" (click)="toggleDetails(item)">\n\n        <ion-icon item-left color="#444" name="{{item.icon}}"></ion-icon>\n\n        <h3>{{item.xmmc}}({{item.ztName}})</h3>\n\n        <p item-right>{{item.cjsj | date:\'yyyy-MM-dd\'}}</p>\n\n      </button>\n\n      <div *ngIf="item.showDetails" class="divcard" padding #divcard>\n\n        <ion-row>\n\n            <p><span>主题：</span>{{item.zhuti}}</p>\n\n          </ion-row>\n\n        <ion-row>\n\n          <p><span>备注：</span>{{item.sxsm}}</p>\n\n        </ion-row>\n\n        <ion-row>\n\n            <p><span>开始时间：</span>{{item.kssj | date:\'yyyy-MM-dd\'}}</p>\n\n        </ion-row>\n\n        <ion-row>\n\n            <p><span>结束时间：</span>{{item.jssj | date:\'yyyy-MM-dd\'}}</p>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col>\n\n            <button ion-button icon-only clear full large (click)="toQuerySchedule(item)">\n\n              <ion-icon name="md-search"></ion-icon>\n\n              查看详情\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </div>\n\n    </ion-list>\n\n    \n\n    <!-- 无级滚动效果实现 -->\n\n    <ion-infinite-scroll (ionInfinite)="gztaskview.doInfinite($event,editDatas)">\n\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n  </div>\n\n\n\n  <!--收文-->\n\n  <div style="margin-top: 80px;" *ngIf="tableName==\'gzreceiveview\'">\n\n    <ion-refresher (ionRefresh)="gzreceiveview.doRefresh($event,editDatas)">\n\n      <ion-refresher-content pullingIcon="arrow-dropdown"  pullingText="下拉刷新"  refreshingSpinner="circles"  refreshingText="刷新中...">\n\n        </ion-refresher-content>\n\n    </ion-refresher>\n\n    <ion-list *ngFor="let item of gzreceiveview.datas" style="padding-left: 1px;right: -7px;width: calc(100% - 15px);margin-top:10px; ">\n\n        <button ion-item detail-none class="buttonscss" (click)="toggleDetails(item)">\n\n          <ion-icon item-left color="#444" name="{{item.icon}}"></ion-icon>\n\n          <h3>{{item.fwzt}}({{item.jjdjName}})</h3>\n\n          <p item-right>{{item.cjsj | date:\'yyyy-MM-dd\'}}</p>\n\n        </button>\n\n        <div *ngIf="item.showDetails" class="divcard" padding #divcard>\n\n          <ion-row>\n\n            <p><span>发文说明：</span>{{item.fwsm}}</p>\n\n          </ion-row>\n\n          <ion-row>\n\n              <p><span>创建人：</span>{{item.cjrmc}}</p>\n\n          </ion-row>\n\n          <ion-row>\n\n              <p><span>部门名称：</span>{{item.bmmcName}}</p>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <button ion-button icon-only clear full large (click)="toQueryReceive(item)">\n\n                <ion-icon name="md-search"></ion-icon>\n\n                查看详情\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </div>\n\n      </ion-list>\n\n\n\n    <!-- 无级滚动效果实现 -->\n\n    <ion-infinite-scroll (ionInfinite)="gzreceiveview.doInfinite($event,editDatas)">\n\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_5__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_6__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 744:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApprovePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_rcap_rcap__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_wfbd_wfbd__ = __webpack_require__(162);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ApprovePage = /** @class */ (function () {
    function ApprovePage(navCtrl, baseService, dataService, securityService, changeDetectorRef, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.baseService = baseService;
        this.dataService = dataService;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.pet = "指派给我的";
        this.datas = [];
        this.user = {};
        this.tableName = 'gztaskview';
        this.showDetails = false;
        //构建formObject对象方式修改，这种方式建立了独立的对象数据
        this.gztaskview = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
        this.schedule = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
        this.gzccgl = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
    }
    ApprovePage.prototype.toQueryTask = function (data) {
        var para = data;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_rcap_rcap__["a" /* RcapPage */], para);
    };
    ;
    ApprovePage.prototype.toQuerySchedule = function (data) {
        var para = data;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_wfbd_wfbd__["a" /* WfbdPage */], para);
    };
    ;
    //初始化页面
    ApprovePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.user = {};
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.initSuccessApprovePage();
            }, function () {
                _this.user = null;
                _this.goLogin();
            });
        }
        else {
            this.initSuccessApprovePage();
        }
    };
    ;
    //跳转到登录界面
    ApprovePage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    /**
     * 成功加载用户任务管理页面时处理
     */
    ApprovePage.prototype.initSuccessApprovePage = function () {
        this.user = this.securityService.user;
        //构建formObject对象方式修改，这种方式建立了独立的对象数据
        if (this.gztaskview.sn == "") {
            this.gztaskview = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
            this.gztaskview.sn = "gztaskview";
            this.gztaskview.orderObject = { "cjsj": 1 };
        }
        if (this.schedule.sn == "") {
            this.schedule = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
            this.schedule.sn = "schedule";
        }
        if (this.gzccgl.sn == "") {
            this.gzccgl = new __WEBPACK_IMPORTED_MODULE_2__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
            this.gzccgl.sn = "gzccgl";
        }
        if (this.tableName == "gztaskview" && this.gztaskview.datas.length == 0) {
            this.gztaskview.getEntitys(this.editDatas);
        }
        else if (this.tableName == "schedule" && this.schedule.datas.length == 0) {
            this.schedule.getEntitys(this.scheduleEditDatas);
        }
        else if (this.tableName == "gzccgl" && this.schedule.datas.length == 0) {
            this.gzccgl.getEntitys(this.editDatas);
        }
    };
    ;
    ApprovePage.prototype.editDatas = function (datas) {
        for (var item = 0; item < datas.length; item++) {
            datas[item].icon = 'ios-arrow-forward';
        }
    };
    ;
    ApprovePage.prototype.scheduleEditDatas = function (datas) {
        for (var item = 0; item < datas.length; item++) {
            datas[item].icon = 'ios-arrow-forward';
            //发布将参与人合并为一个字段
            datas[item].cyr = "";
            for (var item1 = 0; item1 < datas[item].gztask.length; item1++) {
                datas[item].cyr += datas[item].gztask[item1].jsrmc;
                if (item1 != datas[item].gztask.length - 1) {
                    datas[item].cyr += "，";
                }
            }
        }
    };
    ApprovePage.prototype.addScheduleRow = function () {
        var editRow = {};
        editRow.isNew = true;
        editRow.id = this.schedule.getGUID();
        editRow.bmmc = this.user["departmentId"];
        editRow.bmmcName = this.user["departmentName"];
        editRow.cjrid = this.user["userid"];
        editRow.cjrmc = this.user["userName"];
        editRow.cjsj = new Date().getTime();
        ;
        editRow.zt = 'rwzt_wyd';
        editRow.ztName = '未阅读';
        editRow.gztask = [];
        this.toQuerySchedule(editRow);
    };
    ApprovePage.prototype.setTable = function (formName) {
        if (formName == "gztaskview") {
            if (this.gztaskview.datas && this.gztaskview.datas.length > 0) {
            }
            else {
                this.gztaskview.getEntitys(this.editDatas);
            }
        }
        else if (formName == "schedule") {
            if (this.schedule.datas && this.schedule.datas.length > 0) {
            }
            else {
                this.schedule.getEntitys(this.scheduleEditDatas);
            }
        }
        else {
            if (this.gzccgl.datas && this.gzccgl.datas.length > 0) {
            }
            else {
                this.gzccgl.getEntitys(this.editDatas);
            }
        }
        this.tableName = formName;
    };
    /**
     * 重置并查询数据
     */
    ApprovePage.prototype.resetAndLoadDatas = function (formName) {
        if (formName == "gztaskview") {
            this.gztaskview.pageIndex = 1;
            this.gztaskview.dataOver = false;
            this.gztaskview.sn = formName;
            this.gztaskview.queryObject = {};
            this.gztaskview.getEntitys(this.editDatas);
        }
        else {
            this.schedule.pageIndex = 1;
            this.schedule.dataOver = false;
            this.schedule.sn = formName;
            this.schedule.queryObject = {};
            this.schedule.getEntitys(this.scheduleEditDatas);
        }
        this.tableName = formName;
    };
    ;
    //打开或关闭数据详细内容
    ApprovePage.prototype.toggleDetails = function (data) {
        if (data.showDetails) {
            data.showDetails = false;
            data.icon = 'ios-arrow-forward';
        }
        else {
            data.showDetails = true;
            data.icon = 'ios-arrow-down';
        }
    };
    //查询
    ApprovePage.prototype.onSearchKeyUp = function (event) {
        if ("Enter" == event.key) {
            //if(this.user!=null && this.user["userid"]!=null){
            // this.formObject.pageIndex=0;
            // this.formObject.dataOver=false;
            // this.formObject.getEntitys();
        }
        //}else{
        //this.queryValue="";
        //}
    };
    ApprovePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\approve\approve.html"*/'<ion-header class="card-background-page">\n\n  <div class="g-top">\n\n      <div class="m-logo">贵州茂兰国家级自然保护区电子办公系统</div>\n\n  </div>\n\n  <div class="zs-header" style="z-index: 1000;">\n\n    <ion-segment [(ngModel)]="pet">\n\n      <!-- <ion-segment-button (click)="setMajor(\'-\');" disabled="{{!less}}">\n\n        <ion-icon name="md-arrow-dropleft"></ion-icon>\n\n      </ion-segment-button> -->\n\n\n\n      <ion-segment-button value="指派给我的" (click)="setTable(\'gztaskview\')">\n\n        指派给我的\n\n      </ion-segment-button>\n\n      <ion-segment-button value="我发布的" (click)="setTable(\'schedule\')">\n\n        我发布的\n\n      </ion-segment-button>\n\n      <ion-segment-button value="出差管理" (click)="setTable(\'gzccgl\')">\n\n        出差管理\n\n      </ion-segment-button>\n\n    </ion-segment>\n\n    <!--<ion-input class="zs-search" [(ngModel)]="queryValue"\n\n                type="search" placeholder="   查询"\n\n                [ngModelOptions]="{standalone: true}"\n\n                (keyup)="onSearchKeyUp($event)"></ion-input>-->\n\n  </div>\n\n</ion-header>\n\n<ion-content class="card-background-page">\n\n\n\n  <!--指派给我的-->\n\n  <div style="margin-top: 80px;" *ngIf="tableName==\'gztaskview\'">\n\n    <ion-refresher (ionRefresh)="gztaskview.doRefresh($event,editDatas)">\n\n      <ion-refresher-content pullingIcon="arrow-dropdown"  pullingText="下拉刷新"  refreshingSpinner="circles"  refreshingText="刷新中...">\n\n        </ion-refresher-content>\n\n    </ion-refresher>\n\n\n\n    <ion-list *ngFor="let item of gztaskview.datas" style="padding-left: 1px;right: -7px;width: calc(100% - 15px);margin-top:10px; ">\n\n      <button ion-item detail-none class="buttonscss" (click)="toggleDetails(item)">\n\n        <ion-icon item-left color="#444" name="{{item.icon}}"></ion-icon>\n\n        <h3>{{item.xmmc}}({{item.ztName}})</h3>\n\n        <p item-right>{{item.cjsj | date:\'yyyy-MM-dd\'}}</p>\n\n      </button>\n\n      <div *ngIf="item.showDetails" class="divcard" padding #divcard>\n\n        <ion-row>\n\n            <p><span>主题：</span>{{item.zhuti}}</p>\n\n        </ion-row>\n\n        <ion-row>\n\n          <p><span>备注：</span>{{item.sxsm}}</p>\n\n        </ion-row>\n\n        <ion-row>\n\n            <p><span>开始时间：</span>{{item.kssj | date:\'yyyy-MM-dd\'}}</p>\n\n        </ion-row>\n\n        <ion-row>\n\n            <p><span>结束时间：</span>{{item.jssj | date:\'yyyy-MM-dd\'}}</p>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col>\n\n            <button ion-button icon-only clear full large (click)="toQueryTask(item)">\n\n              <ion-icon name="md-search"></ion-icon>\n\n              查看详情\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </div>\n\n    </ion-list>\n\n\n\n    <!-- 无级滚动效果实现 -->\n\n    <ion-infinite-scroll (ionInfinite)="gztaskview.doInfinite($event,editDatas)">\n\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n  </div>\n\n\n\n  <!--我发布的-->\n\n  <div style="margin-top: 80px;" *ngIf="tableName==\'schedule\'">\n\n    <ion-refresher (ionRefresh)="schedule.doRefresh($event,scheduleEditDatas)">\n\n      <ion-refresher-content pullingIcon="arrow-dropdown"  pullingText="下拉刷新"  refreshingSpinner="circles"  refreshingText="刷新中...">\n\n        </ion-refresher-content>\n\n    </ion-refresher>\n\n\n\n    <div style="float:right;margin-right:14px;">\n\n        <button ion-button (click)=\'addScheduleRow()\'>\n\n            发布任务\n\n        </button>\n\n    </div>\n\n\n\n    <ion-list *ngFor="let item of schedule.datas" style="padding-left: 1px;right: -7px;width: calc(100% - 15px);margin-top:10px; ">\n\n        <button ion-item detail-none class="buttonscss" (click)="toggleDetails(item)">\n\n          <ion-icon item-left color="#444" name="{{item.icon}}"></ion-icon>\n\n          <h3>{{item.xmmc}}({{item.ztName}})</h3>\n\n          <p item-right>{{item.cjsj | date:\'yyyy-MM-dd\'}}</p>\n\n        </button>\n\n        <div *ngIf="item.showDetails" class="divcard" padding #divcard>\n\n          <ion-row>\n\n              <p><span>主题：</span>{{item.zhuti}}</p>\n\n          </ion-row>\n\n          <ion-row>\n\n              <p><span>备注：</span>{{item.sxsm}}</p>\n\n          </ion-row>\n\n          <ion-row>\n\n              <p><span>参与人：</span>{{item.cyr}}</p>\n\n          </ion-row>\n\n          <ion-row>\n\n              <p><span>创建人：</span>{{item.cjrmc}}</p>\n\n          </ion-row>\n\n          <ion-row>\n\n              <p><span>部门：</span>{{item.bmmcName}}</p>\n\n          </ion-row>\n\n          <ion-row>\n\n            <ion-col>\n\n              <button ion-button icon-only clear full large (click)="toQuerySchedule(item)">\n\n                <ion-icon name="md-search"></ion-icon>\n\n                查看详情\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </div>\n\n      </ion-list>\n\n    <!-- 无级滚动效果实现 -->\n\n    <ion-infinite-scroll (ionInfinite)="schedule.doInfinite($event,scheduleEditDatas)">\n\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n  </div>\n\n\n\n  <!--我发布的-->\n\n  <div style="margin-top: 80px;" *ngIf="tableName==\'gzccgl\'">\n\n    <ion-refresher (ionRefresh)="gzccgl.doRefresh($event,editDatas)">\n\n      <ion-refresher-content pullingIcon="arrow-dropdown"  pullingText="下拉刷新"  refreshingSpinner="circles"  refreshingText="刷新中...">\n\n      </ion-refresher-content>\n\n    </ion-refresher>\n\n\n\n    <ion-list *ngFor="let item of gzccgl.datas" style="padding-left: 1px;right: -7px;width: calc(100% - 15px);margin-top:10px; ">\n\n      <button ion-item detail-none class="buttonscss" (click)="toggleDetails(item)">\n\n      <ion-icon item-left color="#444" name="{{item.icon}}"></ion-icon>\n\n      <h3>{{item.wcsy}}</h3>\n\n      <p item-right>{{item.sqrq | date:\'yyyy-MM-dd\'}}</p>\n\n      </button>\n\n      <div *ngIf="item.showDetails" class="divcard" padding #divcard>\n\n      <ion-row>\n\n        <p><span>申请人：</span>{{item.ccrxm}}</p>\n\n      </ion-row>\n\n      <ion-row>\n\n        <p><span>开始时间：</span>{{item.kssj | date:\'yyyy-MM-dd\'}}</p>\n\n      </ion-row>\n\n      <ion-row>\n\n        <p><span>结束时间：</span>{{item.jssj | date:\'yyyy-MM-dd\'}}</p>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col>\n\n        <button ion-button icon-only clear full large (click)="toQueryWork(item)">\n\n          <ion-icon name="md-search"></ion-icon>\n\n          查看详情\n\n        </button>\n\n        </ion-col>\n\n      </ion-row>\n\n      </div>\n\n    </ion-list>\n\n    \n\n    <!-- 无级滚动效果实现 -->\n\n    <ion-infinite-scroll (ionInfinite)="gzccgl.doInfinite($event,editDatas)">\n\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n  </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\approve\approve.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_5__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_6__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], ApprovePage);
    return ApprovePage;
}());

//# sourceMappingURL=approve.js.map

/***/ }),

/***/ 746:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MajorHttp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// 用户考题基础
var MajorHttp = /** @class */ (function () {
    function MajorHttp(http, baseService) {
        this.http = http;
        this.baseService = baseService;
    }
    /**
     * 获取专业
     */
    MajorHttp.prototype.getMajors = function () {
        var url = this.baseService.baseUrl + "/dictionarymanage/getChild/education_major/0/false";
        return this.http.post(url, "", { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError([])));
    };
    MajorHttp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */]])
    ], MajorHttp);
    return MajorHttp;
}());

//# sourceMappingURL=MajorHttp.js.map

/***/ }),

/***/ 78:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DictionaryService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__data_DataService__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// 登录服务
var DictionaryService = /** @class */ (function () {
    function DictionaryService(dataService, baseService, dicName, id, nv) {
        this.dataService = dataService;
        this.baseService = baseService;
        this.dicName = dicName;
        this.id = id;
        this.nv = nv;
        this.datas = [];
        this.dicName = dicName;
        this.id = id;
        this.nv = nv;
    }
    DictionaryService.prototype.getDatas = function (suc, fai) {
        var _this = this;
        this.dataService.getDictionaryChilds(this.dicName, this.id, this.nv).subscribe(function (data) {
            if (data) {
                _this.datas = data;
                if (suc)
                    suc();
            }
            else {
                if (fai)
                    fai();
            }
        });
    };
    DictionaryService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_1__data_BaseService__["a" /* BaseService */], String, String, Boolean])
    ], DictionaryService);
    return DictionaryService;
}());

//# sourceMappingURL=DictionaryService.js.map

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SwglPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_adduser_adduser__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var SwglPage = /** @class */ (function () {
    function SwglPage(navCtrl, navParams, dataService, baseService, alert, securityService, changeDetectorRef, loadingCtrl, actionSheetCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseService = baseService;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.readonly = "";
        this.solution = "不显示";
        this.haveError = false;
        this.user = {};
        this.gzpublishfile = new __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__["a" /* BpmObject */](this.dataService, this.baseService, this.changeDetectorRef, this.navCtrl, this.alert, this.loadingCtrl, "gzpublishfile");
        this.gzpublishfile.editRow = {};
        this.gzpublishfile.sn = "gzpublishfile";
        this.gzpublishfile.bpmKey = this.navParams.data["bk"];
        this.gzpublishfile.id = this.navParams.data["dk"];
        var that = this;
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                //this.initUserExam();
                _this.gzpublishfile.findWork(function (data) {
                    if (data.editRow.fj) {
                        var fj = data.editRow.fj;
                        var fjs = fj.split(":");
                        data.editRow.fjname = fjs[2];
                        data.editRow.fjlj = that.baseService.baseUrl + "/file/plupload/download?bucket=" + fjs[0] + "&key=" + fjs[1] + "&name=" + fjs[2];
                    }
                }, null);
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.gzpublishfile.findWork(function (data) {
                data.editRow.fjlst = [];
                if (data.editRow.fj) {
                    var fj = data.editRow.fj;
                    var fjlst = fj.split("|");
                    for (var k = 0; k < fjlst.length; k++) {
                        var item = { fjname: '', fjlj: '' };
                        var fjs = fjlst[k].split(":");
                        item.fjname = fjs[2];
                        item.fjlj = that.baseService.baseUrl + "/file/plupload/download?bucket=" + fjs[0] + "&key=" + fjs[1] + "&name=" + fjs[2];
                        data.editRow.fjlst.push(item);
                    }
                }
            }, null);
        }
    }
    SwglPage.prototype.editData = function (data) {
    };
    SwglPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
    };
    ;
    SwglPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    SwglPage.prototype.goSignPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__["a" /* BpmSignPage */], this.gzpublishfile);
    };
    ;
    SwglPage.prototype.goBackPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__["a" /* BpmBackPage */], this.gzpublishfile);
    };
    ;
    SwglPage.prototype.goProcessPage = function () {
        var _this = this;
        //this.navCtrl.push(BpmProcessPage,this.gzccgl);
        var actionSheet = this.actionSheetCtrl.create({
            title: '更多',
            buttons: [
                {
                    text: '办理过程',
                    handler: function () {
                        //console.log('Destructive clicked');
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__["a" /* BpmProcessPage */], _this.gzpublishfile);
                    }
                }, {
                    text: '流程图',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__["a" /* BpmImgPage */], _this.gzpublishfile);
                    }
                }, {
                    text: '取消',
                    role: '取消',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ;
    // query(){
    //   this.alert.create({
    //     title:"系统提示",
    //     message:"是否确认?",
    //     buttons:[{
    //       text:"确定",
    //       role:"确定",
    //       handler:()=>{
    //         let data=this.gzpublishfile.editRow;
    //         data.sfqr="1";
    //         this.dataService.update(this.gzpublishfile.sn,data);
    //         this.navCtrl.pop();
    //       }
    //     },{
    //       text:"取消",
    //       role:"取消",
    //       handler:()=>{
    //       }
    //     }]
    //   }).present();
    // };
    SwglPage.prototype.addUser = function () {
        if (this.gzpublishfile.editRow == null) {
            this.gzpublishfile.editRow = {};
        }
        if (this.gzpublishfile.editRow.ccsxry == null) {
            this.gzpublishfile.editRow.ccsxry = [];
        }
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__pages_adduser_adduser__["a" /* AddUserPage */], this.gzpublishfile.editRow.gzreceivefile);
    };
    SwglPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-swgl',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\swgl\swgl.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>收文管理</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">申请信息</span>\n\n    </ion-list-header>\n\n    <ion-item>\n\n      <span class="bhq-title">来文单位: </span>\n\n      <span class="bhq-content">{{gzpublishfile.editRow.lwdw}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">来文号: </span>\n\n        <span class="bhq-content">{{gzpublishfile.editRow.lwbh}}</span>\n\n      </ion-item>\n\n      <ion-item>\n\n          <span class="bhq-title">收文时间: </span>\n\n          <span class="bhq-content">{{gzpublishfile.editRow.lwsj | asDate}}</span>\n\n      </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">发文主题: </span>\n\n      <span class="bhq-content">{{gzpublishfile.editRow.fwzt}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">发文说明: </span>\n\n      <span class="bhq-content">{{gzpublishfile.editRow.fwsm}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">紧急等级: </span>\n\n      <span class="bhq-content">{{gzpublishfile.editRow.jjdjName}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">负责人: </span>\n\n      <span class="bhq-content">{{gzpublishfile.editRow.zyfzrname}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">创建人: </span>\n\n      <span class="bhq-content">{{gzpublishfile.editRow.cjrmc}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">创建时间: </span>\n\n      <span class="bhq-content">{{gzpublishfile.editRow.cjsj | asDate}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">部门名称: </span>\n\n      <span class="bhq-content">{{gzpublishfile.editRow.bmmcName}}</span>\n\n    </ion-item>\n\n    <ion-item style="height:auto">\n\n      <span class="bhq-title">附件: </span>\n\n      <span class="bhq-content">\n\n        <div *ngFor="let item of gzpublishfile.editRow.fjlst">\n\n        <a href="{{item.fjlj}}" >\n\n          {{item.fjname | asLength:20}}\n\n        </a><br/></div>\n\n        <!--<a href="{{gzpublishfile.editRow.fjlj}}">{{gzpublishfile.editRow.fjname}}</a>-->\n\n      </span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">处理情况: </span>\n\n      <span class="bhq-content">{{gzpublishfile.editRow.blqkName}}</span>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list" *ngIf="gzpublishfile.editRow.bpmkey==\'sfwgl_jzgb\'">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">收件人</span>\n\n    </ion-list-header>\n\n    <ion-item *ngFor="let item of gzpublishfile.editRow.gzreceivefile">\n\n      <ion-label class="bhq-content">{{item.jsrmc}}({{item.jsrjgmc}})</ion-label>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list" *ngIf="gzpublishfile.editRow.bpmkey==\'sfwgl\'">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab2">办公室审批</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzpublishfile.process.usertask2!=null">\n\n      <div *ngFor="let item of gzpublishfile.process.usertask2.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人:{{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child"\n\n        *ngIf="gzpublishfile.process.usertask2.editRow!=null && gzpublishfile.proauth.bgssp  && gzpublishfile.process.usertask2.userEdit">\n\n        <ion-label cclass="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzpublishfile.process.usertask2.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list" *ngIf="gzpublishfile.editRow.bpmkey==\'sfwgl_kszysj\'">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab2">科长审批</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzpublishfile.process.usertask2!=null">\n\n      <div *ngFor="let item of  gzpublishfile.process.usertask2.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人:{{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child"\n\n        *ngIf="gzpublishfile.process.usertask2.editRow!=null && gzpublishfile.proauth.kzsp  && gzpublishfile.process.usertask2.userEdit">\n\n        <ion-label cclass="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzpublishfile.process.usertask2.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list" *ngIf="gzpublishfile.editRow.bpmkey==\'sfwgl\' || gzpublishfile.editRow.bpmkey==\'sfwgl_zrjbgb\'">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab2">局领导审批</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzpublishfile.process.usertask3!=null">\n\n      <div *ngFor="let item of gzpublishfile.process.usertask3.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child"\n\n        *ngIf="gzpublishfile.process.usertask3.editRow!=null && gzpublishfile.proauth.jldsp  && gzpublishfile.process.usertask3.userEdit">\n\n        <ion-label cclass="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzpublishfile.process.usertask3.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list" *ngIf="gzpublishfile.editRow.bpmkey==\'sfwgl\' || gzpublishfile.editRow.bpmkey==\'sfwgl_zrjbgb\' || gzpublishfile.editRow.bpmkey==\'sfwgl_kszysj\'">\n\n     \n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">收文员转发</span>\n\n      <span style="float: right;padding-right: 1rem;" (click)="addUser();"> \n\n        <!--<ion-icon name="ios-arrow-forward-outline"></ion-icon>--> \n\n        <ion-icon name="md-contacts"></ion-icon>\n\n       </span>\n\n    </ion-list-header>\n\n\n\n    <ion-item class="bhq-last-child"\n\n    *ngIf="gzpublishfile.proauth.swyzf || gzpublishfile.proauth.fwsq">\n\n    <ion-label cclass="bhq-title">办理结果:</ion-label>\n\n    <ion-textarea class="zs-answer-inn" aria-rowspan="5"\n\n    [(ngModel)]="gzpublishfile.editRow.bljg">\n\n    </ion-textarea>\n\n</ion-item>\n\n\n\n<ion-item *ngIf="!(gzpublishfile.proauth.swyzf || gzpublishfile.proauth.fwsq)">\n\n    <span class="bhq-title">办理结果: </span>\n\n    <span class="bhq-content">{{gzpublishfile.editRow.bljg}}</span>\n\n  </ion-item>\n\n\n\n  \n\n    <ion-item *ngFor="let item of gzpublishfile.editRow.gzreceivefile">\n\n      <ion-label class="bhq-content">{{item.jsrmc}}({{item.jsrjgmc}})</ion-label>\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>\n\n\n\n<ion-footer class="bhq-bpm-button">\n\n  <div class="footer-btn" *ngIf="gzpublishfile.auth.opt_th" (click)="gzpublishfile.getBackTask(null,null);">\n\n    <ion-icon name="arrow-dropleft-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>退回</p>\n\n  </div>\n\n  <div class="footer-btn" *ngIf="gzpublishfile.auth.opt_bc" (click)="gzpublishfile.bpmSave(null);">\n\n    <ion-icon name="lock" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>保存</p>\n\n  </div>\n\n  <div class="footer-btn" *ngIf="gzpublishfile.auth.opt_tj" (click)="gzpublishfile.getNextTask(null,null);">\n\n    <ion-icon name="arrow-dropright-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>通过</p>\n\n  </div>\n\n  <!-- <div class="footer-more" (click)="goProcessPage();">...</div> -->\n\n  <div class="footer-btn" (click)="goProcessPage();">\n\n    <ion-icon name="keypad" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>更多</p>\n\n</div>\n\n</ion-footer>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\swgl\swgl.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"]])
    ], SwglPage);
    return SwglPage;
}());

//# sourceMappingURL=swgl.js.map

/***/ }),

/***/ 8:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__BaseService__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// 通用数据访问服务类
var DataService = /** @class */ (function () {
    function DataService(http, baseService) {
        this.http = http;
        this.baseService = baseService;
    }
    // 获取查询数据列表
    DataService.prototype.getEntitys = function (sn, fields, where, order, pageSize, pageIndex) {
        var args = {
            queryFields: fields,
            where: where,
            order: order,
            pageSize: pageSize,
            pageIndex: pageIndex
        };
        args.where = this.baseService.toJSONStr(args.where);
        args.order = this.baseService.toJSONStr(args.order);
        var as = this.baseService.toJSONStr(args);
        var url = this.baseService.baseUrl + "/" + sn + "/getEntitys";
        return this.http.post(url, as, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError([])));
    };
    // 获取查询数据列表
    DataService.prototype.getEntitysByKey = function (sn, id) {
        var url = this.baseService.baseUrl + "/" + sn + "/getEntitys/" + id;
        return this.http.post(url, '', { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError([])));
    };
    // 获取数据总记录数
    DataService.prototype.getRowCounts = function (sn, where) {
        var args = { where: where };
        args.where = this.baseService.toJSONStr(args.where);
        var as = this.baseService.toJSONStr(args);
        var url = this.baseService.baseUrl + "/" + sn + "/getRowCounts";
        return this.http.post(url, as, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError(0)));
    };
    // 获取单个记录
    DataService.prototype.getEntity = function (sn, id) {
        var url = this.baseService.baseUrl + "/" + sn + "/getEntity/" + id;
        return this.http.post(url, '', { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    // 添加
    DataService.prototype.add = function (sn, row) {
        var args = { entity: row };
        args.entity = this.baseService.toJSONStr(args.entity);
        var as = this.baseService.toJSONStr(args);
        var url = this.baseService.baseUrl + "/" + sn + "/add";
        return this.http.post(url, as, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    // 更新
    DataService.prototype.update = function (sn, row) {
        var args = { entity: row };
        args.entity = this.baseService.toJSONStr(args.entity);
        var as = this.baseService.toJSONStr(args);
        var url = this.baseService.baseUrl + "/" + sn + "/update";
        return this.http.post(url, as, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    // 删除单个记录
    DataService.prototype.delete = function (sn, id) {
        var url = this.baseService.baseUrl + "/" + sn + "/delete/" + id;
        return this.http.delete(url, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    // 删除单个记录
    DataService.prototype.deletes = function (sn, ids) {
        var url = this.baseService.baseUrl + "/" + sn + "/deletes/" + ids;
        return this.http.delete(url, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    /**
     * 工作流中的查找工作操作
     * @param sn 表单
     * @param id 数据主键
     * @param bpmKey 流程主键
     * @param user 用户信息
     */
    DataService.prototype.findWork = function (sn, datakey, bpmKey, user) {
        var url = this.baseService.baseUrl + "/" + sn + "/findWork";
        var args = {
            bpmKey: bpmKey,
            id: datakey,
            user: user
        };
        args.user = this.baseService.toJSONStr(args.user);
        var as = this.baseService.toJSONStr(args);
        return this.http.post(url, as, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    ;
    /**
     * 工作流中的保存功能实现
     * @param sn
     * @param optsave
     * @param editRow
     * @param user
     */
    DataService.prototype.bpmSave = function (sn, optsave, editRow, user) {
        var url = this.baseService.baseUrl + '/' + sn + '/processSave';
        var args = {
            optsave: optsave,
            entity: editRow,
            user: user
        };
        args.optsave = this.baseService.toJSONStr(args.optsave);
        args.entity = this.baseService.toJSONStr(args.entity);
        args.user = this.baseService.toJSONStr(args.user);
        var as = this.baseService.toJSONStr(args);
        return this.http.post(url, as, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    ;
    /**
     * 获取流程中的下一个环节节点
     * @param sn
     * @param bpmKey
     * @param entity
     * @param editHistory
     * @param user
     */
    DataService.prototype.getNextTask = function (sn, bpmKey, entity, editHistory, user) {
        var url = this.baseService.baseUrl + '/' + sn + '/getNextTask';
        var args = {
            bpmKey: bpmKey,
            entity: entity,
            editHistory: editHistory,
            user: user
        };
        args.editHistory = this.baseService.toJSONStr(args.editHistory);
        args.entity = this.baseService.toJSONStr(args.entity);
        args.user = this.baseService.toJSONStr(args.user);
        var as = this.baseService.toJSONStr(args);
        return this.http.post(url, as, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    ;
    /**
     * 流程提交
     * @param sn
     * @param bpmKey
     * @param entity
     * @param editHistory
     * @param user
     */
    DataService.prototype.signTask = function (sn, opttj, entity, user) {
        var url = this.baseService.baseUrl + '/' + sn + '/signTask';
        var args = {
            entity: entity,
            opttj: opttj,
            user: user
        };
        args.opttj = this.baseService.toJSONStr(args.opttj);
        args.entity = this.baseService.toJSONStr(args.entity);
        args.user = this.baseService.toJSONStr(args.user);
        var as = this.baseService.toJSONStr(args);
        return this.http.post(url, as, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    ;
    /**
     * 获取流程中的退回环节
     * @param sn
     * @param bpmKey
     * @param entity
     * @param editHistory
     * @param user
     */
    DataService.prototype.getBackTask = function (sn, bpmKey, entity, editHistory, user) {
        var url = this.baseService.baseUrl + '/' + sn + '/getBackTask';
        var args = {
            bpmKey: bpmKey,
            entity: entity,
            editHistory: editHistory,
            user: user
        };
        args.editHistory = this.baseService.toJSONStr(args.editHistory);
        args.entity = this.baseService.toJSONStr(args.entity);
        args.user = this.baseService.toJSONStr(args.user);
        var as = this.baseService.toJSONStr(args);
        return this.http.post(url, as, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    ;
    /**
     * 流程提交
     * @param sn
     * @param bpmKey
     * @param entity
     * @param editHistory
     * @param user
     */
    DataService.prototype.sendBack = function (sn, optback, entity, user) {
        var url = this.baseService.baseUrl + '/' + sn + '/sendBack';
        var args = {
            entity: entity,
            optback: optback,
            user: user
        };
        args.optback = this.baseService.toJSONStr(args.optback);
        args.entity = this.baseService.toJSONStr(args.entity);
        args.user = this.baseService.toJSONStr(args.user);
        var as = this.baseService.toJSONStr(args);
        return this.http.post(url, as, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    ;
    /**
     * 读取字典子表的数据
     * @param dicName
     * @param id
     * @param nv
     */
    DataService.prototype.getDictionaryChilds = function (dicName, id, nv) {
        var url = this.baseService.baseUrl + '/dictionarymanage/getChild/' + dicName + '/' + id + '/' + nv;
        return this.http.post(url, null, { headers: this.baseService.httpOptions(), withCredentials: true })
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["retry"])(3), Object(__WEBPACK_IMPORTED_MODULE_2_rxjs_operators__["catchError"])(this.baseService.handleError({})));
    };
    ;
    DataService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__BaseService__["a" /* BaseService */]])
    ], DataService);
    return DataService;
}());

//# sourceMappingURL=DataService.js.map

/***/ }),

/***/ 80:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QjglPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_dictionary_DictionaryService__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_bpm_BpmObject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_bpmback_bpmback__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_bpmsign_bpmsign__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_bpmprocess_bpmprocess__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_bpmimg_bpmimg__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var QjglPage = /** @class */ (function () {
    function QjglPage(navCtrl, navParams, dataService, baseSErvice, alert, securityService, changeDetectorRef, loadingCtrl, actionSheetCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseSErvice = baseSErvice;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.user = {};
        /**
         * 请假类别
         */
        this.qjlb = new __WEBPACK_IMPORTED_MODULE_4__app_core_dictionary_DictionaryService__["a" /* DictionaryService */](this.dataService, this.baseSErvice, "GZ_QJLX", "0", false);
        this.gzqjgl = new __WEBPACK_IMPORTED_MODULE_7__app_bpm_BpmObject__["a" /* BpmObject */](this.dataService, this.baseSErvice, this.changeDetectorRef, this.navCtrl, this.alert, this.loadingCtrl, "gzqjgl");
        this.gzqjgl.sn = "gzqjgl";
        this.gzqjgl.bpmKey = this.navParams.data["bk"];
        this.gzqjgl.id = this.navParams.data["dk"];
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                //this.initUserExam();
                if (_this.navParams.data != null && _this.navParams.data["bk"] != null) {
                    _this.gzqjgl.findWork(null, null);
                }
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            if (this.navParams.data != null && this.navParams.data["bk"] != null) {
                this.gzqjgl.findWork(null, null);
            }
        }
    }
    QjglPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    QjglPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    QjglPage.prototype.goSignPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_bpmsign_bpmsign__["a" /* BpmSignPage */], this.gzqjgl);
    };
    ;
    QjglPage.prototype.goBackPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_bpmback_bpmback__["a" /* BpmBackPage */], this.gzqjgl);
    };
    ;
    QjglPage.prototype.goProcessPage = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: '更多',
            buttons: [
                {
                    text: '办理过程',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_bpmprocess_bpmprocess__["a" /* BpmProcessPage */], _this.gzqjgl);
                    }
                }, {
                    text: '流程图',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__pages_bpmimg_bpmimg__["a" /* BpmImgPage */], _this.gzqjgl);
                    }
                }, {
                    text: '取消',
                    role: '取消',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ;
    QjglPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad LeaveNewPage');
        this.qjlb.getDatas(null, null);
    };
    QjglPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-qjgl',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\qjgl\qjgl.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>请假</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">申请信息</span>\n\n    </ion-list-header>\n\n    <ion-item>\n\n        <span class="bhq-title">部门: </span>\n\n        <span class="bhq-content">{{gzqjgl.editRow.szdw}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">姓名: </span>\n\n        <span class="bhq-content">{{gzqjgl.editRow.qjrxm}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">请假类型: </span>\n\n        <span class="bhq-content">{{gzqjgl.editRow.qjlxname}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">申请事由: </span>\n\n        <span class="bhq-content">{{gzqjgl.editRow.sqsy}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">开始时间: </span>\n\n        <span class="bhq-content">{{gzqjgl.editRow.kssj | asDate}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">开始时段: </span>\n\n        <span class="bhq-content">{{gzqjgl.editRow.sjsjjdname}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">结束时间: </span>\n\n        <span class="bhq-content">{{gzqjgl.editRow.jssj | asDate}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">结束时段: </span>\n\n        <span class="bhq-content">{{gzqjgl.editRow.jssjjdname}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">请假天数: </span>\n\n        <span class="bhq-content">{{gzqjgl.editRow.qjts}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n        <span class="bhq-title">申请日期: </span>\n\n        <span class="bhq-content">{{gzqjgl.editRow.sqrq | date:\'yyyy-MM-dd HH:mm:ss\'}}</span>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list" *ngIf="gzqjgl.editRow.qjts !=null && gzqjgl.editRow.qjts<3  && gzqjgl.editRow.bpmkey==\'qjgl\'">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab2">部门领导审批</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzqjgl.process.usertask2!=null">\n\n      <div *ngFor="let item of  gzqjgl.process.usertask2.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzqjgl.process.usertask2.editRow!=null && gzqjgl.proauth.bmldsp && gzqjgl.process.usertask2.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzqjgl.process.usertask2.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list" *ngIf="gzqjgl.editRow.qjts !=null && gzqjgl.editRow.qjts>=3 && gzqjgl.editRow.bpmkey==\'qjgl\'">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab2">科室审批</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzqjgl.process.usertask3!=null">\n\n      <div *ngFor="let item of  gzqjgl.process.usertask3.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzqjgl.process.usertask3.editRow!=null && gzqjgl.proauth.kssh  && gzqjgl.process.usertask3.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzqjgl.process.usertask3.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list" *ngIf="(gzqjgl.editRow.qjts !=null && gzqjgl.editRow.qjts>=3 && gzqjgl.editRow.bpmkey==\'qjgl\') || gzqjgl.editRow.bpmkey==\'qjgl_kjgb\'">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab2">分管领导审批</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzqjgl.process.usertask4!=null">\n\n      <div *ngFor="let item of  gzqjgl.process.usertask4.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzqjgl.process.usertask4.editRow!=null && gzqjgl.proauth.fgldsp  && gzqjgl.process.usertask4.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzqjgl.process.usertask4.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list" *ngIf="(gzqjgl.editRow.bpmkey==\'qjgl_jjgb\' || (gzqjgl.editRow.qjts !=null && gzqjgl.editRow.qjts>7)) && gzqjgl.editRow.bpmkey!=\'qjgl_jz\'">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab2">局领导审核</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzqjgl.process.usertask5!=null">\n\n      <div *ngFor="let item of  gzqjgl.process.usertask5.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzqjgl.process.usertask5.editRow!=null && gzqjgl.proauth.jldsp  && gzqjgl.process.usertask5.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzqjgl.process.usertask5.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n</ion-content>\n\n\n\n<ion-footer class="bhq-bpm-button">\n\n  <div class="footer-btn" *ngIf="gzqjgl.auth.opt_th" (click)="gzqjgl.getBackTask(null,null);">\n\n    <ion-icon name="arrow-dropleft-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>退回</p>\n\n  </div>\n\n  <div class="footer-btn" *ngIf="gzqjgl.auth.opt_bc" (click)="gzqjgl.bpmSave(null);">\n\n    <ion-icon name="lock" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>保存</p>\n\n  </div>\n\n  <div class="footer-btn" *ngIf="gzqjgl.auth.opt_tj" (click)="gzqjgl.getNextTask(null,null);">\n\n    <ion-icon name="arrow-dropright-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>通过</p>\n\n  </div>\n\n  <!-- <div class="footer-more" (click)="goProcessPage();">...</div> -->\n\n  <div class="footer-btn" (click)="goProcessPage();">\n\n    <ion-icon name="keypad" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>更多</p>\n\n</div>\n\n</ion-footer>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\qjgl\qjgl.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_5__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"]])
    ], QjglPage);
    return QjglPage;
}());

//# sourceMappingURL=qjgl.js.map

/***/ }),

/***/ 81:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return YcsqPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__app_form_FormObject__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var YcsqPage = /** @class */ (function () {
    function YcsqPage(navCtrl, navParams, dataService, baseSErvice, alert, securityService, changeDetectorRef, loadingCtrl, actionSheetCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseSErvice = baseSErvice;
        this.alert = alert;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.readonly = "";
        this.solution = "不显示";
        this.haveError = false;
        this.user = {};
        //车辆档案选择车辆信息问题
        this.clda = new __WEBPACK_IMPORTED_MODULE_11__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseSErvice, this.changeDetectorRef, this.loadingCtrl);
        this.gzclsq = new __WEBPACK_IMPORTED_MODULE_6__app_bpm_BpmObject__["a" /* BpmObject */](this.dataService, this.baseSErvice, this.changeDetectorRef, this.navCtrl, this.alert, this.loadingCtrl, "gzclsq");
        this.gzclsq.editRow = {};
        this.gzclsq.sn = "gzclsq";
        this.gzclsq.bpmKey = this.navParams.data["bk"];
        this.gzclsq.id = this.navParams.data["dk"];
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                //this.initUserExam();
                _this.gzclsq.findWork(null, null);
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.gzclsq.findWork(null, null);
        }
    }
    YcsqPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
    };
    ;
    YcsqPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    YcsqPage.prototype.goSignPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__pages_bpmsign_bpmsign__["a" /* BpmSignPage */], this.gzclsq);
    };
    ;
    YcsqPage.prototype.goBackPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_bpmback_bpmback__["a" /* BpmBackPage */], this.gzclsq);
    };
    ;
    YcsqPage.prototype.goProcessPage = function () {
        var _this = this;
        //this.navCtrl.push(BpmProcessPage,this.gzccgl);
        var actionSheet = this.actionSheetCtrl.create({
            title: '更多',
            buttons: [
                {
                    text: '办理过程',
                    handler: function () {
                        //console.log('Destructive clicked');
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__pages_bpmprocess_bpmprocess__["a" /* BpmProcessPage */], _this.gzclsq);
                    }
                }, {
                    text: '流程图',
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__pages_bpmimg_bpmimg__["a" /* BpmImgPage */], _this.gzclsq);
                    }
                }, {
                    text: '取消',
                    role: '取消',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ;
    YcsqPage.prototype.selectClcph = function () {
        var that = this;
        this.clda.sn = "gzclda";
        this.clda.ssn = "gzclda";
        this.clda.dataOver = false;
        this.clda.datas = [];
        this.clda.pageSize = 1000;
        this.clda.pageIndex = 1;
        this.clda.orderObject = { id: 0 };
        this.clda.queryObject = { 'clzt': 'clzt_wsy' };
        this.clda.getEntitys(function (data) {
            that.selectCarShow(that);
        });
    };
    ;
    /**
     * 显示选车信息
     * @param that
     */
    YcsqPage.prototype.selectCarShow = function (that) {
        var _this = this;
        if (that.clda.datas == null || that.clda.datas.length < 1) {
            this.alert.create({
                title: "系统提示",
                subTitle: "当前无可用车辆！",
                buttons: ["确定"]
            }).present();
            return;
        }
        var alert = this.alert.create();
        alert.setTitle("车辆选择");
        for (var k = 0; k < that.clda.datas.length; k++) {
            var icar = that.clda.datas[k];
            alert.addInput({
                type: 'checkbox',
                label: icar.sclxh + "-" + icar.sclxxName + "-" + icar.scph,
                value: icar.id,
                checked: true
            });
        }
        alert.addButton('取消');
        alert.addButton({
            text: '确定',
            handler: function (data) {
                console.log('Checkbox data:', data);
                var find = false;
                if (data != null && data.length > 0) {
                    for (var j = 0; j < that.clda.datas.length; j++) {
                        var jcar = that.clda.datas[j];
                        for (var i = 0; i < data.length; i++) {
                            if (jcar.id == data[i]) {
                                that.gzclsq.editRow.cph = jcar.scph;
                                find = true;
                            }
                        }
                    }
                }
                if (!find) {
                    var confirm_1 = _this.alert.create({
                        title: '系统提示',
                        message: "请先选择车辆！",
                        buttons: [
                            {
                                text: '确定',
                                handler: function () {
                                    that.selectCarShow(that);
                                }
                            }
                        ]
                    });
                    confirm_1.present();
                }
            }
        });
        alert.present();
    };
    ;
    YcsqPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-ycsq',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\ycsq\ycsq.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>车辆使用申请</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text">申请信息</span>\n\n    </ion-list-header>\n\n    <ion-item>\n\n      <span class="bhq-title">用车部门: </span>\n\n      <span class="bhq-content">{{gzclsq.editRow.ycbmName}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">申请人: </span>\n\n      <span class="bhq-content">{{gzclsq.editRow.ssqr}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">开始时间: </span>\n\n      <span class="bhq-content">{{gzclsq.editRow.djysj | asDate}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">结束时间: </span>\n\n      <span class="bhq-content">{{gzclsq.editRow.dghsj | asDate}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">用车事由: </span>\n\n      <span class="bhq-content">{{gzclsq.editRow.ssqyy}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">用车地点: </span>\n\n      <span class="bhq-content">{{gzclsq.editRow.ycdd}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">用车人数: </span>\n\n      <span class="bhq-content">{{gzclsq.editRow.ycrs}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">用车人员: </span>\n\n      <span class="bhq-content">{{gzclsq.editRow.ycry}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">驾驶员: </span>\n\n      <span class="bhq-content">{{gzclsq.editRow.jsy}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">联系方式: </span>\n\n      <span class="bhq-content">{{gzclsq.editRow.slxfs}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">申请时间: </span>\n\n      <span class="bhq-content">{{gzclsq.editRow.dsqsj | asDate}}</span>\n\n    </ion-item>\n\n    <ion-item>\n\n      <span class="bhq-title">备注: </span>\n\n      <span class="bhq-content">{{gzclsq.editRow.sbz}}</span>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list" *ngIf="gzclsq.editRow.bpmkey==\'clsysq\'">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab2">部门审批</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzclsq.process.usertask2!=null">\n\n      <div *ngFor="let item of  gzclsq.process.usertask2.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzclsq.process.usertask2.editRow!=null && gzclsq.proauth.sbmspyj  && gzclsq.process.usertask2.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzclsq.process.usertask2.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab3">分管领导审批</span>\n\n    </ion-list-header>\n\n    <div *ngIf="gzclsq.process.usertask3!=null">\n\n      <div *ngFor="let item of  gzclsq.process.usertask3.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n\n\n      <ion-item class="bhq-last-child" *ngIf="gzclsq.process.usertask3.editRow!=null && gzclsq.proauth.fgldsp  && gzclsq.process.usertask3.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzclsq.process.usertask3.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n\n\n  <ion-list class="approval-list" *ngIf="gzclsq.editRow.bpmkey==\'clsysq\' || gzclsq.editRow.bpmkey==\'clsysq_kjgb\'">\n\n    <ion-list-header>\n\n      <span class="approval-list-header-text" id="tab4">办公室调度意见</span>\n\n    </ion-list-header>\n\n\n\n    <ion-item>\n\n        <ion-label class="bhq-title">现金加油:</ion-label>\n\n        <ion-label class="bhq-content" *ngIf="!gzclsq.proauth.ddsh">{{gzclsq.editRow.sxjjy}}</ion-label>\n\n        <ion-input class="bhq-content-right" [(ngModel)]="gzclsq.editRow.sxjjy" *ngIf="gzclsq.proauth.ddsh"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label class="bhq-title">卡加油:</ion-label>\n\n        <ion-label class="bhq-content" *ngIf="!gzclsq.proauth.ddsh">{{gzclsq.editRow.skjy}}</ion-label>\n\n        <ion-input class="bhq-content-right" [(ngModel)]="gzclsq.editRow.skjy" *ngIf="gzclsq.proauth.ddsh"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <!--<ion-label class="bhq-title"></ion-label>-->\n\n        车牌号:{{gzclsq.editRow.cph}}\n\n       <!-- <ion-label class="bhq-content" *ngIf="!gzclsq.proauth.ddsh">{{gzclsq.editRow.cph}}</ion-label>-->\n\n\n\n        <button ion-button (click)="selectClcph();" *ngIf="gzclsq.proauth.ddsh">选择车辆</button >\n\n        <!--<ion-input class="bhq-content-right" [(ngModel)]="gzclsq.editRow.cph" *ngIf="gzclsq.proauth.ddsh"></ion-input>-->\n\n      </ion-item>\n\n\n\n    <div *ngIf="gzclsq.process.usertask4!=null">\n\n      <div *ngFor="let item of  gzclsq.process.usertask4.history">\n\n        <ion-item>\n\n          <ion-label class="bhq-content">{{item.optcontent}}</ion-label>\n\n        </ion-item>\n\n        <ion-item class="bhq-last-child">\n\n          <ion-label class="bhq-content">审批人: {{item.optassigneesname}}</ion-label>\n\n          <ion-label class="bhq-content">时间:{{item.opttime | asDate}}</ion-label>\n\n        </ion-item>\n\n      </div>\n\n      <ion-item class="bhq-last-child" *ngIf="gzclsq.process.usertask4.editRow!=null && gzclsq.proauth.ddsh  && gzclsq.process.usertask4.userEdit">\n\n        <ion-label class="bhq-title">审批意见:</ion-label>\n\n        <ion-textarea class="zs-answer-inn" aria-rowspan="5" [(ngModel)]="gzclsq.process.usertask4.editRow.optcontent">\n\n        </ion-textarea>\n\n      </ion-item>\n\n    </div>\n\n  </ion-list>\n\n</ion-content>\n\n\n\n<ion-footer class="bhq-bpm-button">\n\n  <div class="footer-btn" *ngIf="gzclsq.auth.opt_th" (click)="gzclsq.getBackTask(null,null);">\n\n    <ion-icon name="arrow-dropleft-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>退回</p>\n\n  </div>\n\n  <div class="footer-btn" *ngIf="gzclsq.auth.opt_bc" (click)="gzclsq.bpmSave(null);">\n\n    <ion-icon name="lock" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>保存</p>\n\n  </div>\n\n  <div class="footer-btn" *ngIf="gzclsq.auth.opt_tj" (click)="gzclsq.getNextTask(null,null);">\n\n    <ion-icon name="arrow-dropright-circle" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>通过</p>\n\n  </div>\n\n  <!-- <div class="footer-more" (click)="goProcessPage();">...</div> -->\n\n  <div class="footer-btn" (click)="goProcessPage();">\n\n    <ion-icon name="keypad" style="font-size:2em;color: #5477b0;"></ion-icon>\n\n    <p>更多</p>\n\n</div>\n\n</ion-footer>'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\ycsq\ycsq.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"]])
    ], YcsqPage);
    return YcsqPage;
}());

//# sourceMappingURL=ycsq.js.map

/***/ }),

/***/ 82:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RcapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_form_FormObject__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_utils_DateUtil__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var RcapPage = /** @class */ (function () {
    function RcapPage(navCtrl, navParams, dataService, baseService, alert, dateUtil, securityService, changeDetectorRef, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.baseService = baseService;
        this.alert = alert;
        this.dateUtil = dateUtil;
        this.securityService = securityService;
        this.changeDetectorRef = changeDetectorRef;
        this.loadingCtrl = loadingCtrl;
        this.readonly = "";
        this.solution = "不显示";
        this.haveError = false;
        this.user = {};
        this.gztaskview = new __WEBPACK_IMPORTED_MODULE_6__app_form_FormObject__["a" /* FormObject */](this.dataService, this.baseService, this.changeDetectorRef, this.loadingCtrl);
        this.gztaskview.editRow = {};
        this.gztaskview.sn = "gztaskview";
        if (this.securityService.user == null || this.securityService.user["userid"] == null) {
            this.securityService.getUserInfo(function () {
                _this.user = _this.securityService.user;
                _this.gztaskview.editRow = _this.navParams.data;
            }, function () {
                _this.goLogin();
            });
        }
        else {
            this.user = this.securityService.user;
            this.gztaskview.editRow = this.navParams.data;
        }
    }
    RcapPage.prototype.goLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
    };
    ;
    RcapPage.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    ;
    RcapPage.prototype.query = function () {
        var _this = this;
        this.alert.create({
            title: "系统提示",
            message: "是否确认?",
            buttons: [{
                    text: "确定",
                    role: "确定",
                    handler: function () {
                        var data = _this.gztaskview.editRow;
                        data.zt = "rwzt_yyd";
                        //data.wcsj=this.dateUtil.getDateFormateCommon(new Date());
                        //this.dataService.update(this.gztaskview.sn,data).subscribe((data)=>{
                        //this.navCtrl.pop();
                        //});
                    }
                }, {
                    text: "取消",
                    role: "取消",
                    handler: function () {
                    }
                }]
        }).present();
    };
    ;
    RcapPage.prototype.complete = function () {
        var _this = this;
        this.alert.create({
            title: "系统提示",
            message: "是否确认完成?",
            buttons: [{
                    text: "确定",
                    role: "确定",
                    handler: function () {
                        var data = _this.gztaskview.editRow;
                        data.zt = "rwzt_ywc";
                        data.wcsj = _this.dateUtil.getDateFormateCommon(new Date());
                        _this.dataService.update(_this.gztaskview.sn, data).subscribe(function (data) {
                            _this.navCtrl.pop();
                        });
                    }
                }, {
                    text: "取消",
                    role: "取消",
                    handler: function () {
                    }
                }]
        }).present();
    };
    ;
    RcapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-rcap',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\rcap\rcap.html"*/'<ion-header>\n\n	<ion-navbar>\n\n		<ion-title>日程安排</ion-title>\n\n	</ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n	<ion-list class="approval-list">\n\n		<ion-item>\n\n			<span class="bhq-title">项目名称: </span>\n\n			<span class="bhq-content">{{gztaskview.editRow.xmmc}}</span>\n\n		</ion-item>\n\n		<ion-item>\n\n			<span class="bhq-title">主题: </span>\n\n			<span class="bhq-content">{{gztaskview.editRow.zhuti}}</span>\n\n		</ion-item>\n\n		<ion-item>\n\n			<span class="bhq-title">部门名称: </span>\n\n			<span class="bhq-content">{{gztaskview.editRow.bmmcName}}</span>\n\n		</ion-item>\n\n		<ion-item>\n\n			<span class="bhq-title">负责人: </span>\n\n			<span class="bhq-content">{{gztaskview.editRow.fzrmc}}</span>\n\n		</ion-item>\n\n		<ion-item>\n\n			<span class="bhq-title">开始时间: </span>\n\n			<span class="bhq-content">{{gztaskview.editRow.kssj | asDate}}</span>\n\n		</ion-item>\n\n		<ion-item>\n\n			<span class="bhq-title">结束时间: </span>\n\n			<span class="bhq-content">{{gztaskview.editRow.jssj | asDate}}</span>\n\n		</ion-item>\n\n		<ion-item>\n\n			<span class="bhq-title">备注: </span>\n\n			<span class="bhq-content">{{gztaskview.editRow.sxsm}}</span>\n\n		</ion-item>\n\n		<ion-item>\n\n			<span class="bhq-title">发布人: </span>\n\n			<span class="bhq-content">{{gztaskview.editRow.cjrmc}}</span>\n\n		</ion-item>\n\n		<ion-item>\n\n			<span class="bhq-title">发布时间: </span>\n\n			<span class="bhq-content">{{gztaskview.editRow.cjsj | asDate}}</span>\n\n		</ion-item>\n\n		<ion-item>\n\n			<span class="bhq-title">状态: </span>\n\n			<span class="bhq-content">{{gztaskview.editRow.ztName}}</span>\n\n		</ion-item>\n\n		<ion-item>\n\n			<span class="bhq-title">完成时间: </span>\n\n			<span class="bhq-content">{{gztaskview.editRow.wcsj | asDate}}</span>\n\n		</ion-item>\n\n	</ion-list>\n\n\n\n	<!-- <div class="zs-page-foot"> \n\n		<a class="back"></a>\n\n		<a class="back_text" (click)="query();">确认</a> \n\n		<a class="right"></a> \n\n		<a class="right_text" (click)="complete();" >完成</a> \n\n	</div> -->\n\n\n\n	<button class="submit-btn" (click)="complete();" *ngIf="gztaskview.editRow.zt!=\'rwzt_ywc\'">完成</button>\n\n</ion-content>\n\n<!--\n\n<ion-footer >\n\n	<div class="footer-btn" (click)="complete();">\n\n		<img src="assets/imgs/pass.png"/>\n\n		<p>完成</p>\n\n	</div>\n\n</ion-footer>-->'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\rcap\rcap.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_data_DataService__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_3__app_core_data_BaseService__["a" /* BaseService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_7__app_utils_DateUtil__["a" /* DateUtil */],
            __WEBPACK_IMPORTED_MODULE_4__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], RcapPage);
    return RcapPage;
}());

//# sourceMappingURL=rcap.js.map

/***/ }),

/***/ 83:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProcessPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ProcessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProcessPage = /** @class */ (function () {
    function ProcessPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ProcessPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProcessPage');
    };
    ProcessPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-process',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\process\process.html"*/'<!--\n\n  Generated template for the ProcessPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>办理过程</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-list class="bhq-list">\n\n    <ion-item class="bhq-item">\n\n      <div class="bhq-item-title"><span class="num">1</span>开始</div>\n\n      <div class="bhq-item-content">\n\n        <div class="bhq-item-left">\n\n          <p>业务来源：余成俊</p>\n\n          <p>接收时间：2017-02-03  08:11</p>\n\n          <p>办理时间：2017-02-03  08:11</p>\n\n        </div>\n\n        <div class="bhq-item-right">\n\n          <p>办理人员：余成俊</p>\n\n          <p>办理部门：森林资源科</p>\n\n          <p>办理结果：创建</p>\n\n        </div>\n\n      </div>\n\n    </ion-item>\n\n    <ion-item class="bhq-item">\n\n      <div class="bhq-item-title"><span class="num">2</span>部门审核</div>\n\n      <div class="bhq-item-content">\n\n        <div class="bhq-item-left">\n\n          <p>业务来源：余成俊</p>\n\n          <p>接收时间：2017-02-03  08:11</p>\n\n          <p>办理时间：2017-02-03  08:11</p>\n\n        </div>\n\n        <div class="bhq-item-right">\n\n          <p>办理人员：余成俊</p>\n\n          <p>办理部门：森林资源科</p>\n\n          <p>办理结果：创建</p>\n\n        </div>\n\n      </div>\n\n    </ion-item>\n\n    <ion-item class="bhq-item">\n\n      <div class="bhq-item-title"><span class="num">3</span>科室审核</div>\n\n      <div class="bhq-item-content">\n\n        <div class="bhq-item-left">\n\n          <p>业务来源：余成俊</p>\n\n          <p>接收时间：2017-02-03  08:11</p>\n\n          <p>办理时间：2017-02-03  08:11</p>\n\n        </div>\n\n        <div class="bhq-item-right">\n\n          <p>办理人员：余成俊</p>\n\n          <p>办理部门：森林资源科</p>\n\n          <p>办理结果：创建</p>\n\n        </div>\n\n      </div>\n\n    </ion-item>\n\n    <ion-item class="bhq-item">\n\n      <div class="bhq-item-title"><span class="num">4</span>分管领导审核</div>\n\n      <div class="bhq-item-content">\n\n        <div class="bhq-item-left">\n\n          <p>业务来源：余成俊</p>\n\n          <p>接收时间：2017-02-03  08:11</p>\n\n          <p>办理时间：2017-02-03  08:11</p>\n\n        </div>\n\n        <div class="bhq-item-right">\n\n          <p>办理人员：余成俊</p>\n\n          <p>办理部门：森林资源科</p>\n\n          <p>办理结果：创建</p>\n\n        </div>\n\n      </div>\n\n    </ion-item>\n\n    <ion-item class="bhq-item">\n\n      <div class="bhq-item-title"><span class="num">5</span>局领导审核</div>\n\n      <div class="bhq-item-content">\n\n        <div class="bhq-item-left">\n\n          <p>业务来源：余成俊</p>\n\n          <p>接收时间：2017-02-03  08:11</p>\n\n          <p>办理时间：2017-02-03  08:11</p>\n\n        </div>\n\n        <div class="bhq-item-right">\n\n          <p>办理人员：余成俊</p>\n\n          <p>办理部门：森林资源科</p>\n\n          <p>办理结果：创建</p>\n\n        </div>\n\n      </div>\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\process\process.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ProcessPage);
    return ProcessPage;
}());

//# sourceMappingURL=process.js.map

/***/ }),

/***/ 9:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_core_security_SecurityService__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, securityService, alterCtrl, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.securityService = securityService;
        this.alterCtrl = alterCtrl;
        this.platform = platform;
        this.securityService.showMessage = true;
        try {
            this.username = window.localStorage.getItem('bhqoa_username');
            this.password = window.localStorage.getItem('bhqoa_password');
        }
        catch (e) {
        }
    }
    LoginPage.prototype.login = function () {
        var _this = this;
        var that = this;
        this.securityService.login(this.username, this.password, function (data) {
            window.localStorage.setItem('bhqoa_username', that.username);
            window.localStorage.setItem('bhqoa_password', that.password);
            window.localStorage.setItem('bhqoa_initLogin', "true");
            // this.navCtrl.push(TabsPage);
            _this.navCtrl.pop();
        });
    };
    LoginPage.prototype.exitApp = function () {
        var _this = this;
        //this.platform.exitApp();
        this.alterCtrl.create({
            title: "系统提示",
            message: "是否退出系统？",
            buttons: [{
                    text: "确定",
                    role: "确定",
                    handler: function () {
                        _this.platform.exitApp();
                    }
                }, {
                    text: "取消",
                    role: "取消",
                    handler: function () {
                    }
                }]
        }).present();
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"F:\workProjects\project-git\bhqoa\app\src\pages\login\login2.html"*/'<!-- -->\n\n<ion-content padding class="animated fadeIn login auth-page">\n\n    <div class="login-content">\n\n\n\n      <!-- Logo -->\n\n      <div padding-horizontal text-center class="animated fadeInDown">\n\n        <div class="logo"></div>\n\n        <h2 ion-text class="text-primary">\n\n          <strong>茂兰</strong>国家级自然保护区\n\n        </h2>\n\n      </div>\n\n\n\n      <!-- Login form -->\n\n      <form class="login-list-form">\n\n        <ion-item>\n\n          <ion-label floating>\n\n            <ion-icon name="md-person" item-start class="text-primary"></ion-icon>\n\n            用户名\n\n          </ion-label>\n\n          <ion-input name="username" type="text" [(ngModel)]="username"></ion-input>\n\n        </ion-item>\n\n\n\n        <ion-item>\n\n          <ion-label floating>\n\n            <ion-icon name="lock" item-start class="text-primary"></ion-icon>\n\n            密码\n\n          </ion-label>\n\n          <ion-input name="password" type="password" [(ngModel)]="password"></ion-input>\n\n        </ion-item>\n\n\n\n        <!-- <ion-item no-lines>\n\n    <label item-right>记住密码</label>\n\n    <ion-toggle></ion-toggle>\n\n  </ion-item> -->\n\n      </form>\n\n\n\n      <div style="margin-top: 4rem;">\n\n        <button ion-button icon-start block color="thirdary" tappable (click)="login()">\n\n          <ion-icon name="log-in"></ion-icon>\n\n          登录\n\n        </button>\n\n      </div>\n\n\n\n\n\n\n\n    </div>\n\n  </ion-content>\n\n'/*ion-inline-end:"F:\workProjects\project-git\bhqoa\app\src\pages\login\login2.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__app_core_security_SecurityService__["a" /* SecurityService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

},[405]);
//# sourceMappingURL=main.js.map